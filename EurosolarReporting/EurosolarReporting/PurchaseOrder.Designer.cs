namespace EurosolarReporting
{
    partial class PurchaseOrder
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseOrder));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.PanDetails = new Telerik.Reporting.Panel();
            this.txtCompanyAddress = new Telerik.Reporting.TextBox();
            this.txtCompanyName = new Telerik.Reporting.TextBox();
            this.txtPODate = new Telerik.Reporting.TextBox();
            this.txtPONo = new Telerik.Reporting.TextBox();
            this.txtCustomerID = new Telerik.Reporting.TextBox();
            this.txtVendor = new Telerik.Reporting.TextBox();
            this.txtVendorAddress = new Telerik.Reporting.TextBox();
            this.txtShipTo = new Telerik.Reporting.TextBox();
            this.txtShipVia = new Telerik.Reporting.TextBox();
            this.txtShippingMethod = new Telerik.Reporting.TextBox();
            this.txtShippingTerms = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.PanShipDetails = new Telerik.Reporting.Panel();
            this.PanStockItemDetails = new Telerik.Reporting.Panel();
            this.tblStockOrderItems = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.txtNotes = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.PanDetails,
            this.PanStockItemDetails,
            this.txtNotes});
            this.detailSection1.Name = "detailSection1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // PanDetails
            // 
            this.PanDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtCompanyAddress,
            this.txtCompanyName,
            this.txtPODate,
            this.txtPONo,
            this.txtCustomerID,
            this.txtVendor,
            this.txtVendorAddress,
            this.txtShipTo,
            this.txtShipVia,
            this.txtShippingMethod,
            this.txtShippingTerms,
            this.textBox1,
            this.PanShipDetails});
            this.PanDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PanDetails.Name = "PanDetails";
            this.PanDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
            // 
            // txtCompanyAddress
            // 
            this.txtCompanyAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(3.4D));
            this.txtCompanyAddress.Name = "txtCompanyAddress";
            this.txtCompanyAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.5D), Telerik.Reporting.Drawing.Unit.Cm(2.6D));
            this.txtCompanyAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtCompanyAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtCompanyAddress.Value = "";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(2.3D));
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.5D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.txtCompanyName.Style.Font.Bold = true;
            this.txtCompanyName.Style.Font.Name = "Arial";
            this.txtCompanyName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(23D);
            this.txtCompanyName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCompanyName.TextWrap = false;
            this.txtCompanyName.Value = "";
            // 
            // txtPODate
            // 
            this.txtPODate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.6D), Telerik.Reporting.Drawing.Unit.Cm(3.7D));
            this.txtPODate.Name = "txtPODate";
            this.txtPODate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtPODate.Style.Font.Bold = true;
            this.txtPODate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPODate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtPODate.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPODate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPODate.Style.Visible = true;
            this.txtPODate.TextWrap = false;
            this.txtPODate.Value = "";
            // 
            // txtPONo
            // 
            this.txtPONo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.6D), Telerik.Reporting.Drawing.Unit.Cm(4.2D));
            this.txtPONo.Name = "txtPONo";
            this.txtPONo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtPONo.Style.Font.Bold = true;
            this.txtPONo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPONo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtPONo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPONo.Style.Visible = true;
            this.txtPONo.TextWrap = false;
            this.txtPONo.Value = "";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.6D), Telerik.Reporting.Drawing.Unit.Cm(4.7D));
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtCustomerID.Style.Font.Bold = true;
            this.txtCustomerID.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomerID.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtCustomerID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomerID.Style.Visible = true;
            this.txtCustomerID.TextWrap = false;
            this.txtCustomerID.Value = "";
            // 
            // txtVendor
            // 
            this.txtVendor.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(6.8D));
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtVendor.Style.Font.Bold = false;
            this.txtVendor.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtVendor.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtVendor.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtVendor.Style.Visible = true;
            this.txtVendor.TextWrap = false;
            this.txtVendor.Value = "";
            // 
            // txtVendorAddress
            // 
            this.txtVendorAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(7.4D));
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6D), Telerik.Reporting.Drawing.Unit.Cm(3.1D));
            this.txtVendorAddress.Style.Font.Bold = false;
            this.txtVendorAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtVendorAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtVendorAddress.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtVendorAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtVendorAddress.Style.Visible = true;
            this.txtVendorAddress.TextWrap = true;
            this.txtVendorAddress.Value = "";
            // 
            // txtShipTo
            // 
            this.txtShipTo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.3D), Telerik.Reporting.Drawing.Unit.Cm(6.8D));
            this.txtShipTo.Name = "txtShipTo";
            this.txtShipTo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.9D), Telerik.Reporting.Drawing.Unit.Cm(3.7D));
            this.txtShipTo.Style.Font.Bold = false;
            this.txtShipTo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtShipTo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtShipTo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtShipTo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtShipTo.Style.Visible = true;
            this.txtShipTo.TextWrap = true;
            this.txtShipTo.Value = "";
            // 
            // txtShipVia
            // 
            this.txtShipVia.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.txtShipVia.Name = "txtShipVia";
            this.txtShipVia.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtShipVia.Style.Font.Bold = false;
            this.txtShipVia.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtShipVia.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtShipVia.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtShipVia.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtShipVia.Style.Visible = true;
            this.txtShipVia.TextWrap = false;
            this.txtShipVia.Value = "";
            // 
            // txtShippingMethod
            // 
            this.txtShippingMethod.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.txtShippingMethod.Name = "txtShippingMethod";
            this.txtShippingMethod.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtShippingMethod.Style.Font.Bold = false;
            this.txtShippingMethod.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtShippingMethod.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtShippingMethod.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtShippingMethod.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtShippingMethod.Style.Visible = true;
            this.txtShippingMethod.TextWrap = false;
            this.txtShippingMethod.Value = "";
            // 
            // txtShippingTerms
            // 
            this.txtShippingTerms.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.4D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.txtShippingTerms.Name = "txtShippingTerms";
            this.txtShippingTerms.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtShippingTerms.Style.Font.Bold = false;
            this.txtShippingTerms.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtShippingTerms.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtShippingTerms.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtShippingTerms.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtShippingTerms.Style.Visible = true;
            this.txtShippingTerms.TextWrap = false;
            this.txtShippingTerms.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Style.Visible = true;
            this.textBox1.TextWrap = false;
            this.textBox1.Value = "";
            // 
            // PanShipDetails
            // 
            this.PanShipDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(10.7D));
            this.PanShipDetails.Name = "PanShipDetails";
            this.PanShipDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            // 
            // PanStockItemDetails
            // 
            this.PanStockItemDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tblStockOrderItems});
            this.PanStockItemDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
            this.PanStockItemDetails.Name = "PanStockItemDetails";
            this.PanStockItemDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(9.6D));
            // 
            // tblStockOrderItems
            // 
            this.tblStockOrderItems.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.788D)));
            this.tblStockOrderItems.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(10.493D)));
            this.tblStockOrderItems.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.006D)));
            this.tblStockOrderItems.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.778D)));
            this.tblStockOrderItems.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.149D)));
            this.tblStockOrderItems.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.tblStockOrderItems.Body.SetCellContent(0, 2, this.textBox3);
            this.tblStockOrderItems.Body.SetCellContent(0, 3, this.textBox5);
            this.tblStockOrderItems.Body.SetCellContent(0, 4, this.textBox7);
            this.tblStockOrderItems.Body.SetCellContent(0, 1, this.textBox2);
            this.tblStockOrderItems.Body.SetCellContent(0, 0, this.textBox4);
            tableGroup1.Name = "group1";
            tableGroup2.Name = "group";
            tableGroup3.Name = "tableGroup";
            tableGroup4.Name = "tableGroup1";
            tableGroup5.Name = "tableGroup2";
            this.tblStockOrderItems.ColumnGroups.Add(tableGroup1);
            this.tblStockOrderItems.ColumnGroups.Add(tableGroup2);
            this.tblStockOrderItems.ColumnGroups.Add(tableGroup3);
            this.tblStockOrderItems.ColumnGroups.Add(tableGroup4);
            this.tblStockOrderItems.ColumnGroups.Add(tableGroup5);
            this.tblStockOrderItems.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox2,
            this.textBox3,
            this.textBox5,
            this.textBox7});
            this.tblStockOrderItems.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.tblStockOrderItems.Name = "tblStockOrderItems";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup";
            this.tblStockOrderItems.RowGroups.Add(tableGroup6);
            this.tblStockOrderItems.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.214D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.TextWrap = false;
            this.textBox3.Value = "=Fields.OrderQuantity";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.778D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.TextWrap = false;
            this.textBox5.Value = "=Fields.UnitPrice";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.149D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.TextWrap = false;
            this.textBox7.Value = "=Fields.Amount";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.493D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.TextWrap = false;
            this.textBox2.Value = "=Fields.StockItem";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.788D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.TextWrap = false;
            this.textBox4.Value = "=Fields.StockItemID";
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(22.1D));
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.3D), Telerik.Reporting.Drawing.Unit.Cm(3D));
            this.txtNotes.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtNotes.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtNotes.Value = "";
            // 
            // PurchaseOrder
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1});
            this.Name = "PurchaseOrder";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Panel PanDetails;
        private Telerik.Reporting.TextBox txtCompanyAddress;
        private Telerik.Reporting.TextBox txtCompanyName;
        private Telerik.Reporting.TextBox txtPODate;
        private Telerik.Reporting.TextBox txtPONo;
        private Telerik.Reporting.TextBox txtCustomerID;
        private Telerik.Reporting.TextBox txtVendor;
        private Telerik.Reporting.TextBox txtVendorAddress;
        private Telerik.Reporting.TextBox txtShipTo;
        private Telerik.Reporting.TextBox txtShipVia;
        private Telerik.Reporting.TextBox txtShippingMethod;
        private Telerik.Reporting.TextBox txtShippingTerms;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Panel PanShipDetails;
        private Telerik.Reporting.Panel PanStockItemDetails;
        private Telerik.Reporting.Table tblStockOrderItems;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox txtNotes;
    }
}