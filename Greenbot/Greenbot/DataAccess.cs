﻿using System.Data;
using System.Data.Common;

namespace Greenbot
{
    class DataAccess
    {
        public static DbCommand CreateCommand()
        {
            string connectionString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";

            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

            DbCommand comm = conn.CreateCommand();

            comm.CommandType = CommandType.StoredProcedure;

            return comm;
        }

        public static int ExecuteNonQuery(DbCommand command)
        {
            int affectedRows = -1;
            
            command.Connection.Open();
            command.CommandTimeout = 100000;
            
            // Execute the command and get the number of affected rows
            affectedRows = command.ExecuteNonQuery();
            
            //Close Connection
            command.Connection.Close();
            
            // return the number of affected rows
            return affectedRows;
        }

        public static DataTable ExecuteSelectCommand(DbCommand command)
        {
            // The DataTable to be returned        
            DataTable table;
            // Execute the command making sure the connection gets closed in the end
            command.Connection.Open();
            command.CommandTimeout = 100000;
            
            // Execute the command and save the results in a DataTable
            DbDataReader reader = command.ExecuteReader();
            table = new DataTable();
            table.Load(reader);
            
            // Close the reader 
            reader.Close();

            command.Connection.Close();

            return table;
        }
    }
}
