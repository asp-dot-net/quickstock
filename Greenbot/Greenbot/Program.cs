﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;

namespace Greenbot
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dt = ClsGreenbot.tbl_APIFetchData_UtilitiesBySource("GreenBot");

            //DataTable dtSerialNo = new DataTable();

            DataTable dtSerialNo = new DataTable();
            dtSerialNo.Columns.Add("ProjectNo", typeof(string));
            dtSerialNo.Columns.Add("SerialNo", typeof(string));
            dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
            dtSerialNo.Columns.Add("CompanyID", typeof(int));
            dtSerialNo.Columns.Add("Varified", typeof(string));
            dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
            dtSerialNo.Columns.Add("InstallerName", typeof(string));

            //for (int i = 0; i < 1; i++)
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string CompanyID = dt.Rows[i]["CompanyID"].ToString();
                //string FromDate = dt.Rows[i]["FromDate"].ToString();
                string ToDate = dt.Rows[i]["ToDate"].ToString();
                string username = dt.Rows[i]["username"].ToString();
                string password = dt.Rows[i]["password"].ToString();

                //string CompanyID = "4";
                string FromDate = "01/01/2020";
                //string ToDate = "28/05/2021";
                //string username = "solarbridgeregistry";
                //string password = "solarbridge123";

                dtSerialNo = GetAllGreenbotDetails(FromDate, ToDate, CompanyID, username, password, dtSerialNo);
            }

            //DataTable dtSerialNo = GetAllGreenbotDetails("01/01/2018", "31/12/2020", "1", "arisesolar", "arisesolar1");

            //--bool Delete = ClsDbData.tblSerialNoFromBSGB_DeleteByBSGBFlag("1");
            int UpdateData = ClsGreenbot.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
            bool UpdateProjectNoArise = ClsGreenbot.tblProjectNoYN_Update();
            bool UpdateFlag = ClsGreenbot.Update_Flags();
            bool Update = ClsGreenbot.tbl_APIFetchData_Utilities_Update("Greenbot", DateTime.Now.AddHours(14).ToString());

            string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
        }

        public static DataTable GetAllGreenbotDetails(string FromDate, string ToDate, string CompanyID, string Username, string Password, DataTable dtSerialNo)
        {
            try
            {
                var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
                var request = new RestRequest(Method.POST);
                //request.AddParameter("Username", "arisesolar");
                //request.AddParameter("Password", "arisesolar1");
                request.AddParameter("Username", Username);
                request.AddParameter("Password", Password);
                var response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                    string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                    client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + FromDate + "&ToDate=" + ToDate);
                    request = new RestRequest(Method.GET);
                    client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                    client.AddDefaultHeader("Content-Type", "application/json");
                    client.AddDefaultHeader("Accept", "application/json");
                    IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                    if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                        DataTable dtSTCDetails = new DataTable();
                        dtSTCDetails.Columns.Add("JobID", typeof(int));
                        dtSTCDetails.Columns.Add("PVDNo", typeof(string));
                        dtSTCDetails.Columns.Add("CalculatedSTC", typeof(string));
                        dtSTCDetails.Columns.Add("STCStatus", typeof(string));
                        dtSTCDetails.Columns.Add("STCSubmissionDate", typeof(string));
                        dtSTCDetails.Columns.Add("NoofPanel", typeof(string));
                        dtSTCDetails.Columns.Add("CompanyID", typeof(int));
                        dtSTCDetails.Columns.Add("ProjectNo", typeof(string));

                        DataTable dtNoOfInverter = new DataTable();
                        dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
                        dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
                        dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

                        //int index = jobData.lstJobData.Count;
                        ////Job_Response.lstJobData lstJobData = jobData.lstJobData[index - 1]; // Root Object
                        //Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                        //Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

                        //SerialNo = JobSystemDetails.SerialNumbers;

                        for (int i = 0; i < jobData.lstJobData.Count; i++)
                        {
                            Job_Response.lstJobData lstJobData = jobData.lstJobData[i]; // Root Object
                            Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details
                            Job_Response.InstallerView InstallerView = lstJobData.InstallerView; // get Child object Details

                            List<Job_Response.lstJobInverterDetails> lstJobInverterDetails = lstJobData.lstJobInverterDetails;
                            string NoOfInverter = "0";
                            if (lstJobInverterDetails.Count > 0)
                            {
                                NoOfInverter = lstJobInverterDetails[lstJobInverterDetails.Count - 1].NoOfInverter;

                                NoOfInverter = !string.IsNullOrEmpty(NoOfInverter) ? NoOfInverter : "0";
                            }

                            string ProjectNo = "";
                            if (CompanyID == "3")
                            {
                                ProjectNo = lstJobData.BasicDetails.JobID;
                            }
                            else
                            {
                                ProjectNo = lstJobData.BasicDetails.RefNumber;
                            }

                            string SerialNo = JobSystemDetails.SerialNumbers;
                            string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

                            string InstallerName = "";
                            try
                            {
                                if (InstallerView != null)
                                {
                                    InstallerName = (InstallerView.FirstName == null ? "" : InstallerView.FirstName.ToString()) + " " + (InstallerView.LastName == null ? "" : InstallerView.LastName.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                int index = i;
                            }

                            for (int j = 0; j < SerialNoList.Length; j++)
                            {
                                DataRow dr = dtSerialNo.NewRow();
                                dr["ProjectNo"] = ProjectNo.Trim();
                                dr["SerialNo"] = SerialNoList[j];
                                dr["BSGBFlag"] = 1; // GreenBot
                                dr["CompanyID"] = CompanyID;
                                dr["StockCategoryID"] = 1;
                                dr["Varified"] = "";
                                dr["InstallerName"] = InstallerName.Trim();
                                dtSerialNo.Rows.Add(dr);
                            }

                            // Now Data Table For Wholesale
                            if (CompanyID == "3")
                            {
                                Job_Response.JobSTCDetails JobSTCDetails = lstJobData.JobSTCDetails;
                                Job_Response.JobSTCStatusData JobSTCStatusData = lstJobData.JobSTCStatusData;

                                string JobID = lstJobData.BasicDetails.JobID;
                                string FailedAccreditationCode = JobSTCDetails.FailedAccreditationCode;
                                string CalculatedSTC = JobSTCStatusData.CalculatedSTC;
                                string STCStatus = JobSTCStatusData.STCStatus;
                                string STCSubmissionDate = JobSTCStatusData.STCSubmissionDate;
                                string NoofPanel = JobSystemDetails.NoOfPanel;

                                DataRow dr = dtSTCDetails.NewRow();
                                dr["JobID"] = JobID;
                                dr["PVDNo"] = FailedAccreditationCode;
                                dr["CalculatedSTC"] = CalculatedSTC;
                                dr["STCStatus"] = STCStatus;
                                dr["STCSubmissionDate"] = STCSubmissionDate;
                                dr["NoofPanel"] = NoofPanel;
                                dr["CompanyID"] = Convert.ToInt32(CompanyID);
                                dr["ProjectNo"] = ProjectNo;
                                dtSTCDetails.Rows.Add(dr);
                            }

                            DataRow dr1 = dtNoOfInverter.NewRow();
                            dr1["ProjectNo"] = ProjectNo;
                            dr1["CompanyID"] = CompanyID;
                            dr1["NoOfInverter"] = NoOfInverter;
                            dtNoOfInverter.Rows.Add(dr1);
                        }

                        if (CompanyID == "3")
                        {
                            if (dtSTCDetails.Rows.Count > 0)
                            {
                                int UpdateData = ClsGreenbot.Bulk_InsertUpdate_tblSTCDetails(dtSTCDetails);
                                string msg = "<script>alert('Total Record is " + dtSTCDetails.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                            }
                        }

                        if (dtNoOfInverter.Rows.Count > 0)
                        {
                            int UpdateData = ClsGreenbot.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                            string msg = "<script>alert('Total Record is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return dtSerialNo;
        }
    }

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
}
#endregion