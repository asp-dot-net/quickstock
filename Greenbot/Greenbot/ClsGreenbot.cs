﻿using System;
using System.Data;
using System.Data.Common;

namespace Greenbot
{
    class ClsGreenbot
    {
        public static DataTable tbl_APIFetchData_UtilitiesBySource(string APISource)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "tbl_APIFetchData_UtilitiesBySource";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@APISource";
            if (!string.IsNullOrEmpty(APISource))
                param.Value = APISource;
            else
                param.Value = DBNull.Value;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);

            DataTable result = new DataTable();
            try
            {
                result = DataAccess.ExecuteSelectCommand(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
            }
            return result;
        }

        public static int Bulk_InsertUpdate_tblSerialNoFromBSGB(DataTable dt)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "Bulk_InsertUpdate_tblSerialNoFromBSGB";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@dtSerialNo";
            if (dt != null)
                param.Value = dt;
            else
                param.Value = DBNull.Value;
            comm.Parameters.Add(param);

            int result = 0;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
            }
            return result;
        }

        public static bool tblProjectNoYN_Update()
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "tblProjectNoYN_Update";

            //DbParameter param = comm.CreateParameter();
            //param.ParameterName = "@tblProjectsArise";
            //if (dt != null)
            //    param.Value = dt;
            //else
            //    param.Value = DBNull.Value;
            ////param.DbType = DbType.Object;
            //comm.Parameters.Add(param);

            int result = -1;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch
            {
            }
            return (result != -1);
        }

        public static bool Update_Flags()
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "Update_Flags";

            //DbParameter param = comm.CreateParameter();
            //param.ParameterName = "@tblProjectsArise";
            //if (dt != null)
            //    param.Value = dt;
            //else
            //    param.Value = DBNull.Value;
            ////param.DbType = DbType.Object;
            //comm.Parameters.Add(param);

            int result = -1;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch
            {
            }
            return (result != -1);
        }

        public static bool tbl_APIFetchData_Utilities_Update(string APISource, string LastUpdatedOn)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "tbl_APIFetchData_Utilities_Update";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@APISource";
            if (!string.IsNullOrEmpty(APISource))
                param.Value = APISource;
            else
                param.Value = DBNull.Value;
            param.DbType = DbType.String;
            param.Size = 50;
            comm.Parameters.Add(param);

            param = comm.CreateParameter();
            param.ParameterName = "@LastUpdatedOn";
            if (!string.IsNullOrEmpty(LastUpdatedOn))
                param.Value = LastUpdatedOn;
            else
                param.Value = DBNull.Value;
            param.DbType = DbType.DateTime;
            comm.Parameters.Add(param);

            int result = -1;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch
            {
            }
            return (result != -1);
        }

        public static int Bulk_InsertUpdate_tblSTCDetails(DataTable dt)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "Bulk_InsertUpdate_tblSTCDetails";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@dtSTCDetails";
            if (dt != null)
                param.Value = dt;
            else
                param.Value = DBNull.Value;
            comm.Parameters.Add(param);

            int result = 0;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
            }
            return result;
        }

        public static int Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(DataTable dt)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@dtInverterDetails";
            if (dt != null)
                param.Value = dt;
            else
                param.Value = DBNull.Value;
            comm.Parameters.Add(param);

            int result = 0;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
            }
            return result;
        }
    }
}
