﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Job_Response
/// </summary>
public class Job_Response
{
    public Job_Response()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class RootObject
    {
        public List<lstJobData> lstJobData { get; set; }

        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class lstJobData
    {
        public BasicDetails BasicDetails { get; set; }
        public JobSystemDetails JobSystemDetails { get; set; }
        public InstallerView InstallerView { get; set; }
        public JobSTCDetails JobSTCDetails { get; set; }
        public JobSTCStatusData JobSTCStatusData { get; set; }
        public List<lstJobInverterDetails> lstJobInverterDetails { get; set; }
    }

    public class BasicDetails
    {
        public string VendorJobId { get; set; }
        public string JobID { get; set; }
        public string Title { get; set; }
        public string RefNumber { get; set; }
        public string Description { get; set; }
        public string JobType { get; set; }
        public string JobStage { get; set; }
        public string Priority { get; set; }
        public string InstallationDate { get; set; }
        public string CreatedDate { get; set; }
    }

    public class JobSystemDetails
    {
        public string SystemSize { get; set; }
        public string SerialNumbers { get; set; }
        public string CalculatedSTC { get; set; }
        public string NoOfPanel { get; set; }
    }

    public class InstallerView
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class JobSTCDetails
    {
        public string FailedAccreditationCode { get; set; }
    }

    public class JobSTCStatusData
    {
        public string STCStatus { get; set; }
        public string STCSubmissionDate { get; set; }
        public string CalculatedSTC { get; set; }
    }

    public class lstJobInverterDetails
    {
        public string NoOfInverter { get; set; }
    }

    #region
    public partial class RootObjectBS
    {
        [JsonProperty("Success")]
        public Success Success { get; set; }
    }

    public partial class Success
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Details")]
        public Details Details { get; set; }
    }

    public class Details
    {
        //public Panels panels { get; set; }
        //public Inverters inverters { get; set; }

        [JsonProperty("crmid", NullValueHandling = NullValueHandling.Ignore)]
        public string Crmid { get; set; }

        [JsonProperty("panels")]
        public Dictionary<string, The03091220_C100084> Panels { get; set; }

        [JsonProperty("inverters")]
        public Dictionary<string, The03091220_C100084> Inverters { get; set; }
    }

    public enum The03091220_C100084 { N, V, U };

    public partial class Panels
    {
        public List<string> panels { get; set; }
    }

    public partial class Inverters
    {
        public List<string> inverters { get; set; }
    }

    public partial class NoOfPanelInverter
    {
        public string NoOfPanel;
        public string NoOfInverter;
    }

    #endregion

}