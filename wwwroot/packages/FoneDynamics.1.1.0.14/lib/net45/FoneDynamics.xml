<?xml version="1.0"?>
<doc>
    <assembly>
        <name>FoneDynamics</name>
    </assembly>
    <members>
        <member name="T:FoneDynamics.Base.ApiException">
            <summary>
            Represents an exception that occurred during an API request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Base.ApiException.HttpStatusCode">
            <summary>
            The status code of the HTTP request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Base.ApiException.ErrorCode">
            <summary>
            The error code as per https://www.fonedynamics.com/docs/rest-api/errors/
            </summary>
        </member>
        <member name="M:FoneDynamics.Base.ApiException.#ctor(System.Int32,System.String,System.String)">
            <summary>
            Constructs a new ApiException.
            </summary>
        </member>
        <member name="T:FoneDynamics.Extensions.UnixDateExtensions">
            <summary>
            Extension methods for dealing with Unix dates.
            </summary>
        </member>
        <member name="F:FoneDynamics.Extensions.UnixDateExtensions._epoch">
            <summary>
            The Unix epoch.
            </summary>
        </member>
        <member name="M:FoneDynamics.Extensions.UnixDateExtensions.ToUnixTime(System.DateTime)">
            <summary>
            Converts the specified time to Unix time.
            </summary>
        </member>
        <member name="M:FoneDynamics.Extensions.UnixDateExtensions.FromUnixTime(System.Int64)">
            <summary>
            Converts the specified Unix time to a DateTime.
            </summary>
        </member>
        <member name="T:FoneDynamics.FoneDynamicsClient">
            <summary>
            The Fone Dynamics client.
            </summary>
        </member>
        <member name="M:FoneDynamics.FoneDynamicsClient.#ctor(System.String,System.String,System.String)">
            <summary>
            Constructs a new FoneDynamicsClient.
            </summary>
            <param name="accountSid">The AccountSid that represents the account you are using.</param>
            <param name="token">A token associated with the account.</param>
            <param name="defaultPropertySid">The PropertySid to use in applicable requests where no PropertySid is specified.</param>
        </member>
        <member name="P:FoneDynamics.FoneDynamicsClient.DefaultInstance">
            <summary>
            The default instance of FoneDynamicsClient.
            </summary>
        </member>
        <member name="M:FoneDynamics.FoneDynamicsClient.CreateInstance(System.String,System.String,System.String)">
            <summary>
            Creates a default instance using the specified parameters.
            </summary>
        </member>
        <member name="M:FoneDynamics.FoneDynamicsClient.Initialize(System.String,System.String,System.String)">
            <summary>
            Initializes the default FoneDynamicsClient.
            </summary>
            <param name="accountSid">The AccountSid that represents the account you are using.</param>
            <param name="token">A token associated with the account.</param>
            <param name="defaultPropertySid">The PropertySid to use in applicable requests where no PropertySid is specified.</param>
        </member>
        <member name="P:FoneDynamics.FoneDynamicsClient.AccountSid">
            <summary>
            The AccountSid to use for requests.
            </summary>
        </member>
        <member name="P:FoneDynamics.FoneDynamicsClient.Token">
            <summary>
            The token to use for requests.
            </summary>
        </member>
        <member name="P:FoneDynamics.FoneDynamicsClient.DefaultPropertySid">
            <summary>
            The default PropertySid to use for requests where a PropertySid is not specified.
            </summary>
        </member>
        <member name="P:FoneDynamics.FoneDynamicsClient.HttpClient">
            <summary>
            Gets the HttpClient associated with this FoneDynamicsClient.
            </summary>
        </member>
        <member name="M:FoneDynamics.FoneDynamicsClient.SetDefaults(System.String@,FoneDynamics.FoneDynamicsClient@)">
            <summary>
            Sets the default values.
            Throws validation exceptions when not configured correctly.
            </summary>
        </member>
        <member name="T:FoneDynamics.Http.HttpClient">
            <summary>
            Client to make HTTP requests.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.HttpClient._httpClient">
            <summary>
            The .NET HttpClient to use when making requests.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpClient.#ctor">
            <summary>
            Constructor.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpClient.Send(FoneDynamics.Http.Request)">
            <summary>
            Sends the specified request and returns the response.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpClient.SendAsync(FoneDynamics.Http.Request)">
            <summary>
            Sends the specified request and returns the response.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpClient.CreateHttpRequest(FoneDynamics.Http.Request)">
            <summary>
            Creates the HTTP request.
            </summary>
        </member>
        <member name="T:FoneDynamics.Http.HttpMethod">
            <summary>
            Describes a HTTP method.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.HttpMethod.Get">
            <summary>
            GET.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.HttpMethod.Post">
            <summary>
            POST.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.HttpMethod.Put">
            <summary>
            PUT.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.HttpMethod.Delete">
            <summary>
            DELETE.
            </summary>
        </member>
        <member name="T:FoneDynamics.Http.HttpResponse">
            <summary>
            Represents the response from a HTTP request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.HttpResponse.HttpStatusCode">
            <summary>
            The status code of the response.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.HttpResponse.Content">
            <summary>
            The response body.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpResponse.#ctor(System.Int32,System.Byte[])">
            <summary>
            Constructs a new HttpResponse.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.HttpResponse.GetContentString">
            <summary>
            Returns the content as a string. Uses UTF8.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.HttpResponse.IsSuccess">
            <summary>
            Gets whether the HttpStatusCode indicates success.  ie. whether it is 2xx.
            </summary>
        </member>
        <member name="T:FoneDynamics.Http.Request">
            <summary>
            Describes a request to an external endpoint.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.Request.PLATFORM">
            <summary>
            Platform version and details string to use in the User-Agent.
            </summary>
        </member>
        <member name="F:FoneDynamics.Http.Request.API_HOST">
            <summary>
            The hostname of the API.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.Path">
            <summary>
            The path to make the request to.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.QueryParameters">
            <summary>
            Optional list of query string parameters.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.Method">
            <summary>
            The HTTP method to use for the request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.RequestBody">
            <summary>
            Optional body of the request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.ContentType">
            <summary>
            Optional content type of the request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.AccountSid">
            <summary>
            The AccountSid to use for this request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Http.Request.Token">
            <summary>
            The token to use for this request.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.Request.#ctor(FoneDynamics.Http.HttpMethod,System.String,System.String,System.String)">
            <summary>
            Constructs a new request.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.Request.AddQueryParameter(System.String,System.String)">
            <summary>
            Adds a new query parameter to the request.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.Request.SetBody(System.Byte[],System.String)">
            <summary>
            Sets the body of the request.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.Request.SetBody(System.String,System.String)">
            <summary>
            Sets the body of the request. The request body will be encoded as UTF8.
            </summary>
        </member>
        <member name="M:FoneDynamics.Http.Request.CreateUri">
            <summary>
            Returns the URI for this request.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.Errors.ErrorResponse">
            <summary>
            Represents an error resource.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.Errors.ErrorResponse.ResponseStatus">
            <summary>
            Information about the response.
            </summary>
        </member>
        <member name="M:FoneDynamics.Rest.V2.Errors.ErrorResponse.CreateException(FoneDynamics.Http.HttpResponse)">
            <summary>
            Creates an exception from the provided response.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.Errors.ResponseStatus">
            <summary>
            The ResponseStatus object.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.Errors.ResponseStatus.ErrorCode">
            <summary>
            The error code associated with the response status.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.Errors.ResponseStatus.Message">
            <summary>
            The message associated with the error code.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.BatchMessageRequest">
            <summary>
            The request to a BatchMessage.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.BatchMessageRequest.Messages">
            <summary>
            The messages to be sent.
            </summary>
        </member>
        <member name="M:FoneDynamics.Rest.V2.BatchMessageRequest.#ctor(System.Collections.Generic.IEnumerable{FoneDynamics.Rest.V2.MessageResource})">
            <summary>
            Constructs a new BatchMessageRequest with the specified messages.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.BatchMessageResource">
            <summary>
            A MessageResource also containing details of any errors occurred
            while processing the send request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.BatchMessageResource.ErrorMessage">
            <summary>
            The error message associated with the failure.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.BatchMessageResource.Successful">
            <summary>
            Gets whether sending the message was successful.
            If it failed, ErrorCode and ErrorMessage will be set containing details of the error.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.MessageDirection">
            <summary>
            Represents the direction of a message.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageDirection.Transmit">
            <summary>
            The message is an outgoing message.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageDirection.Receive">
            <summary>
            The message is an incoming message.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.MessageResource">
            <summary>
            Represents a message and contains messaging functionality.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.MessageSid">
            <summary>
            The message secure identifier which uniquely identifies this message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.AccountSid">
            <summary>
            The account secure identifier associated with the message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.PropertySid">
            <summary>
            The property secure identifier associated with the message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.From">
            <summary>
            The alphanumeric or E164 formatted sender ID of the message.
            If composing a message, a null or empty value indicates that a number should
            be dynamically leased for this message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.To">
            <summary>
            The E164 formatted recipient of the SMS message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Text">
            <summary>
            The content of the SMS message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.DeliveryReceipt">
            <summary>
            Set to true if a delivery receipt was requested and false otherwise.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.NumSegments">
            <summary>
            The number of message segments.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Status">
            <summary>
            The message status. See below table for possible values.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Direction">
            <summary>
            The direction of the message.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Scheduled">
            <summary>
            Timestamp of when the message is/was scheduled to be sent.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Created">
            <summary>
            Timestamp of when the message resource was created.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Submitted">
            <summary>
            Timestamp of when the message was submitted to the SMS Center.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Delivered">
            <summary>
            Timestamp of when the message delivery occurred.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Received">
            <summary>
            Timestamp of when an inbound message was received.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.ErrorCode">
            <summary>
            Error code (if message submission or delivery failed).
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.Schedule">
            <summary>
            The UTC time in seconds since Unix Epoch to send the message. This can be at most
            14 days in the future. If the value is in the past, the request will fail, unless
            the value is less than 1 hour in the past, in which case the request will succeed
            and the message will be sent immediately.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.DeliveryReceiptWebhookUri">
            <summary>
            The callback URI to invoke when a delivery receipt is received. Note that
            DeliveryReceipt must be set to true for this to be triggered.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.DeliveryReceiptWebhookMethod">
            <summary>
            The method to use for delivery receipt callbacks. Valid options are POST and GET.
            Default is POST.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.ForwardToSms">
            <summary>
            When a response is received, forward it to this number in E164 format.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.ForwardFromSms">
            <summary>
            When forwarding via SMS, send from this E164 formatted number or alphanumeric sender
            ID. By default the sender ID will be the sender ID of the responding party.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.ForwardToEmail">
            <summary>
            When a response is received, forward it over email to this email address.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.ForwardFromEmail">
            <summary>
            When forwarding via email, send from this email address. By default this is a
            "no reply" email address.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.WebhookUri">
            <summary>
            The callback URI to invoke when a response is received.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResource.WebhookMethod">
            <summary>
            The method to use for response callbacks. Valid options are POST and GET.
            Default is POST.
            </summary>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.#ctor">
            <summary>
            Hidden parameterless constructor.
            </summary>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.#ctor(System.String,System.String,System.String,System.Nullable{System.Int64},System.String,System.Nullable{FoneDynamics.Rest.V2.WebhookMethod},System.Boolean,System.String,System.Nullable{FoneDynamics.Rest.V2.WebhookMethod},System.String,System.String,System.String,System.String)">
            <summary>
            Constructs a new message resource.  This does not send the message.
            To send a message, use MessageResource.Send().
            </summary>
            <param name="to">The recipient of the SMS message in E164 format (including + prefix).</param>
            <param name="text">The content of the SMS message.</param>
            <param name="from">
            The sender ID. This can be a mobile phone number in E164 format (including + prefix),
            an alphanumeric string, or blank or null to send from a leased number.
            </param>
            <param name="schedule">
            The UTC time in seconds since Unix Epoch to send the message.
            This can be at most 14 days in the future. If the value is in the past, the request will
            fail, unless the value is less than 1 hour in the past, in which case the request will
            succeed and the message will be sent immediately.
            </param>
            <param name="webhookUri">The callback URI to invoke when a response is received.</param>
            <param name="webhookMethod">
            The method to use for response callbacks. Valid options are POST and GET. Default is POST.
            </param>
            <param name="deliveryReceipt">Whether to request a delivery receipt.</param>
            <param name="deliveryReceiptWebhookUri">
            The callback URI to invoke when a delivery receipt is received.
            Note that DeliveryReceipt must be set to true for this to be triggered.
            </param>
            <param name="deliveryReceiptWebhookMethod">
            The method to use for delivery receipt callbacks. Valid options are POST and GET.
            Default is POST.
            </param>
            <param name="forwardToSms">
            When a response is received, forward it to this number in E164 format.
            </param>
            <param name="forwardFromSms">
            When forwarding via SMS, send from this E164 formatted number or alphanumeric sender ID.
            By default the sender ID will be the sender ID of the responding party.
            </param>
            <param name="forwardToEmail">
            When a response is received, forward it over email to this email address.
            </param>
            <param name="forwardFromEmail">
            When forwarding via email, send from this email address.  By default this is a "no reply"
            email address.
            </param>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.Send(FoneDynamics.Rest.V2.MessageResource,System.String,FoneDynamics.FoneDynamicsClient)">
            <summary>
            Sends the specified message.
            </summary>
            <param name="message">The message to send constructed using the MessageResource constructor.</param>
            <param name="propertySid">
            The PropertySid of the property against which to send the message.
            If null, the default PropertySid will be used, unless it is undefined,
            in which case an exception will be thrown.
            </param>
            <param name="foneDynamicsClient">
            The FoneDynamicsClient instance to use.  If null, the default instance will be used.
            </param>
            <returns>The MessageResource that was sent, or an exception on failure.</returns>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.Send(System.String,System.String,System.String,System.Nullable{System.Int64},System.String,System.Nullable{FoneDynamics.Rest.V2.WebhookMethod},System.Boolean,System.String,System.Nullable{FoneDynamics.Rest.V2.WebhookMethod},System.String,System.String,System.String,System.String,System.String,FoneDynamics.FoneDynamicsClient)">
            <summary>
            Sends the specified message.
            </summary>
            <param name="to">The recipient of the SMS message in E164 format (including + prefix).</param>
            <param name="text">The content of the SMS message.</param>
            <param name="from">
            The sender ID. This can be a mobile phone number in E164 format (including + prefix),
            an alphanumeric string, or blank or null to send from a leased number.
            </param>
            <param name="schedule">
            The UTC time in seconds since Unix Epoch to send the message.
            This can be at most 14 days in the future. If the value is in the past, the request will
            fail, unless the value is less than 1 hour in the past, in which case the request will
            succeed and the message will be sent immediately.
            </param>
            <param name="webhookUri">The callback URI to invoke when a response is received.</param>
            <param name="webhookMethod">
            The method to use for response callbacks. Valid options are POST and GET. Default is POST.
            </param>
            <param name="deliveryReceipt">Whether to request a delivery receipt.</param>
            <param name="deliveryReceiptWebhookUri">
            The callback URI to invoke when a delivery receipt is received.
            Note that DeliveryReceipt must be set to true for this to be triggered.
            </param>
            <param name="deliveryReceiptWebhookMethod">
            The method to use for delivery receipt callbacks. Valid options are POST and GET.
            Default is POST.
            </param>
            <param name="forwardToSms">
            When a response is received, forward it to this number in E164 format.
            </param>
            <param name="forwardFromSms">
            When forwarding via SMS, send from this E164 formatted number or alphanumeric sender ID.
            By default the sender ID will be the sender ID of the responding party.
            </param>
            <param name="forwardToEmail">
            When a response is received, forward it over email to this email address.
            </param>
            <param name="forwardFromEmail">
            When forwarding via email, send from this email address.  By default this is a "no reply"
            email address.
            </param>
            <param name="propertySid">
            The PropertySid of the property against which to send the message.
            If null, the default PropertySid will be used, unless it is undefined,
            in which case an exception will be thrown.
            </param>
            <param name="foneDynamicsClient">
            The FoneDynamicsClient instance to use.  If null, the default instance will be used.
            </param>
            <returns>The MessageResource that was sent, or an exception on failure.</returns>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.Get(System.String,System.String,FoneDynamics.FoneDynamicsClient)">
            <summary>
            Gets a message against the account by its MessageSid.
            </summary>
            <param name="messageSid">The MessageSid of the message to retrieve.</param>
            <param name="propertySid">The PropertySid of the property associated with the the message.</param>
            <param name="foneDynamicsClient">
            The FoneDynamicsClient instance to use.  If null, the default instance will be used.
            </param>
            <returns>The MessageResource that was sent, or an exception on failure.</returns>
        </member>
        <member name="M:FoneDynamics.Rest.V2.MessageResource.Send(System.Collections.Generic.IEnumerable{FoneDynamics.Rest.V2.MessageResource},System.String,FoneDynamics.FoneDynamicsClient)">
            <summary>
            Sends multiple messages at once.  Individual messages can succeed or fail using this request.
            The details of successful and failed messages is available in the response list.
            </summary>
            <param name="messages">The messages to send constructed using the MessageResource constructor.</param>
            <param name="propertySid">
            The PropertySid of the property against which to send the message.
            If null, the default PropertySid will be used, unless it is undefined,
            in which case an exception will be thrown.
            </param>
            <param name="foneDynamicsClient">
            The FoneDynamicsClient instance to use.  If null, the default instance will be used.
            </param>
            <returns>A list of messages that were sent, including successful and failed messages.</returns>
        </member>
        <member name="T:FoneDynamics.Rest.V2.MessageResponse">
            <summary>
            The response to a Message request.
            </summary>
        </member>
        <member name="P:FoneDynamics.Rest.V2.MessageResponse.Message">
            <summary>
            The message that was sent.
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.MessageStatus">
            <summary>
            The status of a message.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageStatus.Processing">
            <summary>
            The message has been received by Fone Dynamics and is being processed.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageStatus.Submitted">
            <summary>
            The message has been submitted successfully to the SMS Center.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageStatus.Delivered">
            <summary>
            The message has been delivered successfully (if delivery receipts are activated).
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageStatus.Failed">
            <summary>
            The message has failed (submission to the SMS Center was not possible within a reasonable time limit).
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.MessageStatus.Received">
            <summary>
            The message has been received (for inbound messages).
            </summary>
        </member>
        <member name="T:FoneDynamics.Rest.V2.WebhookMethod">
            <summary>
            The method to use for webhooks.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.WebhookMethod.Post">
            <summary>
            Webhooks should use POST.
            </summary>
        </member>
        <member name="F:FoneDynamics.Rest.V2.WebhookMethod.Get">
            <summary>
            Webhooks should use GET.
            </summary>
        </member>
        <member name="T:FoneDynamics.Utility.Web">
            <summary>
            Functionality for encoding.
            </summary>
        </member>
        <member name="M:FoneDynamics.Utility.Web.UrlEncode(System.String)">
            <summary>
            Returns the URL encoded string.
            </summary>
        </member>
        <member name="M:FoneDynamics.Utility.Web.CompileQueryString(System.Collections.Generic.List{System.Collections.Generic.KeyValuePair{System.String,System.String}})">
            <summary>
            Returns a query string, or null if query is null or empty.
            </summary>
        </member>
        <member name="T:FoneDynamics.Utility.Json">
            <summary>
            Functionality for JSON serialisation.
            </summary>
        </member>
        <member name="T:FoneDynamics.Utility.Json.CustomResolver">
            <summary>
            Contract resolver to read/write non-public properties.
            </summary>
        </member>
        <member name="F:FoneDynamics.Utility.Json.CONTENT_TYPE">
            <summary>
            The content type for JSON text payloads: application/json
            </summary>
        </member>
        <member name="M:FoneDynamics.Utility.Json.Serialize(System.Object)">
            <summary>
            Serializes the specified object as JSON.
            </summary>
        </member>
        <member name="M:FoneDynamics.Utility.Json.Deserialize``1(System.String)">
            <summary>
            Deserializes the specified JSON object specified as a string.
            </summary>
        </member>
        <member name="M:FoneDynamics.Utility.Json.Deserialize``1(System.Byte[])">
            <summary>
            Deserializes the specified JSON object specified as a byte array.
            Assumes UTF8 encoding.
            </summary>
        </member>
    </members>
</doc>
