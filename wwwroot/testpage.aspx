﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testpage.aspx.cs" Inherits="testpage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="http://localhost:57356/admin/theme/bower_components/select2/css/select2.min.css" />
    <script src="http://localhost:57356/admin/theme/bower_components/jquery/js/jquery.min.js "></script>
    <script src="http://localhost:57356/admin/theme/assets/js/select2.js"></script>
    <script src="http://localhost:57356/admin/theme/assets/js/select2.js"></script>
    <link rel="stylesheet" type="text/css" href="http://localhost:57356/admin/theme/assets/css/bootstrap-datepicker.css" />
     <link rel="stylesheet" type="text/css" href="http://localhost:57356/admin/theme/assets/css/custom.css" />
    <script src="http://localhost:57356/admin/theme/assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
    <style type="text/css">

    </style>
    <script>
        $('.sandbox-container input').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form-group col-md-6">
            <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                Start Date</asp:Label>
            <div class="input-group sandbox-container col-sm-12">
                <div class="input-group-addon">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <asp:TextBox ID="txtSDate" runat="server" type="text" class="form-control">
                </asp:TextBox>

            </div>
        </div>
        <div class="form-group col-md-6">
            <asp:Label ID="Label24" runat="server" class="col-sm-2  control-label">
                                                End Date</asp:Label>
            <div class="input-group sandbox-container col-sm-12">
                <div class="input-group-addon">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <asp:TextBox ID="txtEDate" runat="server" type="text" class="form-control">
                </asp:TextBox>

            </div>
        </div>
        <div class="form-group col-md-6">
            <div class="input-group col-sm-12" style="display:none">
                <asp:Button Text="Fetch P_BS/GB Arise" ID="btnP_BSGB" runat="server" CssClass="btn btn-warning" OnClick="btnP_BSGB_Click" />
            </div>
            
            <div class="input-group col-sm-12">
                <asp:Button Text="Fetch GreenBot Arise" ID="btnAllGreenBotData" runat="server" CssClass="btn btn-warning" OnClick="btnAllGreenBotData_Click" />
            </div>
            <div class="input-group col-sm-12">
                <asp:Button Text="Fetch BridgeSelect" ID="btnAllBridgeSelect" runat="server" CssClass="btn btn-warning" OnClick="btnAllBridgeSelect_Click" />
            </div>
        </div>

        <div class="form-group col-md-6">
            <div class="input-group col-sm-6"> Role Name
                <asp:TextBox ID="txtRole" runat="server" class="form-control">
                </asp:TextBox>
            </div>
            
            <div class="input-group col-sm-6">
                <asp:Button Text="Create Role" ID="Button2" runat="server" CssClass="btn btn-primary" OnClick="Button2_Click" />
            </div>
        </div>
    </form>
</body>

</html>
