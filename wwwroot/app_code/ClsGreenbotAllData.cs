﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsGreenbotAllData
/// </summary>
public class ClsGreenbotAllData
{
    public ClsGreenbotAllData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static int SP_GreenbotAllData_BulkInsertUpdate(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_GreenbotAllData_BulkInsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dt";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int Bulk_InsertUpdate_tbl_BSAllSerialInverterSerialNo(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_tbl_BSAllSerialInverterSerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtSerialNo";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
}