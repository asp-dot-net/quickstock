﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsDashboard
/// </summary>
public class ClsDashboard
{
    public ClsDashboard()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable tblStockItemsLocation_Dashboard_GetData(string CompanyLocationID, string Category)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Dashboard_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Category";
        if (Category != string.Empty)
            param.Value = Category;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataSet SP_GetDataDashboard_StateWise(string Category)
    {
        string connectionString = SiteConfiguration.DbConnectionString;
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

        var comm = conn.CreateCommand();
        comm.CommandText = "SP_GetDataDashboard_StateWise";
        comm.CommandType = CommandType.StoredProcedure;

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Category";
        if (Category != string.Empty)
            param.Value = Category;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataSet result = new DataSet();
        try
        {
            System.Data.SqlClient.SqlDataAdapter dsa = new System.Data.SqlClient.SqlDataAdapter(comm);
            dsa.Fill(result);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
}