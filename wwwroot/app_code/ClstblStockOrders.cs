using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblStockOrders
{
    public string orderid;
    public string OrderNumber;
    public string StockCategoryID;
    public string StockItemID;
    public string OrderQuantity;
    public string DateOrdered;
    public string ExpectedDelivery;
    public string Delivered;
    public string StockFrom;
    public string Cancelled;
    public string OrderedBy;
    public string ReceivedBy;
    public string Notes;
    public string CompanyLocationID;
    public string CustomerID;
    public string BOLReceived;
    public string upsize_ts;
    public string ManualOrderNumber;
    public string ActualDelivery;

    public string CompanyLocation;
    public string Vendor;
    public string OrderedName;
    public string VendorInvoiceNo;
    public string DeliveryOrderStatus;
    public string StockContainer;
    public string ReceivedBy1;
    public string verifyStatus;
    public string ExcelUploaded;
    public string PurchaseCompanyId;
    public string ItemNameId;
    public string ItemNameValue;
    public string DeleveryId;
    public string Deleveryvalue;
    public string PurchaseCompanyName;
    public string DeliveryOrderStatusNew;
    public string StockFromNew;
    public string TransCharges;


    public string arrivaldate;
    public string arrivaldoc;
    public string transport_comp_Id;
    public string telex_Release_date;
    public string telex_Release_doc;
    public string supplier_invlice_no;
    public string total_pi_amt;
    public string transport_comp_jobno;
    public string transport_comp_service_invno;
    public string transport_comp_service_dist_invno;
    public string transport_comp_service_dist_doc;
    public string pckemailwhdate;
    public string storage_charge_amt;
    public string storage_charge_invdoc;
    public string storage_change_Reason;
    public string transportcompsentdate;
    public string extranotes;
    public string storage_charge_invno;

    public string paperworkarrival;
    public string TelexReleasedoc;
    public string TransportDistInvoice;
    public string storagechanrgeinv;
    public string pmtDueDate;
    public string arrivaldatedoc;
    public string StockOrderStatus;
    public string transport_comp_service_invoiceno;
    public string PaymentMethodID;

    public string transport_comp_service_invoice_Amount;
    public string transport_comp_service_dist_invno_Amount;
    public string VendorDiscount;
    public string GST_Type;
    public string Currency;
    public string OrderFor;
}

public struct SttblStockOrderItems
{
    public string StockOrderID;
    public string StockItemID;
    public string OrderQuantity;
    public string StockOrderItem;
    public string StockCode;
    public string StockLocation;
    public string Isverify;
    public string ItemDelivered;
}

public struct sttblStockSerialNo
{
    public string StockItemID;
    public string SerialNo;
    public string StockOrderID;
}
public class ClstblStockOrders
{
    public static SttblStockOrders tblStockOrders_SelectByStockOrderID(String StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockOrders details = new SttblStockOrders();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OrderNumber = dr["OrderNumber"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["Qty"].ToString();
            details.DateOrdered = dr["DateOrdered"].ToString();
            details.ExpectedDelivery = dr["ExpectedDelivery"].ToString();
            details.Delivered = dr["Delivered"].ToString();
            details.Cancelled = dr["Cancelled"].ToString();
            details.OrderedBy = dr["OrderedBy"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.BOLReceived = dr["BOLReceived"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ManualOrderNumber = dr["ManualOrderNumber"].ToString();
            details.ActualDelivery = dr["ActualDelivery"].ToString();

            details.CompanyLocation = dr["CompanyLocation"].ToString();
            details.Vendor = dr["Vendor"].ToString();
            details.OrderedName = dr["OrderedName"].ToString();
            details.VendorInvoiceNo = dr["VendorInvoiceNo"].ToString();
            details.DeliveryOrderStatus = dr["DeliveryOrderStatus"].ToString();
            details.StockContainer = dr["StockContainer"].ToString();
            details.ReceivedBy1 = dr["ReceivedBy1"].ToString();
            details.verifyStatus = dr["verifyStatus"].ToString();
            details.ExcelUploaded = dr["ExcelUploaded"].ToString();
            details.PurchaseCompanyId = dr["PurchaseCompanyId"].ToString();
            details.ItemNameId = dr["ItemNameId"].ToString();
            details.ItemNameValue = dr["ItemNameValue"].ToString();
            details.DeleveryId = dr["DeleveryId"].ToString();
            details.Deleveryvalue = dr["Deleveryvalue"].ToString();
            details.PurchaseCompanyName = dr["PurchaseCompanyName"].ToString();
            details.StockFrom = dr["StockFrom"].ToString();
            details.DeliveryOrderStatusNew = dr["DeliveryOrderStatusNew"].ToString();
            details.StockFromNew = dr["StockFromNew"].ToString();
            details.TransCharges = dr["TransCharges"].ToString();
            details.arrivaldate = dr["arrivaldate"].ToString();
            details.arrivaldoc = dr["arrivaldoc"].ToString();
            details.transport_comp_Id = dr["transport_comp_Id"].ToString();
            details.telex_Release_date = dr["telex_Release_date"].ToString();
            //details.telex_Release_doc = dr[""].ToString();
            details.supplier_invlice_no = dr["supplier_invlice_no"].ToString();
            details.total_pi_amt = dr["total_pi_amt"].ToString();
            details.transport_comp_jobno = dr["transport_comp_jobno"].ToString();
            details.transport_comp_service_invno = dr["transport_comp_service_invno"].ToString();
            details.transport_comp_service_dist_invno = dr["transport_comp_service_dist_invno"].ToString();
            details.transport_comp_service_dist_doc = dr["transport_comp_service_dist_doc"].ToString();
            details.pckemailwhdate = dr["pckemailwhdate"].ToString();
            details.storage_charge_amt = dr["storage_charge_amt"].ToString();
            details.storage_charge_invdoc = dr["storage_charge_invdoc"].ToString();
            details.storage_change_Reason = dr["storage_change_Reason"].ToString();
            details.transportcompsentdate = dr["transportcompsentdate"].ToString();
            details.extranotes = dr["extranotes"].ToString();
            details.storage_charge_invno = dr["storage_charge_invno"].ToString();
            details.arrivaldoc = dr["arrivaldoc"].ToString();
            details.TelexReleasedoc = dr["TelexReleasedoc"].ToString();
            details.TransportDistInvoice = dr["TransportDistInvoice"].ToString();
            details.storagechanrgeinv = dr["storagechanrgeinv"].ToString();
            details.pmtDueDate = dr["paymentDueDate"].ToString();
            details.arrivaldatedoc = dr["arrivaldatedoc"].ToString();
            details.StockOrderStatus = dr["StockOrderStatus"].ToString();
            details.transport_comp_service_invoiceno = dr["transport_comp_service_invoiceno"].ToString();
            details.PaymentMethodID = dr["PaymentMethodID"].ToString();
            details.transport_comp_service_invoice_Amount = dr["transport_comp_service_invoice_Amount"].ToString();
            details.transport_comp_service_dist_invno_Amount = dr["transport_comp_service_dist_invno_Amount"].ToString();
            details.VendorDiscount = dr["VendorDiscount"].ToString();
            details.GST_Type = dr["GST_Type"].ToString();
            details.Currency = dr["Currency"].ToString();
            details.OrderFor = dr["OrderFor"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblStockOrders_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectBySearch(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectBySearchNewQuickStock(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem, string DeliveryOrderStatus, string StockContainer, string PurchaseCompanyId, string ItemNameId, string DeleveryId, string StockOrderStatus, string PaymentMethod, string transport_comp_Id, string Partial, string GST_Type, string orderFor)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "tblStockOrders_SelectBySearchNewQuickStock";
        comm.CommandText = "tblStockOrders_SelectBySearchNewQuickStockNEW";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (DeliveryOrderStatus != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        if (StockContainer != string.Empty)
            param.Value = StockContainer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (PurchaseCompanyId != string.Empty)
            param.Value = PurchaseCompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameId";
        if (ItemNameId != string.Empty)
            param.Value = ItemNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleveryId";
        if (DeleveryId != string.Empty)
            param.Value = DeleveryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderstatus";
        if (StockOrderStatus != string.Empty)
            param.Value = StockOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMethod";
        if (PaymentMethod != string.Empty)
            param.Value = PaymentMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_Id";
        if (transport_comp_Id != string.Empty)
            param.Value = transport_comp_Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Partial";
        if (Partial != string.Empty)
            param.Value = Partial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GST_Type";
        if (GST_Type != string.Empty)
            param.Value = GST_Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@OrderFor";
        if (orderFor != string.Empty)
            param.Value = orderFor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockOrders_Insert(String DateOrdered, String OrderedBy, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ManualOrderNumber, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DateOrdered";
        param.Value = DateOrdered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderedBy";
        if (!string.IsNullOrEmpty(OrderedBy))
            param.Value = OrderedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        if (ManualOrderNumber != string.Empty)
            param.Value = ManualOrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockOrders_Update_OrderNumber(string StockOrderID, String OrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_OrderNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        param.Value = OrderNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblStockOrders_Update_Cancelled(string StockOrderID, String Cancelled)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Cancelled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Cancelled";
        param.Value = Cancelled;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblStockOrders_Update_Delivered(string StockOrderID, string ReceivedBy, string Delivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Delivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        if (ReceivedBy != string.Empty)
            param.Value = ReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_DeliveredOnly(string StockOrderID, string Delivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_DeliveredOnly";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        param.Value = Delivered;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_ExcelUploaded(string StockOrderID, string ExcelUploaded)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_ExcelUploaded";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExcelUploaded";
        param.Value = ExcelUploaded;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrderItems_Update_ItemDeliveredOnly(string StockOrderItemID, string ItemDelivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Update_ItemDeliveredOnly";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemDelivered";
        param.Value = ItemDelivered;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_ActualDelivery(string StockOrderID, string ActualDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_ActualDelivery";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualDelivery";
        if (ActualDelivery != string.Empty)
            param.Value = ActualDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update(string StockOrderID, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ManualOrderNumber, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        if (ManualOrderNumber != string.Empty)
            param.Value = ManualOrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_VendorInvoiceNo(string StockOrderID, String VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_DeliveryStatus(string StockOrderID, string DeliveryOrderStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_DeliveryStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (DeliveryOrderStatus != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@DeliveryOrderStatus";
        //param.Value = DeliveryOrderStatus;
        //param.DbType = DbType.Int32;
        //// param.Size = 100;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_DeliveryOrderStatusNew(string StockOrderID, string DeliveryOrderStatusNew)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_DeliveryOrderStatusNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatusNew";
        if (DeliveryOrderStatusNew != string.Empty)
            param.Value = DeliveryOrderStatusNew;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //param = comm.CreateParameter();
        //param.ParameterName = "@DeliveryOrderStatusNew";
        //param.Value = DeliveryOrderStatusNew;
        //param.DbType = DbType.Int32;
        //// param.Size = 100;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_PurchaseCompanyID(string StockOrderID, int PurchaseCompanyId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_PurchaseCompanyID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        param.Value = PurchaseCompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_ItemNameByID(string StockOrderID, int ItemNameID, string ItemNameValue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_ItemNameByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameID";
        param.Value = ItemNameID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameValue";
        if (ItemNameValue != string.Empty)
            param.Value = ItemNameValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_ItemName(string StockOrderID, int ItemNameId, string ItemNameValue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_ItemName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameId";
        param.Value = ItemNameId;
        param.DbType = DbType.Int32;
        // param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameValue";
        param.Value = ItemNameValue;
        param.DbType = DbType.String;
        // param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_DeleveryType(string StockOrderID, int DeleveryId, string Deleveryvalue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_DeleveryType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@DeleveryId";
        param.Value = DeleveryId;
        param.DbType = DbType.Int32;
        // param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Deleveryvalue";
        param.Value = Deleveryvalue;
        param.DbType = DbType.String;
        // param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_TransCharges(string StockOrderID, String TransCharges)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_TransCharges";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransCharges";
        if (TransCharges != string.Empty)
            param.Value = TransCharges;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_Update_StockContainer(string StockOrderID, string StockContainer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_StockContainer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        param.Value = StockContainer;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblStockOrders_Update_PurchaseCompanyByID(string StockOrderID, int PurchaseCompanyID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_PurchaseCompanyByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyID";
        param.Value = PurchaseCompanyID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblStockOrders_Exits_VendorInvoiceNo(string VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Exits_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static int tblStockOrders_ExistsByIdVendorInvoiceNo(string StockOrderID, string VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_ExistsByIdVendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStockOrders_Exists_VendorInvoiceNo(string VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Exists_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStockOrders_ExistsByIdManualOrderNo(string StockOrderID, string ManualOrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_ExistsByIdManualOrderNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        param.Value = ManualOrderNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStockOrders_Exists_ManualOrderNo(string ManualOrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Exists_ManualOrderNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        param.Value = ManualOrderNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStockOrders_ExistsByIdStockContainer(string StockOrderID, string StockContainer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_ExistsByIdStockContainer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        param.Value = StockContainer;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStockOrders_Exists_StockContainer(string StockContainer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Exists_StockContainer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        param.Value = StockContainer;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    /////////////////////////////////////////////////	tblStockOrderItems

    public static SttblStockOrderItems tblStockOrderItems_SelectByStockOrderItemID(String StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_SelectByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockOrderItems details = new SttblStockOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockOrderID = dr["StockOrderID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.StockOrderItem = dr["StockOrderItem"].ToString();
            details.StockCode = dr["StockCode"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();
            details.Isverify = dr["Isverify"].ToString();
            details.ItemDelivered = dr["ItemDelivered"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblStockOrderItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_Select_ByStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(string StockOrderID, string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockOrderItems_Insert(String StockOrderID, String StockItemID, String OrderQuantity, String Amount, String StockOrderItem, String StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItem";
        if (StockOrderItem != string.Empty)
            param.Value = StockOrderItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblStockOrderItems_Update(string StockOrderItemID, String StockOrderID, String StockItemID, String OrderQuantity, String StockOrderItem, String StockCode, String StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        param.Value = OrderQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItem";
        param.Value = StockOrderItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        param.Value = StockCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrderItems_Delete(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblStockOrderItems_DeleteStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_DeleteStockOrderID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblStockOrderItems_SelectQty(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_SelectQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockOrdersReport_Search(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location, string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrdersReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrdersReport_ByLocSearch(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrdersReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockOrders_getStockItemState(string StockItemID, String State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_getStockItemState";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    ///ORDER INSTALLED REPORT

    public static DataTable tblOrderInstalledReport_Search(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblOrderInstalledReport_ByLocSearch(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblOrderInstalledReport_ByInstalled(string CustomerID, string startdate, string enddate, string StockItemID, String Location, string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_ByInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockOrderItems_whole_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_whole_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SStockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblStockSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockSerialNo_InsertWithOrderID(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_InsertWithOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblStockSerialNo_InsertWithOrderID_ForPortal(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String StockOrderID, string IsActive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_InsertWithOrderID_ForPortal";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsActive";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockSerialNo_UpdateStockOrderItemID(string id, string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_UpdateStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        if (StockOrderItemID != string.Empty)
            param.Value = StockOrderItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_DeleteBYStockOrderItemID(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_DeleteBYStockOrderItemID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblStockSerialNoByProjNo_Insert(String ProjectNumber, String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByProjNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockSerialNoByInvNo_Insert(String InvoiceNo, String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByInvNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockDeductSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String ProjectNumber, String DeductedBy, string DeductedDate, string InventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedBy";
        if (DeductedBy != string.Empty)
            param.Value = DeductedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedDate";
        if (DeductedDate != string.Empty)
            param.Value = DeductedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        if (InventoryHistoryId != string.Empty)
            param.Value = InventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblStockOrderItems_QtySum(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_QtySum";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_QtySum_ByStockOrderItemID(string StockOrderID, string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_QtySum_ByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_SearchByPallet(string StockItemID, string StockLocationID, string Pallet)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_SearchByPallet";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNoByProjNo_SearchByProjNo(string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByProjNo_SearchByProjNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNoByInvNo_SearchByInvNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByInvNo_SearchByInvNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockDeductSerialNo_SelectBy_ProjectNumber_RFlag(string ProjectNumber, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_SelectBy_ProjectNumber_RFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockDeductSerialNo_UpdateRevert(string SerialNo, String Pallet, string ProjectNumber, string RevertDate, string RevertedBy, string InventoryHistoryId, string RevertInventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        param.Value = RevertDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertedBy";
        param.Value = RevertedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        param.Value = InventoryHistoryId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertInventoryHistoryId";
        param.Value = RevertInventoryHistoryId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblStockRevertSerialNo_Insert(String ProjectNumber, String SerialNo, String Pallet, string DeductInventoryHistoryId, string RevertInventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockRevertSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductInventoryHistoryId";
        if (DeductInventoryHistoryId != string.Empty)
            param.Value = DeductInventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertInventoryHistoryId";
        if (RevertInventoryHistoryId != string.Empty)
            param.Value = RevertInventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblStockSerialNoCount_ByStockOrderID(string StockOrderID, string StockCategoryID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoCount_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_Select_ByStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Select_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_Select_ByStockOrderItemID(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Select_ByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockSerialNo_Exist(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Exist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static sttblStockSerialNo tblStockSerialNo_Getdata_BySerialNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Getdata_BySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblStockSerialNo details = new sttblStockSerialNo();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.SerialNo = dr["SerialNo"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockOrderID = dr["StockOrderID"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblStockOrders_SelectBySearchExcel(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem, string DeliveryOrderStatus, string StockContainer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearchExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (DeliveryOrderStatus != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        if (StockContainer != string.Empty)
            param.Value = StockContainer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockOrders_Update_userid(string id, string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_userid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = UserId;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblStockOrders_update_NewFields(string ArrivalDate, string transportcompId, string telexreleasedate, string supplierinvoiceno, string totalpi_amt, string transportcompjobno,
        string transport_comp_service_invno, string transport_comp_service_dist_invno,
        string pckemailwhdate, string storage_charge_amt,
        string storage_change_Reason, string extranotes,
        string Notes, string TransCompSentdate, string storage_charge_invno
 )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_update_NewFields";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@arrivaldate";
        if (ArrivalDate != string.Empty)
            param.Value = ArrivalDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_Id";
        if (transportcompId != string.Empty)
            param.Value = transportcompId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@telex_Release_date";
        if (telexreleasedate != string.Empty)
            param.Value = telexreleasedate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@supplier_invlice_no";
        if (supplierinvoiceno != string.Empty)
            param.Value = supplierinvoiceno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@total_pi_amt";
        if (totalpi_amt != string.Empty)
            param.Value = totalpi_amt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_jobno";
        if (transportcompjobno != string.Empty)
            param.Value = transportcompjobno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_invno";
        if (transport_comp_service_invno != string.Empty)
            param.Value = transport_comp_service_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_dist_invno";
        if (transport_comp_service_dist_invno != string.Empty)
            param.Value = transport_comp_service_dist_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@pckemailwhdate";
        if (pckemailwhdate != string.Empty)
            param.Value = pckemailwhdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_charge_amt";
        if (storage_charge_amt != string.Empty)
            param.Value = storage_charge_amt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@extranotes";
        if (extranotes != string.Empty)
            param.Value = extranotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_charge_invno";
        if (storage_charge_invno != string.Empty)
            param.Value = storage_charge_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transportcompsentdate";
        if (TransCompSentdate != string.Empty)
            param.Value = TransCompSentdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_change_Reason";
        if (storage_change_Reason != string.Empty)
            param.Value = storage_change_Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;
    }
    public static bool tblStockOrders_TelexReleasedoc(string ProjectID, string telexdoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_TelexReleasedoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TelexReleasedoc";
        if (telexdoc != string.Empty)
            param.Value = telexdoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_UpdateArriveDoc(string ProjectID, string arrivedoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdateArriveDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@arrivaldatedoc";
        if (arrivedoc != string.Empty)
            param.Value = arrivedoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_TransportDistInvoice(string ProjectID, string TransDistInvoice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_TransportDistInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@arrivaldatedoc";
        if (TransDistInvoice != string.Empty)
            param.Value = TransDistInvoice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_storagechanrgeinv(string ProjectID, string storagechargeinv)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_storagechanrgeinv";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@arrivaldatedoc";
        if (storagechargeinv != string.Empty)
            param.Value = storagechargeinv;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_updat_NewFields(string orederid,
        string ArrivalDate, string transportcompId,
        string telexreleasedate, string supplierinvoiceno,
        string totalpi_amt, string transportcompjobno,
        string transport_comp_service_invno, string transport_comp_service_dist_invno,
        string pckemailwhdate, string storage_charge_amt,
        string storage_change_Reason, string extranotes,
        string Notes, string TransCompSentdate, string storage_charge_invno
 )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_updat_NewFields";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@arrivaldate";
        if (ArrivalDate != string.Empty)
            param.Value = ArrivalDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        if (orederid != string.Empty)
            param.Value = orederid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_Id";
        if (transportcompId != string.Empty)
            param.Value = transportcompId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@telex_Release_date";
        if (telexreleasedate != string.Empty)
            param.Value = telexreleasedate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@supplier_invlice_no";
        if (supplierinvoiceno != string.Empty)
            param.Value = supplierinvoiceno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@total_pi_amt";
        if (totalpi_amt != string.Empty)
            param.Value = totalpi_amt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_jobno";
        if (transportcompjobno != string.Empty)
            param.Value = transportcompjobno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_invno";
        if (transport_comp_service_invno != string.Empty)
            param.Value = transport_comp_service_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_dist_invno";
        if (transport_comp_service_dist_invno != string.Empty)
            param.Value = transport_comp_service_dist_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@pckemailwhdate";
        if (pckemailwhdate != string.Empty)
            param.Value = pckemailwhdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_charge_amt";
        if (storage_charge_amt != string.Empty)
            param.Value = storage_charge_amt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@extranotes";
        if (extranotes != string.Empty)
            param.Value = extranotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_charge_invno";
        if (storage_charge_invno != string.Empty)
            param.Value = storage_charge_invno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transportcompsentdate";
        if (TransCompSentdate != string.Empty)
            param.Value = TransCompSentdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@storage_change_Reason";
        if (storage_change_Reason != string.Empty)
            param.Value = storage_change_Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 20;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static int Tblstockorder_PaymentDetail_Insert(string OrderNumber, string depositDate, string usdRate, string depositamtusd, string depositamtAud, string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrdrno";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@deporitDate";
        if (depositDate != string.Empty)
            param.Value = depositDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        // param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@usdRate";
        if (usdRate != string.Empty)
            param.Value = usdRate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@depositamtUsd";
        if (depositamtusd != string.Empty)
            param.Value = depositamtusd;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@depositamtAsd";
        if (depositamtAud != string.Empty)
            param.Value = depositamtAud;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        //int id = DataAccess.ExecuteNonQuery(comm);
        string id = DataAccess.ExecuteScalar(comm);
        return Convert.ToInt32(id);
    }

    public static DataTable Tblstockorder_PaymentDetail_GetData(string PmtDetailsId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stock_payment_DetailsId";
        param.Value = PmtDetailsId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable Tblstockorder_PaymentDetail_GetDataByStockOrderId(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_GetDataByStockOrderId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrdrno";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static bool Tblstockorder_PaymentDetail_DeleteByStockOrderId(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_DeleteByStockOrderId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrdrno";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static bool Tblstockorder_PaymentDetail_Update(string pmtid, string usdrate, string AudAmount, string usdAmount, string Depodate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_Update";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@pmtDetailId";
        //param.Value = ;
        if (pmtid != string.Empty)
            param.Value = pmtid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@usdRate";
        //param.Value = ;
        if (usdrate != string.Empty)
            param.Value = usdrate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@depositamtUsd";
        //param.Value = ;
        if (usdAmount != string.Empty)
            param.Value = usdAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@depositamtAsd";
        if (AudAmount != string.Empty)
            param.Value = AudAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@deporitDate";
        if (Depodate != string.Empty)
            param.Value = Depodate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tblStockOrders_UpdatepmtDueDate(string ProjectID, string pmtduedates)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdatepmtDueDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paymentDueDate ";
        if (pmtduedates != string.Empty)
            param.Value = pmtduedates;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool Tblstockorder_PaymentDetail_updatepaymentNotes(string ProjectID, string pmtnotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_updatepaymentNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stock_payment_DetailsId";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paymentNotes";
        if (pmtnotes != string.Empty)
            param.Value = pmtnotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static int tblStockOrders_ExistsByOrderNo(string orderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_ExistsByOrderNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderNumber";
        if (orderNumber != string.Empty)
            param.Value = orderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static SttblStockOrders tblStockOrders_SelectByStockOrderNumber(String StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectByStockOrderNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderNumber";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockOrders details = new SttblStockOrders();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OrderNumber = dr["OrderNumber"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["Qty"].ToString();
            details.DateOrdered = dr["DateOrdered"].ToString();
            details.ExpectedDelivery = dr["ExpectedDelivery"].ToString();
            details.Delivered = dr["Delivered"].ToString();
            details.Cancelled = dr["Cancelled"].ToString();
            details.OrderedBy = dr["OrderedBy"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.BOLReceived = dr["BOLReceived"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ManualOrderNumber = dr["ManualOrderNumber"].ToString();
            details.ActualDelivery = dr["ActualDelivery"].ToString();

            details.CompanyLocation = dr["CompanyLocation"].ToString();
            details.Vendor = dr["Vendor"].ToString();
            details.OrderedName = dr["OrderedName"].ToString();
            details.VendorInvoiceNo = dr["VendorInvoiceNo"].ToString();
            details.DeliveryOrderStatus = dr["DeliveryOrderStatus"].ToString();
            details.StockContainer = dr["StockContainer"].ToString();
            details.ReceivedBy1 = dr["ReceivedBy1"].ToString();
            details.verifyStatus = dr["verifyStatus"].ToString();
            details.ExcelUploaded = dr["ExcelUploaded"].ToString();
            details.PurchaseCompanyId = dr["PurchaseCompanyId"].ToString();
            details.ItemNameId = dr["ItemNameId"].ToString();
            details.ItemNameValue = dr["ItemNameValue"].ToString();
            details.DeleveryId = dr["DeleveryId"].ToString();
            details.Deleveryvalue = dr["Deleveryvalue"].ToString();
            details.PurchaseCompanyName = dr["PurchaseCompanyName"].ToString();
            details.StockFrom = dr["StockFrom"].ToString();
            details.DeliveryOrderStatusNew = dr["DeliveryOrderStatusNew"].ToString();
            details.StockFromNew = dr["StockFromNew"].ToString();
            details.TransCharges = dr["TransCharges"].ToString();
            details.arrivaldate = dr["arrivaldate"].ToString();
            details.arrivaldoc = dr["arrivaldoc"].ToString();
            details.transport_comp_Id = dr["transport_comp_Id"].ToString();
            //details.telex_Release_date = dr[""].ToString();
            //details.telex_Release_doc = dr[""].ToString();
            details.supplier_invlice_no = dr["supplier_invlice_no"].ToString();
            details.total_pi_amt = dr["total_pi_amt"].ToString();
            details.transport_comp_jobno = dr["transport_comp_jobno"].ToString();
            details.transport_comp_service_invno = dr["transport_comp_service_invno"].ToString();
            details.transport_comp_service_dist_invno = dr["transport_comp_service_dist_invno"].ToString();
            details.transport_comp_service_dist_doc = dr["transport_comp_service_dist_doc"].ToString();
            details.pckemailwhdate = dr["pckemailwhdate"].ToString();
            details.storage_charge_amt = dr["storage_charge_amt"].ToString();
            details.storage_charge_invdoc = dr["storage_charge_invdoc"].ToString();
            details.storage_change_Reason = dr["storage_change_Reason"].ToString();
            details.transportcompsentdate = dr["transportcompsentdate"].ToString();
            details.extranotes = dr["extranotes"].ToString();
            details.storage_charge_invno = dr["storage_charge_invno"].ToString();
            details.arrivaldoc = dr["arrivaldoc"].ToString();
            details.TelexReleasedoc = dr["telex_Release_doc"].ToString();
            details.TransportDistInvoice = dr["TransportDistInvoice"].ToString();
            details.storagechanrgeinv = dr["storagechanrgeinv"].ToString();
            details.pmtDueDate = dr["paymentDueDate"].ToString();
            details.orderid = dr["stockorderid"].ToString();
            details.StockOrderStatus = dr["StockOrderStatus"].ToString();
            details.PaymentMethodID = dr["PaymentMethodID"].ToString();
        }
        // return structure details
        return details;
    }

    public static bool tblStockOrders_UpateDateForContainer(string StockOrderId, string BOLReceived, string ExpectedDelivery, string @telex_Release_date, string arrivaldate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpateDateForContainer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderId";
        param.Value = @StockOrderId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@telex_Release_date";
        if (telex_Release_date != string.Empty)
            param.Value = telex_Release_date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@arrivaldate";
        if (arrivaldate != string.Empty)
            param.Value = arrivaldate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_SelectBySearchNewQuickStock_PaymentTracker(string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string stockitem, string hideshow, string orderStatus, string PmtStatus,string DelStatus, string PaymentMethod, string PaymentYesNo, string PurchaseCompanyId, string DeliveryOrderStatus, string Deliverytype, string GST_Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearchNewQuickStock_PaymentContainer";

        DbParameter param = comm.CreateParameter();
        //param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@hideShow";
        if (hideshow != string.Empty)
            param.Value = hideshow;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderstatus";
        if (orderStatus != string.Empty)
            param.Value = orderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameId";
        if (PmtStatus != string.Empty)
            param.Value = PmtStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleveryId";
        if (DelStatus != string.Empty)
            param.Value = DelStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMethod";
        if (PaymentMethod != string.Empty)
            param.Value = PaymentMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentYesNo";
        if (PaymentYesNo != string.Empty)
            param.Value = PaymentYesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (PurchaseCompanyId != string.Empty)
            param.Value = PurchaseCompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (DeliveryOrderStatus != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Deliverytype";
        if (Deliverytype != string.Empty)
            param.Value = Deliverytype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GST_Type";
        if (GST_Type != string.Empty)
            param.Value = GST_Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_selectforContainerTracker(string OrderNumber, string DateType, string StartDate, string EnDdate, string StockItem, string Location, string Vendor,string OrderStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReportForbrokenReport";

        comm.CommandText = "tblStockOrders_selectforContainerTracker";


        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //  param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EnDdate != string.Empty)
            param.Value = EnDdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //  param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (Vendor != string.Empty)
            param.Value = Vendor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderstatus";
        if (OrderStatus != string.Empty)
            param.Value = OrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_selectforContainerTrackerNew(string OrderNumber, string DateType, string StartDate, string EnDdate, string StockItem, string Location, string Vendor, string OrderStatus, string Val, string StockSize, string StockManufacturer, string transport_comp_Id, int OrderBy, string ContainerNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReportForbrokenReport";

        comm.CommandText = "tblStockOrders_selectforContainerTrackerNew";
        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //  param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EnDdate != string.Empty)
            param.Value = EnDdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //  param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (Vendor != string.Empty)
            param.Value = Vendor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderstatus";
        if (OrderStatus != string.Empty)
            param.Value = OrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@val";
        if (Val != string.Empty)
            param.Value = Val;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        if (StockManufacturer != string.Empty)
            param.Value = StockManufacturer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_Id";
        if (transport_comp_Id != string.Empty)
            param.Value = transport_comp_Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderBy";
        param.Value = OrderBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContainerNo";
        if (ContainerNo != string.Empty)
            param.Value = ContainerNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockOrderItems_updateExpAndMondelNo(string orditemid, string ExpDate, string Modelno)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_updateExpAndMondelNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = orditemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockExpiryDate";
        if (ExpDate != null && ExpDate != string.Empty)
            param.Value = ExpDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModelNo";
        if (Modelno != string.Empty)
            param.Value = Modelno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_UpdatepmtOrderedDate(string ProjectID, string orderDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdatepmtOrderedDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateOrdered";
        if (orderDate != string.Empty)
            param.Value = orderDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_UpdateOrderedBy(string ProjectID, string OrderBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdateOrderedBy";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderedBy";
        if (OrderBy != string.Empty)
            param.Value = OrderBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_UpdateStockOrderStatus(string ProjectID, string StockOrderStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdateStockOrderStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderStatus";
        if (StockOrderStatus != string.Empty)
            param.Value = StockOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_UpdateCompanyLocationId(string ProjectID, string Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_UpdateCompanyLocationId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrderItems_Update_Delivered(string orderid, string del)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Update_Delivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = orderid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (del != string.Empty)
            param.Value = del;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders__UpdateTransportTaxInvoice(string ProjectID, string arrivedoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders__UpdateTransportTaxInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@arrivaldatedoc";
        if (arrivedoc != string.Empty)
            param.Value = arrivedoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblstockorders_updatepaymentStatus(string orderid, string PmtId, string PmtValue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblstockorders_updatepaymentStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = orderid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameID";
        if (PmtId != null && PmtId != string.Empty)
            param.Value = PmtId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameValue";
        if (PmtValue != string.Empty)
            param.Value = PmtValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblstockorders_UpdateTelexFlag(string ProjectID, string IsTelexUpload)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblstockorders_UpdateTelexFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockOrderId";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTelexUpload";
        if (IsTelexUpload != string.Empty)
            param.Value = IsTelexUpload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
       // param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblstockorders_Upload_IspaperUpload(string ProjectID, string IsTelexUpload)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblstockorders_Upload_IspaperUpload";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockOrderId";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTelexUpload";
        if (IsTelexUpload != string.Empty)
            param.Value = IsTelexUpload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_PurchaseCompany_GetAll()
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReportForbrokenReport";

        comm.CommandText = "tbl_PurchaseCompany_GetAll";
        DbParameter param = comm.CreateParameter();
        
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockOrders_Update_PaymentMethod(string StockOrderID, string PaymentMethod)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_PaymentMethod";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMethod";
        if (PaymentMethod != string.Empty)
            param.Value = PaymentMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_SelectPaymentMethod_ByStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectPaymentMethod_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_Transport_CompMaster_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Transport_CompMaster_GetData";

       
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPaymentMethodtype_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentMethodtype_GetData";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool Tblstockorder_PaymentDetail_UpdatePaymentMethodTypeByStockOrderId(string stock_payment_DetailsId, string PaymentMethodType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_UpdatePaymentMethodTypeByStockOrderId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stock_payment_DetailsId";
        param.Value = stock_payment_DetailsId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMethodType";
        if (PaymentMethodType != string.Empty)
            param.Value = PaymentMethodType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_TargetDate(string StockOrderID, string TargetDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_TargetDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TargetDate";
        if (TargetDate != string.Empty)
            param.Value = @TargetDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_SelectTargetDate_ByStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectTargetDate_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStckOrderItems_Get_StockItembyOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStckOrderItems_Get_StockItembyOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockOrders__UpdateTransportTaxInvoiceNo(string StockOrderID, string TransportTaxInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders__UpdateTransportTaxInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockorderid";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTaxInvoiceNo";
        if (TransportTaxInvoiceNo != string.Empty)
            param.Value = TransportTaxInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_GetData(string OrderNumber, string stockitem, string CompanyLocationID, string StockOrderStatus, string PurchaseCompanyId, string CustomerID, string Delivered, string DateType, string StartDate, string EndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderStatus";
        if (StockOrderStatus != string.Empty)
            param.Value = StockOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (PurchaseCompanyId != string.Empty)
            param.Value = PurchaseCompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_OtherQtyLog_ByStockOrderID(string StockOrderID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_OtherQtyLog_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockOrder_UpdateSubmit(string StockOrderID, string ActualDelivery, string ReceivedBy1, string Reason)
    {   
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrder_UpdateSubmit";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualDelivery";
        if (ActualDelivery != string.Empty)
            param.Value = ActualDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy1";
        if (ReceivedBy1 != string.Empty)
            param.Value = ReceivedBy1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_Inv_InvDist_Amount(string StockOrderID, string transport_comp_service_invoice_Amount, string transport_comp_service_dist_invno_Amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Inv_InvDist_Amount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_invoice_Amount";
        if (transport_comp_service_invoice_Amount != string.Empty)
            param.Value = transport_comp_service_invoice_Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_service_dist_invno_Amount";
        if (transport_comp_service_dist_invno_Amount != string.Empty)
            param.Value = transport_comp_service_dist_invno_Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_Get_Qty_UploadQty(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Get_Qty_UploadQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool SP_UpdateLocation_MH_SSN(string ID, string LocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_UpdateLocation_MH_SSN";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrderItems_UpdateUnitPrice(string StockOrderItemID, string UnitPrice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_UpdateUnitPrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        if (StockOrderItemID != string.Empty)
            param.Value = StockOrderItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UnitPrice";
        if (UnitPrice != string.Empty)
            param.Value = UnitPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrders_SelectBySearchNewQuickStockNewExcelDate(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem, string DeliveryOrderStatus, string StockContainer, string PurchaseCompanyId, string ItemNameId, string DeleveryId, string StockOrderStatus, string PaymentMethod, string transport_comp_Id, string Partial)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearchNewQuickStockNewExcelDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (DeliveryOrderStatus != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        if (StockContainer != string.Empty)
            param.Value = StockContainer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        if (PurchaseCompanyId != string.Empty)
            param.Value = PurchaseCompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemNameId";
        if (ItemNameId != string.Empty)
            param.Value = ItemNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleveryId";
        if (DeleveryId != string.Empty)
            param.Value = DeleveryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockorderstatus";
        if (StockOrderStatus != string.Empty)
            param.Value = StockOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMethod";
        if (PaymentMethod != string.Empty)
            param.Value = PaymentMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@transport_comp_Id";
        if (transport_comp_Id != string.Empty)
            param.Value = transport_comp_Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Partial";
        if (Partial != string.Empty)
            param.Value = Partial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockOrders_Update_Discount(string StockOrderID, string VendorDiscount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Discount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VendorDiscount";
        if (VendorDiscount != string.Empty)
            param.Value = VendorDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool Tblstockorder_PaymentDetail_Update_GST_Type(string stock_payment_DetailsId, string GST_Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Tblstockorder_PaymentDetail_Update_GST_Type";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stock_payment_DetailsId";
        param.Value = stock_payment_DetailsId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GST_Type";
        if (GST_Type != string.Empty)
            param.Value = GST_Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_GST_Type(string StockOrderID, string GST_Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_GST_Type";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GST_Type";
        if (GST_Type != string.Empty)
            param.Value = GST_Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_Currency(string StockOrderID, string Currency)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Currency";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Currency";
        if (Currency != string.Empty)
            param.Value = Currency;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int USP_tblStockItemsLocation_UpdateMinQtyByExcel(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_tblStockItemsLocation_UpdateMinQtyByExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@tblStockItemsLocation";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = -1;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static bool Update_tblStockOrders_OrderFor(string stockOrderId, string orderFor)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblStockOrders_OrderFor";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockOrderId";
        if (stockOrderId != string.Empty)
            param.Value = stockOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@orderFor";
        if (orderFor != string.Empty)
            param.Value = orderFor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable SP_GetSerialNo_ByOrderID(string stockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_GetSerialNo_ByOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (stockOrderID != string.Empty)
            param.Value = stockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}