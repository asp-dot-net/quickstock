﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsWholesaleService
/// </summary>
public class ClsWholesaleService
{
    public struct stWholesaleService
    {
        public string TicketNo;
        public string InvoiceNo;
        public string GB_BS_Formbay_Id;
        public string ServiceCompanyTicketNo;
        public string ContactName;
        public string Address;
        public string Email;
        public string PhoneNo;
        public string JobStatus;
        public string CompanyName;
        public string ShipmentTrackingNo;
        public string ServiceChargeInvoiceValue;
        public string InvNoToClaim;
        public string CalimedAmount;
        public string FaultyPickUpStatus;
        public string CreatedBy;
        public string CreatedOn;
    }

    public static stWholesaleService WholesaleService_SelectByTicketNo(string ticketNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleService_SelectByTicketNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        param.Value = ticketNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        stWholesaleService details = new stWholesaleService();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.TicketNo = dr["TicketNo"].ToString();
            details.InvoiceNo = dr["InvoiceNo"].ToString();
            details.GB_BS_Formbay_Id = dr["GB_BS_Formbay_Id"].ToString();
            details.ServiceCompanyTicketNo = dr["ServiceCompanyTicketNo"].ToString();
            details.ContactName = dr["ContactName"].ToString();
            details.Address = dr["Address"].ToString();
            details.Email = dr["Email"].ToString();
            details.PhoneNo = dr["PhoneNo"].ToString();
            details.JobStatus = dr["JobStatus"].ToString();
            details.CompanyName = dr["CompanyName"].ToString();
            details.ShipmentTrackingNo = dr["ShipmentTrackingNo"].ToString();
            details.ServiceChargeInvoiceValue = dr["ServiceChargeInvoiceValue"].ToString();
            details.InvNoToClaim = dr["InvNoToClaim"].ToString();
            details.CalimedAmount = dr["CalimedAmount"].ToString();
            details.FaultyPickUpStatus = dr["FaultyPickUpStatus"].ToString();
            details.CreatedOn = dr["CreatedOn"].ToString();
            details.CreatedBy = dr["CreatedBy"].ToString();
        }
        // return structure details
        return details;
    }

    public static DataTable WholesaleService_Select(string ticketNo, string invoiceNo, string companyName, string jobStatus, string shipmentTrackingNo, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleService_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyName";
        if (companyName != string.Empty)
            param.Value = companyName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatus";
        if (jobStatus != string.Empty)
            param.Value = jobStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ShipmentTrackingNo";
        if (shipmentTrackingNo != string.Empty)
            param.Value = shipmentTrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int WholesaleService_Insert(string createdOn, string createdBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleService_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch
        {
            // log errors if any
        }
        return id;
    }

    public static bool WholesaleService_Update(string ticketNo, string invoiceNo, string bsgbId, string serviceCompanyTicketNo, string contactName, string address, string email, string phoneNo, string jobStatus, string customerId, string shipmentTrackingNo, string serviceChargeInvoiceValue, string invNoToClaim, string calimedAmount, string faultyPickUpStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleService_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GB_BS_Formbay_Id";
        if (bsgbId != string.Empty)
            param.Value = bsgbId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceCompanyTicketNo";
        if (serviceCompanyTicketNo != string.Empty)
            param.Value = serviceCompanyTicketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactName";
        if (contactName != string.Empty)
            param.Value = contactName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Address";
        if (address != string.Empty)
            param.Value = address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (email != string.Empty)
            param.Value = email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PhoneNo";
        if (phoneNo != string.Empty)
            param.Value = phoneNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatus";
        if (jobStatus != string.Empty)
            param.Value = jobStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyName";
        if (customerId != string.Empty)
            param.Value = customerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ShipmentTrackingNo";
        if (shipmentTrackingNo != string.Empty)
            param.Value = shipmentTrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceChargeInvoiceValue";
        if (serviceChargeInvoiceValue != string.Empty)
            param.Value = serviceChargeInvoiceValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNoToClaim";
        if (invNoToClaim != string.Empty)
            param.Value = invNoToClaim;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CalimedAmount";
        if (calimedAmount != string.Empty)
            param.Value = calimedAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultyPickUpStatus";
        if (faultyPickUpStatus != string.Empty)
            param.Value = faultyPickUpStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool WholesaleService_Delete(string ticketNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleService_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable WholesaleServiceItems_SelectByTicketNo(string ticketNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceItems_SelectByTicketNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int WholesaleServiceItems_Insert(string ticketNo, string stockItem, string stockNotes, string oldSerialNo, string newSerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (stockItem != string.Empty)
            param.Value = stockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockNotes";
        if (stockNotes != string.Empty)
            param.Value = stockNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldSerialNo";
        if (oldSerialNo != string.Empty)
            param.Value = oldSerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewSerialNo";
        if (newSerialNo != string.Empty)
            param.Value = newSerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch
        {
            // log errors if any
        }
        return id;
    }

    public static bool WholesaleServiceItems_Delete(string ticketNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceItems_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable WholesaleServiceNotes_SelectByTicketNo(string ticketNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceNotes_SelectByTicketNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int WholesaleServiceNotes_Insert(string createdOn, string createdBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceNotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch
        {
            // log errors if any
        }
        return id;
    }

    public static bool WholesaleServiceNotes_Update(string id, string ticketNo, string notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceNotes_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@TicketNo";
        if (ticketNo != string.Empty)
            param.Value = ticketNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (notes != string.Empty)
            param.Value = notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool WholesaleServiceNotes_DeleteById(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "WholesaleServiceNotes_DeleteById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}