﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;

public class Reports
{
    public Reports()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    public static DataTable tblEmployees_Select_TeamWise(string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Select_TeamWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tblProjects_Count_Online(string SalesTeamID, string StreetState, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Count_Online";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectByCategory(string StockCategoryID, string StockItem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByCategory";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startdate";
        //if (startdate != string.Empty)
        //    param.Value = startdate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@enddate";
        //if (enddate != string.Empty)
        //    param.Value = enddate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        // param = comm.CreateParameter();
        // param.ParameterName = "@LocationID";
        // if (LocationID != string.Empty)
        //     param.Value = LocationID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);




        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_Count(string StockCategoryID, string StockItemID, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Count";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_Count_Stock(string StockItemID, string InstallState, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Count_Stock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblExtraStock_SelectStockList(string InstallerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_SelectStockList";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_PanelStock()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_PanelStock";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable aspnet_Users_SelectCustInst(string alpha, string rolename)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnet_Users_SelectCustInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@rolename";
        if (rolename != string.Empty)
            param.Value = rolename;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_ActiveReport(string userid, string startdate, string enddate, string userid1, String SalesTeam, String Employee)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_ActiveReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeam";
        if (SalesTeam != string.Empty)
            param.Value = SalesTeam;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Employee";
        if (Employee != string.Empty)
            param.Value = Employee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //HttpContext.Current.Response.Write( userid+"=="+  startdate + "==" + enddate + "==" + userid1 + "==" + SalesTeam + "==" + Employee);
        //HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_InverterPanelReport(string type, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InverterPanelReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startdate";
        //if (startdate != string.Empty)
        //    param.Value = startdate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_ActiveReportCount(string userid, string startdate, string enddate, string userid1, String SalesTeam, String Employee)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_ActiveReportCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeam";
        if (SalesTeam != string.Empty)
            param.Value = SalesTeam;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Employee";
        if (Employee != string.Empty)
            param.Value = Employee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_StockReportCount(string StockCategoryID, string StockItem, string startdate, string enddate, string LocationID, string hidePanel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_StockReportCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@hidePanel";
        if (hidePanel != string.Empty)
            param.Value = hidePanel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoTransfIDWholeID(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoTransfIDWholeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable SerialNumberWiseReport_ByProjNoTransfIDWholeID_SMProject(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoTransfIDWholeID_SMProject";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }



    public static DataTable SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedOnly(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedSolarMinerOnly(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        // comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly";
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnlySolarMiner";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedOnly_new(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly_new";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedOnly_SM(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }



    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedOnly_newSolarMiner(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly_newForSolarMiner";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }





    public static DataTable ReconciliationReport_QuickStock(string StockOrderID, string ProjectNumber, string WholesaleInvoiceNo, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ReconciliationReport_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleInvoiceNo";
        if (WholesaleInvoiceNo != string.Empty)
            param.Value = WholesaleInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NewReconciliationReport_QuickStock(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "NewReconciliationReport_QuickStock";
        comm.CommandText = "ReconciliationReport_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NewReconciliationReport_QuickStockWholesale(string InvoiceNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string CustomerID, string StatusID, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "NewReconciliationReport_QuickStockWholesale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_QuickStock(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate, string IsVerifyIN, string PickListwise, string ProjectStatus, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_QuickStock";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerifyIN";
        if (IsVerifyIN != string.Empty)
            param.Value = IsVerifyIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListwise";
        if (PickListwise != string.Empty)
            param.Value = PickListwise;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_QuickStockShowAll(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string Projectwise, string IsDifferece, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_QuickStockShowAll";
        

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListwise";
        if (Projectwise != string.Empty)
            param.Value = Projectwise;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifferece != string.Empty)
            param.Value = IsDifferece;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_QuickStockWholesale(string InvoiceNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string CustomerID, string datetype, string startdate, string enddate, string IsVerifyIN, string JobStatusId, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_QuickStockWholesale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerifyIN";
        if (IsVerifyIN != string.Empty)
            param.Value = IsVerifyIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (JobStatusId != string.Empty)
            param.Value = JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_QuickStockWholesaleShowAll(string InvoiceNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string CustomerID, string StatusID, string datetype, string startdate, string enddate, string Isverify, string IsDifference, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_QuickStockWholesaleShowAll";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockwiseReport_QuickStock(string stockitemModel, string StockCategoryID, string CompanyLocationID, string Active, string Type, string datetype, string startdate, string enddate, string StockModel, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockwiseReport_QuickStock";
        //comm.CommandText = "StockwiseReport_QuickStockCheck";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockPredicationReport_QuickStock(string stockitemModel, string StockCategoryID, string CompanyLocationID, string Active, string SalesTag, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockPredicationReport_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }



    public static DataTable StockReceivedReport_QuickStock(string stockitemModel,
        string StockCategoryID, string CompanyLocationID,
        string Active, string SalesTag,
        string datetype, string startdate,
        string enddate, string Type, string CompId, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "StockReceivedReport_QuickStock";
        comm.CommandText = "StockReceivedReport_QuickStock_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompId != string.Empty)
            param.Value = CompId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockReceivedReport_LocationWise(string stockitemModel, string StockCategoryID, string CompanyLocationID, string Active, string SalesTag, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockReceivedReport_LocationWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockitemliveReport_QuickStock(string stockitemModel, string StockCategoryID, string CompanyLocationID, string Active, string ScannedFlag, string DateType, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockitemliveReport_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ScannedFlag";
        if (ScannedFlag != string.Empty)
            param.Value = ScannedFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Get_StockItemLiveReport(string stockitemModel, string StockCategoryID, string CompanyLocationID, string Active, string ScannedFlag, string DateType, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Get_StockItemLiveReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ScannedFlag";
        if (ScannedFlag != string.Empty)
            param.Value = ScannedFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockitemliveReportDetailProjdeduct_QuickStock(string ProjectNumber, string InstallerID, string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockitemliveReportDetailProjdeduct_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockitemliveReportDetailProjScanned_QuickStock(string ProjectNumber, string InstallerID, string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockitemliveReportDetailProjScanned_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockitemliveReportDetailTrnsfDeduct_QuickStock(string StockTransferID, string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockitemliveReportDetailTrnsfDeduct_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockitemliveReportDetailWholesaleDeduct_QuickStock(string WholesaleOrderID, string InvoiceNumber, string CustomerID, string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockitemliveReportDetailWholesaleDeduct_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable AssignedStockTransfer_ForProjWholesale(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "AssignedStockTransfer_ForProjWholesale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable ViewRevertedStockItems(string PickListId, string WholesaleOrderId, string CategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ViewRevertedStockItems";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        if (PickListId != string.Empty)
            param.Value = PickListId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderId";
        if (WholesaleOrderId != string.Empty)
            param.Value = WholesaleOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CategoryID";
        if (CategoryID != string.Empty)
            param.Value = CategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable ViewRevertedStockItems_GroupByStockItem(string PickListId, string WholesaleOrderId, string CategoryID, string SerialNoList)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ViewRevertedStockItems_GroupByStockItem";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        if (PickListId != string.Empty)
            param.Value = PickListId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderId";
        if (WholesaleOrderId != string.Empty)
            param.Value = WholesaleOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CategoryID";
        if (CategoryID != string.Empty)
            param.Value = CategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNoList";
        if (SerialNoList != string.Empty)
            param.Value = SerialNoList;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = -1;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable ViewRevertedStockItems_GroupBy_OnlyDirectItems(string PickListId, string WholesaleOrderId, string CategoryID, string SerialNoList)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ViewRevertedStockItems_GroupBy_OnlyDirectItems";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        if (PickListId != string.Empty)
            param.Value = PickListId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderId";
        if (WholesaleOrderId != string.Empty)
            param.Value = WholesaleOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CategoryID";
        if (CategoryID != string.Empty)
            param.Value = CategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNoList";
        if (SerialNoList != string.Empty)
            param.Value = SerialNoList;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = -1;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNoHistoryReport(string ProjectNumber, string WholesaleOrderID, string SerialNo, string modulenm)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReport";
        comm.CommandText = "SerialNoHistoryReport_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulenm != string.Empty)
            param.Value = modulenm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@smProject";
        //if (modulenm != string.Empty)
        //    param.Value = modulenm;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 50;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByOrderid(string StockItemID, string StockLocation, string OrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByOrderid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable SerialNumberWiseReport_StockDeductDetailReport(string StockItemID, string StockLocation, string OrderNumber, string vendor, string txtStartDate, string txtEndDate, string datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ForStockdeductReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@vendor";
        if (vendor != string.Empty)
            param.Value = vendor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (txtStartDate != string.Empty)
            param.Value = txtStartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (txtEndDate != string.Empty)
            param.Value = txtEndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }



    public static DataTable StockWiseReport_SelectStockOrderQuantity(string StockItemID, string StockLocation, string Ordernumber, string Vendor)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockWiseReport_SelectStockOrderQuantity";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Ordernumber";
        if (Ordernumber != string.Empty)
            param.Value = Ordernumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Vendor";
        if (Vendor != string.Empty)
            param.Value = Vendor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable StockWiseReport_SelectWholeSalerOrder(string StockItemID, string StockLocation, string Invoiceno, string Customer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockWiseReport_SelectWholeSalerOrder";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Invoiceno";
        if (Invoiceno != string.Empty)
            param.Value = Invoiceno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Stockwisereport_GetStockSold(string StockItemID, string StockLocationId, string Projectnumber, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Stockwisereport_GetStockSold";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationId";
        if (StockLocationId != string.Empty)
            param.Value = StockLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        if (Projectnumber != string.Empty)
            param.Value = Projectnumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 20;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Select_weekdays()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Select_weekdays";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable SerialNoHistoryReportForbrokenReport(string ProjectNumber, string WholesaleOrderID, string SerialNo, string modulenm, string StartDate, string EnDdate, string DateType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNoHistoryReportForbrokenReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulenm != string.Empty)
            param.Value = modulenm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (EnDdate != string.Empty)
            param.Value = EnDdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockItemWiseReportNew_QuickStock(string stockitemModel,
        string StockCategoryID, string CompanyLocationID,
        string Active, string Type, string Depatched,
        string DateType, string startdate,
        string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockItemWiseReportNew_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Depatched";
        if (Depatched != string.Empty)
            param.Value = Depatched;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);



        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    #region Swipe Job Report
    public static DataTable SptblProject_GetProjectNumber()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblProject_GetProjectNumber";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int SptblProject_CompareProjectNumber(string F_ProjectInvoiceNo, string S_ProjectInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblProject_CompareProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FProjectNumber";
        if (F_ProjectInvoiceNo != string.Empty)
            param.Value = F_ProjectInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SProjectNumber";
        if (S_ProjectInvoiceNo != string.Empty)
            param.Value = S_ProjectInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int SptblProject_ExistsNumber(string number)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblProject_ExistsNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@number";
        if (number != string.Empty)
            param.Value = number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable SptblProject_GetDataByProjectNumber(string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblProject_GetDataByProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SptblWholesale_GetDataByInvoiceNumber(string InvoiceNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblWholesale_GetDataByInvoiceNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SptblStockSerialNo_GetSerialNoByProjID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SptblStockSerialNo_GetSerialNoByProjID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockDeductReport_QuickStock_Project(string stockitemModel, string StockCategoryID, string Active, string datetype,
        string startdate, string enddate, string CompanyId, string type, string IsDeduct, string EmpId, string InstallerId, string StockModel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockDeductReport_QuickStock_Project";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        if (EmpId != string.Empty)
            param.Value = EmpId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerId != string.Empty)
            param.Value = InstallerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockDeductReport_QuickStock_ProjectByID(string StockItemID, string CompanyId, string IsDeduct, string State, string InstallerName, string ProjectNumber, string DateType, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "StockDeductReport_QuickStock_ProjectByID";
        comm.CommandText = "StockDeductReport_QuickStock_ProjectByID_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static SttblProjects tblProjects_SelectByProjectIDForSolarMiner(string ProjectID)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjects details = new SttblProjects();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();

            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();


            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.ProjectStatus = dr["ProjectStatus"].ToString();

        }
        // return structure details
        return details;
    }
    #endregion
    public static DataTable SerialNoHistoryReportForbrokenReportNew(string StockItem, string WholesaleOrderID, string SerialNo, string modulenm, string StartDate, string EnDdate, string DateType, string Projectnumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReportForbrokenReport";
        comm.CommandText = "SerialNoHistoryReportForbrokenReportNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulenm != string.Empty)
            param.Value = modulenm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (EnDdate != string.Empty)
            param.Value = EnDdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        if (Projectnumber != string.Empty)
            param.Value = Projectnumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    #region New Swipe Job Report
    public static DataTable SwipeJobReport_QuickStock_GetDataByProjPickNumber(string ProjectNo, string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_GetDataByProjPickNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SwipeJobReport_QuickStock_GetDataByInvoiceNumber(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_GetDataByInvoiceNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SwipeJobReport_QuickStock_tblMaintainHistoryDeduct(string PickId, string modulenm)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_tblMaintainHistoryDeduct";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulenm != string.Empty)
            param.Value = modulenm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByPickID(string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByPickID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockItemId";
        //if (StockItemId != string.Empty)
        //    param.Value = StockItemId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static bool SwipeJobReport_QuickStock_Updatetbl_PickListLog(string PickId, string DeductBy, string DeductOn, string IsDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_Updatetbl_PickListLog";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductBy";
        if (DeductBy != string.Empty)
            param.Value = DeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductOn";
        if (DeductOn != string.Empty)
            param.Value = DeductOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
            //result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable SwipeJobReport_QuickStock_GetDataByInvoiceNo(string InvoiceNo, string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_GetDataByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = @WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByWholesaleOrderId(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByWholesaleOrderId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }
    #endregion

    #region StockWiseReportDetails
    public static DataTable QuickStock_StockWiseReport_DetailsByStockId(string StockItemID, string CompanyLocationId, string datetype, string startdate, string enddate, string OrderNumber, string Page)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_DetailsByStockId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationId";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Page";
        if (Page != string.Empty)
            param.Value = Page;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static int QuickStock_StockWiseReport_AriseSoldDetailsPanelorInverter(String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_AriseSoldDetailsPanelorInverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Panel(string StockItemID, string CompanyLocationId, string @datetype, string startdate, string enddate, string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Panel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationId";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Inverter(string StockItemID, string CompanyLocationId, string @datetype, string startdate, string enddate, string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Inverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationId";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static int QuickStock_StockWiseReport_SMSoldDetailsPanelorInverter(String StockItemID)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_SMSoldDetailsPanelorInverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccessSolarMiner.ExecuteScalar(comm));
        return id;
    }

    public static DataTable QuickStock_StockWiseReport_SMSoldDetailsByStockId_Panel(string StockItemID, string CompanyLocationId, string @datetype, string startdate, string enddate, string ProjectNumber)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_SMSoldDetailsByStockId_Panel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationId";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_SMSoldDetailsByStockId_Inverter(string StockItemID, string CompanyLocationId, string @datetype, string startdate, string enddate, string ProjectNumber)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_SMSoldDetailsByStockId_Inverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationId";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_AriseSoldPicklistDetailsByStockId(string StockItemID, string ComapanyLocationId, string ComapanyId, string ProjectNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_AriseSoldPicklistDetailsByStockId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComapanyLocationId";
        if (ComapanyLocationId != string.Empty)
            param.Value = ComapanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComapanyId";
        if (ComapanyId != string.Empty)
            param.Value = ComapanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@datetype";
        //if (datetype != string.Empty)
        //    param.Value = datetype;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startdate";
        //if (startdate != string.Empty)
        //    param.Value = startdate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@enddate";
        //if (enddate != string.Empty)
        //    param.Value = enddate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ProjectNumber";
        //if (ProjectNumber != string.Empty)
        //    param.Value = ProjectNumber;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_WholesaleDraftInvoiceDetailsByStockId(string StockItemID, string CompanyLocationId, string Type,
        string InvoiceNo, string CustomerId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_WholesaleDraftInvoiceDetailsByStockId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationId != string.Empty)
            param.Value = CompanyLocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        if (CustomerId != string.Empty)
            param.Value = CustomerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockwiseReport_GetSMSoldTotal(string StockItemID, string CompanyLocationID, string cat, string stockitemModel)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_StockwiseReport_GetSMSoldTotal";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cat";
        if (cat != string.Empty)
            param.Value = cat;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);

        return result;
    }
    #endregion

    public static DataTable InstallerwiseReportSolarMiner_QuickStock(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate, string IsVerifyIN, string PickListwise, string ProjectStatus, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "InstallerwiseReportSolarMiner_QuickStock";
        comm.CommandText = "InstallerwiseReportSolarMiner_QuickStock_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerifyIN";
        if (IsVerifyIN != string.Empty)
            param.Value = IsVerifyIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListwise";
        if (PickListwise != string.Empty)
            param.Value = PickListwise;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReportSolarMiner_QuickStock_New(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate, string ProjectStatus, string IsDifference, string IsAudit, string ApprovedFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReportSolarMiner_QuickStock_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifference";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (IsAudit != string.Empty)
            param.Value = @IsAudit;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedFlag";
        if (ApprovedFlag != string.Empty)
            param.Value = ApprovedFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_GetSMProjectDetailsByPNo(string ProjectNumber)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        //comm.CommandText = "InstallerwiseReportSolarMiner_QuickStock";
        comm.CommandText = "QuickStock_GetSMProjectDetailsByPNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NewReconciliationReportSolarMiner_QuickStock(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "NewReconciliationReportSolarMiner_QuickStock";
        comm.CommandText = "ReconciliationReport_GetData_SolarMiner";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblMaintainHostory_GetSerialNo(string PickId, string modulename, string SerialNo, string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHostory_GetSerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulename != string.Empty)
            param.Value = modulename;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_QuickStockShowAllSolerMiner(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string Projectwise, string IsDifference, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_QuickStockShowAllSolerMiner";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListwise";
        if (Projectwise != string.Empty)
            param.Value = Projectwise;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNoHistoryReport_GetTotal(string ProjectNumber, string Wholesale)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReport";
        comm.CommandText = "SerialNoHistoryReport_GetTotal";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Wholesale";
        if (Wholesale != string.Empty)
            param.Value = Wholesale;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPendingReport_Updated(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate, string ProjectStatus, string IsDifference, string @IsAudit)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPendingReport_Updated";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifference";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (IsAudit != string.Empty)
            param.Value = @IsAudit;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPredicationReport_Update(string StockItem, string StockModel, string StockCategoryID, string CompanyLocationID, string Active, string YesNo, string Weekdate, string StockContainer, string TansfComp, string DateType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPredicationReport_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Weekdate";
        if (Weekdate != string.Empty)
            param.Value = Weekdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockContainer";
        if (StockContainer != string.Empty)
            param.Value = StockContainer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Value = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TansfComp";
        if (TansfComp != string.Empty)
            param.Value = TansfComp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Value = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Value = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPredicationDetailsReport_GetData(string StockItemID, string LocationID, string OrderNumber, string Vendor, string txtStartDate, string txtEndDate, string datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPredicationDetailsReport_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@vendor";
        if (Vendor != string.Empty)
            param.Value = Vendor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (txtStartDate != string.Empty)
            param.Value = txtStartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (txtEndDate != string.Empty)
            param.Value = txtEndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPendingReport_ProjectWise(string ProjectNumber, string Location, string ProjectStatus, string InstallerName, string DateType, string StartDate, string EndDate, string IsDifference, string IsAudit)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPendingReport_ProjectWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifference";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (IsAudit != string.Empty)
            param.Value = IsAudit;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPendingReport_ItemWise(string ProjectNumber, string InstallerName, string StockItem, string StockModel, string Location, string DateType, string StartDate, string EndDate, string IsDifference, string IsAudit, string Category)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPendingReport_ItemWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifference";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (IsAudit != string.Empty)
            param.Value = IsAudit;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Category";
        if (Category != string.Empty)
            param.Value = Category;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_StockPendingReport_Updated_New(string ProjectNumber, string SerialNo, string stockitemModel, string CompanyLocationID, string InstallerID, string datetype, string startdate, string enddate, string ProjectStatus, string IsDifference, string IsAudit, string ApprovedFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockPendingReport_Updated_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifference";
        if (IsDifference != string.Empty)
            param.Value = IsDifference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (IsAudit != string.Empty)
            param.Value = IsAudit;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedFlag";
        if (ApprovedFlag != string.Empty)
            param.Value = ApprovedFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int StockPending_Picklistlog_UpdateInstalled(string Number, string PicklistID, string InstalledType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockPending_Picklistlog_UpdateInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Number";
        if (Number != string.Empty)
            param.Value = Number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstalledType";
        if (InstalledType != string.Empty)
            param.Value = InstalledType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_InstalledLog_Insert(string ProjectNumber, string PicklistID, string UserID, string DataTime, string CategoryID, string OldInstalled, string NewInstalled, string Notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_InstalledLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DataTime";
        if (DataTime != string.Empty)
            param.Value = DataTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CategoryID";
        if (CategoryID != string.Empty)
            param.Value = CategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldInstalled";
        if (OldInstalled != string.Empty)
            param.Value = OldInstalled;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewInstalled";
        if (NewInstalled != string.Empty)
            param.Value = NewInstalled;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable SrockPendingItemWise_GetDetailsOut(string StockItemID, string CompanyLocation, string modulename, string ProjectNumber, string DateType, string txtStartDate, string txtEndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SrockPendingItemWise_GetDetailsOut";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocation";
        if (CompanyLocation != string.Empty)
            param.Value = CompanyLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulename != string.Empty)
            param.Value = modulename;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (txtStartDate != string.Empty)
            param.Value = txtStartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (txtEndDate != string.Empty)
            param.Value = txtEndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable SrockPendingItemWise_GetDetailsInstalled(string StockItemID, string CompanyLocation, string ProjectNumber, string DateType, string txtStartDate, string txtEndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SrockPendingItemWise_GetDetailsInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocation";
        if (CompanyLocation != string.Empty)
            param.Value = CompanyLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (txtStartDate != string.Empty)
            param.Value = txtStartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (txtEndDate != string.Empty)
            param.Value = txtEndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_GetData( string StockItem, string StockModel, string StockCategoryID, string Active, string CompanyLocationID, string ExpiryDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        //param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpiryDate";
        if (ExpiryDate != string.Empty)
            param.Value = ExpiryDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_Getdata_UnusedSerialNoByStockItemID(string StockItemID, string LocationID, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Getdata_UnusedSerialNoByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_Picklistlog_UpdateBSGB(string ProjectNo, string P_BSGB, string I_BSGB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Picklistlog_UpdateBSGB";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@P_BSGB";
        if (P_BSGB != string.Empty)
            param.Value = P_BSGB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@I_BSGB";
        if (I_BSGB != string.Empty)
            param.Value = I_BSGB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_Picklistlog_Update_ApprovedFlagByPickID(string PicklistID, string ApprovedFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Picklistlog_Update_ApprovedFlagByPickID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedFlag";
        if (ApprovedFlag != string.Empty)
            param.Value = ApprovedFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        int id = DataAccess.ExecuteNonQuery(comm);
        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_InstalledLog_GetNotesByID(string PickListID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_InstalledLog_GetNotesByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListID";
        if (PickListID != string.Empty)
            param.Value = PickListID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SerialNumberWiseReport_ByProjNoNotRevertedOnly_new_SM(string ReportType, string stockitemModel, string StockCategoryID, string CompanyLocationID, string StockOrderID, string ReportID, string SerialNo, string InvoiceNumber, string PicklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNumberWiseReport_ByProjNoNotRevertedOnly_new_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        if (ReportType != string.Empty)
            param.Value = ReportType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportID";
        if (ReportID != string.Empty)
            param.Value = ReportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_Picklistlog_Update_ApprovedFlagNoteByPickID(string PicklistID, string ApprovedFlagNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Picklistlog_Update_ApprovedFlagNoteByPickID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedFlagNotes";
        if (ApprovedFlagNotes != string.Empty)
            param.Value = ApprovedFlagNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable InstallerwiseReport_GetDate(string ProjectNumber, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string IsDifferece, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_GetDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifferece != string.Empty)
            param.Value = IsDifferece;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockPendingReport_GetDate(string ProjectNumber, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string IsDifferece, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockPendingReport_GetDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifferece != string.Empty)
            param.Value = IsDifferece;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockPendingReport_GetData_SM(string ProjectNumber, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string IsDifferece, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockPendingReport_GetData_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifferece != string.Empty)
            param.Value = IsDifferece;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable InstallerwiseReport_GetDate_SM(string ProjectNumber, string CompanyLocationID, string InstallerID, string ProjectStatusID, string datetype, string startdate, string enddate, string Isverify, string IsDifferece, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InstallerwiseReport_GetDate_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        if (Isverify != string.Empty)
            param.Value = Isverify;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDifferece";
        if (IsDifferece != string.Empty)
            param.Value = IsDifferece;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsAudit";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable StockwiseReport_GetData(string StockItem, string StockModel, string StockCategoryID, string CompanyLocationID, string Active, string YesNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockwiseReport_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@YesNo";
        if (YesNo != string.Empty)
            param.Value = YesNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    #region StockWiseReportDetails
    public static DataTable QuickStock_StockWiseReport_TotalDetails(string StockItem, string IsActive, string LocationID, string StockCategoryID, string StockModel, string Page)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_TotalDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Page";
        if (Page != string.Empty)
            param.Value = Page;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_TotalAriseSoldDetails(string StockItem, string IsActive, string LocationID, string StockCategoryID, string StockModel, string Page)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_TotalAriseSoldDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Page";
        if (Page != string.Empty)
            param.Value = Page;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_TotalSMSoldDetails(string StockItem, string IsActive, string LocationID, string StockCategoryID, string StockModel, string Page)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_TotalSMSoldDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Page";
        if (Page != string.Empty)
            param.Value = Page;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_WholesaleDetails(string StockItem, string IsActive, string LocationID, string StockCategoryID, string StockModel, string Page, string Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_WholesaleDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Page";
        if (Page != string.Empty)
            param.Value = Page;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable QuickStock_StockWiseReport_TotPickListDetails(string StockItem, string IsActive, string LocationID, string StockCategoryID, string StockModel, string ComapanyId, string Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "QuickStock_StockWiseReport_TotPickListDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (IsActive != string.Empty)
            param.Value = IsActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (LocationID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComapanyId";
        if (ComapanyId != string.Empty)
            param.Value = ComapanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }
    #endregion

    public static DataTable tblStockTransfers_GetDataByStockCode(string StockCode, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_GetDataByStockCode";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        if (StockCode != string.Empty)
            param.Value = StockCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        return result;
    }

    public static DataTable Sp_SerialNoHistory_GetData(string ProjectNumber, string WholesaleOrderID, string SerialNo, string modulenm)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "SerialNoHistoryReport";
        comm.CommandText = "Sp_SerialNoHistory_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulenm != string.Empty)
            param.Value = modulenm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@smProject";
        //if (modulenm != string.Empty)
        //    param.Value = modulenm;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 50;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable ViewRevertedStockItems_SM(string PickListId, string WholesaleOrderId, string CategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ViewRevertedStockItems_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        if (PickListId != string.Empty)
            param.Value = PickListId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderId";
        if (WholesaleOrderId != string.Empty)
            param.Value = WholesaleOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CategoryID";
        if (CategoryID != string.Empty)
            param.Value = CategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    
}