﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;


public struct SttbPurchaseCompany
{
    public string PurchaseCompanyName;
    public string Address;
}
public class ClsPurchaseCompany
{
   
        public static DataTable tbl_PurchaseCompany_Select()
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "tbl_PurchaseCompany_Select";

            DataTable result = new DataTable();
            try
            {
                result = DataAccess.ExecuteSelectCommand(comm);
            }
            catch
            {
                // log errors if any
            }
            return result;
        }

    public static DataTable tbl_PurchaseCompanyGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompanyGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

     
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_PurchaseCompany_Exist(string PurchaseCompanyName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_Exist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyName";
        if (PurchaseCompanyName != string.Empty)
            param.Value = PurchaseCompanyName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);



        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }

    public static int tbl_PurchaseCompany_ID_Exist(String Id, String PurchaseCompanyName)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_ID_Exist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyName";
        if (PurchaseCompanyName != string.Empty)
            param.Value = PurchaseCompanyName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;

    }
    public static int tbl_PurchaseCompany_Insert(String PurchaseCompany)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompany";
        param.Value = PurchaseCompany;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_PurchaseCompany_Update(string PurchaseCompanyID, String PurchaseCompany)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyID";
        param.Value = PurchaseCompanyID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompany";
        param.Value = PurchaseCompany;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

     

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttbPurchaseCompany tbl_PurchaseCompany_SelectByPurchaseCompanyID(String PurchaseCompanyId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_SelectByPurchaseCompanyID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        param.Value = PurchaseCompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttbPurchaseCompany details = new SttbPurchaseCompany();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.PurchaseCompanyName = dr["PurchaseCompanyName"].ToString();
            details.Address = dr["Address"].ToString();
        }
        // return structure details
        return details;
    }

    public static bool tbl_PurchaseCompany_Delete(string PurchaseCompanyId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PurchaseCompanyId";
        param.Value = PurchaseCompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PurchaseCompany_Update_Address(string Id, string Address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PurchaseCompany_Update_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Address";
        if (Address != string.Empty)
            param.Value = Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}