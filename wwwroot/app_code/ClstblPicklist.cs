﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblPicklist
/// </summary>
public class ClstblPicklist
{
    public ClstblPicklist()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public static int tblPickListLog_ExistsByProjectNo_CompanyId(string ProjectNo, string CompanyId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_ExistsByProjectNo_CompanyId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        param.Value = ProjectNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        param.Value = CompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = 0;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static SttblProjects tblProjects_SelectByProjectNumber(string ProjectNo)
    {
        DbCommand comm = DataAccessAriseSolar.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        param.Value = ProjectNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccessAriseSolar.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjects details = new SttblProjects();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();

            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();
            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.ProjectStatus = dr["ProjectStatus"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblProjects SM_tblProjects_SelectByProjectNumber(string ProjectNo)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "SM_tblProjects_SelectByProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        param.Value = ProjectNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjects details = new SttblProjects();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();

            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();
            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.ProjectStatus = dr["ProjectStatus"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
        }
        // return structure details
        return details;
    }

    public static DataTable tblStockItems_Select_Category(string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Select_Category";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblPickListLog_Insert(String ProjectID, String Reason, String Note, String PickListDateTime, string InstallBookedDate, string InstallerID, string InstallerName, string DesignerID, string ElectricianID, String CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListDateTime";
        if (PickListDateTime != string.Empty)
            param.Value = PickListDateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_picklistlog_updatePicklistType(string ID, string PicklistType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_updatePicklistType";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistType";
        param.Value = PicklistType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (ID != null && ID != "")
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tbl_PickListLog_Update_IsToponeByProjectId(string ProjectID, string IsTopone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByProjectId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_IsToponeByPickListId(string PickId, string IsTopone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByPickListId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_upateCompanyid(string PickListId, string Companyid, string Projectnumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_upateCompanyid";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Companyid";
        param.Value = Companyid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        param.Value = Projectnumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_locationId_CompanyWise(string PickListId, string LocationId, string CompanyId, string ProjectName, string RoofType, string RooftypeId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_locationId_CompanyWise";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (LocationId != string.Empty)
            param.Value = LocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project";
        if (ProjectName != string.Empty)
            param.Value = ProjectName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeId";
        if (RooftypeId != string.Empty)
            param.Value = RooftypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        if (RoofType != string.Empty)
            param.Value = RoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_picklistlog_UpdateIPanel(string PickId, string Panel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateIPanel";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPanel";
        param.Value = Panel;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_picklistlog_UpdateInverter(string PickId, string Inverter)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateInverter";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IInverter";
        param.Value = Inverter;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static int tbl_PicklistItemDetail_Insert(string PickId, string StockItemId, string Picklistitem, string OrderQuantity, string Picklistlocation, string PicklistCategoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistitem";
        if (Picklistitem != string.Empty)
            param.Value = Picklistitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistlocation";
        if (Picklistlocation != string.Empty)
            param.Value = Picklistlocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistCategoryId";
        if (PicklistCategoryId != string.Empty)
            param.Value = PicklistCategoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_PicklistItemDetail_GetDataByPickID(string PicklilstID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_GetDataByPickID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklilstID";
        if (PicklilstID != string.Empty)
            param.Value = PicklilstID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PickListLog_SelectByPNo_CompnayID(string ProjectNo, string CompId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectByPNo_CompnayID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompId != string.Empty)
            param.Value = CompId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblPickListLog_Update(string PicklistID, string Reason, string Note, string InstallBookedDate, string InstallerID, string InstallerName, string DesignerID, string ElectricianID, string CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tbl_PicklistItemsDetail_Delete(string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemsDetail_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListItemId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblMaintainHistory_ExistsBySectionID(string SectionID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_ExistsBySectionID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SectionID";
        param.Value = SectionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = 0;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tbl_PickListLog_DeleteByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_DeleteByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblStockItems_GetFixItemIDByStockItemID(string CompanyID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_GetFixItemIDByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable Sp_OldPickList_getData(string CompanyID, string ProjectNo, string LocationId, string InstallBookingDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_OldPickList_getData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationId";
        if (LocationId != string.Empty)
            param.Value = LocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PickListLog_GetDataByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_GetDataByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PickListCategory_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListCategory_GetData";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_PickListLog_UpdatePickListCategory(string ID, string PickListCategory)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_UpdatePickListCategory";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListCategory";
        if (PickListCategory != string.Empty)
            param.Value = PickListCategory;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_stockWith(string ID, string stockWith)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_stockWith";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockWith";
        if (stockWith != string.Empty)
            param.Value = stockWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
}