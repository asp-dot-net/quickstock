﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for jobsResponse
/// </summary>
public class jobsResponse
{
    public jobsResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class root
    {
        public List<lstJobData> lstJobData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class lstJobData
    {
        public BasicDetails BasicDetails { get; set; }
        
        public JobOwnerDetails JobOwnerDetails { get; set; }
        
        public JobInstallationDetails JobInstallationDetails { get; set; }
        
        public JobSystemDetails JobSystemDetails { get; set; }
        
        public JobSTCDetails JobSTCDetails { get; set; }
        
        public InstallerView InstallerView { get; set; }
        
        public DesignerView DesignerView { get; set; }
        
        public JobElectricians JobElectricians { get; set; }
        
        public List<LstJobInverterDetail> lstJobInverterDetails { get; set; }
        
        public List<LstJobPanelDetail> lstJobPanelDetails { get; set; }
        
        public JobSTCStatusData JobSTCStatusData { get; set; }
    }

    public class BasicDetails
    {
        public string VendorJobId { get; set; }
        
        public int JobID { get; set; }
        
        public string Title { get; set; }
        
        public string RefNumber { get; set; }
        
        public string Description { get; set; }
        
        public int JobType { get; set; }
        
        public int JobStage { get; set; }
        
        public int Priority { get; set; }
        
        public DateTime? InstallationDate { get; set; }
        
        public DateTime? CreatedDate { get; set; }
        
        public bool IsGst { get; set; }
    }

    public class JobOwnerDetails
    {
        public string OwnerType { get; set; }
        
        public string CompanyName { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string Phone { get; set; }
        
        public string Mobile { get; set; }
        
        public string Email { get; set; }
        
        public bool IsPostalAddress { get; set; }
        
        public int? UnitTypeID { get; set; }
        
        public string UnitNumber { get; set; }
        
        public string StreetNumber { get; set; }
        
        public string StreetName { get; set; }
        
        public int StreetTypeID { get; set; }
        
        public string Town { get; set; }
        
        public string State { get; set; }
        
        public string PostCode { get; set; }
    }

    public class JobInstallationDetails
    {
        public int? DistributorID { get; set; }
        
        public string MeterNumber { get; set; }
        
        public string PhaseProperty { get; set; }
        
        public int? ElectricityProviderID { get; set; }
        
        public bool IsSameAsOwnerAddress { get; set; }
        
        public bool IsPostalAddress { get; set; }
        
        public int? UnitTypeID { get; set; }
        
        public string UnitNumber { get; set; }
        
        public string StreetNumber { get; set; }
        
        public string StreetName { get; set; }
        
        public int StreetTypeID { get; set; }
        
        public string Town { get; set; }
        
        public string State { get; set; }
        
        public string PostCode { get; set; }
        
        public string NMI { get; set; }
        
        public object SystemSize { get; set; }
        
        public string PropertyType { get; set; }
        
        public string SingleMultipleStory { get; set; }
        
        public string InstallingNewPanel { get; set; }
        
        public string Location { get; set; }
        
        public bool ExistingSystem { get; set; }
        
        public object ExistingSystemSize { get; set; }
        
        public string SystemLocation { get; set; }
        
        public object NoOfPanels { get; set; }
        
        public string AdditionalInstallationInformation { get; set; }
    }

    public class JobSystemDetails
    {
        public double? SystemSize { get; set; }
        public string SerialNumbers { get; set; }
        public int? NoOfPanel { get; set; }
    }

    public class JobSTCDetails
    {
        public string AdditionalCapacityNotes { get; set; }
        public string TypeOfConnection { get; set; }
        public string SystemMountingType { get; set; }
        public string DeemingPeriod { get; set; }
        public string CertificateCreated { get; set; }
        public string FailedAccreditationCode { get; set; }
        public string FailedReason { get; set; }
        public string MultipleSGUAddress { get; set; }
        public string Location { get; set; }
        public string AdditionalLocationInformation { get; set; }
        public string AdditionalSystemInformation { get; set; }
    }

    public class InstallerView
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string CECAccreditationNumber { get; set; }
        public string ElectricalContractorsLicenseNumber { get; set; }
        public bool IsPostalAddress { get; set; }
        public int UnitTypeID { get; set; }
        public string UnitNumber { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public int StreetTypeID { get; set; }
        public string Town { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
    }

    public class DesignerView
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string CECAccreditationNumber { get; set; }
        public string ElectricalContractorsLicenseNumber { get; set; }
        public bool IsPostalAddress { get; set; }
        public int UnitTypeID { get; set; }
        public string UnitNumber { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public int StreetTypeID { get; set; }
        public string Town { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
    }

    public class JobElectricians
    {
        public string CompanyName { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string Email { get; set; }
        
        public string Phone { get; set; }
        
        public string Mobile { get; set; }
        
        public string LicenseNumber { get; set; }
        
        public bool IsPostalAddress { get; set; }
        
        public int? UnitTypeID { get; set; }
        
        public string UnitNumber { get; set; }
        
        public string StreetNumber { get; set; }
        
        public string StreetName { get; set; }
        
        public int? StreetTypeID { get; set; }
        
        public string Town { get; set; }
        
        public string State { get; set; }
        
        public string PostCode { get; set; }
    }

    public class LstJobInverterDetail
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Series { get; set; }
        public int? NoOfInverter { get; set; }
    }

    public class LstJobPanelDetail
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public int NoOfPanel { get; set; }
    }

    public class JobSTCStatusData
    {
        public string STCStatus { get; set; }
        public double? CalculatedSTC { get; set; }
        public string STCPrice { get; set; }
        public string FailureNotice { get; set; }
        public string ComplianceNotes { get; set; }
        public DateTime? STCSubmissionDate { get; set; }
        public string STCInvoiceStatus { get; set; }
        public bool IsInvoiced { get; set; }
    }
}