﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblrevertItem
/// </summary>
public class ClstblrevertItem
{
    public static DataTable tblRevertItem_SelectData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRevertItem_SelectData";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblRevertItem_UpdateIsRevert(int Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRevertItem_UpdateIsRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);        

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblRevertItem_InsertData(string ProjectNumber, string ProjectId,string ItemId,string OrderQty, string picklistid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRevertItem_InsertData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;        
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;        
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ItemId";
        param.Value = ItemId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQty";
        param.Value = OrderQty;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        param.Value = picklistid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_StockRevertItems_SelectDataBySRid(string SRid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockRevertItems_SelectDataBySRid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SRid";
        param.Value = SRid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_StockRevertItems_DeleteBySerialNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockRevertItems_DeleteBySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_StockRevert_DeleteByid(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockRevert_DeleteByid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_UpdateIsverify(int Id, DateTime VerifyOn, string VerifyBy,string Isverify)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_UpdateIsverify";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        param.Value = Isverify;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyOn";
        param.Value = VerifyOn;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyBy";
        param.Value = VerifyBy;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_UpdateIsverify_ForInstallerReport(int Id, DateTime VerifyOn, string VerifyBy, string Isverify)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_UpdateIsverify_ForInstallerReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        param.Value = Isverify;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyOn";
        param.Value = VerifyOn;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyBy";
        param.Value = VerifyBy;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_UpdateIsverify(int WholesaleOrderID, DateTime VerifyOn, string VerifyBy, string Isverify)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateIsverify";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        param.Value = Isverify;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyOn";
        param.Value = VerifyOn;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyBy";
        param.Value = VerifyBy;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_UpdateIsverify__ForInstallerReport(int WholesaleOrderID, DateTime VerifyOn, string VerifyBy, string Isverify)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateIsverify__ForInstallerReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Isverify";
        param.Value = Isverify;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyOn";
        param.Value = VerifyOn;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifyBy";
        param.Value = VerifyBy;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_NoteUpdate(int Id, string notedate, string Verifynote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_NoteUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Verifynote";
        if (!string.IsNullOrEmpty(Verifynote))
            param.Value = Verifynote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@notedate";
        if (!string.IsNullOrEmpty(notedate))
            param.Value = notedate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);      

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_NoteUpdateIN(int Id, string notedateIN, string VerifynoteIN)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_NoteUpdateIN";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifynoteIN";
        if (!string.IsNullOrEmpty(VerifynoteIN))
            param.Value = VerifynoteIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@notedateIN";
        if (!string.IsNullOrEmpty(notedateIN))
            param.Value = notedateIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_NoteUpdate(int WholesaleOrderID, string NoteDes, string NoteDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_NoteUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteDes";
        if (!string.IsNullOrEmpty(NoteDes))
            param.Value = NoteDes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteDate";
        if (!string.IsNullOrEmpty(NoteDate))
            param.Value = NoteDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_NoteUpdateIN(int WholesaleOrderID, string NoteDesIN, string NoteDateIN)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_NoteUpdateIN";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteDesIN";
        if (!string.IsNullOrEmpty(NoteDesIN))
            param.Value = NoteDesIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteDateIN";
        if (!string.IsNullOrEmpty(NoteDateIN))
            param.Value = NoteDateIN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_UpdateData(string WholesaleOrderID,string ReportType,string StockDeductByUserId,string StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReportType";
        param.Value = ReportType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductByUserId";
        if (StockDeductByUserId != string.Empty)
            param.Value = StockDeductByUserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        if (StockDeductDate != string.Empty)
            param.Value = StockDeductDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Ispartialflag(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Ispartialflag";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);

    }
}