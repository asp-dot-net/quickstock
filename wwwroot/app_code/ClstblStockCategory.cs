using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblStockCategory
{
    public string StockCategory;
    public string Active;
    public string Seq;
}


public class ClstblStockCategory
{
    public static SttblStockCategory tblStockCategory_SelectByStockCategoryID(String StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_SelectByStockCategoryID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockCategory details = new SttblStockCategory();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockCategory = dr["StockCategory"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblStockCategory_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockCategory_Select_ByAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_Select_ByAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblStockCategory_Insert(String StockCategory, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategory";
        param.Value = StockCategory;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblStockCategory_Update(string StockCategoryID, String StockCategory, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategory";
        param.Value = StockCategory;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblStockCategory_InsertUpdate(Int32 StockCategoryID, String StockCategory, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategory";
        param.Value = StockCategory;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockCategory_Delete(string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockCategory_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int StockCategoryNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockCategoryNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
     public static int StockCategoryNameExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "StockCategoryNameExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
     public static DataTable tblStockCategoryGetDataByAlpha(string alpha)
     {
         // get a configured DbCommand object
         DbCommand comm = DataAccess.CreateCommand();
         comm.CommandText = "tblStockCategoryGetDataByAlpha";

         DbParameter param = comm.CreateParameter();
         param.ParameterName = "@alpha";
         param.Value = alpha;
         param.DbType = DbType.String;
         param.Size = 100;
         comm.Parameters.Add(param);
         // execute the stored procedure
         return DataAccess.ExecuteSelectCommand(comm);
     }
     public static DataTable tblStockCategoryGetDataBySearch(string alpha, string Active)
     {
         // get a configured DbCommand object
         DbCommand comm = DataAccess.CreateCommand();

         // set the stored procedure type
         comm.CommandText = "tblStockCategoryGetDataBySearch";
         DbParameter param = comm.CreateParameter();
         param.ParameterName = "@alpha";
         param.Value = alpha;
         param.DbType = DbType.String;
         param.Size = 100;
         comm.Parameters.Add(param);

         param = comm.CreateParameter();
         param.ParameterName = "@Active";
         if (Active != string.Empty)
             param.Value = Active;
         else
             param.Value = DBNull.Value;
         param.DbType = DbType.Int32;
         comm.Parameters.Add(param);
         // execute the stored procedure
         return DataAccess.ExecuteSelectCommand(comm);
     }
}