﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Common;



public class Clstbl_Wallet
{  
    public static int tbl_Wallet_ItemDetail_Exist(string StockItemID, string PickListid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Wallet_ItemDetail_Exist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListid";
        param.Value = PickListid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);      

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
   
    public static int tbl_Wallet_ItemDetail_Insert(string Accreditation, string StockItemID, string Qty, string PickListid, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Wallet_ItemDetail_Insert";

        DbParameter param = comm.CreateParameter();        
        param.ParameterName = "@Accreditation";
        param.Value = Accreditation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Qty";
        if (Qty != string.Empty)
            param.Value = Qty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListid";
        if (PickListid != string.Empty)
            param.Value = PickListid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
}