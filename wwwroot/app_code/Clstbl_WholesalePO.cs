﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Clstbl_WholesalePO
/// </summary>
public class Clstbl_WholesalePO
{
    public struct sttbl_WholesalePO
    {
        public string id;
        public string invoiceNo;
        public string customerId;
        public string stcNo;
        public string stcQty;
        public string stcRate;
        public string stcAmt;
        public string POStatus;
        public string paymentStatus;
        public string Delivery;
        public string xeroAmt;
        public string POType;
        public string POTypeText;
        public string gstValue;
    }

    public Clstbl_WholesalePO()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static sttbl_WholesalePO tabl_WholesalePO_GetDataByInvoiceNo(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_GetDataByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        sttbl_WholesalePO details = new sttbl_WholesalePO();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
           
            // get product details
            details.id = dr["id"].ToString();
            details.invoiceNo = dr["invoiceNo"].ToString();
            details.customerId = dr["customerId"].ToString();
            details.stcNo = dr["stcNo"].ToString();
            details.stcQty = dr["stcQty"].ToString();
            details.stcRate = dr["stcRate"].ToString();
            details.stcAmt = dr["stcAmt"].ToString();
            details.POStatus = dr["POStatus"].ToString();
            details.paymentStatus = dr["paymentStatus"].ToString();
            details.Delivery = dr["Delivery"].ToString();
            details.xeroAmt = dr["xeroAmt"].ToString();
            details.POType = dr["POType"].ToString();
            details.POTypeText = dr["POTypeText"].ToString();
            details.gstValue = dr["gstValue"].ToString();
        }
        return details;
    }

    public static DataTable tabl_WholesalePO_GetData(string invoiceNo, string customerId, string stcId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerId";
        if (customerId != string.Empty)
            param.Value = customerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stcId";
        if (stcId != string.Empty)
            param.Value = stcId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startdate";
        //if (startdate != string.Empty)
        //    param.Value = startdate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@enddate";
        //if (enddate != string.Empty)
        //    param.Value = enddate;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.DateTime;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@DateType";
        //if (DateType != string.Empty)
        //    param.Value = DateType;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@OrderNumber";
        //if (OrderNumber != string.Empty)
        //    param.Value = OrderNumber;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@State";
        //if (State != string.Empty)
        //    param.Value = State;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 10;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@stockitem";
        //if (stockitem != string.Empty)
        //    param.Value = stockitem;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 200;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ReferenceNo";
        //if (ReferenceNo != string.Empty)
        //    param.Value = ReferenceNo;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 50;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@IsDeliveredOrNot";
        //if (IsDeliveredOrNot != string.Empty)
        //    param.Value = IsDeliveredOrNot;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Boolean;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@TransportTypeId";
        //if (TransportTypeId != string.Empty)
        //    param.Value = TransportTypeId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);


        //param = comm.CreateParameter();
        //param.ParameterName = "@InstallTypeId";
        //if (InstallTypeId != string.Empty)
        //    param.Value = InstallTypeId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@InstallerNameId";
        //if (InstallerNameId != string.Empty)
        //    param.Value = InstallerNameId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@JobStatusId";
        //if (@JobStatusId != string.Empty)
        //    param.Value = @JobStatusId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@SolarTypeId";
        //if (SolarTypeId != string.Empty)
        //    param.Value = SolarTypeId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@JobTypeID";
        //if (JobTypeID != string.Empty)
        //    param.Value = JobTypeID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@DeliveryOptionID";
        //if (DeliveryOptionID != string.Empty)
        //    param.Value = DeliveryOptionID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@custemail";
        //if (custEmailID != string.Empty)
        //    param.Value = custEmailID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@EmpId";
        //if (EmpId != string.Empty)
        //    param.Value = EmpId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@InvoiceType";
        //if (invoiceType != string.Empty)
        //    param.Value = invoiceType;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ConsignPerson";
        //if (ConsignPerson != string.Empty)
        //    param.Value = ConsignPerson;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 100;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@Void";
        //if (Void != string.Empty)
        //    param.Value = Void;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tabl_WholesalePO_Insert()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Insert";

        //DbParameter aram = comm.CreateParameter();
        //param.ParameterName = "@OrderedBy";
        //if (OrderedBy != string.Empty)
        //    param.Value = OrderedBy;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 200;
        //comm.Parameters.Add(param);

        int id = 0; 
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch
        {
            // log errors if any
        }
        return id;
    }

    public static bool tabl_WholesalePO_Update(string id, string invoiceNo, string customerId, string stcNo, string stcQty, string stcRate, string paymentStatus, string poType, string poTypeText, string createdOn, string createdBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerId";
        if (customerId != string.Empty)
            param.Value = customerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stcNo";
        if (stcNo != string.Empty)
            param.Value = stcNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stcQty";
        if (stcQty != string.Empty)
            param.Value = stcQty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stcRate";
        if (stcRate != string.Empty)
            param.Value = stcRate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paymentStatus";
        if (paymentStatus != string.Empty)
            param.Value = paymentStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@poType";
        if (poType != string.Empty)
            param.Value = poType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@poTypeText";
        if (poTypeText != string.Empty)
            param.Value = poTypeText;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@createdOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@createdBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tabl_WholesalePO_Delete(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}