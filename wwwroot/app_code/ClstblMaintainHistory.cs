﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;

/// <summary>
/// Summary description for ClstblMaintainHistory
/// </summary>
public class ClstblMaintainHistory
{
    public static int tblMaintainHistory_InsertWithOrderID(String Userid, String SectionId, String SerialNo, String Section,string Message, DateTime CreatedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_InsertWithOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Userid";
        if (Userid != string.Empty)
            param.Value = Userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SectionId";
        if (SectionId != string.Empty)
            param.Value = SectionId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerailNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Section";
        if (Section != string.Empty)
            param.Value = Section;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Message";
        if (Message != string.Empty)
            param.Value = Message;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedDate";
        param.Value = CreatedDate;    
        param.DbType = DbType.DateTime;        
        comm.Parameters.Add(param);
      

        //int id = DataAccess.ExecuteNonQuery(comm);
        int id= Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblMaintainHistory_GetData_BySerialNo(String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_GetData_BySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblMaintainHistory_Delete_BySerailNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_Delete_BySerailNo";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerailNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);      
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblMaintainHistory_GetRevertItemCount(string Type, String modulename, string Sectionid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_GetRevertItemCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Type";
        param.Value = Type;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        param.Value = modulename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Sectionid";
        param.Value = Sectionid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblMaintainHistory_Select_WholesaleRevertById(string WholesaleOrderID,string itemid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_Select_WholesaleRevertById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@itemid";
        //param.Value = itemid;
        //param.DbType = DbType.Int32;      
        //comm.Parameters.Add(param);
        param = comm.CreateParameter();
        param.ParameterName = "@itemid";
        if (itemid != string.Empty)
            param.Value = itemid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblMaintainHistory_Updateinoutflag(string id, string inoutflag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_Updateinoutflag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@inoutflag";
        if (inoutflag != string.Empty)
            param.Value = inoutflag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);       

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblMaintainHistory_DeletePickListOut(string SerialNo, string PickListId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_DeletePickListOut";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblMaintainHistory_DeleteWholeSale(string SerialNo, string WholeSaleOrderId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_DeleteWholeSale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholeSaleOrderId";
        param.Value = WholeSaleOrderId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblMaintainHistory_DeleteRecord_SerialNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_DeleteRecord_SerialNo";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblMaintainHistory_wholesalescancount(string Wholesaleid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_wholesalescancount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Wholesaleid";
        param.Value = Wholesaleid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool Delate_tblMaintainHistoryByStockOrderItemID(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Delate_tblMaintainHistoryByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
        }
        return (result != -1);
    }
}