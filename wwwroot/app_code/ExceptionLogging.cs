﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using context = System.Web.HttpContext;

/// <summary>
/// Summary description for ExceptionLogging
/// </summary>
public class ExceptionLogging
{
    private static String exepurl;
    static SqlConnection con;
    public ExceptionLogging()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static void connecttion()
    {
        string connectionString = SiteConfiguration.DbConnectionString;
        con = new SqlConnection(connectionString);
        con.Open();
    }

    public static void SendExcepToDB(Exception exdb)
    {
        exepurl = context.Current.Request.Url.ToString();
        connecttion();
        SqlCommand com = new SqlCommand("ExceptionLoggingToDataBase", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.AddWithValue("@ExceptionMsg", exdb.Message.ToString());
        com.Parameters.AddWithValue("@ExceptionType", exdb.GetType().Name.ToString());
        com.Parameters.AddWithValue("@ExceptionURL", exepurl);
        com.Parameters.AddWithValue("@ExceptionSource", exdb.StackTrace.ToString());
        com.ExecuteNonQuery();
    }
}