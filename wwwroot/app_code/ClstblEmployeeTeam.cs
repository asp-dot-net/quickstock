using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblEmployeeTeam
{
    public string EmployeeID;
    public string SalesTeamID;

}


public class ClstblEmployeeTeam
{
    public static SttblEmployeeTeam tblEmployeeTeam_SelectByEmployeeTeamID(String EmployeeTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_SelectByEmployeeTeamID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeTeamID";
        param.Value = EmployeeTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblEmployeeTeam details = new SttblEmployeeTeam();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblEmployeeTeam_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblEmployeeTeam_Insert(String EmployeeID, String SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblEmployeeTeam_Update(string EmployeeTeamID, String EmployeeID, String SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeTeamID";
        param.Value = EmployeeTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblEmployeeTeam_Delete(string EmployeeTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeTeamID";
        param.Value = EmployeeTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblEmployeeTeam_DeletebyEmployeeID(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_DeletebyEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblEmployeeTeam_SelectbyEmployeeID(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeTeam_SelectbyEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable aspnet_MembershipLastLoginDateSelectByUserId(string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnet_MembershipLastLoginDateSelectByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_Selectbyroles(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Selectbyroles";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}