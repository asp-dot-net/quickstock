﻿/// <summary>
/// Summary description for loginResponse
/// </summary>
public class loginResponse
{
    public loginResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class Root
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }
}