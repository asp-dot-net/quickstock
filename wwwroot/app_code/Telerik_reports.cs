﻿using System;
using System.Web;
using System.Data;
using System.IO;


/// <summary>
/// Summary description for Telerik_reports
/// </summary>
public class Telerik_reports
{

    public Telerik_reports()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void generate_WholesalePicklist(string WholesaleOrderID)
    {
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.WholesalePicklist();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox26 = rpt.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox27 = rpt.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            textBox7.Value = st.InvoiceNo;
            textBox11.Value = st.ReferenceNo;

            table3.DataSource = dt;
        }
        catch { }

        try
        {
            SttblCustomers st3 = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
            textBox1.Value = st3.Customer;
            //string postaladd = string.Empty;
            //postaladd = st3.unit_number + " " + st3.unit_type;
            //if(string.IsNullOrEmpty(st3.unit_number + " " + st3.unit_type))
            //{
            //    postaladd = st3.street_number + " " + st3.street_name +" "+ st3.street_type; 
            //}
            //else
            //{
            //    postaladd= postaladd.Trim()+" "+ st3.street_number + " " + st3.street_name + " " + st3.street_type;
            //}
            textBox26.Value = st3.PostalAddress.Trim();
            string address = string.Empty;
            if (!string.IsNullOrEmpty(st3.PostalCity))
            {
                address = st3.PostalCity;
            }
            if (!string.IsNullOrEmpty(st3.PostalState))
            {
                address = address + ", " + st3.PostalState;
            }
            if (!string.IsNullOrEmpty(st3.PostalPostCode))
            {
                address = address + ", " + st3.PostalPostCode;
            }
            textBox27.Value = address;
        }
        catch { }

        ExportPdf(rpt);
    }

    public static void generate_WholesaleSTC(string WholesaleOrderID)
    {

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.WholesaleSTC();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox26 = rpt.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox27 = rpt.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtqty = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtunitprice = rpt.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtamount = rpt.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtToalAud = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtNotes = rpt.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;

        txtqty.Value = "0";
        txtunitprice.Value = "0";
        txtamount.Value = "0";
        txtToalAud.Value = "0";
        table4.DataSource = st;
        table5.DataSource = st;

        decimal tot = 0;
        if (!string.IsNullOrEmpty(st.STCNumber))
        {
            txtqty.Value = st.STCNumber;
        }
        if (!string.IsNullOrEmpty(st.StcValue))
        {
            txtunitprice.Value = st.StcValue;
        }


        tot = Convert.ToDecimal(txtqty.Value) * Convert.ToDecimal(txtunitprice.Value);
        txtamount.Value = Convert.ToString(tot);
        txtToalAud.Value = Convert.ToString(tot);
        textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
        //textBox7.Value = st.InvoiceNo;
        textBox11.Value = st.ReferenceNo;
        try
        {
            //table3.DataSource = st;
            txtNotes.Value = st.Notes;
        }
        catch { }

        try
        {
            SttblCustomers st3 = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
            textBox1.Value = st3.Customer;
            //string postaladd = string.Empty;
            //postaladd = st3.unit_number + " " + st3.unit_type;
            //if(string.IsNullOrEmpty(st3.unit_number + " " + st3.unit_type))
            //{
            //    postaladd = st3.street_number + " " + st3.street_name +" "+ st3.street_type; 
            //}
            //else
            //{
            //    postaladd= postaladd.Trim()+" "+ st3.street_number + " " + st3.street_name + " " + st3.street_type;
            //}
            textBox26.Value = st3.PostalAddress.Trim();
            string address = string.Empty;
            if (!string.IsNullOrEmpty(st3.PostalCity))
            {
                address = st3.PostalCity;
            }
            if (!string.IsNullOrEmpty(st3.PostalState))
            {
                address = address + ", " + st3.PostalState;
            }
            if (!string.IsNullOrEmpty(st3.PostalPostCode))
            {
                address = address + ", " + st3.PostalPostCode;
            }
            textBox27.Value = address;
        }
        catch { }

        ExportPdf1(rpt, st.ReferenceNo);
    }

    public static void generate_WholesalePicklist(string WholesaleOrderID, string SaveOrDownload)
    {

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);


        Telerik.Reporting.Report rpt = new EurosolarReporting.WholesalePicklist();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            textBox7.Value = st.InvoiceNo;
            textBox11.Value = st.ReferenceNo;

            table3.DataSource = dt;
        }
        catch { }

        SavePDF2(rpt, WholesaleOrderID + "_Wholesale.pdf", "SavePDF2");
    }

    public static void ExportPdf(Telerik.Reporting.Report rpt)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = result.DocumentName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);

        HttpContext.Current.Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    public static void ExportPdf(Telerik.Reporting.Report rpt, string fName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = fName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);

        HttpContext.Current.Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    public static void SavePDF(Telerik.Reporting.Report rpt, string QuoteID)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = QuoteID + result.DocumentName + "." + result.Extension;

        string ExcelFilename = fileName;

        //string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\", ExcelFilename);
        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoctest\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        //SiteConfiguration.deleteimage(fileName, "quotedoc");
        SiteConfiguration.UploadPDFFile("quotedoctest", fileName);
        SiteConfiguration.deleteimage(fileName, "quotedoctest");
    }

    public static void SavePDFForSQ(Telerik.Reporting.Report rpt, string FileName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        //string fileName = QuoteID + result.DocumentName + "." + result.Extension;
        string fileName = FileName;

        string ExcelFilename = fileName;

        //string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQ\\", ExcelFilename);
        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQtest\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        //SiteConfiguration.UploadPDFFile("SQ", fileName);
        //SiteConfiguration.deleteimage(fileName, "SQ");
        SiteConfiguration.UploadPDFFile("SQtest", fileName);
        SiteConfiguration.deleteimage(fileName, "SQtest");
    }

    public static void RemovePDF(Telerik.Reporting.Report rpt, string QuoteID)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = QuoteID + result.DocumentName + "." + result.Extension;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Truncate, FileAccess.ReadWrite);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        SiteConfiguration.DeletePDFFile("quotedoc", fileName);
        SiteConfiguration.deleteimage(fileName, "quotedoc");
    }

    public static void SavePDF2(Telerik.Reporting.Report rpt, string FileName, string FolderName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\" + FolderName + "\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
    }

    public static void WholesalePDF(string WholeSaleOrderID)
    {
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectForPDF(WholeSaleOrderID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.STCInvoice();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox textBox35 = rpt.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox13 = rpt.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox31 = rpt.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox34 = rpt.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox20= rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox19 = rpt.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox15 = rpt.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table4.DataSource = dt;
        textBox35.Value = dt.Rows[0]["InvoiceNo"].ToString();
        DateTime datetime = DateTime.UtcNow.Date;
        textBox13.Value = "STC For InvoiceNo" + dt.Rows[0]["InvoiceNo"].ToString();
        textBox7.Value = datetime.ToString("dd/MMMM/yyyy");
        textBox10.Value = dt.Rows[0]["Customername"].ToString();
        textBox31.Value = dt.Rows[0]["Address"].ToString();
        textBox34.Value = dt.Rows[0]["MobileNumber"].ToString();
        textBox20.Value = dt.Rows[0]["InvoiceAmount"].ToString();
        textBox19.Value = dt.Rows[0]["InvoiceAmount"].ToString();
        DateTime dateTime1 = Convert.ToDateTime(dt.Rows[0]["DateOrdered"].ToString());
        textBox15.Value = dateTime1.ToString("dd/MMMM/yyyy");
        //textBox5.Value = dt.Rows[0]["CustWeb"].ToString();



        //SavePDF2(rpt, WholeSaleOrderID + "_Wholesale.pdf", "SavePDF2");
        ExportPdf(rpt);
        //ExportPdf1(rpt, textBox35.Value);
    }

    public static void ExportPdf1(Telerik.Reporting.Report rpt,string Id)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = result.DocumentName+"-"+ Id + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);

        //HttpContext.Current.Response.Flush();
        // HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    public static void Generate_PurchaseOrder(string OrderID)
    {
        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(OrderID);
        DataTable dtOrderItems = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(OrderID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.PurchaseOrder();
        //Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox txtCompanyName = rpt.Items.Find("txtCompanyName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCompanyAddress = rpt.Items.Find("txtCompanyAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPODate = rpt.Items.Find("txtPODate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPONo = rpt.Items.Find("txtPONo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCustomerID = rpt.Items.Find("txtCustomerID", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtVendor = rpt.Items.Find("txtVendor", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtVendorAddress = rpt.Items.Find("txtVendorAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtShipTo = rpt.Items.Find("txtShipTo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtShipVia = rpt.Items.Find("txtShipVia", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtShippingMethod = rpt.Items.Find("txtShippingMethod", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtShippingTerms = rpt.Items.Find("txtShippingTerms", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table tblStockOrderItems = rpt.Items.Find("tblStockOrderItems", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtNotes = rpt.Items.Find("txtNotes", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            //textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            //textBox7.Value = st.InvoiceNo;
            //textBox11.Value = st.ReferenceNo;

            //table3.DataSource = dt;
        }
        catch(Exception ex)
        {
        }

        ExportPdf(rpt);
    }

    public static void Generate_Barcode(string projectNo, DataTable dtSerialNo)
    {
        Telerik.Reporting.Report rpt = new EurosolarReporting.Barcode();

        Telerik.Reporting.TextBox txtHeader = rpt.Items.Find("txtHeader", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

        try
        {
            txtHeader.Value = projectNo + " Jobs Serial No";
            table1.DataSource = dtSerialNo;
        }
        catch (Exception ex)
        {
        }

        ExportPdf(rpt);
    }

    public static void Generate_STCInvoice(string OrderID)
    {
        Telerik.Reporting.Report rpt = new QsReporting.Invoice();

        Telerik.Reporting.TextBox txtInvoiceNo = rpt.Items.Find("txtInvoiceNo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInvoiceDate = rpt.Items.Find("txtInvoiceDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPaymentDueDate = rpt.Items.Find("txtPaymentDueDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAmountDue = rpt.Items.Find("txtAmountDue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtSalesOfSTC = rpt.Items.Find("txtSalesOfSTC", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtQty = rpt.Items.Find("txtQty", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPrice = rpt.Items.Find("txtPrice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAmount = rpt.Items.Find("txtAmount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBankName = rpt.Items.Find("txtBankName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAccountName = rpt.Items.Find("txtAccountName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBSBNo = rpt.Items.Find("txtBSBNo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAccountNo = rpt.Items.Find("txtAccountNo", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txtCustName = rpt.Items.Find("txtCustName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAdd1 = rpt.Items.Find("txtAdd1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAdd2 = rpt.Items.Find("txtAdd2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPhone = rpt.Items.Find("txtPhone", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCustEmail = rpt.Items.Find("txtCustEmail", true)[0] as Telerik.Reporting.TextBox;

        Sttbl_WholesaleOrders sttbl = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(OrderID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(sttbl.CustomerID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByCustomerID(sttbl.CustomerID);

        try
        {
            decimal Qty = !string.IsNullOrEmpty(sttbl.Stc_Value_Excel) ? Convert.ToDecimal(sttbl.Stc_Value_Excel) : 0;
            decimal stcValue = !string.IsNullOrEmpty(sttbl.StcValue) ? Convert.ToDecimal(sttbl.StcValue.Trim()) : 0;
            decimal Amt = Qty * stcValue;

            txtInvoiceNo.Value = sttbl.InvoiceNo;
            txtInvoiceDate.Value = System.DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
            txtPaymentDueDate.Value = System.DateTime.Now.AddHours(15).AddDays(7).ToString("dd/MM/yyyy");
            txtAmountDue.Value = "$" + Amt.ToString("F");

            txtSalesOfSTC.Value = sttbl.ReferenceNo;
            txtQty.Value =  Convert.ToInt64(Qty).ToString();
            txtPrice.Value = "$" + sttbl.StcValue;
            txtAmount.Value = "$" + Amt.ToString("F");

            txtBankName.Value = stCust.BankName;
            txtAccountName.Value = stCust.AccountName;
            txtBSBNo.Value = stCust.BSBNo;
            txtAccountNo.Value = stCust.AccountNo;

            txtCustName.Value = stCust.Customer;
            txtAdd1.Value = stCust.StreetAddress;
            txtAdd2.Value = stCust.StreetCity + ", " + stCust.StreetState + " " + stCust.StreetPostCode;
            txtPhone.Value = stCont.ContMobile;
            txtCustEmail.Value = stCont.ContEmail;
        }
        catch (Exception ex)
        {
        }

        ExportPdf(rpt, sttbl.InvoiceNo);
    }
}