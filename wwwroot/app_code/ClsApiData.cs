﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsApiData
/// </summary>
public class ClsApiData
{
    public ClsApiData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable Sp_GetProjectNo()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_GetProjectNo";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@CompanyID";
        //if (CompanyID != string.Empty)
        //    param.Value = CompanyID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tblSerialNoFromBSGB_Insert(string ProjectNo, string SerialNo, string BSGBFlag, string CompanyID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSerialNoFromBSGB_Insert";

        DbParameter param = comm.CreateParameter();
        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BSGBFlag";
        if (BSGBFlag != string.Empty)
            param.Value = BSGBFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {

        }
        return id;
    }
}