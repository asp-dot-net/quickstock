﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Invoice_Response
/// </summary>
public class Invoice_Response
{
    public Invoice_Response()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class Address
    {
        public string AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }

    public class Phone
    {
        public string PhoneType { get; set; }
    }

    public class Contact
    {
        public string TaxNumber { get; set; }
        public string ContactID { get; set; }
        public string ContactStatus { get; set; }
        public string Name { get; set; }
        public List<Address> Addresses { get; set; }
        public List<Phone> Phones { get; set; }
        public DateTime UpdatedDateUTC { get; set; }
        public string IsSupplier { get; set; }
        public string IsCustomer { get; set; }
    }

    public class Tracking
    {
        public string TrackingCategoryID { get; set; }
        public string Name { get; set; }
        public string Option { get; set; }
    }

    public class LineItem
    {
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string UnitAmount { get; set; }
        public string TaxType { get; set; }
        public string TaxAmount { get; set; }
        public string LineAmount { get; set; }
        public string AccountCode { get; set; }
        public List<Tracking> Tracking { get; set; }
        public string LineItemID { get; set; }
    }

    public class Payment
    {
        public DateTime Date { get; set; }
        public string Amount { get; set; }
        public string PaymentID { get; set; }
    }

    public class LineItem_Invoice
    {
        public string LineItemID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class Invoice
    {
        public string Type { get; set; }
        public Contact Contact { get; set; }
        public string Reference { get; set; }
        
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateString { get; set; }
        public DateTime DueDateString { get; set; }
        public string Status { get; set; }
        public string LineAmountTypes { get; set; }
        public List<LineItem> LineItems { get; set; }
        public string SubTotal { get; set; }
        public string TotalTax { get; set; }
        public string Total { get; set; }
        public DateTime UpdatedDateUTC { get; set; }
        public string CurrencyCode { get; set; }
        public string InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }
        public List<Payment> Payments { get; set; }
        public string AmountDue { get; set; }
        public string AmountPaid { get; set; }
        public string AmountCredited { get; set; }
    }

    public class RootObject
    {
        public List<Invoice> Invoices { get; set; }
    }
    public class ItemObject
    {
        public List<LineItem_Invoice> Items { get; set; }
    }
    public class Connections
    {
        public string id { get; set; }
        public string tenantId { get; set; }
        public string tenantType { get; set; }
        public DateTime createdDateUtc { get; set; }
        public DateTime updatedDateUtc { get; set; }
    }

}