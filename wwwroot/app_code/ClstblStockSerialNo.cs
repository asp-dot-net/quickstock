﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;

public class ClstblStockSerialNo
{
    public static bool tblStockSerialNo_updateFlag(String SerialNo, String StockItemID, String StockLocationID,string Pallet, String Userid, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_updateFlag";

        DbParameter param = comm.CreateParameter();        
        param.ParameterName = "@SerailNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);      

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;       
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Userid";
        if (Userid != string.Empty)
            param.Value = Userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_Revertall_ByPickListIDSerialNo(string PicklistId, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Revertall_ByPickListIDSerialNo ";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistId";
        if (PicklistId != string.Empty)
            param.Value = PicklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;       
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_Revertall_ByWholesaleIDSerialNo(string WholesaleOrderID, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Revertall_ByWholesaleIDSerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo(string SerialNo,string WholesaleOrderID, string IsActive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsActive";
        param.Value = IsActive;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockSerialNoCount_ByWholesaleOrderID(string WholesaleOrderID, string StockCategoryID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoCount_ByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNoCount_ByWholesaleOrderIDStockItemID(string WholesaleOrderID,  string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoCount_ByWholesaleOrderIDStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool Update_tblStockSerialNo_ProjectIdandPickListId(string ProjectId, string PickListId, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblStockSerialNo_ProjectIdandPickListId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool Update_tblStockSerialNo_WholesaleOrderID(string WholesaleOrderID, string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblStockSerialNo_WholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);      

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockSerialNo_Select_WholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Select_WholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockSerialNo_Update_StockdeductReport(string SerialNo, string ProjectId, string PickListId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Update_StockdeductReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_Update_StockdeductReportWholeSale(string SerialNo, string WholeSaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Update_StockdeductReportWholeSale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholeSaleOrderID";
        param.Value = WholeSaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_DeleteRecord_SerialNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_DeleteRecord_SerialNo";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_UpdateSerialno(string SerialNo, string StockItemId,string UpdateSeriNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_UpdateSerialno";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        param.Value = StockItemId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdateSeriNo";
        param.Value = UpdateSeriNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockSerialNo_Clear(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Clear";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool Update_tblMaintainHistory_SectionIdandMsgBySerialNo(string SectionId, string msg, string SerialNo, string modulename, string PickID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblMaintainHistory_SectionIdandMsgBySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Msg";
        param.Value = msg;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SectionId";
        param.Value = SectionId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        param.Value = modulename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        //try
        //{
            result = DataAccess.ExecuteNonQuery(comm);
        //}
        //catch(Exception ex)
        //{
            // log errors if any
        //}
        return (result != -1);
    }

    public static bool Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo(string SectionId, string msg, string SerialNo, string modulename, string PickID, string NewModuleName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Msg";
        param.Value = msg;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SectionId";
        param.Value = SectionId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        param.Value = modulename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewModuleName";
        param.Value = NewModuleName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        //try
        //{
        result = DataAccess.ExecuteNonQuery(comm);
        //}
        //catch(Exception ex)
        //{
        // log errors if any
        //}
        return (result != -1);
    }
}