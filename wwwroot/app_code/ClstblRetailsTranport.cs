﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblRetailsTranport
/// </summary>

public struct SttblRetailTransport
{
    public string ID;
    public string TransportDate;
    public string EnteredBy;
    public string CompanyId;
    public string TransportCompany;
    public string TransportFrom;
    public string Location;
    public string TrackingNo;
    public string Quantity;
    public string Weight;
    public string Amount;
    public string Notes;
}

public class ClstblRetailsTranport
{
    public ClstblRetailsTranport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static int tbl_RetailTransport_Exits_TrackingNumber(string TrackingNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_Exits_TrackingNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TrackingNo";
        param.Value = TrackingNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static int tbl_RetailTransport_Insert(string TransportDate, string EnteredBy, string CompanyId, string TransportCompany, string TransportFrom, string Location, string TrackingNo, string Quantity, string Weight, string Amount, string Notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TransportDate";
        if (TransportDate != string.Empty)
            param.Value = TransportDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EnteredBy";
        if (EnteredBy != string.Empty)
            param.Value = EnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportCompany";
        if (TransportCompany != string.Empty)
            param.Value = TransportCompany;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportFrom";
        if (TransportFrom != string.Empty)
            param.Value = TransportFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNo";
        if (TrackingNo != string.Empty)
            param.Value = TrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Quantity";
        if (Quantity != string.Empty)
            param.Value = Quantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Weight";
        if (Weight != string.Empty)
            param.Value = Weight;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_RetailTransportProjectNo_Insert(string RetailtransportID, string ProjectNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransportProjectNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RetailtransportID";
        if (RetailtransportID != string.Empty)
            param.Value = RetailtransportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return id;
    }

    public static bool tbl_RetailTransport_Update(string ID, string TransportDate, string EnteredBy, string CompanyId, string TransportCompany, string TransportFrom, string Location, string TrackingNo, string Quantity, string Weight, string Amount, string Notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportDate";
        if (TransportDate != string.Empty)
            param.Value = TransportDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EnteredBy";
        if (EnteredBy != string.Empty)
            param.Value = EnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportCompany";
        if (TransportCompany != string.Empty)
            param.Value = TransportCompany;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportFrom";
        if (TransportFrom != string.Empty)
            param.Value = TransportFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNo";
        if (TrackingNo != string.Empty)
            param.Value = TrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Quantity";
        if (Quantity != string.Empty)
            param.Value = Quantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Weight";
        if (Weight != string.Empty)
            param.Value = Weight;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_RetailTransportProjectNo_DeleteByRetailtransportID(string RetailtransportID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransportProjectNo_DeleteByRetailtransportID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RetailtransportID";
        if (RetailtransportID != string.Empty)
            param.Value = RetailtransportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_RetailTransport_DeleteByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_DeleteByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttblRetailTransport tbl_RetailTransport_SelectByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblRetailTransport details = new SttblRetailTransport();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.ID = dr["ID"].ToString();
            details.TransportDate = dr["TransportDate"].ToString();
            details.EnteredBy = dr["EnteredBy"].ToString();
            details.CompanyId = dr["CompanyId"].ToString();
            details.TransportCompany = dr["TransportCompany"].ToString();
            details.TransportFrom = dr["TransportFrom"].ToString();
            details.Location = dr["Location"].ToString();
            details.TrackingNo = dr["TrackingNo"].ToString();
            details.Quantity = dr["Quantity"].ToString();
            details.Weight = dr["Weight"].ToString();
            details.Amount = dr["Amount"].ToString();
            details.Notes = dr["Notes"].ToString();
        }
        // return structure details
        return details;
    }

    public static DataTable tbl_RetailTransportProjectNo_SearchByRetailtransportID(string RetailtransportID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransportProjectNo_SearchByRetailtransportID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RetailtransportID";
        if (RetailtransportID != string.Empty)
            param.Value = RetailtransportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_RetailTransport_GetData(string TransportID, string TransportFrom, string TransportCompany, string Location, string TrackingNo, string DateType, string StartDate, string EndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_RetailTransport_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TransportID";
        if (TransportID != string.Empty)
            param.Value = TransportID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportFrom";
        if (TransportFrom != string.Empty)
            param.Value = TransportFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportCompany";
        if (TransportCompany != string.Empty)
            param.Value = TransportCompany;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNo";
        if (TrackingNo != string.Empty)
            param.Value = TrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

}