﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Payment_Response
/// </summary>
public class Payment_Response
{
    public Payment_Response()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Account
    {
        public string AccountID { get; set; }
        public string Code { get; set; }
    }

    public class BatchPayment
    {
        public Account Account { get; set; }
        public string BatchPaymentID { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string TotalAmount { get; set; }
        public DateTime UpdatedDateUTC { get; set; }
        public string IsReconciled { get; set; }
    }

    public class Contact
    {
        public string ContactID { get; set; }
        public string Name { get; set; }
    }

    public class Invoice
    {
        public string Type { get; set; }
        public string InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }
        public Contact Contact { get; set; }
    }

    public class Payment
    {
        public string PaymentID { get; set; }
        public string BatchPaymentID { get; set; }
        public BatchPayment BatchPayment { get; set; }
        public DateTime Date { get; set; }
        public double BankAmount { get; set; }
        public double Amount { get; set; }
        public string Reference { get; set; }
        public double CurrencyRate { get; set; }
        public string PaymentType { get; set; }
        public string Status { get; set; }
        public DateTime UpdatedDateUTC { get; set; }
        public bool HasAccount { get; set; }
        public bool IsReconciled { get; set; }
        public Account Account { get; set; }
        public Invoice Invoice { get; set; }
    }

    public class Root
    {
        public List<Payment> Payments { get; set; }
    }


}