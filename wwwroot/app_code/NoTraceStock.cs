﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NoTraceStock
/// </summary>
public class NoTraceStock
{
    public NoTraceStock()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable GreenbotAllData_InveterSerialNo()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GreenbotAllData_InveterSerialNo";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@companyID";
        //if (companyID != string.Empty)
        //    param.Value = companyID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int Bulk_Insert_tbl_SerialNo(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_Insert_tbl_SerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dt";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SP_TrackSerialNo_Project_Inverter(string CompanyID, string ProjectNo, string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_TrackSerialNo_Project_Inverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NoTraceStockInverter_StockItem(string companyID, string projectNo, string installerId, string locationId, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "NoTraceStockInverter_StockItem";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@companyID";
        if (companyID != string.Empty)
            param.Value = companyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectNo";
        if (projectNo != string.Empty)
            param.Value = projectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerID";
        if (installerId != string.Empty)
            param.Value = installerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (locationId != string.Empty)
            param.Value = locationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NoTraceStockInverter_Project(string companyID, string projectNo, string installerId, string locationId, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "NoTraceStockInverter_Project";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@companyID";
        if (companyID != string.Empty)
            param.Value = companyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectNo";
        if (projectNo != string.Empty)
            param.Value = projectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerID";
        if (installerId != string.Empty)
            param.Value = installerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (locationId != string.Empty)
            param.Value = locationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NoTraceStockInverter_SerialNo(string companyID, string projectNo, string installerId, string locationId, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "NoTraceStockInverter_SerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@companyID";
        if (companyID != string.Empty)
            param.Value = companyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectNo";
        if (projectNo != string.Empty)
            param.Value = projectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerID";
        if (installerId != string.Empty)
            param.Value = installerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (locationId != string.Empty)
            param.Value = locationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable NoTraceStockInverter(string companyID, string projectNo, string installerId, string locationId, string dateType, string startDate, string endDate, string managerId, string pendingAuditYN, string ScanReq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "NoTraceStockInverterNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@companyID";
        if (companyID != string.Empty)
            param.Value = companyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectNo";
        if (projectNo != string.Empty)
            param.Value = projectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerID";
        if (installerId != string.Empty)
            param.Value = installerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (locationId != string.Empty)
            param.Value = locationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@managerId";
        if (managerId != string.Empty)
            param.Value = managerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pendingAuditYN";
        if (pendingAuditYN != string.Empty)
            param.Value = pendingAuditYN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@ScanReq";
        if (ScanReq != string.Empty)
            param.Value = ScanReq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string tbl_BSAllSerialInverterSerialNo_Insert(string projectNo, string serialNo, string companyId, string categoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BSAllSerialInverterSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectNo";
        if (projectNo != null)
            param.Value = projectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@serialNo";
        if (serialNo != null)
            param.Value = serialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@companyId";
        if (companyId != null)
            param.Value = companyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@categoryId";
        if (categoryId != null)
            param.Value = categoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string SerialNoNotes_Insert(string SerialNo, string Notes, string CatId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SerialNoNotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != null)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != null)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CatId";
        if (CatId != null)
            param.Value = CatId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
            result = ex.Message;
        }
        return result;
    }

    public static DataTable tblTransportDepo_GetAllActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTransportDepo_GetAllActive";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@companyID";
        //if (companyID != string.Empty)
        //    param.Value = companyID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_InstallerNotes_Update(string id, string StockWith, string TransportDepo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_InstallerNotes_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@StockWith";
        if (StockWith != string.Empty)
            param.Value = StockWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportDepo";
        if (TransportDepo != string.Empty)
            param.Value = TransportDepo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}