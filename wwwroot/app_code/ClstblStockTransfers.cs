using System;
using System.Data;
using System.Data.Common;

public struct SttblStockTransfers
{
    public string StockTransferNumber;
    public string StockCodeXX;
    public string TransferQtyXX;
    public string LocationFrom;
    public string LocationTo;
    public string TransferDate;
    public string TransferBy;
    public string ReceivedDate;
    public string ReceivedBy;
    public string TransferNotes;
    public string TrackingNumber;
    public string Received;

    public string FromLocation;
    public string ToLocation;
    public string TransferByName;
    public string ReceivedByName;
    public string ReceivedByDate;
    public string ReceiveByUserName;
    public string TransferCompanyID;
    public string Amount;
    public string CompanyID;
    public string Location;
}

public class ClstblStockTransfers
{
    public static SttblStockTransfers tblStockTransfers_SelectByStockTransferID(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_SelectByStockTransferID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockTransfers details = new SttblStockTransfers();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.StockTransferNumber = dr["StockTransferNumber"].ToString();
            details.StockCodeXX = dr["StockCodeXX"].ToString();
            details.TransferQtyXX = dr["TransferQtyXX"].ToString();
            details.LocationFrom = dr["LocationFrom"].ToString();
            details.LocationTo = dr["LocationTo"].ToString();
            details.TransferDate = dr["TransferDate"].ToString();
            details.TransferBy = dr["TransferBy"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.TransferNotes = dr["TransferNotes"].ToString();
            details.TrackingNumber = dr["TrackingNumber"].ToString();
            details.Received = dr["Received"].ToString();

            details.FromLocation = dr["FromLocation"].ToString();
            details.ToLocation = dr["ToLocation"].ToString();
            details.TransferByName = dr["TransferByName"].ToString();
            details.ReceivedByName = dr["ReceivedByName"].ToString();
            details.ReceivedByDate = dr["ReceivedByDate"].ToString();
            details.ReceiveByUserName = dr["ReceiveByUserName"].ToString();
            details.TransferCompanyID = dr["TransferCompanyID"].ToString();
            details.Amount = dr["Amount"].ToString();
            details.CompanyID = dr["CompanyID"].ToString();
            details.Location = dr["Location"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblStockTransfers_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockTransfers_Insert(string LocationFrom, string LocationTo, string TransferDate, string TransferBy, string ReceivedDate, string ReceivedBy, string TransferNotes, string TrackingNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferDate";
        if (TransferDate != string.Empty)
            param.Value = TransferDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferBy";
        if (TransferBy != string.Empty)
            param.Value = TransferBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedDate";
        if (ReceivedDate != string.Empty)
            param.Value = ReceivedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        if (ReceivedBy != string.Empty)
            param.Value = ReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferNotes";
        param.Value = TransferNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNumber";
        param.Value = TrackingNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockTransfers_Update(string StockTransferID, string LocationFrom, string LocationTo, string TransferDate, string TransferBy, string ReceivedDate, string ReceivedBy, string TransferNotes, string TrackingNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferDate";
        if (TransferDate != string.Empty)
            param.Value = TransferDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferBy";
        if (TransferBy != string.Empty)
            param.Value = TransferBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedDate";
        if (ReceivedDate != string.Empty)
            param.Value = ReceivedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        if (ReceivedBy != string.Empty)
            param.Value = ReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferNotes";
        param.Value = TransferNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNumber";
        param.Value = TrackingNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockTransfers_Delete(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockTransfers_UpdateReceived(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateReceived";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockTransfers_UpdateTransferNumber(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateTransferNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblStockTransfersGetDataBySearch(string TransferBy, string LocationFrom, string LocationTo, string Received, string startdate, string enddate, string DateType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfersGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TransferBy";
        if (TransferBy != string.Empty)
            param.Value = TransferBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Received";
        if (Received != string.Empty)
            param.Value = Received;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransfersGetDataBySearchNew(string stockitemModel,string StockTranferNumber, string TransferBy, string LocationFrom, string LocationTo, string Received, string startdate, string enddate, string DateType, string TransferedOrNot, string TransferCompanyID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfersGetDataBySearchNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockTranferNumber";
        if (StockTranferNumber != string.Empty)
            param.Value = StockTranferNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferBy";
        if (TransferBy != string.Empty)
            param.Value = TransferBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Received";
        if (Received != string.Empty)
            param.Value = Received;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferedOrNot";
        if (TransferedOrNot != string.Empty)
            param.Value = TransferedOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferCompanyID";
        if (TransferCompanyID != string.Empty)
            param.Value = TransferCompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransfersGetDataBySearchExcel(string stockitemModel, string StockTranferNumber, string TransferBy, string LocationFrom, string LocationTo, string Received, string startdate, string enddate, string DateType, string TransferedOrNot)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfersGetDataBySearchExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemModel";
        if (stockitemModel != string.Empty)
            param.Value = stockitemModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockTranferNumber";
        if (StockTranferNumber != string.Empty)
            param.Value = StockTranferNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferBy";
        if (TransferBy != string.Empty)
            param.Value = TransferBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Received";
        if (Received != string.Empty)
            param.Value = Received;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferedOrNot";
        if (TransferedOrNot != string.Empty)
            param.Value = TransferedOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static int tblStockTransfers_Exits_TrackingNumber(string TrackingNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Exits_TrackingNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TrackingNumber";
        param.Value = TrackingNumber;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblStockTransfers_SelectExits_TrackingNumber(string TrackingNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_SelectExits_TrackingNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TrackingNumber";
        param.Value = TrackingNumber;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    /* ====================== tblStockTransferItems ====================== */

    public static DataTable tblStockTransferItems_SelectQty(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_SelectQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransferItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransferItems_SelectByStockTransferID(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_SelectByStockTransferID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransferItemsGetDataBySearch(string StockCode, string StockTransferID)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblStockTransferItemsGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        if (StockCode != string.Empty)
            param.Value = StockCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static int tblStockTransferItems_Insert(string StockTransferID, string StockCode, string TransferQty)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        param.Value = StockCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferQty";
        param.Value = TransferQty;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockTransferItems_Delete(string StockTransferItemsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferItemsID";
        param.Value = StockTransferItemsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockTransferItems_DeletebyStockTransferID(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_DeletebyStockTransferID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblStockTransfers_report(string LocationFrom, string LocationTo, string startdate, string enddate, string DateType, string StockItem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_report";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockTransferItems_GetReportDetails(string StockCode, string startdate, string enddate, string DateType,
        string LocationFrom, string LocationTo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransferItems_GetReportDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        if (StockCode != string.Empty)
            param.Value = StockCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        if (LocationFrom != string.Empty)
            param.Value = LocationFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        if (LocationTo != string.Empty)
            param.Value = LocationTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockTransfers_UpdateReceived_revert(string StockTransferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateReceived_revert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblStockTransfers_Update_receive(string StockTransferID, string ReceivedDate, string ReceivedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_Update_receive";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        param.Value = StockTransferID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

      

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedDate";
        if (ReceivedDate != string.Empty)
            param.Value = ReceivedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        if (ReceivedBy != string.Empty)
            param.Value = ReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

      

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblTransferCompany_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTransferCompany_GetData";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockTransfers_UpdateTransferCompanyID(string StockTransferID, string TransferCompanyID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateTransferCompanyID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferCompanyID";
        if (TransferCompanyID != string.Empty)
            param.Value = TransferCompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return id;
    }

    public static int tblStockTransfers_UpdateAmount(string StockTransferID, string Amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateAmount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = 0;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return id;
    }

    public static int tblStockTransfers_UpdateCompanyID(string StockTransferID, string CompanyID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateCompanyID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return id;
    }

    public static int tblStockTransfers_UpdateLocation(string StockTransferID, string Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockTransfers_UpdateLocation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = null;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return id;
    }

    public static DataTable tblStockSerialNo_GetData_ByTransferID(string StockTransferID, string LocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_GetData_ByTransferID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferID";
        if (StockTransferID != string.Empty)
            param.Value = StockTransferID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        if (StockTransferID != string.Empty)
            param.Value = LocationID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblMaintainHistory_GetSerialNo_BySectionIDModuleName(string SectionID, string modulename)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_GetSerialNo_BySectionIDModuleName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SectionID";
        if (SectionID != string.Empty)
            param.Value = SectionID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@modulename";
        if (modulename != string.Empty)
            param.Value = modulename;
        else
            param.Value = null;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SP_GetSerial_ByStockTransferId(string stockTransferId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_GetSerial_ByStockTransferId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockTransferId";
        if (stockTransferId != string.Empty)
            param.Value = stockTransferId;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int ScannedItem(string CatId, string TransferID, string StockItemId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ScannedItem";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CatId";
        if (CatId != string.Empty)
            param.Value = CatId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@TransferID";
        if (TransferID != string.Empty)
            param.Value = TransferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = 0;
        try
        {
            returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return returnid;
    }
}