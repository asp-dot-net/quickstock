﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SendSMSData
/// </summary>
public class SendMessagedata
{
    [JsonProperty("MessageSid")]
    public string MessageSid { get; set; }

    [JsonProperty("AccountSid")]
    public string AccountSid { get; set; }

    [JsonProperty("PropertySid")]
    public string PropertySid { get; set; }

    [JsonProperty("From")]
    public string From { get; set; }

    [JsonProperty("To")]
    public string To { get; set; }

    [JsonProperty("Text")]
    public string Text { get; set; }

    [JsonProperty("DeliveryReceipt")]
    public bool DeliveryReceipt { get; set; }

    [JsonProperty("NumSegments")]
    public bool NumSegments { get; set; }

    [JsonProperty("Status")]
    public string Status { get; set; }

    [JsonProperty("Direction")]
    public string Direction { get; set; }

    [JsonProperty("Created")]
    public string Created { get; set; }

}

public class RootObject
{
    [JsonProperty("Message")]
    public SendMessagedata Message { get; set; }

    //public List<SendMessagedata> Message { get; set; }
}

public class RootObjectReplay
{
    [JsonProperty("Message")]
    public Dictionary<string, SendMessagedata> Message { get; set; }

    //public List<SendMessagedata> Message { get; set; }
}