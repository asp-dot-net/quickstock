﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct Sttbl_WholesaleOrders
{
    public string OrderNumber;
    public string WholesaleCategoryID;
    public string StockItemID;
    public string OrderQuantity;
    public string DateOrdered;
    public string ExpectedDelivery;
    public string Delivered;
    public string Cancelled;
    public string OrderedBy;
    public string ReceivedBy;
    public string Notes;
    public string CompanyLocationID;
    public string CustomerID;
    public string BOLReceived;
    public string upsize_ts;
    public string ReferenceNo;
    public string ActualDelivery;

    public string CompanyLocation;
    public string CompanyLocationState;
    public string Invoice;
    public string OrderedName;
    public string InvoiceNo;
    public string vendor;
    public string IsDeduct;
    public string StockDeductBy;
    public string StockDeductDate;
    public string StockDeductByUserId;
    public string JobTypeID;
    public string JobType;
    public string InvoiceAmount;
    public string PVDNumber;
    public string StatusID;
    public string Status;
    public string CustEmailFlag;
    public string DeliveryOption;
    public string DeliveryOptionID;

    public string TransportTypeId;
    public string InstallTypeId;
    public string SolarTypeId;
    public string InstallerNameId;
    public string JobStatusId;
    public string ConsignPerson;
    public string CreditStC;
    public string STCNumber;

    public string TransportTypeName;
    public string InstallTypeName;
    public string JobTypeName;
    public string SolarTypeName;
    public string InstallerName;
    public string StcValue;
    public string InvoiceTypeId;
    public string FrieghtCharge;
    public string CustFrieghtCharge;
    public string SlenergyReferenceNo;
    public string STCID;
    public string GB_STCStatusID;
    
    public string XeroInvoiceAmount;
    public string existsPO;
    public string Stc_Value_Excel;

}

public struct Sttbl_WholesaleOrderItems
{
    public string WholesaleOrderID;
    public string WholesaleOrderItemID;
    public string StockItemID;
    public string OrderQuantity;
    public string WholesaleOrderItem;
    public string WholesaleCode;
    public string WholesaleLocation;
    public string WholesaleOrderItemlist;


}
public class Clstbl_WholesaleOrders
{
    public static Sttbl_WholesaleOrders tbl_WholesaleOrders_SelectByWholesaleOrderID(String WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrders details = new Sttbl_WholesaleOrders();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OrderNumber = dr["OrderNumber"].ToString();
            details.WholesaleCategoryID = dr["WholesaleCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            //details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.DateOrdered = dr["DateOrdered"].ToString();
            details.ExpectedDelivery = dr["ExpectedDelivery"].ToString();
            details.Delivered = dr["Delivered"].ToString();
            details.Cancelled = dr["Cancelled"].ToString();
            details.OrderedBy = dr["OrderedBy"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.BOLReceived = dr["BOLReceived"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ReferenceNo = dr["ReferenceNo"].ToString();
            details.ActualDelivery = dr["ActualDelivery"].ToString();

            details.CompanyLocation = dr["CompanyLocation"].ToString();
            //details.CompanyLocationState = dr["CompanyLocationState"].ToString();
            //details.Invoice = dr["Invoice"].ToString();
            details.OrderedName = dr["OrderedName"].ToString();
            details.InvoiceNo = dr["InvoiceNo"].ToString();
            details.vendor = dr["vendor"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.StockDeductByUserId = dr["StockDeductByUserId"].ToString();
            details.JobTypeID = dr["JobTypeID"].ToString();
            details.JobType = dr["JobType"].ToString();
            details.InvoiceAmount = dr["InvoiceAmount"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.StatusID = dr["StatusID"].ToString();
            details.Status = dr["Status"].ToString();
            details.CustEmailFlag = dr["CustEmailFlag"].ToString();
            details.DeliveryOption = dr["DeliveryOption"].ToString();
            details.DeliveryOptionID = dr["DeliveryOptionID"].ToString();

            details.TransportTypeId = dr["TransportTypeId"].ToString();
            details.InstallTypeId = dr["InstallTypeId"].ToString();
            details.InstallerNameId = dr["InstallerNameId"].ToString();
            details.SolarTypeId = dr["SolarTypeId"].ToString();
            details.JobStatusId = dr["JobStatusId"].ToString();
            details.ConsignPerson = dr["ConsignPerson"].ToString();
            details.CreditStC = dr["CreditStC"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();


            details.TransportTypeName = dr["TransportTypeName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.InstallTypeName = dr["InstallTypeName"].ToString();
            details.SolarTypeName = dr["SolarTypeName"].ToString();
            details.JobTypeName = dr["JobTypeName"].ToString();
            details.StcValue = dr["StcValue"].ToString();
            details.InvoiceTypeId = dr["InvoiceTypeId"].ToString();
            details.FrieghtCharge = dr["FrieghtCharge"].ToString();
            details.CustFrieghtCharge = dr["CustFrieghtCharge"].ToString();
            details.STCID = dr["STCID"].ToString();
            details.GB_STCStatusID = dr["GB_STCStatusID"].ToString();

            //details.SlenergyReferenceNo = dr["SlenergyReferenceNo"].ToString();

            details.XeroInvoiceAmount = dr["xeroinvamt"].ToString();
            details.existsPO = dr["existsPO"].ToString();
            details.Stc_Value_Excel = dr["Stc_Value_Excel"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tbl_WholesaleOrders_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectBySearch(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string ReferenceNo, string State, string stockitem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tbl_WholesaleOrders_SelectBySearchNew(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string ReferenceNo, string State, string stockitem, string IsDeliveredOrNot,string TransportTypeId, string InstallTypeId,string InstallerNameId, string JobStatusId,string SolarTypeId, string JobTypeID, string DeliveryOptionID,string custEmailID,string EmpId,string invoiceType, string ConsignPerson, string Void, string readyToPickup)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectBySearchNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeliveredOrNot";
        if (IsDeliveredOrNot != string.Empty)
            param.Value = IsDeliveredOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
         comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@InstallTypeId";
        if (InstallTypeId != string.Empty)
            param.Value = InstallTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameId";
        if (InstallerNameId != string.Empty)
            param.Value = InstallerNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (@JobStatusId != string.Empty)
            param.Value = @JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SolarTypeId";
        if (SolarTypeId != string.Empty)
            param.Value = SolarTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOptionID";
        if (DeliveryOptionID != string.Empty)
            param.Value = DeliveryOptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@custemail";
        if (custEmailID != string.Empty)
            param.Value = custEmailID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        if (EmpId != string.Empty)
            param.Value = EmpId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceType";
        if (invoiceType != string.Empty)
            param.Value = invoiceType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ConsignPerson";
        if (ConsignPerson != string.Empty)
            param.Value = ConsignPerson;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Void";
        if (Void != string.Empty)
            param.Value = Void;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@readyToPickup";
        if (readyToPickup != string.Empty)
            param.Value = readyToPickup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectforTracker(string Delivered,
        string CustomerID, string startdate, string enddate, string DateType, string OrderNumber
        , string ReferenceNo, string State, string IsDeliveredOrNot,string 
        PVDNumber,string StatusID,string TransportTypeId,String InstallTypeId,String InstallerNameId,
        String SolarTypeId,String JobStatusId, string JobTypeID, string DeliveryOptionID,string EmpID,
        string IsApproved, string PaymentStatus)//,string IsPo
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "tbl_WholesaleOrders_SelectforTracker";
        comm.CommandText = "tbl_WholesaleOrders_SelectforTrackerNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeliveredOrNot";
        if (IsDeliveredOrNot != string.Empty)
            param.Value = IsDeliveredOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallTypeId";
        if (InstallTypeId != string.Empty)
            param.Value = InstallTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameId";
        if (InstallerNameId != string.Empty)
            param.Value = InstallerNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (JobStatusId != string.Empty)
            param.Value = @JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SolarTypeId";
        if (SolarTypeId != string.Empty)
            param.Value = SolarTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOptionID";
        if (DeliveryOptionID != string.Empty)
            param.Value = DeliveryOptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        if (EmpID != string.Empty)
            param.Value = EmpID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsApproved";
        if (IsApproved != string.Empty)
            param.Value = IsApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@PaymentStatus";
        if (PaymentStatus != string.Empty)
            param.Value = PaymentStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectBySearchExcel(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string ReferenceNo, string State, string stockitem, string IsDeliveredOrNot, string TransportTypeId, String InstallTypeId, string InstallerNameId, String JobStatusId, string SolarTypeId,string JobTypeID,string DeliveryOptionID,string EmpID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectBySearchExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeliveredOrNot";
        if (IsDeliveredOrNot != string.Empty)
            param.Value = IsDeliveredOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallTypeId";
        if (InstallTypeId != string.Empty)
            param.Value = InstallTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameId";
        if (InstallerNameId != string.Empty)
            param.Value = InstallerNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (JobStatusId != string.Empty)
            param.Value = JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SolarTypeId";
        if (SolarTypeId != string.Empty)
            param.Value = SolarTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOptionID";
        if (DeliveryOptionID != string.Empty)
            param.Value = DeliveryOptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        if (EmpID != string.Empty)
            param.Value = EmpID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleDeduct_SelectBySearch(string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string stockitem, string Historic)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleDeduct_SelectBySearch";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        if (Historic != string.Empty)
            param.Value = Historic;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectWarehouseAllocated(string IsDeduct, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectWarehouseAllocated";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWholesaleOrderItems_StockRevert(string IsDeduct, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrderItems_StockRevert";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if (RevertFlag != string.Empty)
            param.Value = RevertFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrders_Insert(String DateOrdered, String OrderedBy, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ReferenceNo, string ExpectedDelivery)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DateOrdered";
        param.Value = DateOrdered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderedBy";
        if(OrderedBy !=string.Empty)
        param.Value = OrderedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return id;


    }

    public static bool tbl_WholesaleOrders_Update_OrderNumber(string WholesaleOrderID, String OrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_OrderNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        param.Value = OrderNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_CustEmailFlag(string WholesaleOrderID, string CustEmailFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_CustEmailFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEmailFlag";
        param.Value = CustEmailFlag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateIsDeductStatus_QuickStock(string ProjectId, string PicklistId,string IsDeduct ,string UserID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateIsDeductStatus_QuickStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        param.Value = PicklistId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value =UserID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);

    }

    public static bool tbl_WholesaleOrders_OrderCompleteQuickStock(string OrderId,string UserID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_OrderComplete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderId";
        param.Value = OrderId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);

    }
    public static bool tbl_WholesaleOrders_Update_Cancelled(string WholesaleOrderID, String Cancelled)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Cancelled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Cancelled";
        param.Value = Cancelled;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Delivered(string WholesaleOrderID, string ReceivedBy, string Delivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Delivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        param.Value = ReceivedBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        param.Value = Delivered;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_ActualDelivery(string WholesaleOrderID, string ActualDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_ActualDelivery";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualDelivery";
        if (ActualDelivery != string.Empty)
            param.Value = ActualDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_IsDeduct_Date_User(string WholesaleOrderID, string IsDeduct, string StockDeductByUserId, string StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_IsDeduct_Date_User";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductByUserId";
        if (StockDeductByUserId != string.Empty)
            param.Value = StockDeductByUserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        if (StockDeductDate != string.Empty)
            param.Value = StockDeductDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update(string WholesaleOrderID, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ReferenceNo, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Delete(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_VendorInvoiceNo(string WholesaleOrderID, String InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_StcValue(string WholesaleOrderID, String StcValue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_StcValue";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StcValue";
        param.Value = StcValue;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Employee(string WholesaleOrderID, string EmpId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Employee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        param.Value = EmpId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);

    }

    public static bool tbl_WholesaleOrders_Update_DeliveryOption(string WholesaleOrderID, string JobTypeID, string JobType, string InvoiceAmount, string PVDNumber, string StatusID, string Status, string DeliveryOption, string DeliveryOptionID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_DeliveryOption";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobType";
        if (JobType != string.Empty)
            param.Value = JobType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceAmount";
        if (InvoiceAmount != string.Empty)
            param.Value = InvoiceAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOption";
        if (DeliveryOption != string.Empty)
            param.Value = DeliveryOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOptionID";
        if (DeliveryOptionID != string.Empty)
            param.Value = DeliveryOptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
         }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Newparameters(string WholesaleOrderID, string TransportTypeId, string InstallTypeId, string InstallerNameId, string SolarTypeId,  string JobStatusId, string ConsignPerson, string CreditSTC, string STCNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Newparameters";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallTypeId";
        if (InstallTypeId != string.Empty)
            param.Value = InstallTypeId;
        else
            param.Value = DBNull.Value;
            param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameId";
        if (InstallerNameId != string.Empty)
            param.Value = InstallerNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SolarTypeId";
        if (SolarTypeId != string.Empty)
            param.Value = SolarTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (JobStatusId != string.Empty)
            param.Value = JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ConsignPerson";
        if (ConsignPerson != string.Empty)
            param.Value = ConsignPerson;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreditSTC";
        if (CreditSTC != string.Empty)
            param.Value = CreditSTC;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

  


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_WholesaleOrders_Exits_VendorInvoiceNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Exits_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static int tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo(string WholesaleOrderID, string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_WholesaleOrders_ExistsByReferenceNo(string ReferenceNo, string invoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByReferenceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_WholesaleOrders_ExistsByInvoiceNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    /////////////////////////////////////////////////	tbl_WholesaleOrderItems

    public static Sttbl_WholesaleOrderItems tbl_WholesaleOrderItems_SelectByWholesaleOrderID(String WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrderItems details = new Sttbl_WholesaleOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WholesaleOrderID = dr["WholesaleOrderID"].ToString();
            details.WholesaleOrderItemID = dr["WholesaleOrderItemID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.WholesaleOrderItem = dr["WholesaleOrderItem"].ToString();
            details.WholesaleCode = dr["WholesaleCode"].ToString();
            details.WholesaleLocation = dr["WholesaleLocation"].ToString();
            details.WholesaleOrderItemlist = dr["WholesaleOrderItemlist"].ToString();

        }
        // return structure details
        return details;
    }



    public static Sttbl_WholesaleOrderItems tbl_WholesaleOrderItems_SelectByWholesaleOrderItemID(String WholesaleOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWholesaleOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrderItems details = new Sttbl_WholesaleOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WholesaleOrderID = dr["WholesaleOrderID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.WholesaleOrderItem = dr["WholesaleOrderItem"].ToString();
            details.WholesaleCode = dr["WholesaleCode"].ToString();
            details.WholesaleLocation = dr["WholesaleLocation"].ToString();
            details.WholesaleOrderItemlist = dr["WholesaleOrderItemlist"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tbl_WholesaleOrderItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Select_ByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectForPDF(string WholeSaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectForPDF";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholeSaleOrderID";
        param.Value = WholeSaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_WholesaleOrderItems_Select_SerialNo(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Select_SerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_Selectby_IdandLocation(string WholesaleOrderID, string CompanyLocationID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Selectby_Id&Location";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID(string WholesaleOrderID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrderItems_Insert(String WholesaleOrderID, String StockItemID, String OrderQuantity, String WholesaleOrderItem, String WholesaleLocation, string wholesaleorderPanelID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItem";
        if (WholesaleOrderItem != string.Empty)
            param.Value = WholesaleOrderItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 250;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleLocation";
        if (WholesaleLocation != string.Empty)
            param.Value = WholesaleLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@wholesaleorderPanelID";
        if (wholesaleorderPanelID != string.Empty)
            param.Value = wholesaleorderPanelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_WholesaleOrderItems_Update(string WholesaleOrderItemID, String WholesaleOrderID, String StockItemID, String OrderQuantity, String WholesaleOrderItem, String WholesaleCode, String WholesaleLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        param.Value = OrderQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItem";
        param.Value = WholesaleOrderItem;
        param.DbType = DbType.String;
        param.Size = 250;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleCode";
        param.Value = WholesaleCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleLocation";
        param.Value = WholesaleLocation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrderItems_Delete(string WholesaleOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tbl_WholesaleOrderItems_DeleteWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_DeleteWholesaleOrderID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrderItems_SelectQty(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrdersReport_Search(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location, string WholesaleCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleCategoryID";
        if (WholesaleCategoryID != string.Empty)
            param.Value = WholesaleCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrdersReport_ByLocSearch(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_getWholesaleItemState(string StockItemID, String State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_getWholesaleItemState";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    ///ORDER INSTALLED REPORT

    //public static DataTable tblOrderInstalledReport_Search(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblOrderInstalledReport_Search";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@CustomerID";
    //    if (CustomerID != string.Empty)
    //        param.Value = CustomerID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@startdate";
    //    if (startdate != string.Empty)
    //        param.Value = startdate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@enddate";
    //    if (enddate != string.Empty)
    //        param.Value = enddate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StockItemID";
    //    if (StockItemID != string.Empty)
    //        param.Value = StockItemID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@Location";
    //    if (Location != string.Empty)
    //        param.Value = Location;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    DataTable result = new DataTable();
    //    result = DataAccess.ExecuteSelectCommand(comm);
    //    try
    //    {
    //    }
    //    catch
    //    {
    //        // log errors if any
    //    }
    //    return result;
    //}

    public static DataTable tbl_WholesaleOrderInstalledReport_ByLocSearch(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderInstalledReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderInstalledReport_ByInstalled(string CustomerID, string startdate, string enddate, string StockItemID, String Location, string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderInstalledReport_ByInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrderItems_whole_Delete(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_whole_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblWholesaleStockItemInventoryHistory_Insert(String HeadId, String StockItemID, String CompanyLocationID, String CurrentStock, String Stock, String UserId, string WholesaleOrderID, string WholesaleOrderItemID, string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HeadId";
        param.Value = HeadId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentStock";
        if (CurrentStock != string.Empty)
            param.Value = CurrentStock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Stock";
        if (Stock != string.Empty)
            param.Value = Stock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        if (WholesaleOrderItemID != string.Empty)
            param.Value = WholesaleOrderItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblWholesaleStockItemInventoryHistory_SelectStock(string WholesaleOrderID, String InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_SelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWholesaleOrders_UpdateDeduct(string WholesaleOrderID, string StockDeductBy, string IsDeduct, String StockAllocationStore, String StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateDeduct";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        param.Value = StockAllocationStore;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        param.Value = StockDeductDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblWholesaleOrders_UpdateDeductDate(string WholesaleOrderID, String StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateDeductDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        param.Value = StockDeductDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblWholesaleStockItemInventoryHistory_UpdateRevert(string id, string RevertFlag, string RevertDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if (RevertFlag != string.Empty)
            param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        if (RevertDate != string.Empty)
            param.Value = RevertDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblWholesaleOrders_UpdateRevert(string WholesaleOrderID, string StockDeductBy, string IsDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblWholesaleStockItemInventoryHistory_WholesaleOrderID(string WholesaleOrderID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_WholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblWholesaleItemInventoryHistory_SelectStock(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleItemInventoryHistory_SelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWholesaleItemInventoryHistory_revert(string WholesaleOrderID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleItemInventoryHistory_revert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblWholesaleStockDeductSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String InvoiceNo, String WholesaleOrderID, String DeductedBy, string DeductedDate, string InventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockDeductSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedBy";
        if (DeductedBy != string.Empty)
            param.Value = DeductedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedDate";
        if (DeductedDate != string.Empty)
            param.Value = DeductedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        if (InventoryHistoryId != string.Empty)
            param.Value = InventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag(string InvoiceNo, string WholesaleOrderID, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWholesaleStockDeductSerialNo_UpdateRevert(string SerialNo, String Pallet, string WholesaleOrderID, string RevertDate, string RevertedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockDeductSerialNo_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        param.Value = RevertDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertedBy";
        param.Value = RevertedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockSerialNo_Select_ByWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Select_ByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockCategoryID";
        //param.Value = StockCategoryID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static bool tblStockSerialNo_DeleteByWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_DeleteByWholesaleOrderID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_UpdatePVDStatus(string WholesaleOrderID, string PVDNumber, string StatusID, string Status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdatePVDStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblTransportType_Select(string DeliveryIndex)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTransportType_Select";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DeliveryIndex";
        param.Value = DeliveryIndex;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblTransportType_Selectwithoutparameter()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTransportType_Selectwithoutparameter";


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblSolarType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSolarType_Select";
                     
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblInstallType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallType_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_select_WholeSaleEmp()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_select_WholeSaleEmp";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblJobStatus_Select(string Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblJobStatus_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Type";
        param.Value = Type;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbltblStockSerialNo_WholesaleorderCount(string WholesaleOrderID,string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbltblStockSerialNo_WholesaleorderCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_StockTransferCount(string StockTransferId, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_StockTransferCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockTransferId";
        param.Value = StockTransferId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrders_SelectbyID(string ProjectID)
    { 
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectbyID ";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tbl_WholesaleOrders_OrderComplete(string WholesaleOrderID, string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_OrderComplete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderId";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_UpdatePartialFlag(string ProjectId, string Pickid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_UpdatePartialFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        param.Value = Pickid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);

    }

    public static bool tbl_WholesaleOrders_UpdatePartialFlag (string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdatePartialFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderId";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_UpdateIsaaproved(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateIsaaproved";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderId";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_GetStcDetails(string ReferenceNo, string PVDNumber, string Status, string Applied_date, string Stc_Value_Excel, string StatusText, string NumberofPanels)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_GetStcDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if(ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Applied_date";
        if (Applied_date != string.Empty)
            param.Value = Applied_date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Stc_Value_Excel";
        if (Stc_Value_Excel != string.Empty)
            param.Value = Stc_Value_Excel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@Status";
        //if (Status != string.Empty)
        //    param.Value = Status;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@statustext";
        if (StatusText != string.Empty)
            param.Value = StatusText;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberofPanels";
        if (NumberofPanels != string.Empty)
            param.Value = NumberofPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }

        catch(Exception ex)
        {

        }

        return(result != -1);
    }

    public static int tbl_WholesaleOrders_ExistStcId(string Refno)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistStcId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Stcid";
        param.Value = Refno;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblCustomers_SelectByCustomerID(String CustomerID)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByCustomerID";



        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);

        return table;

    }

    public static bool tbl_WholesaleOrders_UpdatePoforTracker(string WholesaleOrderID, String Po)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdatePoforTracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsPo";
        param.Value = Po;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_WholesaleOrdersNotes_Insert(String Date, String Notes, String WholesaleOrderID)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersNotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Date";
        param.Value = Date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return id;
    }

    public static DataTable tbl_WholesaleOrdersNotes_SelectByWholesaleOrderId(string WholesaleOrderId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersNotes_SelectByWholesaleOrderId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderId";
        if (WholesaleOrderId != string.Empty)
            param.Value = WholesaleOrderId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tbl_WholesaleOrdersNotes_Update_userid(string id,string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersNotes_Update_userid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = UserId;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrdersNotes_DeleteByid(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersNotes_DeleteByid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_InvoiceType(string WholesaleOrderID, string InvoiceTypeId, string InvoiceType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_InvoiceType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceTypeId";
        param.Value = InvoiceTypeId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceType";
        param.Value = InvoiceType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_JobStatus(string WholesaleOrderID, string JobStatusId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_JobStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (JobStatusId != string.Empty)
            param.Value = JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblMaintainHistory_Select_WholesaleTotalRevertById(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_Select_WholesaleTotalRevertById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_Transporttracker_GetData(string OrderNo, string InvoiceType, string DateType, string StartDate, string EndDate, string Location, string JobType, string ReferenceNo, string DeliveryOption, string Customer, string Employee, string IsCustDelivered, string TransportTypeId, string ConsignPerson, string IsPaid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Transporttracker_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OrderNo";
        if (OrderNo != string.Empty)
            param.Value = OrderNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceType";
        if (InvoiceType != string.Empty)
            param.Value = InvoiceType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        

        param = comm.CreateParameter();
        param.ParameterName = "@JobType";
        if (JobType != string.Empty)
            param.Value = JobType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOption";
        if (DeliveryOption != string.Empty)
            param.Value = DeliveryOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Employee";
        if (Employee != string.Empty)
            param.Value = Employee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsCustDelivered";
        if (IsCustDelivered != string.Empty)
            param.Value = IsCustDelivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ConsignPerson";
        if (ConsignPerson != string.Empty)
            param.Value = ConsignPerson;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsPaid";
        if (IsPaid != string.Empty)
            param.Value = IsPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_Update_IsCustDelivered(string WholesaleOrderID, string IsCustDelivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_IsCustDelivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsCustDelivered";
        if (IsCustDelivered != string.Empty)
            param.Value = IsCustDelivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrders_Get_IsCustDeliveredByWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Get_IsCustDeliveredByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_Update_FrieghtCharge(string WholesaleOrderID, string FrieghtCharge)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_FrieghtCharge";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FrieghtCharge";
        if (FrieghtCharge != string.Empty)
            param.Value = FrieghtCharge;
        else
            param.Value = 0;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_CustFrieghtCharge(string WholesaleOrderID, string CustFrieghtCharge)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_CustFrieghtCharge";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFrieghtCharge";
        if (CustFrieghtCharge != string.Empty)
            param.Value = CustFrieghtCharge;
        else
            param.Value = 0;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_CustDeliveredNote(string WholesaleOrderID, string CustDeliveredNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_CustDeliveredNote";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustDeliveredNotes";
        if (CustDeliveredNotes != string.Empty)
            param.Value = CustDeliveredNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_IsPaidByWholesaleOrderID(string WholesaleOrderID, string IsPaid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_IsPaidByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsPaid";
        if (IsPaid != string.Empty)
            param.Value = IsPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tbl_WholesaleOrders_SelectByDraft()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectByDraft";
     
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblWholesaleOrders_UPdate_IsDraft(string WholesaleOrderID, String IsDraft)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UPdate_IsDraft";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDraft";
        param.Value = IsDraft;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_UpdateSubmit(string WholesaleOrderID, string StockDeductDate, string IsDeduct, string StockDeductByUserId, string Reason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateSubmit";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        if (StockDeductDate != string.Empty)
            param.Value = StockDeductDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductByUserId";
        if (StockDeductByUserId != string.Empty)
            param.Value = StockDeductByUserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblCustomer_GetDataByCompanyID()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_GetDataByCompanyID";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_tbl_WholesaleOrders_Insert_STC(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_tbl_WholesaleOrders_Insert_STC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtWholesaleOrdersSTC";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = -1;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static bool tbl_WholesaleOrderItems_UpdateUnitPrice(string WholesaleOrderID, string UnitPrice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_UpdateUnitPrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UnitPrice";
        if (UnitPrice != string.Empty)
            param.Value = UnitPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrders_SelectBySearchNewExcelUnitPrice(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string ReferenceNo, string State, string stockitem, string IsDeliveredOrNot, string TransportTypeId, string InstallTypeId, string InstallerNameId, string JobStatusId, string SolarTypeId, string JobTypeID, string DeliveryOptionID, string custEmailID, string EmpId, string invoiceType, string ConsignPerson)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectBySearchNewExcelUnitPrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeliveredOrNot";
        if (IsDeliveredOrNot != string.Empty)
            param.Value = IsDeliveredOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransportTypeId";
        if (TransportTypeId != string.Empty)
            param.Value = TransportTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@InstallTypeId";
        if (InstallTypeId != string.Empty)
            param.Value = InstallTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameId";
        if (InstallerNameId != string.Empty)
            param.Value = InstallerNameId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobStatusId";
        if (@JobStatusId != string.Empty)
            param.Value = @JobStatusId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SolarTypeId";
        if (SolarTypeId != string.Empty)
            param.Value = SolarTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOptionID";
        if (DeliveryOptionID != string.Empty)
            param.Value = DeliveryOptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@custemail";
        if (custEmailID != string.Empty)
            param.Value = custEmailID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        if (EmpId != string.Empty)
            param.Value = EmpId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceType";
        if (invoiceType != string.Empty)
            param.Value = invoiceType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ConsignPerson";
        if (ConsignPerson != string.Empty)
            param.Value = ConsignPerson;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_Update_STCID(string WholesaleOrderID, string STCID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_STCID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCID";
        if (STCID != string.Empty)
            param.Value = STCID;
        else
            param.Value = 0;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_GBSTCStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GBSTCStatus_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_UpdateGB_STCStatusID(string WholesaleOrderID, string StatusID, string Status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_UpdateGB_STCStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = null;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrders_getInvoiceNo()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_getInvoiceNo";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrders_Update_xeroinvamt(string wholesaleOrderId, string xeroinvamt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_xeroinvamt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@wholesaleOrderId";
        if (wholesaleOrderId != string.Empty)
            param.Value = wholesaleOrderId;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@xeroinvamt";
        if (xeroinvamt != string.Empty)
            param.Value = xeroinvamt;
        else
            param.Value = null;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable Sp_OutstandingReport(string customerId, string CreditAmount, string JobTypeID, string dateType, string startDate, string endDate, string isDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_OutstandingReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerId";
        if (customerId != string.Empty)
            param.Value = customerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreditAmount";
        if (CreditAmount != string.Empty)
            param.Value = CreditAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (isDeduct != string.Empty)
            param.Value = isDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Sp_OutstandingReportDetails(string customerId, string JobTypeID, string InvoiceNo, string dateType, string startDate, string endDate, string isDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_OutstandingReportDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerId";
        if (customerId != string.Empty)
            param.Value = customerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (isDeduct != string.Empty)
            param.Value = isDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tabl_WholesalePO_InsertUpdate(string invoiceNo, string POType, string POTypeText)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@POType";
        if (POType != string.Empty)
            param.Value = POType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@POTypeText";
        if (POTypeText != string.Empty)
            param.Value = POTypeText;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tabl_WholesalePO_getInvoiceNo()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_getInvoiceNo";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tabl_WholesalePO_Update_xeroAmt(string invoiceNo, string xeroAmt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Update_xeroAmt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = null;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@xeroAmt";
        if (xeroAmt != string.Empty)
            param.Value = xeroAmt;
        else
            param.Value = null;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tabl_WholesalePO_ExistsByInoviceNo(string invoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_ExistsByInoviceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = null;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tabl_WholesalePO_Delete_ByInvoice(string invoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Delete_ByInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_finalInvAmt(string WholesaleOrderID, string finalInvAmt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_finalInvAmt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@finalInvAmt";
        if (finalInvAmt != string.Empty)
            param.Value = finalInvAmt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tabl_WholesalePO_Update_gstValue(string invoiceNo, string gstValue)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tabl_WholesalePO_Update_gstValue";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@gstValue";
        if (gstValue != string.Empty)
            param.Value = gstValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_propertyType(string WholesaleOrderID, string propertyType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_propertyType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@propertyType";
        if (propertyType != string.Empty)
            param.Value = propertyType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_UsdRate_Insert(string USD, string AUD, string CreatedOn, string CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UsdRate_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@USD";
        if (USD != string.Empty)
            param.Value = USD;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AUD";
        if (AUD != string.Empty)
            param.Value = AUD;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (CreatedOn != string.Empty)
            param.Value = CreatedOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);
        
        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_WholesaleOrderItems_UpdateXeroPrice(string WholesaleOrderID, string XeroPrice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_UpdateXeroPrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@XeroPrice";
        if (XeroPrice != string.Empty)
            param.Value = XeroPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int Bulk_InsertUpdate_tbl_WholesaleOrders_XeroTotalAmt(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_tbl_WholesaleOrders_XeroTotalAmt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtInvoice";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string tbl_UsdRate_GetAud()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UsdRate_GetAud";

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_UnreconciledInvAmt_Insert(string CreatedBy, string CreatedOn)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UnreconciledInvAmt_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != null)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (CreatedOn != null)
            param.Value = CreatedOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {

        }
        return id;
    }

    public static bool tbl_UnreconciledInvAmt_Update(string Id, string InvoiceNo, string Amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UnreconciledInvAmt_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_UnreconciledInvAmt_Update_Type(string Id, string Type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UnreconciledInvAmt_Update_Type";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (Type != string.Empty)
            param.Value = Type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tbl_POInvoicePayment_Insert(string CreatedBy, string CreatedOn)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != null)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (CreatedOn != null)
            param.Value = CreatedOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = 0;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {

        }
        return id;
    }

    public static bool tbl_POInvoicePayment_Update(string Id, string InvoiceNo, string PaymentTypeId, string PaymentType, string TransferInvoiceNo, string Amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeId";
        if (PaymentTypeId != string.Empty)
            param.Value = PaymentTypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@PaymentType";
        if (PaymentType != string.Empty)
            param.Value = PaymentType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferInvoiceNo";
        if (TransferInvoiceNo != string.Empty)
            param.Value = TransferInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tbl_POInvoicePayment_GetDataByInvoiceNo(string InvoiceNo, string userId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_GetDataByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userId";
        if (userId != string.Empty)
            param.Value = userId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_POInvoicePayment_DeleteByID(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_DeleteByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_POInvoicePayment_SelectByID(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string GetSTCRemaining(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetSTCRemaining";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    //public static DataTable GetJobNumber(string OrganizationId)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "GetJobNumber";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@OrganizationId";
    //    if (OrganizationId != string.Empty)
    //        param.Value = OrganizationId;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    DataTable result = new DataTable();
    //    try
    //    {
    //        result = DataAccess.ExecuteSelectCommand(comm);
    //    }
    //    catch (Exception ex)
    //    {
    //        // log errors if any
    //    }
    //    return result;
    //}

    public static int Bulk_InsertUpdate_RefundJobs(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_RefundJobs";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtRefundJobs";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool UpdateToSP()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "UpdateToSP";

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception)
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable UnReconciledType_Get()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "UnReconciledType_Get";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_POInvoicePayment_Update_PaymentStatus(string Id, string PaymentStatus, string CreatedBy, string CreatedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_POInvoicePayment_Update_PaymentStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        if (Id != string.Empty)
            param.Value = Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentStatus";
        if (PaymentStatus != string.Empty)
            param.Value = PaymentStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedDate";
        if (CreatedDate != string.Empty)
            param.Value = CreatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrders_GetSTCTracker(string ReferenceNo, string PVDNumber, string StatusID, string DateType, string StartDate, string EndDate, string PaymentStatus, string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_GetSTCTracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentStatus";
        if (PaymentStatus != string.Empty)
            param.Value = PaymentStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_BDM_GetAll()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BDM_GetAll";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}