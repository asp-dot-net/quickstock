﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;

/// <summary>
/// Summary description for ClsDbData
/// </summary>
public class ClsDbData
{
    public ClsDbData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable QuickStock_GetSMtblProjectData()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_GetSMtblProjectData";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable QuickStock_GetSMtblStockItemsData()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "QuickStock_GetSMtblStockItemsData";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_InsertUpdate_tbl_SMProject(DataTable SMDt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_tbl_SMProject";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SMProject_Details";
        if (SMDt != null)
            param.Value = SMDt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static int USP_InsertUpdate_tbl_SMStockItems(DataTable SMDt1)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_tbl_SMStockItems";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@tbl_SMStockItems_Details";
        if (SMDt1 != null)
            param.Value = SMDt1;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static DataTable tblProjects_GetSMProjectStatus()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_GetSMProjectStatus";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_InsertUpdate_SM_tblProjectStatus()
    {
        DataTable dt = tblProjects_GetSMProjectStatus();

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_SM_tblProjectStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SM_tblProjectStatus";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = -1;
        try
        {
            id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static DataTable tblProjects_GetSMProjectID()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_GetSMProjectID";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_InsertUpdate_SM_tblProjectNos()
    {
        DataTable dt = tblProjects_GetSMProjectID();

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_SM_tblProjectNos";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SM_tblProjectNos";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);
        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        try
        {
            
        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static DataTable tblProjects_GetSMProjectDetails()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_GetSMProjectDetails";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_InsertUpdate_tbl_SMProjectsRVerification()
    {
        DataTable dt = tblProjects_GetSMProjectDetails();

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_tbl_SMProjectsRVerification";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SMProjectsRVerification_Details";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static DataTable tblProjects_GetData()
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_GetData";

        DataTable result = new DataTable();
        result = DataAccessSolarMiner.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_InsertUpdate_tblProjects_SM()
    {
        DataTable dt = tblProjects_GetData();

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_tblProjects_SM";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@tblProjects_SM";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    //======================Fatch Arise Table Data==============================
    public static DataTable tbl_PickListLog_GetAllProjectNumber()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_GetAllProjectNumber";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProject_GetDataByProjectNumber()
    {
        DbCommand comm = DataAccessAriseSolar.CreateCommand();
        comm.CommandText = "tblProject_GetDataByProjectNumber";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@ProjectNumber";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccessAriseSolar.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int USP_Bulk_InsertUpdate_tblProjectsArise(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_Bulk_InsertUpdate_tblProjectsArise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@tblProjectsArise";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    //==========================================================================

    //==========================================Update Stock Items=================================
    public static int Bulk_InsertUpdate_Arise_tblStockItems()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_Arise_tblStockItems";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int Bulk_InsertUpdate_SolarMiner_tblStockItems()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_SolarMiner_tblStockItems";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    //=============================================================================================

    public static DataTable tbl_APIFetchData_UtilitiesBySource(string APISource)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_APIFetchData_UtilitiesBySource";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@APISource";
        if (!string.IsNullOrEmpty(APISource))
            param.Value = APISource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int Bulk_InsertUpdate_tblSerialNoFromBSGB(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_tblSerialNoFromBSGB";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtSerialNo";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblSerialNoFromBSGB_DeleteByBSGBFlag(string BSGBFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSerialNoFromBSGB_DeleteByBSGBFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@BSGBFlag";
        param.Value = BSGBFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_APIFetchData_Utilities_Update(string APISource, string LastUpdatedOn)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_APIFetchData_Utilities_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@APISource";
        if (!string.IsNullOrEmpty(APISource))
            param.Value = APISource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LastUpdatedOn";
        if (!string.IsNullOrEmpty(LastUpdatedOn))
            param.Value = LastUpdatedOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int Bulk_InsertUpdate_tblSTCDetails(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_tblSTCDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtSTCDetails";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrders_Merge_STCStatus()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Merge_STCStatus";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockItemLocation_Update()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemLocation_Update";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjectNoYN_Update()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectNoYN_Update";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_STC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_STC";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(DataTable dt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dtInverterDetails";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string tbl_APIFetchData_UtilitiesLastUpdateDate(string APISource)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_APIFetchData_UtilitiesLastUpdateDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@APISource";
        if (!string.IsNullOrEmpty(APISource))
            param.Value = APISource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool Update_Flags()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_Flags";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@tblProjectsArise";
        //if (dt != null)
        //    param.Value = dt;
        //else
        //    param.Value = DBNull.Value;
        ////param.DbType = DbType.Object;
        //comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}