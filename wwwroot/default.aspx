<%@ Page Language="C#" MasterPageFile="~/admin/templates/loginmaster.master" AutoEventWireup="true"
    CodeFile="default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="dashboardlogin">
        <asp:Login ID="login1" runat="server" DestinationPageUrl="~/admin/adminfiles/dashboard.aspx" OnLoggedIn="Login1_LoggedIn" DisplayRememberMe="true">
            <LayoutTemplate>
                
            <div class="login-card card-block auth-body mr-auto ml-auto">
                            <form class="md-float-material">
                               
                                <div class="auth-box">
                                        <div class="text-center">
                                                <img src="images/logo_Login.png">
                                        </div>
                                    <div class="row m-b-20">
                                        <div class="col-md-12">
                                            <h3 class="txt-primary">Portal login</h3>
                                        </div>
                                    </div>
                                    <hr/>

                                    <div class="input-group"><span class="input-group-prepend"><label class="input-group-text"><i class="icofont icofont-ui-user"></i></label></span>
                                        <asp:TextBox ID="UserName" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="UserNameRequired" runat="server" Display="dynamic" ControlToValidate="UserName" ErrorMessage=""
                                            ValidationGroup="Login1"></asp:RequiredFieldValidator>
                                    <span class="md-line"></span>
                                    </div>
                                   
                                    <div class="input-group">

                                            <span class="input-group-prepend"><label class="input-group-text"><i class="icofont  icofont-lock"></i></label></span>
                                            <asp:TextBox ID="Password" runat="server" Display="dynamic" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator
                                                ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage=""
                                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                                        <span class="md-line"></span>
                                    </div>
                                    <div class="row m-t-25 text-left">
                                        <div class="col-12">
                                            <div class="checkbox-fade fade-in-primary d-">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                    <asp:CheckBox ID="RememberMe" runat="server" />
                                                    <span class="text">Remember me</span>
                                                </label>
                                            </div>
                                            <div class="forgot-phone text-right f-right">
                                                    <asp:HyperLink ID="HyperLink1" runat="server" >Forgot Password?</asp:HyperLink>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-30">
                                        <div class="col-md-12">
                                            <asp:Button ID="LoginButton" runat="server" CommandName="Login" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"
                                            Text="Sign In" ValidationGroup="Login1" />
                                        </div>
                                    </div>
                                
                                   
    
                                </div>
                            </form>
                            <!-- end of form -->
                        </div>




           
            </LayoutTemplate>
        </asp:Login>
    </div>
   <!---<div class="text-center copytext">2019 Copyright <%=SiteName %> </div>--> 
    <%-- <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=login1.FindControl("LoginButton").ClientID %>').click(function () {
                formValidate();
            });
        });
    </script>--%>
</asp:Content>