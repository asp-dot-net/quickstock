﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WholesaleAttachment.aspx.cs" Inherits="mailtemplate_WholesaleAttachment" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />   

</head>
<body>
    <div class="brdemail">
        <table cellpadding="0" cellspacing="0" width="600px" style="border: 1px solid #293245; margin: 0px; auto; display: block; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
            <tr>
                <td >
                    <asp:Image runat="server" ID="emailtop" />
                </td>
            </tr>
            <tr>
                <td>
                    <table id="mytable" border="0" cellpadding="5" align="center" cellspacing="0" style="border-collapse: collapse; width: 600px;">
                        <tr>
                            <td colspan="2" style="background-color: #293245; color: #ffffff; padding: 5px 15px;">
                                <b>Details as follow :</b>
                            </td>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">
                                Your order has been dispatched!<br/><br/>

                                Hello <b><asp:label runat="server" id="lblCustName"></asp:label></b>,<br />
                                
                                Thank you for your order. This email is to confirm that your order  <asp:label runat="server" id="lblWholesaleorderID"/> has been dispatched. See attached PDF below for details  <br />                                                    
                             
                            </td>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">
                                Thanks and Regards,
                                <br /><asp:label runat="server" id="lblSiteName"></asp:label>
                            </td>
                       
                        </tr>
                         
            <tr runat="server">
                <td style="background: #293245; color: #ffffff; text-align: center; vertical-align: middle; height: 55px; font-family: Verdana, Geneva, sans-serif; font-size: 12px;">Copyrights &copy; <asp:label runat="server" id="lblyear"></asp:label> Achievers Energy. All rights reserved.
                </td>
            </tr>
                        
        </table>
                    </td>
                </tr>
            </table>
    </div>
</body>
</html>
