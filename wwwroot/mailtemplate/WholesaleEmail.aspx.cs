﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_WholesaleEmail : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        //string WholesaleOrderID = "10049";
        string WholesaleOrderID = Request.QueryString["WholesaleOrderID"].ToString();
        lblWholeSaleOrder.Text = "WholeSale Order Detail for WholeSaleID:" + WholesaleOrderID.ToString();

        if (!IsPostBack)
        {
            DataTable dt = new DataTable();

            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("3", "", "", "", "", WholesaleOrderID, "", "", "", "", "", "");
            rptItems.DataBind();

        }


    }
}