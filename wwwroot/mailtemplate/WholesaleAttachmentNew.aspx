﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WholesaleAttachmentNew.aspx.cs" Inherits="mailtemplate_WholesaleAttachment" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />

</head>
<body>
    <div class="brdemail">
        <table cellpadding="0" cellspacing="0" width="600px" style="border: 1px solid #293245; margin: 0px; auto; display: block; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
            <tr>
                <td>
                    <%--<asp:Image runat="server" ID="emailtop" />--%>
                    <img src="~/image/AchieverEnergy.jpg" />
                </td>
            </tr>
            <tr>
                <td>
                    <table id="mytable" border="0" cellpadding="5" align="center" cellspacing="0" style="border-collapse: collapse; width: 600px;">
                        <tr>
                            <td colspan="2" style="background-color: #293245; color: #ffffff; padding: 5px 15px;">
                                <b>Details as follow :</b>
                            </td>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">Dear Sir/Madam,<br />
                                <br />


                                Please find the tracking details of your Invoice:
                                <asp:Label runat="server" ID="lblinvoicenumber"></asp:Label><asp:Label runat="server" ID="lblrefno"></asp:Label><br />
                                <br />

                                Tracking Details<br />
                                Transport company : <asp:Label runat="server" ID="lbltransportcomp"></asp:Label><br />
                                Tracking number : <asp:Label runat="server" ID="lbltrackingno"></asp:Label><br />
                                <br />

                                <div id="divrep" runat="server">
                                    The following items has been sent in this consignment.<br />

                                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered" style="border: #a2a0a0 solid 1px; margin-bottom: 15px;">
                                        <tr>
                                            <th align="left" style="background: #ddd; border-bottom: #a2a0a0 solid 1px;">Category</th>
                                            <th align="left" align="center" style="background: #ddd; border-bottom: #a2a0a0 solid 1px;">Stock Item</th>
                                            <th align="left" align="center" style="background: #ddd; border-bottom: #a2a0a0 solid 1px;">Serial No</th>
                                        </tr>

                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <tr style="padding: 5px 10px;">
                                                    <td align="left">
                                                        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("StockCategory") %>'></asp:Label></td>
                                                   
                                                    <td align="left">
                                                        <asp:Label ID="lblItem" runat="server" Text='<%#Eval("StockItem") %>'></asp:Label></td>
                                               
                                                    <td align="left">
                                                        <asp:Label ID="lblserialno" runat="server" Text='<%#Eval("SerialNo") %>'></asp:Label></td>
                                              
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <br />
                                    </table>
                                </div>

                                <asp:Label ID="lblerrormsg" runat="server" Visible="false">Serial No not found</asp:Label>
                            </td>
                            <tr class="" style="padding: 5px 10px; display: block;">
                                <td>If any additional information is required regarding this consignment then please contact 
                                <br />
                                    Jerry (Transport) : 0450164994
                                <br />
                                    Prit (Orders and accounts) : 0424902085
                                </td>
                            </tr>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">Thanks and Regards,
                                <br />
                                <asp:Label runat="server" ID="lblSiteName" Text="Achievers Energy."></asp:Label>
                            </td>

                        </tr>

                        <tr runat="server">
                            <td style="background: #293245; color: #ffffff; text-align: center; vertical-align: middle; height: 55px; font-family: Verdana, Geneva, sans-serif; font-size: 12px;">Copyrights &copy;
                                <asp:Label runat="server" ID="lblyear"></asp:Label>
                                Achievers Energy. All rights reserved.
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
