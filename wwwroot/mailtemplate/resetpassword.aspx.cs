﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class mailtemplate_resetpassword : System.Web.UI.Page
{
    protected string SiteURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
        SiteURL = st.siteurl;
        SiteURL = SiteURL + (SiteURL.EndsWith("/") ? "" : "/");

        emailtop.ImageUrl = SiteURL + "userfiles/emailtop/" + st.emailtop;
        emailbottom.ImageUrl = SiteURL + "userfiles/emailbottom/" + st.emailbottom;
    }
}