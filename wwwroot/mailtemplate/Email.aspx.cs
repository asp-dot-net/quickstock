﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_Email: System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        //string[] arg = new string[2];
        //arg = Request.QueryString["ProjectNo"].ToString().Split('&');
        //string ProjectNo = "800315";

        //string PicklistId = "50";
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string PicklistId = Request.QueryString["PickList"].ToString();
        lblprojectNo.Text = " Stock Order Detail for Project Number:" + ProjectNo.ToString();
        //string ProjectNo= Request.QueryString["ProjectNo"];
        //string PicklistId = Request.QueryString["PicklistId"];

        if (!IsPostBack)
        {
            //DataTable dt = new DataTable();

            if (ProjectNo != "0" && PicklistId != "0")
            {

                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("1", "", "", "", "", ProjectNo, "", "", PicklistId, "", "", "");
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("1", "", "", "", "", ProjectNo, "", "", "", "", "", "");
                rptItems.DataBind();
            }


            }

       //emailtop.ImageUrl = SiteURL + "/images/logo_Login.png";
    }
}