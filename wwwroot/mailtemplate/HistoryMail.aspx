﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HistoryMail.aspx.cs" Inherits="mailtemplate_HistoryMail" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <style>
        .table_border{
            border: 1px solid #333;
        }
    </style>
</head>
<body>
    <form runat="server">
    <div class="brdemail">
        <table cellpadding="10" class="table_border" cellspacing="0" width="600" style="border: 1px solid #333; margin: 0px; auto; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
            <tr>
                <td height="80" valign="middle" style="border-bottom:#666 solid 1px; padding-left:10px;">
                    <%--<asp:image runat="server" id="emailtop" />--%>
                    <%--<img src="../images/logo_Login.png" style="margin-top:3px;" />--%>
                </td>
            </tr>
            <tr>
                <td style="padding:20px 10px 10px 10px;"><asp:label runat="server" ID="lblprojectNo">
                   
                                                         </asp:label>  </td>
            </tr>
            <tr>
                <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="7" class="table table-bordered" style="border:#a2a0a0 solid 1px; margin-bottom:15px;">
                                                <tr>
                                                      <th height="25" width="60" align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Index No.</th>
                                                    <th height="25" width="60" align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">ProjeNumber</th>
                                                    <th width="25%" align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">WholesaleOrderID</th>
                                                    <th width="15%" align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Category Name</th>
                                                    <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Serial No</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Pallet No</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Item Name</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Message</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">User Name</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">Created Date</th>
                                                     <th align="center" style="background:#ddd; border-bottom:#a2a0a0 solid 1px;">ModuleName</th>

                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>

                                                            <td align="center"><%#Container.ItemIndex+1 %></td>
                                                            <td align="center">
                                                                <asp:Label ID="lblProjectno" runat="server"><%#Eval("ProjNumber") %></asp:Label></td>
                                                            <td align="center">
                                                                <asp:Label ID="lblWholesaleorderid" runat="server"><%#Eval("WholesaleOrderID") %></asp:Label></td>
                                                            <td align="center">
                                                                <asp:Label ID="lblCategoryname" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                             <td align="center">
                                                                <asp:Label ID="Label1" runat="server"><%#Eval("SerailNo") %></asp:Label></td>
                                                              <td align="center">
                                                                <asp:Label ID="Label6" runat="server"><%#Eval("Pallet") %></asp:Label></td>
                                                             <td align="center">
                                                                <asp:Label ID="Label2" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                             <td align="center">
                                                                <asp:Label ID="Label3" runat="server"><%#Eval("Message") %></asp:Label></td>
                                                             <td align="center">
                                                                <asp:Label ID="Label4" runat="server"><%#Eval("UserName") %></asp:Label></td>
                                                             <td align="center">
                                                                <asp:Label ID="Label5" runat="server"><%#Eval("CreatedDate") %></asp:Label></td>
                                                            <td align="center">
                                                                <asp:Label ID="Label7" runat="server"><%#Eval("modulename") %></asp:Label></td>
                                                                  <td align="center">
                                                       <%--         <asp:Label ID="Label6" runat="server"><%#Eval("CreatedBy") %></asp:Label></td>--%>

                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                   <%-- <div class="table-responsive BlockStructure">
                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                           AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                            <Columns>
                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                  <%--      <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")%></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Serial.No" SortExpression="serial.No">
                                    <ItemTemplate>                                     
                                        <asp:Label ID="Lblserialno" runat="server" Width="30px">
                                            <%#Eval("SerialNo")%></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                 <asp:TemplateField HeaderText="CategoryName">
                                    <ItemTemplate>                                     
                                        <asp:Label ID="lblcategory" runat="server" Width="30px">
                                            <%#Eval("CategoryName")%></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="StockItem">
                                    <ItemTemplate>                                     
                                        <asp:Label ID="lblstockitem" runat="server" Width="30px">
                                            <%#Eval("StockItem")%></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>



                            </Columns>

                        </asp:GridView>

                    </div>--%>
                </td>
            </tr>             
        </table>
    </div>
        </form>
</body>
</html>
