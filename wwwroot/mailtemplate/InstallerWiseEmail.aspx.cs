﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_Email: System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        //emailtop.ImageUrl = SiteURL + "images/" + "logo_Login.jpg";
        //string[] arg = new string[2];
        //arg = Request.QueryString["ProjectNo"].ToString().Split('&');

        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string PicklistId = Request.QueryString["PickList"].ToString();      
        string MAiltype= Request.QueryString["MAiltype"].ToString();
        string WholesaleOrderID= Request.QueryString["WholesaleOrderID"].ToString();
        if (!IsPostBack)
        {
            //DataTable dt = new DataTable();
            if (MAiltype == "PickList")
            {
                lblprojectNo.Text = "Project Detail For Project Number Is:" + ProjectNo;
                if (ProjectNo != "0" && PicklistId != "0")
                {

                    rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                    rptItems.DataBind();
                }
                else
                {
                    rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                    rptItems.DataBind();
                }
            }
            else
            {
                lblprojectNo.Text = "Wholesale Detail For Wholesaleid Is:" + WholesaleOrderID;
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
                rptItems.DataBind();
            }
        }
        //emailbottom.ImageUrl = SiteURL + "userfiles/emailbottom/" + st.emailbottom;
    }
}