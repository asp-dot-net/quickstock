﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_HistoryMail : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string ProjNo = Request.QueryString["ProjectNumber"];
        string Wholesaleorderid = Request.QueryString["WholesaleOrderID"];
        string serialno = Request.QueryString["SerailNo"];
        string Modulename = Request.QueryString["Modulename"];
        if (!IsPostBack)
        {

            rptItems.DataSource = Reports.SerialNoHistoryReport(ProjNo, Wholesaleorderid, serialno, Modulename);
            rptItems.DataBind();



        }

        //emailtop.ImageUrl = SiteURL + "/images/logo_Login.png";
    }
}