﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_WholesaleAttachment : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string WholesaleOrderID = Request.QueryString["WholesaleorderID"];


        if (!IsPostBack)
        {
            Sttbl_WholesaleOrders st2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
            SttblContacts st3 = ClstblContacts.tblContacts_SelectByCustomerID(st2.CustomerID);
            
            lblCustName.Text = st3.ContFirst+" "+st3.ContLast;
            lblWholesaleorderID.Text = WholesaleOrderID;
            lblSiteName.Text = ConfigurationManager.AppSettings["SiteName"].ToString() + "."; 
            //lblSiteName2.Text = ConfigurationManager.AppSettings["SiteName"].ToString();
            lblyear.Text = Convert.ToString(DateTime.Now.Year);
        }
        emailtop.ImageUrl = SiteURL + "images/" + "AchieverEnergy.jpg";
        //emailbottom.ImageUrl = SiteURL + "userfiles/emailbottom/" + st.emailbottom;
    }
}