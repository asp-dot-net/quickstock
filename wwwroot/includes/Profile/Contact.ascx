﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contact.ascx.cs" Inherits="includes_Profile_Contact" %>
<div id="contacts" class="tab-pane">
    <div class="row">
        <div class="col-md-6">
            <div class="profile-contacts">

                <div class="profile-badge orange"><i class="fa fa-phone orange"></i><span>Contacts</span></div>
                <div class="contact-info">
                    <p>
                        Phone	: +1 1 2345 6789
                        <br>
                        Cell		: +1 9 876 5432
                    </p>
                    <p>
                        Email		: kim@gmail.com
                                                                        <br>
                        Skype		: kim.ryder
                    </p>
                    <p>
                        Facebook	: facebook.com/Kim.Ryder
                                                                        <br>
                        Twitter	: @KimRyder
                    </p>
                </div>
                <div class="profile-badge azure">
                    <i class="fa fa-map-marker azure"></i><span>Location</span>
                </div>
                <div class="contact-info">
                    <p>
                        Address<br>
                        Department 98<br>
                        44-46 Morningside Road<br>
                        Toronto, Canada
                    </p>
                    <p>
                        Office<br>
                        44-46 Morningside Road<br>
                        Toronto, Canada
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="contact-map" class="animated flipInY"></div>
        </div>
    </div>
</div>
