﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_Profile_Contact : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        BindSetting();
    }
    public void BindSetting()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        txtname.Text = st.EmpFirst + " " + st.EmpLast;
        txtfamily.Text = string.Empty;
        txtphone.Text = st.EmpPhone;
        txtmobile.Text = st.EmpMobile;
        txtbirthdate.Text = st.DateOfBirth;
       
    }
}