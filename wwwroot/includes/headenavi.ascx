﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="headenavi.ascx.cs" Inherits="includes_headenavi" %>
<%--<div class="clearfix wrapper dk nav-user hidden-xs">--%>
<%--<div class="dropdown">--%>
<%--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="thumb avatar pull-left m-r">--%>
<%--<img src="images/a0.png" class="dker" alt="...">--%>
<%-- <asp:Image runat="server" ID="imguser" ImageUrl="~/images/crm.png" />
        <br />--%>
<%--<asp:Image runat="server" ID="Image1" ImageUrl="~/images/crm_img.jpg" />--%>
<%--<i class="on md b-black"></i>--%><%--</span><span class="hidden-nav-xs clearone"><span class="block m-t-xs"><strong class="font-bold text-lt">--%><%--<asp:Label ID="lblusername" runat="server"></asp:Label>--%><%--Customer Relationship Management</strong>--%> <%--<b class="caret"></b>--%><%--</span>--%><%--<span class="text-muted text-xs block">Art Director</span>--%> <%--</span></a>--%>
<%--<ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><span class="arrow top hidden-nav-xs"></span><a href="#">Settings</a> </li>
            <li><a href="profile.html">Profile</a> </li>
            <li><a href="#"><span class="badge bg-danger pull-right">3</span> Notifications </a></li>
            <li><a href="docs.html">Help</a> </li>
            <li class="divider"></li>
            <li><a href="modal.lockme.html" data-toggle="ajaxModal">Logout</a> </li>
        </ul>--%>
<%--</div>--%>
<%--</div>--%>
<nav class="nav-primary hidden-xs">
    <ul class="nav nav-main" data-ride="collapse">
        <li id="lidashboard" runat="server"><a href="<%=SiteURL%>admin/adminfiles/dashboard.aspx" class="auto"><i class="i icon dashboard">
            <!-- <img src="<%=SiteURL%>images/icon_dashboard.png" width="18" height="9">-->
        </i><span>Dashboard</span> </a></li>
        <li id="limaster" runat="server" visible="false"><a class="auto"><i class="i icon master">
            <!--<img src="<%=SiteURL%>images/icon_master.png">-->
        </i><span>Master</span> <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk  always-visible" id="subnavmain">
                <li id="licompanylocations" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/companylocations.aspx" class="auto"><i class="i i-dot"></i><span>Company Locations</span> </a></li>
            </ul>
        </li>
      
        <li id="listock" runat="server" visible="false"><a class="auto"><i class="i icon stock">
            <!--<img src="<%=SiteURL%>images/icons/icon_stock.png">-->
        </i><span>Stock Item</span> <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk always-visible">
                <li id="listockitem" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockitem.aspx" class="auto"><i class="i i-dot"></i><span>Stock Item</span> </a></li>
                <li id="listocktransfer" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stocktransfer.aspx" class="auto"><i class="i i-dot"></i><span>Stock Transfer</span> </a></li>
                <li id="listockorder" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockorder.aspx" class="auto"><i class="i i-dot"></i><span>Stock Order</span> </a></li>
                <li id="listockdeduct" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockdeduct.aspx" class="auto"><i class="i i-dot"></i><span>Stock Deduct</span> </a></li>
                <li id="listockusage" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/stockusage.aspx" class="auto"><i class="i i-dot"></i><span>Stock Usage</span> </a></li>
                <li id="listocktransferreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/stocktransferreportdetail.aspx" class="auto"><i class="i i-dot"></i><span>Stock Transfer Report</span> </a></li>
                <li id="listockorderreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/stockorderreport.aspx" class="auto"><i class="i i-dot"></i><span>Stock Order Report</span> </a></li>
                <li id="listockreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/stockreport.aspx" class="auto"><i class="i i-dot"></i><span>Stock Report</span> </a></li>

            </ul>
        </li>
        
          <li id="licalendar" runat="server"><a href="<%=SiteURL%>admin/adminfiles/calendar/stockcalendar.aspx" class="auto"><i class="">
            <!-- <img src="<%=SiteURL%>images/icon_dashboard.png" width="18" height="9">-->
        </i><span>Calendar</span> </a></li>
      
    </ul>
    <div class="line dk hidden-nav-xs"></div>
    <%-- <div class="text-muted text-xs hidden-nav-xs padder m-t-sm m-b-sm">Contacts</div>--%>
    <ul class="nav">
        <li id="li1" runat="server"><a href="<%=SiteURL%>admin/adminfiles/contacts/allcontacts.aspx" class="auto"><i class="i i-circle-sm text-info-dk"></i><span>Contacts</span> </a></li>
    </ul>
    <ul class="nav brdnonebtm">
        <%-- <li><a href="#"><i class="i i-circle-sm text-danger-dk"></i><span>Activity Stream</span> </a></li>
        <li><a href="conversation.html"><i class="i i-circle-sm text-info-dk"></i><span>Conversation</span> </a></li>
        <li><a href="calendar.html"><i class="i i-circle-sm text-warning-dk"></i><span>Calendar</span> </a></li>
        <li><a href="#"><i class="i i-circle-sm text-muted-dk"></i><span>My Task</span> </a></li>--%>
    </ul>
    <div class="navicopy">
        <span class="copytext iconmegh">&copy; 2014 Euro Solar. All rights reserved.<br>
            Designed by: <a href="http://www.meghtechnologies.com/" target="_blank">&nbsp;</a></span>
    </div>
</nav>
