﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_revertitem : System.Web.UI.UserControl
{
    protected string jschart1;
    protected string endValue;
    protected string SiteURL;
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
        SiteURL = st1.siteurl;
        if (!IsPostBack)
        {
            BindGrid(0);
          
        }
    }
    protected DataTable GetGridData()
    {
        //String active = ddlActive.SelectedValue;

        DataTable dt = ClstblrevertItem.tblRevertItem_SelectData();

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {              
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {            
            PanGrid.Visible = false;          
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();           
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName == "set")
        {
            ClstblrevertItem.tblRevertItem_UpdateIsRevert(id);
        }
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

    }
}