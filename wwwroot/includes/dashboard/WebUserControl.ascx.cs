﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_WebUserControl : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        BindGrid();
    }

    public void BindPage()
    {
        BindDropdown();
        BindGrid();
    }

    public void BindGrid()
    {
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dt = ClsDashboard.tblStockItemsLocation_Dashboard_GetData(ddlLocation.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //divdeprec.Attributes.Add("style", "overflow-y:scroll; height:350px;");
            PanNoRecord.Visible = false;
        }
        else
        {
            GridView1.Visible = false;
            PanNoRecord.Visible = true;
        }
    }

    public void BindDropdown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select();

        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}