﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="revertitem.ascx.cs" Inherits="includes_dashboard_revertitem" %>

<asp:UpdatePanel runat="server" ID="UpdateLead">   
    <ContentTemplate>  
        <div class="col-md-12">           
            <div class="panlenew">
                 <strong><h4>Revert Item Detail</h4></strong>
                <section class="panel panelwidth">
                    <div class="panel-body paddnone">
                        <div class="imgresponsive weeklystatus row">
                            <div class="col-md-12 col-sm-12 paystat">
                                <div class="hpanel hbgblue">
                                    <div class="panel-body">
                                        <div class="finalgrid">
                                            <div class="table-responsive printArea tableWidth">
                                                <div id="PanGrid" runat="server" oncopy="return false">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" CssClass="tooltip-demo text-center table table-bordered"
                                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                            OnRowCreated="GridView1_RowCreated" OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                         <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectId") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                                      <%--  <%#Eval("ProjectNumber")%> --%>                                                                       
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="brdnoneleft" />
                                                                    <HeaderStyle CssClass="brdnoneleft" />
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Category" ItemStyle-Width="100px" HeaderStyle-Width="100px" Visible="true" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                    <asp:Label ID="lblcategory" runat="server" Text='<%# Eval("CategoryID").ToString()=="1"?"Module":"Inverter"%>'></asp:Label>    
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Item Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%#Eval("ItemName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Revert Qty" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top"
                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="left" >
                                                                    <ItemTemplate>
                                                                        <%#Eval("OrderQty")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>       
                                                                
                                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="btnset" runat="server" CssClass="btn btn-inverse btn-mini" CommandName="set" data-original-title="set" data-toggle="tooltip" data-placement="top"
                                                                                OnClientClick="return confirm('Are you sure you Revert item?');"
                                                                                CommandArgument='<%#Eval("id")%>' CausesValidation="false">
                                                    <i class="fa fa-retweet"></i> IsRevert
                                                                            </asp:LinkButton> 
                                                                     <%--   <span class="btn btn-primary btn-xs"><%#Eval("Active").ToString()=="True"?"<i class='fa fa-check-square-o'></i>":"<i class='fa fa-square-o'></i>"%>
                                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                                            </asp:LinkButton> --%>                                                                        
                                                                           <%-- <asp:LinkButton ID="lbtnReset" runat="server" CssClass="btn btn-blue btn-xs" CommandName="Reset" data-original-title="Reset Password" data-toggle="tooltip" data-placement="top"
                                                                                OnClientClick="return confirm('Are you sure you want to change password?');"
                                                                                CommandArgument='<%#Eval("CompanyLocationID")%>' CausesValidation="false">
                                                    <i class="fa fa-refresh"></i> Reset
                                                                            </asp:LinkButton>    --%>                                                                      
                                                                           <%-- <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("CompanyLocationID")%>'>
                                                                         <i class="fa fa-trash"></i> Delete
                                                                            </asp:LinkButton>  --%>                                                                     
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <AlternatingRowStyle />
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>
                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopage">
                                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    function doMyAction() {
        if ($.fn.select2 !== 'undefined') {
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
        }
    }
</script>
