﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebUserControl.ascx.cs" Inherits="includes_dashboard_WebUserControl" %>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }

    .table_row_red {
        border-width: 3px;
        padding-bottom: 10px;
        font-size: 16px;
    }

    .table_row_normal {
        font-size: 15px;
    }


    .header_orage{display:flex; flex-direction:row; justify-content:space-between; align-items:center; background:#fb6e52; padding:10px; }
    .hd1{width:60%; font-size:20px; color:#fff!important;}
    .shd_right{width:40%; display:flex; flex-direction:row; flex-wrap; justify-content:flex-end;}
    .box_right{margin-left:15px;}
    .width150{min-width:160px;}
</style>

<div class="col-md-12">
    <div class="header_orage">
        <div class="hd1">Stock Details</div>
        <div class="shd_right">
            <div class="box_right width150">
                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                            <asp:ListItem Value="">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
            <div class="box_right"><asp:Button runat="server" ID="btnGo" CssClass="btn btn-primary btn-xs" Text="Go" OnClick="btnGo_Click"/></div>
        </div>
    </div>
    
    <div class="col-md-12" style="padding: 0px;">
        <div class="table-responsive well" runat="server" id="divdeprec" style="max-height:200px!important">
            <div class="messesgarea">
                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
            <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server"
                CssClass="table table-hover" AutoGenerateColumns="False" HeaderStyle-BorderWidth="2" HeaderStyle-CssClass="bordered-darkorange" BorderWidth="0" GridLines="Horizontal">
                <Columns>
                    <asp:TemplateField HeaderText="Stock Item" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <%# Eval("StockItem")%>
                        </ItemTemplate>
                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <%# Eval("CompanyLocation")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stock Quantity" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <%# Eval("StockQuantity")%>
                        </ItemTemplate>
                        <ItemStyle Width="150px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Quantity" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <%# Eval("PurQty")%>
                        </ItemTemplate>
                        <ItemStyle Width="150px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Quantity" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <%# Eval("OrderQuantity")%>
                        </ItemTemplate>
                        <ItemStyle Width="150px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="table_row_red">
                        <ItemTemplate>
                            <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandArgument='<%# Eval("ProjectID")%>' CommandName="deprec" ImageUrl="~/images/icon_edit.png"
                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="center" CssClass="verticaaline" />
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</div>
