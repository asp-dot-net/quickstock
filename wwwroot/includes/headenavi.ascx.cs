﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class includes_headenavi : System.Web.UI.UserControl
{
    protected string SiteURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
        SiteURL = st.siteurl;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stuser = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stuser.EmployeeID;
        string date = string.Format("{0:mm/dd/yyyy}", DateTime.Now.AddHours(14).ToShortDateString());
        //if (!Roles.IsUserInRole("Installer"))
        //{
        //    //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        //    if (stuser.EmpImage != string.Empty)
        //    {
        //        imguser.ImageUrl = "~/userfiles/EmployeeProfile/" + stuser.EmpImage;
        //    }
        //    else
        //    {
        //        imguser.ImageUrl = "~/images/anynomous.png";
        //    }
        //}
        //lblusername.Text = stuser.EmpFirst + " " + stuser.EmpLast;

        if (Roles.IsUserInRole("SalesRep"))
        {
            ///dtfollwoup = ClstblCustInfo.tblCustInfo_SelectByDate(date, EmployeeID);
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            EmployeeID = "";
          
            listockorderreport.Visible = true;
           
        }

        //DataTable dtfollwoup = ClstblCustInfo.tblCustInfo_SelectByDate(date, date, EmployeeID, "", "", "", "");
        //if (dtfollwoup.Rows.Count > 0)
        //{
        //    lblFollowupcount.Text = dtfollwoup.Rows.Count.ToString();
        //    lblFollowupcount.Visible = true;
        //}
        //else
        //{
        //    lblFollowupcount.Visible = false;
        //}

        if (Roles.IsUserInRole("Administrator"))
        {
            limaster.Visible = true;
           
            listock.Visible = true;
           
        }
        if (Roles.IsUserInRole("Finance"))
        {
            

        }
        if (Roles.IsUserInRole("SalesRep"))
        {
           
        }
        if (Roles.IsUserInRole("DSalesRep"))
        {
            
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            
            listockreport.Visible = false;
          
        }
        if (Roles.IsUserInRole("DSales Manager"))
        {
           
        }
        if (Roles.IsUserInRole("Installer"))
        {
            
        }
        if (Roles.IsUserInRole("CompanyManager"))
        {
           
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
           
            listock.Visible = true;
           
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            
            listock.Visible = true;
            listockitem.Visible = false;
            listocktransfer.Visible = false;
            listockorder.Visible = false;
            listockusage.Visible = false;
            listocktransferreport.Visible = false;
            listockorderreport.Visible = false;
            listockreport.Visible = false;
            listockdeduct.Visible = true;

        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
           
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
          
        }
        if (Roles.IsUserInRole("Maintenance"))
        {
           
        }
        if (Roles.IsUserInRole("STC"))
        {
            
            //liformbay.Visible = false;
        }
        if (Roles.IsUserInRole("Verification"))
        {
           
            listock.Visible = true;
           
        }
        if (Roles.IsUserInRole("Accountant"))
        {
           

            listocktransferreport.Visible = true;
            listockorderreport.Visible = true;

           
            //liformbay.Visible = false;

           
            listockreport.Visible = true;
            
        }
        if (Roles.IsUserInRole("Customer"))
        {
            
        }
        if (Roles.IsUserInRole("WarehouseManager"))
        {
            listock.Visible = true;
          
            listockorder.Visible = false;
            listocktransferreport.Visible = true;
            listockreport.Visible = true;
        }
        if (Roles.IsUserInRole("Purchase Manager"))
        {
            listock.Visible = true;
            
            listockreport.Visible = true;
            
            listockorderreport.Visible = true;
        }
        if (stuser.SalesTeamID != string.Empty)
        {
            if (Roles.IsUserInRole("Lead Manager"))
            {
                
            }
            if (Roles.IsUserInRole("leadgen"))
            {
                lidashboard.Visible = true;
               
            }
        }
        else
        {
            if (Roles.IsUserInRole("Administrator"))
            {
             
            }
        }

        if (Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager"))
        {
            
        }
        else
        {
           
        }

        if (Roles.IsUserInRole("DSalesRep"))
        {
            
        }

        if (Roles.IsUserInRole("Installer"))
        {
           
        }
        else
        {
           
        }
        if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("Customer"))
        {

        }
        else
        {
            if (stuser.username != string.Empty)
            {
                if (stuser.username.ToLower() == "karen")
                {
                    
                }
            }
        }
        if (stuser.username != string.Empty)//SubAdministrator
        {
               // Roles.CreateRole("SubAdministrator");
                if (Roles.IsUserInRole("SubAdministrator"))
                {
                //if (stuser.username.ToLower() == "rvala" || stuser.username.ToLower() == "dharmik")
                //{

                //licompanylocations.Visible = false;
                //licustsource.Visible = false;
                //licustsourcesub.Visible = false;
                //lielecdistributor.Visible = false;
                //lielecretailer.Visible = false;
                //liemployee.Visible = false;
                //lihousetype.Visible = false;
                //lifinancewith.Visible = false;
                //licusttype.Visible = false;
                //limaster.Visible = true;
                //lipromosend.Visible = false;
                //lileadcancelreason.Visible = false;
                //limtcereason.Visible = false;
                //lilteamtracker.Visible = false;
                //liprojecttypes.Visible = false;
                //listctracker.Visible = false;
                //liinstinvoice.Visible = false;
                //lisalesinvoice.Visible = false;
                //liprojectcancel.Visible = false;
                //liprospectcategory.Visible = false;
                //liroofangles.Visible = false;
                //liprojecttrackers.Visible = false;
                //liinstalltracker.Visible = false;
                //lipaymenttype.Visible = false;
                //licustinstusers.Visible = false;
                //liprojectstatus.Visible = false;
                //liaccrefund.Visible = false;
                //litransactiontypes.Visible = false;
                //lisalesteams.Visible = false;
                //liformbaydocsreport.Visible = false;
                //liprojectonhold.Visible = false;
                //liinstallation.Visible = false;
                //liinvoicesissued.Visible = false;
                //lipromotiontype.Visible = false;
                //liinvoicespaid.Visible = false;
                //liprojectvariations.Visible = false;
                //listock.Visible = false;
                //limtcereasonsub.Visible = false;
                //licasualmtce.Visible = false;
                //liReports.Visible = false;
                //listockcategory.Visible = false;
                //liroottye.Visible = false;
                //lilteamcalendar.Visible = false;
                //liinstallercalendar.Visible = false;
                //lipromooffer.Visible = false;
                //li1.Visible = false;
                //liinvcommontype.Visible = false;
                //lipostcodes.Visible = false;
                //linewsletter.Visible = false;
                //lilogintracker.Visible = false;
                //lilteamtime.Visible = false;
                //lirefundoptions.Visible = false;
                //liupdateformbayId.Visible = false;
                //lipickup.Visible = false;
                //listreettype.Visible = true;
                //listreetname.Visible = true;
                //lileftempprojects.Visible = true;
                //liunittype.Visible = true;

                licompanylocations.Visible = false;
                
                limaster.Visible = true;
                
                listock.Visible = false;
               
                li1.Visible = false;
               
            }
               // }
            
        }


        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();
        string foldername = string.Empty;
       // Utilities.GetPageFolderName(Request.Url.PathAndQuery).ToLower();


        if (pagename == "dashboard" || pagename == "mydashboard")
        {
            lidashboard.Attributes.Add("class", "active");
        }

        if (pagename == "stockcalendar")
        {
            licalendar.Attributes.Add("class", "active");
        }

        else if (pagename == "stockitem" || pagename == "stockorder" || pagename == "stocktransfer" || pagename == "additemqty" || pagename == "stockdeduct" || pagename == "stockusage" || pagename == "order" || pagename == "transfer" || pagename == "usage")
        {
            listock.Attributes.Add("class", "active");
        }
        
        else if (((foldername == "master") && ((pagename == "companylocations") || (pagename == "custsource") || (pagename == "custsourcesub") || (pagename == "custtype") || (pagename == "elecdistributor") || (pagename == "elecretailer") || (pagename == "employee") || (pagename == "empdetails") || (pagename == "emppermissions") || (pagename == "empreferences") || (pagename == "emptitle") || (pagename == "financewith") || (pagename == "housetype") || (pagename == "leadcancelreason") || (pagename == "mtcereason") || (pagename == "mtcereasonsub") || (pagename == "projectcancel") || (pagename == "projectonhold") || (pagename == "projectstatus") || (pagename == "projecttrackers") || (pagename == "projecttypes") || (pagename == "projectvariations") || (pagename == "promotiontype") || (pagename == "prospectcategory") || (pagename == "roofangles") || (pagename == "roottye") || (pagename == "salesteams") || (pagename == "stockcategory") || (pagename == "transactiontypes") || (pagename == "paymenttype") || (pagename == "invcommontype") || (pagename == "postcodes") || (pagename == "custinstusers") || (pagename == "newsletter") || (pagename == "logintracker") || (pagename == "lteamtime"))) || (pagename == "leftempprojects") || (pagename == "refundoptions"))
        {
            limaster.Attributes.Add("class", "active");
        }
       


        if (pagename == "companylocations")
        {
            licompanylocations.Attributes.Add("class", "selected");
        }
       
        if (pagename == "stockitem")
        {
            listockitem.Attributes.Add("class", "selected");
        }
        if (pagename == "stocktransfer")
        {
            listocktransfer.Attributes.Add("class", "selected");
        }
        if (pagename == "stockorder")
        {
            listockorder.Attributes.Add("class", "selected");
        }
        if (pagename == "stockdeduct")
        {
            listockdeduct.Attributes.Add("class", "selected");
        }
        if (pagename == "stockusage")
        {
            listockusage.Attributes.Add("class", "selected");
        }

       
        if (pagename == "stockreport")
        {
            listockreport.Attributes.Add("class", "selected");
        }
        if (pagename == "stocktransferreport")
        {
            listocktransferreport.Attributes.Add("class", "selected");
        }

        




        DataTable dtemp = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(EmployeeID);
        if (dtemp.Rows.Count > 0)
        {
            foreach (DataRow dr in dtemp.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "2" || dr["SalesTeamID"].ToString() == "5" || dr["SalesTeamID"].ToString() == "7" || dr["SalesTeamID"].ToString() == "8")
                {
                    li1.Visible = true;
                }
                else
                {
                    li1.Visible = false;
                }
            }
        }

    }
}