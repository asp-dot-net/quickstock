﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Security;

public partial class testpage : System.Web.UI.Page
{
    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }

    public class ClsBridgeSelect
    {
        public long startDate;
        public long endDate;
    }

    public class BSDetailsRoot
    {
        public List<Jobs> jobs { get; set; }
    }

    public class Jobs
    {
        public string Installer { get; set; }

        //public int crmid { get; set; }

        //Nullable<int> crmid { get; set; }

        [JsonProperty("crmid", NullValueHandling = NullValueHandling.Ignore)]
        public string Crmid { get; set; }

        [JsonProperty("panels")]
        public Dictionary<string, The0038500> Panels { get; set; }

        //[JsonProperty("inverters")]
        //public Inverters Inverters { get; set; }
        [JsonProperty("inverters")]
        public Dictionary<string, The0038500> Inverters { get; set; }
    }

    public partial class The0038500
    {
        [JsonProperty("s")]
        public S S { get; set; }

        [JsonProperty("t", NullValueHandling = NullValueHandling.Ignore)]
        public long? T { get; set; }
    }

    public enum S { N, U, V };

    public class Panels
    {
        public string[] ss { get; set; }
    }
    #endregion

    static readonly System.Text.RegularExpressions.Regex trimmer = new System.Text.RegularExpressions.Regex(@"\s\s+");
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    Response.Write(Request.PhysicalApplicationPath);
        //}

        //Roles.CreateRole("Quick Stock");

        string dateTime = DateTime.Now.ToLongTimeString();
        string date = "16/03/2021";

        DateTime dt = DateTime.Now;

        string dat = date + ' ' + dateTime;

        DateTime NewDate = Convert.ToDateTime(dat).AddHours(14);


        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        //GetAllBridgeSelectDetailsByJobID("876821", "2", dtSerialNo);

        //string SerialNo = GetJobDetails("348385", "achieversenergy", "achievers1");
        //string SerialNo = GetJobDetails("348383", "achieversenergy", "achievers1");
    }

    protected void btnP_BSGB_Click(object sender, EventArgs e)
    {
        DataTable dtProject = ClsApiData.Sp_GetProjectNo();

        // Company ID=== 1-AriseSolar 2-SolarMiner
        // BSGBFlag====== 1- GreenBot 2-BridgeSelect

        if (dtProject.Rows.Count > 0)
        {
            for (int i = 0; i < dtProject.Rows.Count; i++)
            {
                if (dtProject.Rows[i]["BSGBFlag"].ToString() == "1") // Update GreenBot
                {
                    string ProjectNo = dtProject.Rows[i]["ProjectNumber"].ToString();
                    string CompanyID = dtProject.Rows[i]["CompanyID"].ToString();

                    if (CompanyID == "1") // For Arise Solar
                    {
                        UpdateSerialNoforGreenbot(ProjectNo, "arisesolar", "arisesolar1", CompanyID);
                    }
                    else if (CompanyID == "2") // for Solar Miner
                    {
                        UpdateSerialNoforGreenbot(ProjectNo, "mac.solarminer@gmail.com", "sminer234", CompanyID);
                    }
                    else if (CompanyID == "3") // for Wholesale
                    {
                        UpdateSerialNoforGreenbot(ProjectNo, "achieversenergy", "achievers1", CompanyID);
                    }
                }
                else //Update BridgeSelect
                {

                }
            }
        }
    }

    public void UpdateSerialNoforGreenbot(string JobNo, string un, string pwd, string CompanyID)
    {
        if (!string.IsNullOrEmpty(JobNo))
        {
            string SerialNo = GetJobDetails(JobNo, un, pwd);
            string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

            for (int i = 0; i < SerialNoList.Length; i++)
            {
                if (SerialNoList[i] != "")
                {
                    int Insert = ClsApiData.tblSerialNoFromBSGB_Insert(JobNo, SerialNoList[i], "1", CompanyID);
                }
            }
        }
    }

    public string GetJobDetails(string JobNumber, string Username, string Password)
    {
        string SerialNo = "";
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                //client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?RefNumber=" + JobNumber);
                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?JobID=" + JobNumber);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    int index = jobData.lstJobData.Count;
                    //Job_Response.lstJobData lstJobData = jobData.lstJobData[index - 1]; // Root Object
                    Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                    Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

                    SerialNo = JobSystemDetails.SerialNumbers;
                }
            }

        }
        catch (Exception ex)
        {

        }

        return SerialNo;
    }

    protected void btnAllGreenBotData_Click(object sender, EventArgs e)
    {
        DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("GreenBot");

        //DataTable dtSerialNo = new DataTable();

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        //for (int i = 0; i < 1; i++)
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string CompanyID = dt.Rows[i]["CompanyID"].ToString();
            //string FromDate = dt.Rows[i]["FromDate"].ToString();
            string ToDate = dt.Rows[i]["ToDate"].ToString();
            string username = dt.Rows[i]["username"].ToString();
            string password = dt.Rows[i]["password"].ToString();

            //string CompanyID = "4";
            string FromDate = "01/01/2020";
            //string ToDate = "28/05/2021";
            //string username = "solarbridgeregistry";
            //string password = "solarbridge123";

            dtSerialNo = GetAllGreenbotDetails(FromDate, ToDate, CompanyID, username, password, dtSerialNo);
        }

        //DataTable dtSerialNo = GetAllGreenbotDetails("01/01/2018", "31/12/2020", "1", "arisesolar", "arisesolar1");

        //--bool Delete = ClsDbData.tblSerialNoFromBSGB_DeleteByBSGBFlag("1");
        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        bool UpdateProjectNoArise = ClsDbData.tblProjectNoYN_Update();
        bool UpdateFlag = ClsDbData.Update_Flags();
        bool Update = ClsDbData.tbl_APIFetchData_Utilities_Update("Greenbot", DateTime.Now.AddHours(14).ToString());

        string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";

        Response.Write(msg);
    }

    public DataTable GetAllGreenbotDetails(string FromDate, string ToDate, string CompanyID, string Username, string Password, DataTable dtSerialNo)
    {
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + FromDate + "&ToDate=" + ToDate);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    DataTable dtSTCDetails = new DataTable();
                    dtSTCDetails.Columns.Add("JobID", typeof(int));
                    dtSTCDetails.Columns.Add("PVDNo", typeof(string));
                    dtSTCDetails.Columns.Add("CalculatedSTC", typeof(string));
                    dtSTCDetails.Columns.Add("STCStatus", typeof(string));
                    dtSTCDetails.Columns.Add("STCSubmissionDate", typeof(string));
                    dtSTCDetails.Columns.Add("NoofPanel", typeof(string));
                    dtSTCDetails.Columns.Add("CompanyID", typeof(int));
                    dtSTCDetails.Columns.Add("ProjectNo", typeof(string));

                    DataTable dtNoOfInverter = new DataTable();
                    dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
                    dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
                    dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

                    //int index = jobData.lstJobData.Count;
                    ////Job_Response.lstJobData lstJobData = jobData.lstJobData[index - 1]; // Root Object
                    //Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                    //Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

                    //SerialNo = JobSystemDetails.SerialNumbers;

                    for (int i = 0; i < jobData.lstJobData.Count; i++)
                    {
                        Job_Response.lstJobData lstJobData = jobData.lstJobData[i]; // Root Object
                        Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details
                        Job_Response.InstallerView InstallerView = lstJobData.InstallerView; // get Child object Details

                        List<Job_Response.lstJobInverterDetails> lstJobInverterDetails = lstJobData.lstJobInverterDetails;
                        string NoOfInverter = "0";
                        if (lstJobInverterDetails.Count > 0)
                        {
                            NoOfInverter = lstJobInverterDetails[lstJobInverterDetails.Count - 1].NoOfInverter;

                            NoOfInverter = !string.IsNullOrEmpty(NoOfInverter) ? NoOfInverter : "0";
                        }

                        string ProjectNo = "";
                        if (CompanyID == "3")
                        {
                            ProjectNo = lstJobData.BasicDetails.JobID;
                        }
                        else
                        {
                            ProjectNo = lstJobData.BasicDetails.RefNumber;
                        }

                        string SerialNo = JobSystemDetails.SerialNumbers;
                        string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

                        string InstallerName = "";
                        try
                        {
                            if (InstallerView != null)
                            {
                                InstallerName = (InstallerView.FirstName == null ? "" : InstallerView.FirstName.ToString()) + " " + (InstallerView.LastName == null ? "" : InstallerView.LastName.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            int index = i;
                        }

                        for (int j = 0; j < SerialNoList.Length; j++)
                        {
                            DataRow dr = dtSerialNo.NewRow();
                            dr["ProjectNo"] = ProjectNo.Trim();
                            dr["SerialNo"] = SerialNoList[j];
                            dr["BSGBFlag"] = 1; // GreenBot
                            dr["CompanyID"] = CompanyID;
                            dr["StockCategoryID"] = 1;
                            dr["Varified"] = "";
                            dr["InstallerName"] = InstallerName.Trim();
                            dtSerialNo.Rows.Add(dr);
                        }

                        // Now Data Table For Wholesale
                        if (CompanyID == "3")
                        {
                            Job_Response.JobSTCDetails JobSTCDetails = lstJobData.JobSTCDetails;
                            Job_Response.JobSTCStatusData JobSTCStatusData = lstJobData.JobSTCStatusData;

                            string JobID = lstJobData.BasicDetails.JobID;
                            string FailedAccreditationCode = JobSTCDetails.FailedAccreditationCode;
                            string CalculatedSTC = JobSTCStatusData.CalculatedSTC;
                            string STCStatus = JobSTCStatusData.STCStatus;
                            string STCSubmissionDate = JobSTCStatusData.STCSubmissionDate;
                            string NoofPanel = JobSystemDetails.NoOfPanel;

                            DataRow dr = dtSTCDetails.NewRow();
                            dr["JobID"] = JobID;
                            dr["PVDNo"] = FailedAccreditationCode;
                            dr["CalculatedSTC"] = CalculatedSTC;
                            dr["STCStatus"] = STCStatus;
                            dr["STCSubmissionDate"] = STCSubmissionDate;
                            dr["NoofPanel"] = NoofPanel;
                            dr["CompanyID"] = Convert.ToInt32(CompanyID);
                            dr["ProjectNo"] = ProjectNo;
                            dtSTCDetails.Rows.Add(dr);
                        }

                        DataRow dr1 = dtNoOfInverter.NewRow();
                        dr1["ProjectNo"] = ProjectNo;
                        dr1["CompanyID"] = CompanyID;
                        dr1["NoOfInverter"] = NoOfInverter;
                        dtNoOfInverter.Rows.Add(dr1);
                    }

                    if (CompanyID == "3")
                    {
                        if (dtSTCDetails.Rows.Count > 0)
                        {
                            int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSTCDetails(dtSTCDetails);
                            string msg = "<script>alert('Total Record is " + dtSTCDetails.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                            Response.Write(msg);
                        }
                    }

                    if (dtNoOfInverter.Rows.Count > 0)
                    {
                        int UpdateData = ClsDbData.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                        string msg = "<script>alert('Total Record is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                        Response.Write(msg);
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return dtSerialNo;
    }

    protected void btnAllBridgeSelect_Click(object sender, EventArgs e)
    {
        //DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("BridgeSelect");

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));
        //dtSerialNo.Columns.Add("InstallationDate", typeof(DateTime));

        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //string CompanyID = dt.Rows[i]["CompanyID"].ToString();
        //string FromDate = dt.Rows[i]["FromDate"].ToString();
        //string ToDate = dt.Rows[i]["ToDate"].ToString();

        //string username = dt.Rows[i]["username"].ToString();
        //string password = dt.Rows[i]["password"].ToString();

        //string FromDate = "10/02/2021";
        //string ToDate = "15/02/2021";
        string CompanyID = "1";
        string FromDate = txtSDate.Text;
        string ToDate = txtEDate.Text;
        long _SDate = DateTimeStamp(FromDate);
        long _EDate = DateTimeStamp(ToDate);
        dtSerialNo = GetAllBridgeSelectDetails(_SDate, _EDate, CompanyID, dtSerialNo);
        //}

        //string jsonFilePath = @"D:\Suresh\Project\QuickStock_Git\wwwroot\find-jobs (2).json";
        //using (StreamReader r = new StreamReader(jsonFilePath))
        //{
        //    string json = r.ReadToEnd();

        //    BSDetailsRoot jobData = JsonConvert.DeserializeObject<BSDetailsRoot>(json);

        //    //for (int i = 0; i < jobData.jobs.Count; i++)
        //    //{
        //    //    Jobs jobs = jobData.jobs[i];

        //    //    string InstallerName = jobs.installer;

        //    //    //string[] ss = jobs.Panels;
        //    //}
        //}

        //bool Delete = ClsDbData.tblSerialNoFromBSGB_DeleteByBSGBFlag("2");
        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        bool UpdateProjectNoArise = ClsDbData.tblProjectNoYN_Update();
        bool UpdateFlag = ClsDbData.Update_Flags();
        bool Update = ClsDbData.tbl_APIFetchData_Utilities_Update("BridgeSelect", DateTime.Now.AddHours(14).ToString());

        string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";

        Response.Write(msg);
    }

    #region Get Data from BridgeSelect 
    public DataTable GetAllBridgeSelectDetails(long FromDate, long ToDate, string CompanyID, DataTable dtSerialNo)
    {
        Job_Response.Details ReturnDetails = new Job_Response.Details();
        try
        {
            var obj = new ClsBridgeSelect
            {
                startDate = FromDate,
                endDate = ToDate
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/find/jobs/date/";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<BSDetailsRoot> JsonData = client.Execute<BSDetailsRoot>(request);

            BSDetailsRoot jobData = JsonConvert.DeserializeObject<BSDetailsRoot>(JsonData.Content);

            DataTable dtNoOfInverter = new DataTable();
            dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
            dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
            dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

            for (int i = 0; i < jobData.jobs.Count; i++)
            {
                Jobs jobs = jobData.jobs[i];

                string InstallerName = jobs.Installer;

                string ProjectNo = jobs.Crmid == null ? "" : jobs.Crmid.ToString();
                //string ProjectNo = "";
                //string ProjectNo = !string.IsNullOrEmpty(jobs.Crmid.ToString()) ? trimmer.Replace(jobs.Crmid.ToString(), "") : "";

                foreach (var item in jobs.Panels)
                {
                    string SerialNo = item.Key;
                    The0038500 PanelStatus = item.Value;
                    string Verified = PanelStatus.S.ToString();

                    DataRow dr = dtSerialNo.NewRow();
                    dr["ProjectNo"] = ProjectNo;
                    dr["SerialNo"] = SerialNo;
                    dr["BSGBFlag"] = 2; // GreenBot
                    dr["CompanyID"] = CompanyID;
                    dr["Varified"] = Verified;
                    dr["StockCategoryID"] = 1;
                    dr["InstallerName"] = InstallerName;
                    dtSerialNo.Rows.Add(dr);
                }

                foreach (var item in jobs.Inverters)
                {
                    string SerialNo = item.Key;
                    The0038500 PanelStatus = item.Value;
                    string Verified = PanelStatus.S.ToString();

                    DataRow dr = dtSerialNo.NewRow();
                    dr["ProjectNo"] = ProjectNo;
                    dr["SerialNo"] = SerialNo;
                    dr["BSGBFlag"] = 2; // BridgeSelect
                    dr["CompanyID"] = CompanyID;
                    dr["Varified"] = Verified;
                    dr["StockCategoryID"] = 2;
                    dr["InstallerName"] = InstallerName;
                    dtSerialNo.Rows.Add(dr);
                }

                string NoOfInverter = jobs.Inverters.Count.ToString();

                DataRow dr1 = dtNoOfInverter.NewRow();
                dr1["ProjectNo"] = ProjectNo;
                dr1["CompanyID"] = CompanyID;
                dr1["NoOfInverter"] = NoOfInverter;
                dtNoOfInverter.Rows.Add(dr1);
            }

            if (dtNoOfInverter.Rows.Count > 0)
            {
                int UpdateData = ClsDbData.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                string msg = "<script>alert('Total Record is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                Response.Write(msg);
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return dtSerialNo;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    public static long DateTimeStamp(string date)
    {
        DateTime Date = Convert.ToDateTime(date).Date;

        //DateTime value = DateTime.Now;
        long epoch = (Date.Ticks - 621355968000000000) / 10000;

        return epoch;
    }
    #endregion

    protected void Button2_Click(object sender, EventArgs e)
    {
        string RoleName = txtRole.Text.Trim();

        if (RoleName != "")
        {
            Roles.CreateRole(RoleName);
        }
    }

    public class ClsBridgeSelect1
    {
        public string crmid;
    }

    public DataTable GetAllBridgeSelectDetailsByJobID(string JobID, string CompanyID, DataTable dtSerialNo)
    {
        Job_Response.Details ReturnDetails = new Job_Response.Details();
        try
        {
            var obj = new ClsBridgeSelect1
            {
                crmid = JobID
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/status";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<BSDetailsRoot> JsonData = client.Execute<BSDetailsRoot>(request);

            BSDetailsRoot jobData = JsonConvert.DeserializeObject<BSDetailsRoot>(JsonData.Content);

            for (int i = 0; i < jobData.jobs.Count; i++)
            {
                Jobs jobs = jobData.jobs[i];

                string InstallerName = jobs.Installer;

                string ProjectNo = jobs.Crmid == null ? "" : jobs.Crmid.ToString();
                //string ProjectNo = "";
                //string ProjectNo = !string.IsNullOrEmpty(jobs.Crmid.ToString()) ? trimmer.Replace(jobs.Crmid.ToString(), "") : "";

                foreach (var item in jobs.Panels)
                {
                    string SerialNo = item.Key;
                    The0038500 PanelStatus = item.Value;
                    string Verified = PanelStatus.S.ToString();

                    DataRow dr = dtSerialNo.NewRow();
                    dr["ProjectNo"] = ProjectNo;
                    dr["SerialNo"] = SerialNo;
                    dr["BSGBFlag"] = 2; // GreenBot
                    dr["CompanyID"] = CompanyID;
                    dr["Varified"] = Verified;
                    dr["StockCategoryID"] = 1;
                    dr["InstallerName"] = InstallerName;
                    dtSerialNo.Rows.Add(dr);
                }

                foreach (var item in jobs.Inverters)
                {
                    string SerialNo = item.Key;
                    The0038500 PanelStatus = item.Value;
                    string Verified = PanelStatus.S.ToString();

                    DataRow dr = dtSerialNo.NewRow();
                    dr["ProjectNo"] = ProjectNo;
                    dr["SerialNo"] = SerialNo;
                    dr["BSGBFlag"] = 2; // BridgeSelect
                    dr["CompanyID"] = CompanyID;
                    dr["Varified"] = Verified;
                    dr["StockCategoryID"] = 2;
                    dr["InstallerName"] = InstallerName;
                    dtSerialNo.Rows.Add(dr);
                }
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return dtSerialNo;
    }
   
}