﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;

public partial class GreenbotAllData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Fetch();

        
        BS_Fetch();
    }

    protected void Fetch()
    {
        //DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("GreenBot");

        //jobsResponse.root jobData = getAsyc("arisesolar", "arisesolar1");
        //jobsResponse.root jobData = getAsyc("mac.solarminer@gmail.com", "sminer234");
        //jobsResponse.root jobData = getAsyc("achieversenergy", "achievers1");
        //jobsResponse.root jobData = getAsyc("ariseregistry", "arise980");
        jobsResponse.root jobData = getAsyc("solarbridgeregistry", "solarbridge123");
        //GetAsyc("arisesolar", "arisesolar1");

        DataTable dtDetails = convertClassToDataTable(jobData, "4");

        if (dtDetails.Rows.Count > 0)
        {
            int UpdateLog = ClsGreenbotAllData.SP_GreenbotAllData_BulkInsertUpdate(dtDetails);
        }
    }

    protected jobsResponse.root getAsyc(string uname, string pwd)
    {
        jobsResponse.root jobsData = new jobsResponse.root();
        try
        {
            var client = new RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            request.AddParameter("Username", uname);
            request.AddParameter("Password", pwd);

            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                loginResponse.Root root = JsonConvert.DeserializeObject<loginResponse.Root>(response.Content);
                string access_token = root.TokenData.access_token; // get Login Access Token

                string ToDate = System.DateTime.Now.AddHours(15).AddDays(-1).ToShortDateString();
                client = new RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=01/01/2020&ToDate=" + ToDate);
                request = new RestSharp.RestRequest(RestSharp.Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", access_token));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<jobsResponse.root> JsonData = client.Execute<jobsResponse.root>(request);

                if (JsonData.Data.StatusCode == "OK")
                {
                    jobsData = JsonConvert.DeserializeObject<jobsResponse.root>(JsonData.Content);
                }
            }
        }
        catch (Exception ex)
        {
            //ExceptionLogging.SendExcepToDB(ex);
        }

        return jobsData;
    }

    protected DataTable convertClassToDataTable(jobsResponse.root jobData, string companyId)
    {
        DataTable dtDetails = generateDatatable();

        try
        {
            if (jobData != null)
            {
                foreach (var item in jobData.lstJobData)
                {
                    jobsResponse.lstJobData lstJobData = item;
                    bool result = true;
                    int n = 0;
                    if (companyId != "3")
                    {
                        if (!int.TryParse(lstJobData.BasicDetails.RefNumber, out n))
                        {
                            result = false;
                        }
                    }

                    if (result)
                    {
                        DataRow dr = dtDetails.NewRow();

                        int projectNo = 0;
                        if (companyId == "3")
                        {
                            projectNo = lstJobData.BasicDetails.JobID;
                        }
                        else
                        {
                            projectNo = Convert.ToInt32(lstJobData.BasicDetails.RefNumber);
                        }

                        dr["CompanyId"] = companyId;
                        dr["ProjectNo"] = projectNo;

                        // BasicDetails
                        jobsResponse.BasicDetails basicDetails = lstJobData.BasicDetails;
                        if (basicDetails != null)
                        {
                            dr["VendorJobId"] = basicDetails.VendorJobId;
                            dr["JobID"] = basicDetails.JobID;
                            dr["Title"] = basicDetails.Title;
                            dr["RefNumber"] = basicDetails.RefNumber;
                            dr["Description"] = basicDetails.Description;
                            dr["JobType"] = basicDetails.JobType;
                            dr["JobStage"] = basicDetails.JobStage;
                            dr["Priority"] = basicDetails.Priority;
                            dr["InstallationDate"] = basicDetails.InstallationDate;
                            dr["CreatedDate"] = basicDetails.CreatedDate;
                            dr["IsGst"] = basicDetails.IsGst;
                        }

                        //JobOwnerDetails
                        jobsResponse.JobOwnerDetails jobOwnerDetails = lstJobData.JobOwnerDetails;
                        if (jobOwnerDetails != null)
                        {
                            dr["OwnerType_JOD"] = jobOwnerDetails.OwnerType;
                            dr["CompanyName_JOD"] = jobOwnerDetails.CompanyName;
                            dr["FirstName_JOD"] = jobOwnerDetails.FirstName;
                            dr["LastName_JOD"] = jobOwnerDetails.LastName;
                            dr["Phone_JOD"] = jobOwnerDetails.Phone;
                            dr["Mobile_JOD"] = jobOwnerDetails.Mobile;
                            dr["Email_JOD"] = jobOwnerDetails.Email;
                            dr["IsPostalAddress_JOD"] = jobOwnerDetails.IsPostalAddress;
                            dr["UnitTypeID_JOD"] = jobOwnerDetails.UnitTypeID;
                            dr["UnitNumber_JOD"] = jobOwnerDetails.UnitNumber;
                            dr["StreetNumber_JOD"] = jobOwnerDetails.StreetNumber;
                            dr["StreetName_JOD"] = jobOwnerDetails.StreetName;
                            dr["StreetTypeID_JOD"] = jobOwnerDetails.StreetTypeID;
                            dr["Town_JOD"] = jobOwnerDetails.Town;
                            dr["State_JOD"] = jobOwnerDetails.State;
                            dr["PostCode_JOD"] = jobOwnerDetails.PostCode;
                        }

                        //JobInstallationDetails
                        jobsResponse.JobInstallationDetails jobInstallationDetails = lstJobData.JobInstallationDetails;
                        if (jobInstallationDetails != null)
                        {
                            dr["DistributorID_JID"] = jobInstallationDetails.DistributorID;
                            dr["MeterNumber_JID"] = jobInstallationDetails.MeterNumber;
                            dr["PhaseProperty_JID"] = jobInstallationDetails.PhaseProperty;
                            dr["ElectricityProviderID_JID"] = jobInstallationDetails.ElectricityProviderID;
                            dr["IsSameAsOwnerAddress_JID"] = jobInstallationDetails.IsSameAsOwnerAddress;
                            dr["IsPostalAddress_JID"] = jobInstallationDetails.IsPostalAddress;
                            dr["UnitTypeID_JID"] = jobInstallationDetails.UnitTypeID;
                            dr["UnitNumber_JID"] = jobInstallationDetails.UnitNumber;
                            dr["StreetNumber_JID"] = jobInstallationDetails.StreetNumber;
                            dr["StreetName_JID"] = jobInstallationDetails.StreetName;
                            dr["StreetTypeID_JID"] = jobInstallationDetails.StreetTypeID;
                            dr["Town_JID"] = jobInstallationDetails.Town;
                            dr["State_JID"] = jobInstallationDetails.State;
                            dr["PostCode_JID"] = jobInstallationDetails.PostCode;
                            dr["NMI_JID"] = jobInstallationDetails.NMI;
                            dr["SystemSize_JID"] = jobInstallationDetails.SystemSize;
                            dr["PropertyType_JID"] = jobInstallationDetails.PropertyType;
                            dr["SingleMultipleStory_JID"] = jobInstallationDetails.SingleMultipleStory;
                            dr["InstallingNewPanel_JID"] = jobInstallationDetails.InstallingNewPanel;
                            dr["Location_JID"] = jobInstallationDetails.Location;
                            dr["ExistingSystem_JID"] = jobInstallationDetails.ExistingSystem;
                            dr["ExistingSystemSize_JID"] = jobInstallationDetails.ExistingSystemSize;
                            dr["SystemLocation_JID"] = jobInstallationDetails.SystemLocation;
                            dr["NoOfPanels_JID"] = jobInstallationDetails.NoOfPanels;
                            dr["AdditionalInstallationInformation_JID"] = jobInstallationDetails.AdditionalInstallationInformation;
                        }

                        //JobSystemDetails
                        jobsResponse.JobSystemDetails jobSystemDetails = lstJobData.JobSystemDetails;
                        if (jobSystemDetails != null)
                        {
                            dr["SystemSize_JSD"] = jobInstallationDetails.DistributorID;
                            dr["SerialNumbers_JSD"] = jobInstallationDetails.MeterNumber;
                            dr["NoOfPanel_JSD"] = jobInstallationDetails.PhaseProperty;
                        }

                        //JobSystemDetails
                        jobsResponse.JobSTCDetails jobSTCDetails = lstJobData.JobSTCDetails;
                        if (jobSTCDetails != null)
                        {
                            dr["AdditionalCapacityNotes_JSTC"] = jobSTCDetails.AdditionalCapacityNotes;
                            dr["TypeOfConnection_JSTC"] = jobSTCDetails.TypeOfConnection;
                            dr["SystemMountingType_JSTC"] = jobSTCDetails.SystemMountingType;
                            dr["DeemingPeriod_JSTC"] = jobSTCDetails.DeemingPeriod;
                            dr["CertificateCreated_JSTC"] = jobSTCDetails.CertificateCreated;
                            dr["FailedAccreditationCode_JSTC"] = jobSTCDetails.FailedAccreditationCode;
                            dr["FailedReason_JSTC"] = jobSTCDetails.FailedReason;
                            dr["MultipleSGUAddress_JSTC"] = jobSTCDetails.MultipleSGUAddress;
                            dr["Location_JSTC"] = jobSTCDetails.Location;
                            dr["AdditionalLocationInformation_JSTC"] = jobSTCDetails.AdditionalLocationInformation;
                            dr["AdditionalSystemInformation_JSTC"] = jobSTCDetails.AdditionalSystemInformation;
                        }

                        //InstallerView
                        jobsResponse.InstallerView installerView = lstJobData.InstallerView;
                        if (installerView != null)
                        {
                            dr["FirstName_IV"] = installerView.FirstName;
                            dr["LastName_IV"] = installerView.LastName;
                            dr["Email_IV"] = installerView.Email;
                            dr["Phone_IV"] = installerView.Phone;
                            dr["Mobile_IV"] = installerView.Mobile;
                            dr["CECAccreditationNumber_IV"] = installerView.CECAccreditationNumber;
                            dr["ElectricalContractorsLicenseNumber_IV"] = installerView.ElectricalContractorsLicenseNumber;
                            dr["IsPostalAddress_IV"] = installerView.IsPostalAddress;
                            dr["UnitTypeID_IV"] = installerView.UnitTypeID;
                            dr["UnitNumber_IV"] = installerView.UnitNumber;
                            dr["StreetNumber_IV"] = installerView.StreetNumber;
                            dr["StreetName_IV"] = installerView.StreetName;
                            dr["StreetTypeID_IV"] = installerView.StreetTypeID;
                            dr["Town_IV"] = installerView.Town;
                            dr["State_IV"] = installerView.State;
                            dr["PostCode_IV"] = installerView.PostCode;
                        }

                        //DesignerView
                        jobsResponse.DesignerView designerView = lstJobData.DesignerView;
                        if (designerView != null)
                        {
                            dr["FirstName_DV"] = designerView.FirstName;
                            dr["LastName_DV"] = designerView.LastName;
                            dr["Email_DV"] = designerView.Email;
                            dr["Phone_DV"] = designerView.Phone;
                            dr["Mobile_DV"] = designerView.Mobile;
                            dr["CECAccreditationNumber_DV"] = designerView.CECAccreditationNumber;
                            dr["ElectricalContractorsLicenseNumber_DV"] = designerView.ElectricalContractorsLicenseNumber;
                            dr["IsPostalAddress_DV"] = designerView.IsPostalAddress;
                            dr["UnitTypeID_DV"] = designerView.UnitTypeID;
                            dr["UnitNumber_DV"] = designerView.UnitNumber;
                            dr["StreetNumber_DV"] = designerView.StreetNumber;
                            dr["StreetName_DV"] = designerView.StreetName;
                            dr["StreetTypeID_DV"] = designerView.StreetTypeID;
                            dr["Town_DV"] = designerView.Town;
                            dr["State_DV"] = designerView.State;
                            dr["PostCode_DV"] = designerView.PostCode;
                        }

                        //JobElectricians
                        jobsResponse.JobElectricians jobElectricians = lstJobData.JobElectricians;
                        if (jobElectricians != null)
                        {
                            dr["CompanyName_JE"] = jobElectricians.CompanyName;
                            dr["FirstName_JE"] = jobElectricians.FirstName;
                            dr["LastName_JE"] = jobElectricians.LastName;
                            dr["Email_JE"] = jobElectricians.Email;
                            dr["Phone_JE"] = jobElectricians.Phone;
                            dr["Mobile_JE"] = jobElectricians.Mobile;
                            dr["LicenseNumber_JE"] = jobElectricians.LicenseNumber;
                            dr["IsPostalAddress_JE"] = jobElectricians.IsPostalAddress;
                            dr["UnitTypeID_JE"] = jobElectricians.UnitTypeID;
                            dr["UnitNumber_JE"] = jobElectricians.UnitNumber;
                            dr["StreetNumber_JE"] = jobElectricians.StreetNumber;
                            dr["StreetName_JE"] = jobElectricians.StreetName;
                            dr["StreetTypeID_JE"] = jobElectricians.StreetTypeID;
                            dr["Town_JE"] = jobElectricians.Town;
                            dr["State_JE"] = jobElectricians.State;
                            dr["PostCode_JE"] = jobElectricians.PostCode;
                        }

                        //LstJobInverterDetail
                        List<jobsResponse.LstJobInverterDetail> lstJobInverterDetails = lstJobData.lstJobInverterDetails;
                        if (lstJobInverterDetails.Count > 0)
                        {
                            dr["Brand_LstJID"] = lstJobInverterDetails[0].Brand;
                            dr["Model_LstJID"] = lstJobInverterDetails[0].Model;
                            dr["Series_LstJID"] = lstJobInverterDetails[0].Series;
                            dr["NoOfInverter_LstJID"] = lstJobInverterDetails[0].NoOfInverter;
                        }

                        //LstJobInverterDetail
                        List<jobsResponse.LstJobPanelDetail> lstJobPanelDetails = lstJobData.lstJobPanelDetails;
                        if (lstJobPanelDetails.Count > 0)
                        {
                            dr["Brand_LstJPD"] = lstJobPanelDetails[0].Brand;
                            dr["Model_LstJPD"] = lstJobPanelDetails[0].Model;
                            dr["NoOfInverter_LstJPD"] = lstJobPanelDetails[0].NoOfPanel;
                        }

                        //JobSTCStatusData
                        jobsResponse.JobSTCStatusData jobSTCStatusData = lstJobData.JobSTCStatusData;
                        if (jobSTCStatusData != null)
                        {
                            dr["STCStatus_JSTCD"] = jobSTCStatusData.STCStatus;
                            dr["CalculatedSTC_JSTCD"] = jobSTCStatusData.CalculatedSTC;
                            dr["STCPrice_JSTCD"] = jobSTCStatusData.STCPrice;
                            dr["FailureNotice_JSTCD"] = jobSTCStatusData.FailureNotice;
                            dr["ComplianceNotes_JSTCD"] = jobSTCStatusData.ComplianceNotes;
                            dr["STCSubmissionDate_JSTCD"] = jobSTCStatusData.STCSubmissionDate;
                            dr["STCInvoiceStatus_JSTCD"] = jobSTCStatusData.STCInvoiceStatus;
                            dr["IsInvoiced_JSTCD"] = jobSTCStatusData.IsInvoiced;
                        }

                        bool contains = dtDetails.AsEnumerable().Any(row => projectNo == row.Field<int>("ProjectNo"));

                        if(contains == false)
                        {
                            dtDetails.Rows.Add(dr);

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ExceptionLogging.SendExcepToDB(ex);
        }

        return dtDetails;
    }

    private DataTable generateDatatable()
    {
        DataTable dtDetails = new DataTable();

        // BasicDetails  
        dtDetails.Columns.Add("CompanyId", typeof(int));
        dtDetails.Columns.Add("ProjectNo", typeof(int));
        dtDetails.Columns.Add("VendorJobId", typeof(string));
        dtDetails.Columns.Add("JobID", typeof(int));
        dtDetails.Columns.Add("Title", typeof(string));
        dtDetails.Columns.Add("RefNumber", typeof(string));
        dtDetails.Columns.Add("Description", typeof(string));
        dtDetails.Columns.Add("JobType", typeof(int));
        dtDetails.Columns.Add("JobStage", typeof(int));
        dtDetails.Columns.Add("Priority", typeof(int));
        dtDetails.Columns.Add("InstallationDate", typeof(string));
        dtDetails.Columns.Add("CreatedDate", typeof(string));
        dtDetails.Columns.Add("IsGst", typeof(bool));

        //JobOwnerDetails
        dtDetails.Columns.Add("OwnerType_JOD", typeof(string));
        dtDetails.Columns.Add("CompanyName_JOD", typeof(string));
        dtDetails.Columns.Add("FirstName_JOD", typeof(string));
        dtDetails.Columns.Add("LastName_JOD", typeof(string));
        dtDetails.Columns.Add("Phone_JOD", typeof(string));
        dtDetails.Columns.Add("Mobile_JOD", typeof(string));
        dtDetails.Columns.Add("Email_JOD", typeof(string));
        dtDetails.Columns.Add("IsPostalAddress_JOD", typeof(bool));
        dtDetails.Columns.Add("UnitTypeID_JOD", typeof(string));
        dtDetails.Columns.Add("UnitNumber_JOD", typeof(string));
        dtDetails.Columns.Add("StreetNumber_JOD", typeof(string));
        dtDetails.Columns.Add("StreetName_JOD", typeof(string));
        dtDetails.Columns.Add("StreetTypeID_JOD", typeof(int));
        dtDetails.Columns.Add("Town_JOD", typeof(string));
        dtDetails.Columns.Add("State_JOD", typeof(string));
        dtDetails.Columns.Add("PostCode_JOD", typeof(string));

        //JobInstallationDetails
        dtDetails.Columns.Add("DistributorID_JID", typeof(string));
        dtDetails.Columns.Add("MeterNumber_JID", typeof(string));
        dtDetails.Columns.Add("PhaseProperty_JID", typeof(string));
        dtDetails.Columns.Add("ElectricityProviderID_JID", typeof(string));
        dtDetails.Columns.Add("IsSameAsOwnerAddress_JID", typeof(bool));
        dtDetails.Columns.Add("IsPostalAddress_JID", typeof(bool));
        dtDetails.Columns.Add("UnitTypeID_JID", typeof(string));
        dtDetails.Columns.Add("UnitNumber_JID", typeof(string));
        dtDetails.Columns.Add("StreetNumber_JID", typeof(string));
        dtDetails.Columns.Add("StreetName_JID", typeof(string));
        dtDetails.Columns.Add("StreetTypeID_JID", typeof(int));
        dtDetails.Columns.Add("Town_JID", typeof(string));
        dtDetails.Columns.Add("State_JID", typeof(string));
        dtDetails.Columns.Add("PostCode_JID", typeof(string));
        dtDetails.Columns.Add("NMI_JID", typeof(string));
        dtDetails.Columns.Add("SystemSize_JID", typeof(string));
        dtDetails.Columns.Add("PropertyType_JID", typeof(string));
        dtDetails.Columns.Add("SingleMultipleStory_JID", typeof(string));
        dtDetails.Columns.Add("InstallingNewPanel_JID", typeof(string));
        dtDetails.Columns.Add("Location_JID", typeof(string));
        dtDetails.Columns.Add("ExistingSystem_JID", typeof(bool));
        dtDetails.Columns.Add("ExistingSystemSize_JID", typeof(string));
        dtDetails.Columns.Add("SystemLocation_JID", typeof(string));
        dtDetails.Columns.Add("NoOfPanels_JID", typeof(string));
        dtDetails.Columns.Add("AdditionalInstallationInformation_JID", typeof(string));

        //JobSystemDetails
        dtDetails.Columns.Add("SystemSize_JSD", typeof(string));
        dtDetails.Columns.Add("SerialNumbers_JSD", typeof(string));
        dtDetails.Columns.Add("NoOfPanel_JSD", typeof(string));

        //JobSTCDetails
        dtDetails.Columns.Add("AdditionalCapacityNotes_JSTC", typeof(string));
        dtDetails.Columns.Add("TypeOfConnection_JSTC", typeof(string));
        dtDetails.Columns.Add("SystemMountingType_JSTC", typeof(string));
        dtDetails.Columns.Add("DeemingPeriod_JSTC", typeof(string));
        dtDetails.Columns.Add("CertificateCreated_JSTC", typeof(string));
        dtDetails.Columns.Add("FailedAccreditationCode_JSTC", typeof(string));
        dtDetails.Columns.Add("FailedReason_JSTC", typeof(string));
        dtDetails.Columns.Add("MultipleSGUAddress_JSTC", typeof(string));
        dtDetails.Columns.Add("Location_JSTC", typeof(string));
        dtDetails.Columns.Add("AdditionalLocationInformation_JSTC", typeof(string));
        dtDetails.Columns.Add("AdditionalSystemInformation_JSTC", typeof(string));

        //InstallerView
        dtDetails.Columns.Add("FirstName_IV", typeof(string));
        dtDetails.Columns.Add("LastName_IV", typeof(string));
        dtDetails.Columns.Add("Email_IV", typeof(string));
        dtDetails.Columns.Add("Phone_IV", typeof(string));
        dtDetails.Columns.Add("Mobile_IV", typeof(string));
        dtDetails.Columns.Add("CECAccreditationNumber_IV", typeof(string));
        dtDetails.Columns.Add("ElectricalContractorsLicenseNumber_IV", typeof(string));
        dtDetails.Columns.Add("IsPostalAddress_IV", typeof(bool));
        dtDetails.Columns.Add("UnitTypeID_IV", typeof(int));
        dtDetails.Columns.Add("UnitNumber_IV", typeof(string));
        dtDetails.Columns.Add("StreetNumber_IV", typeof(string));
        dtDetails.Columns.Add("StreetName_IV", typeof(string));
        dtDetails.Columns.Add("StreetTypeID_IV", typeof(int));
        dtDetails.Columns.Add("Town_IV", typeof(string));
        dtDetails.Columns.Add("State_IV", typeof(string));
        dtDetails.Columns.Add("PostCode_IV", typeof(string));

        //DesignerView
        dtDetails.Columns.Add("FirstName_DV", typeof(string));
        dtDetails.Columns.Add("LastName_DV", typeof(string));
        dtDetails.Columns.Add("Email_DV", typeof(string));
        dtDetails.Columns.Add("Phone_DV", typeof(string));
        dtDetails.Columns.Add("Mobile_DV", typeof(string));
        dtDetails.Columns.Add("CECAccreditationNumber_DV", typeof(string));
        dtDetails.Columns.Add("ElectricalContractorsLicenseNumber_DV", typeof(string));
        dtDetails.Columns.Add("IsPostalAddress_DV", typeof(bool));
        dtDetails.Columns.Add("UnitTypeID_DV", typeof(int));
        dtDetails.Columns.Add("UnitNumber_DV", typeof(string));
        dtDetails.Columns.Add("StreetNumber_DV", typeof(string));
        dtDetails.Columns.Add("StreetName_DV", typeof(string));
        dtDetails.Columns.Add("StreetTypeID_DV", typeof(int));
        dtDetails.Columns.Add("Town_DV", typeof(string));
        dtDetails.Columns.Add("State_DV", typeof(string));
        dtDetails.Columns.Add("PostCode_DV", typeof(string));

        //JobElectricians
        dtDetails.Columns.Add("CompanyName_JE", typeof(string));
        dtDetails.Columns.Add("FirstName_JE", typeof(string));
        dtDetails.Columns.Add("LastName_JE", typeof(string));
        dtDetails.Columns.Add("Email_JE", typeof(string));
        dtDetails.Columns.Add("Phone_JE", typeof(string));
        dtDetails.Columns.Add("Mobile_JE", typeof(string));
        dtDetails.Columns.Add("LicenseNumber_JE", typeof(string));
        dtDetails.Columns.Add("IsPostalAddress_JE", typeof(bool));
        dtDetails.Columns.Add("UnitTypeID_JE", typeof(string));
        dtDetails.Columns.Add("UnitNumber_JE", typeof(string));
        dtDetails.Columns.Add("StreetNumber_JE", typeof(string));
        dtDetails.Columns.Add("StreetName_JE", typeof(string));
        dtDetails.Columns.Add("StreetTypeID_JE", typeof(string));
        dtDetails.Columns.Add("Town_JE", typeof(string));
        dtDetails.Columns.Add("State_JE", typeof(string));
        dtDetails.Columns.Add("PostCode_JE", typeof(string));

        //LstJobInverterDetail
        dtDetails.Columns.Add("Brand_LstJID", typeof(string));
        dtDetails.Columns.Add("Model_LstJID", typeof(string));
        dtDetails.Columns.Add("Series_LstJID", typeof(string));
        dtDetails.Columns.Add("NoOfInverter_LstJID", typeof(string));

        //LstJobPanelDetail
        dtDetails.Columns.Add("Brand_LstJPD", typeof(string));
        dtDetails.Columns.Add("Model_LstJPD", typeof(string));
        dtDetails.Columns.Add("NoOfInverter_LstJPD", typeof(int));

        //JobSTCStatusData
        dtDetails.Columns.Add("STCStatus_JSTCD", typeof(string));
        dtDetails.Columns.Add("CalculatedSTC_JSTCD", typeof(string));
        dtDetails.Columns.Add("STCPrice_JSTCD", typeof(string));
        dtDetails.Columns.Add("FailureNotice_JSTCD", typeof(string));
        dtDetails.Columns.Add("ComplianceNotes_JSTCD", typeof(string));
        dtDetails.Columns.Add("STCSubmissionDate_JSTCD", typeof(string));
        dtDetails.Columns.Add("STCInvoiceStatus_JSTCD", typeof(string));
        dtDetails.Columns.Add("IsInvoiced_JSTCD", typeof(bool));


        return dtDetails;
    }


    public class ClsBridgeSelect
    {
        public long startDate;
        public long endDate;
    }

    public class BSDetailsRoot
    {
        public List<Jobs> jobs { get; set; }
    }

    public class Jobs
    {
        public string Installer { get; set; }

        //public int crmid { get; set; }

        //Nullable<int> crmid { get; set; }

        [JsonProperty("crmid", NullValueHandling = NullValueHandling.Ignore)]
        public string Crmid { get; set; }

        [JsonProperty("panels")]
        public Dictionary<string, The0038500> Panels { get; set; }

        //[JsonProperty("inverters")]
        //public Inverters Inverters { get; set; }
        [JsonProperty("inverters")]
        public Dictionary<string, The0038500> Inverters { get; set; }
    }

    public partial class The0038500
    {
        [JsonProperty("s")]
        public S S { get; set; }

        [JsonProperty("t", NullValueHandling = NullValueHandling.Ignore)]
        public long? T { get; set; }
    }

    public enum S { N, U, V };

    protected void BS_Fetch()
    {
        try
        {
            string FromDate = "31/08/2021";
            string ToDate = "15/09/2021";

            long _SDate = DateTimeStamp(FromDate);
            long _EDate = DateTimeStamp(ToDate);

            var obj = new ClsBridgeSelect
            {
                startDate = _SDate,
                endDate = _EDate
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/find/jobs/date/";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<BSDetailsRoot> JsonData = client.Execute<BSDetailsRoot>(request);

            BSDetailsRoot jobData = JsonConvert.DeserializeObject<BSDetailsRoot>(JsonData.Content);

            DataTable dtSerialNo = new DataTable();
            dtSerialNo.Columns.Add("ProjectNo", typeof(string));
            dtSerialNo.Columns.Add("SerialNo", typeof(string));
            dtSerialNo.Columns.Add("CompanyID", typeof(int));
            dtSerialNo.Columns.Add("Varified", typeof(string));
            dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
            dtSerialNo.Columns.Add("InstallerName", typeof(string));
            for (int i = 0; i < jobData.jobs.Count; i++)
            {
                Jobs jobs = jobData.jobs[i];

                string InstallerName = jobs.Installer;

                string ProjectNo = jobs.Crmid == null ? "" : jobs.Crmid.ToString().Trim();
                //string ProjectNo = "";
                //string ProjectNo = !string.IsNullOrEmpty(jobs.Crmid.ToString()) ? trimmer.Replace(jobs.Crmid.ToString(), "") : "";

                //foreach (var item in jobs.Panels)
                //{
                //    string SerialNo = item.Key;
                //    The0038500 PanelStatus = item.Value;
                //    string Verified = PanelStatus.S.ToString();

                //    DataRow dr = dtSerialNo.NewRow();
                //    dr["ProjectNo"] = ProjectNo;
                //    dr["SerialNo"] = SerialNo;
                //    dr["BSGBFlag"] = 2; // GreenBot
                //    dr["CompanyID"] = CompanyID;
                //    dr["Varified"] = Verified;
                //    dr["StockCategoryID"] = 1;
                //    dr["InstallerName"] = InstallerName;
                //    dtSerialNo.Rows.Add(dr);
                //}

                foreach (var item in jobs.Inverters)
                {
                    string SerialNo = item.Key;
                    The0038500 inverterStatus = item.Value;
                    string Verified = inverterStatus.S.ToString();

                    DataRow dr = dtSerialNo.NewRow();
                    dr["ProjectNo"] = ProjectNo;
                    dr["SerialNo"] = SerialNo;
                    dr["CompanyID"] = 1;
                    dr["Varified"] = Verified;
                    dr["StockCategoryID"] = 2;
                    dr["InstallerName"] = InstallerName;
                    dtSerialNo.Rows.Add(dr);
                }
            }

            int UpdateData = ClsGreenbotAllData.Bulk_InsertUpdate_tbl_BSAllSerialInverterSerialNo(dtSerialNo);

            string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";

            Response.Write(msg);
        }
        catch(Exception ex)
        {
            
        }
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    public static long DateTimeStamp(string date)
    {
        DateTime Date = Convert.ToDateTime(date).Date;

        //DateTime value = DateTime.Now;
        long epoch = (Date.Ticks - 621355968000000000) / 10000;

        return epoch;
    }
}