using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //  int suc = clstblcustomers.tbl_custimage_insert("2", "test");
        //bool suc2 = clstblcustomers.tbl_custimage_updateimage(suc.tostring(), "nirali");

        if (!IsPostBack)
        {

            //Membership.CreateUser("admin", "Camlin357", "admin@urosolar.com.au");
            // Roles.AddUserToRole("Divya", "Sales Manager");
            //Roles.CreateRole("Sales Manager");
            //Roles.CreateRole("SalesRep");
            //Roles.CreateRole("STC");
            //Roles.CreateRole("SubAdministrator");
            //Roles.CreateRole("Verification");
            //Roles.CreateRole("WarehouseManager");
            //Roles.CreateRole("Administrator");
            //Roles.CreateRole("Administrator");
            //Roles.CreateRole("Administrator");

            //StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("2");
            //SiteName = st.sitename;

            //Siteurl = SiteConfiguration.GetURL();
            //SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            HyperLink HyperLink1 = (HyperLink)login1.FindControl("HyperLink1");
            HyperLink1.NavigateUrl = "admin/forgotpassword.aspx";
            if (Request.Cookies["myCookie"] != null)
            {
                HttpCookie cookie = Request.Cookies.Get("myCookie");
                if (Request.Cookies["Uname"] != null)
                {
                    string username = cookie.Values["Uname"].ToString();
                    string password = cookie.Values["Pass"].ToString();

                    TextBox uname = (TextBox)login1.FindControl("UserName");
                    uname.Attributes.Add("value", username);

                    TextBox pass = (TextBox)login1.FindControl("Password");
                    pass.TextMode = TextBoxMode.Password;
                    pass.Attributes.Add("value", password);
                    login1.RememberMeSet = true;
                }
            }
        }
        //TextBox txt = (TextBox)login1.FindControl("UserName");
        //txt.Focus();
    }
    public void Login1_LoggedIn(object sender, EventArgs e)
    {
        CheckBox rm = (CheckBox)login1.FindControl("RememberMe");
        TextBox UserName = (TextBox)login1.FindControl("UserName");
        if (rm.Checked)
        {
            HttpCookie myCookie = new HttpCookie("myCookie");
            Response.Cookies.Remove("myCookie");
            Response.Cookies.Add(myCookie);

            myCookie.Values.Add("Uname", this.login1.UserName.ToString());
            myCookie.Values.Add("Pass", this.login1.Password.ToString());
            DateTime dtExpiry = DateTime.Now.AddDays(100);

            Response.Cookies["myCookie"].Expires = dtExpiry;
            Response.Redirect("~/admin/adminfiles/dashboard.aspx");
        }
        else
        {
            HttpCookie myCookie = new HttpCookie("myCookie");

            Response.Cookies.Remove("myCookie");
            DateTime dtExpiry = DateTime.Now.AddDays(10);
            Response.Cookies["myCookie"].Expires = dtExpiry;
            Response.Redirect("~/admin/adminfiles/dashboard.aspx");
        }

        //DataTable dt = ClsAdminUtilities.Aspnet_Users_GetRole(UserName.Text);
        //if (dt.Rows.Count > 0)
        //{
        //    if (dt.Rows[0]["rolename"].ToString() == "Administrator")
        //    {
        //        Response.Redirect("~/admin/adminfiles/dashboard.aspx");
        //    }

        //}

    }
}
