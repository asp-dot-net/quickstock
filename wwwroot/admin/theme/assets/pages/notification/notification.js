  'use strict';
$(window).on('load',function(){
    //Welcome Message (not for login page)
    //function notify(message, type){
    //    $.growl({
    //        message: message
    //    },{
    //        type: type,
    //        allow_dismiss: true,
    //        label: 'Cancel',
    //        className: 'btn-xs btn-inverse',
    //        placement: {
    //            from: 'top',
    //            align: 'right'
    //        },
    //        delay: 250000,
    //        animate: {
    //                enter: 'animated fadeInRight',
    //                exit: 'animated fadeOutRight'
    //        },
    //        offset: {
    //            x: 30,
    //            y: 30
    //        }
    //    });
    //};

    function notifymsg(message, type) {
        $.growl({
            message: message
        }, {
                type: type,
                allow_dismiss: true,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 3000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
    }

   
       //notify('Welcome to Notification page', 'inverse');
   
});

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_pageLoaded(pageLoadedpro);

//raised before processing of an asynchronous postback starts and the postback request is sent to the server.
prm.add_beginRequest(beginrequesthandler);

// raised after an asynchronous postback is finished and control has been returned to the browser.
prm.add_endRequest(endrequesthandler);

function beginrequesthandler(sender, args) {


    //shows the modal popup - the update progress
    document.getElementById('loader_div').style.visibility = "visible";

}
function endrequesthandler(sender, args) {

    //hide the modal popup - the update progress
}

function pageLoadedpro() {

    $(document).ready(function () {

        function toaster(msg) {
            //alert("54345");
            notifymsg(msg, 'inverse');
        }

        function notifymsg(message, type) {
            $.growl({
                message: message
            }, {
                    type: type,
                    allow_dismiss: true,
                    label: 'Cancel',
                    className: 'btn-xs btn-inverse',
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    delay: 3000,
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    },
                    offset: {
                        x: 30,
                        y: 30
                    }
                });
        }
    });

}

$(document).ready(function() {
   
    /*--------------------------------------
         Notifications & Dialogs
     ---------------------------------------*/
    /*
     * Notifications
     */
    function toaster(msg) {
        //alert("54345");
        notifymsg(msg, 'inverse');
    }

    function notify(from, align, icon, type, animIn, animOut){
        $.growl({
            icon: icon,
            title: ' Bootstrap Growl ',
            message: 'Turning standard Bootstrap alerts into awesome notifications',
            url: ''
        },{
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: from,
                align: align
            },
            offset: {
                x: 30,
                y: 30
            },
            spacing: 10,
            z_index: 999999,
            delay: 2500,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {
                enter: animIn,
                exit: animOut
            },
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert">' +
            '<button type="button" class="close" data-growl="dismiss">' +
            '<span aria-hidden="true">&times;</span>' +
            '<span class="sr-only">Close</span>' +
            '</button>' +
            '<span data-growl="icon"></span>' +
            '<span data-growl="title"></span>' +
            '<span data-growl="message"></span>' +
            '<a href="#" data-growl="url"></a>' +
            '</div>'
        });
    };

    $('.notifications .btn').on('click',function(e){
        e.preventDefault();
        var nFrom = $(this).attr('data-from');
        var nAlign = $(this).attr('data-align');
        var nIcons = $(this).attr('data-icon');
        var nType = $(this).attr('data-type');
        var nAnimIn = $(this).attr('data-animation-in');
        var nAnimOut = $(this).attr('data-animation-out');

        notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
    });

});

function notifymsg(message, type) {
    $.growl({
        message: message
    }, {
            type: type,
            allow_dismiss: true,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 3000,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },
            offset: {
                x: 30,
                y: 30
            }
        });
}


function toaster(msg) {
    //alert("54345");
    notifymsg(msg, 'inverse');
}

