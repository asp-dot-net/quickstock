﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/loginmaster.master" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
.toast-error2{background-color:#fff!important;
 background: #fff!important; padding:10px; border:1px solid #eaeaea; border-radius:3px;
    border-left: 6px solid #e74c3c!important;}
</style>
<div class="dashboardlogin">
    <div class="login-container login-card auth-body ">
        <div class="row">
            <div class="col-md-12">
                   
                    <asp:Panel ID="PanSuccess" runat="server" Visible="false" CssClass="alert alert-success">

                            <strong>Success ! </strong>
                            &nbsp;
                            <asp:Label ID="lblSuccess" runat="server"
                                Text="Your password has been sent to you. Please check your Email."></asp:Label>

                        </asp:Panel>
                        <asp:Panel ID="Panfail" runat="server" Visible="false">
                            <div class="toast-error2">
                                <strong>
                                    <asp:Literal ID="Literal2" runat="server" Text="Failure !"></asp:Literal>
                                    <%-- Success !--%>
                                </strong>
                                <br />
                                <asp:Label ID="Label1" runat="server"
                                    Text="Invalid Username"></asp:Label>
                                <%-- password_change--%>
                            </div>
                        </asp:Panel>
            <div>
                <div class="auth-box" id="tabform" runat="server">
                        <div class="text-center">
                                <img src="../images/logo_Login.png">
                        </div>
                        <div class="row m-b-20">

                                <div class="col-md-12">
                                    <h3 class="txt-primary">Forgot Password</h3>
                                </div>
                            </div>
                            <div class="input-group"><span class="input-group-prepend"><label class="input-group-text"><i class="icofont icofont-ui-user"></i></label></span>
                                <asp:TextBox CssClass="form-control" ID="UserName" runat="server" MaxLength="100" Width="300" placeholder="User Name"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalUserName" runat="server"
                                    ControlToValidate="UserName" CssClass="requiredfield" ErrorMessage="* Required"
                                    Display="dynamic" ValidationGroup="PasswordRecovery1"></asp:RequiredFieldValidator>
                            </div>
                            
                          
                            <div class="row">
                            <div class="col-md-12 text-right">
                                    
                                    <asp:LinkButton ID="SubmitButton" CausesValidation="true" CssClass="btn btn-primary btn-md waves-effect text-center m-b-5 btn-block" runat="server"
                                    OnClick="SubmitButton_Click1" CommandName="Submit" ValidationGroup="PasswordRecovery1" 
                                    Text=" Reset Passowrd " />
                                <asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="btn btn-danger btn-md waves-effect text-center m-b-20 btn-block" runat="server"
                                    OnClick="LinkButton1_Click" 
                                    Text=" Cancel " />
                                </div>
                            </div>
                </div>
            </div> 
                
              
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <%--<strong>HOMER</strong> - AngularJS Responsive WebApp--%>
                <%--<br />--%>
               <!-- 2016 Copyright Achievers Energy-->
            </div>
        </div>
    </div>
</div>
</asp:Content>

