<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="utilities.aspx.cs" Inherits="admin_adminfiles_utilities_utilities"
    Title="Utilities | Manage Utilities" Theme="admin" Culture="en-us" UICulture="en-us"
    EnableEventValidation="true" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="breadcrumb">
        Manage Utilities
    </div>
    <div id="midArea">
        <div id="midAreaTop">
            <span id="midAreaRight"></span><span id="midAreaLeft"></span>
        </div>
        <div id="midAreaCont">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <h2 class="Blue">
                            Manage Utilities logo.png</h2>
                    </td>
                </tr>
            </table>
            <div class="hr">
            </div>
            <asp:Panel ID="PanSuccess" runat="server" CssClass="tick">
                <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanError" runat="server" CssClass="cross">
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanNoRecord" runat="server" CssClass="cross">
                <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
            </asp:Panel>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <%-- END    : Select Panels --%>
                        <%-- START  : ADD / UPDATE Panels --%>
                        <asp:Panel ID="PanAddUpdate" runat="server" Visible="false" CssClass="PanAddUpdate">
                            <table cellpadding="5" cellspacing="0" border="1" width="100%" class="tblDisplay widthPc"
                                summary="">
                                <tr>
                                    <td colspan="2">
                                        <h2 class="Blue">
                                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                            Utilities</h2>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="widthLeft" valign="top">
                                        Site Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbsitename" runat="server" Width="200" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="widthLeft" valign="top">
                                        Site Logo
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tblogo" runat="server" Width="200" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Site URL
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbsiteurl" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/admin/images/save.gif"
                                            OnClick="btnUpdate_Click" Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%-- END    : ADD / UPDATE Panels --%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="midAreaBot">
            <span id="midBotRight"></span><span id="midBotLeft"></span>
        </div>
    </div>
</asp:Content>
