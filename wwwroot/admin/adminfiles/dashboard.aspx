<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="admin_adminfiles_dashboard" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<%@ Register Src="~/includes/dashboard/revertitem.ascx" TagPrefix="uc5" TagName="revertitem" %>
<%@ Register Src="~/includes/dashboard/WebUserControl.ascx" TagPrefix="uc1" TagName="MinPurchase" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdateDashboard">
        <ContentTemplate>
            <style type="text/css">
                .selected_row {
                    background-color: #A1DCF2 !important;
                }

                .table_row_red {
                    border-width: 3px;
                    padding-bottom: 10px;
                    font-size: 16px;
                }

                .table_row_normal {
                    font-size: 15px;
                }


                .header_orage {
                    display: flex;
                    flex-direction: row;
                    justify-content: space-between;
                    align-items: center;
                    background: #fb6e52;
                    padding: 10px;
                }

                .hd1 {
                    width: 60%;
                    font-size: 20px;
                    color: #fff !important;
                }

                .shd_right {
                    width: 40%;
                    display: flex;
                    flex-direction: row;
                    flex-wrap;
                    justify-content: flex-end;
                }

                .excel_right {
                    width: 9%;
                    font-size: 20px;
                    display: flex;
                    flex-direction: row;
                    flex-wrap;
                    justify-content: flex-end;
                }

                .box_right {
                    margin-left: 15px;
                }

                .width150 {
                    min-width: 160px;
                }
            </style>
            <div class="col-lg-12 text-center m-t-md">
                <h2>Welcome <asp:Label ID="lblusername" runat="server"></asp:Label>
                </h2>
                <p>
                    <strong>Login Detail:</strong>
                    <strong>Date: </strong><asp:Label ID="lbldate" runat="server"></asp:Label>
                    <strong>Time: </strong><asp:Label ID="lbltime" runat="server"></asp:Label>
                </p>
            </div>

            <section class="minheight500 dnone">
                <div class="card" style="display: none">
                    <div class="card-header"></div>
                    <div class="card-block">
                        <%--     <div class="col-md-12">
         <uc3:leadcount runat="server" ID="leadcount1" visible="true" />
         <uc4:weeklystatus runat="server" ID="weeklystatus1" visible="true"/>         
         </div>--%>
                        <%--  <div>
              <uc5:revertitem runat="server" ID="revertitem2" visible="true"/>
         </div>--%>
                    </div>
                </div>
            </section>



            <section visible="false" id="divTeamStatus" runat="server">
                <div class="col-md-12">
                    <%--<uc1:MinPurchase runat="server" ID="MinPurchase1" />--%>
                    <div class="col-md-12">
                        <div class="header_orage">
                            <div class="hd1">Stock Details</div>
                            <div class="shd_right dnone">
                                <div class="box_right width150">
                                    <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <%--<div class="box_right">
                                    <asp:Button runat="server" ID="btnGo" CssClass="btn btn-primary btn-xs" Text="Go" OnClick="btnGo_Click" />
                                </div>--%>
                            </div>
                            <div class="excel_right">
                                <div style="margin-right:5px!important" class="width150">
                                <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval"
                                    OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="">Category</asp:ListItem>
                                    </asp:DropDownList>
                                </Div>
                                <div style="margin-right:5px!important">
                                <asp:LinkButton ID="lbtnExport" runat="server" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                 CausesValidation="false" Style="background-color: #218838;border-color:#218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                </div>
                                <div>
                                    <asp:LinkButton ID="lbtnExportV2" runat="server" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExportV2_Click"
                                 CausesValidation="false" Style="background-color: #218838;border-color:#218838"><i class="fa fa-file-excel-o"></i> Excel V2 </asp:LinkButton>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-12" style="padding: 0px;">
                            <div class="table-responsive well" runat="server" id="divdeprec">
                                <div class="messesgarea">
                                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                    </div>
                                </div>
                                <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server"
                                    CssClass="table table-hover" AutoGenerateColumns="False" HeaderStyle-BorderWidth="2" HeaderStyle-CssClass="bordered-darkorange" BorderWidth="0" GridLines="Horizontal">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stock Item" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("StockItem")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("CompanyLocation")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="250px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current Stock" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("Current_Stock")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-CssClass="table_row_red">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblHeaderNetSituation" runat="server" data-original-title="((Live Qty + Stk Order + ETA + TargetDate + ThirdPartyStock) 
                                                            - (AriseSold + SMSold + WholesaleDeduct))"
                                                    data-toggle="tooltip" data-placement="top">
                                                            Net Situation
                                                </asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("NetSituation")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                            <HeaderStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-CssClass="table_row_red" HeaderText="Quantity Sold">
                                            <ItemTemplate>
                                                <%# Eval("QuantitySold")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                            <HeaderStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Min. Quantity" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("Min_Quantity")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("NetSold")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Target Quantity" HeaderStyle-CssClass="table_row_red">
                                            <ItemTemplate>
                                                <%# Eval("OrderQuantity")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                      
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                </div>
            </section>





            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //$('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //$('.loading-container').css('display', 'none');
                    //if (args.get_error() != undefined) {
                    //    args.set_errorhandled(true);
                    //}
                }
                function pageLoaded() {
                    //$('.loading-container').css('display', 'none');
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });



                }
            </script>




            <style type="text/css">
                .selected_row {
                    background-color: #A1DCF2 !important;
                }
            </style>

            </script>
    <script type="text/javascript">


        $(function () {
            $("[id*=GridViewLastFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewLastFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=GridViewNextFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewNextFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=GridViewNewLead] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewNewLead] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

        });
        function doMyAction() {
            if ($.fn.select2 !== 'undefined') {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                $(".myval1").select2({
                    minimumResultsForSearch: -1
                });
            }
        }
    </script>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="lbtnExportV2" />
            <asp:PostBackTrigger ControlID="ddlCategory" />
        </Triggers>
    </asp:UpdatePanel>
    
</asp:Content>
