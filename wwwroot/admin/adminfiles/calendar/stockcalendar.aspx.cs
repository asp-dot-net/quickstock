using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_calendar_stockcalendar : System.Web.UI.Page
{
    protected string SiteURL;

    protected void Page_Load(object sender, EventArgs e)
    {
            
        if (!IsPostBack)
        {
           
            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
                      
            SiteURL = st1.siteurl;


   


        }
    }
    
    protected void GridViewNewLead_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //GridViewNewLead.PageIndex = e.NewPageIndex;
       // BindNewLead();
    }
    public void BindLastFollowUp()
    {
        //  PanGridLast10FollowUps.Visible = true;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";

        DataTable dt = ClstblCustInfo.tblCustInfo_TodayFollowup(EmployeeID);

    }

    protected void GridViewLastFollowUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // GridViewLastFollowUp.PageIndex = e.NewPageIndex;
        BindLastFollowUp();
    }

   
    protected void GridViewNewLead_RowCreated(object sender, GridViewRowEventArgs e)
    {
        
    }
  
    protected void btnFollowupgo_Click(object sender, EventArgs e)
    {
        BindLastFollowUp();
        BindScript();
    }
    protected void btnLead_Click(object sender, EventArgs e)
    {
       // BindNewLead();
        BindScript();
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(UpdateLead, this.GetType(), "MyAction", "doMyAction();", true);
    }
}
