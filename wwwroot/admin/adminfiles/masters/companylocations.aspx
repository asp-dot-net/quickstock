<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="companylocations.aspx.cs" Inherits="admin_adminfiles_master_companylocations" %>

    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

           <%-- <asp:UpdatePanel ID="updatepanelgrid" runat="server">
                <ContentTemplate>--%>
                    <style type="text/css">
                        .table-responsive {
                            width: 100%;
                            overflow-x: auto;
                            display: inline
                        }
                        
                        .Popup-Zindex {
                            z-index: 901!important;
                        }
                    </style>

         

              <script>

                //function DoFocus(fld) {
                //    fld.className = 'form-control modaltextbox bcolor';
                //}

                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });

                function toaster(msg = "Record Exist.") {
                    //alert("54345");
                    notifymsg(msg, 'inverse');
                }

                function notifymsg(message, type) {
                    $.growl({
                        message: message
                    }, {
                            type: type,
                            allow_dismiss: true,
                            label: 'Cancel',
                            className: 'btn-xs btn-inverse',
                            placement: {
                                from: 'top',
                                align: 'right'
                            },
                            delay: 30000,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            },
                            offset: {
                                x: 30,
                                y: 30
                            }
                        });
                }

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $(".myvalcomloc").select2({
                                //placeholder: "select",
                                allowclear: true,
                                minimumResultsForSearch: -1
                            });

                    $('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    })

                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    })





                    function toaster(msg) {
                        //alert("54345");
                        notifymsg(msg, 'inverse');
                    }

                    $(document).ready(function () {
                       

                        $('[data-toggle="tooltip"]').tooltip({
                            trigger: 'hover'
                        });

                        function toaster(msg) {
                            //alert("54345");
                            notifymsg(msg, 'inverse');
                        }
                        //$(".myval").select2({
                        //    //placeholder: "select",
                        //    allowclear: true,
                        //    minimumResultsForSearch: -1
                        //});
                    });



                }
               
            </script>

                 <%--   <script>
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_pageLoaded(pageLoadedpro);
                        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                        prm.add_beginRequest(beginrequesthandler);
                        // raised after an asynchronous postback is finished and control has been returned to the browser.
                        prm.add_endRequest(endrequesthandler);

                        function beginrequesthandler(sender, args) {
                            //shows the modal popup - the update progress
                            //  $('.loader_div').removeClass('loading-inactive');
                            document.getElementById('loader_div').style.visibility = "visible";
                        }

                        function endrequesthandler(sender, args) {
                            //hide the modal popup - the update progress
                            //document.getElementById('interactive');
                            //document.getElementById('loader_div').style.visibility = "hidden";
                            //document.getElementById('loader').style.visibility = "false";
                        }

                        function pageLoadedpro() {

                            document.getElementById('loader_div').style.visibility = "hidden";


                            $('#<%=btnAdd.ClientID %>').click(function(e) {

                                formValidate();
                            });
                            $('#<%=btnUpdate.ClientID %>').click(function(e) {
                                formValidate();
                            });

                            $('body').removeClass('modal-open');

                            $('.modal-backdrop').remove();

                            $(document).ready(function() {
                                $('[data-toggle="tooltip"]').tooltip({
                                    trigger: 'hover'
                                });
                            });


                            //$('[data-toggle="tooltip"]').tooltip({
                            //  trigger: 'hover'
                            //})

                            // $("[data-toggle=tooltip]").tooltip();

                            $(".myvalcomloc").select2({
                                //placeholder: "select",
                                allowclear: true,
                                minimumResultsForSearch: -1
                            });

                        }

                        function ShowProgress() {
                            setTimeout(function() {
                                //alert("check");
                                if (Page_IsValid) {
                                    //alert("true");
                                    //$('.modal_popup').css('display', 'none');
                                    $('.modal_popup').css('z-index', '901');
                                    //this is because when popup is open, and loader is called background color becomes dark.
                                    document.getElementById('loader_div').style.visibility = "visible";
                                } else {
                                    //alert("false");
                                }
                            }, 200);
                        }
                        $(function() {
                            $('form').on("click", '#<%=lnkokreset.ClientID %>', function() {
                                //  $('form').on('submit', function () {
                                //alert(this.selector);
                                ShowProgress();
                            });
                        });
                    </script>--%>

                    <div class="page-header card">
                        <div class="card-block">
                            <h5>Manage Company Locations

                                <div id="hbreadcrumb" class="pull-right">

                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary btnaddicon"> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>

                                </div>
                            </h5>
                            <div class="clear"></div>


                        </div>
                    </div>





                    <div class="finaladdupdate">
                        <div id="PanAddUpdate" runat="server" visible="false">
                            <div class="panel-body animate-panel padtopzero">
                                <div class="card  addform">
                                    <div class="card-header bordered-blue">
                                        <h5>
                                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                            Company Locations
                                        </h5>
                                    </div>
                                    <div class="card-block padleft25">
                                        <div class="form-horizontal">
                                            
                                            <div class="form-group row">
                                                <asp:Label ID="lblFirstName" runat="server" class="col-sm-3 control-label">
                                                    Company Locations</asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtCompanyLocation" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Display="dynamic" ControlToValidate="txtCompanyLocation" ErrorMessage="" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="lblLastName" runat="server" class="col-sm-3 control-label">
                                                    State</asp:Label>
                                                <div class="col-sm-3 drpValidate">
                                                    <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalcomloc">
                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic" ControlToValidate="ddlState" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="reqerror" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="ddlState" InitialValue="" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="seq" runat="server" visible="false">
                                                <asp:Label ID="lblMobile" runat="server" class="col-sm-3 control-label">
                                                    <strong>Seq</strong></asp:Label>
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtSeq" runat="server" MaxLength="3" Text="0" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regvalSortOrder" runat="server" ErrorMessage="Number Only" ValidationExpression="^[0-9]*$" ControlToValidate="txtSeq" ValidationGroup="Req" ForeColor="#F47442"></asp:RegularExpressionValidator>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" ControlToValidate="txtSeq" Display="Dynamic" CssClass="reqerror" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group checkareanew row">
                                                <div class="col-sm-3 rightalign right-text">
                                                    <asp:Label ID="Label2" runat="server" class="control-label">
                                                        Is Active?</asp:Label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div>


                                                        <div class="checkbox-fade fade-in-primary d-">

                                                            <label for="<%=chkActive.ClientID %>">
                                                        <asp:CheckBox ID="chkActive" runat="server" />
                                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                        <asp:CheckBox ID="RememberMe" runat="server" />

                                                    </label>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req" Text="Add" UseSubmitBehavior="false" />
                                                    <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" ValidationGroup="Req" Text="Save" Visible="false" UseSubmitBehavior="false" />
                                                    <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click" CausesValidation="false" Text="Reset" UseSubmitBehavior="false" />
                                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel" UseSubmitBehavior="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body padtopzero">
                        <asp:Panel runat="server" ID="PanGridSearch">
                            <div class="animate-panelmessesgarea padbtmzero">
                                <div class="alert alert-success" id="PanSuccess" runat="server">

                                    <%--  <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>--%>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <%--<i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>--%>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                    <%-- <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>--%>
                                </div>
                                <div class="alert alert-info" id="PanNoRecord" runat="server">
                                    <%-- <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>--%>
                                </div>
                            </div>
                            <div class="searchfinal">

                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Company Locations" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch" WatermarkText="Company Locations" />
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170 fullWidth">
                                                        <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomloc">
                                                            <asp:ListItem Value="">All Records</asp:ListItem>
                                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Active</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-1 martop5 max_width170 fullWidth">
                                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" UseSubmitBehavior="false" />
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left" CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="datashowbox inlineblock">
                                            <div class="row">
                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myvalcomloc">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="finalgrid">
                                <div class="xsroll">
                                    <div id="PanGrid" runat="server">
                                        <div class="card shadownone brdrgray">
                                            <div class="card-block">
                                                <div>
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridView1" DataKeyNames="CompanyLocationID" runat="server" CssClass="tooltip-demo table table-bordered dataTable" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Company Locations" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                                            <%#Eval("CompanyLocation")%>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="brdnoneleft" />
                                                                    <HeaderStyle CssClass="brdnoneleft" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                                            <%#Eval("State")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Seq" ItemStyle-Width="100px" HeaderStyle-Width="100px" Visible="false" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Seq")%>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <span class="btn btn-primary btn-mini waves-effect waves-light">
                                                                    <%#Eval("Active").ToString()=="True"?"<i class='fa fa-check-square-o'></i>":"<i class='fa fa-square-o'></i>"%>

                                                                    <%--    <asp:CheckBox ID="chkStatus" runat="server" Checked='<%# Eval("Active")%>'
                                                                        /><i class="fa-check-square-o"></i>--%>
                                                                    <%-- <label for="<%=chkStatus.ClientID %>"
                                                                        runat="server" id="lblchk">
                                                                        <span></span>
                                                                        </label>--%> Active</span>
                                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-mini" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        </asp:LinkButton>
                                                                      
                                                                         <%--Users --%>
                                                                            <asp:LinkButton ID="lbtnViewUsers" runat="server" CssClass="btn btn-inverse btn-mini waves-effect waves-light" CommandName="Users" data-original-title="View Users" data-toggle="tooltip" data-placement="top" CommandArgument='<%#Eval("CompanyLocationID")%>'
                                                                                CausesValidation="false">
                                                                                <i class="fa fa-eye"></i> Users
                                                                            </asp:LinkButton>
                                                                            <!--DELETE Modal Templates-->
                                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini waves-effect waves-light" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("CompanyLocationID")%>'>
                                                                                <i class="fa fa-trash"></i> Delete
                                                                            </asp:LinkButton>

                                                                            <!--END DELETE Modal Templates-->

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <AlternatingRowStyle />
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination" id="divpage" runat="server">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="paginationnew1" runat="server" id="divnopage">
                                                    <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                <%--            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                                <ProgressTemplate>
                                    <div class="splash">
                                        <div class="color-line"></div>
                                        <div class="splash-title">
                                            <%--<h1>Homer - Responsive Admin Theme</h1>
                <p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p>--%>
                                  <%--              <div class="spinner">
                                                    <div class="rect1"></div>
                                                    <div class="rect2"></div>
                                                    <div class="rect3"></div>
                                                    <div class="rect4"></div>
                                                    <div class="rect5"></div>
                                                </div>
                                        </div>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                            <%--<cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1" PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />--%>


                        </asp:Panel>
                    </div>

                    <!--Email Modal Templates-->
                    <div id="myModal" style="display: none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="To" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Subject" required="">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Content" rows="5" required=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnid" runat="server" />
                    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground" PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete">
                    </cc1:ModalPopupExtender>
                    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message">

                        <div class="modal-dialog">
                            <div class=" modal-content ">
                                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                                    <div class="modal-header">
                                        <h5 class="modal-title fullWidth">Delete
                                            <%--  <div style="float: right">
                                                <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                            </div>--%>
                                        </h5>
                                    </div>
                                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                                    <div class="modal-footer " style="text-align: center">
                                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton5" runat="server" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Cancel</span></asp:LinkButton>
                                    </div>
                            </div>
                        </div>

                    </div>

                    <asp:HiddenField ID="hdndelete" runat="server" />


                    <asp:Button ID="btnresetpop" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtenderReset" runat="server" BackgroundCssClass="modalbackground" PopupControlID="divreset" DropShadow="false" CancelControlID="blnkcancelrest" OkControlID="btnOKMobile12" TargetControlID="btnresetpop">
                    </cc1:ModalPopupExtender>
                    <div id="divreset" runat="server" style="display: none" class="modal_popup modal-danger modal-message">

                        <div class="modal-dialog">
                            <div class=" modal-content ">
                                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                                    <div class="modal-header">
                                        <h5 class="modal-title fullWidth">Reset
                                            <%--  <div style="float: right">
                                                <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                            </div>--%>
                                        </h5>
                                    </div>
                                    <div class="modal-body ">Are you sure you want to change password?</div>
                                    <div class="modal-footer " style="text-align: center">
                                        <%--  <asp:Button ID="lnkokreset" runat="server" OnClick="lnkokreset_Click" class="btn btn-danger" Text="OK" />--%>
                                            <asp:LinkButton ID="lnkokreset" runat="server" OnClick="lnkokreset_Click" class="btn btn-danger">OK</asp:LinkButton>
                                            <asp:LinkButton ID="lnkcancelrest" runat="server" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Cancel</span></asp:LinkButton>
                                    </div>
                            </div>
                        </div>

                    </div>
                    <asp:HiddenField runat="server" ID="hdnCompanyLocationID" />

                    <asp:Button ID="btnnotifi" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtenderSuccess" runat="server" BackgroundCssClass="modalbackground" PopupControlID="divnotifi" DropShadow="false" CancelControlID="lnkclose" OkControlID="btnOKMobile123" TargetControlID="btnnotifi">
                    </cc1:ModalPopupExtender>
                    <div id="divnotifi" runat="server" style="display: none" class="modal_popup modal-danger modal-message">

                        <div class="modal-dialog">
                            <div class=" modal-content ">
                                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                                    <div class="modal-header" style="text-align:center;">
                                        <h5 class="modal-title fullWidth">Success
                                            <%--  <div style="float: right">
                                                <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                            </div>--%>
                                        </h5>
                                    </div>
                                    <div class="modal-body ">Your password has been reset and sent successfully on your email-id</div>
                                    <div class="modal-footer " style="text-align: center;justify-content:center;">
                                        <%--  <asp:Button ID="lnkokreset" runat="server" OnClick="lnkokreset_Click" class="btn btn-danger" Text="OK" />--%>
                                            <asp:LinkButton ID="lnkclose" runat="server" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Close</span></asp:LinkButton>
                                    </div>
                            </div>
                        </div>

                    </div>


          <%--      </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnAdd" />--%>
                        <%-- <asp:PostBackTrigger ControlID="lnkokreset" />--%>
            <%--    </Triggers>
            </asp:UpdatePanel>--%>

        </asp:Content>