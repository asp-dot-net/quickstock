using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_master_PurchaseCompany : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //chkActive.Focus();
            BindGrid(0);

        }
    }

    protected DataTable GetGridData()
    {
        DataTable dt = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        if (dt.Rows.Count == 0)
        {
            //PanSearch.Visible = false;
        }
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            dt = ClsPurchaseCompany.tbl_PurchaseCompanyGetDataBySearch(txtSearch.Text);
        }
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {

        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            // HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            //Response.Write("yes" + ddlSelectRecords.SelectedValue + "All" );
            //Response.End();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

            //foreach (GridViewRow row in GridView1.Rows)
            //{
            //    row.Cells[2].Enabled = false;
            //}
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string PurchaseComapnyName = txtpurchasecompany.Text;
        int exist = ClsPurchaseCompany.tbl_PurchaseCompany_Exist(PurchaseComapnyName);

        if(exist==0)
        {
            int success = ClsPurchaseCompany.tbl_PurchaseCompany_Insert(PurchaseComapnyName);
            bool SucAddress = ClsPurchaseCompany.tbl_PurchaseCompany_Update_Address(success.ToString(), txtAddress.Text);


            if (success != 0)
            {
                SetAdd();
            }
            else
            {
                SetError();
            }

            BindGrid(0);
        }
        else
        {
            IntExist();
            //SetExist();
            Notification("Record with this name already exists.");
        }
      



        //Reset();


        //--- do not chage this code end------

    }

    public void IntExist()
    {
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string PurchaseCompanyName = txtpurchasecompany.Text;
        int exist = ClsPurchaseCompany.tbl_PurchaseCompany_ID_Exist(id1, PurchaseCompanyName);
       
        if (exist == 0)
        {
            bool success = ClsPurchaseCompany.tbl_PurchaseCompany_Update(id1, PurchaseCompanyName);
            bool SucAddress = ClsPurchaseCompany.tbl_PurchaseCompany_Update_Address(id1, txtAddress.Text);
            if (success)
            {
                Notification("Transaction Successful!");
                // SetUpdate();
            }
            else
            {
                // InitUpdate();
                SetError();
            }
            Reset();
            BindGrid(0);
        }
        else {
            IntExist();
            //SetExist();
            Notification("Record with this name already exists.");
            
        }
            //--- do not chage this code Start------
      
        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;

        ModalPopupExtenderDelete.Show();

        BindGrid(0);

        //--- do not chage this code end------
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttbPurchaseCompany st = ClsPurchaseCompany.tbl_PurchaseCompany_SelectByPurchaseCompanyID(id);

        txtpurchasecompany.Text = st.PurchaseCompanyName;
        txtpurchasecompany.Enabled = true;
        txtAddress.Text = st.Address;
        txtAddress.Enabled = true;

        InitUpdate();

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful!");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        //PanSuccess.Visible = true;
        Notification("Transaction Successful!");
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful!");
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        //PanError.Visible = true;
        Notification("Transaction Failed!");
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        //PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtpurchasecompany.Text = string.Empty;
        txtAddress.Text = string.Empty;
        //txtSeq.Text = String.Empty;
       
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName.ToLower() == "reset")
        //{
        //    ModalPopupExtenderReset.Show();
        //    hdnpurchasecompanyID.Value = e.CommandArgument.ToString();
        //}
    }

    //protected void lnkokreset_Click(object sender, EventArgs e)
    //{
    //    string PurchaseCompanyID = hdnpurchasecompanyID.Value;
    //    if (!string.IsNullOrEmpty(PurchaseCompanyID))
    //    {
    //        SttbPurchaseCompany st = ClsPurchaseCompany.tbl_PurchaseCompany_SelectByPurchaseCompanyID(PurchaseCompanyID);
    //        string Userid = st.Userid;
    //        if (Userid != "")
    //        {
    //            Random random = new Random();
    //            string Password = Convert.ToString(random.Next(000000, 999999));
    //            bool suc = ClsClientuser.aspnet_Membership_UpdatePassword(Userid, Password);
    //            stuser stu = ClsClientuser.ClientUserAspnetUserGetDataStructByUserId(Userid);
    //            string UserName = "";
    //            string Emailid = "";
    //            UserName = stu.username;
    //            Emailid = stu.email;
    //            if (suc)
    //            {
    //                string from = ClsAdminUtilities.StUtilitiesGetDataStructById("1").from.ToString();
    //                TextWriter txtWriter = new StringWriter() as TextWriter;

    //                Server.Execute("~/mailtemplate/resetpassword.aspx?username=" + UserName + "&password=" + Password, txtWriter);
    //                try
    //                {
    //                    //Response.Write(txtWriter.ToString());
    //                    //Response.End();

    //                    Utilities.SendMail(from, Emailid, "Reset Password", txtWriter.ToString());
    //                    ModalPopupExtenderSuccess.Show();
    //                    //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Your password has been reset and sent successfully on your emailid')", true);                        
    //                }
    //                catch
    //                {
    //                }
    //            }
    //            else
    //            {
    //                Notification("Transaction failed!");
    //            }
    //        }
    //    }
    //}

    public void BindScript()
    {
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;

            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;
                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;
            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;
            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {

                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {

            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        //chkActive.Focus();
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {


    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
      
        BindGrid(0);
    }

    protected void lnkdelete_Click1(object sender, EventArgs e)
    {

        string ID = hdndelete.Value.ToString();


        hdnid.Value = ID;

        bool sucess1 = ClsPurchaseCompany.tbl_PurchaseCompany_Delete(ID);
        if (sucess1)
        {
            SetAdd();
        }
        else
        {
            SetError();
        }
        GridView1.EditIndex = -1;
        BindGrid(0);

    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    protected void lnkokreset_Click(object sender, EventArgs e)
    {
        //string PurchaseCompanyId = hdnpurchasecompanyID.Value;
        //if (!string.IsNullOrEmpty(PurchaseCompanyId))
        //{
        //    SttbPurchaseCompany st = ClsPurchaseCompany.tbl_PurchaseCompany_SelectByPurchaseCompanyID(PurchaseCompanyId);
        //}
        }
}
