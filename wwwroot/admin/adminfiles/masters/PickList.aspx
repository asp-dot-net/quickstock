﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PickList.aspx.cs" Inherits="admin_adminfiles_masters_PickList"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .table-responsive {
            width: 100%;
            overflow-x: auto;
            display: inline
        }

        .Popup-Zindex {
            z-index: 901 !important;
        }

        .sbtn {
            background-color: #53a93f !important;
            border-color: #53a93f !important;
            color: #fff;
        }
    </style>
    <!-- notification JS -->
    <script src="<%=Siteurl %>admin/theme/assets/js/bootstrap-growl.min.js"></script>
    <script src="<%=Siteurl %>admin/theme/assets/pages/notification/notification.js"></script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);


        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //  $('.loader_div').removeClass('loading-inactive');
            document.getElementById('loader_div').style.visibility = "visible";
        }

        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //document.getElementById('interactive');
            //document.getElementById('loader_div').style.visibility = "hidden";
            //document.getElementById('loader').style.visibility = "false";
        }

        function pageLoadedpro() {

            document.getElementById('loader_div').style.visibility = "hidden";

            <%--$('#<%=btnAdd.ClientID %>').click(function (e) {
                formValidate();
            });

            $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();
            });--%>

            $('body').removeClass('modal-open');

            $('.modal-backdrop').remove();

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    trigger: 'hover'
                });
            });

            //$('[data-toggle="tooltip"]').tooltip({
            //  trigger: 'hover'
            //})

            // $("[data-toggle=tooltip]").tooltip();

            $(".myvalcomloc").select2({
                //placeholder: "select",
                allowclear: true,
                minimumResultsForSearch: -1
            });

            $(".myval").select2({
                placeholder: "select",
                allowclear: true
            });
        }

        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    //$('.modal_popup').css('display', 'none');
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    document.getElementById('loader_div').style.visibility = "visible";
                } else {
                    //alert("false");
                }
            }, 200);
        }

        $(function () {
                           <%-- $('form').on("click", '#<%=lnkokreset.ClientID %>', function() {
                                //  $('form').on('submit', function () {
                                //alert(this.selector);
                                ShowProgress();
                            });--%>
            $(".myvalcomloc").select2({
                //placeholder: "select",
                allowclear: true,
                minimumResultsForSearch: -1
            });
        });

    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <div class="page-header card">
                <div class="card-block">
                    <h5>Generate Picklist                             
                     <div id="hbreadcrumb" class="pull-right">
                         <%--<asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary btnaddicon"> Add</asp:LinkButton>
                         <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>
                     </div>

                    </h5>
                </div>
                <div class="clear"></div>
            </div>

            <div class="searchfinal">
                <div class="card shadownone brdrgray pad10">
                    <div class="card-block">
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                            <div class="inlineblock martop5">
                                <div class="row">
                                    <div class="input-group col-sm-2 martop5 max_width170">
                                        <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0"
                                            class="myvalcomloc">
                                            <asp:ListItem Value="">Company</asp:ListItem>
                                            <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                            <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                            <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                            ValidationGroup="AddPicklist" ControlToValidate="ddlCompany" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="input-group col-sm-2 martop5 max_width170">
                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Project No." CssClass="form-control m-b"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch" WatermarkText="Project No." />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                            ValidationGroup="AddPicklist" ControlToValidate="txtSearch" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="input-group col-sm-1 martop5 max_width170 fullWidth">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false"
                                            CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" UseSubmitBehavior="false" />
                                    </div>

                                    <div class="input-group martop5 col-sm-1 max_width170">
                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left" CausesValidation="false" CssClass="btn btn-primary btnclear fullWidth"
                                            OnClick="btnClearAll_Click"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%--<div class="datashowbox inlineblock">
                                    <div class="row">
                                        <div class="input-group col-sm-2 martop5 max_width170">
                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myvalcomloc">
                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>--%>
                    </div>
                </div>
            </div>

            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <%--<asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>--%>
                                    Generate Picklist
                                </h5>
                            </div>
                            <div class="card-block padleft25">
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <span class="name disblock">
                                                <label>
                                                    Stock Category
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlPickListCategory" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomloc"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlPickListCategory" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hndInstallBookingDate" />
                                            </span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span class="name disblock">
                                                <label>
                                                    Stock With
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlStockWith" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomloc"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">With Installer</asp:ListItem>
                                                        <asp:ListItem Value="2">With Customer</asp:ListItem>
                                                        <asp:ListItem Value="3">With Transportation</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlStockWith" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                </div>
                                                <asp:HiddenField runat="server" ID="HiddenField1" />
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <asp:HiddenField ID="hnd1" runat="server" />
                                        <div class="col-md-12" style="position: relative">
                                            <asp:Repeater ID="rptPickList" runat="server" OnItemDataBound="rptPickList_ItemDataBound">
                                                <ItemTemplate>
                                                    <div class="form-group row">
                                                        <div class="col-sm-2">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Category
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:HiddenField ID="hndStockCategoryID" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                <asp:HiddenField ID="PicklistitemId" runat="server" Value='<%# Eval("PicklistitemId") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged1"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" CssClass="comperror" ForeColor="Red"
                                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Item
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" CssClass="comperror" ForeColor="Red"
                                                                        ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="AddPicklist"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Quantity
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:HiddenField ID="hndQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Text='<%# Eval("Qty") %>'></asp:TextBox>
                                                            </span>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Wal Qty
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtWallateQty" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="padding_top30">
                                                                <span class="name ">
                                                                    <!--
                                                                            <asp:CheckBox ID="chkdelete" runat="server" />
                                                                            <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                                <span></span>
                                                                            </label>
                                                                            <br />-->
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:LinkButton ID="litremovePicklistItem" runat="server" CssClass="btn-danger btn btn-xs" OnClick="litremovePicklistItem_Click" CausesValidation="false">
                                                                                                                                            <i class="fa fa-close"></i> Remove
                                                                    </asp:LinkButton>

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-5">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtPickListAgainReson" CssClass="form-control m-b" runat="server" TextMode="MultiLine" Width="100%" Height="80px"></asp:TextBox>
                                                </div>
                                            </span>
                                        </div>

                                        <div class="col-sm-5">
                                            <span class="name disblock">
                                                <label>
                                                    Notes for generating PickList again
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtPicklistAgainNotes" CssClass="form-control m-b" TextMode="MultiLine" runat="server" Width="100%" Height="80px"></asp:TextBox>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12" style="position: relative">
                                            <asp:LinkButton ID="lnkAddItem" runat="server" CssClass="btn-primary btn btn-xs" OnClick="lnkAddItem_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-plus"></i> Add Item
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnSavePickList" runat="server" CssClass="btn btn-xs sbtn" OnClick="btnSavePickList_Click" ValidationGroup="AddPicklist">
                                                                                                                <i class="fa fa-save"></i> Save
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnUpdatePicklist" Visible="false" runat="server" CssClass="btn btn-xs sbtn" OnClick="btnUpdatePicklist_Click" ValidationGroup="AddPicklist">
                                                                                                                <i class="fa fa-save"></i> Update
                                            </asp:LinkButton>

                                            <%-- <asp:LinkButton ID="lnkDownloadPicklist" runat="server" CssClass="btn btn-xs sbtn" OnClick="lnkDownloadPicklist_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-save"></i> Download
                                            </asp:LinkButton>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">

                    <div class="finalgrid">
                        <div class="xsroll">
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div>
                                            <div class="table-responsive BlockStructure">
                                                <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered dataTable" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                    OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowEditing="GridView1_RowEditing" OnRowDeleting="GridView1_RowDeleting">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Index" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                                            <ItemTemplate>
                                                                <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                                <%#Eval("ID")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Picklist Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("pcktype")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Category" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("PickListCategoryName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Stock With" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("StockWithName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Generated Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("InstallerName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="System Detail" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%--<%#Eval("SystemDetail")%>--%>
                                                                <asp:Label ID="lblstytemdetail" Width="100px" runat="server" Text='<%#Eval("SystemDetail")%>'
                                                                    data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetail")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCompanyLocation" Width="100px" runat="server" Text='<%#Eval("CompanyLocation")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reason" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReason" Width="100px" runat="server" Text='<%#Eval("Reason")%>'
                                                                    data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Reason")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Created By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("CreatedBy1")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Deduct On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("DeductOn", "{0:dd/MM/yyyy}")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="" ItemStyle-Width="20px" HeaderStyle-Width="50px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="btnUpdatepicklist" runat="server" CommandName="Edit" CommandArgument='<%#Eval("ID")%>' CausesValidation="false" class="btn btn-primary">
                                                                  <i class="fa fa-edit"></i>
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("ID")%>' CausesValidation="false" class="btn btn-danger">
                                                                                                                                        <i class="fa fa-trash"></i>
                                                                </asp:LinkButton>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <AlternatingRowStyle />
                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination" id="divpage" runat="server">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground" PopupControlID="modal_danger1" DropShadow="false" CancelControlID="LinkButton5" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger1" runat="server" style="display: none" class="modal_popup modal-danger modal-message">
                <div class="modal-dialog">
                    <div class=" modal-content ">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth">Delete
                            </h5>
                        </div>
                        <div class="modal-body ">
                            <asp:Label ID="lblMassage" runat="server" />
                        </div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkDeletePickList" runat="server" class="btn btn-danger" OnClick="lnkDeletePickList_Click" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton5" runat="server" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Cancel</span></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField runat="server" ID="hndPickDelete" />
            <!--Danger Modal Templates-->
            <%--<asp:Button ID="Button3" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDeleteNew" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger1" DropShadow="false" CancelControlID="lnkcancel1" TargetControlID="Button3">
            </cc1:ModalPopupExtender>
            <div id="modal_danger1" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>
                        <div class="modal-title">Delete</div>
                        <label id="Label22" runat="server"></label>
                        <div class="modal-body ">
                            <asp:Label ID="lblMassage" runat="server" />
                        </div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkDeletePickList" runat="server" CausesValidation="false" class="btn btn-danger" OnClick="lnkDeletePickList_Click">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel1" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hndDelete" runat="server" />--%>

            <!--End Danger Modal Templates-->
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
