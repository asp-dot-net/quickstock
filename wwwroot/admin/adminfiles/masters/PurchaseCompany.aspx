<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="PurchaseCompany.aspx.cs" Inherits="admin_adminfiles_master_PurchaseCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%-- <asp:UpdatePanel ID="updatepanelgrid" runat="server">
                <ContentTemplate>--%>
    <style type="text/css">
        .table-responsive {
            width: 100%;
            overflow-x: auto;
            display: inline
        }

        .Popup-Zindex {
            z-index: 901 !important;
        }
    </style>
    <!-- notification JS -->
    <script src="<%=Siteurl %>admin/theme/assets/js/bootstrap-growl.min.js"></script>
    <script src="<%=Siteurl %>admin/theme/assets/pages/notification/notification.js"></script>

    <%--   <script>
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_pageLoaded(pageLoadedpro);
                        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                        prm.add_beginRequest(beginrequesthandler);
                        // raised after an asynchronous postback is finished and control has been returned to the browser.
                        prm.add_endRequest(endrequesthandler);

                        function beginrequesthandler(sender, args) {
                            //shows the modal popup - the update progress
                            //  $('.loader_div').removeClass('loading-inactive');
                            document.getElementById('loader_div').style.visibility = "visible";
                        }

                        function endrequesthandler(sender, args) {
                            //hide the modal popup - the update progress
                            //document.getElementById('interactive');
                            //document.getElementById('loader_div').style.visibility = "hidden";
                            //document.getElementById('loader').style.visibility = "false";
                        }

                        function pageLoadedpro() {

                            document.getElementById('loader_div').style.visibility = "hidden";


                            $('#<%=btnAdd.ClientID %>').click(function(e) {

                                formValidate();
                            });
                            $('#<%=btnUpdate.ClientID %>').click(function(e) {
                                formValidate();
                            });

                            $('body').removeClass('modal-open');

                            $('.modal-backdrop').remove();

                            $(document).ready(function() {
                                $('[data-toggle="tooltip"]').tooltip({
                                    trigger: 'hover'
                                });
                            });


                            //$('[data-toggle="tooltip"]').tooltip({
                            //  trigger: 'hover'
                            //})

                            // $("[data-toggle=tooltip]").tooltip();

                            $(".myvalcomloc").select2({
                                //placeholder: "select",
                                allowclear: true,
                                minimumResultsForSearch: -1
                            });

                        }

                        function ShowProgress() {
                            setTimeout(function() {
                                //alert("check");
                                if (Page_IsValid) {
                                    //alert("true");
                                    //$('.modal_popup').css('display', 'none');
                                    $('.modal_popup').css('z-index', '901');
                                    //this is because when popup is open, and loader is called background color becomes dark.
                                    document.getElementById('loader_div').style.visibility = "visible";
                                } else {
                                    //alert("false");
                                }
                            }, 200);
                        }
                        $(function() {
                            $('form').on("click", '#<%=lnkokreset.ClientID %>', function() {
                                //  $('form').on('submit', function () {
                                //alert(this.selector);
                                ShowProgress();
                            });
                        });
                    </script>--%>

    <div class="page-header card">
        <div class="card-block">          
                <h5>Manage Purchase Company                             
                     <div id="hbreadcrumb" class="pull-right">
                         <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary btnaddicon"> Add</asp:LinkButton>
                         <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                     </div>

                </h5>           
        </div>
        <div class="clear"></div>


    </div>






    <div class="finaladdupdate">
        <div id="PanAddUpdate" runat="server" visible="false">
            <div class="panel-body animate-panel padtopzero">
                <div class="card  addform">
                    <div class="card-header bordered-blue">
                        <h5>
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            Purchase Company
                        </h5>
                    </div>
                    <div class="card-block padleft25">
                        <div class="form-horizontal">
                            <div class="form-group row">
                                <asp:Label ID="lblusername" runat="server" class="col-sm-3 control-label">
                                                 Purchase Company</asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtpurchasecompany" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>



                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic" ControlToValidate="txtpurchasecompany" ErrorMessage="This value is required." ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtpurchasecompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group row">
                                <asp:Label ID="Label1" runat="server" class="col-sm-3 control-label">
                                                 Address</asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="form-control modaltextbox" Height="100px"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" ControlToValidate="txtAddress" ErrorMessage="This value is required." ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-7">
                                    <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req" Text="Add" UseSubmitBehavior="false" />
                                    <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" ValidationGroup="Req" Text="Save" Visible="false" UseSubmitBehavior="false" />
                                    <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click" CausesValidation="false" Text="Reset" UseSubmitBehavior="false" />
                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel" UseSubmitBehavior="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panelmessesgarea padbtmzero">
                <div class="alert alert-success" id="PanSuccess" runat="server">

                    <%--  <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>--%>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server">
                    <%--<i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>--%>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                    <%-- <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>--%>
                </div>
                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <%-- <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>--%>
                </div>
            </div>
            <div class="searchfinal">

                <div class="card shadownone brdrgray pad10">
                    <div class="card-block">
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                            <div class="inlineblock martop5">
                                <div class="row">
                                    <div class="input-group col-sm-2 martop5 max_width170">
                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Purchase Company" CssClass="form-control m-b"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch" WatermarkText="Purchase Company" />
                                    </div>

                                    <div class="input-group col-sm-1 martop5 max_width170 fullWidth">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" UseSubmitBehavior="false" />
                                    </div>
                                    <div class="input-group martop5 col-sm-1 max_width170">
                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left" CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="datashowbox inlineblock">
                            <div class="row">
                                <div class="input-group col-sm-2 martop5 max_width170">
                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myvalcomloc">
                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="finalgrid">
                <div class="xsroll">
                    <div id="PanGrid" runat="server">
                        <div class="card shadownone brdrgray">
                            <div class="card-block">
                                <div>
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" CssClass="tooltip-demo table table-bordered dataTable" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Index" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                        <%#Eval("Id")%>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="brdnoneleft" />
                                                    <HeaderStyle CssClass="brdnoneleft" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Purchase Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="600px">
                                                    <ItemTemplate>

                                                        <%#Eval("PurchaseCompanyName")%>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="brdnoneleft" />
                                                    <HeaderStyle CssClass="brdnoneleft" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="20px" HeaderStyle-Width="50px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-mini" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                                            <i class="fa fa-edit"></i> Edit
                                                        </asp:LinkButton>

                                                        <!--DELETE Modal Templates-->
                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini waves-effect waves-light" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("Id")%>'>
                                                                                <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>

                                                        <!--END DELETE Modal Templates-->

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <AlternatingRowStyle />
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination" id="divpage" runat="server">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </asp:Panel>
    </div>


    <asp:HiddenField ID="hdnid" runat="server" />
    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground" PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete">
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message">

        <div class="modal-dialog">
            <div class=" modal-content ">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header">
                    <h5 class="modal-title fullWidth">Delete
                                            <%--  <div style="float: right">
                                                <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                            </div>--%>
                    </h5>
                </div>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click1" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton5" runat="server" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Cancel</span></asp:LinkButton>
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField ID="hdndelete" runat="server" />


    <%--      </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnAdd" />--%>
    <%-- <asp:PostBackTrigger ControlID="lnkokreset" />--%>
    <%--    </Triggers>
            </asp:UpdatePanel>--%>
</asp:Content>
