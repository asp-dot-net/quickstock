using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ClosedXML.Excel;

public partial class admin_adminfiles_dashboard : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
                      
            SiteURL = st1.siteurl;
            BindDropdown();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            lblusername.Text = st.EmpFirst + " " + st.EmpLast;
            DataTable dt_date = ClstblEmployeeTeam.aspnet_MembershipLastLoginDateSelectByUserId(userid);
            if (dt_date.Rows.Count > 0)
            {
                //string date = dt.Rows[0]["LastActivityDate"].ToString();
                //DateTime dt1 = Convert.ToDateTime(date);
                //lastupdatedate.Text = "LastUpdated" + " " + (dt1.ToString("h:mm tt, MMM dd, yyyy"));

                string date = dt_date.Rows[0]["LastLoginDate"].ToString();
                DateTime time = Convert.ToDateTime(dt_date.Rows[0]["LastLoginDate"].ToString());
                lbldate.Text = (time.ToString("dd MMM yyyy"));
                lbltime.Text = time.ToString("hh:mm tt");

            }

            if (Roles.IsUserInRole("Purchase Manager"))
            {
                divTeamStatus.Visible = true;
                BindGrid();
            }
        }
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdateDashboard, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void BindGrid()
    {
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dt = ClsDashboard.tblStockItemsLocation_Dashboard_GetData(ddlLocation.SelectedValue, ddlCategory.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //divdeprec.Attributes.Add("style", "overflow-y:scroll; height:350px;");
            //PanNoRecord.Visible = false;
        }
        else
        {
            GridView1.Visible = false;
            //PanNoRecord.Visible = true;
        }
    }

    public void BindDropdown()
    {
        //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select();

        //ddlLocation.DataSource = dt;
        //ddlLocation.DataTextField = "CompanyLocation";
        //ddlLocation.DataValueField = "CompanyLocationID";
        //ddlLocation.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
        ddlCategory.SelectedValue = "1";
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = ClsDashboard.tblStockItemsLocation_Dashboard_GetData(ddlLocation.SelectedValue, ddlCategory.SelectedValue);

        Response.Clear();
        int count = dt.Rows.Count;
        try
        {
            Export oExport = new Export();
            string FileName = "Stock Details_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 8, 5, 6, 7 };
            string[] arrHeader = { "Stock Item", "Location", "Current Stock", "Net Situation", "Quantity Sold", "Min. Quantity", "Action", "Target Quantity" };
            
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataSet ds = ClsDashboard.SP_GetDataDashboard_StateWise(ddlCategory.SelectedValue);

        try
        {
            if (ds != null)
            {
                //Set Name of DataTables.
                ds.Tables[0].TableName = "Brisbane";
                ds.Tables[1].TableName = "Melbourne";
                ds.Tables[2].TableName = "Sydney";
                ds.Tables[3].TableName = "Adelaide";
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                foreach (DataTable dt in ds.Tables)
                {
                    wb.Worksheets.Add(dt);
                }

                string FileName = "Dashboard " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
        catch(Exception ex)
        {
            Notification(ex.Message);
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}
