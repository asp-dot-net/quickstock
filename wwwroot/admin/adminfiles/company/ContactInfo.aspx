<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Async="true"
    MaintainScrollPositionOnPostback="true" Culture="en-GB" UICulture="en-GB"
    CodeFile="ContactInfo.aspx.cs" Inherits="admin_adminfiles_company_company"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <style>
        input[type=checkbox]:checked,
        input[type=radio]:checked,
        input[type=checkbox]:focus,
        input[type=radio]:focus {
            outline: none !important;
        }

            input[type=checkbox]:checked ~ .text:before,
            input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
                display: inline-block;
                content: '\f00c';
                background-color: #f5f8fc;
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                border-color: #333333;
            }

        input[type=checkbox]:hover ~ .text :before,
        input[type=radio]:hover ~ .text :before, input[type=radio]:hover ~ label:before {
            border-color: #737373;
        }

        input[type=checkbox]:active ~ .text :before,
        input[type=radio]:active ~ .text :before, input[type=radio]:active ~ label :before {
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
        }


        input[type=checkbox] ~ .text,
        input[type=radio] ~ .text, input[type=radio] ~ label {
            position: relative;
            z-index: 11;
            display: inline-block;
            margin: 0;
            line-height: 20px;
            min-height: 18px;
            min-width: 18px;
            font-weight: normal;
        }

            input[type=checkbox] ~ .text:before,
            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                font-family: fontAwesome;
                font-weight: bold;
                font-size: 13px;
                color: #333333;
                content: "\a0";
                background-color: #fafafa;
                border: 1px solid #c8c8c8;
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                border-radius: 0;
                display: inline-block;
                text-align: center;
                vertical-align: middle;
                height: 18px;
                line-height: 16px;
                min-width: 18px;
                margin-right: 5px;
                margin-bottom: 2px;
                -webkit-transition: all 0.3s ease;
                -moz-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }

            input[type=checkbox] ~ .text:hover:before,
            input[type=radio] ~ .text:hover:before, input[type=radio] ~ label:hover:before {
                border-color: #737373;
            }

            input[type=checkbox] ~ .text:active:before,
            input[type=radio] ~ .text:active:before, input[type=radio] ~ label:active:before {
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            }

            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                border-radius: 100%;
                font-size: 10px;
                font-family: FontAwesome;
                line-height: 17px;
                height: 19px;
                min-width: 19px;
            }

        input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
            content: "\f111";
        }
    </style>
    <script type="text/javascript">
    $(document).ready(function () {
        HighlightControlToValidate();
        $('#<%=btnUpdateDetail.ClientID %>').click(function () {
            formValidate();
        });
        loader();
    });
    function loader() {
        $(window).load(function () {
        });
    }
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#b94a48");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
  
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Manage Installer Info
                                <asp:Literal runat="server" ID="ltcompname"></asp:Literal>

                            <div id="hbreadcrumb" class="pull-right">
                                <%--  <a id="ctl00_ContentPlaceHolder1_lnkAdd" class="btn btn-primary purple btnaddicon" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$lnkAdd','')"> Add</a>--%>
                                <asp:LinkButton ID="lnkAdd1" runat="server" CausesValidation="false" OnClick="lnkAdd1_Click" CssClass="btn btn-primary purple btnaddicon" Visible="false"> Add</asp:LinkButton>
                                <asp:LinkButton ID="lnkBack1" runat="server" OnClick="lnkBack1_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                            </div>
                        </h5>
                        <div class="clear"></div>
                    </div>
                </div>


            </div>
                   
            <div>
                <section class="row m-b-md" runat="server" id="update11">
    <div class="col-sm-12">
        <div class="contactsarea">
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
            </div>
            <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                <div class="row">
                    <div class="col-md-6">
                        <asp:Panel ID="Panel1" runat="server">
                          <div class="form-group textareaboxheight">
                            <span class="name">
                                <label class="control-label">
                                    Notes
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContNotes" runat="server" TextMode="MultiLine"
                                    CssClass="form-control"></asp:TextBox></span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Entered
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactEntered" runat="server" Enabled="false" CssClass="form-control"
                                   ></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    By
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactEnteredBy" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    ContactID
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactID" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Ref BSB
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtRefBSB" runat="server" MaxLength="12" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Ref Account
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtRefAccount" runat="server"  MaxLength="30" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        </asp:Panel>
                      
                    </div>
                    <div class="col-md-6" id="divInst" runat="server" visible="false">
                        <asp:Panel ID="Panel3" runat="server">
                          <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Elec Licence
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtElecLicence" runat="server"  MaxLength="20" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>


                             <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Accreditation
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtAccreditation" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>

                            <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">                                                    
                                        <div class="col-md-4 checkbox-info checkbox">
                                            <span class="fistname">
                                               <label for="<%=chkDocStoreDone.ClientID %>">
                                                    <asp:CheckBox ID="chkDocStoreDone" runat="server" AutoPostBack="true" OnCheckedChanged="chkDocStoreDone_CheckedChanged" />
                                                    <span class="text">   Stored&nbsp;</span>
                                                </label>
                                                <asp:HyperLink ID="lblAL" runat="server" Target="_blank" Visible="false"></asp:HyperLink>                                        
                                            </span>
                                        </div>                                       
                                    </div>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>



                           
                        <div class="form-group " id="divAL" runat="server" visible="false">
                            <span class="name">
                                <label class="control-label">
                                </label>
                            </span><span class="dateimg">
                                <asp:FileUpload ID="fuAL" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAL" runat="server" ErrorMessage="This value is required."
                                    ValidationGroup="contactinfo" SetFocusOnError="true" ControlToValidate="fuAL"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </span>
                            <div class="clear">
                            </div>
                        </div>


                        <div class="formainline">
                             <span class="name">
                                <label class="control-label">
                                    Lic Expires
                                </label>
                            </span>   
                         
                                <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <asp:TextBox ID="txtElecLicenceExpires" runat="server" class="form-control" placeholder="Expires Date">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                        ControlToValidate="txtElecLicenceExpires" Display="Dynamic" ValidationGroup="instcal"></asp:RequiredFieldValidator>

                            </div>
                            <div class="clear"></div>
                       </div>
                            

                        </asp:Panel>
                      
                               <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">                                             
                                        <div class="col-md-4 checkbox-info checkbox">
                                            <span class="mrdiv">
                                                 <label for="<%=chkInstaller.ClientID %>">
                                                   <asp:CheckBox ID="chkInstaller" runat="server" />
                                                        <span class="text">   Installer&nbsp;</span>
                                                    </label>
                                            </span>
                                        </div>
                                           
                                        <div class="col-md-4 checkbox-info checkbox">
                                            <span class="firstname">
                                               <label for="<%=chkDesigner.ClientID %>">
                                               <asp:CheckBox ID="chkDesigner" runat="server" />
                                                    <span class="text">Designer&nbsp;</span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-4 checkbox-info checkbox">              
                                            
                                                <span class="lastname" style="width: 41%;">                                                
                                                     <label for="<%=chkElectrician.ClientID %>">
                                                       <asp:CheckBox ID="chkElectrician" runat="server" Checked="true" />
                                                            <span class="text">Electrician&nbsp;</span>
                                                        </label>
                                                </span>                                           
                                        </div>                                        
                                    </div>

                            </div>
                                </span>
                                </div>
                       
                         

                            <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">                                                    
                                       <div class="col-md-4 checkbox-info checkbox">
                                            <span class="fistname">
                                               <label for="<%=chkMeterElectrician.ClientID %>">
                                               <asp:CheckBox ID="chkMeterElectrician" runat="server" />
                                                    <span class="text">MeterElectrician&nbsp;</span>
                                                </label>
                                            </span>
                                        </div>                                  
                                    </div>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row" id="divAddUpdate" runat="server">
                    <div class="col-md-12 textcenterbutton center-text">
                        <div>
                            <span class="name">&nbsp;</span><span>
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateDetail" runat="server" OnClick="btnUpdateDetail_Click"
                                    Text="Save" CausesValidation="true" ValidationGroup="contactinfo" />
                            </span>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</section>
            </div>                   



        </ContentTemplate>

        <Triggers>
            <asp:PostBackTrigger ControlID="chkDocStoreDone" />
            <asp:PostBackTrigger ControlID="btnUpdateDetail" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
