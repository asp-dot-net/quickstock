<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="OLDWholesale.aspx.cs" Inherits="admin_adminfiles_stock_OLDWholesale" Culture="en-GB" UICulture="en-GB" %>




<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script runat="server">

    protected void btnpdf_Click(object sender, EventArgs e)
    {

    }
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<meta  http-equiv="Refresh" content="5"> --%>

    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });


                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }


                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    //alert("begin");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    //alert("end");

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/
                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    });

                    $(document).ready(function () {

                        $('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }

                function doMyAction() {

                    $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });
                }

            </script>

            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Wholesale
                                        <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                            </div>
                        </h5>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updatepanel3" runat="server">
                <ContentTemplate>
                    <div class="finaladdupdate printorder">
                        <div id="PanAddUpdate" runat="server" visible="true">

                            <div class="panel-body animate-panel padtopzero stockorder">
                                <div class="card addform">
                                    <div class="card-header bordered-blue">
                                        <h5>
                                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                            Wholesale Order
                                        </h5>
                                    </div>
                                    <%-- <style>
                                .div_block{ display:block; float:none; margin-top:-10px;}
                                .margin_set1{margin-top:30px;}
                                .set_w99 .select2-container--default .select2-selection--single .select2-selection__rendered{width:99%;}

                            </style>--%>
                                    <div class="card-block padleft25">
                                        <div class="form-horizontal">
                                            <asp:Panel CssClass="form-group row" runat="server" ID="divfirstrow" Enabled="true">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblInvoiceType" runat="server">Invoice Type</asp:Label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlInvoiceType" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">Draft</asp:ListItem>
                                                            <asp:ListItem Value="2">Invoice</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlInvoiceType" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Label ID="Label1" runat="server">Invoice No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtVendorInvoiceNo" runat="server" MaxLength="200" CssClass="form-control modaltextbox" AutoPostBack="true"
                                                            OnTextChanged="txtVendorInvoiceNo_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtVendorInvoiceNo" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                        <asp:Button ID="btnFetchInvoice" runat="server" Text="Fetch Invoice" OnClick="btnFetchInvoice_Click" CssClass="btn btn-info redreq"
                                                            CausesValidation="false" />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblLastName" runat="server">
                                                    Customer Name</asp:Label>
                                                    <div class="input-group selectboxman icon_text">
                                                        <asp:DropDownList ID="ddlVendor" runat="server" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                        <span class="input-group-addon" id="basic-addon1">
                                                            <asp:LinkButton ID="btnNewVendor" runat="server" CausesValidation="false" OnClick="btnNewVendor_Click1"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Customer Name" CssClass="btn btn-into btn-mini">
                                                                            <i class="fa fa-user"></i>
                                                            </asp:LinkButton></span>
                                                    </div>
                                                    <div class="div_block">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                            ControlToValidate="ddlVendor" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>

                                                </div>

                                                <div class="col-md-2">
                                                    <span>Job Type
                                                    </span>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddljobtype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true"
                                                            AppendDataBoundItems="true" CausesValidation="false" OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                            <asp:ListItem Value="2">STC</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddljobtype" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblFirstName" runat="server">
                                                    STC ID</asp:Label>
                                                    <div class="selectboxman">
                                                        <asp:TextBox ID="txtManualOrderNumber" runat="server" MaxLength="200" CssClass="form-control modaltextbox"
                                                            AutoPostBack="true" OnTextChanged="ReferenceNo_TextChanged"></asp:TextBox>
                                                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtManualOrderNumber" FilterType="Numbers" />--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="dynamic" Enabled="false"
                                                            ControlToValidate="txtManualOrderNumber" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>

                                                </div>
                                                <div class="col-md-2">
                                                    <span>Solar Type
                                                    </span>
                                                    <div class="drpValidate" runat="server">
                                                        <asp:DropDownList ID="ddlSolarType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                            AppendDataBoundItems="true" CausesValidation="false">
                                                            <%-- <asp:ListItem Value="">Select</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlSolarType" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                

                                            </asp:Panel>

                                        </div>
                                        <div class="form-group row" style="margin-bottom:8px!important;">
                                            <div class="col-md-2">
                                                    <div class="drpValidate">
                                                        <asp:Label ID="Label3" runat="server">
                                                                Stock Location</asp:Label>
                                                        <asp:DropDownList ID="ddlStockLocation" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlStockLocation_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlStockLocation" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                            <div class="col-md-2">
                                                <span>Delivery Option
                                                </span>
                                                <%--<div class="drpValidate" runat="server">--%>
                                                <asp:DropDownList ID="ddldeliveryoption" runat="server" AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddldeliveryoption_SelectedIndexChanged">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Pick up</asp:ListItem>
                                                    <asp:ListItem Value="2">Transport</asp:ListItem>
                                                    <asp:ListItem Value="3">STC OutRight</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator172" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddldeliveryoption" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                <%--</div>--%>
                                            </div>
                                            <div class="col-md-2" id="divtransporttype" runat="server">
                                                <asp:Label ID="Label26" runat="server">
                                                 Transport Type</asp:Label>
                                                <div class="input-group selectboxman icon_text">
                                                    <asp:DropDownList ID="ddlTransporttype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlTransporttype" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                </div>

                                            </div>
                                            <div class="col-md-2" id="divconsign" runat="server">
                                                <asp:Label ID="Label28" runat="server">
                                         Consign/Person</asp:Label>
                                                <div class="selectboxman">
                                                    <asp:TextBox ID="Txtconsignno" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="Txtconsignno" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <span>Installer Type
                                                </span>
                                                <div class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddlinstallertype" runat="server" AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" CausesValidation="false" OnSelectedIndexChanged="ddlinstallertype_SelectedIndexChanged">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlinstallertype" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-2">

                                                <asp:Label ID="Label27" runat="server" class="disblock">
                                                          Installer Date</asp:Label>
                                                <div class="date datetimepicker1 custom_datepicker">
                                                    <div class="input-group sandbox-container">

                                                        <asp:TextBox ID="txtExpectedDelivery" runat="server" class="form-control" placeholder="Expected Delevery Date">
                                                        </asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="div_block">
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="dynamic"
                                                        ControlToValidate="txtExpectedDelivery" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            

                                        </div>
                                        <%--<br />
                                        <br />--%>
                                        <div class="form-group row">
                                            <div class="col-md-2" id="divinstallername" runat="server">
                                                <span>Installer Name
                                                </span>
                                                <div class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddlinstallername" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" CausesValidation="false">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="" CssClass=""
                                        ControlToValidate="ddlinstallername" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="col-md-2" id="div3" runat="server" style="display: none;">
                                                <asp:Label ID="Label29" runat="server">
                                      Credit STC</asp:Label>
                                                <div class="selectboxman">
                                                    <asp:TextBox ID="txtcreditstc" runat="server" MaxLength="200" CssClass="form-control modaltextbox"
                                                        OnTextChanged="ReferenceNo_TextChanged"></asp:TextBox>

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtcreditstc" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                </div>

                                            </div>


                                            <div class="col-md-1" id="div5" runat="server">
                                                <asp:Label ID="Label30" runat="server">
                                       No of STC</asp:Label>
                                                <div class="selectboxman">
                                                    <asp:TextBox ID="txtstcnumber" runat="server" MaxLength="200" CssClass="form-control modaltextbox"
                                                        Text="0"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtstcnumber" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server"
                                                        ErrorMessage="Number Only" CssClass="" ControlToValidate="txtstcnumber"
                                                        Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370" ValidationExpression="\d+"
                                                        EnableClientScript="true">
                                                    </asp:RegularExpressionValidator>
                                                </div>
                                            </div>

                                            <div class="col-md-1" id="div6" runat="server">
                                                <asp:Label ID="Label31" runat="server">
                                       STC Value</asp:Label>
                                                <div class="selectboxman">
                                                    <asp:TextBox ID="txtstcvalue" runat="server" MaxLength="200" CssClass="form-control modaltextbox"
                                                        Text="0"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtstcvalue"
                                                        ValidationGroup="Req" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                        ValidationExpression="^[0-9]+([.][0-9]*)?$"></asp:RegularExpressionValidator>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtstcvalue" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                </div>

                                            </div>

                                            <div class="col-md-2">
                                                <span>Job Status
                                                </span>
                                                <div class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddljobstatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" CausesValidation="false">
                                                        <%--<asp:ListItem Value="">Select</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddljobstatus" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <span>Invoice Amount</label>
                                      
                                        <asp:TextBox ID="txtinvoiceamt" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtinvoiceamt"
                                                        ValidationGroup="Req" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                        ValidationExpression="^[0-9]+([.][0-9]*)?$"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-md-2">
                                                <span>PVDNumber</label>
                                      
                                        <asp:TextBox ID="txtpvdno" runat="server"  CssClass="form-control"></asp:TextBox>
                                            </div>
                                             <div class="col-md-2">
                                                <span>Status
                                                </span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlstatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Pending</asp:ListItem>
                                                        <asp:ListItem Value="2">JobBooked</asp:ListItem>
                                                        <asp:ListItem Value="3">Void</asp:ListItem>
                                                        <asp:ListItem Value="4">PVD Applied</asp:ListItem>
                                                        <asp:ListItem Value="5">Approved</asp:ListItem>
                                                        <asp:ListItem Value="6">Failed</asp:ListItem>
                                                        <asp:ListItem Value="7" Selected="true">Blank</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div class="form-group row">
                                            
                                            
                                            <div class="col-md-2">
                                                <div class="custom_datepicker">
                                                    <asp:Label ID="Label23" runat="server" class="disblock">
                                                            Delivered Date</asp:Label>
                                                    <div class="input-group date" data-provide="datepicker">

                                                        <asp:TextBox ID="txtBOLReceived" runat="server" class="form-control" placeholder="Delivered Date">
                                                        </asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%--New Dropdown--%>
                                            <div class="col-md-2">
                                                <div class="custom_datepicker">
                                                    <asp:Label ID="Label32" runat="server" class="disblock">
                                                            Employee</asp:Label>
                                                    <div class="input-group date">
                                                        <asp:DropDownList ID="ddlEmployee" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                            AppendDataBoundItems="true">
                                                            <asp:ListItem Value="">Employee Name</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                        </div>
                                        <asp:HiddenField runat="server" ID="hdnWholesaleOrderID2" />
                                        <asp:Panel CssClass="graybgarea" runat="server" ID="divrepeater" Enabled="true">
                                            <div class="row">
                                                <div class="col-sm-9 set_w99">
                                                    <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <div class="row form-group">
                                                                <div class="col-md-3">

                                                                    <span class="name disblock">
                                                                        <span>Stock Category
                                                                        </span>
                                                                    </span><span>

                                                                        <asp:HiddenField ID="hdnStockCategory" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                        <div class="drpValidate">
                                                                            <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                                AutoPostBack="true" CausesValidation="false">
                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                            </asp:DropDownList>

                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                                ValidationGroup="Req" ForeColor="#FF5370" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                        </div>
                                                                    </span>

                                                                </div>
                                                                <div class="col-md-3">

                                                                    <span class="name disblock">
                                                                        <span>Stock Item</span>
                                                                    </span><span>
                                                                        <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                        <div class="drpValidate">
                                                                            <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged">
                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                        </div>
                                                                    </span>

                                                                </div>
                                                                <div class="col-md-2">

                                                                    <span class="name disblock">
                                                                        <span>Quantity</span>
                                                                    </span><span style="width: 60%">
                                                                        <asp:HiddenField ID="hdnOrderQuantity" runat="server" Value='<%#Eval("OrderQuantity") %>' />
                                                                        <asp:TextBox ID="txtOrderQuantity" runat="server" MaxLength="8" CssClass="form-control" OnTextChanged="txtOrderQuantity_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtOrderQuantity" ValidationGroup="Req" ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                                            ControlToValidate="txtOrderQuantity" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                    </span>

                                                                </div>
                                                                <div class="col-md-2">
                                                                    <span class="name disblock">
                                                                        <span>Available Qty</span>
                                                                    </span><span style="width: 60%">
                                                                        <asp:HiddenField ID="hdnAvailableQuantity" runat="server" Value='<%#Eval("AvailableQty") %>' />
                                                                        <asp:TextBox ID="txtAvailableQuantity" ReadOnly="true" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                                    </span>

                                                                </div>
                                                                <div class="col-md-2">
                                                                    <span class="name disblock">
                                                                        <span>Ordered Qty</span>
                                                                    </span><span style="width: 60%">
                                                                       <%-- <asp:HiddenField ID="HiddenField3" runat="server" Value='<%#Eval("AvailableQty") %>' />--%>
                                                                        <asp:TextBox ID="txtOrderedQty" ReadOnly="true" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                                    </span>

                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div style="padding-top: 20px">
                                                                        <span class="name">
                                                                            <!-- <asp:HiddenField ID="hdnWholesaleOrderItemID" runat="server" Value='<%# Eval("WholesaleOrderItemID") %>' />
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />-->
                                                                            <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                            <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                                CausesValidation="false" />
                                                                            <asp:Button ID="btnRevertScannedItem" runat="server" OnClick="btnRevertScannedItem_Click" Visible="false" Text="Revert" CssClass="btn btn-inverse btncancelicon"
                                                                                CausesValidation="false" /><br />

                                                                        </span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div style="margin-top: 20px">
                                                        <asp:Button ID="btnaddnew" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-info redreq"
                                                            CausesValidation="false" />
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel CssClass="form-group row" runat="server" ID="divnote" Enabled="true">
                                            <div class="col-sm-12">

                                                <span id="Label4" runat="server" class="disblock">Notes</span>
                                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="100px" Width="100%"
                                                    CssClass="form-control modaltextbox">
                                                </asp:TextBox>

                                            </div>

                                        </asp:Panel>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group row">
                                            <%--<div class="col-sm-3"></div>--%>
                                            <div class="col-sm-12 text-center">
                                                <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req"
                                                    Text="Add" />
                                                <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CausesValidation="true" ValidationGroup="Req"
                                                    Text="Save" Visible="false" />
                                                <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                        </div>


                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170 ">
                                                    <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Invoice No\Wholesale ID" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                        WatermarkText="Invoice No" />
                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlinvoicetype1" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">Draft</asp:ListItem>
                                                            <asp:ListItem Value="2">Invoice</asp:ListItem>
                                                        </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchjobtype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Job Type</asp:ListItem>
                                                        <asp:ListItem Value="1">Cash</asp:ListItem>
                                                        <asp:ListItem Value="2">STC</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtreferenceno" runat="server" placeholder="STC ID" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtreferenceno"
                                                        WatermarkText="STC ID" />
                                                    <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtreferenceno" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchdeliveryoption" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Delivery Option </asp:ListItem>
                                                        <asp:ListItem Value="1">Pick up</asp:ListItem>
                                                        <asp:ListItem Value="2">Transport</asp:ListItem>
                                                        <asp:ListItem Value="3">STC OutRight</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchTransportType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">TransportType</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Locations</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDeliveredOrNot" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" OnSelectedIndexChanged="ddlDeliveredOrNot_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="False" Selected="True">Not Dispatched</asp:ListItem>
                                                        <asp:ListItem Value="True">Dispatched</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170" id="divEmailFilter" runat="server" visible="false">
                                                    <asp:DropDownList ID="ddlEmail" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Email</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                        <%--<asp:ListItem Value="3">Delivered</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlsearchSolarType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">SolarType</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlsearchInstallType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">InstallType</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlsearchinstallername" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">InstallerName</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlsearchJobStatus" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">JobStatus</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Employee Name</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                        <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                        <asp:ListItem Value="3">Delivered</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                        <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="input-group col-sm-2">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>

                                                <div class="input-group col-sm-1 dnone">
                                                    <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" Visible="true">
                                                        <asp:ListItem Value="">Show</asp:ListItem>
                                                        <asp:ListItem Value="False">Not Received</asp:ListItem>
                                                        <asp:ListItem Value="True">Received</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1 dnone">
                                                    <asp:DropDownList ID="ddlDue" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" Visible="true">
                                                        <asp:ListItem Value="">Due</asp:ListItem>
                                                        <asp:ListItem Value="0">Due Today</asp:ListItem>
                                                        <asp:ListItem Value="1">Due Tomorrow</asp:ListItem>
                                                        <asp:ListItem Value="2">OverDue</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>

                </asp:Panel>
            </div>

            <div class="finalgrid">
                <asp:Panel ID="panel" runat="server">
                    <div class="page-header card" id="divtot" runat="server" visible="false">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Total Number of Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of Inverters:&nbsp;</b><asp:Literal ID="lblTotalInverters" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of STC:&nbsp;</b><asp:Literal ID="lblTotalSTC" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>

                        <div id="PanGrid" runat="server">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand"
                                            AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">
                                                            <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                            <%--<img id='imgdiv<%# Eval("WholesaleOrderID") %>' src="../../../images/icon_plus.png" />--%>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="OrderNumber" Visible="false">
                                                    <ItemTemplate>

                                                        <asp:Label ID="Label1" runat="server" Width="100px" CssClass="gridmainspan">
                                                            <asp:HiddenField ID="hdnStockOrderID" runat="server" Value='<%# Eval("WholesaleOrderID") %>' />
                                                            <asp:HyperLink ID="hlDetails1" runat="server" NavigateUrl='<%# "~/admin/adminfiles/reports/Wholesaleorder.aspx?id="+ Eval("WholesaleOrderID").ToString()%>'>
                                                                    <%#Eval("OrderNumber")%>
                                                            </asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--      <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="80px">
                                                <%#Eval("InvoiceNo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Width="80px">
                                                   <%#Eval("InvoiceNo")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="InvoiceType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelinvtype2" runat="server" Width="80px">
                                                   <%#Eval("InvoiceType")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Invoice Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="DateOrdered">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "DateOrdered", "{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stock For" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Width="80px">
                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="JobType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbljobtype" runat="server" Width="80px" Text='<%#Eval("JobType")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STC ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="ReferenceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Width="80px">
                                                <%#Eval("ReferenceNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STC" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="STCNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTCNumber" runat="server" Width="80px" Text='<%#Eval("STCNumber").ToString()==""?"0":Eval("STCNumber")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivery Option" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="DeliveryOption">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label50" runat="server" Width="80px">
                                                <%#Eval("DeliveryOption")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Items Ordered" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="WholesaleOrderItem" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                            Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="Vendor">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label7" runat="server" data-placement="top" data-original-title='<%#Eval("Vendor")%>' data-toggle="tooltip"
                                                            Width="170px"><%#Eval("Vendor")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label8" runat="server" Width="50px"><%#Eval("Qty")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivered" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="StockDeductDateFormated">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Width="50px"><%#Eval("StockDeductDateFormated")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivered By" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="StockDeductedByName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label99" runat="server" Width="50px"><%#Eval("StockDeductedByName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <%-- <ItemTemplate>
                                                            <asp:Label ID = "lblTotalAmount" runat = "server" Width = "25px" Text = "<%#Eval("TotalAmount")%>"></asp:Label>
                                                        </ItemTemplate>--%>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="EmployeeName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label100" runat="server" Width="50px"><%#Eval("EmployeeName")%></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <%--                                                    <asp:TemplateField HeaderText="TransportType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransportType" runat="server" Width="80px" Text='<%#Eval("TransportTypeName")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="InstallType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblInstallType" runat="server" Width="80px" Text='<%#Eval("InstallTypeName")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="InstallerName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInstallerName" runat="server" Width="80px" Text='<%#Eval("InstallerName")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SolarType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSolarType" runat="server" Width="80px" Text='<%#Eval("SolarTypeName")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="JobStatus" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblJobStatus" runat="server" Width="80px" Text='<%#Eval("JobTypeName")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="hypDetail" runat="server" CausesValidation="false" CommandName="detail" CommandArgument='<%# Eval("WholesaleOrderID") %>'
                                                            data-placement="top" data-original-title="Detail" data-toggle="tooltip" CssClass="btn btn-primary btn-mini">
                                                                        <%-- <img src="../../../images/icon_detail.png" width="15" height="16" />--%>
                                                                          <i class="fa fa-link"></i>Detail
                                                        </asp:LinkButton>

                                                        <asp:LinkButton runat="server" ID="btnviewPDF" CommandName="ViewPDF" CommandArgument='<%#Eval("WholesaleOrderID")%>' CssClass="btn btn-success btn-mini " Style="color: white;" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="View PDF" PostBackUrl="~/admin/adminfiles/stock/Wholesale.aspx" data-original-title="View" Visible='<%#Eval("IsDeduct").ToString() == "0" ? false : true%>'>
                                                                         <i class="fa fa-eye"></i> View
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnstcpdf" CommandName="ViewSTCPDF" CommandArgument='<%#Eval("WholesaleOrderID")%>' CssClass="btn btn-success btn-mini " Style="color: white;" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="View PDF" PostBackUrl="~/admin/adminfiles/stock/Wholesale.aspx" data-original-title="View">
                                                                         <i class="fa fa-eye"></i>STC View
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btnMail" CommandName="SendEmail" CommandArgument='<%#Eval("WholesaleOrderID")%>' CssClass="btn btn-warning btn-mini POPupLoader" Style="color: white;" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="Send Email" PostBackUrl="~/admin/adminfiles/stock/Wholesale.aspx" data-original-title="Email" Visible='<%#Eval("IsDeduct").ToString() == "0" ? false:true %>'>
                                                                         <i class="fa fa-envelope"></i> Mail
                                                        </asp:LinkButton>
                                                        <asp:HyperLink ID="lnkEmailSent" runat="server" CausesValidation="false" CssClass="btn btn-info" Visible='<%#Eval("IsDeduct").ToString() == "0" ? false:Eval("CustEmailFlag").ToString()=="False"? false:true%>'
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Email sent" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important; color: #fff!important;">
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Mail
                                                        </asp:HyperLink>
                                                        <asp:HyperLink ID="lnkEmailNotSent" runat="server" CausesValidation="false" CssClass="btn btn-danger" Visible='<%#Eval("IsDeduct").ToString() == "0" ? false:Eval("CustEmailFlag").ToString()=="False"?true:false%>'
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Email not sent" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important; color: #fff!important;">
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> Mail
                                                        </asp:HyperLink>

                                                        <%--  <img runat="server" id="imgr" src="../../images/icon_delivered.png" visible=' <%# Eval("Delivered").ToString() == "False" ? false : true%>' />--%>
                                                        <%--<asp:LinkButton ID="btnDelivered" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-maroon btn-mini" Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'
                                                        data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" OnClientClick="yes" CommandArgument='<%#Eval("StockOrderID")%>'> <i class="fa fa-truck"></i> Delivery</asp:LinkButton>--%>
                                                        <asp:LinkButton ID="btnRevert" runat="server" Text="Revert" CausesValidation="false" CssClass="btn btn-inverse btn-mini" Visible='<%# Eval("IsDeduct").ToString() == "0" ? false : true%>'
                                                            data-toggle="tooltip" data-placement="top" title="Revert" CommandName="Revert" CommandArgument='<%#Eval("WholesaleOrderID")%>'> <i class="fa fa-retweet"></i> Revert</asp:LinkButton>


                                                        <%--<asp:Image runat="server"  ImageUrl="~/images/revert_deactive.png" />--%>
                                                        <%--	<span class="btn btn-default btn-mini" ID="imgko" Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'  data-toggle="tooltip" data-placement="top" title="Deactive Revert" runat="server"><i class="fa fa-retweet"></i> Revert</span>--%>
                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" CssClass="btn btn-info btn-mini">
                                                            <i class="fa fa-edit"></i>Edit
                                                        </asp:LinkButton>

                                                        <!--DELETE Modal Templates-->

                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("WholesaleOrderID")%>' Visible='<%# Eval("IsDeduct").ToString() == "0" ? true : false%>'>
                        <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>
                                                        <!--PDF Modal Templates-->

                                                        <%-- <asp:LinkButton ID="gvbtnpdf" runat="server" CausesValidation="false" CommandName="PDF"  Visible='<%#Eval("DeliveryOptionID").ToString()=="3"?true : false %>' CommandArgument='<%#Eval("WholesaleOrderID")%>'
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" CssClass="btn btn-warning" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important;">
                                                            <i class="fa fa-edit"></i>PDF

                                                            </asp:LinkButton>--%>
                                                        <asp:LinkButton runat="server" ID="LinkButton9" CommandName="PDF" CommandArgument='<%#Eval("WholesaleOrderID")%>' Visible='<%#Eval("DeliveryOptionID").ToString()=="3"?true : false %>' CssClass="btn btn-success btn-mini " Style="color: white;" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="PDF" PostBackUrl="~/admin/adminfiles/stock/Wholesale.aspx" data-original-title="View">
                                                                         <i class="fa fa-eye"></i>PDF
                                                        </asp:LinkButton>
                                                        <%-- <asp:ImageButton ID="gvbtnUpdate" runat="server" Visible='<%# Eval("Delivered").ToString() == "True" ? false : true%>'
                                                                    CommandName="Select" ImageUrl="../../../images/icon_edit.png" CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="verticaaline" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("WholesaleOrderID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("WholesaleOrderID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td style="width: 2%;"><b>Id</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblwholesaleid" runat="server" Width="80px" Text='<%# Eval("WholesaleOrderID") %>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%;"><b>TransportType</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="LblTransportType" runat="server" Width="80px" Text='<%#Eval("TransportTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%"><b>InstallType</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="LblInstallType" runat="server" Width="80px" Text='<%#Eval("InstallTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%"><b>Consign/Person</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblConsignPerson" runat="server" Width="80px" Text='<%#Eval("ConsignPerson")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%;"><b>InstallerName</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblInstallerName" runat="server" Width="80px" Text='<%#Eval("InstallerName")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%;"><b>Install Date</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblInstalldate" runat="server" Width="80px" Text='<%#Eval("Expecteddelivery","{0:dd MMM yyyy}")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%;"><b>Amount</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblamount" runat="server" Width="80px" Text='<%#Eval("InvoiceAmount")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%"><b>SolarType</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblSolarType" runat="server" Width="80px" Text='<%#Eval("SolarTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td><b>JobStatus</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblJobStatus" runat="server" Width="80px" Text='<%#Eval("JobTypeName")%>'></asp:Label>
                                                                            </td>
                                                                           <%--<td style="width: 5%"><b>OrderItem</b>
                                                                                <asp:Label ID="Label33" runat="server" Width="80px" Text='<%#Eval("WholesaleOrderItem")%>'></asp:Label>
                                                                            </td>--%>
                                                                             <td><b>OrderItem</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label33" runat="server" Width="80px" Text='<%#Eval("WholesaleOrderItem")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                <div class="pagination">

                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid printorder" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!--End Danger Modal Templates-->
                    </div>
                </asp:Panel>

            </div>





            <asp:HiddenField ID="hndWholesaleOrderID" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="btnCancelDetail">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="color-line "></div>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Wholesale Order Detail
                                        <div style="float: right" class="printorder">
                                            <asp:Button ID="btnCancelDetail" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />
                                            <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success" OnClick="gvbtnPrint_Click" CausesValidation="false">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF
                                            </asp:LinkButton>
                                        </div>
                            </h5>
                        </div>
                        <h4 id="myModalLabel1" style="display: none" class="row-title"><i class="typcn typcn-th-small"></i>Wholesale Order Detail</h4>
                        <div class="modal-body paddnone" style="width: auto">
                            <div class="panel-body" id="detailsid" runat="server">
                                <div class="formainline" style="width: auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                <tr>
                                                    <td width="200px" valign="top">Location
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Customer
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">BOL Received
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Reference No
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Expected Delevery
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Transport Type
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbtransporttype" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Install Type
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblinstalltype" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Installer Name
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblinstallername" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Solar Type
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblsolartype" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Job Status
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbljobstatus" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Notes
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trOrderItem" runat="server">
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                            <tr class="graybgarea">
                                                                <td colspan="3">
                                                                    <h4>Wholesale Order Item Detail</h4>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Stock Category</b></td>
                                                                <td><b>Quantity</b></td>
                                                                <td><b>Stock Item</b></td>
                                                            </tr>
                                                            <asp:Repeater ID="rptOrder" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%#Eval("WholesaleOrderItem") %></td>
                                                                        <td class="text-center"><%#Eval("OrderQuantity") %></td>
                                                                        <td><%#Eval("StockItem") %></td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupdeliver" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divdeliver" TargetControlID="btnNull1"
                CancelControlID="btnCancel1">
            </cc1:ModalPopupExtender>
            <div id="divdeliver" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid" runat="server" />
                <div class="modal-dialog" style="width: 300px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancel1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H1">Delivery Date</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label25" runat="server" class="control-label ">
                                                <strong>Delivery Date</strong></asp:Label>
                                        </span>
                                        <div class="input-group sandbox-container">
                                            <span class="input-group">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtdatereceived" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label22" runat="server" class="control-label ">
                                                <strong>Serial No.</strong></asp:Label>
                                        </span>
                                        <span class="">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block; padding: 0; margin-top: 5px; margin-bottom: 5px;" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                            <%--<div class="col-sm-12" style="padding:0; margin-top:5px;">                            
                                            <asp:TextBox ID="txtserialno" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>--%>
                                        </span>
                                    </div>



                                    <div class="form-group marginleft col-sm-12" style="text-align: center; margin-bottom: 0;">
                                        <asp:Button ID="btndeliver" runat="server" Text="Save" OnClick="btndeliver_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="adddate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderStock" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStock" TargetControlID="btnNULLStock"
                CancelControlID="btnCancelStock">
            </cc1:ModalPopupExtender>
            <div id="divStock" class="modal_popup" runat="server" style="display: none">
                <div class="modal-dialog" style="width: 800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H3">Add Stock Item</h4>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body" style="min-height: 500px; overflow: auto;">
                                <div class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label12" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Category</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                </asp:DropDownList>

                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                    ControlToValidate="ddlstockcategory" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label13" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Item</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label16" runat="server" class="col-sm-3 control-label">
                                                <strong>Brand</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label2" runat="server" class="col-sm-3 control-label">
                                                <strong> Stock&nbsp;Model</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label17" runat="server" class="col-sm-3 control-label">
                                                <strong> Stock&nbsp;Series</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label18" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Size</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ErrorMessage="Enter valid digit"
                                                    ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label5" runat="server" class="col-sm-3 control-label">
                                                <strong>Min.&nbsp;Stock</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                    ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtminstock" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group checkareanew" id="sales" runat="server" visible="false">
                                            <div class="col-md-4 rightalign">
                                                <asp:Label ID="Label19" runat="server" class=" control-label">
                                                <strong>Sales&nbsp;Tag</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6 checkbox-info checkbox">
                                                <asp:CheckBox ID="chksalestag" runat="server" />
                                                <label for="<%=chksalestag.ClientID %>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="form-group checkareanew">
                                            <div class="col-md-4 rightalign ">
                                                <asp:Label ID="Label11" runat="server" class=" control-label">
                                                <strong>Is Active?</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="<%=chkisactive.ClientID %>">
                                                    <asp:CheckBox ID="chkisactive" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>

                                            </div>
                                            <div class="clear"></div>
                                        </div>


                                        <div class="form-group checkareanew">
                                            <div class="col-md-4 rightalign">
                                                <asp:Label ID="Label20" runat="server" class=" control-label">
                                                <strong>Is&nbsp;Dashboard?</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="<%=chkDashboard.ClientID %>">
                                                    <asp:CheckBox ID="chkDashboard" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>



                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="name form-group">

                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Location </strong>
                                                    </label>

                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Quantity</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Sales&nbsp;Tag</strong>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                            <ItemTemplate>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <span>
                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <asp:HiddenField ID="hndlocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <span>
                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                                            ControlToValidate="txtqty" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3 right-text checkbox-info checkbox ">
                                                                    <div class="thkmartop">
                                                                        <label for="<%# Container.FindControl("chksalestagrep").ClientID %>">
                                                                            <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                            <span class="text">&nbsp;</span>
                                                                        </label>



                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <div id="Div4" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Description</label>
                                            </span><span>
                                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                    Width="100%" Style="resize: none;">
                                                </asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <%--<div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Is&nbsp;Dashboard?</label>
                                                            </span><span>
                                                                <asp:CheckBox ID="chkDashboard" runat="server" />

                                                                <label for="<%=chkDashboard.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>--%>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row">
                                        <div class="col-sm-8 col-sm-offset-4 text-right">
                                            <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />

                                        </div>
                                    </div>
                                </div>
                                <%--   <div class="formainline">

                                    <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label12" runat="server" class="col-sm-6 control-label"> Stock&nbsp;Category</asp:Label>

                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlstockcategory" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Item</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                                            Brand</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Model</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Series</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Size</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[0-9]*$" ValidationGroup="stock"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtStockSize" Display="Dynamic"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                                             Min.&nbsp;Stock</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="comperror" ValidationGroup="stock"
                                                            ControlToValidate="txtminstock" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[0-9]*$"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chksalestag.ClientID %>">
                                                            <asp:CheckBox ID="chksalestag" runat="server" />
                                                            <span class="text">Sales&nbsp;Tag</span>
                                                        </label>


                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chkisactive.ClientID %>">
                                                            <asp:CheckBox ID="chkisactive" runat="server" />
                                                            <span class="text">Is&nbsp;Active?</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Stock&nbsp;Location
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                &nbsp;&nbsp;   Stock&nbsp;Quantity
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass=""
                                                                                            ControlToValidate="txtqty" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock" Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>



                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">
                                                            Description</label>

                                                        <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                            Width="100%" Style="resize: none;">
                                                        </asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-12">

                                                        <label for="<%=chkDashboard.ClientID %>">
                                                            <asp:CheckBox ID="chkDashboard" runat="server" />
                                                            <span class="text">Is&nbsp;Dashboard?</span>
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="row" style="text-align: center">
                                            <div class="form-group marginleft">
                                                <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />
                                            </div>
                                        </div>
                                    </div>

                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button1" Style="display: none;" runat="server" />


                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                        DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                        CancelControlID="btnCancelVndr">
                    </cc1:ModalPopupExtender>
                    <div id="div1" runat="server" style="display: none;" class="modal_popup">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <h4 class="modal-title">Add New Vendor</h4>
                                    <div style="float: right">
                                        <button id="Button2" runat="server" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                    </div>

                                </div>
                                <div class="modal-body paddnone">
                                    <div class="panel-body">
                                        <div class="formainline">

                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label14" runat="server" class="col-sm-4 control-label">
                                                <strong>Customerdf</strong></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                    --%>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label15" runat="server" class="col-sm-4 control-label">
                                                <strong>Name:</strong></asp:Label>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox2" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox3" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group center-text col-sm-12">
                                                <asp:Button ID="Button3" runat="server" Text="Add" OnClick="ibtnaddvendor_click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="vendor" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button4" Style="display: none;" runat="server" />
                    <!--Danger Modal Templates-->
                    <asp:Button ID="Button5" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                        PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
                    </cc1:ModalPopupExtender>
                    <div id="Div2" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                        <div class="modal-dialog " style="margin-top: -300px">
                            <div class=" modal-content ">
                                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                                <div class="modal-header text-center">
                                    <i class="glyphicon glyphicon-fire"></i>
                                </div>


                                <div class="modal-title">Delete</div>
                                <label id="Label21" runat="server"></label>
                                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                                <div class="modal-footer " style="text-align: center">
                                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton6" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>

                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <!--End Danger Modal Templates-->
                </div>
            </div>
            <asp:Button ID="btnNULLStock" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupExtenderVendor" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                CancelControlID="btnCancelVndr">
            </cc1:ModalPopupExtender>
            <div id="divVendor" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <h4 class="modal-title fullWidth">Add New Vendor
                                        <div style="float: right">
                                            <button id="btnCancelVndr" runat="server" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                        </div>
                            </h4>


                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label9" runat="server" class="col-sm-4 control-label">
                                                <strong>Customer</strong></asp:Label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                            --%>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label10" runat="server" class="col-sm-4 control-label">
                                                <strong>Name</strong></asp:Label>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContFirst" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContLast" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group center-text col-sm-12">
                                        <div class="col-sm-12">
                                            <asp:Button ID="ibtnAddVendor" runat="server" Text="Add" OnClick="ibtnaddvendor_click"
                                                CssClass="btn btn-primary savewhiteicon" ValidationGroup="vendor" CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNullVndr" Style="display: none;" runat="server" />
            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth">Delete
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                            </h5>
                        </div>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:Button ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" CommandName="deleteRow" Text="Ok" />
                            <asp:Button ID="LinkButton7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupRevert2" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="DivRevertPopup2" TargetControlID="btnNull2" CancelControlID="btnCancel2">
            </cc1:ModalPopupExtender>
            <div id="DivRevertPopup2" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid2" runat="server" />
                <asp:HiddenField ID="hndStockOrderItemID2" runat="server" />
                <div class="modal-dialog" style="width: 340px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Reset Wholesale Order</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <asp:Label runat="server" ID="lblalert" Text="Are you sure you want to reset wholesale order?"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnOK3btnOK3" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnOK3btnOK3_Click"></asp:Button>
                            <asp:Button ID="btnCancel2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>

                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupExtenderRevertScannedItem" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divRevertScannedItem" TargetControlID="Button8" CancelControlID="btnCancel3">
            </cc1:ModalPopupExtender>
            <div id="divRevertScannedItem" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="HiddenField2" runat="server" />

                <div class="modal-dialog" style="width: 340px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Reset Wholesale Order Item</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <asp:Label runat="server" ID="Label24" Text="Are you sure you want to reset this wholesale order item?"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnOK4" runat="server" type="button" class="btn btn-danger POPupLoader" PostBackUrl="~/admin/adminfiles/stock/Wholesale.aspx" data-dismiss="modal" Text="Ok" OnClick="btnOK4_Click"></asp:Button>
                            <asp:Button ID="btnCancel3" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>

                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button8" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndStockItemID3" runat="server" />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <asp:PostBackTrigger ControlID="btndeliver" />
            <asp:PostBackTrigger ControlID="gvbtnPrint" />
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%--<asp:PostBackTrigger ControlID="gvbtnpdf" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
