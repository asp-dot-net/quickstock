using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_OLDWholesale : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string mode = "";
    protected string SiteURL;
    static DataView dv;


    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;


        HidePanels();
        if (!IsPostBack)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            if (Roles.IsUserInRole("Warehouse"))
            {
                BindLocation();
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["State"].ToString();
                ddlSearchState.SelectedValue = CompanyLocationID;
                ddlSearchState.Enabled = false;
            }
            else
            { BindLocation(); }
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();


            BindGrid(0);

            BindVendor();
            BindTransportType();
            BindSolarType();
            BindInstallType();
            BindJobStatus();
            BindInstallerName();
            BindEmployee();
            BindSearchEmployee();

            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
            //ddljobstatus.SelectedValue = "2";
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Wholesale")) || (Roles.IsUserInRole("Warehouse")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else if ((Roles.IsUserInRole("WarehouseManager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //  GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }
        }

        ModeAddUpdate();

        if(Session["access_token"] == null)
        {
            string clientId = "CCC1E5862F7D4ACB92503EFD484A064E";// "437ACD16C59E4C17A43FE968BA55CE06";
            string redirectUrl = "http://localhost:57356/admin/adminfiles/xero/Wholesale.aspx";
            var AuthCode = (Request["code"] == null) ? "" : Request["code"].ToString();
            if (AuthCode == "")
            {
                Session["code_verifier"] = MakeCodeVerifier(32);
                string code_challenge = MakeCodeChallenge(Session["code_verifier"].ToString());
                Response.Redirect("https://login.xero.com/identity/connect/authorize?response_type=code&client_id=" + clientId + "&redirect_uri=" + redirectUrl + "&scope=openid profile email accounting.transactions offline_access&code_challenge=" + code_challenge + "&code_challenge_method=S256");
            }
            else
            {
                if (Session["code_verifier"] != null)
                {
                    var client = new RestSharp.RestClient("https://identity.xero.com/connect/token");
                    var request = new RestRequest(Method.POST);
                    request.AddParameter("grant_type", "authorization_code");
                    request.AddParameter("client_id", clientId);
                    request.AddParameter("code", AuthCode);
                    request.AddParameter("redirect_uri", redirectUrl);
                    request.AddParameter("code_verifier", Session["code_verifier"].ToString());
                    var response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        dynamic tokendata = JsonConvert.DeserializeObject(response.Content);

                        Session["access_token"] = tokendata.access_token;
                    }
                }
                Session["code_verifier"] = null;
            }
        }
    }

    private string MakeCodeChallenge(string codeVerifier)
    {
        using (var sha256 = new SHA256CryptoServiceProvider())
        {
            return EncodeBase64URL(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(codeVerifier)));
        }
    }

    private string MakeCodeVerifier(int numBytes = 32)
    {
        using (var rng = new RNGCryptoServiceProvider())
        {
            byte[] bytes = new byte[numBytes];
            rng.GetBytes(bytes);
            return EncodeBase64URL(bytes);
        }
    }

    string EncodeBase64URL(byte[] arg)
    {
        string s = Convert.ToBase64String(arg); // Regular base64 encoder
        s = s.Split('=')[0]; // Remove any trailing '='s
        s = s.Replace('+', '-'); // 62nd char of encoding
        s = s.Replace('/', '_'); // 63rd char of encoding
        return s;
    }

    protected void btnFetchInvoice_Click(object sender, EventArgs e)
    {
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        if (txtVendorInvoiceNo.Text == "")
        {
            return;
        }
        if(Session["access_token"] == null)
        {
            Response.Redirect(Request.RawUrl);
        }
        string accesstoken = Session["access_token"].ToString();

        var client = new RestSharp.RestClient("https://api.xero.com/connections");
        var request = new RestRequest(Method.GET);
        client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", accesstoken));
        IRestResponse<List<Invoice_Response.Connections>> response = client.Execute<List<Invoice_Response.Connections>>(request);
        if (response.StatusCode == System.Net.HttpStatusCode.OK)
        {
            var connections = response.Data;
            if(connections.Count > 0)
            {
                client = new RestSharp.RestClient("https://api.xero.com/api.xro/2.0/Invoices/" + txtVendorInvoiceNo.Text);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", accesstoken));
                client.AddDefaultHeader("xero-tenant-id", connections.FirstOrDefault().tenantId);
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Invoice_Response.RootObject> invresponse = client.Execute<Invoice_Response.RootObject>(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var invoicedetail = invresponse.Data;
                    if(invoicedetail.Invoices.Count>0)
                    {
                        //invoicedetail.Invoices[0].Contact.
                    }
                }
            }
        }
    }

    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlStockLocation.DataSource = dt;
        ddlStockLocation.DataTextField = "location";
        ddlStockLocation.DataValueField = "CompanyLocationID";
        ddlStockLocation.DataBind();

        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlVendor.DataSource = dt;
        ddlVendor.DataTextField = "Customer";
        ddlVendor.DataValueField = "CustomerID";
        ddlVendor.DataBind();

        ddlSearchVendor.DataSource = dt;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();
    }

    public void BindSolarType()

    {
        DataTable dt = Clstbl_WholesaleOrders.tblSolarType_Select();
        ddlSolarType.DataSource = dt;
        ddlSolarType.DataTextField = "SolarTypeName";
        ddlSolarType.DataValueField = "Id";
        ddlSolarType.DataBind();

        ddlSolarType.SelectedValue = "1";

        ddlsearchSolarType.DataSource = dt;
        ddlsearchSolarType.DataTextField = "SolarTypeName";
        ddlsearchSolarType.DataValueField = "Id";
        ddlsearchSolarType.DataBind();

    }

    public void BindInstallType()

    {
        DataTable dt = Clstbl_WholesaleOrders.tblInstallType_Select();
        ddlinstallertype.DataSource = dt;
        ddlinstallertype.DataTextField = "InstallTypeName";
        ddlinstallertype.DataValueField = "Id";
        ddlinstallertype.DataBind();


        ddlsearchInstallType.DataSource = dt;
        ddlsearchInstallType.DataTextField = "InstallTypeName";
        ddlsearchInstallType.DataValueField = "Id";
        ddlsearchInstallType.DataBind();

    }

    public void BindJobStatus()
    {
        ddljobstatus.SelectedIndex = 1;
        //ddljobstatus.Items.Insert(0, new ListItem("Installed", "Installed"));

        DataTable dt1 = null;
        DataTable dt = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
        if (string.IsNullOrEmpty(hndWholesaleOrderID.Value))
        {
            dt1 = Clstbl_WholesaleOrders.tblJobStatus_Select("2");
            ddljobstatus.DataSource = dt1;
            ddljobstatus.DataTextField = "JobStatusType";
            ddljobstatus.DataValueField = "Id";
            ddljobstatus.DataBind();
        }
        else
        {
            //dt1 = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
            ddljobstatus.DataSource = dt;
            ddljobstatus.DataTextField = "JobStatusType";
            ddljobstatus.DataValueField = "Id";
            ddljobstatus.DataBind();
        }

        ddlsearchJobStatus.DataSource = dt;
        ddlsearchJobStatus.DataTextField = "JobStatusType";
        ddlsearchJobStatus.DataValueField = "Id";
        ddlsearchJobStatus.DataBind();
    }

    public void BindInstallerName()
    {

        ddlinstallername.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlinstallername.DataValueField = "ContactID";
        ddlinstallername.DataMember = "Contact";
        ddlinstallername.DataTextField = "Contact";
        ddlinstallername.DataBind();

        ddlsearchinstallername.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlsearchinstallername.DataValueField = "ContactID";
        ddlsearchinstallername.DataMember = "Contact";
        ddlsearchinstallername.DataTextField = "Contact";
        ddlsearchinstallername.DataBind();



    }

    public void BindTransportType()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblTransportType_Select(ddldeliveryoption.SelectedIndex.ToString());
        ddlTransporttype.DataSource = dt;
        ddlTransporttype.DataTextField = "TransportTypeName";
        ddlTransporttype.DataValueField = "Id";
        ddlTransporttype.DataBind();

        DataTable dt1 = Clstbl_WholesaleOrders.tblTransportType_Selectwithoutparameter();
        ddlSearchTransportType.DataSource = dt1;
        ddlSearchTransportType.DataTextField = "TransportTypeName";
        ddlSearchTransportType.DataValueField = "Id";
        ddlSearchTransportType.DataBind();

    }

    //Bind Employee Dropdown
    public void BindEmployee()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }

    public void BindSearchEmployee()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlSearchEmployee.DataSource = dt;
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        //if (txtreferenceno.Text != "")
        //{
        //    SetExist();
        //}

        dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectBySearchNew(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), txtreferenceno.Text.Trim(), ddlSearchState.SelectedValue, "", ddlDeliveredOrNot.SelectedValue, ddlSearchTransportType.SelectedValue, ddlsearchInstallType.SelectedValue, ddlsearchinstallername.SelectedValue, ddlsearchJobStatus.SelectedValue, ddlsearchSolarType.SelectedValue, ddlSearchjobtype.SelectedValue, ddlSearchdeliveryoption.SelectedValue, ddlEmail.SelectedValue, ddlSearchEmployee.SelectedValue, ddlinvoicetype1.SelectedValue,"");

        //int iTotalRecords = dt.Rows.Count;
        //int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //if (iEndRecord > iTotalRecords)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //if (iStartsRecods == 0)
        //{
        //    iStartsRecods = 1;
        //}
        //if (iEndRecord == 0)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        divtot.Visible = false;
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
        Bind();

        int Amount = 0;
        int TotalAmount = 0;
        int STC = 0;
        int TotalSTC = 0;
        int Panel = 0;
        int TotalPanel = 0;
        int Inverter = 0;
        int TotalInverter = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int InvoiceAmount = 0;
            if (!string.IsNullOrEmpty(dt.Rows[i]["InvoiceAmount"].ToString()))
            {
                InvoiceAmount = Convert.ToInt32(dt.Rows[i]["InvoiceAmount"]);
            }

            Amount = InvoiceAmount;
            TotalAmount += Amount;

            int STCNumber = 0;
            if (!string.IsNullOrEmpty(dt.Rows[i]["STCNumber"].ToString()))
            {
                STCNumber = Convert.ToInt32(dt.Rows[i]["STCNumber"]);
            }

            STC = STCNumber;
            TotalSTC += STC;


            int PanelQty = 0;
            if (!string.IsNullOrEmpty(dt.Rows[i]["PanelQty"].ToString()))
            {
                PanelQty = Convert.ToInt32(dt.Rows[i]["PanelQty"]);
            }

            Panel = PanelQty;
            TotalPanel += Panel;


            int InverterQty = 0;
            if (!string.IsNullOrEmpty(dt.Rows[i]["InverterQty"].ToString()))
            {
                InverterQty = Convert.ToInt32(dt.Rows[i]["InverterQty"]);
            }

            Inverter = InverterQty;
            TotalInverter += Inverter;
        }

        lblTotalAmount.Text = TotalAmount.ToString();
        lblTotalSTC.Text = TotalSTC.ToString();
        lblTotalPanels.Text = TotalPanel.ToString();
        lblTotalInverters.Text = TotalInverter.ToString();
    }

    public void Bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {

        int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(txtVendorInvoiceNo.Text);

        if (existaddress != 1)
        {
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            // string StockCategoryID = ddlStockCategoryID.SelectedValue.ToString();

            string DateOrdered = DateTime.Now.AddHours(14).ToString();
            string employee = ddlVendor.SelectedValue;

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            string Notes = txtNotes.Text;
            string DeliveryDate = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            string ReferenceNo = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;
            string CreditSTC = txtcreditstc.Text;
            string ConsignPerson = Txtconsignno.Text;
            string STCNumber = txtstcnumber.Text;

            var vals = "";
            var vals1 = "";
            string transporttype = "";
            string installername = "";

            if (ddlStockLocation.SelectedValue != string.Empty)
            {
                vals = StockLocation_Text.Split(':')[0];
                vals1 = StockLocation_Text.Split(':')[1];
            }
            if ((ddlTransporttype.SelectedValue != "Select") || (ddlTransporttype.SelectedValue != ""))
            {
                transporttype = ddlTransporttype.SelectedValue;
            }
            if (ddlinstallername.SelectedValue != "Select" || ddlinstallername.SelectedValue != "")
            {
                installername = ddlinstallername.SelectedValue;
            }


            string state = Convert.ToString(vals);

            int success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Insert(DateOrdered, employee, Notes, StockLocation, vendor, DeliveryDate, ReferenceNo, ExpectedDelivery);

            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_OrderNumber(success.ToString(), success.ToString());
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_VendorInvoiceNo(success.ToString(), txtVendorInvoiceNo.Text);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_DeliveryOption(success.ToString(), ddljobtype.SelectedValue, ddljobtype.SelectedItem.Text, txtinvoiceamt.Text, txtpvdno.Text, ddlstatus.SelectedValue, ddlstatus.SelectedItem.Text, ddldeliveryoption.SelectedItem.Text, ddldeliveryoption.SelectedValue);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Newparameters(success.ToString(), ddlTransporttype.SelectedValue, ddlinstallertype.SelectedValue, ddlinstallername.SelectedValue, ddlSolarType.SelectedValue, ddljobstatus.SelectedValue, ConsignPerson, CreditSTC, STCNumber);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_StcValue(success.ToString(), txtstcvalue.Text);

            string EmpId = "";
            if (ddlEmployee.SelectedValue != "Employee Name" || ddlEmployee.SelectedValue != "")
            {
                EmpId = ddlEmployee.SelectedValue;
            }
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Employee(success.ToString(), EmpId);

            if (ddldeliveryoption.SelectedValue == "3")
            {
                Clstbl_WholesaleOrders.tbl_WholesaleOrders_OrderComplete(success.ToString(), userid);
            }

            bool suc = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_InvoiceType(success.ToString(), ddlInvoiceType.SelectedValue, ddlInvoiceType.SelectedItem.ToString());


            foreach (RepeaterItem item in rptattribute.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    TextBox txtAvailableQuantity= (TextBox)item.FindControl("txtAvailableQuantity");
                    string StockItem = ddlStockItem.SelectedValue.ToString();
                    string OrderQuantity = txtOrderQuantity.Text;
                    string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();
                    if (ddldeliveryoption.SelectedValue == "1")//Draft
                    {
                        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                        if (!chkdelete.Checked)
                        {
                            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                            {
                                int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                                //Response.Write(success1);

                            }
                        }
                    }
                    else if (ddldeliveryoption.SelectedValue == "2")//Invoice
                    {
                        DataTable dtstock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockLocation.SelectedValue, ddlStockItem.SelectedValue);
                        if (dtstock.Rows.Count > 0)
                        {
                            if (dtstock.Rows[0]["StockQuantity"].ToString() != null && dtstock.Rows[0]["StockQuantity"].ToString() != "")
                            {
                                int LiveQty = Convert.ToInt32(dtstock.Rows[0]["StockQuantity"].ToString());

                                if (LiveQty >= 0)
                                {
                                    CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                                    if (!chkdelete.Checked)
                                    {
                                        if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                                        {
                                            int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                                            //Response.Write(success1);

                                        }
                                    }
                                }
                            }
                            //if (txtAvailableQuantity.Text != null && txtAvailableQuantity.Text != "")
                            //{
                            //    int AvailQty = Convert.ToInt32(txtAvailableQuantity.Text);
                            //    
                            //    if (AvailQty >= 0)
                            //    {
                            //        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                            //        if (!chkdelete.Checked)
                            //        {
                            //            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                            //            {
                            //                int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                            //                //Response.Write(success1);

                            //            }
                            //        }
                            //    }
                            //}
                        }

                    }
                }
            }
            // Response.End();

            //--- do not chage this code start------
            if (success > 0)
            {
                SetAdd();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            PAnAddress.Visible = true;
            //BindGrid(0);
            //lblexistame.Visible = true;
            //lblexistame.Text = "Record with this Address already exists ";
        }
        BindGrid(0);
        // Reset();
        mode = "Add";
        Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (exist == 0)
        {
            string Notes = txtNotes.Text;
            string BOLReceived = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            var vals = StockLocation_Text.Split(':')[0];
            var vals1 = StockLocation_Text.Split(':')[1];
            string state = Convert.ToString(vals);
            string ManualOrderNumber = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;
            string CreditSTC = txtcreditstc.Text;
            string ConsignPerson = Txtconsignno.Text;
            string STCNumber = txtstcnumber.Text;

            bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_DeliveryOption(id1, ddljobtype.SelectedValue, ddljobtype.SelectedItem.Text, txtinvoiceamt.Text, txtpvdno.Text, ddlstatus.SelectedValue, ddlstatus.SelectedItem.Text, ddldeliveryoption.SelectedItem.Text, ddldeliveryoption.SelectedValue);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Newparameters(id1, ddlTransporttype.SelectedValue, ddlinstallertype.SelectedValue, ddlinstallername.SelectedValue, ddlSolarType.SelectedValue, ddljobstatus.SelectedValue, ConsignPerson, CreditSTC, STCNumber);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_StcValue(id1, txtstcvalue.Text);

            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Employee(id1, ddlEmployee.SelectedValue);

            if (ddldeliveryoption.SelectedValue == "3")
            {
                Clstbl_WholesaleOrders.tbl_WholesaleOrders_OrderComplete(id1, userid);
            }
            //foreach (RepeaterItem item in rptviewdata.Items)
            //{
            //    HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
            //    CheckBox chkdelete1 = (CheckBox)item.FindControl("chkdelete1");
            //    if (chkdelete1.Checked)
            //    {
            //        ClstblStockOrders.tblStockOrderItems_Delete(hdnWholesaleOrderItemID.Value);
            //    }
            //}
            Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_whole_Delete(id1);
            //Response.Write(rptattribute.Items.Count);
            //Response.End();
            bool suc = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_InvoiceType(id1, ddlInvoiceType.SelectedValue, ddlInvoiceType.SelectedItem.ToString());

            foreach (RepeaterItem item in rptattribute.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
                    string StockItem = ddlStockItem.SelectedValue.ToString();
                    string OrderQuantity = txtOrderQuantity.Text;
                    string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();
                    int qty=0;
                    CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                    if (ddldeliveryoption.SelectedValue == "1")//Draft
                    {
                        if (!chkdelete.Checked)
                        {

                            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                            {
                                int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(id1, StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                            }
                        }

                        if (chkdelete.Checked)
                        {
                            Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Delete(hdnWholesaleOrderItemID.Value);
                        }

                    }
                    else if (ddldeliveryoption.SelectedValue == "2")
                    {
                        DataTable dtstock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockLocation.SelectedValue, ddlStockItem.SelectedValue);
                        if (dtstock.Rows.Count > 0)
                        {
                            if (dtstock.Rows[0]["StockQuantity"].ToString() != null && dtstock.Rows[0]["StockQuantity"].ToString() != "")
                            {
                                int LiveQty = Convert.ToInt32(dtstock.Rows[0]["StockQuantity"].ToString());

                                if (LiveQty >= 0)
                                {
                                    //CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                                    if (!chkdelete.Checked)
                                    {
                                        if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                                        {
                                            int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(id1, StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                                            //Response.Write(success1);

                                        }
                                    }
                                }
                            }
                            //if (txtAvailableQuantity.Text != null && txtAvailableQuantity.Text != "")/?Changes on 16-03-2020
                            //{
                            //    int AvailQty = Convert.ToInt32(txtAvailableQuantity.Text);
                            //    if (AvailQty >= 0)
                            //    {
                            //        if (!chkdelete.Checked)
                            //        {

                            //            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                            //            {
                            //                int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(id1, StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                            //            }
                            //        }

                            //        if (chkdelete.Checked)
                            //        {
                            //            Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Delete(hdnWholesaleOrderItemID.Value);
                            //        }
                            //    }
                            //}
                        }
                    }
                     
                }
            }
            //--- do not chage this code Start------
            if (success)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            //InitUpdate();
            PAnAddress.Visible = true;
        }
        BindScript();
        BindGrid(0);
        Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

        //--- do not chage this code end------
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
        divtransporttype.Visible = true;
        divinstallername.Visible = true;
        divconsign.Visible = true;
        mode = "Edit";

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnWholesaleOrderID2.Value = id;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(id);
        ddlStockLocation.SelectedValue = st.CompanyLocationID;
        try
        {
            ddlVendor.SelectedValue = st.CustomerID;
            txtstcvalue.Text = st.StcValue;
        }
        catch { }
        try
        {
            txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
        }
        catch { }
        txtNotes.Text = st.Notes;
        txtManualOrderNumber.Text = st.ReferenceNo;
        try
        {
            txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
        }
        catch { }
        txtVendorInvoiceNo.Text = st.InvoiceNo;
        try
        {
            ddljobtype.SelectedValue = st.JobTypeID;
        }
        catch { }
        try
        {
            ddldeliveryoption.SelectedValue = st.DeliveryOptionID;
            ddlInvoiceType.SelectedValue = st.InvoiceTypeId;
        }
        catch { }

        try
        {

            ddlInvoiceType.SelectedValue = st.InvoiceTypeId;
        }
        catch { }

        try
        {
            txtinvoiceamt.Text = SiteConfiguration.ChangeCurrency_Val(st.InvoiceAmount);
        }
        catch { }
        DataTable dtwholesaleisrevert = ClstblStockSerialNo.tblStockSerialNo_Select_WholesaleOrderID(id);
        DataTable dt1 = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
        if (st.JobStatusId == "2" && dtwholesaleisrevert.Rows.Count > 0)
        {
            ddljobstatus.Items.Clear();
            ddljobstatus.DataSource = dt1;
            ddljobstatus.DataTextField = "JobStatusType";
            ddljobstatus.DataValueField = "Id";
            ddljobstatus.DataBind();
        }
        DataTable dtwholesaleScancount = ClstblMaintainHistory.tblMaintainHistory_wholesalescancount(id);
        if (string.IsNullOrEmpty(st.StockDeductDate))
        {
            if (dtwholesaleScancount.Rows.Count == 0)
            {
                ddljobstatus.Items.Clear();
                ddljobstatus.DataSource = dt1;
                ddljobstatus.DataTextField = "JobStatusType";
                ddljobstatus.DataValueField = "Id";
                ddljobstatus.DataBind();
            }
        }

        txtpvdno.Text = st.PVDNumber;
        //BindTransportType();
        ddlTransporttype.Items.Clear();
        DataTable dtTransportType = Clstbl_WholesaleOrders.tblTransportType_Select(ddldeliveryoption.SelectedIndex.ToString());
        ddlTransporttype.DataSource = dtTransportType;
        ddlTransporttype.DataTextField = "TransportTypeName";
        ddlTransporttype.DataValueField = "Id";
        ddlTransporttype.DataBind();

        try
        {
            ddlstatus.SelectedValue = st.StatusID;
        }
        catch
        { }

        try
        {
            ddlTransporttype.SelectedValue = st.TransportTypeId;
        }
        catch { }
        try
        {
            ddlinstallertype.SelectedValue = st.InstallTypeId;
        }
        catch
        {
        }
        try
        {
            ddlinstallername.SelectedValue = st.InstallerNameId;
        }
        catch
        { }

        try
        {
            ddljobstatus.SelectedValue = st.JobStatusId;
        }
        catch { }
        Txtconsignno.Text = st.ConsignPerson;
        txtcreditstc.Text = st.CreditStC;
        txtstcnumber.Text = st.STCNumber;
        try
        {
            ddlSolarType.SelectedValue = st.SolarTypeId;
        }
        catch { }
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(id);


        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;

        }
        ModeAddUpdate();
        //MaxAttribute = 1;
        //bindrepeater();
        //BindAddedAttribute();
        //--- do not chage this code start------
        //lnkBack.Visible = true;
        InitUpdate();
        DataTable dtwholesale = null;
        if (st.IsDeduct == "1")
        {


            foreach (RepeaterItem item in rptattribute.Items)
            {
                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
                TextBox txtOrderedQty = (TextBox)item.FindControl("txtOrderedQty");

                dtwholesale = ClstblMaintainHistory.tblMaintainHistory_Select_WholesaleRevertById(id, ddlStockItem.SelectedValue);
                if (dtwholesale.Rows.Count > 0)
                {
                    divrepeater.Enabled = true;
                    if (Convert.ToInt32(txtOrderQuantity.Text) == Convert.ToInt32(dtwholesale.Rows.Count))
                    {
                        ddlStockCategoryID.Enabled = true;
                        ddlStockItem.Enabled = true;
                        txtOrderQuantity.Enabled = true;
                    }
                    else
                    {
                        ddlStockCategoryID.Enabled = false;
                        ddlStockItem.Enabled = false;
                        txtOrderQuantity.Enabled = true;
                    }
                }
                else
                {
                    ddlStockCategoryID.Enabled = false;
                    ddlStockItem.Enabled = false;
                    txtOrderQuantity.Enabled = false;
                }
            }

            divfirstrow.Enabled = true;
            txtVendorInvoiceNo.Enabled = false;
            ddlVendor.Enabled = true;
            ddljobtype.Enabled = true;
            txtManualOrderNumber.Enabled = true;
            ddlSolarType.Enabled = true;
            ddlStockLocation.Enabled = true;
            txtNotes.Enabled = true;
            //divrepeater.Enabled = false;
            //divfirstrow.Enabled = false;
            divnote.Enabled = true;

            //divfirstrow.Attributes["class"] = "form-group row aspNetDisabled";
            //divrepeater.CssClass = "graybgarea aspNetDisabled";
            //divfirstrow.Attributes.Add("class", "form-group row aspNetDisabled ");
            //divrepeater.Attributes.Add("class", "graybgarea aspNetDisabled");
        }
        else
        {
            divfirstrow.Enabled = true;
            divrepeater.Enabled = true;
            divnote.Enabled = true;
            //divfirstrow.Attributes.Add("class", "form-group row");
            //divrepeater.Attributes.Add("class", "graybgarea");
        }

        //--- do not chage this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Delivered")
        {
            //Response.Write("cbv");
            //Response.End();
            ModalPopupdeliver.Show();
            // Button lnkBtn = (Button)e.CommandSource;    // the button
            //GridViewRow myRow = (GridViewRow)lnkBtn.Parent.Parent;  // the row
            //GridView myGrid = (GridView)sender; // the gridview
            //GridViewRow gvr = (GridViewRow)lnkBtn.NamingContainer;
            string ID = e.CommandArgument.ToString();
            hndid.Value = ID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            //hdnStockOrderID
            //int id = (int)GridView1.DataKeys[gvr.RowIndex].Value;
            //string ID = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, stEmployeeid.EmployeeID, "1");
            //SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);
            //if (success)
            //{
            //    DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
            //            ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
            //            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
            //        }
            //    }
            //    SetDelete();
            //}
            //else
            //{
            //    SetError();
            //}
            //BindGrid(0);  
        }

        if (e.CommandName == "Revert")
        {
            ModalPopupRevert2.Show();
            hndid2.Value = e.CommandArgument.ToString();
        }


        if (e.CommandName == "ViewPDF")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            Telerik_reports.generate_WholesalePicklist(WholesaleOrderID);
        }

        if (e.CommandName == "PDF")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            Telerik_reports.WholesalePDF(WholesaleOrderID);
        }


        if (e.CommandName == "SendEmail")
        {
            try
            {
                string WholesaleOrderID = e.CommandArgument.ToString();

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                Sttbl_WholesaleOrders st2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
                SttblContacts st3 = ClstblContacts.tblContacts_SelectByCustomerID(st2.CustomerID);
                String Subject = "Wholesale Order Number: " + WholesaleOrderID + " has been dispatched.";

                Server.Execute("~/mailtemplate/WholesaleAttachmentNew.aspx?WholesaleorderID=" + WholesaleOrderID, txtWriter);


                Telerik_reports.generate_WholesalePicklist(WholesaleOrderID, "Save");

                string FileName = WholesaleOrderID + "_Wholesale.pdf";

                string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SavePDF2\\", FileName);

                Utilities.SendMailWithAttachmentForAchieversEnergy(from, st3.ContEmail, Subject, txtWriter.ToString(), fullPath, FileName);

                //Utilities.SendMailWithAttachmentForAchieversEnergy(from, "nilesh.rai@meghtechnologies.com", Subject, txtWriter.ToString(), fullPath, FileName);

                SiteConfiguration.deleteimage(FileName, "SavePDF2");

                Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(WholesaleOrderID, "true");

                Notification("Email sent successfully to the Customer.");
                BindGrid(0);
            }
            catch (Exception ex)
            {
                Notification("Email not sent.");
            }
        }

        if (e.CommandName == "print")
        {
            string StockOrderID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/order.aspx?id=" + StockOrderID);
        }

        if (e.CommandName.ToLower() == "detail")
        {

            hndWholesaleOrderID.Value = e.CommandArgument.ToString();
            string StockOrderID = e.CommandArgument.ToString();

            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(StockOrderID);

            if (st.CompanyLocation != string.Empty)
            {
                lblStockLocation.Text = st.CompanyLocation;
            }
            else
            {
                lblStockLocation.Text = "-";
            }
            if (st.vendor != string.Empty)
            {
                lblVendor.Text = st.vendor;
            }
            else
            {
                lblVendor.Text = "-";
            }
            if (st.BOLReceived != string.Empty)
            {
                lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
            }
            else
            {
                lblBOLReceived.Text = "-";
            }
            if (st.ReferenceNo != string.Empty)
            {
                lblManualOrderNo.Text = st.ReferenceNo;
            }
            else
            {
                lblManualOrderNo.Text = "-";
            }
            if (st.ExpectedDelivery != string.Empty)
            {
                lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
            }
            else
            {
                lblExpectedDelevery.Text = "-";
            }
            if (st.TransportTypeName != string.Empty)
            {
                lbtransporttype.Text = st.TransportTypeName;
            }
            else
            {
                lbtransporttype.Text = "-";
            }
            if (st.InstallTypeName != string.Empty)
            {
                lblinstalltype.Text = st.InstallTypeName;
            }
            else
            {
                lblinstalltype.Text = "-";
            }
            if (st.InstallerName != string.Empty)
            {
                lblinstallername.Text = st.InstallerName;
            }
            else
            {
                lblinstallername.Text = "-";
            }
            if (st.SolarTypeName != string.Empty)
            {
                lblsolartype.Text = st.SolarTypeName;
            }
            else
            {
                lblsolartype.Text = "-";
            }
            if (st.JobTypeName != string.Empty)
            {
                lbljobstatus.Text = st.JobTypeName;
            }
            else
            {
                lbljobstatus.Text = "-";
            }

            if (st.Notes != string.Empty)
            {
                lblNotes.Text = st.Notes;
            }

            else
            {
                lblNotes.Text = "-";
            }
            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(StockOrderID);
            if (dt.Rows.Count > 0)
            {
                rptOrder.DataSource = dt;
                rptOrder.DataBind();
                trOrderItem.Visible = true;
            }
            else
            {
                trOrderItem.Visible = false;
            }
            ModalPopupExtenderDetail.Show();
        }

        if (e.CommandName == "ViewSTCPDF")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            Telerik_reports.generate_WholesaleSTC(WholesaleOrderID);
        }
        // BindGrid(0);
        //Response.Write("testing");
        //Response.End();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindScript();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        MaxAttribute = 1;
        bindrepeater();
        BindAddedAttribute();
        BindScript();
        PanGridSearch.Visible = false;
        PanGrid.Visible = false;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Reset();
        InitAdd();
        mode = "Add";
        if (mode == "Add")
        {
            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
        }
        divtot.Visible = false;
        PanGrid.Visible = false;
        divfirstrow.Enabled = true;
        divrepeater.Enabled = true;
        divnote.Enabled = true;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindLocation();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid2);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();

            ddlStockLocation.SelectedValue = CompanyLocationID;
            ddlStockLocation.Enabled = false;
        }

        //  PanGridSearch.Visible = false;
        BindScript();

    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        // Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindLocation();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid2);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();

            ddlStockLocation.SelectedValue = CompanyLocationID;
            ddlStockLocation.Enabled = false;
        }

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        Notification("Transaction Failed.");
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;
        divtot.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        ddlEmployee.SelectedValue = "";
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;

        PanAddUpdate.Visible = true;
        rptattribute.DataSource = null;
        rptattribute.DataBind();
        //rptviewdata.DataSource = null;
        //rptattribute.DataBind();
        ddlStockLocation.ClearSelection();
        ddlVendor.ClearSelection();
        txtBOLReceived.Text = "";
        txtNotes.Text = "";
        txtManualOrderNumber.Text = string.Empty;
        txtExpectedDelivery.Text = string.Empty;
        //rptviewdata.Visible = false;

        ddlstockcategory.ClearSelection();
        //txtstockitem.Text = string.Empty;
        //txtbrand.Text = string.Empty;
        //txtmodel.Text = string.Empty;
        //txtseries.Text = string.Empty;
        //txtminstock.Text = string.Empty;
        //chksalestag.Checked = false;
        //chkisactive.Checked = false;
        //txtdescription.Text = string.Empty;
        //txtStockSize.Text = string.Empty;
        txtVendorInvoiceNo.Text = string.Empty;
        ddljobtype.SelectedValue = "";
        ddldeliveryoption.SelectedValue = "";
        txtreferenceno.Text = string.Empty;
        txtinvoiceamt.Text = "0";
        txtstcvalue.Text = "0";
        txtpvdno.Text = string.Empty;
        ddlstatus.SelectedValue = "7";
        //ddlTransporttype.SelectedValue = "0";
        ddlTransporttype.ClearSelection();
        ddlSolarType.SelectedValue = "1";
        ddlinstallertype.SelectedValue = "";
        ddlinstallername.SelectedValue = "";
        ddljobstatus.SelectedValue = "1";
        Txtconsignno.Text = string.Empty;
        txtcreditstc.Text = string.Empty;
        txtstcnumber.Text = "0";
        ddlInvoiceType.SelectedValue = "1";

        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindLocation();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid2);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();

            ddlStockLocation.SelectedValue = CompanyLocationID;
            ddlStockLocation.Enabled = false;
        }


        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            txtqty.Text = "0";
        }
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            txtOrderQuantity.Text = "";
            ddlStockItem.SelectedValue = "";
            ddlStockCategoryID.SelectedValue = "";
            txtAvailableQuantity.Text = string.Empty;
        }

    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //if (ddlSearchdeliveryoption.SelectedValue == "1")
        //{
        //    GridView1.Columns[GridView1.Columns.Count-2].Visible = true;
        //}
        BindGrid(0);
        BindScript();
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void ddlStockLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            ddlStockItem.Items.Clear();

            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            if (ddlStockLocation.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "")
            {
                DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
                ddlStockItem.DataSource = dtStockItem;
                ddlStockItem.DataTextField = "StockItem";
                ddlStockItem.DataValueField = "StockItemID";
                ddlStockItem.DataBind();
                txtOrderQuantity.Text = string.Empty;
                txtAvailableQuantity.Text = string.Empty;
            }
            else if (ddldeliveryoption.SelectedIndex == 3 && ddlStockLocation.SelectedValue != "")
            {
                try
                {
                    ddlStockCategoryID.SelectedValue = "2";
                    DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location("2", ddlStockLocation.SelectedValue.ToString());
                    ddlStockItem.DataSource = dtStockItem;
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                    ddlStockItem.SelectedValue = "11186";
                    txtOrderQuantity.Text = "0";
                }
                catch
                { }
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {

        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");

        if (ddlStockLocation.SelectedValue != "")
        {
            ddlStockItem.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();
            if (ddlStockItem.SelectedValue == "")
            {
                txtAvailableQuantity.Text = string.Empty;
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }


    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
        TextBox txtOrderedQty = (TextBox)item.FindControl("txtOrderedQty");
        int LiveQty = 0;
        int ArisePickList = 0;
        int Smpl = 0;
        int Wholesale = 0;
        int Avalqty = 0;

        if (ddlStockLocation.SelectedValue != "")
        {
            if (ddlStockCategoryID.SelectedValue != "")
            {
                DataTable dtstock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockLocation.SelectedValue, ddlStockItem.SelectedValue);
                if (dtstock.Rows.Count > 0)
                {
                    txtAvailableQuantity.Text = dtstock.Rows[0]["StockQuantity"].ToString();
                    if (dtstock.Rows[0]["StockQuantity"].ToString() != null && dtstock.Rows[0]["StockQuantity"].ToString() != "")
                    {
                        LiveQty = Convert.ToInt32(dtstock.Rows[0]["StockQuantity"].ToString());
                        Avalqty= Convert.ToInt32(dtstock.Rows[0]["StockQuantity"].ToString());
                    }
                }
                else
                {
                    txtAvailableQuantity.Text = string.Empty;
                }
            }

            if (ddlStockItem.SelectedValue != "")
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_QuickStock_wholesale(ddlStockItem.SelectedValue, ddlStockLocation.SelectedValue);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        txtOrderedQty.Text = dtorderQty.Rows[0]["OrderQuantity"].ToString();
                    }
                    else
                    {
                        txtOrderedQty.Text = "0";
                    }
                }

                DataTable dtLive = null;
                int MinStock = 0;
                int LiveStock = 0;

                if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue))
                {
                    dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Wholesale(ddlStockItem.SelectedValue, ddlStockLocation.SelectedValue);
                }
                if (dtLive.Rows.Count > 0)
                {
                    MinStock = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                }
                if (dtLive.Rows.Count > 0)
                {
                    LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
                }
                if ((LiveStock <= MinStock))
                {
                    Notification("Minimum Stock is less than Live Stock,please Remove Item");
                    ddlStockItem.SelectedValue = "";
                    txtOrderQuantity.Text = "";
                    txtOrderedQty.Text = "";
                }
                if (ddlStockLocation.SelectedItem.ToString() != null && ddlStockLocation.SelectedItem.ToString() != "")
                {
                    string nm = ddlStockLocation.SelectedItem.ToString();
                    string []loc= nm.Split(':');
                   string  str = loc[1].Replace(" ", "");
                    if (loc.Length > 1)
                    {
                        DataTable dtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", str);
                        if (dtApl != null && dtApl.Rows.Count > 0)
                        {
                            if (dtApl.Rows[0]["Qty"].ToString() != null && dtApl.Rows[0]["Qty"].ToString() != "")
                            {
                                ArisePickList = Convert.ToInt32(dtApl.Rows[0]["Qty"].ToString());
                            }
                        }
                        DataTable dtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", str);
                        if (dtSmpl != null && dtApl.Rows.Count > 0)
                        {
                            if (dtSmpl.Rows[0]["Qty"].ToString() != null && dtSmpl.Rows[0]["Qty"].ToString() != "")
                            {
                                Smpl = Convert.ToInt32(dtSmpl.Rows[0]["Qty"].ToString());
                            }
                        }
                        DataTable dtWholsale = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, str);
                        if (dtWholsale != null && dtWholsale.Rows.Count > 0)
                        {

                            if (dtWholsale.Rows[0]["OrderQuantity"].ToString() != null && dtWholsale.Rows[0]["OrderQuantity"].ToString() != "")
                            {
                                Wholesale = Convert.ToInt32(dtWholsale.Rows[0]["OrderQuantity"].ToString());
                            }
                        }
                    }
                    int UsedCount = ArisePickList + Smpl + Wholesale;
                    int AvailableStock = Avalqty - UsedCount;
                    txtAvailableQuantity.Text = Convert.ToString(AvailableStock);
                }
            }

        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        //  BindScript();
    }

    protected void btnAddUpdateRow_Click(object sender, EventArgs e)
    {
        InitUpdate();
        AddmoreAttribute();
    }

    protected void rptattribute_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");
        Button btnRevertScannedItem = (Button)item.FindControl("btnRevertScannedItem");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtOrderQuantity");
        TextBox txtAvailableQuantity = (TextBox)e.Item.FindControl("txtAvailableQuantity");
        TextBox txtOrderedQty = (TextBox)e.Item.FindControl("txtOrderedQty");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");

        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(hdnStockCategory.Value, ddlStockLocation.SelectedValue.ToString());
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();
        HiddenField hdnOrderQuantity = (HiddenField)e.Item.FindControl("hdnOrderQuantity");
        HiddenField hdnAvailableQuantity = (HiddenField)e.Item.FindControl("hdnAvailableQuantity");
        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtOrderQuantity.Text = hdnOrderQuantity.Value;
        txtOrderedQty.Text = hdnOrderQuantity.Value;
        txtAvailableQuantity.Text = hdnAvailableQuantity.Value;
        //RequiredFieldValidator RequiredFieldValidator11 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator11");
        //RequiredFieldValidator RequiredFieldValidator1 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator1");
        //RequiredFieldValidator RequiredFieldValidator19 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator19");

        //if (e.Item.ItemIndex == 0)
        //{
        //    //btnDelete.Visible = false;
        //    chkdelete.Visible = false;
        //    litremove.Visible = false;
        //    //RequiredFieldValidator11.Visible = true;
        //    // RequiredFieldValidator1.Visible = true;
        //    //   RequiredFieldValidator19.Visible = true;
        //}
        //else
        //{
        //    //btnDelete.Visible = true;
        //    chkdelete.Visible = true;
        //    litremove.Visible = true;

        //    //RequiredFieldValidator11.Visible = false;
        //    //RequiredFieldValidator1.Visible = false;
        //    //  RequiredFieldValidator19.Visible = false;
        //}

        try//it should above code where remove btn ibdex=0 is made visible false
        {
            DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
            //DataTable dtstock2 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(hdnStockOrderID2.Value);
            //SttblStockOrderItems st = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockOrderItemID.Value);
            //if (dtstock.Rows.Count == Convert.ToInt32(hdnOrderQuantity.Value))
            //{
            //    PanelRepeater.Enabled = false;
            //    AddButtonDisable = AddButtonDisable + 1;

            //    // ddlStockCategoryID.Enabled = false;
            //}
            //else
            //{
            //    PanelRepeater.Enabled = true;
            //    //AddButtonDisable = 0;

            //    //   ddlStockCategoryID.Enabled = true;
            //}
            if (dtstock.Rows.Count > 0)
            {
                litremove.Visible = false;
                btnRevertScannedItem.Visible = true;
            }
            else
            {
                if (e.Item.ItemIndex == 0)
                {
                    //btnDelete.Visible = false;
                    chkdelete.Visible = false;
                    litremove.Visible = false;
                    //RequiredFieldValidator11.Visible = true;
                    // RequiredFieldValidator1.Visible = true;
                    //   RequiredFieldValidator19.Visible = true;
                }
                else
                {
                    //btnDelete.Visible = true;
                    chkdelete.Visible = true;
                    litremove.Visible = true;

                    //RequiredFieldValidator11.Visible = false;
                    //RequiredFieldValidator1.Visible = false;
                    //  RequiredFieldValidator19.Visible = false;
                }
                btnRevertScannedItem.Visible = false;
            }
        }
        catch { }
    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("AvailableQty", Type.GetType("System.String"));
        rpttable.Columns.Add("WholesaleOrderItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));


        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["OrderQuantity"] = txtOrderQuantity.Text;
            dr["AvailableQty"] = txtAvailableQuantity.Text;
            dr["WholesaleOrderItemID"] = hdnWholesaleOrderItemID.Value;
            dr["type"] = hdntype.Value;


            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
            rpttable.Columns.Add("AvailableQty", Type.GetType("System.String"));
            rpttable.Columns.Add("WholesaleOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("txtOrderedQty", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["OrderQuantity"] = "";
            dr["AvailableQty"] = "";
            dr["WholesaleOrderItemID"] = "";
            dr["type"] = "";
            dr["OrderQuantity"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            HiddenField hdnStockCategory = (HiddenField)item1.FindControl("hdnStockCategory");
            HiddenField hdnStockItem = (HiddenField)item1.FindControl("hdnStockItem");
            Button btnRevertScannedItem = (Button)item1.FindControl("btnRevertScannedItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            try
            {
                DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
                if (dtstock.Rows.Count > 0)
                {
                    lbremove1.Visible = false;
                    btnRevertScannedItem.Visible = true;
                }
                else
                {
                    lbremove1.Visible = true;
                    btnRevertScannedItem.Visible = false;
                }
            }
            catch
            {
                lbremove1.Visible = true;
            }
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlShow.SelectedValue = "";
    //    ddlDue.SelectedValue = "";
    //    ddlSearchVendor.SelectedValue = "";
    //    ddlDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchOrderNo.Text = string.Empty;

    //    BindGrid(0);
    //}
    //protected void btnNewVendor_Click(object sender, ImageClickEventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    ModalPopupExtenderVendor.Show();
    //    txtCompany.Text = string.Empty;
    //    txtContFirst.Text = string.Empty;
    //    txtContLast.Text = string.Empty;
    //    BindScript();
    //}
    protected void ibtnaddvendor_click(object sender, EventArgs e)
    {

        if (txtCompany.Text != string.Empty)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string employeeid = st.EmployeeID;

            int success = ClstblCustomers.tblCustomers_Insert("", employeeid, "false", "6", "3", "", "", "", "1", employeeid, "", "", "", "", "", txtCompany.Text, "", "", "", "", "", "", "", "", "", "", "australia", "", "", "", "", "", "", "", "", "", "", "false", "", "", "", "", "", "", "", "");
            //Here 6 is custtyype id for wholesale customers binded it will be 6 but for vendors in stock order it is 13 
            //ddlvendor searches for 13 for stockorder but for wholesale it will be 6.
            int succontacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", txtContFirst.Text.Trim(), txtContLast.Text.Trim(), "", "", employeeid, employeeid);
            int succustinfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "customer entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(succontacts), employeeid, "1");

            if (Convert.ToString(success) != string.Empty)
            {
                ModalPopupExtenderVendor.Hide();
                BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(success);
                Notification("There are no items to show in this view");
            }
            else
            {
                Notification("Transaction Failed.");
            }
        }
    }
    protected void btnNewStock_Click(object sender, EventArgs e)
    {
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        chkDashboard.Checked = false;

        PanAddUpdate.Visible = true;
        ModalPopupExtenderStock.Show();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlstockcategory.Items.Clear();
        ddlstockcategory.Items.Add(item8);

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dtStockCategory;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindScript();
    }
    protected void ibtnAddStock_Click(object sender, EventArgs e)
    {
        InitAdd();
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();

        int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
        //ClstblStockItems.wholesaleitemitemNpanels(stockcategory);

        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hyplocationid = (HiddenField)item.FindControl("hyplocationid");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            if (txtqty.Text != "" || hyplocationid.Value != "")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
            }
        }
        if (success > 0)
        {
            ModalPopupExtenderStock.Hide();
            Notification("There are no items to show in this view");
        }
        else
        {
            ModalPopupExtenderStock.Show();
            Notification("Transaction Failed.");
        }
        BindScript();
    }


    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }


    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
        ScriptManager.RegisterStartupScript(updatepanel3, this.GetType(), "MyAction", "doMyAction();", true);

    }

    
    protected void btndeliver_Click(object sender, EventArgs e)
    {

        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        if (receive != string.Empty)
        {
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_ActualDelivery(hndid.Value, receive);
        }

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        if (success)
        {
            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hndid.Value);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                    ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                }
            }
            //SetDelete();

            int success1 = 0;
            //Response.Expires = 400;
            //Server.ScriptTimeout = 1200;

            if (FileUpload1.HasFile)
            {
                Notification("There are no items to show in this view");
                FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
                string connectionString = "";

                //if (FileUpload1.FileName.EndsWith(".csv"))
                //{
                //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
                //}
                // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

                //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


                if (FileUpload1.FileName.EndsWith(".xls"))
                {
                    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
                }
                else if (FileUpload1.FileName.EndsWith(".xlsx"))
                {
                    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

                }


                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    using (DbCommand command = connection.CreateCommand())
                    {
                        // Cities$ comes from the name of the worksheet

                        command.CommandText = "SELECT * FROM [Sheet1$]";
                        command.CommandType = CommandType.Text;

                        // string employeeid = string.Empty;
                        //string flag = "true";
                        // SttblStockOrders st = Clstbl_WholesaleOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
                        DataTable dt2 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hndid.Value);
                        DataRow dr1 = dt2.Rows[0];
                        string stockid = dr1["StockItemID"].ToString();
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                Notification("There are no items to show in this view");
                                //if (flag == "false")
                                //{
                                //    PanEmpty.Visible = true;
                                //}
                                //else
                                //{
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";

                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();
                                    if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                    {
                                        //try
                                        //{

                                        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                        // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                        //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                        //Response.End();
                                        success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);
                                        //}
                                        //catch { }
                                    }
                                    if (success1 > 0)
                                    {
                                        Notification("There are no items to show in this view");
                                    }
                                    else
                                    {
                                        Notification("Transaction Failed.");
                                    }
                                    //}
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            SetError();
        }
        txtdatereceived.Text = string.Empty;

        // txtserialno.Text = string.Empty;


        BindGrid(0);
    }
    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnNewVendor_Click1(object sender, EventArgs e)
    {
        ModalPopupExtenderVendor.Show();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        txtCompany.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        BindScript();
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchEmployee.SelectedValue = "";
        ddlShow.SelectedValue = "";
        ddlDue.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        txtreferenceno.Text = string.Empty;
        ddlDeliveredOrNot.SelectedValue = "False";
        ddlEmail.SelectedValue = "";
        divEmailFilter.Visible = false;
        ddlSearchTransportType.SelectedValue = "";
        ddlsearchInstallType.SelectedValue = "";
        ddlsearchinstallername.SelectedValue = "";
        ddlsearchSolarType.SelectedValue = "";
        ddlsearchJobStatus.SelectedValue = "";
        ddlSearchjobtype.SelectedValue = "";
        ddlSearchdeliveryoption.SelectedValue = "";
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindLocation();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["State"].ToString();
            ddlSearchState.SelectedValue = CompanyLocationID;
            ddlSearchState.Enabled = false;
        }


        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }
    protected void btnDelivered_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.TrimEnd(',');

        bool sucess1 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Cancelled(id, Convert.ToString(true));
        Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_DeleteWholesaleOrderID(id);
        Clstbl_WholesaleOrders.tbl_WholesaleOrders_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            // SetDelete();
            Notification("Transaction Successful.");
            //PanSuccess.Visible = true;
            //Response.Redirect(Page.Request.RawUrl, false);
        }
        else
        {
            Notification("Transaction Failed.");
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        //BindGrid(1);
        PanAddUpdate.Visible = false;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            LinkButton hypDetail = (LinkButton)e.Row.FindControl("hypDetail");
            LinkButton btnviewPDF = (LinkButton)e.Row.FindControl("btnviewPDF");
            LinkButton btnMail = (LinkButton)e.Row.FindControl("btnMail");
            LinkButton btnRevert = (LinkButton)e.Row.FindControl("btnRevert");
            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            HyperLink lnkEmailSent = (HyperLink)e.Row.FindControl("lnkEmailSent");
            HyperLink lnkEmailNotSent = (HyperLink)e.Row.FindControl("lnkEmailNotSent");
            LinkButton gvbtnpdf = (LinkButton)e.Row.FindControl("LinkButton9");

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")))
            {
                hypDetail.Visible = true;
                btnviewPDF.Visible = true;
                //btnMail.Visible = true;
                //btnRevert.Visible = true;
                gvbtnUpdate.Visible = true;
                //gvbtnDelete.Visible = true;
                //lnkEmailSent.Visible = true;
                //lnkEmailNotSent.Visible = true;
            }
            else if ((Roles.IsUserInRole("Wholesale")))
            {
                btnRevert.Visible = false;
                hypDetail.Visible = true;
                btnviewPDF.Visible = true;
                //btnMail.Visible = true;
                gvbtnUpdate.Visible = true;
                //gvbtnDelete.Visible = true;
            }
            else if ((Roles.IsUserInRole("Accountant")))
            {
                lnkAdd.Enabled = false;
                lnkBack.Enabled = false;
                btnRevert.Visible = false;
                gvbtnUpdate.Enabled = false;
                gvbtnDelete.Enabled = false;
                btnMail.Enabled = false;
                gvbtnpdf.Enabled = false;
                lnkEmailSent.Enabled = false;
                lnkEmailNotSent.Enabled = false;
            }
        }
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            HiddenField hdnStockCategory = (HiddenField)item1.FindControl("hdnStockCategory");
            HiddenField hdnStockItem = (HiddenField)item1.FindControl("hdnStockItem");
            Button btnRevertScannedItem = (Button)item1.FindControl("btnRevertScannedItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            try
            {
                DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
                if (dtstock.Rows.Count > 0)
                {
                    lbremove1.Visible = false;
                    btnRevertScannedItem.Visible = true;
                }
                else
                {
                    lbremove1.Visible = true;
                    btnRevertScannedItem.Visible = false;
                }
            }
            catch
            {
                lbremove1.Visible = true;
            }
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void txtVendorInvoiceNo_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtVendorInvoiceNo.Text))
        {
            int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByInvoiceNo(txtVendorInvoiceNo.Text);
            if (exist == 1)
            {
                Notification("Field with this value already exists in the records.");
                txtVendorInvoiceNo.Text = string.Empty;
            }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void ReferenceNo_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtManualOrderNumber.Text))
        {
            int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByReferenceNo(txtManualOrderNumber.Text);
            if (exist == 1)
            {
                Notification("Field with this value already exists in the records.");
                txtManualOrderNumber.Text = string.Empty;
            }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void gvbtnPrint_Click(object sender, EventArgs e)
    {
        Telerik_reports.generate_WholesalePicklist(hndWholesaleOrderID.Value.TrimEnd(','));
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        //DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectBySearchExcel(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), txtreferenceno.Text.Trim(), "", "", ddlDeliveredOrNot.SelectedValue, ddlSearchTransportType.SelectedValue, ddlsearchInstallType.SelectedValue, ddlsearchinstallername.SelectedValue, ddlsearchJobStatus.SelectedValue, ddlsearchSolarType.SelectedValue, ddlSearchjobtype.SelectedValue, ddlSearchdeliveryoption.SelectedValue, ddlSearchEmployee.SelectedValue);
        //Response.Clear();
        //dt.Constraints = false;
        int count = dt.Rows.Count;
        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "Wholesaleorder_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
  int[] ColList = { 17, 60,4,63, 23, 15,50, 29, 65, 61, 68, 70,75};
            string[] arrHeader = { "Invoice No","Invoice Type","Invoice Date","Stock For","Job Type","STC ID",
                    "STC","Delivery Option","Customer","Qty","Delivered","Delivered By","Employee Name"};
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void btnOK3btnOK3_Click(object sender, EventArgs e)
    {
        string ID = hndid2.Value;
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsDeduct_Date_User(ID, "0", "", "");
        bool success2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(ID, "false");

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(ID);

        DataTable dtQty = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectQty(ID);
        if (success)
        {
            DataTable dt1 = Clstbl_WholesaleOrders.tblStockSerialNo_Select_ByWholesaleOrderID(ID);
            if (dt1.Rows.Count > 0)
            {
                string SerialNo = "";
                foreach (DataRow dtserial in dt1.Rows)
                {
                    SerialNo = dtserial["SerialNo"].ToString();

                    string Section = "Wholesale IN";
                    string Message = "Wholesale revert for Order Number:" + ID;

                    ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, ID, SerialNo, Section, Message, Currendate);
                    ClstblStockSerialNo.tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo(SerialNo, "0", "0");
                }
            }

            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(ID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", row["StockItemID"].ToString(), st.CompanyLocationID, stOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, row["WholesaleOrderID"].ToString(), row["WholesaleOrderItemID"].ToString(), st.InvoiceNo);
                    Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    ClsProjectSale.tblStockItems_RevertStock(row["StockItemID"].ToString(), row["OrderQuantity"].ToString(), st.CompanyLocationID.ToString());
                }
                //Response.End();
            }
            //SetDelete();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
    }

    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddljobtype.SelectedValue == "2")//stc
        {
            RequiredFieldValidator12.Enabled = true;
        }
        else
        {
            RequiredFieldValidator12.Enabled = false;
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void btnRevertScannedItem_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        HiddenField hdnStockItem = (HiddenField)item.FindControl("hdnStockItem");
        ModalPopupExtenderRevertScannedItem.Show();
        hndStockItemID3.Value = hdnStockItem.Value;
    }

    protected void btnOK4_Click(object sender, EventArgs e)
    {
        DataTable dt1 = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderIDStockItemID(hdnWholesaleOrderID2.Value, hndStockItemID3.Value);
        if (dt1.Rows.Count > 0)
        {
            string SerialNo = "";
            foreach (DataRow dtserial in dt1.Rows)
            {
                SerialNo = dtserial["SerialNo"].ToString();
                ClstblStockSerialNo.tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo(SerialNo, "0", "0");
            }
        }

        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hdnWholesaleOrderID2.Value);

        // Notification("Item Reverted Successfully.");//write notification above binding repeater again
        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;

        }
    }

    protected void ddldeliveryoption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldeliveryoption.SelectedIndex != 0)
        {
            ddlTransporttype.Items.Clear();
            ddlTransporttype.Items.Insert(0, new ListItem("Select", ""));


            divtransporttype.Visible = true;
            divconsign.Visible = true;

            if (ddldeliveryoption.SelectedIndex == 1)
            {
                BindTransportType();
                ddlTransporttype.SelectedValue = "4";
                // divtransporttype.Visible = true;
            }
            else if (ddldeliveryoption.SelectedIndex == 3)
            {
                BindTransportType();
                ddlTransporttype.SelectedValue = "4";
                foreach (RepeaterItem item in rptattribute.Items)
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");

                    if (ddlStockLocation.SelectedValue != "")
                    {
                        try
                        {
                            ddlStockCategoryID.SelectedValue = "2";
                            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location("2", ddlStockLocation.SelectedValue.ToString());
                            ddlStockItem.DataSource = dtStockItem;
                            ddlStockItem.DataTextField = "StockItem";
                            ddlStockItem.DataValueField = "StockItemID";
                            ddlStockItem.DataBind();
                            ddlStockItem.SelectedValue = "11186";
                            txtOrderQuantity.Text = "0";
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        Notification("Please Select Company Locatation");
                    }
                }
            }
            else
            {
                BindTransportType();
                // divtransporttype.Visible = true;
            }
        }
        else
        {
            //divtransporttype.Visible = false;
            divtransporttype.Visible = true;
            divconsign.Visible = true;
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;


    }

    protected void ddlinstallertype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlinstallertype.SelectedIndex != 0)
        {
            ddlinstallername.Items.Clear();
            ddlinstallername.Items.Insert(0, new ListItem("Select", ""));
            divinstallername.Visible = true;
            BindInstallerName();
        }
        else
        {
            divinstallername.Visible = true;
            //divinstallername.Visible = false;
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void txtOrderQuantity_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
        string wholsaleid = hdnWholesaleOrderID2.Value;
        if (!string.IsNullOrEmpty(txtOrderQuantity.Text) && (!string.IsNullOrEmpty(wholsaleid)))
        {
            DataTable datacount = Clstbl_WholesaleOrders.tbltblStockSerialNo_WholesaleorderCount(wholsaleid, ddlStockItem.SelectedValue);
            string Count = datacount.Rows[0]["Column1"].ToString();
            if (Convert.ToInt32(txtOrderQuantity.Text) < Convert.ToInt32(Count.ToString()))
            {
                Notification("Enter more then " + Convert.ToInt32(Count.ToString()) + " Quantity");

            }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void ddlDeliveredOrNot_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDeliveredOrNot.SelectedValue != "")
            {
                if (ddlDeliveredOrNot.SelectedItem.Text == "Dispatched")
                {
                    divEmailFilter.Visible = true;

                }
                else
                {
                    divEmailFilter.Visible = false;
                    ddlEmail.SelectedValue = "";

                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        // BindEmployee();
        try
        {
            string cust_id = ddlVendor.SelectedValue;
            if (cust_id != null && cust_id != "")
            {
                DataTable dt = Clstbl_WholesaleOrders.tblCustomers_SelectByCustomerID(cust_id);
                if (dt.Rows[0]["EmployeeID"].ToString() != null && dt.Rows[0]["EmployeeID"].ToString() != "")
                {
                    ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}