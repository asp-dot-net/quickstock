﻿using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_transfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(Request.QueryString["id"]);
            lblFrom.Text = st.FromLocation;
            lblTo.Text = st.ToLocation;
            try
            {
                lblTransferDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.TransferDate));
            }
            catch { }
            lblTransferBy.Text = st.TransferByName;
            //lblQty.Text = st.TransferByName;
            //lblCode.Text = st.TransferByName;
            //lblItem.Text = st.TransferByName;
            lblTrackingNo.Text = st.TrackingNumber;
            try
            {
                // lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedDate));
                lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedByDate));
            }
            catch { }
            lblReceivedBy.Text = st.ReceiveByUserName;
           // lblReceivedBy.Text = st.ReceivedByName;
            lblNotes.Text = st.TransferNotes;

            DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(Request.QueryString["id"]);
            if (dt.Rows.Count > 0)
            {
                rptItems.DataSource = dt;
                rptItems.DataBind();

                int qty = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["TransferQty"].ToString() != string.Empty)
                    {
                        qty += Convert.ToInt32(dr["TransferQty"].ToString());
                    }
                }
                lbltotalqty.Text = qty.ToString();
            }
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stocktransfer.aspx");
    }

}