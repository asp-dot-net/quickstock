using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockorderreport : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {

            BindVendor();
            BindStockCategory();
            rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            rptState.DataBind();

            //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
            //rptlocations.DataSource = dt;
            //rptlocations.DataBind();
            BindGrid(0);
        }
    }
    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();

        ddlSearchVendor.DataSource = dt;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();

        DataTable dt1 = ClstblStockItems.tblStockItems_SelectAll();
        ddlitem.DataSource = dt1;
        ddlitem.DataTextField = "StockItem";
        ddlitem.DataValueField = "StockCode";
        ddlitem.DataBind();
    }

    public void BindStockCategory()
    {
        DataTable dt = ClstblStockCategory.tblStockCategory_Select_ByAsc();
       
        ddlcategorysearch.DataSource = dt;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();

    
    }
    protected DataTable GetGridData()
    {
        //leadstatus
        string location = "";
        foreach (RepeaterItem item in rptState.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                location += "," + hdnID.Value.ToString();
            }
        }
        if (location != "")
        {
            location = location.Substring(1);
        }

        DataTable dt = new DataTable();
        dt = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();// tblCompanyLocations_GetStates(location);
        //dt = ClstblStockOrders.tblStockOrdersReport_Search(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, ddlitem.SelectedValue, location);


        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
       // PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
        }
        else
        {
            rptlocations.DataSource = dt;
            rptlocations.DataBind();

            //main
            string location = "";
            foreach (RepeaterItem item in rptState.Items)
            {
                CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
                HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

                if (chkselect.Checked == true)
                {
                    location += "," + hdnID.Value.ToString();
                }
            }
            if (location != "")
            {
                location = location.Substring(1);
            }
            //Response.Write(ddlShow.SelectedValue + "=" + ddlSearchVendor.SelectedValue + "=" + txtStartDate.Text.Trim() + "=" + txtEndDate.Text.Trim() + "=" + ddlDate.SelectedValue + "=" + ddlitem.SelectedValue + "=" + location + "=" + ddlStockCategory.SelectedValue);
            //Response.End();
            dt1 = ClstblStockOrders.tblStockOrdersReport_Search(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, ddlitem.SelectedValue, location, ddlcategorysearch.SelectedValue);
            //Response.Write(dt1.Rows.Count);
            //Response.End();
            rpt.DataSource = dt1;
            rpt.DataBind();
            if (dt1.Rows.Count > 0)
            {
                PanGrid.Visible = true;
            }
            else
            {
                PanGrid.Visible = false;
            }

            PanNoRecord.Visible = false;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        HidePanels();
        Reset();
        SetAdd1();
        //PanSuccess.Visible = true;

        BindScript();
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();

    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
       // PanNoRecord.Visible = false;

    }
    public void Reset()
    {
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;
    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlShow.SelectedValue = "";
    //    ddlcategorysearch.SelectedValue = "";
    //    ddlSearchVendor.SelectedValue = "";
    //    ddlDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;


    //    BindGrid(0);
      
    //}

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;


        Repeater rptqty = (Repeater)item.FindControl("rptqty");
        HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
        HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
        //Response.Write(hdnStockItemID.Value + "=" + hdnState.Value);
        DataTable dt1 = ClstblCompanyLocations.tblCompanyLocations_SelectbyStockItemID(hdnStockItemID.Value);//ClstblStockOrders.tblStockOrdersReport_ByLocSearch(hdnStockItemID.Value, hdnState.Value);
        if (dt1.Rows.Count > 0)
        {
            rptqty.DataSource = dt1;
            rptqty.DataBind();
        }

















        //HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
        //HiddenField hdnQty = (HiddenField)item.FindControl("hdnQty");

        //SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(hdnStockItemID.Value);

        //SttblStockOrderItems stitem = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockOrderItemID.Value);
        //SttblStockOrders stitem1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(stitem.StockOrderID);




        //SttblStockOrders stitem = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hdnStockItemID.Value);
        //string state = stitem.
        //SttblStockOrders storder = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hdnStockOrderID.Value);
        //DataTable dtorder = ClstblStockOrders.stockitemid(hdnStockOrderID.Value);
        //SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(hdnStockItemID.Value);

        //if (dtorder.Rows.Count > 0 && dtorder.Rows[0]["StockItemID"].ToString() !=string.Empty)
        {
            // string stockitemid = dtorder.Rows[0]["StockItemID"].ToString();

            //SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(stockitemid);
            //Literal ltFTY = (Literal)item.FindControl("ltFTY");
            //Literal ltNSW = (Literal)item.FindControl("ltNSW");
            //Literal ltQLD = (Literal)item.FindControl("ltQLD");
            //Literal ltTAS = (Literal)item.FindControl("ltTAS");
            //Literal ltVIC = (Literal)item.FindControl("ltVIC");
            //Literal ltWHS = (Literal)item.FindControl("ltWHS");
            // DataTable dt1 = ClstblStockOrders.tblStockOrders_getStockItemState(stockitemid, hdnState.Value);
            //if (dt1.Rows.Count > 0)
            //{
            //    if (hdnState.Value == "FTY")
            //    {
            //        ltFTY.Text = hdnQty.Value;
            //    }
            //    if (hdnState.Value == "NSW")
            //    {
            //        ltNSW.Text = hdnQty.Value;//dt1.Rows[0]["Qty"].ToString();
            //    }
            //    if (hdnState.Value == "QLD")
            //    {
            //        //Response.Write(hdnState.Value);
            //        ltQLD.Text = hdnQty.Value;//dt1.Rows[0]["Qty"].ToString();
            //    }
            //    if (hdnState.Value == "TAS")
            //    {
            //        ltTAS.Text = hdnQty.Value;//dt1.Rows[0]["Qty"].ToString();
            //    }
            //    if (hdnState.Value == "VIC")
            //    {
            //        ltVIC.Text = hdnQty.Value;//dt1.Rows[0]["Qty"].ToString();
            //    }
            //    if (hdnState.Value == "WHS")
            //    {
            //        ltWHS.Text = hdnQty.Value;//dt1.Rows[0]["Qty"].ToString();
            //    }
            //}

            //Response.Write(hdnState.Value);
            //Response.End();
            //DataTable dt1 = ClstblStockOrders.tblStockOrdersReport_ByLocSearch(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, stitem.StockItemID, hdnState.Value);
            //Response.Write(dt1.Rows.Count);
            //rptqty.DataSource = dt1;
            //rptqty.DataBind();
            //if (stitem.CompanyLocationID != string.Empty)
            //{
            //    SttblCompanyLocations ststate = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(stitem.CompanyLocationID);
            //    string state = ststate.State;
            //    if (hdnState.Value == state)
            //    {
            //        Response.Write("1");
            //    }
            //    //Response.Write(hdnState.Value + "=" + state);
            //}

            //SttblStockOrderItems stitem = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockItemID.Value);
            //ltStockItem.Text = stitem.StockOrderItem;

            //Repeater rptLoc = (Repeater)item.FindControl("rptLoc");
            //DataTable dt1 = new DataTable();
            //string location = "";
            //foreach (RepeaterItem item1 in rptState.Items)
            //{
            //    CheckBox chkselect = (CheckBox)item1.FindControl("chkselect");
            //    HiddenField hdnID = (HiddenField)item1.FindControl("hdnID");

            //    if (chkselect.Checked == true)
            //    {
            //        location += "," + hdnID.Value.ToString();
            //    }
            //}
            //if (location != "")
            //{
            //    location = location.Substring(1);
            //}
            //HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
            //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_GetStates(location);
            //foreach (DataRow dr in dt.Rows)
            //{
            //    rptLoc.DataSource = null;
            //    rptLoc.DataBind();
            //    if (dr["State"].ToString() == hdnState.Value)
            //    {
            //        Response.Write(dr["State"]);
            //    }
            //}
            //if (hdnState.Value == location)
            //{
            //Response.Write(ddlShow.SelectedValue + "','" + ddlSearchVendor.SelectedValue + "','" + txtStartDate.Text.Trim() + "','" + txtEndDate.Text.Trim() + "','" + ddlDate.SelectedValue + "','" + hdnStockItemID.Value + "','" + hdnState.Value+ "','" +location+"'<br/>");
            //    dt1 = ClstblStockOrders.tblStockOrdersReport_ByLocSearch(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, hdnStockItemID.Value, location);

            //    //Response.End();

            //    if (dt1.Rows.Count > 0)
            //    {
            //        rptLoc.DataSource = dt1;
            //        rptLoc.DataBind();
            //    }
            //    else
            //    {
            //        rptLoc.DataSource = null;
            //        rptLoc.DataBind();
            //    }
            //    DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            //    foreach (RepeaterItem itemloc in rptLoc.Items)
            //    {
            //        Literal ltQty = (Literal)itemloc.FindControl("ltQty");
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            Response.Write(hdnState.Value + " == " + dr["State"] + "<br/>");
            //            if (hdnState.Value == dr["State"])
            //            {
            //                Response.Write(ltQty.Text);
            //            }
            //        }
            //    }
            //}
        }
        BindScript();
    }
    protected void rptLoc_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
    }

    protected void rptqty_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
        HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
        //Response.Write(hdnStockItemID.Value + "=" + hdnState.Value);
        Literal ltstate = (Literal)item.FindControl("ltstate");
        //Response.Write(ddlShow.SelectedValue + "=" + ddlSearchVendor.SelectedValue + "=" + txtStartDate.Text.Trim() + "=" + txtEndDate.Text.Trim() + "=" + ddlDate.SelectedValue + "=" + hdnStockItemID.Value + "=" + hdnState.Value+"<br/>");
        DataTable dt1 = ClstblStockOrders.tblStockOrdersReport_ByLocSearch(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, hdnStockItemID.Value, hdnState.Value);

        if (dt1.Rows.Count > 0)
        {
            ltstate.Text = dt1.Rows[0]["Qty"].ToString();
        }
        else
        {
            ltstate.Text = "0";
        }
        BindScript();

        //Literal ltFTY = (Literal)item.FindControl("ltFTY");
        //HiddenField hdnStockOrderID = (HiddenField)item.FindControl("hdnStockOrderID");
        //HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
        //SttblStockOrders stitem = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hdnStockOrderID.Value);
        //DataTable dt = ClstblStockOrders.tblStockOrders_getStockItemState(stitem.StockItemID, hdnState.Value);
        ////Response.Write(hdnStockItemID.Value + "=");
        //if (dt.Rows.Count > 0)
        //{
        //    //Response.Write(dt.Rows[0]["Qty"]+"<br/>");
        //    if (hdnState.Value == "FTY")
        //    {
        //        ltFTY.Text = dt.Rows[0]["Qty"].ToString();
        //    }
        //    //if (dt.Rows[0]["Qty"] != DBNull.Value)
        //    //{
        //    //    //Response.Write(dt.Rows[0]["Qty"] + "<br/>");
        //    //    ltStockItem.Text = dt.Rows[0]["Qty"].ToString();
        //    //}
        //}
    }


    //EXCEL
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        PrepareGridViewForExport(PanGrid);
        string timestemp = DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss").ToString().Replace(" ", "_");
        string attachment = "attachment; filename=StockOrderReport " + timestemp + ".xls";

        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        PanGrid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    private void PrepareGridViewForExport(Control gv)
    {
        Repeater lb = new Repeater();
        Literal l = new Literal();
        string name = String.Empty;

        for (int i = 0; i < gv.Controls.Count; i++)
        {
            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {
                l.Text = (gv.Controls[i] as LinkButton).Text;
                Response.Write(l.Text);
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(Label))
            {
                l.Text = (gv.Controls[i] as Label).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HyperLink))
            {

                l.Text = (gv.Controls[i] as HyperLink).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {
                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {
                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HiddenField))
            {
                l.Text = "";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            if (gv.Controls[i].HasControls())
            {
                PrepareGridViewForExport(gv.Controls[i]);
            }
        }
        //Response.End();
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlShow.SelectedValue = "";
        ddlcategorysearch.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;


        BindGrid(0);
    }
}