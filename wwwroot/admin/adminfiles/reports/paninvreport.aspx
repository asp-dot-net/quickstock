﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="paninvreport.aspx.cs" Inherits="admin_adminfiles_reports_paninvreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                }
                function pageLoaded(sender, args) {

                    document.getElementById('loader_div').style.visibility = "hidden";

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });
                }

            </script>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Panel Inverter Report
                    </h5>


                </div>
            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                        <div class="searchfinal">
                            <div class="card shadownone brdrgray pad10">
                                <div class="card-block">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <div class="inlineblock martop5">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170" id="divCustomer" runat="server">
                                                    <asp:DropDownList ID="ddlType" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Panel</asp:ListItem>
                                                        <asp:ListItem Value="2">Inverter</asp:ListItem>
                                                        <asp:ListItem Value="3">Roof Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                        <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                    </div>
                                                </div>
                                                <div class="input-group col-sm-2">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                      &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="datashowbox inlineblock">
                                        <div class="row">
                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="form-control input-sm myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                               <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>

                                                <a href="javascript:window.print();" class="btn btn-primary btn-xs Print" style="display:none;">
                                                    <i class="fa fa-print">Print</i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Project">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject" runat="server" Width="300px">
                                                            <%--<asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>--%>
                                                                <%#Eval("Project")%><%--</asp:HyperLink>--%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject9" runat="server" Width="70px"><%#Eval("ProjectNumber")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="No of Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="NumberPanels">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject3239" runat="server" Width="70px"><%#Eval("NumberPanels")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Stock Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="StockItem">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject93" runat="server" Width="90px"><%#Eval("StockItem")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Sys" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="SystemCapKW" HeaderStyle-CssClass="gridheadertext">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject11" runat="server" Width="50px"><%#Eval("SystemCapKW")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Roof Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="housetype">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProjectRoofType" runat="server" Width="100px"><%#Eval("RoofType")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Install Book Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="InstallBookingDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject14" runat="server" Width="120px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="InstallState">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProject16" runat="server" Width="80px"><%#Eval("InstallState")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

