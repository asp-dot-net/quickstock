﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.Common;
using System.IO;

public partial class admin_adminfiles_reports_StockPendingItemWiseReport : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;
    static DataView dv3;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            

            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();

            if (ddlIsDifference.SelectedValue == "1")
            {
                ddlAudit.SelectedValue = "1";
                DivAudit.Visible = true;
            }
            else
            {
                DivAudit.Visible = false;
            }

            BindCheckboxProjectTab();
            BindGrid(0);
        }

    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        #region IsDifference
        string IsDifference = "";
        if (!string.IsNullOrEmpty(ddlIsDifference.SelectedValue))
        {
            IsDifference = ddlIsDifference.SelectedValue;
        }
        #endregion

        #region InstallerName
        string InstallerName = "";
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnAriseInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                InstallerName += "," + hdnModule.Value.ToString();
            }
        }
        if (InstallerName != "")
        {
            InstallerName = InstallerName.Substring(1);
        }
        #endregion

        #region Location
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }
        #endregion

        DataTable dt = Reports.QuickStock_StockPendingReport_ItemWise(txtProjectNumber.Text, InstallerName, txtStockItem.Text, txtStockModel.Text, Location, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlIsDifference.SelectedValue, ddlAudit.SelectedValue, ddlCategory.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
        // bind();
        BindTotal(dt);
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(StockDeducted)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(SaleQty)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int PanelRevert = Convert.ToInt32(dt.Compute("SUM(Revert)", string.Empty));
            int PAudit = (PanelDiff - PanelRevert);
            
            lblpout.Text = PanelStockDeducted.ToString();
            lblPInstalled.Text = SaleQtyPanel.ToString();
            lblPDifference.Text = PanelDiff.ToString();
            lblPRevert.Text = PanelRevert.ToString();
            lblPAudit.Text = PAudit.ToString();
        }
        else
        {
            lblpout.Text = "0";
            lblPInstalled.Text = "0";
            lblPDifference.Text = "0";
            lblPRevert.Text = "0";
            lblPAudit.Text = "0";
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        //DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeID("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "");
        //Response.Clear();
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 19, 22, 3, 4, 21, 16, 20 };
        //    string[] arrHeader = { "Project No.", "Installer Name", "Serial No.", "Pallet No.", "Category", "Stock Item", "Location" };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        //txtstockitemfilter.Text = string.Empty;
        //txtserailno.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "";
        //ddlInstaller.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlIsDifference.SelectedValue = "1";
        //ddlprojectwise.SelectedValue = "1";
        //lstProjectStatus.ClearSelection();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            //ddllocationsearch.Enabled = false;
        }

        ClearCheckBox();
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "verify")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            }
            ModalPopupExtenderverify.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];


            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView1.Rows[rowIndex].FindControl("Label82") as Label).Text;
            string PanelOut = (GridView1.Rows[rowIndex].FindControl("Label452") as Label).Text;

            string Projectid = (GridView1.Rows[rowIndex].FindControl("hndProjectID") as HiddenField).Value;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = PanelOut;

                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            //hndProjectID1.Value = Projectid;

            if (ProjectNo != "0" && PicklistId != "0")
            {
                Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            // chkisactive.Visible = true;
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView1.Rows[rowIndex].FindControl("Label152") as Label).Text;
            string InvertOut = (GridView1.Rows[rowIndex].FindControl("Label4522") as Label).Text;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = InvertOut;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            string Projectid = (GridView1.Rows[rowIndex].FindControl("hndProjectID") as HiddenField).Value;
            //hndProjectID1.Value = Projectid;

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }
        if (e.CommandName.ToLower() == "note")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            DataTable dt;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
                dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        txtnotedesc.Text = dt.Rows[0]["VerifynoteIN"].ToString();
                        DateTime notedt1 = Convert.ToDateTime(dt.Rows[0]["notedateIN"].ToString());
                        txtnotedate.Text = notedt1.ToString("dd/MM/yyyy");
                    }
                    catch { }
                }
                else
                {
                    txtnotedesc.Text = "";
                    txtnotedate.Text = "";
                }
            }
            else
            {
                txtnotedesc.Text = "";
                txtnotedate.Text = "";
            }
            //txtnotedesc.Text = "";
            //txtnotedate.Text = "";
            ModalPopupExtenderNote.Show();
        }
        //if (e.CommandName == "Transfer")
        //{
        //    string[] arg = new string[2];
        //    arg = e.CommandArgument.ToString().Split(';');

        //    string ProjectNo = arg[0];
        //    string PicklistId = arg[1];
        //    ClstblProjects.tbl_picklistlog_UpdateData(ProjectNo, PicklistId,"1","","");
        //}
        if (e.CommandName == "Email")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string InstallerEmail = arg[2];

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Project Detail";

            Server.Execute("~/mailtemplate/InstallerWiseEmail.aspx?ProjectNo=" + ProjectNo + "&PickList=" + PicklistId + "&WholesaleOrderID=0" + "&MAiltype=PickList", txtWriter);

            Utilities.SendMail(from, InstallerEmail, Subject, txtWriter.ToString());
        }
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            string StockItem = rowView["StockItem"].ToString();
            string StockItemID = rowView["StockItemID"].ToString();
            string CompanyLocation = rowView["CompanyLocation"].ToString();

            HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/reports/SrockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
            //            "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text;
            //

            StockDeducted.NavigateUrl = "~/admin/adminfiles/reports/SrockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out";
            StockDeducted.Target = "_blank";

            Revert.NavigateUrl = "~/admin/adminfiles/reports/SrockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert";
            Revert.Target = "_blank";

            Installed.NavigateUrl = "~/admin/adminfiles/reports/SrockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed";
            Installed.Target = "_blank";
        }
    }

    protected void lnkverify_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_UpdateIsverify_ForInstallerReport(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            BindGrid(0);
        }

    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        Export oExport = new Export();
        string FileName = "StockPendingReportItemWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9 };

        string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    protected void lnksubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        //chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        //chkDifference.Checked = false;
        //    }

        //}
        //if (i > Convert.ToInt32(hndDifference.Value))
        //{                    
        //    ModalPopupExtenderRevert.Show();
        //    SetError();
        //}
        //else
        //{
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            HiddenField rpthndProjectid = (HiddenField)item.FindControl("rpthndProjectid");
            HiddenField rpthndPicklistId = (HiddenField)item.FindControl("rpthndPicklistId");
            if (chkDifference.Checked == true)
            {

                if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                {
                    DataTable dt = null;
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                    if (!string.IsNullOrEmpty(txtprojectno.Text))
                    {
                        string[] ProjPickId = txtprojectno.Text.Split('/');
                        dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjPickId[0].ToString());
                        if (dt.Rows.Count > 0)
                        {
                            string projectid = dt.Rows[0]["ProjectID"].ToString();
                            //string PicklistId = dt.Rows[0]["ID"].ToString();
                            string PicklistId = ProjPickId[1].ToString();

                            string LocationId = dt.Rows[0]["LocationId"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                            section = "Stock Deduct";
                            Message = "Stock Deduct For Project No:" + ProjPickId[0].ToString() + "& PicklistId:" + PicklistId + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");

                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            //    ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}

                        }
                    }
                    if (!string.IsNullOrEmpty(TextBox1.Text))
                    {
                        dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                            section = "Stock Deduct";
                            Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value, dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "2", userid, date.ToString());
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}
                            //section = "Wholesale Revert";
                            //Message = "Stock Add For WholesaleId:" + rpthndWholesaleOrderId.Value + "By administrator";
                            //ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                        }
                    }
                    DataTable dtexist = ClstblProjects.tblStockSerialNo_Select_ByProjectIdandPicklistid(rpthndProjectid.Value, rpthndPicklistId.Value);
                    if (dtexist.Rows.Count == 0)
                    {
                        ClstblProjects.tbl_picklistlog_UpdateData(rpthndProjectid.Value, rpthndPicklistId.Value, "1", "", "");
                    }
                }
            }
        }
        BindGrid(0);


    }

    protected void chkDifference_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        //MsgError("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtenderRevert.Show();
    }

    protected void chkDifference1_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference1.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        Notification("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtender1.Show();
    }

    protected void btnsavenote_Click(object sender, EventArgs e)
    {
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_NoteUpdateIN(Convert.ToInt32(PicklistId), txtnotedate.Text, txtnotedesc.Text);
        }
        BindGrid(0);
    }

    protected void txtprojectno_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string[] ProjPickId = txtprojectno.Text.Split('/');
        //string ProjectNumber = txtprojectno.Text;
        string ProjectNumber = ProjPickId[0].ToString(); ;
        DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
        string locationid = dt.Rows[0]["LocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid.Value, Categorynm, ProjPickId[0].ToString());
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            //Notification("Record with this model number already exists.");

        }
        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }
        if (!string.IsNullOrEmpty(txtprojectno.Text))
        {
            TextBox1.Text = string.Empty;
            txtprojectno.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string InvoiceNumber = TextBox1.Text;
        DataTable dt1 = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(InvoiceNumber);
        string locationid = dt1.Rows[0]["CompanyLocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox1.Text = string.Empty;
        }
        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }

        if (!string.IsNullOrEmpty(TextBox1.Text))
        {
            txtprojectno.Text = string.Empty;
            TextBox1.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void chkisactive_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference");

            if (chkisactive.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupExtenderRevert.Show();
    }

    public void BindCheckbox()
    {
        //rptProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        //rptProjectStatus.DataBind();

        rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        rptAriseInstaller.DataBind();

        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();
    }

    public void BindCheckboxProjectTab()
    {
        //rptProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        //rptProjectStatus.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();

        rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        rptAriseInstaller.DataBind();

        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();
    }

    public void ClearCheckBox()
    {
        //foreach (RepeaterItem item in rptProjectStatus.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    chkselect.Checked = false;
        //}

        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void ddlIsDifference_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIsDifference.SelectedValue == "1")
        {
            ddlAudit.SelectedValue = "1";
            DivAudit.Visible = true;
        }
        else
        {
            DivAudit.Visible = false;
        }
    }

    protected void btnUpdateSMData_Click(object sender, EventArgs e)
    {
        int UpdateStatus = ClsDbData.USP_InsertUpdate_SM_tblProjectStatus();
        int UpdateProNo = ClsDbData.USP_InsertUpdate_SM_tblProjectNos();
    }
}