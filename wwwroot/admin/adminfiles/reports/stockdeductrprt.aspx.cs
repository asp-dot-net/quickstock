﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_adminfiles_reports_stockdeductrprt : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindCheckbox();
            HideShowCheckBox();

            BindDropDown();
            BindGrid(0);
        }

    }

    public void BindDropDown()
    {
        //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddllocationsearch.DataSource = dt;
        //ddllocationsearch.DataTextField = "location";
        //ddllocationsearch.DataValueField = "CompanyLocationID";
        //ddllocationsearch.DataBind();

        //try
        //{
        //    ddllocationsearch.SelectedValue = "1";
        //}
        //catch { }


        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch.DataSource = dtcategory;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();

        //DataTable dtEmployee = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        //ddlEmployee.DataSource = dtEmployee;
        //ddlEmployee.DataTextField = "fullname";
        //ddlEmployee.DataValueField = "EmployeeID";
        //ddlEmployee.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string selectedAInstallerID = "";
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnAriseInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedAInstallerID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (selectedAInstallerID != "")
        {
            selectedAInstallerID = selectedAInstallerID.Substring(1);
        }

        string selectedSMInstallerID = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnSMInstallerID = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedSMInstallerID += "," + hdnSMInstallerID.Value.ToString();
            }
        }
        if (selectedSMInstallerID != "")
        {
            selectedSMInstallerID = selectedSMInstallerID.Substring(1);
        }

        string selectedWCustomerID = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnWCustomerID = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWCustomerID += "," + hdnWCustomerID.Value.ToString();
            }
        }
        if (selectedWCustomerID != "")
        {
            selectedWCustomerID = selectedWCustomerID.Substring(1);
        }

        string selectedWEmployeeID = "";
        foreach (RepeaterItem item in rptWEmployee.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnWEmployeeID = (HiddenField)item.FindControl("hdnWEmployeeID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWEmployeeID += "," + hdnWEmployeeID.Value.ToString();
            }
        }
        if (selectedWEmployeeID != "")
        {
            selectedWEmployeeID = selectedWEmployeeID.Substring(1);
        }

        DataTable dt = null;
        
        if(ddlCompany.SelectedValue == "1") //Project Arise
        {
            dt = Reports.StockDeductReport_QuickStock_Project(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddlActive.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, "1", "1", ddlIsDeduct.SelectedValue, "", selectedAInstallerID, txtStockModel.Text);
        }
        else if (ddlCompany.SelectedValue == "2") // SolarMiner
        {
            dt = Reports.StockDeductReport_QuickStock_Project(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddlActive.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, "2", "1", ddlIsDeduct.SelectedValue, "", selectedSMInstallerID, txtStockModel.Text);
        }
        else if(ddlCompany.SelectedValue == "3") //Wholesale
        {
            dt = Reports.StockDeductReport_QuickStock_Project(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddlActive.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlCompany.SelectedValue, "2", ddlIsDeduct.SelectedValue, selectedWEmployeeID, selectedWCustomerID, txtStockModel.Text);
        }
        else  //All Data
        {
            dt = Reports.StockDeductReport_QuickStock_Project(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddlActive.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlCompany.SelectedValue, "3", ddlIsDeduct.SelectedValue, "", "", txtStockModel.Text);
        }

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
        bind(dt);
    }

    public void bind(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int Brisbane = Convert.ToInt32(dt.Compute("SUM(Brisbane)", string.Empty));
            int Melbourne = Convert.ToInt32(dt.Compute("SUM(Melbourne)", string.Empty));
            int Sydney = Convert.ToInt32(dt.Compute("SUM(Sydney)", string.Empty));
            int Perth = Convert.ToInt32(dt.Compute("SUM(Perth)", string.Empty));
            int Darwin = Convert.ToInt32(dt.Compute("SUM(Darwin)", string.Empty));
            int Hobart = Convert.ToInt32(dt.Compute("SUM(Hobart)", string.Empty));
            int Adelaide = Convert.ToInt32(dt.Compute("SUM(Adelaide)", string.Empty));
            int Canberra = Convert.ToInt32(dt.Compute("SUM(Canberra)", string.Empty));

            lblBrisbane.Text = Convert.ToString(Brisbane);
            lblMelbourne.Text = Convert.ToString(Melbourne);
            lblSydney.Text = Convert.ToString(Sydney);
            lblPerth.Text = Convert.ToString(Perth);
            lblDarwin.Text = Convert.ToString(Darwin);
            lblHobart.Text = Convert.ToString(Hobart);
            lblAdelaide.Text = Convert.ToString(Adelaide);
            lblCanberra.Text = Convert.ToString(Canberra);
            lblTotal.Text = Convert.ToString(Brisbane + Melbourne + Sydney + Perth + Darwin + Hobart + Adelaide + Canberra);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        //dt.Rows.RemoveAt(dt.Rows.Count - 1);
        
        
        Export oExport = new Export();
        string FileName = "StockDeductReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 10, 9, 11, 12 };
        string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Brisbane", "Melbourne", "Sydney", "Perth", "Darwin", "Hobart", "Adelaide", "Canberra", "Total" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtstockitemfilter.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "1";
        ddlcategorysearch.SelectedValue = "";
        ddlSalesTag.SelectedValue = "True";
        ddlActive.SelectedValue = "True";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlIsDeduct.SelectedValue = "1";
        ddlEmployee.SelectedValue = "";

        ClearCheckBox();

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string datetype = "";
        if (ddlDate.SelectedValue == "1")
        {
            datetype = "1";
        }
        else if (ddlDate.SelectedValue == "2")
        {
            datetype = "2";
        }
        else
        {
            datetype = "";
        }

        #region InstallerName
        string selectedAInstallerID = "";
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnAriseInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedAInstallerID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (selectedAInstallerID != "")
        {
            selectedAInstallerID = selectedAInstallerID.Substring(1);
        }

        string selectedSMInstallerID = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnSMInstallerID = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedSMInstallerID += "," + hdnSMInstallerID.Value.ToString();
            }
        }
        if (selectedSMInstallerID != "")
        {
            selectedSMInstallerID = selectedSMInstallerID.Substring(1);
        }

        string selectedWCustomerID = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnWCustomerID = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWCustomerID += "," + hdnWCustomerID.Value.ToString();
            }
        }
        if (selectedWCustomerID != "")
        {
            selectedWCustomerID = selectedWCustomerID.Substring(1);
        }
        string InstallerName = "";
        if (selectedAInstallerID != "")
        {
            InstallerName = selectedAInstallerID;
        }
        else if (selectedSMInstallerID != "")
        {
            InstallerName = selectedSMInstallerID;
        }
        else if (selectedWCustomerID != "")
        {
            InstallerName = selectedWCustomerID;
        }
        #endregion

        string[] arg = e.CommandArgument.ToString().Split(';');
        string StockItemID = arg[0];
        string StockItem = arg[1];

        if (e.CommandName.ToString() == "Detailbrisbane")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=QLD&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailMelbourne")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=VIC&CompanyId=" + ddlCompany.SelectedValue +"&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailSydney")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=NSW&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailPerth")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=WA&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailDarwin")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=NT&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailHobart")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=TAS&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailAdelaide")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=SA&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }
        if (e.CommandName.ToString() == "DetailCanberra")
        {
            Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=ACT&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        }

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    HiddenField hndStockitemID = (HiddenField)e.Row.FindControl("hndStockitemID");
        //    //HiddenField hdnCompanyLocationID = (HiddenField)e.Row.FindControl("hdnCompanyLocationID");
        //    HyperLink btnviewbrisbane = (HyperLink)e.Row.FindControl("btnviewbrisbane");
        //    HyperLink btnviewMelbourne = (HyperLink)e.Row.FindControl("btnviewMelbourne");
        //    HyperLink btnviewSydney = (HyperLink)e.Row.FindControl("btnviewSydney");
        //    HyperLink btnviewPerth = (HyperLink)e.Row.FindControl("btnviewPerth");
        //    HyperLink btnviewDarwin = (HyperLink)e.Row.FindControl("btnviewDarwin");
        //    HyperLink btnviewHobart = (HyperLink)e.Row.FindControl("btnviewHobart");
        //    HyperLink btnviewAdelaide = (HyperLink)e.Row.FindControl("btnviewAdelaide");
        //    HyperLink btnviewCanberra = (HyperLink)e.Row.FindControl("btnviewCanberra");
        //    string StockItem = DataBinder.Eval(e.Row.DataItem, "StockItem").ToString();

        //    string datetype = "";
        //    if(ddlDate.SelectedValue == "1")
        //    {
        //        datetype = "1";
        //    }
        //    else if (ddlDate.SelectedValue == "2")
        //    {
        //        datetype = "2";
        //    }
        //    else
        //    {
        //        datetype = "";
        //    }

        //    if (!string.IsNullOrEmpty(hndStockitemID.Value))
        //    {
        //        btnviewbrisbane.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=QLD&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType="+ datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewbrisbane.Target = "_blank";
        //btnviewMelbourne.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=VIC&CompanyId=" + ddlCompany.SelectedValue +
        //    "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //btnviewMelbourne.Target = "_blank";
        //        btnviewSydney.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=NSW&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewSydney.Target = "_blank";
        //        btnviewPerth.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=WA&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewPerth.Target = "_blank";
        //        btnviewDarwin.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=NT&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewDarwin.Target = "_blank";
        //        btnviewHobart.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=TAS&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewHobart.Target = "_blank";
        //        btnviewAdelaide.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=SA&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewAdelaide.Target = "_blank";
        //        btnviewCanberra.NavigateUrl = "~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=ACT&CompanyId=" + ddlCompany.SelectedValue +
        //            "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem;
        //        btnviewCanberra.Target = "_blank";
        //    }
        //}
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideShowCheckBox();
        ClearCheckBox();
        //BindCheckbox();
        BindGrid(0);
    }

    public void BindCheckbox()
    {
        rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        rptAriseInstaller.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();

        rptWEmployee.DataSource = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        rptWEmployee.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWEmployee.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void HideShowCheckBox()
    {
        if(ddlCompany.SelectedValue == "1") //AriseSolar
        {
            DivAriseInstaller.Visible = true;

            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;
            DivWEmployee.Visible = false;
        }
        else if(ddlCompany.SelectedValue == "2") //SolarMiner
        {
            DivSMInstaller.Visible = true;

            DivAriseInstaller.Visible = false;
            DivWCustomer.Visible = false;
            DivWEmployee.Visible = false;
        }
        else if (ddlCompany.SelectedValue == "3") //Wholesale
        {
            DivWCustomer.Visible = true;
            DivWEmployee.Visible = true;

            DivAriseInstaller.Visible = false;
            DivSMInstaller.Visible = false;
        }
        else
        {
            DivAriseInstaller.Visible = false;
            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;
            DivWEmployee.Visible = false;
        }
    }
}