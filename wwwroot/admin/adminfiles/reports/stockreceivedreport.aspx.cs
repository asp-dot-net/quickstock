using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockwisereport : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindCheckBox();
            BindDropDown();
            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    public string GetSelectedItems(Repeater rpt, string chk, string hnd)
    {
        string selectedItem = "";
        foreach (RepeaterItem item in rpt.Items)
        {
            //CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            //HiddenField hdnModule = (HiddenField)item.FindControl("hdnComapnyId");

            CheckBox chkselect = (CheckBox)item.FindControl(chk);
            HiddenField hdnModule = (HiddenField)item.FindControl(hnd);

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        return selectedItem;
    }

    public void BindHeader(DataTable dt)
    {
        string PurchaseCompany = GetSelectedItems(rptSearchCompany, "chkselect", "hdnComapnyId");
        if (dt.Rows.Count > 0)
        {
            HypAustrailia.Text = dt.Compute("SUM(AUS)", string.Empty).ToString();
            HypBrisbane.Text = dt.Compute("SUM(QLD)", string.Empty).ToString();
            HypMelbourne.Text = dt.Compute("SUM(VIC)", string.Empty).ToString();
            HypSydney.Text = dt.Compute("SUM(NSW)", string.Empty).ToString();
            HypAdelaide.Text = dt.Compute("SUM(SA)", string.Empty).ToString();
            HypPerth.Text = dt.Compute("SUM(WA)", string.Empty).ToString();
            lblTotal.Text = dt.Compute("SUM(Total)", string.Empty).ToString();

            HypAustrailia.Enabled = HypAustrailia.Text != "0" ? true : false;
            HypBrisbane.Enabled = HypBrisbane.Text != "0" ? true : false;
            HypMelbourne.Enabled = HypMelbourne.Text != "0" ? true : false;
            HypSydney.Enabled = HypSydney.Text != "0" ? true : false;
            HypAdelaide.Enabled = HypAdelaide.Text != "0" ? true : false;
            HypPerth.Enabled = HypPerth.Text != "0" ? true : false;

            HypAustrailia.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=8&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;

            HypBrisbane.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=1&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;

            HypMelbourne.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=2&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;

            HypSydney.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=3&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;

            HypAdelaide.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=7&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;

            HypPerth.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + "" + "&State=4&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + "&StockItemModel=" + txtStockItem.Text + "&StockCategory=" + ddlCategory.SelectedValue;
        }
    }

    protected DataTable GetGridData1()
    {
        string PurchaseCompany = GetSelectedItems(rptSearchCompany, "chkselect", "hdnComapnyId");
        //DataTable dt1 = Reports.StockReceivedReport_QuickStock(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, ddlActive.SelectedValue, ddlSalesTag.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, DropDownList1.SelectedValue,ddlpurchasecompany.SelectedValue);

        DataTable dt1 = ClsReportsV2.StockReceivedReportV2(txtStockItem.Text, ddlCategory.SelectedValue, ddlActive.SelectedValue, PurchaseCompany, ddlReceived.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlOrderType.SelectedValue);

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindHeader(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData1();
        
        Export oExport = new Export();
        string FileName = "StockReceivedReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Austrailia", "Brisbane", "Melbourne", "Sydney", "Adelaide", "Perth", "Total" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStockItem.Text = string.Empty;
        ddlCategory.SelectedValue = "";
        ddlActive.SelectedValue = "1";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlOrderType.SelectedValue = "";

        foreach (RepeaterItem item in rptSearchCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName.ToLower() == "viewpage1")
        //{
        //    string[] arg = new string[2];
        //    arg = e.CommandArgument.ToString().Split(';');
        //}
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            string StockitemID = dr["StockItemID"].ToString();
            string Stockitem = dr["StockItem"].ToString();
            string PurchaseCompany = GetSelectedItems(rptSearchCompany, "chkselect", "hdnComapnyId");

            HyperLink hypAUS = (HyperLink)e.Row.FindControl("hypAUS");
            HyperLink hypQLD = (HyperLink)e.Row.FindControl("hypQLD");
            HyperLink hypVIC = (HyperLink)e.Row.FindControl("hypVIC");
            HyperLink hypNSW = (HyperLink)e.Row.FindControl("hypNSW");
            HyperLink hypSA = (HyperLink)e.Row.FindControl("hypSA");
            HyperLink hypWA = (HyperLink)e.Row.FindControl("hypWA");

            if (StockitemID != "")
            {
                hypAUS.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=8&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";

                hypQLD.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=1&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";

                hypVIC.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=2&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";

                hypNSW.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=3&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";

                hypSA.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=7&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";

                hypWA.NavigateUrl = "~/admin/adminfiles/reports/stockdeductreportDetailsPage.aspx?StockItemID=" + StockitemID + "&State=4&Active=" + ddlActive.SelectedValue + "&Received=" + ddlReceived.SelectedValue + "&PurCompany=" + PurchaseCompany + "&DateType=" + ddlDate.SelectedValue + "&StartDate=" + txtStartDate.Text + "&EndDate=" + txtEndDate.Text + "&StockItem=" + Stockitem + "&StockItemModel=" + "" + "&StockCategory=" + "";
            }
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    public void BindCheckBox()
    {
        rptSearchCompany.DataSource = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        rptSearchCompany.DataBind();
    }
}