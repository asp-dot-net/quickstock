﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnUsedSerialNo.aspx.cs" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Inherits="admin_adminfiles_reports_UnUsedSerialNo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .req.form-control {
            border-color: #FF5F5F;
            /* background: rgb(249, 232, 232);*/
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <script>
        function ValidateTextBox(source, args) {
            //alert("sfsfsd");
            if (document.getElementById(source.controltovalidate).value != "") {
                //alert("true");
                // alert(document.getElementById(source.controltovalidate).value);
                $(document.getElementById(source.controltovalidate)).removeClass("req form-control");
                $(document.getElementById(source.controltovalidate)).addClass("form-control");
            } else {
                //alert("false");   
                $(document.getElementById(source.controltovalidate)).removeClass("form-control");
                $(document.getElementById(source.controltovalidate)).addClass("req form-control");
                $(document.getElementById(source.controltovalidate)).css("border-color", "#FF5F5F");

            }
        }


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
           <%-- $('form').on("click",'#<%=ibtnUploadPDF.ClientID %>', function () {
                //  $('form').on('submit', function () {
                //alert(this.selector);
                ShowProgress();
            });--%>
        });

    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <div class="page-header card">
                <div class="card-block">
                    <h5>Unused Serial Number
                        
                        
                    </h5>


                </div>
            </div>
            <link rel="stylesheet" type="text/css" href="<%=SiteURL %>admin/theme/assets/pages/notification/notification.css">
            <script src="<%=SiteURL %>admin/theme/assets/pages/notification/notification.js"></script>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    // alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

               <%--     function delayUpdateClick(sender, args) {
                        alert("22")
                        button = $get("<%= buttonUpdate.ClientID %>");
                        setTimeout(function () { button.click(); }, 5000);
                    }--%>

                    //alert("2");
                    document.getElementById('loader_div').style.visibility = "hidden";
                    //$('.loader_div').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });

                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $("[data-toggle=tooltip]").tooltip();

                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();


                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    <%--$('#<%=btnAdd.ClientID %>').click(function (e) {
                        //alert("1");
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();

                    });--%>
                    function toaster(msg) {
                        //alert("54345");
                        notifymsg(msg, 'inverse');
                    }

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });

                    $(".selectlocation .dropdown dt a").on('click', function () {
                        $(".selectlocation .dropdown dd ul").slideToggle('fast');

                    });
                }

            </script>
            <script type="text/javascript">
                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }
                }
            </script>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="searchfinal">
                            <div class="card shadownone brdrgray pad10">
                                <div class="card-block">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <div class="inlineblock martop5">
                                            <div class="row">
                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:TextBox ID="txtSearchStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStockItem"
                                                        WatermarkText="Stock Item" />
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:TextBox ID="txtSearchModel" runat="server" placeholder="Stock Model" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchModel"
                                                        WatermarkText="Stock Model" />
                                                </div>
                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Category</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Location</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddLocation" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptLocation" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <%--<asp:ListItem Value="">Stock Active</asp:ListItem>--%>
                                                        <asp:ListItem Value="True">Only Active</asp:ListItem>
                                                        <asp:ListItem Value="False">Not Active</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group  sandbox-container">
                                                        <asp:TextBox ID="txtExpiryDate" placeholder="Expiry Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group martop5 col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />
                                                </div>
                                                <div class="input-group martop5 col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <div class="datashowbox inlineblock">
                                        <div class="row">
                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" 
                                                    OnClick="lbtnExport_Click" CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="PanTotal" runat="server">
                    <div class="page-header card" id="divtot" runat="server">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Total Unused Serial No:
                                    <asp:Literal ID="lblTotal" runat="server"></asp:Literal></b>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                            AllowSorting="true" OnRowDataBound="GridView1_RowDataBound"
                                            OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                            <Columns>
                                                <%--<asp:TemplateField HeaderText="ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockItemID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label31" runat="server" Width="70px">
                                                                <%#Eval("StockItemID")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Stock Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockItem">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Width="170px">
                                                                <%#Eval("StockItem")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stock Manufacturer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockManufacturer">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("StockManufacturer")%>' data-toggle="tooltip"
                                                            Width="150px"><%#Eval("StockManufacturer")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Model" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockModel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label7" runat="server" Width="150px" data-placement="top" data-original-title='<%#Eval("StockModel")%>' data-toggle="tooltip">
                                                                <%#Eval("StockModel")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Series" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockSeries">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label8" runat="server" Width="70px">
                                                                <%#Eval("StockSeries")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Size" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="StockSeries" HeaderStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSize" runat="server" Width="50px">
                                                                <%#Eval("StockSize")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Width="70px">
                                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Live Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" SortExpression="StockQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLiveQty" runat="server" Width="60px" Text='<%#Eval("StockQuantity")%>'> 
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Unused Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" SortExpression="CountUnudedSerialNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label11" runat="server" Width="60px" Text='<%#Eval("CountUnudedSerialNo")%>'> 
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="30px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="btnserial" CommandName="SerialNo" CommandArgument='<%#Eval("StockItemID") +";"+ Eval("CompanyLocationID")%>' CssClass="btn btn-success btn-mini "
                                                            data-toggle="tooltip" data-placement="top" title="Item History" data-original-title="Edit">
                                                                         <i class="fa fa-eye"></i> View
                                                        </asp:HyperLink>
                                                        <asp:HiddenField runat="server" ID="hdnCompanyLocationID" Value='<%#Eval("CompanyLocationID")%>' />
                                                        <asp:HiddenField ID="hdnStockitemID2" runat="server" Value='<%#Eval("StockItemID") %>' />

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" CssClass="verticaaline" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
