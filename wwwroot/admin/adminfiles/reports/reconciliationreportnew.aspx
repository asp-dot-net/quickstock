<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="reconciliationreportnew.aspx.cs" Inherits="admin_adminfiles_reports_reconciliationreportnew" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 999999 !important;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            $('.modal_popup').css('z-index', '901');
            document.getElementById('loader_div').style.visibility = "visible";
            // $('.modal_popup').css('z-index', '901');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".selectlocation .dropdown dt a").on('click', function () {
                $(".selectlocation .dropdown dd ul").slideToggle('fast');
            });
            $(".selectlocation .dropdown dd ul li a").on('click', function () {
                $(".selectlocation .dropdown dd ul").hide();
            });

            $(".selectlocation2 .dropdown dt a").on('click', function () {
                $(".selectlocation2 .dropdown dd ul").slideToggle('fast');
            });
            $(".selectlocation2 .dropdown dd ul li a").on('click', function () {
                $(".selectlocation2 .dropdown dd ul").hide();
            });

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            // alert("dgfdg3");

            $('.modal_popup').css('z-index', '90001');
            document.getElementById('loader_div').style.visibility = "hidden";
            //  $('.modal_popup').css('z-index', '90001');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();
            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox2();
            });
        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <script>
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddSearchLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
                console.log(html);
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function callMultiCheckbox2() {
            var title = "";
            $("#<%=ddSearchLocation2.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
                console.log(html);
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Reconciliation Report
                    </h5>
                </div>
            </div>
            <%--change by--%>
            <div class="col-md-12" id="divright" runat="server">
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                    <cc1:TabPanel ID="TabProjectNo" runat="server" HeaderText="Project">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel4">
                                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">

                                                                    <%--      <div class="input-group col-sm-2 martop5 max_width170" id="div3" runat="server">
                                            <asp:DropDownList ID="ddlDeductedOrNot" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Not Deducted</asp:ListItem>
                                                <asp:ListItem Value="True">Deducted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                                                    <%----%>
                                                                    <%--                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                                    --%>

                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No/Pick Id." ToolTip="Project No/Pick Id."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                                            WatermarkText="Project No." />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers, Custom"
                                                                            ValidChars="," />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtserailno"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select dnone">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida" id="spanselect">Location</span>
                                                                                    <p class="multiSel"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddSearchLocation" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptSearchLocation" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                            <asp:ListItem Value="2">InstallBooked</asp:ListItem>
                                                                            <asp:ListItem Value="3">Revert Date</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>

                            <div class="finalgrid">
                                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                                    <div>
                                        <div id="PanGrid" runat="server">
                                            <div class="card shadownone brdrgray">
                                                <div class="card-block">
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField runat="server" ID="hdnpicklistid2" />
                                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")+"/"+Eval("ID")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Proj Status" SortExpression="ProjectStatus">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label434" runat="server" Width="60px">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Installer" SortExpression="InstallerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label44" runat="server" Width="60px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Install Booked" SortExpression="InstallBookedDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label424" runat="server" Width="60px">
                                                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label47" runat="server" Width="30px">
                                                                                        <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRevertdate1" runat="server" Width="80px">
                                                                                        <%#Eval("RevertDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%-- <asp:TemplateField HeaderText="Installation Completed" SortExpression="InstallCompleted">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label67" runat="server" Width="20px">
                                                                                        <%#Eval("InstallCompleted","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label94" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Out" SortExpression="PanelStockDeducted">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label452" runat="server" Width="30px">
                                                                                       <%#Eval("PanelStockDeducted")%> </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Installed" SortExpression="PanelInstalled">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label52" runat="server" Width="30px">
                                                                                        <%#Eval("PanelInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Reverted" SortExpression="PanelRevert">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label82" runat="server" Width="30px" Text='<%#Eval("PanelRevert")%>'>
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="btnviewrevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Panel Reverted
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Out" SortExpression="InverterStockDeducted">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4522" runat="server" Width="30px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Installed" SortExpression="InverterInstalled">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label752" runat="server" Width="30px">
                                                                                        <%#Eval("InverterInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Reverted" SortExpression="InverterRevert">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label152" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("InverterRevert"))%>'>
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="btnviewrevert2" runat="server" Style="float: right;"
                                                                            CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Inverter Reverted
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage1" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>

                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopage">
                                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>

                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabWholesale" runat="server" HeaderText="Wholesale">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel7">
                                    <asp:UpdatePanel ID="updatepanel3" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div6" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div7" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label2" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div8" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div9" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel8" runat="server" DefaultButton="btnSearch3">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control m-b" placeholder="Wholesale Id/Invoice No/Stc Id" ToolTip="Wholesale Id/Invoice No/Stc Id"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtInvoiceNo"
                                                                            WatermarkText="Invoice No." />
                                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers, Custom" ValidChars="," />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlstatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                            AppendDataBoundItems="true">
                                                                            <asp:ListItem Value="">Status</asp:ListItem>
                                                                            <asp:ListItem Value="1">Pending</asp:ListItem>
                                                                            <asp:ListItem Value="2">JobBooked</asp:ListItem>
                                                                            <asp:ListItem Value="3">Void</asp:ListItem>
                                                                            <asp:ListItem Value="4">PVD Applied</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter3" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtstockitemfilter3"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170" id="div10" runat="server">
                                                                        <asp:DropDownList ID="ddllocationsearch3" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="form-group spical multiselect selectlocation2 martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida" id="spanselect">Location</span>
                                                                                    <p class="multiSel"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddSearchLocation2" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptSearchLocation2" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtserailno3" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtserailno3"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddldate3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtstartdate3" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtenddate3" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch3" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch3_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll3" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll3_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords3" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged3"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport3" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport3_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport3" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="finalgrid">
                                        <asp:Panel ID="panel9" runat="server" CssClass="xsroll">
                                            <div>
                                                <div id="PanGrid3" runat="server">
                                                    <div class="card shadownone brdrgray">
                                                        <div class="card-block">
                                                            <div class="table-responsive BlockStructure">
                                                                <asp:GridView ID="GridView3" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                                    OnSorting="GridView3_Sorting" OnPageIndexChanging="GridView3_PageIndexChanging" OnRowCommand="GridView3_RowCommand"
                                                                    OnDataBound="GridView3_DataBound" AllowSorting="true" OnRowCreated="GridView3_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Order No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="WholesaleOrderID" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hndWholesaleorderID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                <asp:Label ID="Label61" runat="server" Width="30px">
                                                                                        <%#Eval("WholesaleOrderID")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="InvoiceNo" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label70" runat="server" Width="30px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Stc Id." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="Stcid" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStcid" runat="server" Width="30px">
                                                                                        <%#Eval("Stcid")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vendor" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label71" runat="server" Width="100px">
                                                                                        <%#Eval("Vendor")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="Status" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6689" runat="server" Width="80px">
                                                                                        <%#Eval("Status")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label67" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Deducted On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbldeducton" runat="server" Width="80px">
                                                                                        <%#Eval("StockDeductDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRevertdate" runat="server" Width="80px">
                                                                                        <%#Eval("RevertDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Panel Out" SortExpression="PanelStockDeducted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label4521" runat="server" Width="30px">
                                                                                        <%#Eval("SaleQtyPanel")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Panel Installed" SortExpression="SaleQtyPanel">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label522" runat="server" Width="30px">
                                                                                        <%#Eval("PanelStockDeducted")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Panel Reverted" SortExpression="PanelStockReverted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label823" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("PanelStockReverted"))%>'>
                                                                                </asp:Label>
                                                                                <asp:LinkButton ID="btnviewwholesalerevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Panel Reverted
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Inverter Out" SortExpression="InverterStockDeducted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label45224" runat="server" Width="30px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Inverter Installed" SortExpression="SaleQtyInverter">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label7525" runat="server" Width="30px">
                                                                                        <%#Eval("SaleQtyInverter")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Inverter Reverted" SortExpression="InverterStockReverted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1562" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("InverterStockReverted"))%>'>
                                                                                </asp:Label>
                                                                                <asp:LinkButton ID="btnviewwholesalerevert2" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Inverter Reverted
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="gvbtnView3" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage3" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle />
                                                                    <PagerTemplate>
                                                                        <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        <div class="pagination">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                        </div>
                                                                    </PagerTemplate>
                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                </asp:GridView>

                                                            </div>
                                                            <div class="paginationnew1" runat="server" id="divnopage3">
                                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage3" style="width: 100%; border-collapse: collapse;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabSMProject" runat="server" HeaderText="Soler Miner Project">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel2">
                                    <asp:UpdatePanel ID="updatepanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div1" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div2" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label4" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div3" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div4" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="lnkSMSearch">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">

                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtSMProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No/Pick Id." ToolTip="Project No/Pick Id."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtSMProjectNumber"
                                                                            WatermarkText="Project No." />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSMProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtSMProjectNumber" FilterType="Numbers, Custom"
                                                                            ValidChars="," />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtSMserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSMserailno"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtSMstockitemfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtSMstockitemfilter"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlSMLocation" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select" style="display:none">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida" id="spanselect">Location</span>
                                                                                    <p class="multiSel"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="dd1" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="Repeater1" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlSMInstaller" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlSMDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                            <asp:ListItem Value="2">InstallBooked</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtSMStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtSMEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="lnkSMSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="lnkSMSearch_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSMClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnSMClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSMSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSMSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lSMbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lSMbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lSMbtnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>

                            <div class="finalgrid">
                                <asp:Panel ID="panel5" runat="server" CssClass="xsroll">
                                    <div>
                                        <div id="PanGridSM" runat="server">
                                            <div class="card shadownone brdrgray">
                                                <div class="card-block">
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridView_SM" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                            OnSorting="GridView_SM_Sorting" OnPageIndexChanging="GridView_SM_PageIndexChanging" OnRowCommand="GridView_SM_RowCommand"
                                                            OnDataBound="GridView_SM_DataBound" AllowSorting="True" OnRowCreated="GridView_SM_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField runat="server" ID="hdnpicklistid2" />
                                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                        
                                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")+"/"+Eval("ID")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Proj Status" SortExpression="ProjectStatus">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label434" runat="server" Width="60px">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Installer" SortExpression="InstallerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label44" runat="server" Width="60px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Install Booked" SortExpression="InstallBookingDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label424" runat="server" Width="60px">
                                                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label47" runat="server" Width="30px">
                                                                                        <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Left" SortExpression="RevertDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRevertdate1" runat="server" Width="80px">
                                                                                        <%#Eval("RevertDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label94" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Out" SortExpression="PanelStockDeducted">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label452" runat="server" Width="30px">
                                                                                       <%#Eval("PanelStockDeducted")%> </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Installed" SortExpression="PanelInstalled">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label52" runat="server" Width="30px">
                                                                                        <%#Eval("PanelInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Panel Reverted" SortExpression="PanelRevert">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label82" runat="server" Width="30px" Text='<%# Eval("PanelRevert")%>'>
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="btnviewrevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Panel Reverted
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Out" SortExpression="InverterStockDeducted">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4522" runat="server" Width="30px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Installed" SortExpression="InverterInstalled">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label752" runat="server" Width="30px">
                                                                                        <%#Eval("InverterInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Inverter Reverted" SortExpression="InverterRevert">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label152" runat="server" Width="30px" Text='<%# Eval("InverterRevert")%>'>
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="btnviewrevert2" runat="server" Style="float: right;"
                                                                            CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Inverter Reverted
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage1" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>

                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divSMnopage">
                                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrSMPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>

                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </div>
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="10%" align="center">Index No.</th>
                                                    <th width="25%" align="center">Serial No.</th>
                                                    <th width="15%" align="left">Category</th>
                                                    <th align="50%">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialNo") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupRevertedItem" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divreverteditems" TargetControlID="btnNULL2"
                CancelControlID="LinkButton6">
            </cc1:ModalPopupExtender>
            <div id="divreverteditems" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 1050px!important;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel2">Reverted Stock Item
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="5%" align="center">Index No.</th>
                                                    <th width="10%" align="center">Reverted Through</th>
                                                    <th width="20%" align="center">Reverted Serial No.</th>
                                                    <th width="10" align="center">Reverted On</th>
                                                    <th width="10%" align="center">Reverted By</th>
                                                    <th align="30%">Stock Item</th>
                                                    <th width="10%" align="left">Category</th>
                                                    <th width="5%" align="left">
                                                        <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                            <label for="<%=chkisactive.ClientID %>" style="margin: 0;">
                                                                <asp:CheckBox ID="chkisactive" runat="server" AutoPostBack="true" OnCheckedChanged="chkisactive_CheckedChanged" />
                                                                <span class="cr" style="margin: 0 15px;"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <asp:HiddenField runat="server" ID="hndDifference" />
                                                <asp:Repeater ID="rptReverteditems" runat="server" OnItemDataBound="rptReverteditems_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label3" runat="server"><%#Eval("DirectLogIn").ToString()=="1"?"Direct Scanned":"Proj Scanned" %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo2" runat="server" Text='<%#Eval("SerialNo") %>'></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label1" runat="server"><%#Eval("RevertedDate","{0:dd MMM yyyy}") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label2" runat="server"><%#Eval("StockRevertByName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem2" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory2" runat="server" Text='<%#Eval("CategoryName") %>'><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td style="text-align: center;">
                                                                <div class="right-text checkbox-info checkbox martop7">
                                                                    <div class="thkmartop">
                                                                        <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                                            <label for="<%# Container.FindControl("chksalestagrep").ClientID %>" style="margin: 0;">
                                                                                <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                                <span class="cr" style="margin: 0 15px;"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                                <span class="text">&nbsp;</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:HiddenField runat="server" ID="hdnPicklistid" Value='<%#Eval("Picklistid") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnDirectLogIn" Value='<%#Eval("DirectLogIn") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnSerialNo" Value='<%#Eval("SerialNo") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                                <%--<asp:HiddenField runat="server" ID="hdnStockLocationID" Value='<%#Eval("StockLocationID") %>' />--%>
                                                                <asp:HiddenField runat="server" ID="hdnProjectID" Value='<%#Eval("ProjectID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnSRid" Value='<%#Eval("SRid") %>' />
                                                                <asp:HiddenField runat="server" ID="hndInstaller" Value='<%#Eval("InstallerId") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnStockLocationID" Value='<%#Eval("StockLocationID")%>' />
                                                                <asp:HiddenField runat="server" ID="hnditemid" Value='<%#Eval("StockItemID")%>' />
                                                                <asp:HiddenField runat="server" ID="hndProjectnumber" Value='<%#Eval("Projectnumber")%>' />

                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <table class="margin20">
                                                <tr>
                                                    <td style="padding-right: 5px;">Project Number</td>
                                                    <td>
                                                        <asp:TextBox ID="txtprojectno" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Project No." OnTextChanged="txtprojectno_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtprojectno"
                                                            WatermarkText="Project No." />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtprojectno" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectPickNoByProjectNo"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtprojectno" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/" />
                                                    </td>
                                                    <td></td>
                                                    <td style="padding-right: 5px;">Invoice Number</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control m-b" placeholder="Invoice No." OnTextChanged="TextBox1_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="TextBox1"
                                                            WatermarkText="Invoice No." />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="TextBox1" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetWholesaleID"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="TextBox1" FilterType="Numbers" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div style="display: flex; justify-content: center;">
                                                <asp:Button ID="btnconfirmRevert" runat="server" type="button" class="btn btn-danger POPupLoader" Style="justify-content: center;" data-dismiss="modal" Text="Revert" PostBackUrl="~/admin/adminfiles/reports/reconciliationreportnew.aspx" OnClick="btnconfirmRevert_Click"></asp:Button>
                                                &nbsp;&nbsp;&nbsp;   
                                                <asp:LinkButton ID="lnksubmit" runat="server" Text="Transfer" CssClass="btn btn-info" CausesValidation="false" OnClick="lnksubmit_Click"></asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;   
                                                <asp:HiddenField runat="server" ID="hndStockCategory" />
                                                <asp:LinkButton ID="lnkSwipe" runat="server" Text="Swipe" CssClass="btn btn-info" CausesValidation="false" OnClick="lnkSwipe_Click"></asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btnprojpocket" runat="server" type="button" class="btn btn-danger POPupLoader" Style="justify-content: center; display: none;" data-dismiss="modal" Text="Pocket" PostBackUrl="~/admin/adminfiles/reports/reconciliationreportnew.aspx" OnClick="btnprojpocket_Click"></asp:Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <%--  <div class="modal-footer" style="justify-content: center;">
                           
                           <asp:Button ID="btnCancel2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>--%>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL2" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupRevertedItemWholesale" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divreverteditemwholesale" TargetControlID="Button2"
                CancelControlID="LinkButton7">
            </cc1:ModalPopupExtender>
            <div id="divreverteditemwholesale" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 1050px!important;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel3">Reverted Stock Item
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="5%" align="center">Index No.</th>
                                                    <th width="10%" align="center">Reverted Through</th>
                                                    <th width="20%" align="center">Reverted Serial No.</th>
                                                    <th width="10" align="center">Reverted On</th>
                                                    <th width="10%" align="center">Reverted By</th>
                                                    <th align="30%">Stock Item</th>
                                                    <th width="10%" align="left">Category</th>
                                                    <th width="5%" align="left">
                                                        <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                            <label for="<%=CheckBox1.ClientID %>" style="margin: 0;">
                                                                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                                                <span class="cr" style="margin: 0 15px;"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <asp:HiddenField runat="server" ID="hndDifference1" />
                                                <asp:Repeater ID="rptWholesaleReverteditems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label3" runat="server"><%#Eval("DirectLogIn").ToString()=="1"?"Direct Scanned":"Proj Scanned" %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo2" runat="server" Text='<%#Eval("SerialNo") %>'></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label1" runat="server"><%#Eval("RevertedDate","{0:dd MMM yyyy}") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="Label2" runat="server"><%#Eval("StockRevertByName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem2" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory2" runat="server" Text='<%#Eval("CategoryName") %>'></asp:Label></td>
                                                            <td style="text-align: center;">
                                                                <div class="right-text checkbox-info checkbox martop7">
                                                                    <div class="thkmartop">
                                                                        <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                                            <label for="<%# Container.FindControl("chksalestagrep").ClientID %>" style="margin: 0;">
                                                                                <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                                <span class="cr" style="margin: 0 15px;"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                                <span class="text">&nbsp;</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:HiddenField runat="server" ID="hndInvoiceNo" Value='<%#Eval("InvoiceNo")%>' />
                                                                <asp:HiddenField runat="server" ID="hdnDirectLogIn" Value='<%#Eval("DirectLogIn") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnSerialNo" Value='<%#Eval("SerialNo") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnStockLocationID" Value='<%#Eval("StockLocationID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnWholesaleOrderID" Value='<%#Eval("WholesaleOrderID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnSRid" Value='<%#Eval("SRid") %>' />

                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <table class="margin20">
                                                <tr>
                                                    <td style="padding-right: 5px;">Project Number</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Project No." OnTextChanged="TextBox2_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="TextBox2"
                                                            WatermarkText="Project No." />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="TextBox2" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GEtProjectNumber"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="TextBox2" FilterType="Numbers" />
                                                    </td>
                                                    <td></td>
                                                    <td style="padding-right: 5px;">Invoice Number</td>
                                                    <td>
                                                        <asp:TextBox ID="txtwholesaleprojNo" runat="server" CssClass="form-control m-b" placeholder="Invoice No." OnTextChanged="txtwholesaleprojNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtwholesaleprojNo"
                                                            WatermarkText="Invoice No." />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtwholesaleprojNo" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetWholesaleID"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtwholesaleprojNo" FilterType="Numbers" />
                                                    </td>

                                                </tr>
                                            </table>
                                            <br />
                                            <div style="display: flex; justify-content: center;">
                                                <asp:Button ID="btnconfirmWholesaleRevert" runat="server" type="button" class="btn btn-danger POPupLoader" Style="justify-content: center;" data-dismiss="modal" Text="Confirm Revert" PostBackUrl="~/admin/adminfiles/reports/reconciliationreportnew.aspx" OnClick="btnconfirmWholesaleRevert_Click"></asp:Button>
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkwholeSaleSubmit" runat="server" Text="Submit" CssClass="btn btn-info POPupLoader"
                                                    CausesValidation="false" OnClick="lnkwholeSaleSubmit_Click"></asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:HiddenField runat="server" Id="hndStockCategoryW" />
                                                <asp:LinkButton ID="lnkWholesaleSwipe" runat="server" Text="Swipe" CssClass="btn btn-info POPupLoader"
                                                    CausesValidation="false" OnClick="lnkWholesaleSwipe_Click"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <%--  <div class="modal-footer" style="justify-content: center;">
                           
                           <asp:Button ID="btnCancel2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>--%>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button2" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderConfirmRevert" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divRevertScannedItem" TargetControlID="Button8">
            </cc1:ModalPopupExtender>
            <div id="divRevertScannedItem" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="HiddenField2" runat="server" />

                <div class="modal-dialog" style="width: 340px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Confirmation of Reverted Items</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <%-- <asp:HiddenField ID="hndPicklistid2" runat="server"  />--%>
                                    <asp:Label runat="server" ID="Label24"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnOK4" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnOK4_Click"></asp:Button>
                            <asp:Button ID="btnOK5" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnOK5_Click"></asp:Button>
                            <asp:Button ID="btnCancel3" PostBackUrl="~/admin/adminfiles/reports/reconciliationreportnew.aspx" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel" OnClick="btnCancel3_Click"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button8" Style="display: none;" runat="server" />

        </ContentTemplate>
        <Triggers>
            <%--      <asp:PostBackTrigger ControlID="btnviewwholesalerevert1" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
