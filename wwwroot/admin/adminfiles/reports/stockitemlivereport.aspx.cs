using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockitemlivereport : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            

            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();


            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                BindDropDown();
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                ddllocationsearch.SelectedValue = CompanyLocationID;
                ddllocationsearch.Enabled = false;
                BindGrid(0);
            }
            else
            {
                BindDropDown();
                BindGrid(0);

            }

        }

    }

    public void BindDropDown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataSource = dt;
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataBind();
        ddllocationsearch.Items.Insert(0, new ListItem("All", "0"));
        try
        {
            ddllocationsearch.SelectedValue = "1";
        }
        catch { }


        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch.DataSource = dtcategory;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        //DataTable dt1 = Reports.StockitemliveReport_QuickStock(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, ddlActive.SelectedValue, ddlScanned.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        DataTable dt1 = Reports.Get_StockItemLiveReport(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, ddlActive.SelectedValue, ddlScanned.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
        bind();
    }

    public void bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockItemID"].ToString()))
            {
                SetNoRecords();
                PanGrid.Visible = false;
                divnopage.Visible = false;
            }
        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        DataTable dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockitemLiveReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            //int[] ColList = { 2, 4, 6, 42, 37, 53, 55, 54, 38, 39, 46, 50, 48, 56, 44, 52, 57 };
            int[] ColList = { 2, 4, 6, 12, 38, 52, 54, 53, 41, 40, 45, 49, 47, 43, 51 };
            string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Location", "Current Qty", "Stock Order", "Stock Sold", "W.Order", "Opening Qty", "Stock In", "Projects", "Transfers", "Wholesale", "P.Revert", "T.In" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtstockitemfilter.Text = string.Empty;
        ddllocationsearch.SelectedValue = "1";
        ddlcategorysearch.SelectedValue = "";
        ddlScanned.SelectedValue = "";
        ddlActive.SelectedValue = "True";
        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
        txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {

            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch.SelectedValue = CompanyLocationID;
            ddllocationsearch.Enabled = false;

        }

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }

        if (e.CommandName.ToLower() == "stockordered")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];

            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_stockorderquantity.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }
        }
        if (e.CommandName.ToLower() == "stocksold")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string StockItemID = arg[0];
                string StockLocation = arg[1];

                if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
                {
                    Response.Redirect("stockwisereport_Stocksold.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
                }
            }
            catch { }
        }
        if (e.CommandName.ToLower() == "wholesaleorder")
        {

            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];
            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_WholesaleOrder.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }
        }
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndStockitemID = (HiddenField)e.Row.FindControl("hndStockitemID");
            //HiddenField hdnCompanyLocationID = (HiddenField)e.Row.FindControl("hdnCompanyLocationID");
            HyperLink btnprojdeduct = (HyperLink)e.Row.FindControl("btnprojdeduct");
            HyperLink btntransferdeduct = (HyperLink)e.Row.FindControl("btntransferdeduct");
            HyperLink btnwholesalededuct = (HyperLink)e.Row.FindControl("btnwholesalededuct");
            HyperLink btnProjnotdeduct = (HyperLink)e.Row.FindControl("btnProjnotdeduct");
            if (!string.IsNullOrEmpty(hndStockitemID.Value))
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(hndStockitemID.Value);

                btnprojdeduct.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockitemlivereportdetailprojdeduct.aspx?mode=deduct" + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + ddllocationsearch.SelectedValue;
                btnprojdeduct.Target = "_blank";
                btntransferdeduct.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockitemlivereportdetailtnsfdeduct.aspx?StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + ddllocationsearch.SelectedValue;
                btntransferdeduct.Target = "_blank";
                btnwholesalededuct.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockitemlivereportdetailwholesalededuct.aspx?StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + ddllocationsearch.SelectedValue;
                btnwholesalededuct.Target = "_blank";
                btnProjnotdeduct.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockitemlivereportdetailprojdeduct.aspx?mode=scan" + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + ddllocationsearch.SelectedValue;
                btnProjnotdeduct.Target = "_blank";
            }
            else
            {
                //DataTable dt = new DataTable();
                //dt = GetGridData1();
                //int totStockSize = 0;
                //int totStockQuantity = 0;
                //int totProjDeduct = 0;
                //int totTransferDeduct = 0;
                //int totWholesaleDeduct = 0;
                //int totProjNotDeduct = 0;
                //int StockSize = 0;
                //int StockQuantity = 0;
                //int ProjDeduct = 0;
                //int TransferDeduct = 0;
                //int WholesaleDeduct = 0;
                //int ProjNotDeduct = 0;


                //Label ProjNotDeduct1 = (Label)e.Row.FindControl("Label4522");
                //Label WholesaleDeduct1 = (Label)e.Row.FindControl("Label82");
                //Label TransferDeduct1 = (Label)e.Row.FindControl("Label52");
                //Label ProjDeduct1 = (Label)e.Row.FindControl("Label452");
                //Label OpeningQty = (Label)e.Row.FindControl("Label941");
                //Label CurrentQty = (Label)e.Row.FindControl("Label94");
                //Label StockSize1 = (Label)e.Row.FindControl("Label47");
                //Label ClosingQty = (Label)e.Row.FindControl("Label45221");


                //for (int i = 0; i < dt.Rows.Count; i++)
                //{

                //    StockSize = Convert.ToInt32(dt.Rows[i]["StockSize"]);
                //    StockQuantity = Convert.ToInt32(dt.Rows[i]["StockQuantity"]);
                //    ProjDeduct = Convert.ToInt32(dt.Rows[i]["ProjDeduct"]);
                //    TransferDeduct = Convert.ToInt32(dt.Rows[i]["TransferDeduct"]);
                //    WholesaleDeduct = Convert.ToInt32(dt.Rows[i]["WholesaleDeduct"]);
                //    ProjNotDeduct = Convert.ToInt32(dt.Rows[i]["ProjNotDeduct"]);

                //    totStockSize += StockSize;
                //    totStockQuantity += StockQuantity;
                //    totProjDeduct += ProjDeduct;
                //    totTransferDeduct += TransferDeduct;
                //    totWholesaleDeduct += WholesaleDeduct;
                //    totProjNotDeduct += ProjNotDeduct;
                //}
                //ProjNotDeduct1.Text = totProjNotDeduct.ToString();
                //WholesaleDeduct1.Text = totWholesaleDeduct.ToString();
                //TransferDeduct1.Text = totTransferDeduct.ToString();
                //ProjDeduct1.Text = totProjDeduct.ToString();
                //OpeningQty.Text = totStockQuantity.ToString();
                //CurrentQty.Text = totStockQuantity.ToString();
                //StockSize1.Text = totStockSize.ToString();
                //ClosingQty.Text = (totStockQuantity - totProjDeduct - totTransferDeduct - totWholesaleDeduct - totProjNotDeduct).ToString();

                btnProjnotdeduct.Visible = false;
                btnwholesalededuct.Visible = false;
                btntransferdeduct.Visible = false;
                btnprojdeduct.Visible = false;
            }

            //=====================Changes on 16-03-2020=================================
            //HiddenField hdnCompanyLocationID = (HiddenField)e.Row.FindControl("hdnCompanyLocationID");
            //HyperLink lblSMStockSold = (HyperLink)e.Row.FindControl("lblSMStockSold");
            //HyperLink lblWDraft = (HyperLink)e.Row.FindControl("lblWDraft");
            //HyperLink lblWInvoice = (HyperLink)e.Row.FindControl("lblWInvoice");
            //HyperLink lblAPicklist = (HyperLink)e.Row.FindControl("lblAPicklist");
            //HyperLink lblSMPicklist = (HyperLink)e.Row.FindControl("lblSMPicklist");


            //if (!string.IsNullOrEmpty(hndStockitemID.Value))
            //{
                //DataTable dt1 = ClstblStockItems.tblStockItems_SelectBy_FixedItemId(hndStockitemID.Value);
                //if (dt1.Rows.Count > 0)
                //{
                //    string Id1 = dt1.Rows[0]["StockItemID"].ToString();
                //    SttblProjects obj = ClstblProjects.tblProjects_SelectSolarMinerStockSoldbyStockItemId(Id1, hdnCompanyLocationID.Value);
                //    lblSMStockSold.Text = obj.SMStockSold;
                //}
                //else
                //{
                //    lblSMStockSold.Text = "0";
                //}

                //DataTable dtWholesale = ClstblProjects.tbl_WholesaleOrders_GetStockItemWiseDraftInvoiceCountLive(hndStockitemID.Value, hdnCompanyLocationID.Value);
                //if (dtWholesale.Rows.Count > 0)
                //{
                //    lblWDraft.Text = dtWholesale.Rows[0]["WholesaleDraft"].ToString();
                //    lblWInvoice.Text = dtWholesale.Rows[0]["WholesaleInvoice"].ToString();
                //}
                //else
                //{
                //    lblWDraft.Text = "0";
                //    lblWInvoice.Text = "0";
                //}



                //if (hndStockitemID.Value == "30")
                //{
                //    SttblProjects objArisePicklistCount1 = ClstblProjects.tblProjects_GetItemCountStockitemwiseWise("1", hndStockitemID.Value, hdnCompanyLocationID.Value);
                //    lblAPicklist.Text = objArisePicklistCount1.ArisePickListCount;
                //}


                //SttblProjects objArisePicklistCount = ClstblProjects.tblProjects_GetItemCountStockitemwiseWise("1", hndStockitemID.Value, hdnCompanyLocationID.Value);
                //lblAPicklist.Text = objArisePicklistCount.ArisePickListCount;

                //SttblProjects objSolarMinerlistCount = ClstblProjects.tblProjects_GetItemCountStockitemwiseWise("2", hndStockitemID.Value, hdnCompanyLocationID.Value);
                //lblSMPicklist.Text = objSolarMinerlistCount.SolarMinerPickListCount;


            //}
        }
    }

    protected void btnUpdateSMData_Click(object sender, EventArgs e)
    {
        DataTable SMDt = ClsDbData.QuickStock_GetSMtblProjectData();
        int SuccRow = ClsDbData.USP_InsertUpdate_tbl_SMProject(SMDt);

        DataTable SMDt1 = ClsDbData.QuickStock_GetSMtblStockItemsData();
        int SuccRow1 = ClsDbData.USP_InsertUpdate_tbl_SMStockItems(SMDt1);
    }
}