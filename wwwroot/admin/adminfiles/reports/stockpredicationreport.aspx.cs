using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockwisereport : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;
    public static string Week1 = "";
    public static string Week2 = "";
    public static string Week3 = "";
    public static string Week4 = "";
    public static string Week5 = "";
    public static string Week6 = "";
    public static string Week7 = "";
    public static string Week8 = "";
    public static string Week9 = "";
    public static string Week10 = "";
    public static string Week11 = "";
    public static string Week12 = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            //txtWeekDate.Text= ToDate.AddMonths(-1).ToString("dd/MM/yyyy");
            txtWeekDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            BindTransCompany();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                BindDropDown();
                
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                //ddllocationsearch.SelectedValue = CompanyLocationID;
                SelectLocation(CompanyLocationID);
                //ddllocationsearch.Enabled = false;
                BindGrid(0);
            }
            else
            {
                BindDropDown();
                SelectLocation("1");
                BindGrid(0);
            }

        }

    }

    public void BindDropDown()
    {
        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();
        
        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategorySearch.DataSource = dtCategory;
        ddlCategorySearch.DataTextField = "StockCategory";
        ddlCategorySearch.DataValueField = "StockCategoryID";
        ddlCategorySearch.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string selectedLocationItem = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }

        //DataTable dt1 = Reports.StockPredicationReport_QuickStock(txtstockitemfilter.Text,ddlcategorysearch.SelectedValue,ddllocationsearch.SelectedValue,ddlActive.SelectedValue,ddlSalesTag.SelectedValue, "1", txtStartDate.Text, txtEndDate.Text);

        DataTable dt1 = Reports.QuickStock_StockPredicationReport_Update(txtStockItem.Text, txtStockModel.Text, ddlCategorySearch.Text, selectedLocationItem, ddlActive.SelectedValue, ddlYesNo.SelectedValue, txtWeekDate.Text, txtContainderNo.Text, ddlTranferCompany.SelectedValue, ddlDate.SelectedValue);
        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            PanTotal.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            PanTotal.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < ((dt.Rows.Count)))
                {
                    //========label Hide
                    
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count)-1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count)-1) + " entries";
                    ltrPage.Text = "Showing " + (dt.Rows.Count) + " entries";
                }
            }

        }
        //bind();

        if(dt.Rows.Count > 0)
        {
            Week1 = dt.Rows[0]["WeekDate1"].ToString();
            Week2 = dt.Rows[0]["WeekDate2"].ToString();
            Week3 = dt.Rows[0]["WeekDate3"].ToString();
            Week4 = dt.Rows[0]["WeekDate4"].ToString();
            Week5 = dt.Rows[0]["WeekDate5"].ToString();
            Week6 = dt.Rows[0]["WeekDate6"].ToString();
            Week7 = dt.Rows[0]["WeekDate7"].ToString();
            Week8 = dt.Rows[0]["WeekDate8"].ToString();
            Week9 = dt.Rows[0]["WeekDate9"].ToString();
            Week10 = dt.Rows[0]["WeekDate10"].ToString();
            Week11 = dt.Rows[0]["WeekDate11"].ToString();
            Week12 = dt.Rows[0]["WeekDate12"].ToString();
        }
    }
    
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
       // custom_datepicker
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
       
        //dt.AcceptChanges();
        Export oExport = new Export();
        string FileName = "StockPredicationReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        int[] ColList = { 1, 2, 55, 4, 17, 18, 5, 6, 7, 8, 9, 10, 11, 12,
                        13, 14, 15, 16  };
        string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Current Qty", "Stock Sold", "Wholesale Order", Week1, Week2, Week3, Week4, Week5, Week6, Week7, Week8, Week9, Week10, Week11, Week12 };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStockItem.Text = string.Empty;
        txtStockModel.Text = string.Empty;
        ddlCategorySearch.SelectedValue = "";
        ddlYesNo.SelectedValue = "1";
        ddlActive.SelectedValue = "True";
        ddlTranferCompany.SelectedValue = "";
        
        txtWeekDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            SelectLocation(CompanyLocationID);
            //ddllocationsearch.Enabled = false;
        }
        else
        {
            SelectLocation("1");
        }

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            String StockItemID = rowView["StockItemID"].ToString();
            String StockItem = rowView["StockItem"].ToString();
            String LocationID = rowView["CompanyLocationID"].ToString();
            String WeekStartDate1 = rowView["WeekStartDate1"].ToString();
            String WeekEndDate1 = rowView["WeekEndDate1"].ToString();
            String WeekStartDate2 = rowView["WeekStartDate2"].ToString();
            String WeekEndDate2 = rowView["WeekEndDate2"].ToString();
            String WeekStartDate3 = rowView["WeekStartDate3"].ToString();
            String WeekEndDate3 = rowView["WeekEndDate3"].ToString();
            String WeekStartDate4 = rowView["WeekStartDate4"].ToString();
            String WeekEndDate4 = rowView["WeekEndDate4"].ToString();
            String WeekStartDate5 = rowView["WeekStartDate5"].ToString();
            String WeekEndDate5 = rowView["WeekEndDate5"].ToString();
            String WeekStartDate6 = rowView["WeekStartDate6"].ToString();
            String WeekEndDate6 = rowView["WeekEndDate6"].ToString();
            String WeekStartDate7 = rowView["WeekStartDate7"].ToString();
            String WeekEndDate7 = rowView["WeekEndDate7"].ToString();
            String WeekStartDate8 = rowView["WeekStartDate8"].ToString();
            String WeekEndDate8 = rowView["WeekEndDate8"].ToString();
            String WeekStartDate9 = rowView["WeekStartDate9"].ToString();
            String WeekEndDate9 = rowView["WeekEndDate9"].ToString();
            String WeekStartDate10 = rowView["WeekStartDate10"].ToString();
            String WeekEndDate10 = rowView["WeekEndDate10"].ToString();
            String WeekStartDate11 = rowView["WeekStartDate11"].ToString();
            String WeekEndDate11 = rowView["WeekEndDate11"].ToString();
            String WeekStartDate12 = rowView["WeekStartDate12"].ToString();
            String WeekEndDate12 = rowView["WeekEndDate12"].ToString();


            HyperLink hypWeek1 = (HyperLink)e.Row.FindControl("hypWeek1");
            HyperLink hypWeek2 = (HyperLink)e.Row.FindControl("hypWeek2");
            HyperLink hypWeek3 = (HyperLink)e.Row.FindControl("hypWeek3");
            HyperLink hypWeek4 = (HyperLink)e.Row.FindControl("hypWeek4");
            HyperLink hypWeek5 = (HyperLink)e.Row.FindControl("hypWeek5");
            HyperLink hypWeek6 = (HyperLink)e.Row.FindControl("hypWeek6");
            HyperLink hypWeek7 = (HyperLink)e.Row.FindControl("hypWeek7");
            HyperLink hypWeek8 = (HyperLink)e.Row.FindControl("hypWeek8");
            HyperLink hypWeek9 = (HyperLink)e.Row.FindControl("hypWeek9");
            HyperLink hypWeek10 = (HyperLink)e.Row.FindControl("hypWeek10");
            HyperLink hypWeek11 = (HyperLink)e.Row.FindControl("hypWeek11");
            HyperLink hypWeek12 = (HyperLink)e.Row.FindControl("hypWeek12");

            hypWeek1.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate1 + "&EndDate=" + WeekEndDate1 + "&StockItem=" + StockItem;
            hypWeek1.Target = "_blank";

            hypWeek2.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate2 + "&EndDate=" + WeekEndDate2 + "&StockItem=" + StockItem;
            hypWeek2.Target = "_blank";

            hypWeek3.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate3 + "&EndDate=" + WeekEndDate3 + "&StockItem=" + StockItem;
            hypWeek3.Target = "_blank";

            hypWeek4.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate4 + "&EndDate=" + WeekEndDate4 + "&StockItem=" + StockItem;
            hypWeek4.Target = "_blank";

            hypWeek5.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate5 + "&EndDate=" + WeekEndDate5 + "&StockItem=" + StockItem;
            hypWeek5.Target = "_blank";

            hypWeek6.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate6 + "&EndDate=" + WeekEndDate6 + "&StockItem=" + StockItem;
            hypWeek6.Target = "_blank";

            hypWeek7.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate7 + "&EndDate=" + WeekEndDate7 + "&StockItem=" + StockItem;
            hypWeek7.Target = "_blank";

            hypWeek8.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate8 + "&EndDate=" + WeekEndDate8 + "&StockItem=" + StockItem;
            hypWeek8.Target = "_blank";

            hypWeek9.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate9 + "&EndDate=" + WeekEndDate9 + "&StockItem=" + StockItem;
            hypWeek9.Target = "_blank";

            hypWeek10.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate10 + "&EndDate=" + WeekEndDate10 + "&StockItem=" + StockItem;
            hypWeek10.Target = "_blank";

            hypWeek11.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate11 + "&EndDate=" + WeekEndDate11 + "&StockItem=" + StockItem;
            hypWeek11.Target = "_blank";

            hypWeek12.NavigateUrl = "~/admin/adminfiles/reports/StockPredictionReportDetails.aspx?StockItemID=" + StockItemID + "&LocationID=" + LocationID + "&StartDate=" + WeekStartDate12 + "&EndDate=" + WeekEndDate12 + "&StockItem=" + StockItem;
            hypWeek12.Target = "_blank";
        }
    }

    public void SelectLocation(string LocationID)
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            
            if(hdnModule.Value.ToString()  == LocationID)
            {
                chkselect.Checked = true;
            }
            else
            {
                chkselect.Checked = false;
            }
        }
    }

    public void BindTotal(DataTable dt)
    {
        if(dt.Rows.Count > 0)
        {
            lblCurrentQty.Text = dt.Compute("SUM(StockQuantity)", string.Empty).ToString();
            lblWeek1.Text = dt.Compute("SUM(Week1)", string.Empty).ToString();
            lblWeek2.Text = dt.Compute("SUM(Week2)", string.Empty).ToString();
            lblWeek3.Text = dt.Compute("SUM(Week3)", string.Empty).ToString();
            lblWeek4.Text = dt.Compute("SUM(Week4)", string.Empty).ToString();
            lblWeek5.Text = dt.Compute("SUM(Week5)", string.Empty).ToString();
            lblWeek6.Text = dt.Compute("SUM(Week6)", string.Empty).ToString();
            lblWeek7.Text = dt.Compute("SUM(Week7)", string.Empty).ToString();
            lblWeek8.Text = dt.Compute("SUM(Week8)", string.Empty).ToString();
            lblWeek9.Text = dt.Compute("SUM(Week9)", string.Empty).ToString();
            lblWeek10.Text = dt.Compute("SUM(Week10)", string.Empty).ToString();
            lblWeek11.Text = dt.Compute("SUM(Week11)", string.Empty).ToString();
            lblWeek12.Text = dt.Compute("SUM(Week12)", string.Empty).ToString();
            lblStockSold.Text = dt.Compute("SUM(StockSold)", string.Empty).ToString();
            lblWholesaleOrder.Text = dt.Compute("SUM(WholesaleStockQtyOrdered)", string.Empty).ToString();
        }
        else
        {
            lblCurrentQty.Text = "0";
            lblWeek1.Text = "0";
            lblWeek2.Text = "0";
            lblWeek3.Text = "0";
            lblWeek4.Text = "0";
            lblWeek5.Text = "0";
            lblWeek6.Text = "0";
            lblWeek7.Text = "0";
            lblWeek8.Text = "0";
            lblWeek9.Text = "0";
            lblWeek10.Text = "0";
            lblWeek11.Text = "0";
            lblWeek12.Text = "0";
            lblStockSold.Text = "0";
            lblWholesaleOrder.Text = "0";
        }
    }

    public void BindTransCompany()
    {
        DataTable dt = ClstblStockOrders.tbl_Transport_CompMaster_GetData();
        ddlTranferCompany.DataSource = dt;
        ddlTranferCompany.DataValueField = "Trans_Comp_Id";
        ddlTranferCompany.DataTextField = "Trans_Company_Name";
        ddlTranferCompany.DataBind();
    }
}