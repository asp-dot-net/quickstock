﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="transfer.aspx.cs" Inherits="admin_adminfiles_reports_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="../../../css/style.css" />-->
    <link rel="stylesheet" type="text/css" href="../../../admin/theme/assets/css/print.css" media="print" />
    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>

    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <%-- <h3 class="m-b-xs text-black printpage">Stock Transfer Detail</h3>
            <div class="contactsarea">
                <div class="addcontent printpage">
                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                </div>--%>
            <div class="small-header transition animated fadeIn printorder">
                <div class="hpanel">
                    <div class="panel-body headertopbox">
                        <div class="card">
                            <div class="card-block">
                                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Transfer
                                    <div id="hbreadcrumb" class="pull-right">

                                        <%-- <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click"
                                            CausesValidation="false" CssClass="btn btn-maroon"><i
                                                class="fa fa-backward"></i> Back</asp:LinkButton>


                                    </div>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="bodymianbg">
                        <div class="panel-default">
                            <div class="panel-body">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="col-md-12 margin_bottom10">
                                            <h5>Stock Transfer Detail
                                                <span class="printorder" style="float:right">
                                                    <a href="javascript:window.print();" class="btn btn-danger"><i
                                                            class="fa fa-print" aria-hidden="true"></i>Print</a>
                                                </span>
                                            </h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div runat="server" id="PanGrid" class="printpage">
                                                <div class="stocktransfer">

                                                    <div class="martopbtm20">

                                                        <table class="table table-bordered printarea">
                                                            <tr>
                                                                <td align="left" width="100">
                                                                    <strong>From&nbsp;Location:&nbsp;</strong></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                                </td>
                                                                <td align="left" width="100">
                                                                    <strong>To&nbsp;Location:&nbsp;</strong></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left" width="100"><strong>Transfer Date:
                                                                    </strong></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblTransferDate" runat="server">
                                                                    </asp:Label>
                                                                </td>
                                                                <td align="left" width="100"><strong>Transfer By:
                                                                    </strong></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblTransferBy" runat="server">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </div>
                                                <div class="qty marbmt25">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                        class="table table-bordered printarea">
                                                        <tr>
                                                            <th width="5%" align="center">Qty</th>
                                                            <th width="20%" align="left">Code</th>
                                                            <th align="left">Stock Item</th>
                                                        </tr>
                                                        <asp:Repeater ID="rptItems" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblQty" runat="server">
                                                                            <%#Eval("TransferQty") %></asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblCode" runat="server">
                                                                            <%#Eval("StockCode") %></asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblItem" runat="server">
                                                                            <%#Eval("StockItem") %></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                                <div class="tracking">
                                                    <table width="100%" border="0" cellspacing="5" cellpadding="0"
                                                        class="table table-bordered printarea">
                                                        <tr class="bold">
                                                            <td colspan="2"><strong> Qty:</strong>
                                                                <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="bold">
                                                            <td><strong>Tracking No:</strong>
                                                                <asp:Label ID="lblTrackingNo" runat="server">
                                                                </asp:Label>
                                                            </td>
                                                            <td><strong>Received Date: </strong>
                                                                <asp:Label ID="lblReceivedDate" runat="server">
                                                                </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="bold">
                                                            <td><strong>Notes:</strong></td>
                                                            <td> <strong>Received By:&nbsp;</strong><asp:Label ID="lblReceivedBy"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</asp:Content>