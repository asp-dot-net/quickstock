﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="BrokenSerialNoReport.aspx.cs" Inherits="admin_adminfiles_reports_BrokenSerialNoReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        
    <div class="page-body headertopbox">
        <div class="card">
            <div class="card-block">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Broken Serial Number History
                    <asp:Label runat="server" ID="lblStockItem" />
                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                    <div id="hbreadcrumb" class="pull-right">
                        <%--  <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>
                    </div>
                </h5>

            </div>
        </div>

    </div>
    <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
    <script src="<%=SiteURL%>admin/theme/assets/js/select2.js"></script>
    <script language="javascript" type="text/javascript">
        function WaterMark(txtName, event) {
            var defaultText = "Enter Username Here";
        }
    </script>
    <script>      
        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });



        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            document.getElementById('loader_div').style.visibility = "visible";
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });

            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox();
            //});


            $(".myval1").select2({
                minimumResultsForSearch: -1
            });

            $(".myval").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }


            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox();
            //});

            callMultiCheckbox1();
            callMultiCheckbox2();


            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox2();
            });

            $(".selectlocation .dropdown dt a").on('click', function () {
                $(".selectlocation .dropdown dd ul").slideToggle('fast');
            });
            $(".SelectItem .dropdown dt a").on('click', function () {
                $(".SelectItem .dropdown dd ul").slideToggle('fast');
            });

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});


        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }

    </script>
    <script type="text/javascript">
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddlLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }
        }

        function callMultiCheckbox2() {
            var title = "";
            $("#<%=ddItem.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel1').show();
                $('.multiSel1').html(html);
                $(".hida1").hide();
            }
            else {
                $('#spanselect1').show();
                $('.multiSel1').hide();
            }

        }
    </script>
    <style>
        .txt_height {
            height: 100px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').addClass('loading-inactive');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
        }
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(".myval").select2({
                allowclear: true,
                minimumResultsForSearch: -1
            });
            $(".myvalemployee").select2({
                allowclear: true,
                minimumResultsForSearch: -1
            });
            $('.redreq').click(function () {
                formValidate();
            });
        }
    </script>
    <div class="page-body padtopzero minheight500">
        <asp:Panel runat="server" ID="PanGridSearch" class="">
            <div class="animate-panel">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>

                <div class="searchfinal">
                    <div class="card pad10">
                        <div class="card-block">
                            <div class="inlineblock">
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                    <div class="row">
                                        <div class="input-group col-sm-2 max_width170" id="div1" runat="server">
                                            <asp:TextBox ID="txtProjectNo" runat="server" placeholder="ProjNo/Picklist" CssClass="form-control m-b"></asp:TextBox>
                                            
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtProjectNo"
                                                WatermarkText="ProjNo/Picklist" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtProjectNo" FilterType="Numbers, Custom" ValidChars="," />
                                            
                                        </div>
                                        <div class="input-group col-sm-2 max_width170" id="div2" runat="server">
                                            <asp:TextBox ID="txtInvoiceNo" runat="server" placeholder="Invoice No" CssClass="form-control m-b"></asp:TextBox>
                                            
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtInvoiceNo"
                                                WatermarkText="Invoice No" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers, Custom" ValidChars="," />
                                            
                                        </div>
                                        <div class="input-group col-sm-2 max_width170" id="divprojNo" runat="server">
                                            <asp:TextBox ID="txtStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtProjNo"
                                                WatermarkText="Project No" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjNo" FilterType="Numbers" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                ControlToValidate="txtProjNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                        

                                        <%--<div class="input-group col-sm-2 max_width170">--%>
                                        <%--<asp:TextBox ID="txtWholesaleorderid" Visible="false" runat="server" placeholder="Whsl ID/Inv No" CssClass="form-control m-b"></asp:TextBox>--%>
                                        <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtWholesaleorderid"
                                                WatermarkText="Whsl ID/Inv No" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtWholesaleorderid" FilterType="Numbers" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                ControlToValidate="txtWholesaleorderid" Display="Dynamic" ErrorMessage="Please enter a number"
                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                        <%--</div>--%>
                                        <div class="input-group col-sm-2 max_width170">
                                            <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtserailno"
                                                WatermarkText="Serial No./Pallet No." />--%>
                                        </div>
                                        <%--<div class="input-group col-sm-2 max_width200">
                                            <asp:ListBox ID="lstlocation2" runat="server" CausesValidation="false" SelectionMode="Multiple" CssClass="js-example-basic-multiple" Width="200px" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);"></asp:ListBox>
                                            <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="lstlocation2"
                                                WatermarkText="Modulename" />
                                        </div>--%>
                                         <div class="form-group spical multiselect SelectItem martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida1" id="spanselect1">Item</span>
                                                                <p class="multiSel1"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddItem" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptItems" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnItem" runat="server" Value='<%# Eval("modulename") %>' />
                                                                                <asp:HiddenField ID="hdnItemID" runat="server" Value='<%# Eval("modulename") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("modulename")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                        <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Location</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddlLocation" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptLocation" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                        <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-group col-sm-1 max_width170">
                                            <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                        </div>
                                        <div class="input-group col-sm-1 max_width170 dnone">
                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                        </div>
                                        <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" Visible="false" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click1"
                                                CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        </div>

                                        <div class="input-group col-sm-1 max_width170" style="display: none">
                                            <asp:LinkButton ID="btnEmail" runat="server" Visible="false"
                                                CausesValidation="false" OnClick="btnEmail_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>SendEmail </asp:LinkButton>
                                        </div>

                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="inlineblock">
                                <div class="row bewtten_div">

                                    <div class="col-sm-2 max_width170">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myvalstockitem">
                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <%--                                         <div class="input-group col-sm-1 martop5 max_width170">
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838;border-color:#218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </asp:Panel>

        <div class="finalgrid">
            <asp:Panel ID="panel" runat="server">
                <div class="page-header card" id="divtot" runat="server">
                    <div class="card-block brd_ornge">
                        <div class="printorder" style="font-size: medium">
                            <b>Total Number of Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                <div>
                    <div id="PanGrid" runat="server">
                        <div class="card">
                            <div class="card-block">
                                <div class="table-responsive  BlockStructure">
                                    <asp:GridView ID="GridView1" DataKeyNames="id" runat="server" CssClass="tooltip-demo text-center table GridviewScrollItem table-bordered Gridview"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                        OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Project No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="Pickid">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjNumber" runat="server" Text='<%#Eval("ProjectNumber")+"/"+ Eval("Pickid") %>'>
                                                </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="WholesaleOrderID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Eval("InvoiceNo")+"/"+ Eval("WholesaleOrderID") %>'>
                                                </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--  <asp:TemplateField HeaderText="Category Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="CategoryName">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server">
                                                <%#Eval("CategoryName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Serial No" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="SerialNo" HeaderStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("SerailNo")%>' data-toggle="tooltip"><%#Eval("SerailNo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Pallet No" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="Pallet" HeaderStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("Pallet")%>' data-toggle="tooltip"><%#Eval("Pallet")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Item Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="StockItem">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server">
                                                <%#Eval("StockItem")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Modulename" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="modulename">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server" data-placement="top" data-toggle="tooltip" Text='<%#Eval("modulename")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="UserName" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label31" runat="server" Width="100px" Text='<%#Eval("CompanyLocation")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <%--<asp:TemplateField HeaderText="Created By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="UserName" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label31" runat="server" Width="100px" Text='<%#Eval("UserName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Create Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" SortExpression="CreatedDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label62" runat="server" data-placement="top" data-toggle="tooltip"
                                                        Text='<%#Eval("CreatedDate", "{0:dd MMM yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LnkUpdate1" runat="server" CausesValidation="true" CommandName="Update" CommandArgument='<%# Eval("SerailNo") +","+ Eval("itemId") %>'
                                                        data-placement="top" data-original-title="Update" data-toggle="tooltip" CssClass="btn btn-primary btn-mini">                                                                       
                                                                          <i class="fa fa-edit"></i>Update
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="gvbtnClear" runat="server" CssClass="btn btn-success btn-mini" CommandName="Clear" CommandArgument='<%#Eval("SerailNo")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Clear
                                                    </asp:LinkButton>
                                                    <!--DELETE Modal Templates-->

                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("SerailNo") %>'>
                                                    <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="" ItemStyle-Width="30px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>                                                
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>--%>
                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid printorder" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </div>
    </div>

    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
    <%--<cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>--%>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

        <div class="modal-dialog " style="margin-top: -300px">
            <div class=" modal-content">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header">
                    <h5 class="modal-title fullWidth">Delete
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                    </h5>
                </div>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:Button ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" CommandName="deleteRow" Text="Ok" />
                    <asp:Button ID="LinkButton7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField ID="hdndelete" runat="server" />
    <asp:HiddenField ID="hdnserialno" runat="server" />
    <asp:HiddenField ID="hdnstockitemid" runat="server" />


    <%--   <cc1:ModalPopupExtender ID="ModalPopupExtenderUpdateSerialNo" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divPVDStatus" TargetControlID="Button9" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>--%>
    <div id="divPVDStatus" runat="server" style="display: none" class="modal_popup">
        <asp:HiddenField ID="HiddenField3" runat="server" />

        <div class="modal-dialog" style="width: 700px;">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="justify-content: center;">
                    <h4 class="modal-title">Update Serial No</h4>
                    <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <table class="margin20">
                                <tr>
                                    <td style="padding-right: 5px;">Serial No</td>
                                    <td>
                                        <asp:TextBox ID="txtserialno" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Serial No" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="padding-right: 5px;">Update Serial No</td>
                                    <td>
                                        <asp:TextBox ID="txtupdateserno" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Serial No"></asp:TextBox>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="justify-content: center;">
                    <asp:Button ID="btnUpdateserialno" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnUpdateserialno_Click"></asp:Button>
                    <asp:Button ID="btncancelserialno" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="Button9" Style="display: none;" runat="server" />


    <%--<cc1:ModalPopupExtender ID="ModalPopupExtenderEEmail" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="ModelEmail" TargetControlID="btnNull1"
        CancelControlID="LinkButton6">
    </cc1:ModalPopupExtender>--%>
    <div id="ModelEmail" runat="server" style="display: none; width: 100%" class="modal_popup">
        <div class="modal-dialog" style="max-width: 700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fullWidth" id="myModalLabel1">Send Mail
                                <span style="float: right" class="printorder" />

                        <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="col-md-12">
                                <div class="qty marbmt25">
                                    <br />
                                    <div class="row">
                                        <%-- <asp:Label runat="server">Enter Email</asp:Label>--%>
                                        <div class="col-lg-12 padd_btm10">
                                            <asp:TextBox ID="txtemail" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Enter Multiple Email Address with comma(,)" CssClass="form-control m-b height100 txt_height"></asp:TextBox><br />
                                            <asp:LinkButton ID="btnsendmail" CssClass="btn btn-info POPupLoader" runat="server" Text="Send Mail" OnClick="btnsendmail_Click"></asp:LinkButton>
                                        </div>
                                        <div class="col-lg-12 padd_btm10">
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNull1" Style="display: none;" runat="server" />
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
            </ContentTemplate>
        <Triggers>
            <%-- <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <asp:PostBackTrigger ControlID="btnaddmodule" />
            <asp:PostBackTrigger ControlID="btnAddInverter" />--%>
            <%-- <asp:PostBackTrigger ControlID="btnEmail" />--%>
            <asp:PostBackTrigger ControlID="btnEmail" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

