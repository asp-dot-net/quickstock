<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="stockreceivedreportDetailpage.aspx.cs" Inherits="admin_adminfiles_reports_stockreceivedreportDetailpage" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }
        .table tbody .brd_ornge td, .brd_ornge{border-bottom: 3px solid #ff784f;}
        
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".AriseInstaller .dropdown dt a").on('click', function () {
                $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
            });
            $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
                $(".AriseInstaller .dropdown dd ul").hide();
            });

            $(".SMInstaller .dropdown dt a").on('click', function () {
                $(".SMInstaller .dropdown dd ul").slideToggle('fast');
            });
            $(".SMInstaller .dropdown dd ul li a").on('click', function () {
                $(".SMInstaller .dropdown dd ul").hide();
            });

            $(".WCustomer .dropdown dt a").on('click', function () {
                $(".WCustomer .dropdown dd ul").slideToggle('fast');
            });
            $(".WCustomer .dropdown dd ul li a").on('click', function () {
                $(".WCustomer .dropdown dd ul").hide();
            });

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();
            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox2();
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox3();
            });

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>
    <script>
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddAriseInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }
        }

        function callMultiCheckbox2() {
            var title = "";
            $("#<%=ddSMInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }
        }

        function callMultiCheckbox3() {
            var title = "";
            $("#<%=ddWCustomer.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }
        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Deduct Details - <asp:Label runat="server" ID="txtStockItem" />
                           <asp:Label runat="server" ID="lblStockItem" />
                        <div id="hbreadcrumb" class="pull-right">
                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                        </div>
                    </h5>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">

                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">
                                                    <%--<div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtOrderNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtOrderNumber"
                                                            WatermarkText="Order Number" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtOrderNumber" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GEtOrderNumber"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtOrderNumber" FilterType="Numbers" />
                                                    </div>--%>
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtProjNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                    </div>
                                                    <%--<div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtInstallerName" runat="server" CssClass="form-control m-b" placeholder="Installer Name"></asp:TextBox>
                                                    </div>--%>
                                                    <div class="form-group spical multiselect AriseInstaller martop5 col-sm-2 max_width170 specail1_select" id="DivAriseInstaller" runat="server" visible="false">
                                                    <dl class="dropdown ">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Installer</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddAriseInstaller" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptAriseInstaller" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnAriseInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                                <asp:HiddenField ID="hdnAriseInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltAriseInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div class="form-group spical multiselect SMInstaller martop5 col-sm-2 max_width170 specail1_select" id="DivSMInstaller" runat="server" visible="false">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Installer</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddSMInstaller" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptSMInstaller" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnSMInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                                <asp:HiddenField ID="hdnSMInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltSMInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div class="form-group spical multiselect WCustomer martop5 col-sm-2 max_width170 specail1_select" id="DivWCustomer" runat="server" visible="false">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Customer</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddWCustomer" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptWCustomer" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnWCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                <asp:HiddenField ID="hdnWCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltWCustomer" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Deduct Date</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838" Visible="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel" runat="server" >                  
                    <div class="page-header card" id="divtot" runat="server">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Total Number of Qty:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Project Number" SortExpression="ProjectNumber1">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProjectNumber" runat="server" Width="20px">
                                                                                        <%--<%#Eval("Projectnumber")%>/<%#Eval("PickId")%>--%><%#Eval("ProjectNumber1")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="220px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company Name" SortExpression="CompanyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompanyName" runat="server" Width="20px">
                                                                                        <%#Eval("CompanyName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="220px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Installer Name" SortExpression="InstallerName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallerName" runat="server" Width="20px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="220px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Deduct Date" SortExpression="DeductOn">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeducton" runat="server" Width="20px">
                                                            <%--<%#Eval("DeductOn")%>--%>
                                                            <%#Eval("DeductOn","{0:dd MMM yyyy}")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="220px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty" SortExpression="OrderQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label47" runat="server" Width="20px">
                                                                                        <%#Eval("OrderQuantity")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="220px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
