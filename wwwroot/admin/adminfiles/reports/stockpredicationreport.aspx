<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="stockpredicationreport.aspx.cs" Inherits="admin_adminfiles_reports_stockwisereport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">

        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";


        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");

            $(".Location .dropdown dt a").on('click', function () {
                $(".Location .dropdown dd ul").slideToggle('fast');
            });
            $(".Location .dropdown dd ul li a").on('click', function () {
                $(".Location .dropdown dd ul").hide();
            });

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");
            callMultiCheckbox1();
            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            //callMultiCheckbox();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });
            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel2').show();
                $('.multiSel2').html(html);
                $(".hida2").hide();
            }
            else {
                $('#spanselect2').show();
                $('.multiSel2').hide();
            }

        }

    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Predication Report
                    </h5>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlCategorySearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtStockItem"
                                                            WatermarkText="Stock Item" />
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtStockModel" runat="server" placeholder="Stock Model" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtStockModel"
                                                            WatermarkText="Stock Model" />
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170" style="display: none">
                                                        <asp:TextBox ID="txtContainderNo" runat="server" placeholder="Container No" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtContainderNo"
                                                            WatermarkText="Container No" />
                                                    </div>


                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlTranferCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Transfer Company</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="form-group spical multiselect Location martop5 col-sm-2 max_width170 specail1_select">
                                                        <dl class="dropdown ">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida2" id="spanselect2">Location</span>
                                                                    <p class="multiSel2"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddLocation" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="rptLocation" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                    <asp:HiddenField ID="hdnLocationID" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Stock Active</asp:ListItem>
                                                            <asp:ListItem Value="True" Selected="True">Only Active</asp:ListItem>
                                                            <asp:ListItem Value="False">Not Active</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <%--<asp:ListItem Value="">Sales Tag</asp:ListItem>--%>
                                                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">Target Date</asp:ListItem>
                                                            <asp:ListItem Value="2">ETD</asp:ListItem>
                                                            <asp:ListItem Value="3">ETA</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtWeekDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel2" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanTotal" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <th class="brdrgrayleft" align="center" scope="col" style="width: 360px;">a href="#">Stock Item</a></th>

                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#">Current Qty</a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#">Stock Sold</a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#">Wholesale Order</a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week1%> </a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week2%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week3%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week4%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week5%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week6%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week7%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week8%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week9%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week10%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week11%></a></th>
                                                    <th class="brdrgrayleft" align="center" scope="col"><a href="#"><%=Week12%></a></th>

                                                </tr>
                                                <tr class="brd_ornge">
                                                    <td align="left" valign="top">Total</td>

                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblCurrentQty" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblStockSold" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWholesaleOrder" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek1" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek2" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek3" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek4" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek5" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek6" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek7" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek8" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek9" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek10" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek11" runat="server"></asp:Literal>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:Literal ID="lblWeek12" runat="server"></asp:Literal>
                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stock Item" SortExpression="StockItem">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndStockitemID" runat="server" Value='<%#Eval("StockItemID")%>' />
                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                        <asp:Label ID="Label11" runat="server" Width="50px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StockItem")%>'>
                                                            <%#Eval("StockItem")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="230px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Model" SortExpression="StockModel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStockModel" runat="server" Width="20px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StockModel")%>'>
                                                             <%#Eval("StockModel")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompanyLocation" runat="server" Width="20px">
                                                             <%#Eval("CompanyLocation")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Current Qty" SortExpression="StockQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label94" runat="server" Width="20px">
                                                             <%#Eval("StockQuantity")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stock Sold" SortExpression="StockSold">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label52" runat="server" Width="20px">
                                                                                        <%#Eval("StockSold")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Wholesale Order">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label82" runat="server" Width="20px">                                                                                      
                                                             <%#Convert.ToInt32(Eval("WholesaleStockQtyOrdered"))%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek1" runat="server">
                                                            <%= Week1%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek1" runat="server" Width="30px" >
                                                            <%# Eval("Week1")%> 
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek1" Enabled='<%#Eval("Week1").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week1")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek2" runat="server">
                                                            <%= Week2%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek2" runat="server" Width="30px">
                                                            <%# Eval("Week2")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek2" Enabled='<%#Eval("Week2").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week2")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek3" runat="server">
                                                            <%= Week3%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek3" runat="server" Width="30px">
                                                            <%# Eval("Week3")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek3" Enabled='<%#Eval("Week3").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week3")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek4" runat="server">
                                                            <%= Week4%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek4" runat="server" Width="30px">
                                                            <%# Eval("Week4")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek4" Enabled='<%#Eval("Week4").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week4")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek5" runat="server">
                                                            <%= Week5%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek5" runat="server" Width="30px">
                                                            <%# Eval("Week5")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek5" Enabled='<%#Eval("Week5").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week5")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek6" runat="server">
                                                            <%= Week6%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek6" runat="server" Width="30px">
                                                            <%# Eval("Week6")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek6" Enabled='<%#Eval("Week6").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week6")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek7" runat="server">
                                                            <%= Week7%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek7" runat="server" Width="30px">
                                                            <%# Eval("Week7")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek7" Enabled='<%#Eval("Week7").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week7")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek8" runat="server">
                                                            <%= Week8%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblweek8" runat="server" Width="30px">
                                                            <%# Eval("Week8")%>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink runat="server" ID="hypWeek8" Enabled='<%#Eval("Week8").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week8")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek9" runat="server">
                                                            <%= Week9%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="hypWeek9" Enabled='<%#Eval("Week9").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week9")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek10" runat="server">
                                                            <%= Week10%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="hypWeek10" Enabled='<%#Eval("Week10").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week10")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek11" runat="server">
                                                            <%= Week11%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="hypWeek11" Enabled='<%#Eval("Week11").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week11")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="hdrWeek12" runat="server">
                                                            <%= Week12%> 
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="hypWeek12" Enabled='<%#Eval("Week12").ToString()=="0"?false:true %>'>
                                                            <%# Eval("Week12")%>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="10%" align="center">Index No.</th>
                                                    <th width="25%" align="center">Serial No.</th>
                                                    <th width="15%" align="left">Category</th>
                                                    <th align="50%">Stock Item</th>

                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialNo") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".Location .dropdown dt a").on('click', function () {
            $(".Location .dropdown dd ul").slideToggle('fast');
        });
        $(".Location .dropdown dd ul li a").on('click', function () {
            $(".Location .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox();
            //});
        });

        //function callMultiCheckbox() {


        //}


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
