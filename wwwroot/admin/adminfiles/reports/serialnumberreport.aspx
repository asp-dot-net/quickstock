<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="serialnumberreport.aspx.cs" Inherits="admin_adminfiles_reports_serialnumberreport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }
         
        .focusred {
            border-color: #FF5F5F !important;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();
            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //  callMultiCheckbox();

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Serial Number Wise Report
                    </h5>


                </div>
            </div>

            <div class="col-md-12" id="divright" runat="server">
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged">

                    <cc1:TabPanel ID="TabProjectNo" runat="server" HeaderText="Project">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel4">
                                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                                <div class="alert alert-info" id="EmailFail" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There is no email available</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">

                                                                    <%--      <div class="input-group col-sm-2 martop5 max_width170" id="div3" runat="server">
                                            <asp:DropDownList ID="ddlDeductedOrNot" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Not Deducted</asp:ListItem>
                                                <asp:ListItem Value="True">Deducted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170" id="divCustomer" runat="server">
                                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Stock Order No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                                            WatermarkText="Stock Order No." />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtSearchOrderNo" FilterType="Numbers" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                                            WatermarkText="Project No." />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers" />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtserailno"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                            <asp:ListItem Value="2">InstallBooked</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>
                            <div class="finalgrid">
                                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                                    <div>
                                        <div id="PanGrid" runat="server">
                                            <div class="card shadownone brdrgray">
                                                <div class="card-block">
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                        <asp:HiddenField ID="HndEmail" runat="server" Value='<%#Eval("InstallEmail")%>' />
                                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Installer Name" SortExpression="InstallerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label9" runat="server" Width="70px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Install Booked On" SortExpression="InstallBookedDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label91" runat="server" Width="80px">
                                                                                        <%#Eval("InstallBookedDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <%--
                                                                <asp:TemplateField HeaderText="Category" SortExpression="CategoryName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label95" runat="server" Width="30px">
                                                                                        <%#Eval("CategoryName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Customer" SortExpression="Customer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label193" runat="server" Width="150px">
                                                                                        <%#Eval("Customer")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Project" SortExpression="Project">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label293" runat="server" Width="150px">
                                                                                        <%#Eval("Project")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Stock Detail" SortExpression="SystemDetails">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label93" runat="server" Width="150px">
                                                                                        <%#Eval("SystemDetails")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label94" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductedOn">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label92" runat="server" Width="100px">
                                                                                       <%#Eval("DeductOn","{0: dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="printpage1" CommandArgument='<%#Eval("ProjectNumber")+";"+Eval("PicklistId")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="gvbtnEmail" runat="server" CssClass="btn btn-success btn-mini" CommandName="Email" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("PicklistId")+";"+Eval("InstallEmail")%>' CausesValidation="false" data-original-title="Email" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Email
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>

                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopage">
                                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabStockTransfer" runat="server" HeaderText="Stock Transfer">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel2">
                                    <asp:UpdatePanel ID="updatepanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div1" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div2" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label1" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div3" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div4" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch2">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">

                                                                    <%--      <div class="input-group col-sm-2 martop5 max_width170" id="div3" runat="server">
                                            <asp:DropDownList ID="ddlDeductedOrNot" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Not Deducted</asp:ListItem>
                                                <asp:ListItem Value="True">Deducted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter2" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtstockitemfilter2"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlcategorysearch2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170" id="div5" runat="server">
                                                                        <asp:DropDownList ID="ddllocationsearch2" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtSearchOrderNo2" runat="server" placeholder="Stock Order No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSearchOrderNo2"
                                                                            WatermarkText="Stock Order No." />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSearchOrderNo2" FilterType="Numbers" />
                                                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtTransferNumber" runat="server" CssClass="form-control m-b" placeholder="Transfer No."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtTransferNumber"
                                                                            WatermarkText="Transfer No." />
                                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtTransferNumber" FilterType="Numbers" />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtserailno2" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtserailno2"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlDate2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtstartdate2" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtenddate2" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch2" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch2_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll2" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll2_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords2" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged2"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport2" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport2_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport2" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="finalgrid">
                                        <asp:Panel ID="panel5" runat="server" CssClass="xsroll">
                                            <div>
                                                <div id="PanGrid2" runat="server">
                                                    <div class="card shadownone brdrgray">
                                                        <div class="card-block">
                                                            <div class="table-responsive BlockStructure">
                                                                <asp:GridView ID="GridView2" DataKeyNames="IsStockTransferID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                                    OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging" OnRowCommand="GridView2_RowCommand"
                                                                    OnDataBound="GridView2_DataBound" AllowSorting="true" OnRowCreated="GridView2_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Transfer No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="IsStockTransferID" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hndtransferID" runat="server" Value='<%#Eval("IsStockTransferID")%>' />
                                                                                <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                <asp:Label ID="Label51" runat="server" Width="30px">
                                                                                        <%#Eval("IsStockTransferID")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField HeaderText="Stock Order No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="StockOrderID" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label52" runat="server" Width="30">
                                                                                        <%#Eval("StockOrderID")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Serial No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="SerialNo" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label53" runat="server" Width="80px">
                                                                                        <%#Eval("SerialNo")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                      
                                                                        <asp:TemplateField HeaderText="Category" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CategoryName" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label57" runat="server" Width="30px">
                                                                                        <%#Eval("CategoryName")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField HeaderText="Stock Items" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="TranferedItems" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label55" runat="server" Width="150px">
                                                                                        <%#Eval("TranferedItems")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label56" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Transferred On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="TransferByDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label54" runat="server" Width="100px">
                                                                                        <%#Eval("TransferByDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="gvbtnView2" runat="server" CssClass="btn btn-success btn-mini" CommandName="printpage2" CommandArgument='<%#Eval("StockTransferID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <AlternatingRowStyle />
                                                                    <PagerTemplate>
                                                                        <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        <div class="pagination">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                        </div>
                                                                    </PagerTemplate>
                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                </asp:GridView>

                                                            </div>
                                                            <div class="paginationnew1" runat="server" id="divnopage2">
                                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage2" style="width: 100%; border-collapse: collapse;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabWholesale" runat="server" HeaderText="Wholesale">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel7">
                                    <asp:UpdatePanel ID="updatepanel3" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div6" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div7" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label2" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div8" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div9" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel8" runat="server" DefaultButton="btnSearch3">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter3" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtstockitemfilter3"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlcategorysearch3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170" id="div10" runat="server">
                                                                        <asp:DropDownList ID="ddllocationsearch3" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtSearchOrderNo3" runat="server" placeholder="Stock Order No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSearchOrderNo3"
                                                                            WatermarkText="Stock Order No." />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtSearchOrderNo3" FilterType="Numbers" />
                                                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtWholesaleOrderNo" runat="server" CssClass="form-control m-b" placeholder="Wholesale Order No."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtWholesaleOrderNo"
                                                                            WatermarkText="Wholesale Order No." />
                                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtWholesaleOrderNo" FilterType="Numbers" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control m-b" placeholder="Invoice No."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtInvoiceNo"
                                                                            WatermarkText="Invoice No." />
                                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers" />
                                                                    </div>
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:TextBox ID="txtserailno3" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtserailno3"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddldate3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtstartdate3" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtenddate3" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch3" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch3_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll3" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll3_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords3" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged3"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport3" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport3_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport3" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="finalgrid">
                                        <asp:Panel ID="panel9" runat="server" CssClass="xsroll">
                                            <div>
                                                <div id="PanGrid3" runat="server">
                                                    <div class="card shadownone brdrgray">
                                                        <div class="card-block">
                                                            <div class="table-responsive BlockStructure">
                                                                <asp:GridView ID="GridView3" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                                    OnSorting="GridView3_Sorting" OnPageIndexChanging="GridView3_PageIndexChanging" OnRowCommand="GridView3_RowCommand"
                                                                    OnDataBound="GridView3_DataBound" AllowSorting="true" OnRowCreated="GridView3_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Wholesale Order No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="WholesaleOrderID" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hndWholesaleorderID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                <asp:Label ID="Label61" runat="server" Width="30px">
                                                                                        <%#Eval("WholesaleOrderID")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="InvoiceNo" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label70" runat="server" Width="30px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vendor" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label71" runat="server" Width="100px">
                                                                                        <%#Eval("Vendor")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Stock Items" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="WholesaleOrderItem" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label66" runat="server" Width="200px">
                                                                                        <%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label67" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Deducted On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label63" runat="server" Width="80px">
                                                                                        <%#Eval("StockDeductDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="gvbtnView3" runat="server" CssClass="btn btn-success btn-mini" CommandName="printpage3" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="gvbtnEmail" runat="server" CssClass="btn btn-success btn-mini" CommandName="Email3" CommandArgument='<%#Eval("WholesaleOrderID") + ";" +Eval("CustEmail")%>' CausesValidation="false" data-original-title="Email" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Email
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle />
                                                                    <PagerTemplate>
                                                                        <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        <div class="pagination">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                        </div>
                                                                    </PagerTemplate>
                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                </asp:GridView>

                                                            </div>
                                                            <div class="paginationnew1" runat="server" id="divnopage3">
                                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage3" style="width: 100%; border-collapse: collapse;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="10%" align="center">Index No.</th>
                                                    <th width="25%" align="center">Serial No.</th>
                                                    <th width="15%" align="left">Category</th>
                                                    <th align="50%">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>

                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialNo") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                   <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

                 <cc1:ModalPopupExtender ID="ModalPopupExtenderEEmail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="ModelEmail" TargetControlID="btnNull1"
                CancelControlID="LinkButton6">
            </cc1:ModalPopupExtender>
            <div id="ModelEmail" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel1">Send Multiple Email with comma(,)
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                               <%-- <asp:Label runat="server">Enter Email</asp:Label>--%>
                                                 <asp:HiddenField ID="hndProjectNumber" runat="server"/>
                                                   <asp:HiddenField ID="hndPicklistID" runat="server" />
                                                     <asp:HiddenField ID="hndInstallerEmailID" runat="server" />
                                                  <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtemail" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Enter Multiple Email Address with comma(,)" CssClass="form-control m-b height100 txt_height"></asp:TextBox><br />
                                                    <asp:LinkButton ID="btnsendmail" CssClass="btn btn-info POPupLoader" runat="server" Text="Send Mail" OnClick="btnsendmail_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />

             <cc1:ModalPopupExtender ID="ModalPopupExtenderWholeEmail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="WholesaleEmailModel" TargetControlID="Button1"
                CancelControlID="LinkButton7">
            </cc1:ModalPopupExtender>
            <div id="WholesaleEmailModel" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel2">Send Multiple Email with comma(,)
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                               <%-- <asp:Label runat="server">Enter Email</asp:Label>--%>
                                                 <asp:HiddenField ID="HiddenField1" runat="server"/>
                                                   <asp:HiddenField ID="HiddenField2" runat="server" />
                                                     <asp:HiddenField ID="HiddenField3" runat="server" />
                                                  <div class="col-lg-12 padd_btm10">
                                                       <asp:HiddenField ID="hndwholesaleid" runat="server" />
                                                    <asp:TextBox ID="txtwholesalemail" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Enter Multiple Email Address with comma(,)" CssClass="form-control m-b height100 txt_height"></asp:TextBox><br />
                                                    <asp:LinkButton ID="btnwholesaleSentmail" CssClass="btn btn-info POPupLoader" runat="server" Text="Send Mail" OnClick="btnwholesaleSentmail_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button1" Style="display: none;" runat="server" />


            

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
