﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_SrockPendingItemWiseDetails : System.Web.UI.Page
{
    static string prevPage = String.Empty;
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            prevPage = Request.UrlReferrer.ToString();

            lblStockItem.Text = Request.QueryString["StockItem"].ToString();
            //BindVendor();
            BindGrid(0);
        }

    }

    public void BindVendor()
    {
        //ddlSearchInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlSearchInstaller.DataValueField = "ContactID";
        //ddlSearchInstaller.DataMember = "Contact";
        //ddlSearchInstaller.DataTextField = "Contact";
        //ddlSearchInstaller.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string startdate = Request.QueryString["startdate"].ToString();
        string enddate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();

        string DateType = "";
        if(!string.IsNullOrEmpty(startdate) || !string.IsNullOrEmpty(enddate))
        {
            DateType = "1";
        }

        DataTable dt1 = null;

        if (Page == "Out")
        {
            lblPageName.Text = Page;
            dt1 = Reports.SrockPendingItemWise_GetDetailsOut(StockItemID, CompanyLocation, "Picklist Out", txtProjectNumber.Text, DateType, startdate, enddate);
        }
        else if(Page == "Revert")
        {
            lblPageName.Text = Page;
            dt1 = Reports.SrockPendingItemWise_GetDetailsOut(StockItemID, CompanyLocation, "PickList Revert,Broken Item,Defected Item", txtProjectNumber.Text, DateType, startdate, enddate);
        }
        else if (Page == "Installed")
        {
            lblPageName.Text = Page;
            dt1 = Reports.SrockPendingItemWise_GetDetailsInstalled(StockItemID, CompanyLocation, txtProjectNumber.Text, DateType, startdate, enddate);
            
        }
            //if (Page == "AriseSold")
        //{
        //    int mode = Reports.QuickStock_StockWiseReport_AriseSoldDetailsPanelorInverter(StockItemID);

        //    if (mode == 1)
        //    {
        //        dt1 = Reports.QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Panel(StockItemID, CompanyLocationId, "1", startdate, enddate, txtProjectNumber.Text);
        //    }
        //    else
        //    {
        //        dt1 = Reports.QuickStock_StockWiseReport_AriseSoldDetailsByStockId_Inverter(StockItemID, CompanyLocationId, "1", startdate, enddate, txtProjectNumber.Text);
        //    }
        //}
        //if (Page == "SMSold")
        //{
        //    int mode = Reports.QuickStock_StockWiseReport_SMSoldDetailsPanelorInverter(StockItemID);

        //    if (mode == 1)
        //    {
        //        dt1 = Reports.QuickStock_StockWiseReport_SMSoldDetailsByStockId_Panel(StockItemID, CompanyLocationId, "1", startdate, enddate, txtProjectNumber.Text);
        //    }
        //    else
        //    {
        //        dt1 = Reports.QuickStock_StockWiseReport_SMSoldDetailsByStockId_Inverter(StockItemID, CompanyLocationId, "1", startdate, enddate, txtProjectNumber.Text);
        //    }
        //}

        if (dt1.Rows.Count > 0)
        {
            int sum = Convert.ToInt32(dt1.Compute("SUM(Qty)", string.Empty));
            //int sum = Convert.ToInt32(dt1.Rows.Count);
            lblTotalPanels.Text = sum.ToString();
        }
        else
        {
            lblTotalPanels.Text = "0";
        }

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        //string StockItemID = Request.QueryString["StockItemID"].ToString();
        //string StockLocationId = Request.QueryString["CompanyLocationId"].ToString();
        //Response.Redirect("~/admin/adminfiles/reports/stockwisereport.aspx");
        Response.Redirect(prevPage);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;
                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        //DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeID("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "");
        //Response.Clear();
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 19, 22, 3, 4, 21, 16, 20 };
        //    string[] arrHeader = { "Project No.", "Installer Name", "Serial No.", "Pallet No.", "Category", "Stock Item", "Location" };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        //ddlSearchInstaller.SelectedValue = "";

        BindGrid(0);
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        BindGrid(0);
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        string Page = Request.QueryString["Page"].ToString();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockPendingItemWiseDetails" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            
            int[] ColList = { 2, 3, 4 };
            string[] arrHeader = { "ProjectNumber", "InstallerName", "Qty" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
}