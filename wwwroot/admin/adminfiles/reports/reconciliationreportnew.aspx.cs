using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_reconciliationreportnew : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv3;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //change by
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSMSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSMSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                ddllocationsearch.SelectedValue = CompanyLocationID;
                ddllocationsearch.Enabled = false;
            }

            if (Roles.IsUserInRole("Wholesale"))
            {
                TabProjectNo.Visible = false;
                BindGrid3(0);
                BindDropDown3();
            }

            //BindCheckbox();
            BindDropDown();

            BindGrid(0);
        }

    }

    public void BindDropDown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataSource = dt;
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();

        ddlSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        ddlSMInstaller.DataTextField = "Contact";
        ddlSMInstaller.DataValueField = "ContactID";
        ddlSMInstaller.DataBind();

        ddlSMLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlSMLocation.DataTextField = "location";
        ddlSMLocation.DataValueField = "CompanyLocationID";
        ddlSMLocation.DataBind();
    }

    public void BindDropDown3()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch3.DataSource = dt;
        ddllocationsearch3.DataTextField = "location";
        ddllocationsearch3.DataValueField = "CompanyLocationID";
        ddllocationsearch3.DataMember = "CompanyLocationID";
        ddllocationsearch3.DataBind();

        DataTable dt2 = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlSearchVendor.DataSource = dt2;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        DataTable dt1 = ClsReportsV2.ReconciliationReport_GetDataV2(txtProjectNumber.Text, txtserailno.Text, txtstockitemfilter.Text, ddllocationsearch.SelectedValue, ddlInstaller.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        return dt1;
    }

    protected DataTable GetGridData3()//wholesaleorderid wise
    {
        DataTable dt1 = Reports.NewReconciliationReport_QuickStockWholesale(txtInvoiceNo.Text, txtserailno3.Text, txtstockitemfilter3.Text, ddllocationsearch3.SelectedValue, ddlSearchVendor.SelectedValue, ddlstatus.SelectedValue, ddldate3.SelectedValue, txtstartdate3.Text, txtenddate3.Text);
        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            //PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            lbtnExport.Visible = true;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    public void BindGrid3(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        dv3 = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid3.Visible = false;
            divnopage3.Visible = false;
        }
        else
        {
            PanGrid3.Visible = true;
            GridView3.DataSource = dt;
            GridView3.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords3.SelectedValue != string.Empty && ddlSelectRecords3.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords3.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage3.Visible = false;
                }
                else
                {
                    divnopage3.Visible = true;
                    int iTotalRecords = dv3.ToTable().Rows.Count;
                    int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords3.SelectedValue == "All")
                {
                    divnopage3.Visible = true;
                    ltrPage3.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged3(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords3.SelectedValue) == "All")
        {
            GridView3.AllowPaging = false;
            BindGrid3(0);
        }
        else
        {
            GridView3.AllowPaging = true;
            GridView3.PageSize = Convert.ToInt32(ddlSelectRecords3.SelectedValue);
            BindGrid3(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        //GridView3.DataSource = dv3;
        //GridView3.DataBind();
        BindGrid3(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid3(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView3.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView3.PageIndex - 2;
            page[1] = GridView3.PageIndex - 1;
            page[2] = GridView3.PageIndex;
            page[3] = GridView3.PageIndex + 1;
            page[4] = GridView3.PageIndex + 2;
            page[5] = GridView3.PageIndex + 3;
            page[6] = GridView3.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView3.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView3.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView3.PageIndex == GridView3.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage3 = (Label)gvrow.Cells[0].FindControl("ltrPage3");
            if (dv3.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv3.ToTable().Rows.Count;
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage3.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    void lb_Command3(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid3(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command3);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        DataTable dt1 = Reports.NewReconciliationReport_QuickStock(txtProjectNumber.Text, txtserailno.Text, txtstockitemfilter.Text, ddllocationsearch.SelectedValue, ddlInstaller.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        string[] columnNames = dt1.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 30, 40, 7, 5, 12, 42, 39, 45, 43, 47, 46, 44, 48 };
            string[] arrHeader = { "Project No.", "Project Status", "Installer Name", "Install Booked", "Deducted On", "Revert Date", "Location", "Panel Out", "Panel Installed", "Panel Reverted", "Inverter Out", "Inverter Installed", "Inverter Reverted" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt1, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnExport3_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData3();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

        Response.Clear();

        try
        {
            Export oExport = new Export();
            string FileName = "OrderNoWholesaleWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 0, 1, 14, 3, 6, 2, 5, 7, 8, 10, 12, 11, 9, 13 };
            string[] arrHeader = { "Order No", "Invoice No", "Stc Id", "Customer", "Status", "Location", "Deducted On", "Revert Date", "Panel Out", "Panel Installed", "Panel Reverted", "Inverter Out", "Inverter Installed", "Inverter Reverted" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtstockitemfilter.Text = string.Empty;
        txtserailno.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch.SelectedValue = CompanyLocationID;
            ddllocationsearch.Enabled = false;
        }

        ClearCheckBox();
        BindGrid(0);
    }

    protected void btnClearAll3_Click(object sender, EventArgs e)
    {
        txtstockitemfilter3.Text = string.Empty;
        txtserailno3.Text = string.Empty;
        txtInvoiceNo.Text = string.Empty;
        ddllocationsearch3.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "";
        ddldate3.SelectedValue = "";
        txtstartdate3.Text = string.Empty;
        txtenddate3.Text = string.Empty;
        ddlstatus.SelectedValue = "";
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {


            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch3.SelectedValue = CompanyLocationID;
            ddllocationsearch3.Enabled = false;


        }

        ClearCheckBox();
        BindGrid3(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
            rptItems.DataBind();

            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string PanelReverted = (GridView1.Rows[rowIndex].FindControl("Label82") as Label).Text;
            hndDifference.Value = PanelReverted;
            DataTable dt = Reports.ViewRevertedStockItems(PicklistId, "", "1");
            if (dt.Rows.Count > 0)
            {
                rptReverteditems.DataSource = dt;
                rptReverteditems.DataBind();
                ModalPopupRevertedItem.Show();
            }
            txtprojectno.Text = string.Empty;

            hndStockCategory.Value = "1";
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string InverterReverted = (GridView1.Rows[rowIndex].FindControl("Label152") as Label).Text;
            hndDifference.Value = InverterReverted;
            DataTable dt = Reports.ViewRevertedStockItems(PicklistId, "", "2");
            if (dt.Rows.Count > 0)
            {
                rptReverteditems.DataSource = dt;
                rptReverteditems.DataBind();
                ModalPopupRevertedItem.Show();
            }
            txtprojectno.Text = string.Empty;
            hndStockCategory.Value = "2";
        }
        BindGrid(0);
    }

    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage3")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
            rptItems.DataBind();
            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;
            string PanelReverted = (GridView3.Rows[rowIndex].FindControl("Label823") as Label).Text;
            hndDifference1.Value = PanelReverted;

            DataTable dt = Reports.ViewRevertedStockItems("", WholesaleOrderID, "1");
            if (dt.Rows.Count > 0)
            {
                rptWholesaleReverteditems.DataSource = dt;
                rptWholesaleReverteditems.DataBind();
                ModalPopupRevertedItemWholesale.Show();
            }

            hndStockCategoryW.Value = "1";
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;
            string InverterReverted = (GridView3.Rows[rowIndex].FindControl("Label1562") as Label).Text;
            hndDifference1.Value = InverterReverted;

            DataTable dt = Reports.ViewRevertedStockItems("", WholesaleOrderID, "2");
            if (dt.Rows.Count > 0)
            {
                rptWholesaleReverteditems.DataSource = dt;
                rptWholesaleReverteditems.DataBind();
                ModalPopupRevertedItemWholesale.Show();
            }

            hndStockCategoryW.Value = "2";
        }
        BindGrid3(0);
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("WholeSale"))
        {
            TabContainer1.ActiveTabIndex = 0;

            BindGrid3(0);
            BindDropDown3();
        }
        else
        {
            if (TabContainer1.ActiveTabIndex == 0)
            {
                BindGrid(0);
                BindDropDown();
            }
            else if (TabContainer1.ActiveTabIndex == 1)
            {
                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

                if (Roles.IsUserInRole("Warehouse"))
                {

                    BindDropDown3();
                    DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                    string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                    ddllocationsearch3.SelectedValue = CompanyLocationID;
                    ddllocationsearch3.Enabled = false;
                    BindGrid3(0);

                }
                else
                {
                    BindGrid3(0);
                    BindDropDown3();
                }

            }
            else if (TabContainer1.ActiveTabIndex == 2)
            {
                BindGridSM(0);
            }
        }
    }

    protected void btnconfirmRevert_Click(object sender, EventArgs e)
    {
        ModalPopupRevertedItem.Hide();
        int count = 0;
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
            if (chksalestagrep.Checked == true)
            {
                count++;
            }
        }
        if (count > 0)
        {
            if (count == 1)
            {
                Label24.Text = "Confirm " + count + " item is reverted back.";
            }
            else
            {
                Label24.Text = "Confirm " + count + " items are reverted back.";
            }

        }
        else
        {
            Label24.Text = "No serial number selected to confirm the revert.";
        }
        btnOK5.Visible = false;
        btnOK4.Visible = true;

        ModalPopupExtenderConfirmRevert.Show();

    }

    protected void btnconfirmWholesaleRevert_Click(object sender, EventArgs e)
    {
        ModalPopupRevertedItemWholesale.Hide();
        int count = 0;
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
            if (chksalestagrep.Checked == true)
            {
                count++;
            }
        }
        if (count > 0)
        {
            if (count == 1)
            {
                Label24.Text = "Confirm " + count + " item is reverted back.";
            }
            else
            {
                Label24.Text = "Confirm " + count + " items are reverted back.";
            }

        }
        else
        {
            Label24.Text = "No serial number selected to confirm the revert.";
        }
        btnOK5.Visible = true;
        btnOK4.Visible = false;

        ModalPopupExtenderConfirmRevert.Show();
    }

    protected void chkisactive_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");

            if (chkisactive.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupRevertedItem.Show();
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");

            if (CheckBox1.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupRevertedItemWholesale.Show();
    }

    protected void btnOK4_Click(object sender, EventArgs e)
    {
        string PicklistID = string.Empty;
        string ProjectID = string.Empty;
        List<string> SerialNoArrayList = new List<string>();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        int counttick = 0;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
            HiddenField hdnProjectID = (HiddenField)item.FindControl("hdnProjectID");
            HiddenField hdnPicklistid = (HiddenField)item.FindControl("hdnPicklistid");
            if (chksalestagrep.Checked == true)
            {
                counttick++;
                SerialNoArrayList.Add(hdnSerialNo.Value);
                PicklistID = hdnPicklistid.Value;
                ProjectID = hdnProjectID.Value;
            }
        }
        var SerialNoList = String.Join(",", SerialNoArrayList);
        if (counttick > 0)
        {
            if (!string.IsNullOrEmpty(ProjectID) && !string.IsNullOrEmpty(PicklistID))
            {
                SttblProjects stproj = new SttblProjects();
                if (TabContainer1.TabIndex == 0)
                {
                    stproj = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                }
                else if(TabContainer1.TabIndex == 2)
                {
                    stproj = ClstblProjects.tblProjects_SelectByProjectIDSM(ProjectID);
                }
                
                DataTable dt3 = Reports.ViewRevertedStockItems_GroupBy_OnlyDirectItems(PicklistID, "", "", SerialNoList.ToString());
                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        bool suc = ClsProjectSale.tblStockItems_RevertStock(dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), dt3.Rows[i]["StockLocationID"].ToString());
                        int suc1 = ClstblrevertItem.tblRevertItem_InsertData(stproj.ProjectNumber, ProjectID, dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), PicklistID);
                    }
                }

                DataTable dt = Reports.ViewRevertedStockItems_GroupByStockItem(PicklistID, "", "", SerialNoList.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString());
                        int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString(), stOldQty.StockQuantity, dt.Rows[i]["QTY"].ToString(), userid, dt.Rows[i]["ProjectID"].ToString());
                        ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    }
                }

                foreach (RepeaterItem item in rptReverteditems.Items)
                {
                    CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");

                    HiddenField hdnDirectLogIn = (HiddenField)item.FindControl("hdnDirectLogIn");
                    HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
                    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hdnProjectID = (HiddenField)item.FindControl("hdnProjectID");
                    HiddenField hdnPicklistid = (HiddenField)item.FindControl("hdnPicklistid");
                    HiddenField hdnSRid = (HiddenField)item.FindControl("hdnSRid");

                    if (chksalestagrep.Checked == true)
                    {

                        bool success = ClstblStockSerialNo.tblStockSerialNo_Revertall_ByPickListIDSerialNo(hdnPicklistid.Value, hdnSerialNo.Value);
                        if (hdnDirectLogIn.Value == "1")
                        {
                            string Section = "Revert Item";
                            string Message = "Stock Revert For Project No:" + stproj.ProjectNumber + " & PicklistId:" + PicklistID;
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, hdnPicklistid.Value, hdnSerialNo.Value, Section, Message, Currendate);
                            ClstblrevertItem.tbl_StockRevertItems_DeleteBySerialNo(hdnSerialNo.Value);
                            DataTable dtrid = ClstblrevertItem.tbl_StockRevertItems_SelectDataBySRid(hdnSRid.Value);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hdnStockItemID.Value, hdnStockLocationID.Value, "1");

                            if (dtrid.Rows.Count == 0)
                            {
                                ClstblrevertItem.tbl_StockRevert_DeleteByid(hdnSRid.Value);
                            }
                        }
                    }
                }




                DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", stproj.ProjectNumber, "", "", PicklistID);
                if (dt2.Rows.Count == 0)
                {
                    //it changes deduct flag if no serial number is available
                    ClstblProjects.tblProjects_tbl_PicklistLog_RevertIsDeductStatus(ProjectID, PicklistID, "False", "", "");
                }
            }

        }
        else
        {
            ModalPopupExtenderConfirmRevert.Hide();
            ModalPopupRevertedItem.Show();
        }

        if (TabContainer1.TabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.TabIndex == 2)
        {
            BindGridSM(0);
        }
    }

    protected void btnOK5_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = string.Empty;
        List<string> SerialNoArrayList = new List<string>();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        int counttick = 0;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
            HiddenField hdnWholesaleOrderID = (HiddenField)item.FindControl("hdnWholesaleOrderID");
            if (chksalestagrep.Checked == true)
            {
                counttick++;
                SerialNoArrayList.Add(hdnSerialNo.Value);
                WholesaleOrderID = hdnWholesaleOrderID.Value;
            }
        }
        var SerialNoList = String.Join(",", SerialNoArrayList);
        if (counttick > 0)
        {
            if (!string.IsNullOrEmpty(WholesaleOrderID))
            {
                Sttbl_WholesaleOrders stWholesaleID = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
                DataTable dt3 = Reports.ViewRevertedStockItems_GroupBy_OnlyDirectItems("", WholesaleOrderID, "", SerialNoList.ToString());
                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        bool suc = ClsProjectSale.tblStockItems_RevertStock(dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), dt3.Rows[i]["StockLocationID"].ToString());
                        //int suc1 = ClstblrevertItem.tblRevertItem_InsertData(stproj.ProjectNumber, ProjectID, dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), PicklistID);
                    }
                }

                DataTable dt = Reports.ViewRevertedStockItems_GroupByStockItem("", WholesaleOrderID, "", SerialNoList.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString());
                        int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString(), stOldQty.StockQuantity, dt.Rows[i]["QTY"].ToString(), userid, dt.Rows[i]["WholesaleOrderID"].ToString(), "", "");
                        Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    }
                }

                foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
                {
                    CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");

                    HiddenField hdnDirectLogIn = (HiddenField)item.FindControl("hdnDirectLogIn");
                    HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
                    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hdnWholesaleOrderID = (HiddenField)item.FindControl("hdnWholesaleOrderID");
                    HiddenField hdnSRid = (HiddenField)item.FindControl("hdnSRid");

                    if (chksalestagrep.Checked == true)
                    {

                        bool success = ClstblStockSerialNo.tblStockSerialNo_Revertall_ByWholesaleIDSerialNo(hdnWholesaleOrderID.Value, hdnSerialNo.Value);
                        if (hdnDirectLogIn.Value == "1")
                        {
                            string Section = "Revert Item";
                            string Message = "Wholesale revert for Invoice No:" + stWholesaleID.InvoiceNo + " and Order Number:" + WholesaleOrderID;
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, hdnWholesaleOrderID.Value, hdnSerialNo.Value, Section, Message, Currendate);
                            ClstblrevertItem.tbl_StockRevertItems_DeleteBySerialNo(hdnSerialNo.Value);
                            DataTable dtrid = ClstblrevertItem.tbl_StockRevertItems_SelectDataBySRid(hdnSRid.Value);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hdnStockItemID.Value, hdnStockLocationID.Value, "1");

                            if (dtrid.Rows.Count == 0)
                            {
                                ClstblrevertItem.tbl_StockRevert_DeleteByid(hdnSRid.Value);
                            }
                        }
                    }
                }

                DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
                if (dt2.Rows.Count == 0)
                {
                    //it changes deduct flag if no serial number is available
                    bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsDeduct_Date_User(WholesaleOrderID, "0", "", "");
                    bool success2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(WholesaleOrderID, "false");
                }
            }

        }
        else
        {
            ModalPopupExtenderConfirmRevert.Hide();
            ModalPopupRevertedItemWholesale.Show();
        }
        BindGrid3(0);
    }

    protected void btnCancel3_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderConfirmRevert.Hide();
        ModalPopupRevertedItem.Show();
    }

    protected void lnkwholeSaleSubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        int i = 0;
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");

            if (chkDifference.Checked == true)
            {
                //chkDifference.Checked = true;
                i++;
            }
            else
            {
                //chkDifference.Checked = false;
            }

        }
        if (i > Convert.ToInt32(hndDifference1.Value))
        {

            SetError();
            ModalPopupRevertedItemWholesale.Show();
            // break;
        }
        else
        {
            foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
                HiddenField rpthndWholesaleOrderId = (HiddenField)item.FindControl("hdnWholesaleOrderID");
                HiddenField hndInvoiceNo = (HiddenField)item.FindControl("hndInvoiceNo");
                if (chkDifference.Checked == true)
                {
                    if ((!string.IsNullOrEmpty(txtwholesaleprojNo.Text)) || (!string.IsNullOrEmpty(TextBox2.Text)))
                    {
                        HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                        HiddenField hnditemid = (HiddenField)item.FindControl("hdnStockItemID");
                        DataTable dt = null;
                        if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
                        {
                            dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                            if (dt.Rows.Count > 0)
                            {
                                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                                section = "Stock Deduct";
                                Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                                section = "Revert Item";
                                Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                                ClstblrevertItem.tbl_WholesaleOrders_UpdateData(txtwholesaleprojNo.Text, "2", userid, date.ToString());
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                                //if (i == Convert.ToInt32(hndDifference1.Value))
                                //{
                                ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                                //}
                            }
                        }
                        if (!string.IsNullOrEmpty(TextBox2.Text))
                        {
                            //dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                            dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                            if (dt.Rows.Count > 0)
                            {
                                //dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                                string projectid = dt.Rows[0]["ProjectID"].ToString();
                                string PicklistId = dt.Rows[0]["ID"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                                section = "Stock Deduct";
                                Message = "Stock Deduct For Project No:" + txtprojectno.Text + "& PicklistId:" + PicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                                section = "Revert Item";
                                Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                                ClstblProjects.tbl_picklistlog_UpdateData(projectid, PicklistId, "2", date.ToString(), userid);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                                //if (i == Convert.ToInt32(hndDifference1.Value))
                                //{
                                ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                                //}
                            }
                        }

                        DataTable dtexist = Clstbl_WholesaleOrders.tblStockSerialNo_Select_ByWholesaleOrderID(rpthndWholesaleOrderId.Value);
                        if (dtexist.Rows.Count == 0)
                        {
                            ClstblrevertItem.tbl_WholesaleOrders_UpdateData(rpthndWholesaleOrderId.Value, "1", "", "");
                        }
                    }
                }
            }
            BindGrid3(0);

        }




    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hdnStockItemID");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
            Label CategoryName = (Label)item.FindControl("lblCategory2");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid1.Value, Categorynm, TextBox2.Text);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox2.Text = string.Empty;
            TextBox2.Focus();
            //Notification("Record with this model number already exists.");
        }
        if (!string.IsNullOrEmpty(TextBox2.Text))
        {
            txtwholesaleprojNo.Text = string.Empty;
            TextBox2.Focus();
        }
        ModalPopupRevertedItemWholesale.Show();
    }

    protected void txtwholesaleprojNo_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hdnStockItemID");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
            Label CategoryName = (Label)item.FindControl("lblCategory2");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid1.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtwholesaleprojNo.Text = string.Empty;
        }
        if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
        {
            TextBox2.Text = string.Empty;
            txtwholesaleprojNo.Focus();
        }
        ModalPopupRevertedItemWholesale.Show();
    }

    protected void lnksubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        int i = 0;
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");

            if (chkDifference.Checked == true)
            {
                //chkDifference.Checked = true;
                i++;
            }
            else
            {
                //chkDifference.Checked = false;
            }

        }
        if (i > Convert.ToInt32(hndDifference.Value))
        {
            ModalPopupRevertedItem.Show();
            SetError();
        }
        else
        {
            foreach (RepeaterItem item in rptReverteditems.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
                HiddenField rpthndProjectid = (HiddenField)item.FindControl("hdnProjectID");
                HiddenField rpthndPicklistId = (HiddenField)item.FindControl("hdnPicklistid");
                HiddenField hndProjectnumber = (HiddenField)item.FindControl("hndProjectnumber");
                if (chkDifference.Checked == true)
                {

                    if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                    {
                        DataTable dt = null;
                        HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                        HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                        if (!string.IsNullOrEmpty(txtprojectno.Text))
                        {
                            string[] no = txtprojectno.Text.Split('/');
                            string PNo = no[0].ToString();
                            dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(PNo);
                            if (dt.Rows.Count > 0)
                            {
                                string projectid = dt.Rows[0]["ProjectID"].ToString();
                                string PicklistId = dt.Rows[0]["ID"].ToString();
                                string LocationId = dt.Rows[0]["LocationId"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                                section = "Stock Deduct";
                                Message = "Stock Deduct For Project No:" + PNo + "& PicklistId:" + PicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                                section = "Revert Item";
                                Message = "Stock Revert For Project No:" + hndProjectnumber.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                                ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");

                                //if (i == Convert.ToInt32(hndDifference.Value))
                                //{
                                //    ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                                //}

                            }
                        }
                        if (!string.IsNullOrEmpty(TextBox1.Text))
                        {
                            dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                            if (dt.Rows.Count > 0)
                            {
                                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                                section = "Stock Deduct";
                                Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value, dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                                section = "Revert Item";
                                Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                                ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "2", userid, date.ToString());
                                ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                                //if (i == Convert.ToInt32(hndDifference.Value))
                                //{
                                ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                                //}
                                //section = "Wholesale Revert";
                                //Message = "Stock Add For WholesaleId:" + rpthndWholesaleOrderId.Value + "By administrator";
                                //ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                            }
                        }
                        DataTable dtexist = ClstblProjects.tblStockSerialNo_Select_ByProjectIdandPicklistid(rpthndProjectid.Value, rpthndPicklistId.Value);
                        if (dtexist.Rows.Count == 0)
                        {
                            ClstblProjects.tbl_picklistlog_UpdateData(rpthndProjectid.Value, rpthndPicklistId.Value, "1", "", "");
                        }
                    }
                }
            }
            BindGrid(0);
        }


    }

    protected void txtprojectno_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string[] ProjPickId = txtprojectno.Text.Split('/');
        //string ProjectNumber = txtprojectno.Text;
        //string ProjectNumber = ProjPickId[0].ToString();
        //string PickID = ProjPickId[1].ToString();
        string ProjectNumber = "";
        string PickID = "";
        if (txtprojectno.Text != "")
        {
            ProjPickId = txtprojectno.Text.Split('/');
            //string ProjectNumber = txtprojectno.Text;
            ProjectNumber = ProjPickId[0].ToString();
            PickID = ProjPickId[1].ToString();
        }

        DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
        string locationid = dt.Rows[0]["LocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hnditemid = (HiddenField)item.FindControl("hdnStockItemID");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
            Label CategoryName = (Label)item.FindControl("lblCategory2");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                //string[] tokens = txtprojectno.Text.Split(',');
                //string last = tokens[tokens.Length - 1];
                //int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid.Value, Categorynm, ProjectNumber);
                int exist = ClstblProjects.Exist_ItemId_tbl_PicklistItemDetail(hnditemid.Value, Categorynm, PickID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            //Notification("Record with this model number already exists.");

        }
        //if (!string.IsNullOrEmpty(txtprojectno.Text))
        //{
        //    TextBox1.Text = string.Empty;
        //    txtprojectno.Focus();
        //}

        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }
        ModalPopupRevertedItem.Show();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string InvoiceNumber = TextBox1.Text;
        DataTable dt1 = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(InvoiceNumber);
        string locationid = dt1.Rows[0]["CompanyLocationId"].ToString();

        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
            HiddenField hnditemid = (HiddenField)item.FindControl("hdnStockItemID");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
            Label CategoryName = (Label)item.FindControl("lblCategory2");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox1.Text = string.Empty;
        }

        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }

        //if (!string.IsNullOrEmpty(TextBox1.Text))
        //{
        //    TextBox1.Text = string.Empty;
        //    TextBox1.Focus();
        //}
        ModalPopupRevertedItem.Show();
    }

    protected void btnprojpocket_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            string Accreditation = "";
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
            HiddenField rpthndPicklistId = (HiddenField)item.FindControl("hdnPicklistid");
            HiddenField hndInstaller = (HiddenField)item.FindControl("hndInstaller");
            HiddenField hnditemid = (HiddenField)item.FindControl("hdnStockItemID");
            DataTable dt = ClstblContacts.tblContacts_SelectbyInstaller(hndInstaller.Value);
            if (dt.Rows.Count > 0)
            {
                Accreditation = dt.Rows[0]["Accreditation"].ToString();
            }
            if (chkDifference.Checked == true)
            {
                Clstbl_Wallet.tbl_Wallet_ItemDetail_Insert(Accreditation, hnditemid.Value, "1", rpthndPicklistId.Value, lblSerialNo.Text);
            }
        }
        BindGrid(0);
    }

    protected void rptReverteditems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //HiddenField hdnPicklistid = (HiddenField)e.Item.FindControl("hdnPicklistid");
        //HiddenField hdnStockItemID = (HiddenField)e.Item.FindControl("hdnStockItemID");
        //CheckBox chkDifference = (CheckBox)e.Item.FindControl("chksalestagrep");
        //int status = Clstbl_Wallet.tbl_Wallet_ItemDetail_Exist(hdnStockItemID.Value, hdnPicklistid.Value);
        //if (status == 1)
        //{
        //    chkDifference.Enabled = false;
        //    chkisactive.Enabled = false;
        //}
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptSearchLocation2.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void BindCheckbox()
    {
        rptSearchLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptSearchLocation.DataBind();

        rptSearchLocation2.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptSearchLocation2.DataBind();
    }

    protected void lnkSwipe_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        string Message1 = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        string[] ProjPickId;
        //string ProjectNumber = txtprojectno.Text;
        string ProjectNumber = "";
        string PickID = "";
        if (txtprojectno.Text != "")
        {
            ProjPickId = txtprojectno.Text.Split('/');
            //string ProjectNumber = txtprojectno.Text;
            ProjectNumber = ProjPickId[0].ToString();
            PickID = ProjPickId[1].ToString();
        }
        

        int i = 0;
        foreach (RepeaterItem item in rptReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");

            if (chkDifference.Checked == true)
            {
                //chkDifference.Checked = true;
                i++;
            }
            else
            {
                //chkDifference.Checked = false;
            }

        }
        if (i > Convert.ToInt32(hndDifference.Value))
        {
            ModalPopupRevertedItem.Show();
            SetError();
        }
        else
        {
            string SerialNo = "";
            foreach (RepeaterItem item in rptReverteditems.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
                HiddenField rpthndProjectid = (HiddenField)item.FindControl("hdnProjectID");
                HiddenField hdnPicklistid = (HiddenField)item.FindControl("hdnPicklistid");

                if (chkDifference.Checked == true)
                {
                    if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                    {
                        DataTable dt = null;
                        //HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                        //HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");

                        if(SerialNo == "")
                        {
                            SerialNo = lblSerialNo.Text;
                        }
                        else
                        {
                            SerialNo += "," + lblSerialNo.Text;
                            //if (!string.IsNullOrEmpty(SerialNo))
                            //{
                            //    SerialNo = SerialNo.Substring(1).ToString();
                            //}
                        }

                        string CategoryID = hndStockCategory.Value != "" ? hndStockCategory.Value : "0";

                        if (!string.IsNullOrEmpty(ProjectNumber))
                        {
                            
                            dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
                            if (dt.Rows.Count > 0)
                            {
                                string projectid = dt.Rows[0]["ProjectID"].ToString();
                                //string PicklistId = dt.Rows[0]["ID"].ToString();
                                string PicklistId = PickID;
                                string Projectnumber = dt.Rows[0]["Projectnumber"].ToString();

                                //Update Swipe (+) in Project Number
                                bool Update1 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                                Message = "Stock Deduct For Project No:" + Projectnumber + "& PicklistId:" + PicklistId + "By administrator Swipe";

                                bool Update = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(PicklistId, Message, lblSerialNo.Text, "Picklist Out", hdnPicklistid.Value);


                                //Update Swipe (-) in Project Number
                                DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(PicklistId, "Picklist Out", SerialNo, CategoryID);
                                if (dt1.Rows.Count > 0)
                                {
                                    bool Update2 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(rpthndProjectid.Value, hdnPicklistid.Value, dt1.Rows[0]["SerailNo"].ToString());

                                    DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                                    Message1 = "Stock Deduct For Project No: " + dt.Rows[0]["Projectnumber"].ToString() + "& PicklistId: " + hdnPicklistid.Value + " By administrator Swipe";

                                    bool Update3 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(hdnPicklistid.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "Picklist Out", PicklistId);
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(TextBox1.Text))
                        {
                            dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                            if (dt.Rows.Count > 0)
                            {
                                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                                string InvoiceNo = dt.Rows[0]["InvoiceNo"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                                
                                //section = "Revert Item";
                                Message = "Stock Deduct For WholesaleOrderID: " + WholesaleOrderID + " Swipe";

                                bool s = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo(WholesaleOrderID, Message, lblSerialNo.Text, "PickList Out", hdnPicklistid.Value, "WholeSale Out");


                                //Update Swipe (-) in Wholesale Number
                                DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(WholesaleOrderID, "WholeSale Out", SerialNo, CategoryID);
                                if (dt1.Rows.Count > 0)
                                {
                                    bool s1 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(rpthndProjectid.Value, hdnPicklistid.Value, dt1.Rows[0]["SerailNo"].ToString());

                                    DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                                    Message1 = "Stock Deduct For Project No: " + dtProjectNo.Rows[0]["Projectnumber"].ToString() + "& PicklistId: " + hdnPicklistid.Value + " By administrator Swipe";

                                    //Message1 = "Stock Deduct For WholesaleOrderID: " + WholesaleOrderID + " Swipe";

                                    bool s2 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo(hdnPicklistid.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "WholeSale Out", WholesaleOrderID, "PickList Out");
                                }
                            }
                        }
                    }
                }
            }
            if (TabContainer1.TabIndex == 0)
            {
                BindGrid(0);
            }
            else if (TabContainer1.TabIndex == 2)
            {
                BindGridSM(0);
            }
        }


    }

    protected void lnkWholesaleSwipe_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        string Message1 = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        int i = 0;
        foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");

            if (chkDifference.Checked == true)
            {
                //chkDifference.Checked = true;
                i++;
            }
            else
            {
                //chkDifference.Checked = false;
            }

        }
        if (i > Convert.ToInt32(hndDifference1.Value))
        {
            SetError();
            ModalPopupRevertedItemWholesale.Show();
        }
        else
        {
            string SerialNo = "";
            foreach (RepeaterItem item in rptWholesaleReverteditems.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chksalestagrep");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo2");
                HiddenField hdnWholesaleOrderID = (HiddenField)item.FindControl("hdnWholesaleOrderID");

                if (chkDifference.Checked == true)
                {
                    if ((!string.IsNullOrEmpty(txtwholesaleprojNo.Text)) || (!string.IsNullOrEmpty(TextBox2.Text)))
                    {
                                                DataTable dt = null;
                        if (SerialNo == "")
                        {
                            SerialNo = lblSerialNo.Text;
                        }
                        else
                        {
                            SerialNo += "," + lblSerialNo.Text;
                            //if (!string.IsNullOrEmpty(SerialNo))
                            //{
                            //    SerialNo = SerialNo.Substring(1).ToString();
                            //}
                        }

                        string CategoryID = hndStockCategoryW.Value != "" ? hndStockCategoryW.Value : "0";

                        if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
                        {
                            dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);

                            if (dt.Rows.Count > 0)
                            {
                                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                                string InvoiceNo = dt.Rows[0]["InvoiceNo"].ToString();
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);

                                section = "Revert Item";
                                Message = "Stock Deduct For WholesaleOrderID::" + WholesaleOrderID + " Swipe"; 

                                ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(WholesaleOrderID, Message, lblSerialNo.Text, "WholeSale Out", hdnWholesaleOrderID.Value);


                                //Update Swipe (-) in Project Number
                                DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(WholesaleOrderID, "WholeSale Out", SerialNo, CategoryID);
                                if (dt1.Rows.Count > 0)
                                {
                                    ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(hdnWholesaleOrderID.Value, dt1.Rows[0]["SerailNo"].ToString());

                                 Message1 = "Stock Deduct For WholesaleOrderID::" + hdnWholesaleOrderID.Value + " Swipe";

                                    ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(hdnWholesaleOrderID.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "WholeSale Out", WholesaleOrderID);
                                }

                            }
                        }
                        if (!string.IsNullOrEmpty(TextBox2.Text))
                        {
                            dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                            if (dt.Rows.Count > 0)
                            {
                                string projectid = dt.Rows[0]["ProjectID"].ToString();
                                string PicklistId = dt.Rows[0]["ID"].ToString();
                                string Projectnumber = dt.Rows[0]["Projectnumber"].ToString();

                                ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                                section = "Revert Item";
                                Message = "Stock Deduct For Project No:" + Projectnumber + "& PicklistId:" + PicklistId + "By administrator Swipe";

                                ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(PicklistId, Message, lblSerialNo.Text, "Picklist Out", hdnWholesaleOrderID.Value);


                                //Update Swipe (-) in Project Number
                                DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(PicklistId, "Picklist Out", SerialNo, CategoryID);
                                if (dt1.Rows.Count > 0)
                                {
                                    ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(hdnWholesaleOrderID.Value, dt1.Rows[0]["SerailNo"].ToString());

                                    Message1 = "Stock Deduct For WholesaleOrderID::" + hdnWholesaleOrderID.Value + " Swipe";

                                    ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(hdnWholesaleOrderID.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "Picklist Out", PicklistId);
                                }
                            }
                        }
                    }
                }
            }
            BindGrid3(0);
        }
    }

    #region SolarMiner
    protected void lnkSMSearch_Click(object sender, EventArgs e)
    {
        BindGridSM(0);
    }

    protected DataTable GetGridDataSM()//SolarMiner Project
    {
        DataTable dt1 = ClsReportsV2.ReconciliationReport_GetData_SolarMinerV2(txtSMProjectNumber.Text, txtSMserailno.Text, txtSMstockitemfilter.Text, ddlSMLocation.SelectedValue, ddlSMInstaller.SelectedValue, ddlSMDate.SelectedValue, txtSMStartDate.Text, txtSMEndDate.Text);
        return dt1;
    }

    public void BindGridSM(int deleteFlag)
    {
        PanGridSM.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridDataSM();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGridSM.Visible = false;
            divSMnopage.Visible = false;
        }
        else
        {
            //PanGrid.Visible = true;
            GridView_SM.DataSource = dt;
            GridView_SM.DataBind();
            lSMbtnExport.Visible = true;

            if (dt.Rows.Count > 0 && ddlSMSelectRecords.SelectedValue != string.Empty && ddlSMSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSMSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divSMnopage.Visible = false;
                }
                else
                {
                    divSMnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView_SM.PageSize * (GridView_SM.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView_SM.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrSMPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSMSelectRecords.SelectedValue == "All")
                {
                    divSMnopage.Visible = true;
                    ltrSMPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void btnSMClearAll_Click1(object sender, EventArgs e)
    {
        txtSMProjectNumber.Text = string.Empty;
        txtSMstockitemfilter.Text = string.Empty;
        txtSMserailno.Text = string.Empty;
        ddlSMLocation.SelectedValue = "";
        ddlSMInstaller.SelectedValue = "";
        ddlSMDate.SelectedValue = "";
        txtSMStartDate.Text = string.Empty;
        txtSMEndDate.Text = string.Empty;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddlSMLocation.SelectedValue = CompanyLocationID;
            ddlSMLocation.Enabled = false;
        }

        ClearCheckBox();
        BindGridSM(0);
    }

    protected void ddlSMSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSMSelectRecords.SelectedValue) == "All")
        {
            GridView_SM.AllowPaging = false;
            BindGridSM(0);
        }
        else
        {
            GridView_SM.AllowPaging = true;
            GridView_SM.PageSize = Convert.ToInt32(ddlSMSelectRecords.SelectedValue);
            BindGridSM(0);
        }
    }

    protected void lSMbtnExport_Click(object sender, EventArgs e)
    {

        DataTable dt1 = GetGridDataSM();

        string[] columnNames = dt1.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 30, 40, 7, 5, 12, 42, 39, 45, 43, 47, 46, 44, 48 };
            string[] arrHeader = { "Project No.", "Project Status", "Installer Name", "Install Booked", "Deducted On", "Revert Date", "Location", "Panel Out", "Panel Installed", "Panel Reverted", "Inverter Out", "Inverter Installed", "Inverter Reverted" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt1, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView_SM_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView_SM.DataSource = sortedView;
        GridView_SM.DataBind();
    }

    protected void GridView_SM_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_SM.PageIndex = e.NewPageIndex;
        GridView_SM.DataSource = dv;
        GridView_SM.DataBind();
        BindGridSM(0);
    }

    protected void GridView_SM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
            rptItems.DataBind();

            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string PanelReverted = (GridView_SM.Rows[rowIndex].FindControl("Label82") as Label).Text;
            hndDifference.Value = PanelReverted;
            DataTable dt = Reports.ViewRevertedStockItems_SM(PicklistId, "", "1");
            if (dt.Rows.Count > 0)
            {
                rptReverteditems.DataSource = dt;
                rptReverteditems.DataBind();
                txtprojectno.Text = string.Empty;
                ModalPopupRevertedItem.Show();
            }
            txtprojectno.Text = string.Empty;

            hndStockCategory.Value = "1";
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string InverterReverted = (GridView_SM.Rows[rowIndex].FindControl("Label152") as Label).Text;
            hndDifference.Value = InverterReverted;
            DataTable dt = Reports.ViewRevertedStockItems_SM(PicklistId, "", "2");
            if (dt.Rows.Count > 0)
            {
                rptReverteditems.DataSource = dt;
                rptReverteditems.DataBind();
                ModalPopupRevertedItem.Show();
            }
            txtprojectno.Text = string.Empty;
            hndStockCategory.Value = "2";
        }
        BindGridSM(0);
    }

    protected void GridView_SM_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView_SM.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView_SM.PageIndex - 2;
            page[1] = GridView_SM.PageIndex - 1;
            page[2] = GridView_SM.PageIndex;
            page[3] = GridView_SM.PageIndex + 1;
            page[4] = GridView_SM.PageIndex + 2;
            page[5] = GridView_SM.PageIndex + 3;
            page[6] = GridView_SM.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView_SM.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView_SM.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView_SM.PageIndex == GridView_SM.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrSMPage = (Label)gvrow.Cells[0].FindControl("ltrSMPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView_SM.PageSize * (GridView_SM.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView_SM.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrSMPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrSMPage.Text = "";
            }
        }
        catch { }
    }

    void lb_CommandSM(object sender, CommandEventArgs e)
    {
        GridView_SM.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGridSM(0);
    }

    protected void GridView_SM_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_CommandSM);
        }
    }
    #endregion
}