using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockreceivedreportDetailpage : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();

            //BindDropDown();

            txtStockItem.Text = Request.QueryString["StockItem"].ToString();

            HideShowCheckBox();
            BindCheckbox();
            BindGrid(0);

        }

    }

    public void BindDropDown()
    {
        //ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlInstaller.DataValueField = "ContactID";
        //ddlInstaller.DataMember = "Contact";
        //ddlInstaller.DataTextField = "Contact";
        //ddlInstaller.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string selectedAInstallerID = "";
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnAriseInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedAInstallerID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (selectedAInstallerID != "")
        {
            selectedAInstallerID = selectedAInstallerID.Substring(1);
        }

        string selectedSMInstallerID = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnSMInstallerID = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedSMInstallerID += "," + hdnSMInstallerID.Value.ToString();
            }
        }
        if (selectedSMInstallerID != "")
        {
            selectedSMInstallerID = selectedSMInstallerID.Substring(1);
        }

        string selectedWCustomerID = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnWCustomerID = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWCustomerID += "," + hdnWCustomerID.Value.ToString();
            }
        }
        if (selectedWCustomerID != "")
        {
            selectedWCustomerID = selectedWCustomerID.Substring(1);
        }

        string InstallerNameQue = Request.QueryString["InstallerName"].ToString();

        string InstallerName = "";
        if (InstallerNameQue == "")
        {
            if (selectedAInstallerID != "")
            {
                InstallerName = selectedAInstallerID;
            }
            else if (selectedSMInstallerID != "")
            {
                InstallerName = selectedSMInstallerID;
            }
            else if (selectedWCustomerID != "")
            {
                InstallerName = selectedWCustomerID;
            }
        }
        else { InstallerName = InstallerNameQue; }

        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string State = Request.QueryString["State"].ToString();
        string CompanyId = Request.QueryString["CompanyId"].ToString();
        string IsDedect = Request.QueryString["IsDeduct"].ToString();
        string startdate = Request.QueryString["startdate"].ToString();
        string enddate = Request.QueryString["enddate"].ToString();
        string datetype = Request.QueryString["DateType"].ToString();

        DataTable dt1 = new DataTable();

        if (string.IsNullOrEmpty(txtStartDate.Text) && string.IsNullOrEmpty(txtEndDate.Text))
        {
                dt1 = Reports.StockDeductReport_QuickStock_ProjectByID(StockItemID, CompanyId, IsDedect, State, InstallerName, txtProjNumber.Text, datetype, startdate, enddate);
        }
        else
        {
            if (ddlDate.SelectedValue == "")
            {
                dt1 = Reports.StockDeductReport_QuickStock_ProjectByID(StockItemID, CompanyId, IsDedect, State, InstallerName, txtProjNumber.Text, datetype, startdate, enddate);
            }
            else
            {
                dt1 = Reports.StockDeductReport_QuickStock_ProjectByID(StockItemID, CompanyId, IsDedect, State, InstallerName, txtProjNumber.Text, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
            }
        }



        if (dt1.Rows.Count > 0)
        {
            int sum = Convert.ToInt32(dt1.Compute("SUM(OrderQuantity)", string.Empty));
            //int sum = Convert.ToInt32(dt1.Rows.Count);
            lblTotalPanels.Text = sum.ToString();
        }
        else
        {
            lblTotalPanels.Text = "0";
        }

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            lbtnExport.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/reports/stockdeductrprt.aspx");
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        //Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockDeductDetailsReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1, 4, 2, 3, 0 };
            string[] arrHeader = { "Project Number", "Company Name", "Installer Name", "Deduct Date", "Qty" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjNumber.Text = string.Empty;
        //txtInstallerName.Text = string.Empty;
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ClearCheckBox();

        BindGrid(0);
    }

    public void BindCheckbox()
    {
        rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        rptAriseInstaller.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void HideShowCheckBox()
    {
        string CompanyId = Request.QueryString["CompanyId"].ToString();

        if (CompanyId == "1") //AriseSolar
        {
            DivAriseInstaller.Visible = true;

            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;
        }
        else if (CompanyId == "2") //SolarMiner
        {
            DivSMInstaller.Visible = true;

            DivAriseInstaller.Visible = false;
            DivWCustomer.Visible = false;
        }
        else if (CompanyId == "3") //Wholesale
        {
            DivWCustomer.Visible = true;

            DivAriseInstaller.Visible = false;
            DivSMInstaller.Visible = false;
        }
        else
        {
            DivAriseInstaller.Visible = false;
            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;
        }
    }
}