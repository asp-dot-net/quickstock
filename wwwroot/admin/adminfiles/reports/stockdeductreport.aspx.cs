﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockdeductreport : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();

            BindDropDown();
            BindGrid(0);

        }

    }

    public void BindDropDown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataSource = dt;
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataBind();

        try
        {
            ddllocationsearch.SelectedValue = "1";
        }
        catch { }


        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch.DataSource = dtcategory;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        DataTable dt1 = null;// Reports.StockReceivedReport_QuickStock(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, ddlActive.SelectedValue, ddlSalesTag.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, DropDownList1.SelectedValue,"");
        //DataView view = date.dt1;
        //view.Sort = "YourColumn ASC";
        //DataTable dt = view.ToTable();
        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
        bind();
    }
    public void bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        //if (dt.Rows.Count == 1)
        //{
        //    if (string.IsNullOrEmpty(dt.Rows[0]["StockItemID"].ToString()))
        //    {

        //        PanGrid.Visible = false;
        //        divnopage.Visible = false;
        //        SetNoRecords();
        //    }
        //}
        int Brisbane = 0;
        int Melbourne = 0;
        int Sydney = 0;
        int Perth = 0;
        int Darwin = 0;
        int Hobart = 0;
        int Adelaide = 0;
        int Canberra = 0;
        int TotBrisbane = 0;
        int TotMelbourne = 0;
        int TotSydney = 0;
        int TotPerth = 0;
        int TotDarwin = 0;
        int TotHobart = 0;
        int TotAdelaide = 0;
        int TotCanberra = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Brisbane = Convert.ToInt32(dt.Rows[i]["Brisbane"]);
            Melbourne = Convert.ToInt32(dt.Rows[i]["Melbourne"]);
            Sydney = Convert.ToInt32(dt.Rows[i]["Sydney"]);
            Perth = Convert.ToInt32(dt.Rows[i]["Perth"]);
            Darwin = Convert.ToInt32(dt.Rows[i]["Darwin"]);
            Hobart = Convert.ToInt32(dt.Rows[i]["Hobart"]);
            Adelaide = Convert.ToInt32(dt.Rows[i]["Adelaide"]);
            Canberra = Convert.ToInt32(dt.Rows[i]["Canberra"]);

            TotBrisbane += Brisbane;
            TotMelbourne += Melbourne;
            TotSydney += Sydney;
            TotPerth += Perth;
            TotDarwin += Darwin;
            TotHobart += Hobart;
            TotAdelaide += Adelaide;
            TotCanberra += Canberra;
        }

        lblBrisbane.Text = Convert.ToString(TotBrisbane);
        lblMelbourne.Text = Convert.ToString(TotMelbourne);
        lblSydney.Text = Convert.ToString(TotSydney);
        lblPerth.Text = Convert.ToString(TotPerth);
        lblDarwin.Text = Convert.ToString(TotDarwin);
        lblHobart.Text = Convert.ToString(TotHobart);
        lblAdelaide.Text = Convert.ToString(TotAdelaide);
        lblCanberra.Text = Convert.ToString(TotCanberra);

        lblTotal.Text = Convert.ToString(TotBrisbane + TotMelbourne + TotSydney + TotPerth + TotDarwin + TotHobart + TotAdelaide + TotCanberra);



    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData1();
        dt.Rows.RemoveAt(dt.Rows.Count - 1);
        Export oExport = new Export();
        string FileName = "StockReceivedReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Brisbane", "Melbourne", "Sydney", "Perth", "Darwin", "Hobart", "Adelaide", "Canberra", "Total" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtstockitemfilter.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "1";
        ddlcategorysearch.SelectedValue = "";
        ddlSalesTag.SelectedValue = "True";
        ddlActive.SelectedValue = "True";
        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        DropDownList1.SelectedValue = "2";
        BindGrid(0);
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItem = arg[0];
            string State = arg[1];
            //if (string.IsNullOrEmpty(StockItem))
            //{
            //    State = "";
            //}
            if (!string.IsNullOrEmpty(StockItem) && !string.IsNullOrEmpty(State))
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByOrderid(StockItem, State, "");
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByOrderid(StockItem, State, "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndStockitemID = (HiddenField)e.Row.FindControl("hndStockitemID");
            //HiddenField hdnCompanyLocationID = (HiddenField)e.Row.FindControl("hdnCompanyLocationID");
            HyperLink btnviewbrisbane = (HyperLink)e.Row.FindControl("btnviewbrisbane");
            HyperLink btnviewMelbourne = (HyperLink)e.Row.FindControl("btnviewMelbourne");
            HyperLink btnviewSydney = (HyperLink)e.Row.FindControl("btnviewSydney");
            HyperLink btnviewPerth = (HyperLink)e.Row.FindControl("btnviewPerth");
            HyperLink btnviewDarwin = (HyperLink)e.Row.FindControl("btnviewDarwin");
            HyperLink btnviewHobart = (HyperLink)e.Row.FindControl("btnviewHobart");
            HyperLink btnviewAdelaide = (HyperLink)e.Row.FindControl("btnviewAdelaide");
            HyperLink btnviewCanberra = (HyperLink)e.Row.FindControl("btnviewCanberra");
            if (!string.IsNullOrEmpty(hndStockitemID.Value))
            {
                btnviewbrisbane.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=QLD";
                btnviewbrisbane.Target = "_blank";
                btnviewMelbourne.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=VIC";
                btnviewMelbourne.Target = "_blank";
                btnviewSydney.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=NSW";
                btnviewSydney.Target = "_blank";
                btnviewPerth.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=WA";
                btnviewPerth.Target = "_blank";
                btnviewDarwin.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=NT";
                btnviewDarwin.Target = "_blank";
                btnviewHobart.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=TAS";
                btnviewHobart.Target = "_blank";
                btnviewAdelaide.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=SA";
                btnviewAdelaide.Target = "_blank";
                btnviewCanberra.NavigateUrl = Siteurl + "admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + hndStockitemID.Value + "&State=ACT";
                btnviewCanberra.Target = "_blank";
            }
        }
    }
}