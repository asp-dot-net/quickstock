﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="SwipeJobReport.aspx.cs" Inherits="admin_adminfiles_reports_SwipeJobReport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style>
                .modal-dialog1 {
                    margin-left: -300px;
                    margin-right: -300px;
                    width: 985px;
                }

                .focusred {
                    border-color: #FF5F5F !important;
                }

                .padd_btm10 {
                    padding-bottom: 15px;
                }

                .height100 {
                    height: 100px;
                }

                .autocomplete_completionListElement {
                    z-index: 9999999 !important;
                }
            </style>

            <script type="text/javascript">

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    <%--$('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });--%>
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();

                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //alert("dgfdg2");


                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    callMultiCheckbox();

                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });


                }
                function pageLoaded() {
                    //alert($(".search-select").attr("class"));
                    //alert("dgfdg3");

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".myvalinvoiceissued").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                    //gridviewScroll();

                    callMultiCheckbox();

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });
                    //  callMultiCheckbox();

                }


                function stopRKey(evt) {
                    var evt = (evt) ? evt : ((event) ? event : null);
                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
                }
                document.onkeypress = stopRKey;

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
            </script>

            <script>

                $(document).ready(function () {

                });
                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            // alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }


            </script>

            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Swipe Job Report
                    </h5>
                </div>
            </div>

            <div class="col-md-12" id="divright" runat="server">
                <div class="page-body padtopzero">
                    <asp:Panel runat="server" ID="Panel4">
                        <asp:UpdatePanel ID="updatepanel1" runat="server">
                            <ContentTemplate>
                                <div class="messesgarea">
                                    <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                    </div>
                                    <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                        <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                            Text="Transaction Failed."></asp:Label></strong>
                                    </div>
                                    <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                        <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                    </div>
                                    <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                    </div>
                                </div>
                                <div class="searchfinal">
                                    <div class="card shadownone brdrgray pad10">
                                        <div class="card-block">
                                            <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSubmit">
                                                <div class="inlineblock martop5">
                                                    <div class="row">
                                                        <div class="input-group martop5 col-sm-2 max_width170">
                                                            <%--<asp:DropDownList ID="ddlProjectNumber" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlProjectNumber_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myval" AutoPostBack="True" >
                                                                <asp:ListItem Value="">Project/Invoice Number</asp:ListItem>
                                                            </asp:DropDownList>--%>
                                                            <%--<asp:TextBox runat="server" ID="txtProjectFirst" CssClass="form-control m-b" placeholder="Project/InvoceNo" />--%>
                                                            <asp:TextBox ID="txtProjectNoFir" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtProjectNoFir"
                                                                WatermarkText="Project No." />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNoFir" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectPickNoByProjectNo"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNoFir" FilterType="Numbers" />--%>
                                                        </div>
                                                        <div class="input-group martop5 col-sm-2 max_width170">
                                                            <%--<asp:DropDownList ID="ddlProjectNumber2" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlProjectNumber2_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myval" AutoPostBack="True">
                                                                <asp:ListItem Value="">Project/Invoice Number</asp:ListItem>
                                                            </asp:DropDownList>--%>
                                                            <%--<asp:TextBox runat="server" id="txtProjectSecond" CssClass="form-control m-b" placeholder="Project/InvoceNo"/> --%>
                                                            <asp:TextBox ID="txtProjectNoSec" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtProjectNoSec"
                                                                WatermarkText="Project No." />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNoSec" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectPickNoByProjectNo"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtProjectNoSec" FilterType="Numbers" />--%>
                                                        </div>

                                                        <div id="DivSubmit" runat="server" class="input-group martop5 col-sm-1 max_width170">
                                                            <asp:LinkButton ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="search" CssClass="btn btn-info redreq"
                                                                CausesValidation="false" OnClick="btnSubmit_Click"></asp:LinkButton>
                                                        </div>
                                                        <div class="input-group martop5 col-sm-1 max_width170">
                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnClearAll" />

                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </div>
                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                                OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                                <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />

                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("ID")%>' />
                                                            <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")+"/"+Eval("ID")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Project Status" SortExpression="ProjectStatus">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblprojectstatus" runat="server" Width="60px">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Installer" SortExpression="InstallerName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label44" runat="server" Width="60px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Installer Pick Up" SortExpression="InstallerName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinstaller" runat="server" Width="60px">
                                                                                        <%#Eval("Installernm")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Booked On" SortExpression="InstallBookedDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label4287" runat="server" Width="30px">
                                                                                        <%#Eval("InstallBookedDate","{0:dd MMM yyyy }")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label47" runat="server" Width="30px">
                                                                                        <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <%-- <asp:TemplateField HeaderText="Installation Completed" SortExpression="InstallCompleted">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label67" runat="server" Width="20px">
                                                                                        <%#Eval("InstallCompleted","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label94" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="P. Out" SortExpression="PanelStockDeducted">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label452" runat="server" Width="30px" Text='<%#Eval("PanelStockDeducted")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="P. Installed" SortExpression="SaleQtyPanel">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label52" runat="server" Width="30px" Text='<%#Eval("SaleQtyPanel")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="P. Difference">
                                                        <ItemTemplate>
                                                            <%-- <asp:Label ID="Label821" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("PanelStockDeducted"))-Convert.ToInt32(Eval("SaleQtyPanel"))%>'>
                                                                        </asp:Label>--%>
                                                            <asp:Label ID="Label82" runat="server" Width="30px" Text='<%#  (Convert.ToInt32(( (Eval("PanelStockDeducted")==null?0:Eval("PanelStockDeducted"))))) - (Convert.ToInt32(( (Eval("SaleQtyPanel")==null?0:Eval("SaleQtyPanel")))))%>'>                                                                                                                                                          
                                                            </asp:Label>
                                                            <asp:LinkButton ID="btnviewrevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> P. Revert
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="P. Revert">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblpanelrevert" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("PanelRevert"))%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="I. Out" SortExpression="InverterStockDeducted">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label4522" runat="server" Width="30px" Text='<%#Eval("InverterStockDeducted")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="I. Installed" SortExpression="SaleQtyInverter">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label752" runat="server" Width="30px" Text='<%#Eval("SaleQtyInverter")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="I. Difference">
                                                        <ItemTemplate>
                                                            <%--  <asp:Label ID="Label152" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("InverterStockDeducted"))-Convert.ToInt32(Eval("SaleQtyInverter"))%>'>--%>
                                                            <asp:Label ID="Label152" runat="server" Width="30px" Text='<%#  (Convert.ToInt32(( (Eval("InverterStockDeducted")==null?0:Eval("InverterStockDeducted"))))) - (Convert.ToInt32(( (Eval("SaleQtyInverter")==null?0:Eval("SaleQtyInverter")))))%>'>                                                                                  
                                                            </asp:Label>
                                                            <asp:LinkButton ID="btnviewrevert2" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> I. Revert
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="I. Revert">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInverterrevert" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("InvertRevert"))%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage1" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="gvbnNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Note" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                              <i class="btn-label fa fa-edit"></i> Note
                                                            </asp:LinkButton>
                                                            <%-- <asp:LinkButton ID="lnkTransfer" runat="server" CssClass="btn btn-success btn-mini" CommandName="Transfer" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Transfer" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> IsTransfer
                                                                        </asp:LinkButton>--%>
                                                            <asp:LinkButton ID="gvbnEmail" runat="server" CssClass="btn btn-success btn-mini" CommandName="Email" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")+ ";" +Eval("InstallerEmail")%>' CausesValidation="false" data-original-title="Email" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Email
                                                            </asp:LinkButton>
                                                            <%-- <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-primary btn-mini" CommandName="Verify" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Verify" data-toggle="tooltip" data-placement="top" Visible="false">                                                                          
                                              <i class="btn-label fa fa-close"></i> Verify
                                                                        </asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 50px;"><b>Date</b>
                                                                                </td>
                                                                                <td style="width: 100px;">
                                                                                    <asp:Label ID="Label2" runat="server" Width="80px"><%#Eval("notedateIN","{0:dd MMM yyyy }")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 50px;"><b>Note</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblProject11" runat="server" Width="50px"><%#Eval("VerifynoteIN")%></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>

                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage1" visible="false">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td style="width: 15%">Total Panel:
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:Label runat="server" ID="lbltotpanel"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">Total Inverter:
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:Label runat="server" ID="lbltotInverter"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
