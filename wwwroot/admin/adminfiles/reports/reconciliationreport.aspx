<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="reconciliationreport.aspx.cs" Inherits="admin_adminfiles_reports_reconciliationreport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Reconciliation Report
                    </h5>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <%--      <div class="input-group col-sm-2 martop5 max_width170" id="div3" runat="server">
                                            <asp:DropDownList ID="ddlDeductedOrNot" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Not Deducted</asp:ListItem>
                                                <asp:ListItem Value="True">Deducted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                                    <%--<div class="input-group col-sm-2 max_width170">
                                                        <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                            WatermarkText="Stock Item/Model" />
                                                    </div>--%>
                                                    <%--                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170" id="divCustomer" runat="server">
                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>--%>
                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Stock Order No." CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                            WatermarkText="Stock Order No." />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtSearchOrderNo" FilterType="Numbers" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                            ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                            WatermarkText="Project No." />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers" />
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtserailno"
                                                            WatermarkText="Serial No./Pallet No." />
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control m-b" placeholder="Invoice No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtInvoiceNo"
                                                            WatermarkText="Invoice No." />
                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers" />
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="SerialNo" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                                        <%#Eval("ProjectNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Revert Serial No." SortExpression="SerialNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label91" runat="server" Width="80px">
                                                                                        <%#Eval("SerialNo")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label912" runat="server" Width="80px">
                                                                                       <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Deducted By" SortExpression="StockDeductByName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Width="70px">
                                                                                        <%#Eval("StockDeductByName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reverted On" SortExpression="RevertedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label913" runat="server" Width="80px">
                                                                                      <%#Eval("RevertedDate","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reverted By" SortExpression="StockRevertByName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label967" runat="server" Width="70px">
                                                                                        <%#Eval("StockRevertByName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="Pallet No." SortExpression="Pallet">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label92" runat="server" Width="100px">
                                                                                        <%#Eval("Pallet")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                <%--   <asp:TemplateField HeaderText="Category" SortExpression="CategoryName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label95" runat="server" Width="30px">
                                                                                        <%#Eval("CategoryName")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Stock Item" SortExpression="Stockitem">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label93" runat="server" Width="150px">
                                                                                        <%#Eval("Stockitem")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label94" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnreconcile" runat="server" CssClass="btn btn-inverse btn-mini" CausesValidation="false" data-original-title="Reconcile" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-retweet"></i> Reconcile
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
