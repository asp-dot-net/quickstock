﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_SwipeJobReport : System.Web.UI.Page
{
    static int MaxAttribute = 1;
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            //ddlSelectRecords.DataBind();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //BindProjectNumber();

            BindGrid(0);
            PanGrid.Visible = false;
            DivSubmit.Visible = true;
            btnSubmit.Visible = true;

            //divnopage.Visible = false;
        }
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        DataTable dt = new DataTable();
        //DataTable dt = Reports.InstallerwiseReport_QuickStock(txtProjectNumber.Text, txtserailno.Text, txtstockitemfilter.Text, ddllocationsearch.SelectedValue, ddlInstaller.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Isverify, ddlprojectwise.SelectedValue, Location);
        //try
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        #region 
        //        //
        //        for (int i = 0; i < GridView1.Rows.Count; i++)
        //        {

        //            HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
        //            Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
        //            Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
        //            Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevert");
        //            Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
        //            Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblInverterrevert");
        //            Label lblpaneninstall = (Label)GridView1.Rows[i].FindControl("Label52");
        //            Label lblinverterIntall = (Label)GridView1.Rows[i].FindControl("Label752");
        //            Label lblpanelOut = (Label)GridView1.Rows[i].FindControl("Label452");
        //            Label lblinverterOut = (Label)GridView1.Rows[i].FindControl("Label4522");
        //            //LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
        //            LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");
        //            LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");


        //            //if (i != dt.Rows.Count - 1)
        //            //{
        //            //    if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
        //            //    {
        //            //       // gvbtnVerify.Visible = true;
        //            //    }
        //            //    else
        //            //    {
        //            //       // gvbtnVerify.Visible = false;
        //            //    }
        //            //}

        //            DataTable dt1 = null;
        //            dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
        //            if (dt1.Rows.Count > 0)
        //            {
        //                try
        //                {
        //                    if (!string.IsNullOrEmpty(dt1.Rows[0]["VerifynoteIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedateIN"].ToString()))
        //                    {
        //                        // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
        //                        gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
        //                    }
        //                }
        //                catch { }
        //            }
        //        }

        //        //Total Grid bind
        //        int PanelStockDeducted = 0;
        //        int SaleQtyPanel = 0;
        //        int InverterStockDeducted = 0;
        //        int SaleQtyInverter = 0;
        //        int paneltotdiff = 0;
        //        int paneltotrevert = 0;
        //        int invertertotdiff = 0;
        //        int invertertotrevert = 0;
        //        int totPanelStockDeducted = 0;
        //        int totSaleQtyPanel = 0;
        //        int totInverterStockDeducted = 0;
        //        int totSaleQtyInverter = 0;


        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
        //            SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
        //            InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
        //            SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

        //            paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
        //            paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
        //            invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
        //            invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

        //            totPanelStockDeducted += PanelStockDeducted;
        //            totSaleQtyPanel += SaleQtyPanel;
        //            totInverterStockDeducted += InverterStockDeducted;
        //            totSaleQtyInverter += SaleQtyInverter;
        //        }
        //        lblPDifference.Text = paneltotdiff.ToString();
        //        lblPRevert.Text = paneltotrevert.ToString();
        //        lblIDifference.Text = invertertotdiff.ToString();
        //        lblIRevert.Text = invertertotrevert.ToString();

        //        lblpout.Text = totPanelStockDeducted.ToString();
        //        lblPInstalled.Text = totSaleQtyPanel.ToString();
        //        lblIOut.Text = totInverterStockDeducted.ToString();
        //        lblIInstalled.Text = totSaleQtyInverter.ToString();


        //        //lbltotpanel.Text = Convert.ToString(totpanel);
        //        //totpanel = paneltotdiff - paneltotrevert;

        //        //totinverter = invertertotdiff - invertertotrevert;
        //        //lbltotInverter.Text = Convert.ToString(totinverter);
        //        #endregion
        //    }
        //}
        //catch { }
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        //DataTable dt = new DataTable();
        //dt = GetGridData1();


        //dv = new DataView(dt);

        //if (dt.Rows.Count == 0)
        //{
        //    if (deleteFlag == 1)
        //    {
        //        //SetDelete();
        //    }
        //    else
        //    {
        //        SetNoRecords();
        //        //PanNoRecord.Visible = true;
        //    }
        //    PanGrid.Visible = false;
        //    divnopage.Visible = false;
        //}
        //else
        //{
        //    PanGrid.Visible = true;
        //    GridView1.DataSource = dt;
        //    GridView1.DataBind();
        //    if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
        //    {
        //        if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
        //        {
        //            //========label Hide
        //            divnopage.Visible = false;
        //        }
        //        else
        //        {
        //            divnopage.Visible = true;
        //            int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
        //            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //            if (iEndRecord > iTotalRecords)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            if (iStartsRecods == 0)
        //            {
        //                iStartsRecods = 1;
        //            }
        //            if (iEndRecord == 0)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //        }
        //    }
        //    else
        //    {
        //        if (ddlSelectRecords.SelectedValue == "All")
        //        {
        //            divnopage.Visible = true;
        //            ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
        //        }
        //    }

        //}
        // bind();

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string[] FirProjPickId = txtProjectNoFir.Text.Split('/');
        string[] SecProjPickId = txtProjectNoSec.Text.Split('/');

        if (txtProjectNoFir.Text != txtProjectNoSec.Text)
        {
            int Firexists = Reports.SptblProject_ExistsNumber(FirProjPickId[0].ToString());
            int Secexists = Reports.SptblProject_ExistsNumber(SecProjPickId[0].ToString());

            //Project to Project
            if (Firexists == 1 && Secexists == 1) // Project to Project
            {
                //Notification("Project Project");

                //Project
                DataTable dtFirst = Reports.SwipeJobReport_QuickStock_GetDataByProjPickNumber(FirProjPickId[0].ToString(), FirProjPickId[1].ToString());
                DataTable dtSecond = Reports.SwipeJobReport_QuickStock_GetDataByProjPickNumber(SecProjPickId[0].ToString(), SecProjPickId[1].ToString());

                int MatchFlag = 0;
                //string pickid = "";
                //string picklistid = "";
                if (dtFirst.Rows.Count > 0 && dtSecond.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFirst.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtSecond.Rows.Count; j++)
                        {
                            if (dtFirst.Rows[i]["StockItemID"].ToString() == dtSecond.Rows[j]["StockItemID"].ToString()
                                && dtFirst.Rows[i]["OrderQuantity"].ToString() == dtSecond.Rows[j]["OrderQuantity"].ToString())
                            {
                                MatchFlag++;
                                //pickid += dtFirst.Rows[i]["PickId"].ToString();
                                //picklistid = dtFirst.Rows[i]["PicklistitemId"].ToString();
                            }

                        }
                    }
                }

                if (MatchFlag == dtFirst.Rows.Count && dtFirst.Rows.Count > 0)
                {
                    DataTable dt = Reports.SwipeJobReport_QuickStock_tblMaintainHistoryDeduct(dtFirst.Rows[0]["PickId"].ToString(), "Picklist Out");
                    string section = "";
                    string Message = "";
                    DateTime date = DateTime.Now;
                    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                    if (dt.Rows.Count > 0)
                    {
                        string projectid = dtFirst.Rows[0]["ProjectID"].ToString();
                        string PicklistId = dtFirst.Rows[0]["PickId"].ToString();
                        //string LocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();
                        //ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                        string DedectProjectId = dtSecond.Rows[0]["ProjectID"].ToString();
                        string DedectPicklistId = dtSecond.Rows[0]["PickId"].ToString();
                        //string DedectLocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();

                        DataTable dtSerialNo = Reports.SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByPickID(PicklistId);

                        if (dtSerialNo.Rows.Count > 0)
                        {
                            //Revert In First ProjectNumber
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Revert Item";
                                Message = "Stock Revert For Project No:" + FirProjPickId[0].ToString() + "& PicklistId:" + PicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                Reports.SwipeJobReport_QuickStock_Updatetbl_PickListLog(FirProjPickId[1].ToString(), "", "", "0");

                            }
                            //Deduct In First SecondNumber
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Stock Deduct";
                                Message = "Stock Deduct For Project No:" + SecProjPickId[0].ToString() + "& PicklistId:" + DedectPicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, DedectPicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(SecProjPickId[0].ToString(), DedectPicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString());
                                //ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId("111", "111", dtSerialNo.Rows[i]["SerialNo"].ToString());
                            }
                        }


                        //ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                        //ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                        SetAdd1();
                        //Notification("");
                    }
                    else
                    {
                        Notification("Stock item does not match");
                        txtProjectNoSec.Text = string.Empty;
                    }
                }
                else
                {
                    Notification("Stock item does not match");
                    txtProjectNoSec.Text = string.Empty;
                }
            }

            //Invoice to Invoice
            if (Firexists == 0 && Secexists == 0)//Invoice to Invoice
            {
                //Notification("Invoice Invoice");

                DataTable dtFirst = Reports.SwipeJobReport_QuickStock_GetDataByInvoiceNo(FirProjPickId[0].ToString(), FirProjPickId[1].ToString());
                DataTable dtSecond = Reports.SwipeJobReport_QuickStock_GetDataByInvoiceNo(SecProjPickId[0].ToString(), SecProjPickId[1].ToString());

                int MatchFlag = 0;
                if (dtFirst.Rows.Count > 0 && dtSecond.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFirst.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtSecond.Rows.Count; j++)
                        {
                            if (dtFirst.Rows[i]["StockItemID"].ToString() == dtSecond.Rows[j]["StockItemID"].ToString()
                                && dtFirst.Rows[i]["OrderQuantity"].ToString() == dtSecond.Rows[j]["OrderQuantity"].ToString())
                            {
                                MatchFlag++;
                                //pickid += dtFirst.Rows[i]["PickId"].ToString();
                                //picklistid = dtFirst.Rows[i]["PicklistitemId"].ToString();
                            }
                        }
                    }
                }

                if (MatchFlag == dtFirst.Rows.Count && dtFirst.Rows.Count > 0)
                {
                    DataTable dt = Reports.SwipeJobReport_QuickStock_tblMaintainHistoryDeduct(dtFirst.Rows[0]["WholesaleOrderID"].ToString(), "WholeSale Out");
                    string section = "";
                    string Message = "";
                    DateTime date = DateTime.Now;
                    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                    if (dt.Rows.Count > 0)
                    {
                        string InvoiceNo = dtFirst.Rows[0]["InvoiceNo"].ToString();
                        string WholesaleOrderID = dtFirst.Rows[0]["WholesaleOrderID"].ToString();
                        //string LocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();
                        //ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                        string DedectInvoiceNo = dtSecond.Rows[0]["InvoiceNo"].ToString();
                        string DedectWholesaleOrderID = dtSecond.Rows[0]["WholesaleOrderID"].ToString();
                        //string DedectLocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();

                        DataTable dtSerialNo = Reports.SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByWholesaleOrderId(WholesaleOrderID);

                        if (dtSerialNo.Rows.Count > 0)
                        {
                            //Revert In First Invoice
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Revert Item";
                                Message = "Wholesale revert for Order Number" + FirProjPickId[1].ToString() + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "1", userid, date.ToString());
                            }

                            //Deduct In Second Invoice
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Stock Deduct";
                                Message = "Stock Deduct For WholesaleOrderID:" + SecProjPickId[1].ToString() + " By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, DedectWholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(DedectWholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString());
                            }
                        }



                        SetAdd1();
                        //Notification("");
                    }
                    else
                    {
                        Notification("Stock item does not match");
                        txtProjectNoSec.Text = string.Empty;
                    }
                }
                else
                {
                    Notification("Stock item does not match");
                    txtProjectNoSec.Text = string.Empty;
                }
            }

            //Project to Invoice
            if (Firexists == 1 && Secexists == 0)//Project to Invoice
            {
                //Notification("Project to Invoice");
                DataTable dtFirst = Reports.SwipeJobReport_QuickStock_GetDataByProjPickNumber(FirProjPickId[0].ToString(), FirProjPickId[1].ToString());
                DataTable dtSecond = Reports.SwipeJobReport_QuickStock_GetDataByInvoiceNo(SecProjPickId[0].ToString(), SecProjPickId[1].ToString());

                int MatchFlag = 0;
                if (dtFirst.Rows.Count > 0 && dtSecond.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFirst.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtSecond.Rows.Count; j++)
                        {
                            if (dtFirst.Rows[i]["StockItemID"].ToString() == dtSecond.Rows[j]["StockItemID"].ToString()
                                && dtFirst.Rows[i]["OrderQuantity"].ToString() == dtSecond.Rows[j]["OrderQuantity"].ToString())
                            {
                                MatchFlag++;
                                //pickid += dtFirst.Rows[i]["PickId"].ToString();
                                //picklistid = dtFirst.Rows[i]["PicklistitemId"].ToString();
                            }

                        }
                    }
                }

                if (MatchFlag == dtFirst.Rows.Count && dtFirst.Rows.Count > 0)
                {
                    DataTable dt = Reports.SwipeJobReport_QuickStock_tblMaintainHistoryDeduct(dtFirst.Rows[0]["PickId"].ToString(), "Picklist Out");
                    string section = "";
                    string Message = "";
                    DateTime date = DateTime.Now;
                    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                    if (dt.Rows.Count > 0)
                    {
                        string projectid = dtFirst.Rows[0]["ProjectID"].ToString();
                        string PicklistId = dtFirst.Rows[0]["PickId"].ToString();
                        //string LocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();
                        //ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                        string DedectInvoiceNo = dtSecond.Rows[0]["InvoiceNo"].ToString();
                        string DedectWholesaleOrderID = dtSecond.Rows[0]["WholesaleOrderID"].ToString();
                        //string DedectLocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();;

                        DataTable dtSerialNo = Reports.SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByPickID(PicklistId);

                        if (dtSerialNo.Rows.Count > 0)
                        {
                            //Revert In First Project Number
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Revert Item";
                                Message = "Stock Revert For Project No:" + FirProjPickId[0].ToString() + "& PicklistId:" + PicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                Reports.SwipeJobReport_QuickStock_Updatetbl_PickListLog(FirProjPickId[1].ToString(), "", "", "0");
                            }

                            //Deduct In Second Invoice
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Stock Deduct";
                                Message = "Stock Deduct For WholesaleOrderID:" + SecProjPickId[1].ToString() + " By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, DedectWholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(DedectWholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString());
                            }
                        }


                        //ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                        //ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                        SetAdd1();
                        //Notification("");
                    }
                    else
                    {
                        Notification("Stock item does not match");
                        txtProjectNoSec.Text = string.Empty;
                    }
                }
                else
                {
                    Notification("Stock item does not match");
                    txtProjectNoSec.Text = string.Empty;
                }
            }

            //Invoice to Project
            if (Firexists == 0 && Secexists == 1)
            {
                //Notification("Invoice to Project");
                DataTable dtFirst = Reports.SwipeJobReport_QuickStock_GetDataByInvoiceNo(FirProjPickId[0].ToString(), FirProjPickId[1].ToString());
                DataTable dtSecond = Reports.SwipeJobReport_QuickStock_GetDataByProjPickNumber(SecProjPickId[0].ToString(), SecProjPickId[1].ToString());

                int MatchFlag = 0;
                if (dtFirst.Rows.Count > 0 && dtSecond.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFirst.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtSecond.Rows.Count; j++)
                        {
                            if (dtFirst.Rows[i]["StockItemID"].ToString() == dtSecond.Rows[j]["StockItemID"].ToString()
                                && dtFirst.Rows[i]["OrderQuantity"].ToString() == dtSecond.Rows[j]["OrderQuantity"].ToString())
                            {
                                MatchFlag++;
                                //pickid += dtFirst.Rows[i]["PickId"].ToString();
                                //picklistid = dtFirst.Rows[i]["PicklistitemId"].ToString();
                            }
                        }
                    }
                }

                if (MatchFlag == dtFirst.Rows.Count && dtFirst.Rows.Count > 0)
                {
                    DataTable dt = Reports.SwipeJobReport_QuickStock_tblMaintainHistoryDeduct(dtFirst.Rows[0]["WholesaleOrderID"].ToString(), "WholeSale Out");
                    string section = "";
                    string Message = "";
                    DateTime date = DateTime.Now;
                    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                    if (dt.Rows.Count > 0)
                    {
                        string InvoiceNo = dtFirst.Rows[0]["InvoiceNo"].ToString();
                        string WholesaleOrderID = dtFirst.Rows[0]["WholesaleOrderID"].ToString();
                        //string LocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();
                        //ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                        string DedectProjectId = dtSecond.Rows[0]["ProjectID"].ToString();
                        string DedectPicklistId = dtSecond.Rows[0]["PickId"].ToString();
                        //string DedectLocationId = dtFirst.Rows[0]["CompanyLocationId"].ToString();

                        DataTable dtSerialNo = Reports.SwipeJobReport_QuickStock_tblStockSerialNo_GetSerialNoByWholesaleOrderId(WholesaleOrderID);

                        if (dtSerialNo.Rows.Count > 0)
                        {
                            //Revert In First Invoice
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Revert Item";
                                Message = "Wholesale revert for Order Number" + FirProjPickId[1].ToString() + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "1", userid, date.ToString());
                            }

                            //Deduct In Second Project
                            for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                            {
                                section = "Stock Deduct";
                                Message = "Stock Deduct For Project No:" + SecProjPickId[0].ToString() + "& PicklistId:" + DedectPicklistId + "By administrator";
                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, DedectPicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString(), section, Message, date);
                                ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(SecProjPickId[0].ToString(), DedectPicklistId, dtSerialNo.Rows[i]["SerialNo"].ToString());
                            }
                        }



                        SetAdd1();
                        //Notification("");
                    }
                    else
                    {
                        Notification("Stock item does not match");
                        txtProjectNoSec.Text = string.Empty;
                    }
                }
                else
                {
                    Notification("Stock item does not match");
                    txtProjectNoSec.Text = string.Empty;
                }
            }
        }
        else
        {
            Notification("Please select different number");
            txtProjectNoSec.Text = string.Empty;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        //ddlProjectNumber.SelectedValue = "";
        //ddlProjectNumber2.SelectedValue = "";
        //txtProjectFirst.Text = string.Empty;
        //txtProjectSecond.Text = string.Empty;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label452");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label52");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label4522");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label752");

            //HiddenField hndprojectnumber = (HiddenField)e.Row.FindControl("hndProjectID");
            //Label lblpaneldiff = (Label)e.Row.FindControl("Label82");
            //Label lblpanelrevrt = (Label)e.Row.FindControl("lblpanelrevert");
            //Label lblinverterdiff = (Label)e.Row.FindControl("Label152");
            //Label lblinverterrevert = (Label)e.Row.FindControl("lblInverterrevert");
            //Label lblProjectNumber = (Label)e.Row.FindControl("Label11");            
            //// LinkButton gvbtnVerify = (LinkButton)e.Row.FindControl("gvbtnVerify");
            //LinkButton gvbtnView = (LinkButton)e.Row.FindControl("gvbtnView");
            //LinkButton btnviewrevert1 = (LinkButton)e.Row.FindControl("btnviewrevert1");
            //LinkButton btnviewrevert2 = (LinkButton)e.Row.FindControl("btnviewrevert2");
            //LinkButton gvbnNote = (LinkButton)e.Row.FindControl("gvbnNote");            
            //LinkButton gvbnEmail = (LinkButton)e.Row.FindControl("gvbnEmail");            
            //Label lbl = (Label)e.Row.FindControl("Label82");
            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            //if (string.IsNullOrEmpty(hndprojectnumber.Value))
            //{
            //    DataTable dt = new DataTable();
            //    dt = GetGridData1();
            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;


            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;



            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblpanelrevrt.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblinverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            // gvbtnVerify.Visible = false;
            //            gvbtnView.Visible = false;
            //            btnviewrevert1.Visible = false;
            //            btnviewrevert2.Visible = false;
            //            gvbnNote.Visible = false;
            //            imgdiv.Visible = false;
            //            gvbnEmail.Visible = false;

            //            lbltotpanel.Text = Convert.ToString(totpanel);
            //            totpanel = paneltotdiff - paneltotrevert;

            //            totinverter = invertertotdiff - invertertotrevert;
            //            lbltotInverter.Text = Convert.ToString(totinverter);
            //        }
            //    }

            //    if (string.IsNullOrEmpty(hndprojectnumber.Value))
            //    {
            //        lblProjectNumber.Text = "";
            //    }                
            //}            
        }
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

}