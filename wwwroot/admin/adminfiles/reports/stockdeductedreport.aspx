<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="stockdeductedreport.aspx.cs" Inherits="admin_adminfiles_stock_stockdeductedreport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script src="<%=Siteurl%>admin/theme/assets/js/select2.js"></script>

    <script>
//        $(document).ready(function() {
//    $('.js-example-basic-multiple').select2();
//});
    </script>
    <script type="text/javascript">

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            //$(".dropdown dt a").on('click', function () {
            //    $(".dropdown dd ul").slideToggle('fast');
            //});

            //$(".dropdown dd ul li a").on('click', function () {
            //    $(".dropdown dd ul").hide();
            //});
            ////callMultiCheckbox();

            //$(document).bind('click', function (e) {
            //    var $clicked = $(e.target);
            //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            //});


        }
        function pageLoaded() {

            callMultiCheckbox1();
            callMultiCheckbox2();
            callMultiCheckbox3();
            callMultiCheckbox4();
            callMultiCheckbox5();

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            //callMultiCheckbox();



            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //  callMultiCheckbox();


            // New Added for MultiSelect //

            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });


            $(".myval1").select2({
                minimumResultsForSearch: -1
            });

            $(".myval").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }


            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });


            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox2();
            });

            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox3();
            });

            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox4();
            });

            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox5();
            });

        }
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddlLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function callMultiCheckbox2() {
            var title = "";
            $("#<%=ddlProjectStats.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel2').show();
                $('.multiSel2').html(html);
                $(".hida2").hide();
                console.log(html);
            }
            else {
                $('#spanselect2').show();
                $('.multiSel2').hide();
            }

        }

        function callMultiCheckbox3() {
            var title = "";
            $("#<%=ddlAInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel3').show();
                $('.multiSel3').html(html);
                $(".hida3").hide();
                console.log(html);
            }
            else {
                $('#spanselect3').show();
                $('.multiSel3').hide();
            }

        }

        function callMultiCheckbox4() {
            var title = "";
            $("#<%=ddlSMInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel4').show();
                $('.multiSel4').html(html);
                $(".hida4").hide();
                console.log(html);
            }
            else {
                $('#spanselect4').show();
                $('.multiSel4').hide();
            }

        }
        function callMultiCheckbox5() {
            var title = "";
            $("#<%=ddWCustomer.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel5').show();
                $('.multiSel5').html(html);
                $(".hida5").hide();
                console.log(html);
            }
            else {
                $('#spanselect5').show();
                $('.multiSel5').hide();
            }

        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="Repeater1" />
            <asp:PostBackTrigger ControlID="Repeater2" />
            <%--            <asp:PostBackTrigger ControlID="lnkwholeSaleSubmit_Click" />--%>
        </Triggers>
    </asp:UpdatePanel>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            document.getElementById('loader_div').style.visibility = "visible";
            //callMultiCheckbox();


        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress



            //$(".dropdown dt a").on('click', function () {
            //    $(".dropdown dd ul").slideToggle('fast');
            //});

            //$(".dropdown dd ul li a").on('click', function () {
            //    $(".dropdown dd ul").hide();
            //});
            //callMultiCheckbox();

            //$(document).bind('click', function (e) {
            //    var $clicked = $(e.target);
            //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            //});


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));

            document.getElementById('loader_div').style.visibility = "hidden";

            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();



            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });


        }
    </script>

    <div class="page-header card">
        <div class="card-block">
            <h5>Daily Stock Deduct Report
                <div class="pull-right">
                            
                            <asp:Button Text="Fetch SM Data" ID="btnUpdateSMData" runat="server" CssClass="btn btn-warning" OnClick="btnUpdateSMData_Click" />
                        </div>
            </h5>


        </div>
    </div>

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="Panel4">
            <div class="animate-panel">
                <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                    </div>
                    <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                            Text="Transaction Failed."></asp:Label></strong>
                    </div>
                    <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                    </div>
                    <div class="alert alert-info" id="Div16" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>

                <div class="searchfinal">
                    <div class="card shadownone brdrgray pad10">
                        <div class="card-block">
                            <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                <div class="inlineblock martop5">
                                    <div class="row">
                                        <div class="input-group col-sm-2 martop5 max_width170">
                                            <asp:TextBox ID="txtProjectNumber" placeholder="Project Number/Invoice No" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                WatermarkText="Project Number" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers, Custom"
                                                ValidChars="," />
                                        </div>
                                        <div class="input-group col-sm-2 martop5 max_width170" id="div8" runat="server">
                                            <%--<asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select Item</asp:ListItem>
                                                    </asp:DropDownList>--%>
                                            <asp:TextBox ID="txtStockItem" placeholder="Stock Item" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                        </div>
                                        <div class="input-group col-sm-2 martop5 max_width170" id="div4" runat="server">
                                            <asp:DropDownList ID="ddlisdeduct" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval" OnSelectedIndexChanged="ddlisdeduct_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                                                <asp:ListItem Value="1"> Arise Project </asp:ListItem>
                                                <asp:ListItem Value="3">Solar Miner Project</asp:ListItem>
                                                <asp:ListItem Value="2">Wholesale</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="input-group col-sm-2 martop5 max_width170" id="divCustomer" style="display: none;" runat="server">
                                            <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                <asp:ListItem Value="">Location</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="input-group col-sm-2 martop5 max_width170" id="div11" runat="server" style="display: none;">
                                            <asp:ListBox ID="lstlocationsearch" runat="server" ToolTip="Location" CausesValidation="false" SelectionMode="Multiple" CssClass="js-example-basic-multiple" Width="200px">
                                                <%-- <asp:ListItem Value="" Selected="True">Project Status</asp:ListItem>--%>
                                            </asp:ListBox>
                                        </div>

                                        <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                            <dl class="dropdown">
                                                <dt>
                                                    <a href="#">
                                                        <span class="hida" id="spanselect">Location</span>
                                                        <p class="multiSel"></p>
                                                    </a>
                                                </dt>
                                                <dd id="ddlLocation" runat="server">
                                                    <div class="mutliSelect" id="mutliSelect">
                                                        <ul>
                                                            <asp:Repeater ID="lstSearchLocation" runat="server" OnItemDataBound="lstSearchLocation_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                        <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                            <span></span>
                                                                        </label>
                                                                        <%-- </span>--%>
                                                                        <label class="chkval">
                                                                            <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                        </label>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>


                                        <div class="input-group col-sm-2 martop5 max_width170" style="display: none;">
                                            <asp:TextBox ID="txtSerachCity" placeholder="City" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                WatermarkText="City" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetOnlyCityiesList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                            </cc1:AutoCompleteExtender>
                                        </div>

                                        <div class="input-group col-sm-1 martop5 max_width170" id="div1" runat="server" style="display: none;">
                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">State</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>


                                        <div class="input-group col-sm-2 martop5 max_width170" style="display: none;">
                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Project Name" CssClass="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                WatermarkText="Project Name" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                        </div>



                                        <div class="input-group col-sm-2 martop5 max_width170" id="div9" runat="server" style="display: none;">
                                            <asp:DropDownList ID="ddlprojectstatus" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Project Status</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>


                                        <div class="input-group col-sm-2 martop5 max_width170" id="div10" runat="server" style="display: none;">
                                            <asp:ListBox ID="lstProjectStatus" runat="server" ToolTip="Project Stutus" CausesValidation="false" SelectionMode="Multiple" CssClass="js-example-basic-multiple" Width="200px">
                                                <%-- <asp:ListItem Value="" Selected="True">Project Status</asp:ListItem>--%>
                                            </asp:ListBox>
                                        </div>

                                        <div class="form-group spical multiselect projectstatus martop5 col-sm-1 max_width200" id="DivProjectStutus" runat="server" visible="false">
                                            <dl class="dropdown ">
                                                <dt>
                                                    <a href="#">
                                                        <span class="hida2" id="spanselect2">Select Project Status</span>
                                                        <p class="multiSel2"></p>
                                                    </a>
                                                </dt>
                                                <dd id="ddlProjectStats" runat="server">
                                                    <div class="mutliSelect" id="mutliSelect">
                                                        <ul>
                                                            <asp:Repeater ID="lstProjecSts" runat="server" OnItemDataBound="lstProjecSts_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("ProjectStatus") %>' />
                                                                        <asp:HiddenField ID="hdnProjectStatusID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />


                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                            <span></span>
                                                                        </label>
                                                                        <%-- </span>--%>
                                                                        <label class="chkval">
                                                                            <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                        </label>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>


                                        <div class="input-group col-sm-2 martop5 max_width170" id="div3" runat="server">
                                            <asp:DropDownList ID="ddlDeductedOrNot" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="False">Not Deducted</asp:ListItem>
                                                <asp:ListItem Value="True" Selected="True">Deducted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        
                                        
                                        <div class="form-group spical multiselect ariseInstaller martop5 col-sm-2 max_width200" id="DivAInstaller" runat="server" visible="false">
                                            <dl class="dropdown ">
                                                <dt>
                                                    <a href="#">
                                                        <span class="hida3" id="spanselect3">Installer</span>
                                                        <p class="multiSel3"></p>
                                                    </a>
                                                </dt>
                                                <dd id="ddlAInstaller" runat="server">
                                                    <div class="mutliSelect" id="mutliSelect">
                                                        <ul>
                                                            <asp:Repeater ID="rptAInstaller" runat="server">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HiddenField ID="hdnAInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                        <asp:HiddenField ID="hdnAInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                            <span></span>
                                                                        </label>
                                                                        <%-- </span>--%>
                                                                        <label class="chkval">
                                                                            <asp:Literal runat="server" ID="ltAInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                        </label>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="form-group spical multiselect SmInstaller martop5 col-sm-2 max_width200" id="DivSMInstaller" runat="server" visible="false">
                                                    <dl class="dropdown ">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida4" id="spanselect4">Installer</span>
                                                                <p class="multiSel4"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddlSMInstaller" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptSMInstaller" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnSMInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                                <asp:HiddenField ID="hdnSMInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltSMInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                        
                                        <div class="form-group spical multiselect WCustomer martop5 col-sm-2 max_width200" id="DivWCustomer" runat="server" visible="false">
                                                    <dl class="dropdown ">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida5" id="spanselect5">Customer</span>
                                                                <p class="multiSel5"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddWCustomer" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptWCustomer" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnWCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                <asp:HiddenField ID="hdnWCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltWCustomer" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                        <div class="input-group col-sm-2 martop5 max_width170" id="div2" runat="server">
                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">InstallBookingDate</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">Deducted date</asp:ListItem>
                                                <%--<asp:ListItem Value="3">Picklist Date</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-group col-sm-2 martop5 max_width170" id="div7" runat="server" style="display: none;">
                                            <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                AutoPostBack="true" CausesValidation="false">
                                                <asp:ListItem Value="">Select Category</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                    ControlToValidate="txtStartDate" ErrorMessage="* Required" CssClass="errormessage"></asp:RequiredFieldValidator>--%>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            </div>
                                       
                                        <div class="input-group martop5 col-sm-1 max_width170">
                                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                            <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                        </div>
                                        <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                            <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                        </div>
                                    </div>
                            </asp:Panel>

                            <div class="datashowbox inlineblock">
                                <div class="row">

                                    <div class="input-group col-sm-2 martop5 max_width170">
                                        <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                            CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>
    </div>

    <div class="finalgrid">
        <asp:Panel ID="panel" runat="server">
            <div class="page-header card" id="divtot" runat="server">
                <div class="card-block brd_ornge">
                    <div class="printorder" style="font-size: medium">
                        <b>Total Number of panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                        &nbsp;&nbsp
                                <b>Total Number of Inverters:&nbsp;</b><asp:Literal ID="lblTotalInverters" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
            <div>
                <div id="PanGrid" runat="server">
                    <div class="card shadownone brdrgray">
                        <div class="card-block">
                            <div class="table-responsive BlockStructure">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" OnRowDataBound="GridView1_RowDataBound"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                    OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Project No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber" HeaderStyle-CssClass="brdrgrayleft">
                                            <ItemTemplate>
                                                <%--<asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />--%>
                                                <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("PRID")%>' />
                                                <asp:HiddenField ID="hndprojectStatusID" runat="server" Value='<%#Eval("projectStatusID")%>' />
                                                <asp:HiddenField ID="hdnCmpLocation" runat="server" Value='<%#Eval("StockAllocationStoreName")%>' />


                                                <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>

                                                <%--<asp:Label ID="Label11" runat="server" Width="50px" Text='<%#Eval("ProjectNumber")%>'>
                                                        </asp:Label>--%>

                                                <asp:Label ID="lblProjectNumber" runat="server" Width="100px" Text='<%#Eval("ProjectNumber") + " / " + Eval("ProjectID") %>'>
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompName" runat="server" Width="250px" Text='<%#Eval("CompName")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject" runat="server" Width="250px" Text='<%#Eval("Project")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblProjectStatus" runat="server" Width="150px">
                                                                                                 

                                                </asp:Label>--%>
                                                <asp:Label ID="Label1" runat="server" Width="150px">
                                                                                                <%# Eval("ProjectStatus")%>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="InstallBookingDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="InstallBookingDate1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallBookingDate1" runat="server" Width="100px" Text='<%#Eval("InstallBookingDate1","{0:dd MMM yyyy}")%>'>
                                                                      <%--  <%#Eval("InstallBookingDate1","{0:dd MMM yyyy}")%>--%>   </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblInstallerName" runat="server" Width="120px">
                                                </asp:Label>--%>
                                                <asp:Label ID="Label2" runat="server" Width="120px">
                                                                      <%# Eval("InstallerName")%>

                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSystemDetails" runat="server" Width="400px" Text='<%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%>'>
                                                                     <%--  <%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%>--%>
                                                             <%-- <%#Eval("SaleQtyPanel")+"X"+Eval("PanemNm")+"+"+Eval("SaleQtyInverter")+"X"+Eval("InvName") %>--%>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockAllocationStoreName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStockAllocationStoreName" runat="server" Width="200px" Text='<%#Eval("StockAllocationStoreName")%>'>
                                                                       
                                                          <%--  <%#Eval("StockAllocationStoreName")%>--%>   </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="PanelStockDeducted">
                                            <ItemTemplate>
                                                <div style="width: 113px;">
                                                    <asp:Label ID="lblpanelQty" runat="server" Width="26px" Text='<%#Eval("PanelStockDeducted")%>' CssClass="paddtop5">
                                                    </asp:Label>

                                                    <%--Previous Not Working Button  --%>
                                                    <%--<asp:LinkButton ID="btnviewrevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel"
                                                            CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ProjectID")%>'
                                                            CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-eye"></i> P. Revert
                                                        </asp:LinkButton>--%>


                                                    <asp:LinkButton ID="btnviewrevert1" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertpanel"
                                                        CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("PRID")%>'
                                                        CausesValidation="false" data-original-title="Panel Reverted" data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-eye"></i> P. Revert
                                                    </asp:LinkButton>


                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Invertes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InverterStockDeducted">
                                            <ItemTemplate>
                                                <div style="width: 113px;">
                                                    <asp:Label ID="lblinverterQty" runat="server" Width="26px" Text='<%#Eval("InverterStockDeducted")%>' CssClass="paddtop5">
                                                    </asp:Label>
                                                    <%--    Previous Not Working code for Pop up
                                                             <asp:LinkButton ID="btnviewrevert2" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ProjectID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-eye"></i> I. Revert
                                                            </asp:LinkButton>--%>

                                                    <asp:LinkButton ID="btnviewrevert2" runat="server" Style="float: right;" CssClass="btn btn-success btn-mini" CommandName="viewrevertinverter" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("PRID")%>' CausesValidation="false" data-original-title="Inverter Reverted" data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-eye"></i> I. Revert
                                                    </asp:LinkButton>

                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext brdrgrayright" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="Label17" runat="server" Width="100px">
                                                    <asp:LinkButton ID="lbtnDeduct" CommandName="deduct" CommandArgument='<%#Eval("ProjectID")%>' CausesValidation="false" runat="server">Deduct</asp:LinkButton>
                                                    <asp:Label ID="lbldiv" runat="server" Visible='<%# Eval("IsDeduct").ToString()=="True"?true:false %>' Text=" / "></asp:Label>
                                                    <asp:LinkButton ID="lbtnMove" CommandName="move" CommandArgument='<%#Eval("ProjectID")%>' Visible='<%# Eval("IsDeduct").ToString()=="True"?true:false %>' CausesValidation="false" runat="server">Move</asp:LinkButton>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Deduct" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStockDeductDate" runat="server" Width="100px" Text='<%#String.IsNullOrEmpty( Eval("StockDeductDate").ToString()) ? "" : Eval("StockDeductDate","{0:dd MMM yyyy}")%>'>
                                                                     <%--  <%#String.IsNullOrEmpty( Eval("StockDeductDate").ToString()) ? "" : Eval("StockDeductDate","{0:dd MMM yyyy}")%>--%> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Deduct By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductByName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStockDeductByName" runat="server" Width="100px" Text='<%#Eval("StockDeductByName")%>'> 
                                                                       <%-- <%#Eval("StockDeductByName")%>--%> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>

                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <%--Pop For Prever--%>
    <cc1:ModalPopupExtender ID="ModalPopupExtenderRevert" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="ModelRevert" TargetControlID="btnNULL1"
        CancelControlID="LinkButton6">
    </cc1:ModalPopupExtender>
    <div id="ModelRevert" runat="server" style="display: none; width: 100%" class="modal_popup">
        <div class="modal-dialog" style="max-width: 700px;">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title fullWidth" id="myModalLabel1">Revert Items
                                <span style="float: right" class="printorder" />

                        <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                        </asp:LinkButton>
                    </h5>
                </div>

                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="col-md-12">
                                <div class="qty marbmt25">
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                        <tr>
                                            <th width="10%" align="center">Index No.</th>
                                            <th width="25%" align="center">Serial No.</th>
                                            <th width="15%" align="left">Category</th>
                                            <th align="40%">Stock Item</th>
                                            <th align="10%" align="center">
                                                <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                    <asp:CheckBox ID="chkisactive" runat="server" AutoPostBack="true" OnCheckedChanged="chkisactive_CheckedChanged" />
                                                </div>
                                            </th>
                                        </tr>
                                        <asp:HiddenField runat="server" ID="hndDifference" />
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <tr>

                                                    <%--<asp:HiddenField ID="hndProjectID1" runat="server" Value='<%#Eval("ProjectNo") %>' />--%>
                                                    <td align="left"><%#Container.ItemIndex+1 %></td>
                                                    <td align="left">
                                                        <asp:HiddenField runat="server" ID="hnditemid" Value='<%#Eval("StockItemID")%>' />

                                                        <asp:HiddenField runat="server" ID="rpthndProjectid" Value='<%#Eval("ProjectID")%>' />

                                                        <%--<asp:HiddenField runat="server" ID="rpthndProjectid" Value='<%#Eval("PRID")%>' />--%>

                                                        <asp:HiddenField runat="server" ID="rpthndPicklistId" Value='<%#Eval("PickListid")%>' />
                                                        <asp:HiddenField runat="server" ID="hdnSerialNo" Value='<%#Eval("SerialNo")%>' />
                                                        <asp:HiddenField runat="server" ID="hdnStockLocationID" Value='<%#Eval("CompanyLocationID")%>' />
                                                        <asp:Label ID="lblSerialNo" runat="server" Text='<%#Eval("SerialNo") %>'></asp:Label></td>
                                                    <td align="left">
                                                        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("CategoryName") %>'></asp:Label></td>
                                                    <td align="left">
                                                        <asp:Label ID="lblItem" runat="server" Text='<%#Eval("StockItem") %>'></asp:Label></td>
                                                    <td style="text-align: center;">
                                                        <asp:CheckBox runat="server" ID="chkDifference" OnCheckedChanged="chkDifference_CheckedChanged" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                    <table class="margin20">
                                        <tr>
                                            <%--<td style="padding-right: 5px;">Project Number</td>--%>
                                            <td>
                                                <asp:TextBox ID="txtprojectno" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Project No." OnTextChanged="txtprojectno_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtprojectno"
                                                    WatermarkText="Project No." />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtprojectno" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GEtProjectNumberByPickList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtprojectno" FilterType="Numbers" />
                                            </td>
                                            <td></td>
                                            <%-- <td style="padding-right: 5px;">Invoice Number</td>--%>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control m-b" placeholder="Invoice No." OnTextChanged="TextBox1_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="TextBox1"
                                                    WatermarkText="Invoice No." />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="TextBox1" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetWholesaleID"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="TextBox1" FilterType="Numbers" />
                                            </td>
                                        </tr>
                                    </table>


                                    <div align="center">
                                        <asp:LinkButton ID="lnksubmit" runat="server" Text="Submit" CssClass="btn btn-info"
                                            CausesValidation="false" OnClick="lnksubmit_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkRevert" runat="server" Text="Revert" CssClass="btn btn-info"
                                            CausesValidation="false" OnClick="lnkRevert_Click"></asp:LinkButton>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="WholesaleModelRevert" TargetControlID="btnNULL2"
        CancelControlID="LinkButton8">
    </cc1:ModalPopupExtender>
    <div id="WholesaleModelRevert" runat="server" style="display: none; width: 100%" class="modal_popup">
        <div class="modal-dialog" style="max-width: 700px;">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title fullWidth" id="myModalLabel2">Wholesale Revert Items
                                <span style="float: right" class="printorder" />

                        <asp:LinkButton ID="LinkButton8" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                        </asp:LinkButton>
                    </h5>
                </div>

                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="col-md-12">
                                <div class="qty marbmt25">
                                    <br />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                        <tr>
                                            <th width="10%" align="center">Index No.</th>
                                            <th width="25%" align="center">Serial No.</th>
                                            <th width="15%" align="left">Category</th>
                                            <th align="40%">Stock Item</th>
                                            <th align="10%">
                                                <div class="checkbox-fade fade-in-primary d-" style="margin: 0;">
                                                    <asp:CheckBox ID="chkisactive1" runat="server" AutoPostBack="true" OnCheckedChanged="chkisactive1_CheckedChanged" />
                                                </div>
                                            </th>
                                        </tr>
                                        <asp:UpdatePanel ID="updatepanel" runat="server">
                                            <ContentTemplate>

                                                <asp:HiddenField runat="server" ID="hndDifference1" />
                                                <asp:Repeater ID="Repeater2" runat="server">
                                                    <ItemTemplate>
                                                        <tr>

                                                            <%--<asp:HiddenField ID="hndProjectID1" runat="server" Value='<%#Eval("ProjectID") %>' />--%>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:HiddenField runat="server" ID="rpthndWholesaleOrderId" Value='<%#Eval("WholesaleOrderId")%>' />
                                                                <asp:HiddenField runat="server" ID="hnditemid1" Value='<%#Eval("StockItemID")%>' />
                                                                <asp:HiddenField runat="server" ID="hdnSerialNo" Value='<%#Eval("SerialNo")%>' />
                                                                <asp:HiddenField runat="server" ID="hdnStockLocationID" Value='<%#Eval("StockLocationID")%>' />
                                                                <asp:HiddenField runat="server" ID="hndInvoiceNo" Value='<%#Eval("InvoiceNo")%>' />
                                                                <asp:Label ID="lblSerialNo" runat="server" Text='<%#Eval("SerialNo") %>'></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("CategoryName") %>'></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server" Text='<%#Eval("StockItem") %>'></asp:Label></td>
                                                            <td style="text-align: center;">
                                                                <asp:CheckBox runat="server" ID="chkDifference1" OnCheckedChanged="chkDifference1_CheckedChanged" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </table>
                                    <table class="margin20">
                                        <tr>
                                            <%-- <td style="padding-right: 5px;">Project Number</td>--%>
                                            <td>
                                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Project No." OnTextChanged="TextBox2_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="TextBox2"
                                                    WatermarkText="Project No." />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="TextBox2" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GEtProjectNumberByPickList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="TextBox2" FilterType="Numbers" />
                                            </td>
                                            <td></td>
                                            <%--<td style="padding-right: 5px;">Invoice Number</td>--%>
                                            <td>
                                                <asp:TextBox ID="txtwholesaleprojNo" runat="server" CssClass="form-control m-b" placeholder="Invoice No." OnTextChanged="txtwholesaleprojNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtwholesaleprojNo"
                                                    WatermarkText="Invoice No." />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtwholesaleprojNo" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetWholesaleID"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtwholesaleprojNo" FilterType="Numbers" />
                                            </td>

                                        </tr>
                                    </table>
                                    <div align="center">
                                        <asp:LinkButton ID="lnkwholeSaleSubmit" runat="server" Text="Submit" CssClass="btn btn-info POPupLoader"
                                            CausesValidation="false" OnClick="lnkwholeSaleSubmit_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkWholeSalerevert" runat="server" Text="WholeSaleRevert" CssClass="btn btn-info POPupLoader"
                                            CausesValidation="false" OnClick="lnkWholeSalerevert_Click"></asp:LinkButton>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL2" Style="display: none;" runat="server" />

    <asp:Button ID="btnNULL1" Style="display: none;" runat="server" />


    <script type="text/javascript">

        //$(".dropdown dt a").on('click', function () {
        //    $(".dropdown dd ul").slideToggle('fast');

        //});

        //$(".dropdown dd ul li a").on('click', function () {
        //    $(".dropdown dd ul").hide();
        //});
        //$(document).bind('click', function (e) {
        //    var $clicked = $(e.target);
        //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        //});


        $(document).ready(function () {
            HighlightControlToValidate();

            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //  //  callMultiCheckbox();
            //});


        });

        //function callMultiCheckbox() {


        //}


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });

        $(".selectlocation .dropdown dt a").on('click', function () {
            $(".selectlocation .dropdown dd ul").slideToggle('fast');
        });

        $(".selectlocation .dropdown dd ul li a").on('click', function () {
            $(".selectlocation .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(".projectstatus .dropdown dt a").on('click', function () {
            $(".projectstatus .dropdown dd ul").slideToggle('fast');
        });

        $(".projectstatus .dropdown dd ul li a").on('click', function () {
            $(".projectstatus .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });

        $(".ariseInstaller .dropdown dt a").on('click', function () {
            $(".ariseInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".ariseInstaller .dropdown dd ul li a").on('click', function () {
            $(".ariseInstaller .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });

        $(".SmInstaller .dropdown dt a").on('click', function () {
            $(".SmInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".SmInstaller .dropdown dd ul li a").on('click', function () {
            $(".SmInstaller .dropdown dd ul").hide();
        });

        $(".WCustomer .dropdown dt a").on('click', function () {
            $(".WCustomer .dropdown dd ul").slideToggle('fast');
        });
        $(".WCustomer .dropdown dd ul li a").on('click', function () {
            $(".WCustomer .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });

    </script>
</asp:Content>
