﻿using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class admin_adminfiles_ReportsV2_SerailInstallerReport : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlCompany.SelectedValue = "1";
            BindCheckboxProjectTab();
            BindDropDown();

            ddlCategory.SelectedValue = "1";

            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;

            BindFetchDate();

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2021";
            //txtEndDate.Text = "20/06/2021";
            //ddlInstaller.SelectedValue = "767415"; //Customer ID // Arise-SolarMiner
            
            ddlDateType.SelectedValue = "2";
            //txtStartDate.Text = "01/01/2021";
            //txtEndDate.Text = "20/06/2021";
            ddlInstaller.SelectedValue = "531124"; //Customer ID // Arise-SolarMiner
            BindGrid(0);
        }
    }

    public string GetProjectStatus()
    {
        string ProjectStatus = "";
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                ProjectStatus += "," + hdnModule.Value.ToString();
            }
        }
        if (ProjectStatus != "")
        {
            ProjectStatus = ProjectStatus.Substring(1);
        }

        return ProjectStatus;
    }

    public string GetLocation()
    {
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }

        return Location;
    }

    protected DataTable GetGridData1()
    {
        string Location = GetLocation();

        string ProjectStatus = GetProjectStatus();

        //DataTable dt = ClsReportsV2.SP_StockPendingReport_InstallerWiseV2(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, txtStockItem.Text, ddlCategory.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ProjectStatus, ddlIsDifference.SelectedValue, ddlAudit.SelectedValue, ddlInstallation.SelectedValue);

        DataTable dt = ClsReportsV2.SP_SerialInstallerReportV2(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ProjectStatus, ddlIsDifference.SelectedValue, ddlAudit.SelectedValue, ddlCategory.SelectedValue, ddlPicklistCreatedBy.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(Out)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(BSGB)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int ScanNo = Convert.ToInt32(dt.Compute("SUM(ScanNo)", string.Empty));
            int NoTraceDiff = Convert.ToInt32(dt.Compute("SUM(NoTraceDiff)", string.Empty));
            int OtherInventory = Convert.ToInt32(dt.Compute("SUM(OtherInventory)", string.Empty));
            int Credit = Convert.ToInt32(dt.Compute("SUM(Credit)", string.Empty));
            int Wholesale = Convert.ToInt32(dt.Compute("SUM(Wholesale)", string.Empty));

            HypOut.Text = PanelStockDeducted.ToString();
            HypInstalled.Text = SaleQtyPanel.ToString();
            //HypDifference.Text = PanelDiff.ToString();
            HypScanNo.Text = ScanNo.ToString();
            HypNoTraceDiff.Text = NoTraceDiff.ToString();
            HypOtherInventory.Text = OtherInventory.ToString();
            HypCredit.Text = Credit.ToString();
            HypWholesale.Text = Wholesale.ToString();
        }
        else
        {
            HypOut.Text = "0";
            HypInstalled.Text = "0";
            //HypDifference.Text = "0";
            HypOtherInventory.Text = "0";
            HypCredit.Text = "0";
            HypWholesale.Text = "0";
        }

        string CompanyLocation = GetLocation();
        string Companyid = ddlCompany.SelectedValue;
        string ProjectStatus = GetProjectStatus();

        //HypDifference.Enabled = HypDifference.Text == "0" ? false : true;
        //HypDifference.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetails.aspx?Contact=" + "Total" + "&ContactID=" + ddlInstaller.SelectedValue + "&DateType=" + ddlDateType.SelectedValue + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Total&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&PS=" + GetProjectStatus() + "&CompanyLocation=" + ddlLocation.SelectedValue + "&Category=" + ddlCategory.SelectedValue + "&PicklistCreatedBy=" + ddlPicklistCreatedBy.SelectedValue;
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
        else
        {
            Notification("Select Installer..");
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ClearCheckBox();
        ddlInstaller.SelectedValue = "";
        ddlCompany.SelectedValue = "1";

        //ddlProjectStatus.SelectedValue = "";
        ddlIsDifference.SelectedValue = "";
        ddlAudit.SelectedValue = "";
        ddlPicklistCreatedBy.ClearSelection();

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
            if (ddlCompany.SelectedValue == "3")
            {
                lblHeaderName.Text = "Customer Name";
            }
            else
            {
                lblHeaderName.Text = "Installer Name";
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetExcelData();

        Export oExport = new Export();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        string FileName = "";
        string Name = "Installer Name";
        if (ddlCompany.SelectedValue == "1")
        {
            FileName = "All No Trace Stock Arise_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else if (ddlCompany.SelectedValue == "2")
        {
            FileName = "All No Trace Stock SolarMiner_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            FileName = "All No Trace Stock Solar Bridge_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            Name = "Installer Name";
            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }

        int[] ColList = { 1, 2, 3, 13, 14, 8, 10, 11, 15, 9, 6, 7 };

        string[] arrHeader = { Name, "Net Out", "BSGB", "SM BSGB", "WO BSGB", "Scan Required", "Other Inventory", "Non Resolve", "Audit Wholesale", "Pending Audit", "Location", "Company" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

    }

    protected DataTable GetExcelData()
    {
        DataTable dt = GetGridData1();
        foreach (System.Data.DataColumn col in dt.Columns) col.ReadOnly = false;

        if (string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            foreach (DataRow row in dt.Rows)
            {
                row["NoTraceDiff"] = 0;
            }
        }

        return dt;
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        //rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //rptLocation.DataBind();

        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1")
        {
            DataTable dtProjectStatus = ClstblProjectStatus.tblProjectStatus_SelectByActive();
            //ddlProjectStatus.DataSource = dtProjectStatus;
            //ddlProjectStatus.DataTextField = "ProjectStatus";
            //ddlProjectStatus.DataValueField = "ProjectStatusID";
            //ddlProjectStatus.DataBind();

            lblProjectStatus.Text = "Project Status";
            rptProjectStatus.DataSource = dtProjectStatus;
            rptProjectStatus.DataBind();


            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("1");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

        }
        else if (ddlCompany.SelectedValue == "2")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("2");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "CompanyName";
            ddlInstaller.DataValueField = "UserId";
            ddlInstaller.DataBind();
        }

        ddlPicklistCreatedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlPicklistCreatedBy.DataValueField = "EmployeeID";
        ddlPicklistCreatedBy.DataMember = "fullname";
        ddlPicklistCreatedBy.DataTextField = "fullname";
        ddlPicklistCreatedBy.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (sender as LinkButton);

        string[] arg = lbtn.CommandArgument.Split(';');
        string InstallerID = arg[0];
        string InstallerName = arg[1];

        DataTable dtSummary = GetSummary(InstallerID);

        DataTable dtStockItemWise = GetDataItemWise(InstallerID);

        //DataTable dtScanRequired = GetScanRequired(InstallerID);

        //DataTable dtNoTracePanel = GetNoTracePanel(InstallerID);

        DataTable dtSerialNo = GetDataSerialNo(InstallerID);

        DataTable dtProjectWise = GetDataProjectWise(InstallerID);

        AddDataInDtproject(dtProjectWise);

        DataTable dtUnusedSerialNo = GetUnusedSerialNo(InstallerID);

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSummary, "Stock Summary");
                wb.Worksheets.Add(dtStockItemWise, "Stock Item Detail");
                //wb.Worksheets.Add(dtScanRequired, "Scan Required");
                //wb.Worksheets.Add(dtNoTracePanel, "No Trace Panel");
                //wb.Worksheets.Add(dtScanRequired, "Pending Audit");
                wb.Worksheets.Add(dtProjectWise, "Pending Audit");
                wb.Worksheets.Add(dtSerialNo, "UnTrace Panel Serial No");
                //wb.Worksheets.Add(dtUnusedSerialNo, "Unused SerialNo");

                string FileName = InstallerName + "_No Trace Stock_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected DataTable GetSummary(string InstallerID)
    {
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;

        DataTable dt = new DataTable();

        dt = ClsReportsV2.SerialInstallerDetails_Summary(Category, InstallerID, DateType, StartDate, EndDate, CompanyID);

        if (dt.Rows.Count > 0)
        {
            string NetOut = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "Net Out").Sum(x => x.Field<int>("TotalCount")).ToString();

            string AriseInstalled = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "Arise Installed - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();
            
            string SMInstalled = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "SM Installed - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();
            
            string WOInstalled = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "WO Installed - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();

            string ScanRequired = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "Scan Required - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();

            string OtherInventory = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "Other Inventory - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();
            
            string Wholesale = dt.AsEnumerable().Where(y => y.Field<string>("Deatils") == "Wholesale - Minus").Sum(x => x.Field<int>("TotalCount")).ToString();

            int PendingAudit = Convert.ToInt32(NetOut) - (Convert.ToInt32(AriseInstalled) + Convert.ToInt32(SMInstalled) + Convert.ToInt32(WOInstalled) + Convert.ToInt32(ScanRequired) + Convert.ToInt32(OtherInventory));

            DataRow dr = dt.NewRow();
            dr[0] = "Pending Audit";
            dr[1] = PendingAudit;
            dt.Rows.Add(dr);
        }

        return dt;
    }

    protected DataTable GetDataItemWise(string InstallerID)
    {
        string ProjectNo = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1" || CompanyID == "4")
        {
            dt = ClsReportsV2.SerialInstallerReportDetailsV2(CompanyID, InstallerID, ProjectNo, "", DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerReportDetailsSMV2(CompanyID, InstallerID, ProjectNo, "", DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }

        dt.Columns.Remove("StockItemID");
        dt.Columns["Out"].ColumnName = "Net Out";
        dt.Columns["Diff"].ColumnName = "UnTrace";
        dt.Columns["ScanNo"].ColumnName = "Scan Required";
        dt.Columns["NoTraceDiff"].ColumnName = "Pending Audit";
        if (dt.Rows.Count > 0)
        {
            dt.DefaultView.Sort = "Pending Audit desc";
            dt = dt.DefaultView.ToTable();
        }

        return dt;
    }

    protected DataTable GetDataSerialNo(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNo = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1" || CompanyID == "4")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2_SerialNo(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2_SM_SerialNo(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }


        if (dt.Rows.Count > 0)
        {
            dt.Columns["SerialNoYN"].ColumnName = "Scan Y/N";
        }

        return dt;
    }

    protected DataTable GetDataProjectWise(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNoSearch = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1" || CompanyID == "4")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2SM(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }


        if (dt.Rows.Count > 0)
        {
            dt.Columns["MissingQty"].ColumnName = "Pending Audit";
            dt.Columns["SerialNo"].ColumnName = "ScanYN";
            dt.Columns["NameOrConsignmentNo"].ColumnName = "Pickup By";

            dt.Columns.Remove("PNo");
            dt.Columns.Remove("StockItemID");
            dt.Columns.Remove("StockCategoryID");
            dt.Columns.Remove("BSGBFlag");
            dt.Columns.Remove("ID");
            dt.Columns.Remove("DeductBy");
            dt.Columns.Remove("CompanyLocation");
        }

        return dt;
    }

    protected void AddDataInDtproject(DataTable dtProjectWise)
    {
        try
        {
            // Add New
            for (int i = 0; i < dtProjectWise.Rows.Count; i++)
            {
                string ProjectNo = dtProjectWise.Rows[i]["Projectnumber"].ToString();

                DataTable dtProject = ClsReportsV2.SP_TrackSerialNo_Project(ddlCompany.SelectedValue, ProjectNo, ddlCategory.SelectedValue);

                for (int j = 0; j < dtProject.Rows.Count; j++)
                {
                    string ColumnName1 = "Project " + (j + 1);
                    //string ColumnName2 = "Pick ID " + (j + 1);
                    string ColumnName3 = "No Panel " + (j + 1);
                    string ColumnName4 = "Project Status " + (j + 1);
                    DataColumnCollection columns = dtProjectWise.Columns;
                    DataColumn dcProject = new DataColumn(ColumnName1, typeof(Int32));
                    //DataColumn dcID = new DataColumn(ColumnName2, typeof(Int32));
                    DataColumn dcPanel = new DataColumn(ColumnName3, typeof(Int32));
                    DataColumn dcProjectStatus = new DataColumn(ColumnName4, typeof(string));
                    if (!columns.Contains(ColumnName1))
                    {
                        dtProjectWise.Columns.Add(dcProject);
                        //dtProjectWise.Columns.Add(dcID);
                        dtProjectWise.Columns.Add(dcPanel);
                        dtProjectWise.Columns.Add(dcProjectStatus);
                    }

                    //string ProjectNumber = dtProject.Rows[j]["ProjectNo"].ToString();
                    dtProjectWise.Rows[i][ColumnName1] = dtProject.Rows[j]["Projectnumber"];
                    //dtProjectWise.Rows[i][ColumnName2] = dtProject.Rows[j]["ID"];
                    dtProjectWise.Rows[i][ColumnName3] = dtProject.Rows[j]["UsedQty"];
                    dtProjectWise.Rows[i][ColumnName4] = dtProject.Rows[j]["ProjectStatus"];
                }
            }
        }
        catch (Exception ex)
        {

        }
        //return dtProjectWise;
    }

    protected DataTable GetScanRequired(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNoSearch = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1")
        {
            dt = ClsReportsV2.SerialInstallerDetails_ScanRequired(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerDetailsSM_ScanRequired(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }

        return dt;
    }

    protected DataTable GetNoTracePanel(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNoSearch = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();
        if (CompanyID == "1")
        {
            dt = ClsReportsV2.SerialInstallerDetails_NoTracePanel(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerDetailsSM_NoTracePanel(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }

        return dt;
    }

    protected DataTable GetUnusedSerialNo(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNo = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1")
        {
            dt = ClsReportsV2.NoTraceStock_UnusedSerialNo(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.NoTraceStockSM_UnusedSerialNo(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }

        int Count = dt.Rows.Count;

        return dt;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string CreatedBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string CreatedOn = System.DateTime.Now.AddHours(15).ToString();
        string ModuleName = ddlMissing.SelectedValue;

        string[] SerialNoList = txtSerialNo.Text.Split(',');

        int UpdateLogCount = 0;
        for (int i = 0; i < SerialNoList.Length; i++)
        {
            int LogId = ClsReportsV2.tbl_MissingSerialNo_InsertLog(ModuleName, SerialNoList[i], CreatedBy, CreatedOn);
            if(LogId != 0)
            {
                UpdateLogCount++;
            }
        }

        if(UpdateLogCount > 0)
        {
            Notification(UpdateLogCount + " Serial no is updated out of " + SerialNoList.Length);
        }

        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
    }

    protected void btnOpenPopUp_Click(object sender, EventArgs e)
    {
        ddlMissing.ClearSelection();
        txtSerialNo.Text = string.Empty;
        ModalPopupMissingLog.Show();
    }

    protected void BindFetchDate()
    {
        string LastUpdatedOn = ClsDbData.tbl_APIFetchData_UtilitiesLastUpdateDate("Greenbot");
        lblUpdatedMsg.Text = "Fetched till " + LastUpdatedOn;
    }
    
    #region Fetch Data from Green Bot
    protected void lbtnFetch_Click(object sender, EventArgs e)
    {
        DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("GreenBot");

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string CompanyID = dt.Rows[i]["CompanyID"].ToString();
            string FromDate = dt.Rows[i]["FromDate"].ToString();
            string ToDate = dt.Rows[i]["ToDate"].ToString();
            string username = dt.Rows[i]["username"].ToString();
            string password = dt.Rows[i]["password"].ToString();

            dtSerialNo = GetAllGreenbotDetails(FromDate, ToDate, CompanyID, username, password, dtSerialNo);
        }

        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        bool UpdateProjectNoArise = ClsDbData.tblProjectNoYN_Update();

        bool Update = ClsDbData.tbl_APIFetchData_Utilities_Update("Greenbot", DateTime.Now.AddHours(15).ToString());

        string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";

        Response.Write(msg);

        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
        BindFetchDate();
    }

    public DataTable GetAllGreenbotDetails(string FromDate, string ToDate, string CompanyID, string Username, string Password, DataTable dtSerialNo)
    {
        try
        {
            var client = new RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + FromDate + "&ToDate=" + ToDate);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    DataTable dtSTCDetails = new DataTable();
                    dtSTCDetails.Columns.Add("JobID", typeof(int));
                    dtSTCDetails.Columns.Add("PVDNo", typeof(string));
                    dtSTCDetails.Columns.Add("CalculatedSTC", typeof(string));
                    dtSTCDetails.Columns.Add("STCStatus", typeof(string));
                    dtSTCDetails.Columns.Add("STCSubmissionDate", typeof(string));

                    DataTable dtNoOfInverter = new DataTable();
                    dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
                    dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
                    dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

                    for (int i = 0; i < jobData.lstJobData.Count; i++)
                    {
                        Job_Response.lstJobData lstJobData = jobData.lstJobData[i]; // Root Object
                        Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details
                        Job_Response.InstallerView InstallerView = lstJobData.InstallerView; // get Child object Details

                        List<Job_Response.lstJobInverterDetails> lstJobInverterDetails = lstJobData.lstJobInverterDetails;
                        string NoOfInverter = "0";
                        if (lstJobInverterDetails.Count > 0)
                        {
                            NoOfInverter = lstJobInverterDetails[lstJobInverterDetails.Count - 1].NoOfInverter;

                            NoOfInverter = !string.IsNullOrEmpty(NoOfInverter) ? NoOfInverter : "0";
                        }

                        string ProjectNo = "";
                        if (CompanyID == "3")
                        {
                            ProjectNo = lstJobData.BasicDetails.JobID;
                        }
                        else
                        {
                            ProjectNo = lstJobData.BasicDetails.RefNumber;
                        }

                        string SerialNo = JobSystemDetails.SerialNumbers;
                        string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

                        string InstallerName = "";
                        try
                        {
                            if (InstallerView != null)
                            {
                                InstallerName = (InstallerView.FirstName == null ? "" : InstallerView.FirstName.ToString()) + " " + (InstallerView.LastName == null ? "" : InstallerView.LastName.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            int index = i;
                        }

                        for (int j = 0; j < SerialNoList.Length; j++)
                        {
                            DataRow dr = dtSerialNo.NewRow();
                            dr["ProjectNo"] = ProjectNo.Trim();
                            dr["SerialNo"] = SerialNoList[j];
                            dr["BSGBFlag"] = 1; // GreenBot
                            dr["CompanyID"] = CompanyID;
                            dr["StockCategoryID"] = 1;
                            dr["Varified"] = "";
                            dr["InstallerName"] = InstallerName.Trim();
                            dtSerialNo.Rows.Add(dr);
                        }

                        // Now Data Table For Wholesale
                        if (CompanyID == "3")
                        {
                            Job_Response.JobSTCDetails JobSTCDetails = lstJobData.JobSTCDetails;
                            Job_Response.JobSTCStatusData JobSTCStatusData = lstJobData.JobSTCStatusData;

                            string JobID = lstJobData.BasicDetails.JobID;
                            string FailedAccreditationCode = JobSTCDetails.FailedAccreditationCode;
                            string CalculatedSTC = JobSTCStatusData.CalculatedSTC;
                            string STCStatus = JobSTCStatusData.STCStatus;
                            string STCSubmissionDate = JobSTCStatusData.STCSubmissionDate;

                            DataRow dr = dtSTCDetails.NewRow();
                            dr["JobID"] = JobID;
                            dr["PVDNo"] = FailedAccreditationCode;
                            dr["CalculatedSTC"] = CalculatedSTC; // GreenBot
                            dr["STCStatus"] = STCStatus;
                            dr["STCSubmissionDate"] = STCSubmissionDate;
                            dtSTCDetails.Rows.Add(dr);
                        }

                        DataRow dr1 = dtNoOfInverter.NewRow();
                        dr1["ProjectNo"] = ProjectNo;
                        dr1["CompanyID"] = CompanyID;
                        dr1["NoOfInverter"] = NoOfInverter;
                        dtNoOfInverter.Rows.Add(dr1);
                    }

                    if (CompanyID == "3")
                    {
                        if (dtSTCDetails.Rows.Count > 0)
                        {
                            int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSTCDetails(dtSTCDetails);
                            string msg = "<script>alert('Total STCDetails Record is " + dtSTCDetails.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                            Response.Write(msg);
                        }
                    }

                    if (dtNoOfInverter.Rows.Count > 0)
                    {
                        int UpdateData = ClsDbData.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                        string msg = "<script>alert('Total No Of Inverter is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                        Response.Write(msg);
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return dtSerialNo;
    }

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    #endregion
    #endregion


    protected void btnOpenCreditPopUp_Click(object sender, EventArgs e)
    {
        ddlCreditedIn.ClearSelection();
        txtCreditSerialNo.Text = string.Empty;
        ModalPopupCreditSerialNo.Show();
    }

    protected void btnSaveCreedit_Click(object sender, EventArgs e)
    {
        string createdBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string creditedIn = ddlCreditedIn.SelectedValue;
        string creditedInText = ddlCreditedIn.SelectedItem.Text;

        string[] serialNoList = txtCreditSerialNo.Text.Split(',');

        int updateLogCount = 0;
        for (int i = 0; i < serialNoList.Length; i++)
        {
            int logId = ClsReportsV2.tbl_CreditSerialNo_Insert(creditedIn, creditedInText, serialNoList[i], createdBy, createdOn);
            if (logId != 0)
            {
                updateLogCount++;
            }
        }

        if (updateLogCount > 0)
        {
            Notification(updateLogCount + " Serial no is updated out of " + serialNoList.Length);
        }

        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
    }
}