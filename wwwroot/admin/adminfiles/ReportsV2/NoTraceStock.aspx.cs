﻿using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.Script.Serialization;

public partial class admin_adminfiles_ReportsV2_NoTraceStock : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //ddlCompany.SelectedValue = "2";
            BindCheckboxProjectTab();
            BindDropDown();

            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;

            BindFetchDate();

            if (Roles.IsUserInRole("PreInstaller"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                ddlEmployees.SelectedValue = stEmp.EmployeeID;
                //ddlEmployees.Enabled = false;
            }

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2021";
            //txtEndDate.Text = "30/06/2021";
            ////ddlInstaller.SelectedValue = "428700"; //Customer ID // Arise-SolarMiner
            //BindGrid(0);

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2020";
            //txtEndDate.Text = "30/06/2021";
            //ddlInstaller.SelectedValue = "531124"; //Customer ID // Arise-SolarMiner
            //BindGrid(0);
        }
    }

    protected DataTable GetGridData1()
    {
        DataTable dt = new DataTable();
        //if (ddlCompany.SelectedValue == "4")
        //{
        //    dt = ClsReportsV2.noTraceStock_SP(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue);
        //}
        //else
        //{
        //    dt = ClsReportsV2.noTraceStock(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue);
        //}

        dt = ClsReportsV2.noTraceStock(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int NetOut = Convert.ToInt32(dt.Compute("SUM(NetOut)", string.Empty));
            int AriseInstall = Convert.ToInt32(dt.Compute("SUM(AriseInstall)", string.Empty));
            int SMInstall = Convert.ToInt32(dt.Compute("SUM(SMInstall)", string.Empty));
            int WOInstall = Convert.ToInt32(dt.Compute("SUM(WOInstall)", string.Empty));
            int ScanRequired = Convert.ToInt32(dt.Compute("SUM(ScanRequired)", string.Empty));
            int OtherInventory = Convert.ToInt32(dt.Compute("SUM(OtherInventory)", string.Empty));
            int NonResolved = Convert.ToInt32(dt.Compute("SUM(NonResolved)", string.Empty));
            int Wholesale = Convert.ToInt32(dt.Compute("SUM(OtherWholesale)", string.Empty));
            int PendingAudit = Convert.ToInt32(dt.Compute("SUM(PendingAudit)", string.Empty));
            int Credit = Convert.ToInt32(dt.Compute("SUM(Credit)", string.Empty));
            int withInstaller = Convert.ToInt32(dt.Compute("SUM(WithInstaller)", string.Empty));
            int PendingInstallation = Convert.ToInt32(dt.Compute("SUM(PendingInstallation)", string.Empty));
            int ActualAudit = Convert.ToInt32(dt.Compute("SUM(ActualAudit)", string.Empty));

            HypNetOut.Text = NetOut.ToString();
            HypAriseBSGB.Text = AriseInstall.ToString();
            HypSMBSGB.Text = SMInstall.ToString();
            HypWOBSGB.Text = WOInstall.ToString();
            lbtnScanRequired.Text = ScanRequired.ToString();
            HypOtherInventory.Text = OtherInventory.ToString();
            HypNonResolve.Text = NonResolved.ToString();
            HypWholesale.Text = Wholesale.ToString();
            HypPendingAudit.Text = PendingAudit.ToString();
            HypCredit.Text = Credit.ToString();
            HypWithInstaller.Text = withInstaller.ToString();
            HypPendingInstallation.Text = PendingInstallation.ToString();
            HypActualAudit.Text = ActualAudit.ToString();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlCompany.SelectedValue = "1";
        txtProjectNumber.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlPendingAudiYN.SelectedValue = "2";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        Export oExport = new Export();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        string FileName = "AllNoTraceReport_Deduct_" + txtStartDate.Text + "_to_" + txtEndDate.Text + "_on_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                        12, 13, 14 };

        string[] arrHeader = { "InstallerID", "Installer Name", "Net Out", "Arise BSGB", "SM BSGB", "WO BSGB", "Scan Required", "Other Inventory", "Other Wholesale", "Credit", "Non Resolve"
                , "With Installer", "Future Installation", "Pending Audit", "Manager Name" };

        try
        {
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        DataTable dtEmployee = ClstblEmployees.tblEmployees_SelectAll();
        ddlEmployees.DataSource = dtEmployee;
        ddlEmployees.DataTextField = "fullname";
        ddlEmployees.DataValueField = "EmployeeID";
        ddlEmployees.DataBind();

        DataTable dtDepo = NoTraceStock.tblTransportDepo_GetAllActive();
        ddlTransportDepo.DataSource = dtDepo;
        ddlTransportDepo.DataTextField = "Name";
        ddlTransportDepo.DataValueField = "id";
        ddlTransportDepo.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("1");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

        }
        else if (ddlCompany.SelectedValue == "2")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("2");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "CompanyName";
            ddlInstaller.DataValueField = "UserId";
            ddlInstaller.DataBind();
        }

    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (sender as LinkButton);

        string[] arg = lbtn.CommandArgument.Split(';');
        string InstallerID = arg[0];
        string InstallerName = arg[1];

        DataTable dtStockItemWise = getDataStockItemWise(InstallerID);
        //DataTable dtStockItemWise = new DataTable();

        DataTable dtProjectWise = getDataProjectWise(InstallerID);

        AddDataInDtproject(dtProjectWise);

        DataTable dtSerialNo = getDataSerialNoWise(InstallerID);

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtStockItemWise, "Stock Item Detail");
                wb.Worksheets.Add(dtProjectWise, "Pending Audit");
                wb.Worksheets.Add(dtSerialNo, "UnTrace Panel Serial No");

                string FileName = InstallerName + "_No Trace Stock_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected DataTable getDataStockItemWise(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsReportsV2.noTraceStock_StockItemWise(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected DataTable getDataProjectWise(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;
        string scanYN = "";
        string category = "";

        dt = ClsReportsV2.noTraceStock_ProjectWise(companyId, projectNo, installerId, locationId, dateType, startDate, endDate, scanYN, category);

        return dt;
    }

    protected DataTable getDataSerialNoWise(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsReportsV2.noTraceStock_SerialNoWise(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected void AddDataInDtproject(DataTable dtProjectWise)
    {
        try
        {
            // Add New
            for (int i = 0; i < dtProjectWise.Rows.Count; i++)
            {
                string ProjectNo = dtProjectWise.Rows[i]["Projectnumber"].ToString();

                DataTable dtProject = ClsReportsV2.SP_TrackSerialNo_Project(ddlCompany.SelectedValue, ProjectNo, "1");

                for (int j = 0; j < dtProject.Rows.Count; j++)
                {
                    string ColumnName1 = "Project " + (j + 1);
                    //string ColumnName2 = "Pick ID " + (j + 1);
                    string ColumnName3 = "No Panel " + (j + 1);
                    string ColumnName4 = "Project Status " + (j + 1);
                    DataColumnCollection columns = dtProjectWise.Columns;
                    DataColumn dcProject = new DataColumn(ColumnName1, typeof(Int32));
                    //DataColumn dcID = new DataColumn(ColumnName2, typeof(Int32));
                    DataColumn dcPanel = new DataColumn(ColumnName3, typeof(Int32));
                    DataColumn dcProjectStatus = new DataColumn(ColumnName4, typeof(string));
                    if (!columns.Contains(ColumnName1))
                    {
                        dtProjectWise.Columns.Add(dcProject);
                        //dtProjectWise.Columns.Add(dcID);
                        dtProjectWise.Columns.Add(dcPanel);
                        dtProjectWise.Columns.Add(dcProjectStatus);
                    }

                    //string ProjectNumber = dtProject.Rows[j]["ProjectNo"].ToString();
                    dtProjectWise.Rows[i][ColumnName1] = dtProject.Rows[j]["Projectnumber"];
                    //dtProjectWise.Rows[i][ColumnName2] = dtProject.Rows[j]["ID"];
                    dtProjectWise.Rows[i][ColumnName3] = dtProject.Rows[j]["UsedQty"];
                    dtProjectWise.Rows[i][ColumnName4] = dtProject.Rows[j]["ProjectStatus"];
                }
            }
        }
        catch (Exception ex)
        {

        }
        //return dtProjectWise;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string CreatedBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string CreatedOn = System.DateTime.Now.AddHours(15).ToString();
        string ModuleName = ddlMissing.SelectedValue;

        string[] SerialNoList = txtSerialNo.Text.Trim().Split('\n');

        int UpdateLogCount = 0;
        for (int i = 0; i < SerialNoList.Length; i++)
        {
            if (!string.IsNullOrEmpty(SerialNoList[i].Trim()))
            {
                int LogId = ClsReportsV2.tbl_MissingSerialNo_InsertLog(ModuleName, SerialNoList[i], CreatedBy, CreatedOn);
                if (LogId != 0)
                {
                    UpdateLogCount += LogId;
                }
            }
        }

        if (UpdateLogCount > 0)
        {
            Notification(UpdateLogCount + " Serial no is updated out of " + SerialNoList.Length);
        }

        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
    }

    protected void btnOpenPopUp_Click(object sender, EventArgs e)
    {
        ddlMissing.ClearSelection();
        txtSerialNo.Text = string.Empty;
        ModalPopupMissingLog.Show();
    }

    protected void BindFetchDate()
    {
        string LastUpdatedOn = ClsDbData.tbl_APIFetchData_UtilitiesLastUpdateDate("Greenbot");
        lblUpdatedMsg.Text = "Fetched till " + LastUpdatedOn;
    }

    #region Fetch Data from Green Bot
    protected void lbtnFetch_Click(object sender, EventArgs e)
    {
        DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("GreenBot");

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string CompanyID = dt.Rows[i]["CompanyID"].ToString();
            string FromDate = dt.Rows[i]["FromDate"].ToString();
            string ToDate = dt.Rows[i]["ToDate"].ToString();
            string username = dt.Rows[i]["username"].ToString();
            string password = dt.Rows[i]["password"].ToString();

            dtSerialNo = GetAllGreenbotDetails(FromDate, ToDate, CompanyID, username, password, dtSerialNo);
        }

        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        bool UpdateProjectNoArise = ClsDbData.tblProjectNoYN_Update();
        bool Update = ClsDbData.tbl_APIFetchData_Utilities_Update("Greenbot", DateTime.Now.AddHours(15).ToString());
        bool UpdateFlag = ClsDbData.Update_Flags();
        string msg = "<script>alert('Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";

        Response.Write(msg);

        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            BindGrid(0);
        }
        BindFetchDate();
    }

    public DataTable GetAllGreenbotDetails(string FromDate, string ToDate, string CompanyID, string Username, string Password, DataTable dtSerialNo)
    {
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + FromDate + "&ToDate=" + ToDate);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    DataTable dtSTCDetails = new DataTable();
                    dtSTCDetails.Columns.Add("JobID", typeof(int));
                    dtSTCDetails.Columns.Add("PVDNo", typeof(string));
                    dtSTCDetails.Columns.Add("CalculatedSTC", typeof(string));
                    dtSTCDetails.Columns.Add("STCStatus", typeof(string));
                    dtSTCDetails.Columns.Add("STCSubmissionDate", typeof(string));

                    DataTable dtNoOfInverter = new DataTable();
                    dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
                    dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
                    dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

                    for (int i = 0; i < jobData.lstJobData.Count; i++)
                    {
                        Job_Response.lstJobData lstJobData = jobData.lstJobData[i]; // Root Object
                        Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details
                        Job_Response.InstallerView InstallerView = lstJobData.InstallerView; // get Child object Details

                        List<Job_Response.lstJobInverterDetails> lstJobInverterDetails = lstJobData.lstJobInverterDetails;
                        string NoOfInverter = "0";
                        if (lstJobInverterDetails.Count > 0)
                        {
                            NoOfInverter = lstJobInverterDetails[lstJobInverterDetails.Count - 1].NoOfInverter;

                            NoOfInverter = !string.IsNullOrEmpty(NoOfInverter) ? NoOfInverter : "0";
                        }

                        string ProjectNo = "";
                        if (CompanyID == "3")
                        {
                            ProjectNo = lstJobData.BasicDetails.JobID;
                        }
                        else
                        {
                            ProjectNo = lstJobData.BasicDetails.RefNumber;
                        }

                        string SerialNo = JobSystemDetails.SerialNumbers;
                        string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

                        string InstallerName = "";
                        try
                        {
                            if (InstallerView != null)
                            {
                                InstallerName = (InstallerView.FirstName == null ? "" : InstallerView.FirstName.ToString()) + " " + (InstallerView.LastName == null ? "" : InstallerView.LastName.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            int index = i;
                        }

                        for (int j = 0; j < SerialNoList.Length; j++)
                        {
                            DataRow dr = dtSerialNo.NewRow();
                            dr["ProjectNo"] = ProjectNo.Trim();
                            dr["SerialNo"] = SerialNoList[j];
                            dr["BSGBFlag"] = 1; // GreenBot
                            dr["CompanyID"] = CompanyID;
                            dr["StockCategoryID"] = 1;
                            dr["Varified"] = "";
                            dr["InstallerName"] = InstallerName.Trim();
                            dtSerialNo.Rows.Add(dr);
                        }

                        // Now Data Table For Wholesale
                        if (CompanyID == "3")
                        {
                            Job_Response.JobSTCDetails JobSTCDetails = lstJobData.JobSTCDetails;
                            Job_Response.JobSTCStatusData JobSTCStatusData = lstJobData.JobSTCStatusData;

                            string JobID = lstJobData.BasicDetails.JobID;
                            string FailedAccreditationCode = JobSTCDetails.FailedAccreditationCode;
                            string CalculatedSTC = JobSTCStatusData.CalculatedSTC;
                            string STCStatus = JobSTCStatusData.STCStatus;
                            string STCSubmissionDate = JobSTCStatusData.STCSubmissionDate;

                            DataRow dr = dtSTCDetails.NewRow();
                            dr["JobID"] = JobID;
                            dr["PVDNo"] = FailedAccreditationCode;
                            dr["CalculatedSTC"] = CalculatedSTC; // GreenBot
                            dr["STCStatus"] = STCStatus;
                            dr["STCSubmissionDate"] = STCSubmissionDate;
                            dtSTCDetails.Rows.Add(dr);
                        }

                        DataRow dr1 = dtNoOfInverter.NewRow();
                        dr1["ProjectNo"] = ProjectNo;
                        dr1["CompanyID"] = CompanyID;
                        dr1["NoOfInverter"] = NoOfInverter;
                        dtNoOfInverter.Rows.Add(dr1);
                    }

                    if (CompanyID == "3")
                    {
                        if (dtSTCDetails.Rows.Count > 0)
                        {
                            int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSTCDetails(dtSTCDetails);
                            string msg = "<script>alert('Total STCDetails Record is " + dtSTCDetails.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                            Response.Write(msg);
                        }
                    }

                    if (dtNoOfInverter.Rows.Count > 0)
                    {
                        int UpdateData = ClsDbData.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                        string msg = "<script>alert('Total No Of Inverter is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                        Response.Write(msg);
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return dtSerialNo;
    }

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }

    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }

    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    #endregion
    #endregion

    protected void btnOpenCreditPopUp_Click(object sender, EventArgs e)
    {
        ddlCreditedIn.ClearSelection();
        txtCreditSerialNo.Text = string.Empty;
        txtCreditAgainst.Text = string.Empty;

        ModalPopupCreditSerialNo.Show();
    }

    protected void btnSaveCreedit_Click(object sender, EventArgs e)
    {
        string createdBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string creditedIn = ddlCreditedIn.SelectedValue;
        string creditedInText = ddlCreditedIn.SelectedItem.Text;

        string[] serialNoList = txtCreditSerialNo.Text.Trim().Split('\n');

        string[] serialNoListCreditAgainst = txtCreditAgainst.Text.Trim().Split('\n');

        string existSerialNo = "";

        for (int i = 0; i < serialNoList.Length; i++)
        {
            if (!string.IsNullOrEmpty(serialNoList[i].Trim()))
            {
                string serialNo = serialNoList[i].Trim();
                int Id = ClsReportsV2.tbl_CreditSerialNo_Exists(serialNo);
                if (Id != 0)
                {
                    existSerialNo += "," + serialNo;
                }
            }
        }

        if (existSerialNo != "")
        {
            string msg = existSerialNo.Substring(1) + " Serial no is already credited.";
            Notification(msg);
            ModalPopupCreditSerialNo.Show();
        }
        else
        {
            int updateLogCount = 0;
            int updateLogCountNull = 0;
            for (int i = 0; i < serialNoList.Length; i++)
            {
                if (!string.IsNullOrEmpty(serialNoList[i].Trim()))
                {
                    string serialNo = serialNoList[i].Trim();
                    int logId = ClsReportsV2.tbl_CreditSerialNo_Insert(creditedIn, creditedInText, serialNo, createdBy, createdOn);
                    if (logId != 0)
                    {
                        updateLogCount++;
                    }
                }
                else
                {
                    updateLogCountNull++;
                }
            }

            int updateFlagCount = 0;
            int updateFlagCountNull = 0;

            for (int i = 0; i < serialNoListCreditAgainst.Length; i++)
            {
                if (!string.IsNullOrEmpty(serialNoListCreditAgainst[i].Trim()))
                {
                    string serialNo = serialNoListCreditAgainst[i].Trim();
                    bool updateFlag = false;
                    if (ddlCreditAgainst.SelectedValue == "1") // Pending Audit
                    {
                        updateFlag = ClsReportsV2.tblStockSerialNo_Update_CreditFlag(serialNo, creditedIn); // 1) Other Inventory Credit 2) Wholesale Credit 3) Purchase Credit
                    }
                    else if (ddlCreditAgainst.SelectedValue == "2") // Non Resolve
                    {
                        updateFlag = ClsReportsV2.tblStockSerialNo_Update_CreditFlag(serialNo, creditedIn); // 1) Other Inventory Credit 2) Wholesale Credit 3) Purchase Credit
                        bool updateCreditFlag = ClsReportsV2.tbl_MissingSerialNo_Update_CreditFlag(serialNo, "1");
                    }

                    if (updateFlag)
                    {
                        updateFlagCount++;
                    }
                }
                else
                {
                    updateFlagCountNull++;
                }
            }

            if (updateLogCount > 0)
            {
                Notification(updateLogCount + " Serial no is updated out of " + (Convert.ToInt32(serialNoList.Length) - updateLogCountNull).ToString());
            }
            //if (updateFlagCount > 0)
            //{
            //    Notification(updateFlagCount + " Serial no is updated out of " + (Convert.ToInt32(serialNoListFlag.Length) - updateFlagCountNull).ToString());
            //}

            if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
            {
                BindGrid(0);
            }
        }

    }

    protected DataTable getDataScanRequired()
    {
        DataTable dt = new DataTable();
        string installerId = ddlInstaller.SelectedValue;
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;
        string manegerId = ddlEmployees.SelectedValue;

        dt = ClsReportsV2.noTraceStock_ScanRequiredDeatils(companyId, projectNo, installerId, locationId, dateType, startDate, endDate, manegerId);

        return dt;
    }

    protected void lbtnScanRequired_Click(object sender, EventArgs e)
    {
        DataTable dtScanRequired = getDataScanRequired();
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtScanRequired, "Scan Required");

                string FileName = "Scan Required_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected void btnAssignProjOpenPopUp_Click(object sender, EventArgs e)
    {
        ddlAssignCompany.ClearSelection();
        txtAssignProjectNo.Text = string.Empty;
        txtAssignSerialNo.Text = string.Empty;
        MPAssignProjectNo.Show();
    }

    protected void btnAssign_Click(object sender, EventArgs e)
    {
        string createdBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string companyId = ddlAssignCompany.SelectedValue;
        string projectNo = txtAssignProjectNo.Text;
        string[] serialNoList = txtAssignSerialNo.Text.Trim().Split('\n');

        int exists = ClsReportsV2.tbl_Projects_ExistsByCompanyID(companyId, projectNo);
        if (exists == 1)
        {
            int suc = 0;
            for (int i = 0; i < serialNoList.Length; i++)
            {
                string serialNo = serialNoList[i].Trim();
                if (!string.IsNullOrEmpty(serialNo))
                {
                    int sucInsert = ClsReportsV2.tbl_AssignSerialNoToProject_Insert(companyId, projectNo, serialNo, createdOn, createdBy);
                    if (sucInsert > 0)
                    {
                        suc++;
                    }
                }
            }
            if (suc > 0)
            {
                Notification("Transaction Successfull.");
            }
            else
            {
                Notification("Transaction Failed.");
            }
        }
        else
        {
            Notification("Invalid Project No.");
            MPAssignProjectNo.Show();
        }

    }

    protected void lbtnNotes_Click(object sender, EventArgs e)
    {
        LinkButton lBtn = (sender as LinkButton);

        string installerId = lBtn.CommandArgument.ToString();

        hndInsatllerId.Value = installerId;
        txtNotesProjectNo.Text = string.Empty;
        txtNotes.Text = string.Empty;
        ddlStockWith.ClearSelection();
        ddlTransportDepo.ClearSelection();

        if (ddlStockWith.SelectedValue == "3")
        {
            DivDepo.Visible = true;
        }
        else
        {
            DivDepo.Visible = false;
        }

        mpNotes.Show();
    }

    protected void btnSaveNotes_Click(object sender, EventArgs e)
    {
        string installerId = hndInsatllerId.Value;
        string projectNo = txtNotesProjectNo.Text.Trim();
        string notes = txtNotes.Text.Trim();
        string stockWith = ddlStockWith.SelectedValue;
        string transportDepo = ddlTransportDepo.SelectedValue;
        string createdBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();

        if (!string.IsNullOrEmpty(notes))
        {
            int sucInsert = ClsReportsV2.tbl_InstallerNotes_Insert(projectNo, notes, createdOn, createdBy);
            bool Update = NoTraceStock.tbl_InstallerNotes_Update(sucInsert.ToString(), stockWith, transportDepo);
            if (sucInsert > 0)
            {
                Notification("Transaction Successfull.");
            }
            else
            {
                Notification("Transaction Failed.");
            }
        }
    }

    protected void btnUpdateBSManual_Click(object sender, EventArgs e)
    {
        txtProject.Text = string.Empty;
        ModalPopupExtender1.Show();
    }

    protected void btnBSGBManual_Click(object sender, EventArgs e)
    {
        string projectNo = txtProject.Text;

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));
        
        dtSerialNo = GetAllBridgeSelectDetails(projectNo, "1", dtSerialNo);

        ////bool Delete = ClsDbData.tblSerialNoFromBSGB_DeleteByBSGBFlag("2");
        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        bool UpdateProjectNoArise = ClsDbData.tblProjectNoYN_Update();
        bool UpdateFlag = ClsDbData.Update_Flags();
        bool Update = ClsDbData.tbl_APIFetchData_Utilities_Update("BridgeSelect", DateTime.Now.AddHours(14).ToString());

        string msg = "Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData;

        Notification(msg);
        BindGrid(0);
    }

    #region Get Data from BridgeSelect 
    public DataTable GetAllBridgeSelectDetails(string jobNo, string CompanyID, DataTable dtSerialNo)
    {
        Job_Response.Details ReturnDetails = new Job_Response.Details();
        try
        {
            var obj = new ClsBridgeSelect
            {
                crmid = jobNo
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            //string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/find/jobs/date/";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/products";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<BSDetailsRoot> JsonData = client.Execute<BSDetailsRoot>(request);

            BSDetailsRoot jobData = JsonConvert.DeserializeObject<BSDetailsRoot>(JsonData.Content);

            DataTable dtNoOfInverter = new DataTable();
            dtNoOfInverter.Columns.Add("ProjectNo", typeof(string));
            dtNoOfInverter.Columns.Add("CompanyID", typeof(int));
            dtNoOfInverter.Columns.Add("NoOfInverter", typeof(int));

            Details jobs = jobData.Success.details;
            string InstallerName = "";

            string ProjectNo = jobNo;
            //string ProjectNo = "";
            //string ProjectNo = !string.IsNullOrEmpty(jobs.Crmid.ToString()) ? trimmer.Replace(jobs.Crmid.ToString(), "") : "";

            foreach (var item in jobs.Panels)
            {
                string SerialNo = item.Key;
                The03091220_C100084 PanelStatus = item.Value;
                string Verified = PanelStatus.S.ToString();

                DataRow dr = dtSerialNo.NewRow();
                dr["ProjectNo"] = ProjectNo;
                dr["SerialNo"] = SerialNo;
                dr["BSGBFlag"] = 2; // GreenBot
                dr["CompanyID"] = CompanyID;
                dr["Varified"] = Verified;
                dr["StockCategoryID"] = 1;
                dr["InstallerName"] = InstallerName;
                dtSerialNo.Rows.Add(dr);
            }

            foreach (var item in jobs.Inverters)
            {
                string SerialNo = item.Key;
                The03091220_C100084 PanelStatus = item.Value;
                string Verified = PanelStatus.S.ToString();

                DataRow dr = dtSerialNo.NewRow();
                dr["ProjectNo"] = ProjectNo;
                dr["SerialNo"] = SerialNo;
                dr["BSGBFlag"] = 2; // BridgeSelect
                dr["CompanyID"] = CompanyID;
                dr["Varified"] = Verified;
                dr["StockCategoryID"] = 2;
                dr["InstallerName"] = InstallerName;
                dtSerialNo.Rows.Add(dr);
            }

            string NoOfInverter = jobs.Inverters.Count.ToString();

            DataRow dr1 = dtNoOfInverter.NewRow();
            dr1["ProjectNo"] = ProjectNo;
            dr1["CompanyID"] = CompanyID;
            dr1["NoOfInverter"] = NoOfInverter;
            dtNoOfInverter.Rows.Add(dr1);

            if (dtNoOfInverter.Rows.Count > 0)
            {
                int UpdateData = ClsDbData.Bulk_InsertUpdate_tbl_NoOfInverterFromBSGB(dtNoOfInverter);
                //string msg = "<script>alert('Total Record is " + dtNoOfInverter.Rows.Count + " and Updated Record is " + UpdateData + "');</script>";
                //Response.Write(msg);
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return dtSerialNo;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    public class ClsBridgeSelect
    {
        public string crmid;
    }

    public class BSDetailsRoot
    {
        public Success Success { get; set; }
    }
    
    public class Success
    {
        public Details details { get; set; }
        
        public int Code { get; set; }
        
        public string Description { get; set; }
    }

    public class Details
    {
        [JsonProperty("crmid", NullValueHandling = NullValueHandling.Ignore)]
        public string Crmid { get; set; }

        [JsonProperty("panels")]
        public Dictionary<string, The03091220_C100084> Panels { get; set; }

        [JsonProperty("inverters")]
        public Dictionary<string, The03091220_C100084> Inverters { get; set; }
    }

    public class The03091220_C100084
    {
        [JsonProperty("s")]
        public string S { get; set; }
    }

    //public class Panels
    //{
    //    public string[] ss { get; set; }
    //}
    #endregion

    #region Get Excels Data
    protected DataTable getDataTotalExcelPendingAudit()
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string installerId = ddlInstaller.SelectedValue;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;
        string manegerId = ddlEmployees.SelectedValue;
        string pendingAudiYN = ddlPendingAudiYN.SelectedValue;

        dt = ClsReportsV2.noTraceStockV2_TotalExcel(companyId, projectNo, installerId, locationId, dateType, startDate, endDate, manegerId, pendingAudiYN);

        return dt;
    }

    protected DataTable getDataTotalExcel(string d)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string installerId = ddlInstaller.SelectedValue;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;
        string manegerId = ddlEmployees.SelectedValue;
        string pendingAudiYN = ddlPendingAudiYN.SelectedValue;
        string ExcelData = d;

        dt = ClsReportsV2.noTraceStockV2_TotalExcelAll(companyId, projectNo, installerId, locationId, dateType, startDate, endDate, manegerId, pendingAudiYN, ExcelData);

        return dt;
    }
    #endregion

    protected void HypPendingAudit_Click(object sender, EventArgs e)
    {
        DataTable dt = getDataTotalExcelPendingAudit();
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "All Pending Audit Serial No");

                string FileName = "All Pending Audit Serial No_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected void expertExcelV2(DataTable dt, string fileName, string sheetName)
    {
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, sheetName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + fileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected void TotalExportToExcel(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender as LinkButton;
        string sheetName = btn.CommandArgument.ToString();

        DataTable dt = getDataTotalExcel(sheetName);

        string FileName = "All " + sheetName + " Serial No_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

        expertExcelV2(dt, FileName, sheetName);
    }

    protected void btnSaveSerialNoNotes_Click(object sender, EventArgs e)
    {
        string ex = NoTraceStock.SerialNoNotes_Insert(txtNotesSerialNo.Text.Trim(), txtSerialNoNotes.Text.Trim(), "1");

        Notification(ex);
    }

    protected void btnNotes_Click(object sender, EventArgs e)
    {
        txtNotesSerialNo.Text = string.Empty;
        txtSerialNoNotes.Text = string.Empty;
        ModalPopupExtender2.Show();
    }

    protected void ddlStockWith_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlStockWith.SelectedValue == "3")
        {
            DivDepo.Visible = true;
        }
        else
        {
            DivDepo.Visible = false;
        }

        mpNotes.Show();
    }
}