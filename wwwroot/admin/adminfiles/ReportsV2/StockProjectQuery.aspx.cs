﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockProjectQuery : System.Web.UI.Page
{
    protected DataTable rpttable;
    static DataView dv;
    protected static string Siteurl;
    protected static string userid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropdown();
            BindGrid(0);
        }
    }

    public void BindDropdown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "location";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataMember = "CompanyLocationID";
        ddlLocation.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();
    }

    protected DataTable GetGridData1()
    {
        DataTable dt1 = ClsReportsV2.SP_StockProjectQuery_GetData(ddlCompany.SelectedValue, txtSerachProjectNo.Text, ddlLocation.SelectedValue, ddlInstaller.SelectedValue, userid);

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "StockProjectQuery_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 0, 1, 2, 3, 4,
                        5, 6, 7, 8, 9, 11, 12};

        string[] arrHeader = { "Project No", "Picklist ID", "Project Status", "Location", "Installer",
            "System Details", "Notes", "Picklist Gen.", "Assinged Dept.", "Assigned By", 
            "EmployeeNotes", "AllEmployee Notes" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSerachProjectNo.Text = "";
        ddlLocation.SelectedValue = "";
        ddlInstaller.SelectedValue = "";

        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "UpdateStatus")
        {
            ddlStatus.SelectedValue = "";
            txtStutusNotes.Text = "";

            string arg = e.CommandArgument.ToString();
            hndQueryID.Value = arg;
            ModalPopupExtenderUpdateStatus.Show();
        }

        //if (e.CommandName == "Delete")
        //{
        //    string arg = e.CommandArgument.ToString();
        //    hndDeleteID.Value = arg;
        //    ModalPopupExtenderDelete.Show();
        //}

        if (e.CommandName == "Note")
        {
            try
            {
                hndNewQueryID.Value = "";
                hndMode.Value = "";
                hndCompanyID.Value = "";

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees sttbl = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string [] arg;
                arg = e.CommandArgument.ToString().Split(';');

                string NewQueryID = arg[0];
                string CompanyID = arg[1];

                hndNewQueryID.Value = NewQueryID;
                hndCompanyID.Value = CompanyID;

                BindNotes(sttbl.EmployeeID, NewQueryID, CompanyID);

                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    public void Reset()
    {

    }

    public void SetAdd()
    {
        Notification("Transaction Successful.");
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        string ID = hndQueryID.Value;

        if (ID != "")
        {
            bool SuccStstus = ClstblManageReturnStock.tbl_ManageReturnStock_Update_QueryStatus(ID, ddlStatus.SelectedValue);
            bool SuccStstusNotes = ClstblManageReturnStock.tbl_ManageReturnStock_Update_StatusNotes(ID, txtStutusNotes.Text);

            if (SuccStstus)
            {
                Notification("Transaction Successful.");
                BindGrid(0);
            }
            else
            {
                SetError1();
            }
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        //string ID = hndDeleteID.Value;

        //if (ID != "")
        //{
        //    bool Delete = ClstblManageReturnStock.tbl_ManageReturnStock_DeleteByID(ID);

        //    if (Delete)
        //    {
        //        SetAdd1();
        //        BindGrid(0);
        //    }
        //    else
        //    {
        //        SetError1();
        //    }
        //}
    }

    public void BindNotes(string EmployeeID, string QueryID, string CompanyID)
    {
        DataTable dt = ClsReportsV2.tbl_EmployeeSolvedProjectQueryNotes_GetDataByID(EmployeeID, QueryID, CompanyID);
        if (dt.Rows.Count > 0)
        {
            DivNewNotes.Visible = true;
            RptNewNotes.DataSource = dt;
            RptNewNotes.DataBind();
        }
        else
        {
            DivNewNotes.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNewNotes.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            DivNewNotes.Attributes.Add("style", "");
        }
    }

    protected void RptNewNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNewNote")
        {
            try
            {
                DataTable dt = new DataTable();

                if(hndCompanyID.Value != "3")
                {
                    dt = ClsReportsV2.tbl_EmployeeSolvedProjectQueryNotes_SelectByID(ID);
                }
                else
                {
                    dt = ClsReportsV2.tbl_EmployeeSolvedWholesaleQueryNotes_SelectByID(ID);
                }

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    txtNotesNew.Text = "";
                    hndID.Value = ID;
                    txtNotesNew.Text = dt.Rows[0]["Notes"].ToString();
                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNewNotes")
        {
            try
            {
                bool del = false; 

                if (hndCompanyID.Value != "3")
                {
                    del = ClsReportsV2.tbl_EmployeeSolvedProjectQueryNotes_DeleteByID(ID);
                }
                else
                {
                    del = ClsReportsV2.tbl_EmployeeSolvedWholesaleQueryNotes_DeleteByID(ID);
                }

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees sttbl = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                BindNotes(sttbl.EmployeeID, hndNewQueryID.Value, hndCompanyID.Value);
                ModalPopupExtenderNewNote.Show();
                BindGrid(0);
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNote.Show();
    }

    protected void lbtnSaveNewNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string PickListID = hndNewQueryID.Value;
            string CompanyID = hndCompanyID.Value;
            string EnteredOn = DateTime.Now.AddHours(14).ToShortDateString();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees sttbl = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (hndMode.Value != "Update")
            {
                if (sttbl.EmployeeID != "" && PickListID != "")
                {
                    int ID = 0;
                    if (CompanyID != "3")
                    {
                        ID = ClsReportsV2.tbl_EmployeeSolvedProjectQueryNotes_Insert(sttbl.EmployeeID, EnteredOn, PickListID, txtNotesNew.Text);
                    }
                    else
                    {
                        ID = ClsReportsV2.tbl_EmployeeSolvedWholesaleQueryNotes_Insert(sttbl.EmployeeID, EnteredOn, PickListID, txtNotesNew.Text);
                    }
                    
                    if (ID != 0)
                    {
                        SetAdd1();
                        txtNotesNew.Text = "";

                        BindNotes(sttbl.EmployeeID, PickListID, CompanyID);
                        ModalPopupExtenderNewNote.Show();
                        BindGrid(0);
                    }
                }
            }
            else
            {
                string ID = hndID.Value;
                if (ID != "")
                {
                    bool suc = false;

                    if (CompanyID != "3")
                    {
                        suc = ClsReportsV2.tbl_EmployeeSolvedProjectQueryNotes_Update(ID, txtNotesNew.Text);
                    }
                    else
                    {
                        suc = ClsReportsV2.tbl_EmployeeSolvedWholesaleQueryNotes_Update(ID, txtNotesNew.Text);
                    }

                    if(suc)
                    {
                        SetAdd1();
                    }

                    txtNotesNew.Text = "";
                    hndMode.Value = "";

                    BindNotes(sttbl.EmployeeID, PickListID, CompanyID);
                    ModalPopupExtenderNewNote.Show();

                    BindGrid(0);
                }
                //hndMode.Value = "";
            }
        }
        catch (Exception ex)
        {
            Notification("Inner Exception");
        }
    }
}