﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockPendingInstallerWise : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindCheckboxProjectTab();
            BindDropDown();

            ddlInstallation.SelectedValue = "1";
            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2021";
            //txtEndDate.Text = "21/01/2021";

            BindGrid(0);
        }
    }

    public string GetProjectStatus()
    {
        string ProjectStatus = "";
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                ProjectStatus += "," + hdnModule.Value.ToString();
            }
        }
        if (ProjectStatus != "")
        {
            ProjectStatus = ProjectStatus.Substring(1);
        }

        return ProjectStatus;
    }

    public string GetLocation()
    {
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }

        return Location;
    }

    protected DataTable GetGridData1()
    {
        string Location = GetLocation();

        string ProjectStatus = GetProjectStatus();

        DataTable dt = ClsReportsV2.SP_StockPendingReport_InstallerWiseV2(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, txtStockItem.Text, ddlCategory.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ProjectStatus, ddlIsDifference.SelectedValue, ddlAudit.SelectedValue, ddlInstallation.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(StockDeducted)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(SaleQty)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int PanelRevert = Convert.ToInt32(dt.Compute("SUM(StockRevert)", string.Empty));
            int PAudit = (PanelDiff - PanelRevert);

            HypOut.Text = PanelStockDeducted.ToString();
            HypInstalled.Text = SaleQtyPanel.ToString();
            lblPDifference.Text = PanelDiff.ToString();
            HypRevert.Text = PanelRevert.ToString();
            lblPAudit.Text = PAudit.ToString();
        }
        else
        {
            HypOut.Text = "0";
            HypInstalled.Text = "0";
            lblPDifference.Text = "0";
            HypRevert.Text = "0";
            lblPAudit.Text = "0";
        }

        string CompanyLocation = GetLocation();
        string Companyid = ddlCompany.SelectedValue;
        string ProjectStatus = GetProjectStatus();

        HypOut.Enabled = HypOut.Text == "0" ? false : true;
        HypOut.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseTotalDetails.aspx?Contact=" + "" + "&ContactID=" + ddlInstaller.SelectedValue + "" + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=TotalOut" + "&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + GetProjectStatus() + "&Category=" + ddlCategory.SelectedValue + "&IsDiff=" + ddlIsDifference.SelectedValue + "&IsAudit=" + ddlAudit.SelectedValue + "&CompanyLocation=" + ddlLocation.SelectedValue + "&InstallationDate=" + ddlInstallation.SelectedValue;

        HypInstalled.Enabled = HypInstalled.Text == "0" ? false : true;
        HypInstalled.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseTotalDetails.aspx?Contact=" + "" + "&ContactID=" + ddlInstaller.SelectedValue + "" + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=TotalOut" + "&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + GetProjectStatus() + "&Category=" + ddlCategory.SelectedValue + "&IsDiff=" + ddlIsDifference.SelectedValue + "&IsAudit=" + ddlAudit.SelectedValue + "&CompanyLocation=" + ddlLocation.SelectedValue + "&InstallationDate=" + ddlInstallation.SelectedValue;

        HypRevert.Enabled = HypRevert.Text == "0" ? false : true;
        HypRevert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseTotalDetails.aspx?Contact=" + "" + "&ContactID=" + ddlInstaller.SelectedValue + "" + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=TotalOut" + "&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + GetProjectStatus() + "&Category=" + ddlCategory.SelectedValue + "&IsDiff=" + ddlIsDifference.SelectedValue + "&IsAudit=" + ddlAudit.SelectedValue + "&CompanyLocation=" + ddlLocation.SelectedValue + "&InstallationDate=" + ddlInstallation.SelectedValue;
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ClearCheckBox();
        ddlInstaller.SelectedValue = "";
        ddlCompany.SelectedValue = "1";

        txtStockItem.Text = string.Empty;
        ddlCategory.SelectedValue = "";
        //ddlProjectStatus.SelectedValue = "";
        ddlIsDifference.SelectedValue = "";
        ddlAudit.SelectedValue = "";
        ddlInstallation.SelectedValue = "1";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
            if(ddlCompany.SelectedValue == "3")
            {
                lblHeaderName.Text = "Customer Name";
            }
            else
            {
                lblHeaderName.Text = "Installer Name";
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        Export oExport = new Export();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        string FileName = "";
        string Name = "Installer Name";
        if (ddlCompany.SelectedValue == "1")
        {
            FileName = "StockPendingReportInstallerWise_Arise_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else if (ddlCompany.SelectedValue == "2")
        {
            FileName = "StockPendingReportInstallerWise_SolarMiner_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else if (ddlCompany.SelectedValue == "3")
        {
            FileName = "StockPendingReportInstallerWise_Wholesale_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            Name = "Customer Name";
            //int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8 };

            //string[] arrHeader = { "Stock Item", "Stock Model", "Location", "Out", "Installed", "Difference", "Revert", "Audit" };
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }

        int[] ColList = { 1, 2, 3, 4, 5, 6 };

        string[] arrHeader = { Name, "Out", "Installed", "Difference", "Revert", "Audit" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();

        //rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //rptLocation.DataBind();

        ddlLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1" || ddlCompany.SelectedValue == "2")
        {
            //ddlProjectStatus.Items.Clear();
            //ListItem DefaultItem = new ListItem { Text = "Project Status", Value = "" };
            //ddlProjectStatus.Items.Add(DefaultItem);

            DataTable dtProjectStatus = ClstblProjectStatus.tblProjectStatus_SelectByActive();
            //ddlProjectStatus.DataSource = dtProjectStatus;
            //ddlProjectStatus.DataTextField = "ProjectStatus";
            //ddlProjectStatus.DataValueField = "ProjectStatusID";
            //ddlProjectStatus.DataBind();

            lblProjectStatus.Text = "Project Status";
            rptProjectStatus.DataSource = dtProjectStatus;
            rptProjectStatus.DataBind();

            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else
        {
            //ddlProjectStatus.Items.Clear();
            //ListItem DefaultItem = new ListItem { Text = "Job Status", Value = "" };
            //ddlProjectStatus.Items.Add(DefaultItem);

            DataTable dtJobStatus = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
            //ddlProjectStatus.DataSource = dtJobStatus;
            //ddlProjectStatus.DataTextField = "JobStatusType";
            //ddlProjectStatus.DataValueField = "Id";
            //ddlProjectStatus.DataBind();

            dtJobStatus.Columns["Id"].ColumnName = "ProjectStatusID";
            dtJobStatus.Columns["JobStatusType"].ColumnName = "ProjectStatus";

            lblProjectStatus.Text = "Job Status";
            rptProjectStatus.DataSource = dtJobStatus;
            rptProjectStatus.DataBind();

            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Customer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtCustomer = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlInstaller.DataSource = dtCustomer;
            ddlInstaller.DataTextField = "Customer";
            ddlInstaller.DataValueField = "CustomerID";
            ddlInstaller.DataBind();
        }
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }
}