﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_PicklistReport : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            SiteURL = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlDateType.SelectedValue = "1";
            txtStartDate.Text = DateTime.Now.AddHours(1).ToShortDateString();
            txtEndDate.Text = DateTime.Now.AddHours(1).ToShortDateString();

            BindFilter();
            BindGrid(0);
        }
    }

    public void BindFilter()
    {
        DataTable dtLocation = ClstblCompanyLocations.tblCompanyLocations_Select();
        ddlLocation.DataSource = dtLocation;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
        
        DataTable dtInstaller = ClsReportsV2.tbl_PickListLog_getInstaller();
        ddlInstaller.DataSource = dtInstaller;
        ddlInstaller.DataTextField = "InstallerName";
        ddlInstaller.DataValueField = "InstallerID";
        ddlInstaller.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string companyId = ddlComapny.SelectedValue;
        string projectNo = txtProjectNo.Text;
        string installer = ddlInstaller.SelectedValue;
        string location = ddlLocation.SelectedValue;
        string picklist = ddlPicklist.SelectedValue;
        string deduct = ddlDeduct.SelectedValue;
        string picklistType = ddlPicklistType.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsReportsV2.Sp_Picklist_getData(companyId, projectNo, installer, location, picklist, deduct, picklistType, dateType, startDate, endDate);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "detail")
        {
            string StockOrderID = e.CommandArgument.ToString();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd()
    {
        Notification("There are no items to show in this view");
    }

    public void SetError()
    {
        Notification("Transaction Failed.");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        ddlComapny.SelectedValue = "1";
        txtProjectNo.Text = string.Empty;
        ddlInstaller.ClearSelection();
        ddlLocation.ClearSelection();
        ddlPicklist.SelectedValue = "1";
        ddlDeduct.ClearSelection();
        ddlPicklistType.ClearSelection();
        ddlDateType.SelectedValue = "1";
        txtStartDate.Text = DateTime.Now.AddHours(15).ToShortDateString();
        txtEndDate.Text = DateTime.Now.AddHours(15).ToShortDateString();
        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                GridViewRow gvrow = e.Row;
                Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }

                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = ddlComapny.SelectedItem.Text + " Picklist " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            string[] arrHeader = { "ID", "Picklist Type", "Project No", "Project Status", "Post Code", "Location", "Installer", "Generated Date", "Install Date", "Deduct Date"
                                 , "System Details", "Reason", "CreatedBy" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
}