﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_PendingAudit : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindCheckboxProjectTab();
            BindDropDown();

            ddlCategory.SelectedValue = "1";

            string LastUpdatedOn = ClsDbData.tbl_APIFetchData_UtilitiesLastUpdateDate("Greenbot");
            lblUpdatedMsg.Text = "Fetched till " + LastUpdatedOn;

            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2020";
            //txtEndDate.Text = "30/04/2021";
            //ddlInstaller.SelectedValue = "767415"; //Customer ID // Arise-SolarMiner
            //BindGrid(0);
        }
    }

    public string GetProjectStatus()
    {
        string ProjectStatus = "";
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                ProjectStatus += "," + hdnModule.Value.ToString();
            }
        }
        if (ProjectStatus != "")
        {
            ProjectStatus = ProjectStatus.Substring(1);
        }

        return ProjectStatus;
    }

    public string GetLocation()
    {
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }

        return Location;
    }

    protected DataTable GetGridData()
    {
        string Location = GetLocation();

        string ProjectStatus = GetProjectStatus();

        DataTable dt = GetDataProjectWise(ddlInstaller.SelectedValue);

        AddDataInDtproject(dt);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            DivExcel.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PendingAudit = Convert.ToInt32(dt.Compute("SUM(PendingAudit)", string.Empty));
            int OtherInventory = Convert.ToInt32(dt.Compute("SUM(OtherInventory)", string.Empty));
            int Diff = Convert.ToInt32(dt.Compute("SUM(Diff)", string.Empty));
            int GivenQty = Convert.ToInt32(dt.Compute("SUM(GivenQty)", string.Empty));
            int InstalledQty = Convert.ToInt32(dt.Compute("SUM(InstalledQty)", string.Empty));

            HypPendingAudit.Text = PendingAudit.ToString();
            HypOtherInventory.Text = OtherInventory.ToString();
            HypDiff.Text = Diff.ToString();
            HypGivenQty.Text = GivenQty.ToString();
            HypInstalledQty.Text = InstalledQty.ToString();
        }
        else
        {
            HypPendingAudit.Text = "0";
            HypOtherInventory.Text = "0";
            HypDiff.Text = "0";
            HypGivenQty.Text = "0";
            HypInstalledQty.Text = "0";
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue) || !string.IsNullOrEmpty(txtProjectNumber.Text))
        {
            BindGrid(0);
        }
        else
        {
            Notification("Select Installer..");
        }

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ClearCheckBox();
        ddlInstaller.SelectedValue = "";
        ddlCompany.SelectedValue = "1";

        //ddlProjectStatus.SelectedValue = "";
        ddlIsDifference.SelectedValue = "";
        ddlAudit.SelectedValue = "";
        ddlPicklistCreatedBy.ClearSelection();

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            //Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
            //if (ddlCompany.SelectedValue == "3")
            //{
            //    lblHeaderName.Text = "Customer Name";
            //}
            //else
            //{
            //    lblHeaderName.Text = "Installer Name";
            //}
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        string InstallerName = ddlInstaller.SelectedItem.Text;
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Pending Audit");

                string FileName = InstallerName + "_Pending Audit_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        //rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //rptLocation.DataBind();

        ddlLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1")
        {
            DataTable dtProjectStatus = ClstblProjectStatus.tblProjectStatus_SelectByActive();
            //ddlProjectStatus.DataSource = dtProjectStatus;
            //ddlProjectStatus.DataTextField = "ProjectStatus";
            //ddlProjectStatus.DataValueField = "ProjectStatusID";
            //ddlProjectStatus.DataBind();

            lblProjectStatus.Text = "Project Status";
            rptProjectStatus.DataSource = dtProjectStatus;
            rptProjectStatus.DataBind();


            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("1");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

        }
        else if (ddlCompany.SelectedValue == "2")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("2");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "CompanyName";
            ddlInstaller.DataValueField = "UserId";
            ddlInstaller.DataBind();
        }

        ddlPicklistCreatedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlPicklistCreatedBy.DataValueField = "EmployeeID";
        ddlPicklistCreatedBy.DataMember = "fullname";
        ddlPicklistCreatedBy.DataTextField = "fullname";
        ddlPicklistCreatedBy.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }

    protected DataTable GetDataProjectWise(string InstallerID)
    {
        string StockItemID = "";
        string ProjectNoSearch = txtProjectNumber.Text;
        string CompanyID = ddlCompany.SelectedValue;
        string StartDate = txtStartDate.Text;
        string EndDate = txtEndDate.Text;
        string ProjectStatus = GetProjectStatus();
        string Category = ddlCategory.SelectedValue;
        string DateType = ddlDateType.SelectedValue;
        string CompanyLocation = ddlLocation.SelectedValue;
        string PicklistCreatedBy = ddlPicklistCreatedBy.SelectedValue;

        DataTable dt = new DataTable();

        if (CompanyID == "1" || CompanyID == "4")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }
        else if (CompanyID == "2")
        {
            dt = ClsReportsV2.SerialInstallerDetailsProjV2SM(CompanyID, ProjectNoSearch, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }


        if (dt.Rows.Count > 0)
        {
            dt.Columns["MissingQty"].ColumnName = "PendingAudit";
            dt.Columns["SerialNo"].ColumnName = "ScanYN";
            dt.Columns["NameOrConsignmentNo"].ColumnName = "PickupBy";

            //Hide Column
            dt.Columns.Remove("Installation");
            dt.Columns.Remove("DeductOn");
            dt.Columns.Remove("ScanFilter");
            dt.Columns.Remove("PVDStatus");
            dt.Columns.Remove("PickupBy");
            dt.Columns.Remove("PickID");

            dt.Columns.Remove("PNo");
            dt.Columns.Remove("StockItemID");
            dt.Columns.Remove("StockCategoryID");
            dt.Columns.Remove("BSGBFlag");
            dt.Columns.Remove("ID");
            dt.Columns.Remove("DeductBy");
            dt.Columns.Remove("CompanyLocation");
        }

        return dt;
    }

    protected void AddDataInDtproject(DataTable dtProjectWise)
    {
        try
        {
            // Add New
            for (int i = 0; i < dtProjectWise.Rows.Count; i++)
            {
                string ProjectNo = dtProjectWise.Rows[i]["Projectnumber"].ToString();

                DataTable dtProject = ClsReportsV2.SP_TrackSerialNo_Project(ddlCompany.SelectedValue, ProjectNo, ddlCategory.SelectedValue);

                for (int j = 0; j < dtProject.Rows.Count; j++)
                {
                    string ColumnName1 = "Project " + (j + 1);
                    //string ColumnName2 = "Pick ID " + (j + 1);
                    string ColumnName3 = "No Panel " + (j + 1);
                    string ColumnName4 = "Project Status " + (j + 1);
                    DataColumnCollection columns = dtProjectWise.Columns;
                    DataColumn dcProject = new DataColumn(ColumnName1, typeof(Int32));
                    //DataColumn dcID = new DataColumn(ColumnName2, typeof(Int32));
                    DataColumn dcPanel = new DataColumn(ColumnName3, typeof(Int32));
                    DataColumn dcProjectStatus = new DataColumn(ColumnName4, typeof(string));
                    if (!columns.Contains(ColumnName1))
                    {
                        dtProjectWise.Columns.Add(dcProject);
                        //dtProjectWise.Columns.Add(dcID);
                        dtProjectWise.Columns.Add(dcPanel);
                        dtProjectWise.Columns.Add(dcProjectStatus);
                    }

                    //string ProjectNumber = dtProject.Rows[j]["ProjectNo"].ToString();
                    dtProjectWise.Rows[i][ColumnName1] = dtProject.Rows[j]["Projectnumber"];
                    //dtProjectWise.Rows[i][ColumnName2] = dtProject.Rows[j]["ID"];
                    dtProjectWise.Rows[i][ColumnName3] = dtProject.Rows[j]["UsedQty"];
                    dtProjectWise.Rows[i][ColumnName4] = dtProject.Rows[j]["ProjectStatus"];
                }
            }
        }
        catch (Exception ex)
        {

        }
        //return dtProjectWise;
    }
}