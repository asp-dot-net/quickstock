﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_ManageReturnStock : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    protected static string Siteurl;
    protected static string userid;
    protected static string EmpID;

    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            EmpID = ClstblEmployees.tblEmployees_SelectByUserId(userid).EmployeeID;

            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropdown();

            if(Roles.IsUserInRole("WholeSale"))
            {
                ddlCompany.SelectedValue = "3";
                ddlCompany.Enabled = false;
                ddlSearchCompany.SelectedValue = "3";
                DivSearchCompany.Visible = false;

                lblProjectNumber.InnerText = "Invoice Number";
                lblToProjectNumber.InnerText = "Invoice Number";
                txtProjectNo.Attributes.Add("placeholder", "Invoice No.");
                txtToProjetNo.Attributes.Add("placeholder", "To Invoice No.");

                TextBoxWatermarkExtender2.WatermarkText = "Invoice No.";
                txtSerachProjectNo.Attributes.Add("placeholder", "Invoice No.");
            }

            BindInstallerCustomer();
            BindGrid(0);
        }

    }

    public void BindDropdown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "location";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataMember = "CompanyLocationID";
        ddlLocation.DataBind();

        //ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlInstaller.DataValueField = "ContactID";
        //ddlInstaller.DataMember = "Contact";
        //ddlInstaller.DataTextField = "Contact";
        //ddlInstaller.DataBind();

        ddlAssignToNew.DataSource = ClsReportsV2.tblPendingAssign_GetData(); ;
        ddlAssignToNew.DataValueField = "ID";
        ddlAssignToNew.DataTextField = "Department";
        ddlAssignToNew.DataBind();

        ddlEmployees.DataSource = ClstblEmployees.tblEmployees_Select();
        ddlEmployees.DataValueField = "EmployeeID";
        ddlEmployees.DataTextField = "fullname";
        ddlEmployees.DataBind();
    }

    protected DataTable GetGridData1()
    {
        DataTable dt1 = ClstblManageReturnStock.tbl_ManageReturnStock_GetData(txtSerachProjectNo.Text, ddlSearchStatus.SelectedValue, ddlSearchCategory.SelectedValue, ddlLocation.SelectedValue, ddlInstaller.SelectedValue, ddlSearchCompany.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "StockQuery_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 0, 1, 14, 17, 2, 3, 5,
                          6, 7, 8, 9, 10, 11, 12, 13, 21,
                          22, 20, 23, 25, 26 };

        string[] arrHeader = { "ID", "Project No", "Picklist ID", "To Proj. No", "Project Status", "Location", "Installer",
            "System Details", "Category", "Panles", "Inverter", "Notes", "Status", "Entered On", "Entered By", "Picklist Gen.",
            "Assign To", "Status Notes", "New Notes", "Employee Notes", "AllEmployee Notes" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSerachProjectNo.Text = "";
        ddlSearchStatus.SelectedValue = "New";
        ddlSearchCategory.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlInstaller.SelectedValue = "";

        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "UpdateStatus")
        {
            ddlStatus.SelectedValue = "";
            txtStutusNotes.Text = "";

            string arg = e.CommandArgument.ToString();
            hndQueryID.Value = arg;
            ModalPopupExtenderUpdateStatus.Show();
        }

        if (e.CommandName == "Delete")
        {
            string arg = e.CommandArgument.ToString();
            hndDeleteID.Value = arg;
            ModalPopupExtenderDelete.Show();
        }

        if (e.CommandName == "NewNote")
        {
            try
            {
                ddlAssignToNew.SelectedValue = "";
                ddlEmployees.SelectedValue = "";
                txtNotesNew.Text = "";
                hndNewNotesPicklistID.Value = "";
                hndNewQueryID.Value = "";
                hndMode.Value = "";

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                hndNewNotesPicklistID.Value = arg[0];
                hndNewQueryID.Value = arg[1];

                BindNewAriseNotes(arg[0], userid, arg[1]);

                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkQueryStatus = (LinkButton)e.Row.FindControl("lnkQueryStatus");

            if(Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Quick Stock") || EmpID == "1")
            {
                lnkQueryStatus.Enabled = true;
            }
            else
            {
                lnkQueryStatus.Enabled = false;
            }
        }
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAddUpdate.Visible = false;
        Panel4.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();
        Reset();
        PanGrid.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        btnSave.Visible = true;
        btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        HidePanels();
        //Reset();
        PanGrid.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        btnSave.Visible = false;
        btnUpdate.Visible = true;
        //btnReset.Visible = true;
        //btnCancel.Visible = true;

        lblAddUpdate.Text = "Update ";
    }

    public void Reset()
    {
        DivToProjectNo.Visible = false;
        ddlCategory.SelectedValue = "";
        txtProjectNo.Text = "";
        txtPanels.Text = "0";
        txtInverter.Text = "0";
        txtNotes.Text = "";
        txtToProjetNo.Text = "";

        if(ddlCompany.Enabled == true)
        {
            ddlCompany.SelectedValue = "";
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Reset();
        HidePanels();
        BindGrid(0);
    }

    protected void lnkReset_Click(object sender, EventArgs e)
    {
        InitAdd();
        Reset();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Category = ddlCategory.SelectedValue;
        string[] no = txtProjectNo.Text.Split('/');
        string ProjectNo = no[0];
        string PickListID = no[1];

        string Panels = txtPanels.Text;
        string Inverter = txtInverter.Text;
        string Notes = txtNotes.Text;
        string UpdatedBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string UpdatedOn = System.DateTime.Now.AddHours(14).ToString();

        string ToProjectNo = "";
        string ToPickListID = "";
        if (txtToProjetNo.Text != "")
        {
            if(txtToProjetNo.Text.Contains('/'))
            {
                string[] Tono = txtToProjetNo.Text.Split('/');
                ToProjectNo = Tono[0];
                ToPickListID = Tono[1];
            }
            else
            {
                ToProjectNo = txtToProjetNo.Text;
            }
            
        }

        int Insert = ClstblManageReturnStock.tbl_ManageReturnStock_Insert(Category, ProjectNo, Panels, Inverter, Notes, UpdatedOn, UpdatedBy, PickListID, ToProjectNo, ToPickListID);

        if (Insert > 0)
        {
            ClstblManageReturnStock.tbl_ManageReturnStock_Update_QueryStatus(Insert.ToString(), "New");
            ClstblManageReturnStock.tbl_ManageReturnStock_Update_CompanyID(Insert.ToString(), ddlCompany.SelectedValue);

            SetAdd();
            BindGrid(0);
        }
        else
        {
            Reset();
            SetError1();
        }
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        string ID = hndQueryID.Value;

        if(ID != "")
        {
            string SolvedDate = System.DateTime.Now.AddHours(14).ToString();
            bool SuccStstus = ClstblManageReturnStock.tbl_ManageReturnStock_Update_QueryStatus(ID, ddlStatus.SelectedValue);
            bool SuccStstusNotes = ClstblManageReturnStock.tbl_ManageReturnStock_Update_StatusNotes(ID, txtStutusNotes.Text);
            bool SuccCompanyID = ClstblManageReturnStock.tbl_ManageReturnStock_Update_CompanyID(ID, ddlCompany.SelectedValue);
            bool SuccSolvedDate = ClstblManageReturnStock.tbl_ManageReturnStock_Update_SolvedDate(ID, SolvedDate);

            if (SuccStstus)
            {
                Notification("Transaction Successful.");
                BindGrid(0);
            }
            else
            {
                SetError1();
            }
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
        if (ddlCategory.SelectedValue == "Transfer")
        {
            DivToProjectNo.Visible = true;
        }
        else
        {
            DivToProjectNo.Visible = false;
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string ID = hndDeleteID.Value;

        if(ID != "")
        {
            bool Delete = ClstblManageReturnStock.tbl_ManageReturnStock_DeleteByID(ID);

            if(Delete)
            {
                SetAdd1();
                BindGrid(0);
            }
            else
            {
                SetError1();
            }
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        DataTable dt = ClstblManageReturnStock.tbl_ManageReturnStock_GetDatByID(id);

        if(dt.Rows.Count > 0)
        {
            ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
            ddlCategory.SelectedValue = dt.Rows[0]["Category"].ToString();
            if(dt.Rows[0]["Category"].ToString() == "Transfer")
            {
                DivToProjectNo.Visible = true;
                txtToProjetNo.Text = dt.Rows[0]["ToProjectNo"].ToString() + "/" + dt.Rows[0]["ToPickListID"].ToString();
            }
            else
            {
                DivToProjectNo.Visible = false;
            }

            txtProjectNo.Text = dt.Rows[0]["ProjectNo"].ToString() + "/" + dt.Rows[0]["PicklistID"].ToString();

            txtPanels.Text = dt.Rows[0]["Panles"].ToString();
            txtInverter.Text = dt.Rows[0]["Inverter"].ToString();
            txtNotes.Text = dt.Rows[0]["Notes"].ToString();

            InitUpdate();
        }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string Id = GridView1.SelectedDataKey.Value.ToString();

        string Category = ddlCategory.SelectedValue;
        string[] no = txtProjectNo.Text.Split('/');
        string ProjectNo = no[0];
        string PickListID = no[1];

        string Panels = txtPanels.Text;
        string Inverter = txtInverter.Text;
        string Notes = txtNotes.Text;
        string UpdatedBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string UpdatedOn = System.DateTime.Now.AddHours(14).ToString();

        string ToProjectNo = "";
        string ToPickListID = "";
        if (txtToProjetNo.Text != "")
        {
            string[] Tono = txtToProjetNo.Text.Split('/');
            ToProjectNo = Tono[0];
            ToPickListID = Tono[1];
        }
        


        if (!string.IsNullOrEmpty(ID))
        {
            bool Update = ClstblManageReturnStock.tbl_ManageReturnStock_Update(Id, Category, ProjectNo, Panels, Inverter, Notes, UpdatedOn, UpdatedBy, PickListID, ToProjectNo, ToPickListID);

            if (Update)
            {
                Reset();
                SetAdd();
                //BindGrid(0);
            }
            else
            {
                Reset();
                SetError1();
            }
        }

        BindGrid(0);
    }

    public void BindNewAriseNotes(string PicklistID, string UserID, string QueryID)
    {
        DataTable dt = ClsReportsV2.tbl_StockQueryNotes_GetDataByID(PicklistID, "1", UserID, QueryID);
        if (dt.Rows.Count > 0)
        {
            DivNewNotes.Visible = true;
            RptNewNotes.DataSource = dt;
            RptNewNotes.DataBind();
        }
        else
        {
            DivNewNotes.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNewNotes.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            DivNewNotes.Attributes.Add("style", "");
        }
    }

    protected void RptNewNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNewNote")
        {
            try
            {
                DataTable dt = ClsReportsV2.tbl_StockQueryNotes_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    ddlAssignToNew.SelectedValue = "";
                    ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndID.Value = ID;
                    ddlAssignToNew.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                    ddlEmployees.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                    txtNotesNew.Text = dt.Rows[0]["Notes"].ToString();

                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNewNotes")
        {
            try
            {
                bool del = ClsReportsV2.tbl_StockQueryNotes_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindNewAriseNotes(hndNewNotesPicklistID.Value, userID, hndNewQueryID.Value);
                ModalPopupExtenderNewNote.Show();
                BindGrid(0);
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNote.Show();
    }

    protected void lbtnSaveNewNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string PickListID = hndNewNotesPicklistID.Value;
            string QueryID = hndNewQueryID.Value;
            string EnteredOn = DateTime.Now.AddHours(14).ToShortDateString();
            string EnteredBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            if (hndMode.Value != "Update")
            {
                if (PickListID != "")
                {
                    int ID = ClsReportsV2.tbl_StockQueryNotes_Insert(QueryID, PickListID, ddlSearchCompany.SelectedValue, EnteredOn, EnteredBy);
                    if (ID != 0)
                    {
                        bool suc = ClsReportsV2.tbl_StockQueryNotes_Update(ID.ToString(), ddlAssignToNew.SelectedValue, txtNotesNew.Text);
                        bool sucEmployeeID = ClsReportsV2.tbl_StockQueryNotes_Update_EmployeeID(ID.ToString(), ddlEmployees.SelectedValue);

                        SetAdd1();
                        ddlAssignToNew.SelectedValue = "";
                        ddlEmployees.SelectedValue = "";
                        txtNotesNew.Text = "";

                        BindNewAriseNotes(PickListID, EnteredBy, QueryID);
                        ModalPopupExtenderNewNote.Show();

                        BindGrid(0);
                    }
                }
            }
            else
            {
                string ID = hndID.Value;
                if (ID != "")
                {
                    bool suc = ClsReportsV2.tbl_StockQueryNotes_Update(ID, ddlAssignToNew.SelectedValue, txtNotesNew.Text);
                    bool sucEmployeeID = ClsReportsV2.tbl_StockQueryNotes_Update_EmployeeID(ID, ddlEmployees.SelectedValue);

                    SetAdd1();

                    ddlAssignToNew.SelectedValue = "";
                    ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndMode.Value = "";

                    BindNewAriseNotes(PickListID, EnteredBy, QueryID);
                    ModalPopupExtenderNewNote.Show();

                    BindGrid(0);
                }
                //hndMode.Value = "";
            }
        }
        catch (Exception ex)
        {
            Notification("Inner Exception");
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        if (ddlCompany.SelectedValue == "1")
        {
            lblProjectNumber.InnerText = "Project Number";
            lblToProjectNumber.InnerText = "Project Number";
            txtProjectNo.Attributes.Add("placeholder", "Project No.");
            txtToProjetNo.Attributes.Add("placeholder", "To Project No.");
        }
        else if(ddlCompany.SelectedValue == "2")
        {
            lblProjectNumber.InnerText = "Project Number";
            lblToProjectNumber.InnerText = "Project Number";
            txtProjectNo.Attributes.Add("placeholder", "Project No.");
            txtToProjetNo.Attributes.Add("placeholder", "To Project No.");
        }
        else if(ddlCompany.SelectedValue == "4")
        {
            lblProjectNumber.InnerText = "Project Number";
            lblToProjectNumber.InnerText = "Project Number";
            txtProjectNo.Attributes.Add("placeholder", "Project No.");
            txtToProjetNo.Attributes.Add("placeholder", "To Project No.");
        }
        else
        {
            lblProjectNumber.InnerText = "Invoice Number";
            lblToProjectNumber.InnerText = "Invoice Number";
            txtProjectNo.Attributes.Add("placeholder", "Invoice No.");
            txtToProjetNo.Attributes.Add("placeholder", "To Invoice No.");
        }
    }

    protected void BindInstallerCustomer()
    {
        if (ddlSearchCompany.SelectedValue == "1" || ddlSearchCompany.SelectedValue == "2" || ddlSearchCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Customer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtCustomer = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlInstaller.DataSource = dtCustomer;
            ddlInstaller.DataTextField = "Customer";
            ddlInstaller.DataValueField = "CustomerID";
            ddlInstaller.DataBind();
        }
    }

    protected void ddlSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindInstallerCustomer();
    }
}