﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockAction.aspx.cs" Inherits="admin_adminfiles_ReportsV2_StockAction"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .height100 {
            height: 100px;
        }
        /*.table tbody .brd_ornge td {
            border-bottom: 4px solid #ff784f;
        }*/
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <style>
        .btn_right {
            float: right;
        }

            .btn_right li {
                float: left;
                padding-left: 15px;
                font-size: 12px;
                font-weight: 400;
            }

                .btn_right li div {
                    max-width: 140px;
                    line-height: 15px;
                }

        .font_Custom {
            font-size: 15px;
        }

        .padd_btm10 {
            padding-bottom: 15px;
        }
    </style>

    <script type="text/javascript">

        function doMyAction() {

           <%-- $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();
            });--%>
        }

        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".Location .dropdown dt a").on('click', function () {
                $(".Location .dropdown dd ul").slideToggle('fast');
            });
            $(".Location .dropdown dd ul li a").on('click', function () {
                $(".Location .dropdown dd ul").hide();
            });

            //callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });

            callMultiCheckbox1();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }
        }

    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Action Report
                        <%--<div class="pull-right">
                            <ul class="btn_right">
                                <li>
                                    <asp:Button Text="Fetch SM Data" ID="btnUpdateSMData" runat="server" CssClass="btn btn-warning" OnClick="btnUpdateSMData_Click" />
                                    <div><asp:Label runat="server" ID="lblSMData"></asp:Label></div> 
                                </li>
                                <li>
                                    <asp:Button Text="Fetch Data" ID="btnFatchData" runat="server" CssClass="btn btn-warning" OnClick="btnFatchData_Click" />
                                    <div><asp:Label runat="server" ID="lblData"></asp:Label></div>
                                </li>
                            </ul>
                        </div>--%>

                        <div class="pull-right">
                            <%--<asp:Button Text="Fetch SM Data" ID="btnUpdateSMData" runat="server" CssClass="btn btn-warning" OnClick="btnUpdateSMData_Click" />
                            <asp:Label runat="server" ID="lblSMData" CssClass="font_Custom"></asp:Label>
                            <br />
                            <asp:Button Text="Fetch Data" ID="btnFatchData" runat="server" CssClass="btn btn-warning" OnClick="btnFatchData_Click" />
                            <asp:Label runat="server" ID="lblData" CssClass="font_Custom"></asp:Label>--%>
                        </div>

                    </h5>
                </div>
            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                            WatermarkText="Stock Item" />
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtStockModel" runat="server" placeholder="Stock Model" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtStockModel"
                                                            WatermarkText="Stock Model" />
                                                    </div>
                                                    <div class="input-group col-sm-2 martop5 max_width170" style="display: none;">
                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="false" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group spical multiselect Location martop5 col-sm-2 max_width170 specail1_select">
                                                        <dl class="dropdown ">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida" id="spanselect">Location</span>
                                                                    <p class="multiSel"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddLocation" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="rptLocation" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                    <asp:HiddenField ID="hdnCompanyLocationID" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <%-- </span>--%>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltALocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170 dnone">
                                                        <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="0" Selected="True">Stock Active</asp:ListItem>
                                                            <asp:ListItem Value="1">Only Active</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Active</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <%--<div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:DropDownList ID="ddltype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="" Selected="True">Select All/Receive</asp:ListItem>
                                                            <asp:ListItem Value="1">All</asp:ListItem>
                                                            <asp:ListItem Value="2">Receive</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>--%>

                                                    <div class="input-group col-sm-2 martop5 max_width170 dnone">
                                                        <asp:DropDownList ID="ddlYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="0"></asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <%--<div class="input-group col-sm-2 martop5 max_width170" style="display: none;">
                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">Ordered Date</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div>
                <div class="card shadownone brdrgray xsroll" id="divtot" runat="server">
                    <div class="card-block">
                        <div class="table-responsive BlockStructure">
                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 640px;"><a href="#">Stock Item</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">Live Qty</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">Stk Order</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">TD</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">ETA</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">Third Party</a></th>

                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">AS Sold</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">SM Sold</a></th>
                                        <%--<th class="brdrgrayleft" align="center" scope="col"><a href="#">W. Draft</a></th>--%>
                                        <%--<th class="brdrgrayleft" align="center" scope="col"><a href="#">Unsold Stock</a></th>--%>

                                        <%-- <th class="brdrgrayleft" align="center" scope="col"><a href="#">A. Deduct</a></th>--%>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">W. Deduct</a></th>
                                        <th class="brdrgrayleft dnone" align="center" scope="col"><a href="#">D. Picklist</a></th>
                                        <%--<th class="brdrgrayleft" align="center" scope="col"><a href="#">Transfer</a></th>

                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">A. Picklist</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">SM Picklist</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">W. Invoice</a></th>

                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">Reserved</a></th>
                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#">Stock on hand</a></th>--%>

                                        <th class="brdrgrayleft" align="center" scope="col"><a href="#" data-original-title="((StockQuantity + ThirdPartyStock) - ((AriseSold + SMSold + WholesaleDeduct) / 4))" data-toggle="tooltip" data-placement="top">Action</a></th>

                                    </tr>
                                    <tr class="brd_ornge">
                                        <td align="left" valign="top">Total</td>
                                        <td align="left" valign="top">
                                            <asp:Literal ID="totlblCurrentqty" runat="server"></asp:Literal></td>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblStockOrdered" runat="server"></asp:Literal>--%>
                                            <%--<asp:LinkButton Text="text" runat="server" ID="totlblStockOrdered" Enabled="false" OnClick="totlblStockOrdered_Click" />--%>
                                            <asp:HyperLink runat="server" ID="totlblStockOrdered" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblTD" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="hypTD" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblExpectedOrdered" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="totlblExpectedOrdered" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblExpectedOrdered" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="totlblthirdParty" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblAriseSold" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="totlblAriseSold" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%-- <asp:Literal ID="totlblSMSold" runat="server"></asp:Literal>
                                            <asp:Literal ID="totlblSMSold1" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="totlblSMSold" runat="server" Enabled="false" />
                                        </td>
                                        <%--<td align="left" valign="top">
                                            <asp:HyperLink ID="totlblWholesaleDraft" runat="server" Enabled="false" />
                                        </td>--%>

                                        <%--<td align="left" valign="top">
                                            <asp:Literal ID="totlblTot" runat="server"></asp:Literal></td>--%>

                                        <%--<td align="left" valign="top">
                                            <asp:HyperLink ID="tothypAriseDeduct" runat="server" Enabled="false" />
                                        </td>--%>
                                        <td align="left" valign="top">
                                            <%--<asp:Literal ID="totlblWholesaleDraft" runat="server"></asp:Literal>--%>
                                            <asp:HyperLink ID="tothypWholesaleDeduct" runat="server" Enabled="false" />
                                        </td>
                                        <%-- <td align="left" valign="top" class="dnone">
                                            <asp:HyperLink ID="totlblDraftPickList" runat="server" Enabled="false" />
                                        </td>--%>
                                        <%-- <td align="left" valign="top">
                                            <asp:Literal ID="totlblStockTransfer" runat="server"></asp:Literal></td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="totlblArisePicklist" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="totlblSMPicklist" runat="server" Enabled="false" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="totlblWholesaleInvoice" runat="server" Enabled="false" />
                                        </td>

                                        <td align="left" valign="top">
                                            <asp:Literal ID="totlblReserved" runat="server"></asp:Literal></td>
                                        
                                        <td align="left" valign="top">
                                            <asp:Literal ID="totlblNetTot" runat="server"></asp:Literal></td>--%>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypAction" runat="server" Enabled="false" />
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("StockItemID").ToString() %>','tr<%# Eval("StockItemID").ToString() %>');">
                                                            <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />--%>
                                                            <img id='imgdiv<%# Eval("StockItemID").ToString() %>' src="../../../images/icon_plus.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Item" SortExpression="StockItem">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndStockitemID" runat="server" Value='<%#Eval("StockItemID")%>' />
                                                        <asp:HiddenField ID="hdnCompanyLocationID" runat="server" Value='<%#Eval("CompanyLocationID")%>' />
                                                        <asp:HiddenField ID="hdnCompanyLocation" runat="server" Value='<%#Eval("CompanyLocation")%>' />
                                                        <asp:HiddenField ID="hdnAriseSold" runat="server" Value='<%#Eval("AriseSold")%>' />

                                                        <asp:HiddenField ID="hdnCurrentQty" runat="server" Value='<%#Eval("StockQuantity")%>' />

                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                        <asp:Label ID="Label11" runat="server" Text='<%#Eval("StockItem")%>'
                                                            data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StockItem")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Model" SortExpression="StockModel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label44" runat="server" Text='<%#Eval("StockModel")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Size" SortExpression="StockSize">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label47" runat="server" Width="20px" Text='<%#Eval("StockSize")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label67" runat="server" Width="20px" Text='<%#Eval("CompanyLocation")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="25px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Retail Status" SortExpression="Active">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Active")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Live Qty" SortExpression="StockQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label94" runat="server" Width="10px" Text='<%#Eval("StockQuantity")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stk Order" SortExpression="StockQtyOrdered">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label452" runat="server" Width="20px" Visible="false" Text='<%#Eval("StockQtyOrdered")%>'>
                                                        </asp:Label>
                                                        <%--<asp:LinkButton ID="btnStockQtyOrdered" runat="server" CommandName="stockordered" CommandArgument='<%#Eval("StockItemID") +";"+ Eval("CompanyLocationID")%>'
                                                            CausesValidation="false" data-original-title="Stock Ordered" data-toggle="tooltip" data-placement="top" Text='<%#Eval("StockQtyOrdered")%>'>                                              
                                                        </asp:LinkButton>--%>
                                                        <asp:HyperLink runat="server" ID="btnStockQtyOrdered" CommandName="DetailsStockOrder" CommandArgument='<%#Eval("StockItemID")%>'
                                                            data-toggle="tooltip" data-placement="top" title="View StockQtyOrdered" Style="float: left;" data-original-title="View" Text='<%#Eval("StockQtyOrdered")%>'
                                                            Enabled='<%#Eval("StockQtyOrdered").ToString()=="0"?false:true %>'>                                                                         
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TD" SortExpression="TargetDate">
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="btnTargetDate" CommandName="DetailsStockOrder" CommandArgument='<%#Eval("StockItemID")%>'
                                                            data-toggle="tooltip" data-placement="top" title="View StockQtyOrdered" Style="float: left;" data-original-title="View" Text='<%#Eval("TargetDate")%>'
                                                            Enabled='<%#Eval("TargetDate").ToString()=="0"?false:true %>'>                                                                         
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ETA" SortExpression="ETA">
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="btnExpectedStockOrder" CommandName="DetailsStockOrder" CommandArgument='<%#Eval("StockItemID")%>'
                                                            data-toggle="tooltip" data-placement="top" title="View StockQtyOrdered" Style="float: left;" data-original-title="View" Text='<%#Eval("ETA")%>'
                                                            Enabled='<%#Eval("ETA").ToString()=="0"?false:true %>'>                                                                         
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Third Party" SortExpression="ThirdPartyStock">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="btnThirdPartyStock" CommandName="DetailsThirdPartyStock" CommandArgument='<%#Eval("StockItemID")%>'
                                                            data-toggle="tooltip" data-placement="top" title="View Third Party Stock" Style="float: left;" data-original-title="View" Text='<%#Eval("ThirdPartyStock")%>'
                                                            Enabled='<%#Eval("ThirdPartyStock").ToString()=="0"?false:true %>'>                                                                         
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="AS. Sold" SortExpression="AriseSold">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label52" runat="server" Width="20px" Visible="false" Text='<%#Eval("AriseSold")%>'>
                                                        </asp:Label>

                                                        <asp:HyperLink runat="server" ID="btnStockSold" CommandName="DetailsbtnStockSold" CommandArgument='<%#Eval("StockItemID")%>'
                                                            data-toggle="tooltip" data-placement="top" title="View Arise StockSold" Style="float: left;" data-original-title="View" Text='<%#Eval("AriseSold")%>'
                                                            Enabled='<%#Eval("AriseSold").ToString()=="0"?false:true %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SM Sold" SortExpression="SMSold">
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="lblSMStockSold" Text='<%# Eval("SMSold") %>' Enabled='<%#Eval("SMSold").ToString()=="0"?false:true %>'
                                                            data-toggle="tooltip" data-placement="top" title="View SM StockSold" Style="float: left;" data-original-title="View"></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="W. Draft" SortExpression="WholesaleDraft">
                                                    <ItemTemplate>

                                                        <asp:HyperLink runat="server" ID="lblWDraft" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("WholesaleDraft").ToString()=="0"?false:true %>'
                                                            title="View Wholesale Draft" Style="float: left;" data-original-title="View" Text='<%# Eval("WholesaleDraft") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%-- <asp:TemplateField HeaderText="Unsold Stock" SortExpression="Total">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTotal11" runat="server" Width="20px" Text='<%# Eval("Total") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%--<asp:TemplateField HeaderText="A. Deduct" SortExpression="AriseDeduct">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblADeduct" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("AriseDeduct").ToString()=="0"?false:true %>'
                                                            title="View Arise Deduct" Style="float: left;" data-original-title="View" Text='<%# Eval("AriseDeduct") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="W. Deduct" SortExpression="WholesaleDeduct">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblWDeduct" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("WholesaleDeduct").ToString()=="0"?false:true %>'
                                                            title="View Wholesale Deduct" Style="float: left;" data-original-title="View" Text='<%# Eval("WholesaleDeduct") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="D. Picklist" SortExpression="DraftPickList" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblDraftPickList" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("DraftPickList").ToString()=="0"?false:true %>'
                                                            title="View Draft Picklist" Style="float: left;" data-original-title="View" Text='<%# Eval("DraftPickList") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%--<asp:TemplateField HeaderText="Transfer" SortExpression="StockTransferOut">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblStockTransferOut" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("StockTransferOut").ToString()=="0"?false:true %>'
                                                            title="View Stock Transfer" Style="float: left;" data-original-title="View" Text='<%# Eval("StockTransferOut") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%-- <asp:TemplateField HeaderText="A. Picklist" SortExpression="ArisePicklist">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblArisePicklist" Text='<%# Eval("ArisePicklist") %>' Enabled='<%#Eval("ArisePicklist").ToString()=="0"?false:true %>'
                                                            data-toggle="tooltip" data-placement="top" title="View Arise Picklist" Style="float: left;" data-original-title="View"></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SM. Picklist" SortExpression="SMPicklist">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblSolarMinerPicklist" Text='<%# Eval("SMPicklist") %>' Enabled='<%#Eval("SMPicklist").ToString()=="0"?false:true %>'
                                                            data-toggle="tooltip" data-placement="top" title="View Arise Picklist" Style="float: left;" data-original-title="View"></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="W. Invoice" SortExpression="WholesaleInvoice">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="lblWInvoice" data-toggle="tooltip" data-placement="top" Enabled='<%#Eval("WholesaleInvoice").ToString()=="0"?false:true %>'
                                                            title="View Wholesale Invoice" Style="float: left;" data-original-title="View" Text='<%# Eval("WholesaleInvoice") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reserved" SortExpression="Reserved">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="hypReserved"
                                                            Style="float: left;" data-original-title="View" Text='<%# Eval("Reserved") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%--<asp:TemplateField HeaderText="Stock on Hand" SortExpression="NetTotal">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="hypNetTotal"
                                                            Style="float: left;" data-original-title="View" Text='<%# Eval("NetTotal") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <%--<asp:TemplateField HeaderText="W. Status">
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="UpdateWholeSaleTag"
                                                            CommandArgument='<%# Eval("StockItemID") + ";" + Eval("CompanyLocationID") %>' Visible='<%# Eval("WholeSaleTag").ToString() == "False" ? true : false %>'
                                                            CausesValidation="false" data-original-title="Not Verify" data-toggle="tooltip" data-placement="top">                                                                          
                                                                            <i class="btn-label fa fa-close"></i> Close
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="gvbtnVerify1" runat="server" CssClass="btn btn-success btn-mini" CommandName="UpdateWholeSaleTag"
                                                            CommandArgument='<%# Eval("StockItemID") + ";" + Eval("CompanyLocationID") %>' Visible='<%# Eval("WholeSaleTag").ToString() == "True" ? true : false %>'
                                                            CausesValidation="false" data-original-title="Verify" data-toggle="tooltip" data-placement="top">                                                                          
                                                                            <i class="btn-label fa fa-check"></i> Open
                                                        </asp:LinkButton>

                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="10px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField SortExpression="Action">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderAction" runat="server" data-original-title="((StockQuantity + ThirdPartyStock) - ((AriseSold + SMSold + WholesaleDeduct) / 4))" data-toggle="tooltip" data-placement="top">
                                                            Action
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAction" runat="server" Text='<%# Eval("Action") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Expiry Date" SortExpression="ExpiryDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Eval("ExpiryDate") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <!--Start Add New Notes -->
                                                        <asp:LinkButton ID="gvlnkNotes" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Notes"
                                                            CommandArgument='<%#Eval("StockItemID") + ";" + Eval("CompanyLocationID") %>' CausesValidation="false">
                                                            <i class="btn-label fa fa-edit"></i> Note
                                                        </asp:LinkButton>
                                                        <!--End Add New Notes -->
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("StockItemID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("StockItemID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td style="width: 50px;"><b>Notes</b>
                                                                            </td>
                                                                            <td>
                                                                                <%-- <asp:Label ID="lblSystemDetails" runat="server" Width="50px"
                                                                            data-toggle="tooltip" data-placement="Top" title="" data-original-title='<%#Eval("SystemDetails")%>' >
                                                                                                    <%#Eval("SystemDetails")%></asp:Label>--%>
                                                                                <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <!-- Start New Notes Popup-->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderNewNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="ModelNewNote" TargetControlID="Button7"
                CancelControlID="LinkButton9">
            </cc1:ModalPopupExtender>
            <div id="ModelNewNote" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalNewNote">Project Note
                                <span style="float: right" class="printorder" />
                                <asp:LinkButton ID="LinkButton14" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <asp:HiddenField runat="server" ID="hndStockItemID" />
                                                <asp:HiddenField runat="server" ID="hndCompanyLocationID" />
                                                <%--<div class="input-group col-sm-6 padd_btm10">
                                                    <asp:DropDownList ID="ddlAssignToNew" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                        <asp:ListItem Value="">Assign To</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-6 padd_btm10">
                                                    <asp:DropDownList ID="ddlEmployees" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem form-control">
                                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtNotesNew" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hndMode" />
                                                <asp:HiddenField runat="server" ID="hndID" />
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="lbtnSaveNewNotes" CssClass="btn btn-info" CausesValidation="false" runat="server" Text="Save" OnClick="lbtnSaveNewNotes_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10" runat="server" id="DivNewNotes">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Entered On</th>
                                                            <th>Entered By</th>
                                                            <th>Notes</th>
                                                            <th></th>
                                                        </tr>
                                                        <asp:Repeater ID="RptNewNotes" runat="server" OnItemCommand="RptNewNotes_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%# Container.ItemIndex + 1 %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("EnteredOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("EnteredBy") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("Notes") %>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lbtnEditNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="EditNewNote" CommandArgument='<%#Eval("ID") %>'
                                                                            CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                                                            <i class="btn-label fa fa-pencil"></i> Edit
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="DeleteNewNotes"
                                                                            CommandArgument='<%# Eval("ID") %>' Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'
                                                                            CausesValidation="false" data-original-title="Not Verify" data-toggle="tooltip" data-placement="top"
                                                                            OnClientClick="return confirm('Are you sure you want to delete?');">                                                                          
                                                                            <i class="btn-label fa fa-close"></i> Delete
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />
            <!--End New Notes Popup-->

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="totlblStockOrdered" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
