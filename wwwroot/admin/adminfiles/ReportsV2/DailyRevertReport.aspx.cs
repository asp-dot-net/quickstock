﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_DailyRevertReport : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropDown();
            BindInstallerCustomer();
            BindStatus();
            BindDateType();

            ddlDate.SelectedValue = "1";
            txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        DataTable dtLoaction = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dtLoaction;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlCompany.SelectedValue = "1";
        txtProjectNumber.Text = string.Empty;
        ddlProjectStatus.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlEmployee.SelectedValue = "";

        BindGrid(0);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

        try
        {
            string FileName = "";
            string[] arrHeader = null;
            if (ddlCompany.SelectedValue == "1")
            {
                FileName = "Stock Revert_Arise_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "Project", "Project Status", "Install Booking", "Intaller", "Details", "Store Name", "Panels", "Inverters", "Revert On", "Revert By" };
            }
            else if (ddlCompany.SelectedValue == "2")
            {
                FileName = "Stock Revert_SolarMiner_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "Project", "Project Status", "Install Booking", "Intaller", "Details", "Store Name", "Panels", "Inverters", "Revert On", "Revert By" };
            }
            else if (ddlCompany.SelectedValue == "4")
            {
                FileName = "Stock Revert_SolarBridge_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "Project", "Project Status", "Install Booking", "Intaller", "Details", "Store Name", "Panels", "Inverters", "Revert On", "Revert By" };
            }
            else if (ddlCompany.SelectedValue == "3")
            {
                FileName = "Stock Revert_Wholesale_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Invoice No", "Delivery Option", "Job Status", "Expected Delivery", "Customer", "Details", "Store Name", "Panels", "Inverters", "Revert On", "Revert By" };
            }

            Export oExport = new Export();

            int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    #region Msg/Script Function
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MyfunManulMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void Notification(string msg)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
        }
        catch (Exception e)
        {

        }
    }
    #endregion

    #region GridView Comman Method
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            #region Bind Header Name
            Label lbHeaderNo = (Label)e.Row.FindControl("lbHeaderNo");
            Label lbHeaderProject = (Label)e.Row.FindControl("lbHeaderProject");
            Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
            Label lblHeaderInstaller = (Label)e.Row.FindControl("lblHeaderInstaller");
            Label lblHeaderBooking = (Label)e.Row.FindControl("lblHeaderBooking");

            if (ddlCompany.SelectedValue == "3")
            {
                lbHeaderNo.Text = "Invoice No";
                lbHeaderProject.Text = "Delivery Option";
                lblHeaderStatus.Text = "Job Status";
                lblHeaderInstaller.Text = "Customer";
                lblHeaderBooking.Text = "Expected Delivery";
            }
            else
            {
                lbHeaderNo.Text = "Project No";
                lbHeaderProject.Text = "Project";
                lblHeaderStatus.Text = "Project Status";
                lblHeaderInstaller.Text = "Intaller";
                lblHeaderBooking.Text = "Install Booking";
            }
            #endregion
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            //String SMStockItemID = rowView["SMStockItemID"].ToString();


        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    #endregion

    #region Bind Grid Method
    protected DataTable GetGridData1()
    {
        DataTable dt = new DataTable();

        dt = ClsReportsV2.SP_DailyRevertReport_GetDataV2(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlProjectStatus.SelectedValue, ddlLocation.SelectedValue, ddlInstaller.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployee.SelectedValue);

        return dt;
    }

    public void BindTotal(DataTable dt)
    {
        string Panels = dt.Compute("SUM(Panels)", string.Empty).ToString();
        lblTotalPanels.Text = Panels;

        string Inverters = dt.Compute("SUM(Inverters)", string.Empty).ToString();
        lblTotalInverters.Text = Inverters;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
        }
    }
    #endregion

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindInstallerCustomer();
        BindStatus();
        BindDateType();
    }

    protected void BindInstallerCustomer()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Customer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Customer";
            ddlInstaller.DataValueField = "CustomerID";
            ddlInstaller.DataBind();

            divEmployee.Visible = true;
        }
        else
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

            divEmployee.Visible = false;
        }
    }

    protected void BindStatus()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Job Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "JobStatusType";
            ddlProjectStatus.DataValueField = "Id";
            ddlProjectStatus.DataBind();
        }
        else
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Project Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = ClstblProjectStatus.tblProjectStatus_SelectActive();
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "ProjectStatus";
            ddlProjectStatus.DataValueField = "ProjectStatusID";
            ddlProjectStatus.DataBind();
        }
    }

    protected void BindDateType()
    {
        //string SelectedDate = ddlDate.SelectedValue;
        //if (ddlCompany.SelectedValue == "3")
        //{
        //    ddlDate.Items.Clear();
        //    ListItem listItem = new ListItem() { Text = "Select", Value = "" };
        //    ddlDate.Items.Add(listItem);

        //    ddlDate.Items.Add(new ListItem("Deduct Date", "1"));
        //    ddlDate.Items.Add(new ListItem("Expected Delivery", "2"));
        //}
        //else
        //{
        //    ddlDate.Items.Clear();
        //    ListItem listItem = new ListItem() { Text = "Select", Value = "" };
        //    ddlDate.Items.Add(listItem);

        //    ddlDate.Items.Add(new ListItem("Deduct Date", "1"));
        //    ddlDate.Items.Add(new ListItem("Install Booking", "2"));
        //}

        //ddlDate.SelectedValue = SelectedDate;
    }
}