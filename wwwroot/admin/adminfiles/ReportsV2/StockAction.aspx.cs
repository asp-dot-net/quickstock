﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockAction : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static int STTotal = 0;

    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            //if (Roles.IsUserInRole("Warehouse"))
            //{
            //    BindDropDown();
            //    DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            //    string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //    ddllocationsearch.SelectedValue = CompanyLocationID;
            //    ddllocationsearch.Enabled = false;
            //    BindGrid(0);
            //}

            BindDropDown();
            //ddlSelectRecords.SelectedValue = "All";
            //GridView1.AllowPaging = false;
            //BindGrid(0);
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;

            BindUpdatedLable();

            //txtstockitemfilter.Text = "Suntech 370";
            //BindGrid(0);
        }

    }

    public void BindUpdatedLable()
    {
        //string my_format = "dd MMM yyyy @ h:mm tt";
        //DataTable dt = ClsReportsV2.tbl_fetchDataLog_GetData("FetchSMData");
        //if (dt.Rows.Count > 0)
        //{
        //    string date = Convert.ToDateTime(dt.Rows[0]["UpdatedOn"]).ToString(my_format);
        //    lblSMData.Text = "Last Updated on " + date + " By " + dt.Rows[0]["UpdatedBy"];
        //}
        //DataTable dt1 = ClsReportsV2.tbl_fetchDataLog_GetData("FetchData");
        //if (dt1.Rows.Count > 0)
        //{
        //    string date = Convert.ToDateTime(dt1.Rows[0]["UpdatedOn"]).ToString(my_format);
        //    lblData.Text = "Last Updated on " + date + " By " + dt1.Rows[0]["UpdatedBy"];
        //}
    }

    public void BindDropDown()
    {
        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();

        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch.DataSource = dtcategory;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        string LocationID = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnCompanyLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                LocationID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (LocationID != "")
        {
            LocationID = LocationID.Substring(1);
        }

        //DataTable dt1 = Reports.StockwiseReport_QuickStock(txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, LocationID, ddlActive.SelectedValue, ddltype.SelectedValue, DateType, txtStartDate.Text, txtEndDate.Text, txtStockModel.Text, ddlYesNo.SelectedValue);

        DataTable dt1 = ClsReportsV2.StockActionReport_GetDataV2(txtstockitemfilter.Text, txtStockModel.Text, ddlcategorysearch.SelectedValue, LocationID, ddlActive.SelectedValue, ddlYesNo.SelectedValue);

        return dt1;
    }
    //--ddldifference.SelectedValue

    public void BindTotal(DataTable dt1)
    {
        int StockOrdered = 0;
        int TargetDate = 0;
        int ExpectedOrdered = 0;
        int AriseSold = 0;
        int SMSold = 0;
        int WholesaleDraft = 0;
        int WholesaleInvoice = 0;
        int ArisePickList = 0;
        int SMPickList = 0;
        int DraftPickList = 0;
        int WDeduct = 0;
        int ADeduct = 0;
        int thirdParty = 0;
        //nt Reserved = 0;
        int Action = 0;

        if (dt1.Rows.Count > 0)
        {
            int CurrentQty = Convert.ToInt32(dt1.Compute("SUM(StockQuantity)", string.Empty));
            StockOrdered = Convert.ToInt32(dt1.Compute("SUM(StockQtyOrdered)", string.Empty));
            TargetDate = Convert.ToInt32(dt1.Compute("SUM(TargetDate)", string.Empty));
            ExpectedOrdered = Convert.ToInt32(dt1.Compute("SUM(ETA)", string.Empty));
            AriseSold = Convert.ToInt32(dt1.Compute("SUM(AriseSold)", string.Empty));
            SMSold = Convert.ToInt32(dt1.Compute("SUM(SMSold)", string.Empty));
            //WholesaleDraft = Convert.ToInt32(dt1.Compute("SUM(WholesaleDraft)", string.Empty));
            //int StockTransferOut = Convert.ToInt32(dt1.Compute("SUM(StockTransferOut)", string.Empty));
            //int Total = Convert.ToInt32(dt1.Compute("SUM(Total)", string.Empty));
            //ArisePickList = Convert.ToInt32(dt1.Compute("SUM(ArisePicklist)", string.Empty));
            //SMPickList = Convert.ToInt32(dt1.Compute("SUM(SMPicklist)", string.Empty));
            WholesaleInvoice = Convert.ToInt32(dt1.Compute("SUM(WholesaleInvoice)", string.Empty));
            //int NetTotal = Convert.ToInt32(dt1.Compute("SUM(NetTotal)", string.Empty));
            //DraftPickList = Convert.ToInt32(dt1.Compute("SUM(DraftPickList)", string.Empty));
            WDeduct = Convert.ToInt32(dt1.Compute("SUM(WholesaleDeduct)", string.Empty));
            //ADeduct = Convert.ToInt32(dt1.Compute("SUM(AriseDeduct)", string.Empty));
            thirdParty = Convert.ToInt32(dt1.Compute("SUM(ThirdPartyStock)", string.Empty));
            //Reserved = Convert.ToInt32(dt1.Compute("SUM(Reserved)", string.Empty));
            Action = Convert.ToInt32(dt1.Compute("SUM(Action)", string.Empty));

            totlblCurrentqty.Text = Convert.ToString(CurrentQty);
            totlblStockOrdered.Text = Convert.ToString(StockOrdered);
            hypTD.Text = Convert.ToString(TargetDate);
            totlblAriseSold.Text = Convert.ToString(AriseSold);
            //totlblWholesaleDraft.Text = Convert.ToString(WholesaleDraft);
            //totlblStockTransfer.Text = Convert.ToString(StockTransferOut);
            //totlblWholesaleInvoice.Text = Convert.ToString(WholesaleInvoice);
            //totlblArisePicklist.Text = Convert.ToString(ArisePickList);
            //totlblSMPicklist.Text = Convert.ToString(SMPickList);
            //totlblNetTot.Text = Convert.ToString(NetTotal);
            totlblSMSold.Text = Convert.ToString(SMSold);
            //totlblTot.Text = Convert.ToString(Total);
            totlblExpectedOrdered.Text = Convert.ToString(ExpectedOrdered);
            totlblthirdParty.Text = Convert.ToString(thirdParty);
            //totlblDraftPickList.Text = Convert.ToString(DraftPickList);
            tothypWholesaleDeduct.Text = Convert.ToString(WDeduct);
            //tothypAriseDeduct.Text = Convert.ToString(ADeduct);

            //totlblReserved.Text = Convert.ToString(Reserved);
            HypAction.Text = Convert.ToString(Action);

        }
        else
        {
            totlblCurrentqty.Text = "0";
            totlblStockOrdered.Text = "0";
            totlblAriseSold.Text = "0";
            //totlblWholesaleDraft.Text = "0";
            //totlblWholesaleInvoice.Text = "0";
            //totlblArisePicklist.Text = "0";
            //totlblSMPicklist.Text = "0";
            //totlblNetTot.Text = "0";
            totlblSMSold.Text = "0";
            //totlblTot.Text = "0";
            totlblSMSold.Text = "0";
           // totlblTot.Text = "0";
            totlblExpectedOrdered.Text = "0";
            totlblthirdParty.Text = "0";
            hypTD.Text = "0";
            //totlblStockTransfer.Text = "0";
            //totlblDraftPickList.Text = "0";
            tothypWholesaleDeduct.Text = "0";
            //tothypAriseDeduct.Text = "0";
            //totlblReserved.Text = "0";
            HypAction.Text = "0";
        }

        //===================================Total Details Page Linked=======================================
        string Category = ddlcategorysearch.SelectedValue;
        string StockItem = txtstockitemfilter.Text;
        string StockModel = txtStockModel.Text;
        string LocationID = getLocation();
        string IsActive = ddlActive.SelectedValue;
        string YesNo = ddlYesNo.SelectedValue;
        if (StockOrdered != 0)
        {
            totlblStockOrdered.Enabled = true;
            totlblStockOrdered.NavigateUrl = "~/admin/adminfiles/reports/Details/DetailsOrderQuatntity.aspx?Page=StockOrdered&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            totlblStockOrdered.Target = "_blank";
        }
        if (TargetDate != 0)
        {
            hypTD.Enabled = true;
            hypTD.NavigateUrl = "~/admin/adminfiles/reports/Details/DetailsOrderQuatntity.aspx?Page=TD&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            hypTD.Target = "_blank";
        }

        if (ExpectedOrdered != 0)
        {
            totlblExpectedOrdered.Enabled = true;
            totlblExpectedOrdered.NavigateUrl = "~/admin/adminfiles/reports/Details/DetailsOrderQuatntity.aspx?Page=ETA&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            totlblExpectedOrdered.Target = "_blank";
        }

        if (thirdParty != 0)
        {
            totlblthirdParty.Enabled = true;
            totlblthirdParty.NavigateUrl = "~/admin/adminfiles/reports/Details/DetailsOrderQuatntity.aspx?Page=ThirdParty&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            totlblthirdParty.Target = "_blank";
        }

        if (AriseSold != 0)
        {
            totlblAriseSold.Enabled = true;
            totlblAriseSold.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockSoldDetails.aspx?Page=AriseSold&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            totlblAriseSold.Target = "_blank";
        }
        if (SMSold != 0)
        {
            totlblSMSold.Enabled = true;
            totlblSMSold.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockSoldDetails.aspx?Page=SMSold&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
            totlblSMSold.Target = "_blank";
        }

        //if (WholesaleDraft != 0)
        //{
        //    totlblWholesaleDraft.Enabled = true;
        //    totlblWholesaleDraft.NavigateUrl = "~/admin/adminfiles/reports/Details/StockWholesaleDetails.aspx?Page=WholesaleDraft&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblWholesaleDraft.Target = "_blank";
        //}
        //if (WholesaleInvoice != 0)
        //{
        //    totlblWholesaleInvoice.Enabled = true;
        //    totlblWholesaleInvoice.NavigateUrl = "~/admin/adminfiles/reports/Details/StockWholesaleDetails.aspx?Page=WholesaleInvoice&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblWholesaleInvoice.Target = "_blank";
        //}

        //if (ArisePickList != 0)
        //{
        //    totlblArisePicklist.Enabled = true;
        //    totlblArisePicklist.NavigateUrl = "~/admin/adminfiles/reports/Details/StockPickListDetails.aspx?Page=ArisePicklist&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblArisePicklist.Target = "_blank";
        //}
        //if (SMPickList != 0)
        //{
        //    totlblSMPicklist.Enabled = true;
        //    totlblSMPicklist.NavigateUrl = "~/admin/adminfiles/reports/Details/StockPickListDetails.aspx?Page=SMPicklist&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblSMPicklist.Target = "_blank";
        //}
        //if (DraftPickList != 0)
        //{
        //    totlblDraftPickList.Enabled = true;
        //    totlblDraftPickList.NavigateUrl = "~/admin/adminfiles/reports/Details/StockPickListDetails.aspx?Page=DPicklist&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblDraftPickList.Target = "_blank";
        //}
        //==============================================End====================================================
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
            //if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
            //{
            //    ltrPage.Text = "Showing " + ((GridView1.Rows.Count)) + " entries";
            //    //BindGrid(0);
            //}
        }

        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockItemID"].ToString()))
            {
                PanGrid.Visible = false;
                divnopage.Visible = false;
                SetNoRecords();
            }
        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string LocationID = getLocation();
        string StockItem = txtstockitemfilter.Text;
        string StockModel = txtStockModel.Text;

        if (LocationID == "" && StockItem == "" && StockModel == "")
        {
            //SetAdd1();
            //SetError1();
            MyfunManulMsg("Please Select Location or Enter Stock Items Name");
        }
        else if (LocationID.Length > 1 && StockItem == "")
        {
            MyfunManulMsg("Enter Stock Items Name");
        }
        else
        {
            BindGrid(0);
        }

        //BindScript();
        //BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = "StockActionReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 3, 4, 6, 16, 7, 8, 14, 9, 18, 10, 11, 15, 20, 17, 19, 21 };
            string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Location", "Retail Status", "Live Qty", "Stk Ordered", "TD", "ETA", "Third Party", "Arise Stock Sold", "SM Stock Sold", "Wholesale Deduct", "Action", "Expiry Date", "Stock Category"
              , "Notes" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
        //PrepareGridViewForExport(PanGrid);
        ////BindGrid(0);
        //string timestemp = System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss");
        //string attachment = "attachment; filename=StockWiseReport" + timestemp + ".xls";

        //Response.ClearContent();
        //Response.AddHeader("content-disposition", attachment);
        //Response.ContentType = "application/ms-excel";

        //StringWriter sw = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(sw);
        //PanGrid.RenderControl(htw);
        //////============
        //string style = @"<style> .classext { mso-number-format:\@; } </script> ";
        //Response.Write(style);
        //////============
        //Response.Write(sw.ToString());
        //Response.End();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MyfunManulMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtstockitemfilter.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlcategorysearch.SelectedValue = "";
        txtStockModel.Text = string.Empty;
        //ddltype.SelectedValue = "1";
        ddlActive.SelectedValue = "0";
        //ddlDate.SelectedValue = "1";
        //txtStartDate.Text = string.Empty;
        //txtEndDate.Text = string.Empty;

        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "stockordered")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];

            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_stockorderquantity.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }

        }
        else if (e.CommandName.ToLower() == "wholesaleorder")
        {

            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];
            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_WholesaleOrder.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }
        }
        else if (e.CommandName.ToLower() == "StockSold")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string StockItemID = arg[0];
                string StockLocation = arg[1];

                if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
                {
                    Response.Redirect("stockwisereport_Stocksold.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
                }
            }
            catch { }
        }

        if (e.CommandName == "UpdateWholeSaleTag")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];
            string StockLocation = arg[1];

            bool Update = ClsReportsV2.tblStockItemsLocation_UpdateWholeSaleTag(StockItemID, StockLocation);
        }

        if (e.CommandName == "Notes")
        {
            try
            {
                txtNotesNew.Text = "";
                hndStockItemID.Value = "";
                hndCompanyLocationID.Value = "";
                hndMode.Value = "";

                //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string [] arg = e.CommandArgument.ToString().Split(';');
                hndStockItemID.Value = arg[0];
                hndCompanyLocationID.Value = arg[1];

                BindNewAriseNotes(arg[0], arg[1]);

                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            DataRowView rowView = (DataRowView)e.Row.DataItem;
            String SMStockItemID = rowView["SMStockItemID"].ToString();

            HiddenField hdnCompanyLocationID = (HiddenField)e.Row.FindControl("hdnCompanyLocationID");
            //  LinkButton btnStockQtyOrdered = (LinkButton)e.Row.FindControl("btnStockQtyOrdered");
            HiddenField hndStockitemID = (HiddenField)e.Row.FindControl("hndStockitemID");

            HiddenField CompanyLocation = (HiddenField)e.Row.FindControl("hdnCompanyLocation");

            HiddenField hdnAriseSold = (HiddenField)e.Row.FindControl("hdnAriseSold");

            Label lblStockQuantity = (Label)e.Row.FindControl("Label94");

            HyperLink lblStockQtyOrdered = (HyperLink)e.Row.FindControl("btnStockQtyOrdered");
            HyperLink lblStockSold = (HyperLink)e.Row.FindControl("btnStockSold");
            LinkButton lblWholesaleStockQtyOrdered = (LinkButton)e.Row.FindControl("btnWholesaleStockQtyOrdered");
            //Label lblNetTotal = (Label)e.Row.FindControl("Label4522");

            //Label lblArisePicklst = (Label)e.Row.FindControl("lblArisePicklist");
            HyperLink lblArisePicklst = (HyperLink)e.Row.FindControl("lblArisePicklist");

            //Label lblSolarMinerPicklst = (Label)e.Row.FindControl("lblSolarMinerPicklist");
            HyperLink lblSolarMinerPicklst = (HyperLink)e.Row.FindControl("lblSolarMinerPicklist");
            //Label lblWDraft = (Label)e.Row.FindControl("lblWDraft");
            HyperLink lblWDraft = (HyperLink)e.Row.FindControl("lblWDraft");
            //Label lblWInvoice = (Label)e.Row.FindControl("lblWInvoice");
            HyperLink lblWInvoice = (HyperLink)e.Row.FindControl("lblWInvoice");
            Label lblNetTotal = (Label)e.Row.FindControl("lblNetTotal");
            //Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            //Label lblTotal1 = (Label)e.Row.FindControl("lblTotal1");
            Label lblAriseSold = (Label)e.Row.FindControl("Label52");
            //Label lblSMStockSold = (Label)e.Row.FindControl("lblSMStockSold");
            //LinkButton SMStockSold = (LinkButton)e.Row.FindControl("lblSMStockSold");
            HyperLink SMStockSold = (HyperLink)e.Row.FindControl("lblSMStockSold");
            HyperLink hypNetTotal = (HyperLink)e.Row.FindControl("hypNetTotal");

            HyperLink btnExpectedStockOrder = (HyperLink)e.Row.FindControl("btnExpectedStockOrder");
            HyperLink btnTargetDate = (HyperLink)e.Row.FindControl("btnTargetDate");
            //HyperLink lblStockTransferOut = (HyperLink)e.Row.FindControl("lblStockTransferOut");
            HyperLink btnThirdPartyStock = (HyperLink)e.Row.FindControl("btnThirdPartyStock");

            //string SmSold = "0";
            //string Id1 = "";
            //HiddenField CurrentQty = (HiddenField)e.Row.FindControl("hdnCurrentQty");

            if (hdnCompanyLocationID.Value != "" && hndStockitemID.Value != "")
            {
                //====================Changes by Suresh on 11-03-2020=============================
                if (!string.IsNullOrEmpty(hndStockitemID.Value))
                {
                    string StockItem = DataBinder.Eval(e.Row.DataItem, "StockItem").ToString();
                    //View Stock Ordered
                    lblStockQtyOrdered.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_stockorderquantity.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=StockOrder";
                    lblStockQtyOrdered.Target = "_blank";

                    btnExpectedStockOrder.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_stockorderquantity.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=ExpStockOrder";
                    btnExpectedStockOrder.Target = "_blank";

                    btnThirdPartyStock.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_stockorderquantity.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=ThirdPartyStock";
                    btnThirdPartyStock.Target = "_blank";

                    lblStockSold.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_stocksold.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=AriseSold";
                    lblStockSold.Target = "_blank";

                    SMStockSold.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_stocksold.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=SMSold";
                    SMStockSold.Target = "_blank";

                    //lblArisePicklst.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_picklistdetails.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                    //    "&startdate=&enddate=&Page=ArisePicklist";
                    //lblArisePicklst.Target = "_blank";

                    //lblSolarMinerPicklst.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_picklistdetails.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                    //    "&startdate=&enddate=&Page=SMPicklist";
                    //lblSolarMinerPicklst.Target = "_blank";

                    //lblWDraft.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_WholesaleOrder.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value + "&Page=WDraft";
                    //lblWDraft.Target = "_blank";

                    //lblWInvoice.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/stockwisereport_WholesaleOrder.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value + "&Page=WInvoice";
                    //lblWInvoice.Target = "_blank";

                    btnTargetDate.NavigateUrl = "~/admin/adminfiles/reports/stockwisereport_stockorderquantity.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                        "&startdate=&enddate=&Page=TD";
                    btnTargetDate.Target = "_blank";

                    //lblStockTransferOut.NavigateUrl = "~/admin/adminfiles/reports/Details/StockTransferItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + hndStockitemID.Value + "&CompanyLocationId=" + hdnCompanyLocationID.Value +
                    //    "&startdate=&enddate=&Page=TD";
                    //lblStockTransferOut.Target = "_blank";
                }
                //=============================End Changes========================================

            }

        }
    }

    public string getLocation()
    {
        string LocationID = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnCompanyLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                LocationID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (LocationID != "")
        {
            LocationID = LocationID.Substring(1);
        }

        return LocationID;
    }

    public void Notification(string msg)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
        }
        catch (Exception e)
        {

        }
    }

    //protected void btnUpdateSMData_Click(object sender, EventArgs e)
    //{
    //int UpdateStatus = ClsDbData.USP_InsertUpdate_SM_tblProjectStatus();
    //bool UpdateProNo = ClsDbData.USP_InsertUpdate_tblProjects_SM();

    //    DataTable SMDt1 = ClsDbData.QuickStock_GetSMtblStockItemsData();
    //    int SuccRow1 = ClsDbData.USP_InsertUpdate_tbl_SMStockItems(SMDt1);

    //    int UpdateProject = ClsDbData.USP_InsertUpdate_tblProjects_SM();

    //    //SetAdd();
    //    if (UpdateProject > 0)
    //    {
    //        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //        Notification(UpdateProject + " Record Updated..");
    //        int UpdateLog = ClsReportsV2.tbl_fetchDataLog_Insert("FetchSMData", DateTime.Now.AddHours(14).ToString(), userid);
    //        BindUpdatedLable();
    //    }
    //    else
    //    {
    //        Notification("Error..");
    //    }

    //}

    //protected void btnFatchData_Click(object sender, EventArgs e)
    //{
    //    //DataTable dtProjectNumber = ClsDbData.tbl_PickListLog_GetAllProjectNumber();

    //    DataTable dtProject = ClsDbData.tblProject_GetDataByProjectNumber();

    //    int c = dtProject.Rows.Count;

    //    int UpdateData = ClsDbData.USP_Bulk_InsertUpdate_tblProjectsArise(dtProject);

    //    if (UpdateData > 0)
    //    {
    //        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //        Notification(UpdateData + " Record Updated..");
    //        int UpdateLog = ClsReportsV2.tbl_fetchDataLog_Insert("FetchData", DateTime.Now.AddHours(14).ToString(), userid);
    //        BindUpdatedLable();
    //    }
    //    else
    //    {
    //        Notification("Error..");
    //    }
    //}

    public void BindNewAriseNotes(string StockItemID, string CompanyLocationID)
    {
        DataTable dt = ClsReportsV2.tblStockItemNotes_GetDataByID(StockItemID, CompanyLocationID);
        if (dt.Rows.Count > 0)
        {
            DivNewNotes.Visible = true;
            RptNewNotes.DataSource = dt;
            RptNewNotes.DataBind();
        }
        else
        {
            DivNewNotes.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNewNotes.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            DivNewNotes.Attributes.Add("style", "");
        }
    }

    protected void RptNewNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNewNote")
        {
            try
            {
                DataTable dt = ClsReportsV2.tblStockItemNotes_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    //ddlAssignToNew.SelectedValue = "";
                    //ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndID.Value = ID;
                    //ddlAssignToNew.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                    //ddlEmployees.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                    txtNotesNew.Text = dt.Rows[0]["Notes"].ToString();

                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNewNotes")
        {
            try
            {
                bool del = ClsReportsV2.tblStockItemNotes_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                //string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindNewAriseNotes(hndStockItemID.Value, hndCompanyLocationID.Value);
                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNote.Show();
    }

    protected void lbtnSaveNewNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string StockItemID = hndStockItemID.Value;
            string CompanyLocationID = hndCompanyLocationID.Value;
            string EnteredOn = DateTime.Now.AddHours(14).ToShortDateString();
            string EnteredBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            if (hndMode.Value != "Update")
            {
                if (StockItemID != "" && CompanyLocationID != "")
                {
                    int ID = ClsReportsV2.tblStockItemNotes_Insert(StockItemID, CompanyLocationID, EnteredOn, EnteredBy);
                    if (ID != 0)
                    {
                        bool suc = ClsReportsV2.tblStockItemNotes_Update(ID.ToString(), txtNotesNew.Text);
                        
                        SetAdd1();
                        txtNotesNew.Text = "";

                        BindNewAriseNotes(StockItemID, CompanyLocationID);
                        ModalPopupExtenderNewNote.Show();

                        BindGrid(0);
                    }
                }
            }
            else
            {
                string ID = hndID.Value;
                if (ID != "")
                {
                    bool suc = ClsReportsV2.tblStockItemNotes_Update(ID, txtNotesNew.Text);

                    SetAdd1();

                    //ddlAssignToNew.SelectedValue = "";
                    //ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndMode.Value = "";

                    BindNewAriseNotes(StockItemID, CompanyLocationID);
                    ModalPopupExtenderNewNote.Show();

                    BindGrid(0);
                }
                //hndMode.Value = "";
            }
        }
        catch (Exception ex)
        {
            Notification("Inner Exception");
        }
    }
}