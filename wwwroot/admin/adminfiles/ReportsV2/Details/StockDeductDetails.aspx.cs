﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_Details_StockDeductDetails : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            if(!string.IsNullOrEmpty(Request.QueryString["stockItemId"].ToString()))
            {
                SttblStockItems stStockItem = ClstblStockItems.tblStockItems_SelectByStockItemID(Request.QueryString["stockItemId"].ToString());
                lblStockItem.Text = stStockItem.StockItem;
            }
            else
            {
                lblStockItem.Text = "Total";
            }

            if(Request.QueryString["isDeduct"].ToString() == "1")
            {
                lblPageName.Text = "Deduct";
            }
            else if(Request.QueryString["isDeduct"].ToString() == "0")
            {
                lblPageName.Text = "Pending";
            }
            else if(Request.QueryString["isDeduct"].ToString() == "2")
            {
                lblPageName.Text = "Revert";
            }
            //BindVendor();
            BindGrid(0);
        }
    }

    public void BindVendor()
    {
        //ddlSearchInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlSearchInstaller.DataValueField = "ContactID";
        //ddlSearchInstaller.DataMember = "Contact";
        //ddlSearchInstaller.DataTextField = "Contact";
        //ddlSearchInstaller.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string stockItemId = Request.QueryString["stockItemId"].ToString();
        string companyId = Request.QueryString["companyId"].ToString();
        string Deduct = Request.QueryString["isDeduct"].ToString();
        string installerId = Request.QueryString["installerId"].ToString();
        string locId = Request.QueryString["locId"].ToString();
        string dateType = Request.QueryString["dateType"].ToString();
        string StartDate = Request.QueryString["startDate"].ToString();
        string EndDate = Request.QueryString["endDate"].ToString();
        string CategoryId = Request.QueryString["CategoryId"].ToString();

        string IsDeduct = "1";
        string modulename = "";
        if (companyId == "3")
        {
            modulename = "WholeSale Out";
            if (Deduct == "0")
            {
                IsDeduct = "0";
            }
            else if (Deduct == "2")
            {
                modulename = "Wholesale Revert,Broken Item,Defected Item";
            }
        }
        else
        {
            modulename = "PickList Out";
            if (Deduct == "0")
            {
                IsDeduct = "0";
            }
            else if (Deduct == "2")
            {
                modulename = "PickList Revert,Broken Item,Defected Item";
            }
        }

        DataTable dt = ClsReportsV2.StockDeductReportV2_Details(companyId, stockItemId, installerId, IsDeduct, locId, dateType, StartDate, EndDate, modulename, CategoryId);

        if (dt.Rows.Count > 0)
        {
            int sum = Convert.ToInt32(dt.Compute("SUM(Qty)", string.Empty));
            //int sum = Convert.ToInt32(dt1.Rows.Count);
            lblTotalPanels.Text = sum.ToString();
        }
        else
        {
            lblTotalPanels.Text = "0";
        }

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        //ddlSearchInstaller.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //BindGrid(0);
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            string FileName = "StockDeductDetails" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            
            int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9 };

            string[] arrHeader = { "Project Number", "Project Status", "Installer Name", "Stock Model", "Qty", "InstallBookingDate", "Deduct By", "Deduct On" };

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;
                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            try
            {
                string companyID = Request.QueryString["companyId"].ToString();
                Label lblHeaderNumber = (Label)e.Row.FindControl("lblHeaderNumber");
                Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
                Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
                Label lblHeaderDate = (Label)e.Row.FindControl("lblHeaderDate");

                if (companyID == "1" || companyID == "2" || companyID == "4")
                {
                    lblHeaderNumber.Text = "Project No";
                    lblHeaderStatus.Text = "Project Status";
                    lblHeaderName.Text = "Installer Name";
                    lblHeaderDate.Text = "Install Booking Date";
                }
                else
                {
                    lblHeaderNumber.Text = "Invoice No";
                    lblHeaderStatus.Text = "Job Status";
                    lblHeaderName.Text = "Customer Name";
                    lblHeaderDate.Text = "Order Date";
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}