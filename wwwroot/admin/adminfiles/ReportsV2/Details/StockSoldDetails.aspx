﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockSoldDetails.aspx.cs" Inherits="admin_adminfiles_ReportsV2_Details_StockSoldDetails" 
   MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport2.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();

        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <%--  <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>--%>

    <div class="page-header card">
        <div class="card-block">
            <h5>
                <asp:Label runat="server" ID="lblPageName" /> Details
                <%--<div id="hbreadcrumb" class="pull-right">
                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                </div>--%>
            </h5>

        </div>
    </div>

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="Panel4">
            <%-- <asp:UpdatePanel ID="updatepanel1" runat="server">
                <ContentTemplate>--%>
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                        Text="Transaction Failed."></asp:Label></strong>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
            <div class="searchfinal">
                <div class="card shadownone brdrgray pad10">
                    <div class="card-block">
                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                            <div class="inlineblock martop5">
                                <div class="row">
                                    <div class="input-group col-sm-2 martop5 max_width170 dnone" >
                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project Number."></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                            WatermarkText="Project Number." />
                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers" />
                                    </div>
                                    <%-- <div class="input-group col-sm-2 martop5 max_width170">
                                        <asp:DropDownList ID="ddlSearchInstaller" runat="server" Style="width: 170px!important;" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>--%>

                                    <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                    </div>
                                    <div class="input-group martop5 col-sm-1 max_width170 dnone">
                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                    </div>





                                </div>
                            </div>

                        </asp:Panel>


                        <div class="datashowbox inlineblock">
                            <div class="row">

                                <div class="input-group col-sm-2 martop5 max_width170">
                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                        aria-controls="DataTables_Table_0" class="myval">
                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                    <asp:LinkButton ID="lbtnExport2" runat="server" Visible="false" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                        CausesValidation="false" OnClick="lbtnExport2_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <%--   </ContentTemplate>
                <Triggers>
                    <%--    <asp:PostBackTrigger ControlID="lbtnExport" />
                </Triggers>
            </asp:UpdatePanel>--%>
        </asp:Panel>
    </div>
    <div class="finalgrid">
        <asp:Panel ID="panel" runat="server">
            <div class="page-header card" id="divtot" runat="server">
                <div class="card-block brd_ornge">
                    <div class="printorder" style="font-size: medium">
                        <b>Total Number of Qty:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
            <div>
                <div id="PanGrid" runat="server">
                    <div class="card shadownone brdrgray">
                        <div class="card-block">
                            <div class="table-responsive BlockStructure">
                                <asp:GridView ID="GridView1" DataKeyNames="Projectnumber" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                    OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stock Item" SortExpression="StockItem">
                                            <ItemTemplate>
                   
                                                <asp:Label ID="lblStockItem" runat="server" Width="280px">
                                                                                        <%#Eval("StockItem")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee" SortExpression="EmpName">
                                            <ItemTemplate>
                   
                                                <asp:Label ID="lblEmpName" runat="server">
                                                                                        <%#Eval("EmpName")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Project Number" SortExpression="Projectnumber">

                                            <ItemTemplate>
                                                <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                <asp:Label ID="lblProjectnumber" runat="server" >
                                                                                        <%#Eval("Projectnumber")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Project Status" SortExpression="ProjectStatus">
                                            <ItemTemplate>
                                                <asp:Label ID="LblProjectStatus" runat="server">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Deposit Received" SortExpression="DepositReceived">
                                            <ItemTemplate>
                                                <asp:Label ID="LblDepositReceived" runat="server">
                                                                                        <%#Eval("DepositReceived")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center"/>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Number of Panels" SortExpression="NumberPanels">
                                            <ItemTemplate>
                                                <asp:Label ID="Label47" runat="server" >
                                                                                        <%#Eval("Qty")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center"/>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sys kW" SortExpression="SystemCapKW">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSyskW" runat="server" >
                                                                                        <%#Eval("SystemCapKW")%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Total Price" SortExpression="TotalQuotePrice">
                                            <ItemTemplate>

                                                <asp:Label ID="lblTotalQuotePrice" runat="server" Text='<%# Eval("TotalQuotePrice","{0:N2}") %>'>
                                                                                       <%-- <%#Eval("TotalQuotePrice", "{ 0:n2 }")%>--%></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center"  />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </asp:TemplateField>



                                    </Columns>
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>

                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>

    <%--<cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="10%" align="center">Index No.</th>
                                                    <th width="25%" align="center">Serial No.</th>
                                                    <th width="15%" align="left">Category</th>
                                                    <th align="50%">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialNo") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />--%>
    <%--    </ContentTemplate>
        <Triggers>--%>
    <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
    <%--     </Triggers>
    </asp:UpdatePanel>--%>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>

