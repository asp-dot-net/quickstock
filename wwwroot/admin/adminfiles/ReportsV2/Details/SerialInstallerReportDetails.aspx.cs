﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_Details_SerialInstallerReportDetails : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            lblPageName.Text = "Details";
            lblStockItem.Text = Request.QueryString["Contact"].ToString();

            //BindVendor();
            BindGrid(0);
        }
    }

    public void BindVendor()
    {
        //ddlSearchInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlSearchInstaller.DataValueField = "ContactID";
        //ddlSearchInstaller.DataMember = "Contact";
        //ddlSearchInstaller.DataTextField = "Contact";
        //ddlSearchInstaller.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string ContactID = Request.QueryString["ContactID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        //string DateType = "";
        //if (!string.IsNullOrEmpty(startdate) || !string.IsNullOrEmpty(enddate))
        //{
        //    DateType = "1";
        //}

        DataTable dt = new DataTable();

        dt = ClsReportsV2.SerialInstallerReportDetailsV2(CompanyID, ContactID, ProjectNo, "", DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);

        BindTotal(dt);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int Out = Convert.ToInt32(dt.Compute("SUM(Out)", string.Empty));
            int BSGB = Convert.ToInt32(dt.Compute("SUM(BSGB)", string.Empty));
            int Diff = Convert.ToInt32(dt.Compute("SUM(Diff)", string.Empty));
            //int Revert = Convert.ToInt32(dt.Compute("SUM(Revert)", string.Empty));
            //int Audit = Convert.ToInt32(dt.Compute("SUM(Audit)", string.Empty));
            lblTotalOut.Text = Out.ToString();
            lblTotalBSGB.Text = BSGB.ToString();
            HypDifference.Text = Diff.ToString();

            string ContactID = Request.QueryString["ContactID"].ToString();
            string ProjectNo = Request.QueryString["ProjectNo"].ToString();
            string CompanyID = Request.QueryString["CompanyID"].ToString();
            string StartDate = Request.QueryString["startdate"].ToString();
            string EndDate = Request.QueryString["enddate"].ToString();
            string Page = Request.QueryString["Page"].ToString();
            string ProjectStatus = Request.QueryString["PS"].ToString();
            string Category = Request.QueryString["Category"].ToString();
            string DateType = Request.QueryString["DateType"].ToString();
            string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
            string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

            HypDifference.Enabled = HypDifference.Text == "0" ? false : true;
            HypDifference.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetailsProj.aspx?StockItem=" + "Total" + "&StockItemID=&DateType=" + DateType + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Audit" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&Category=" + Category + "&PicklistCreatedBy=" + PicklistCreatedBy;

        }
        else
        {
            lblTotalOut.Text = "0";
            lblTotalBSGB.Text = "0";
            HypDifference.Text = "0";
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        //ddlSearchInstaller.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ContactID = Request.QueryString["ContactID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string InstallationDate = Request.QueryString["InstallationDate"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        if (e.CommandName == "View SerialNo")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');
            hndStockItemID.Value = arg[0];
            string StockItemID = arg[0];
            string Diff = arg[1];

            DataTable dtMain = new DataTable();
            if (Diff != "")
            {
                if(Convert.ToInt32(Diff) > 0)
                {
                    dtMain = ClsReportsV2.SerialInstallerReportDetails_SerialNo(CompanyID, ContactID, ProjectNo, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, InstallationDate, "Out", CompanyLocation, PicklistCreatedBy);
                }
                else
                {
                    dtMain = ClsReportsV2.SerialInstallerReportDetails_SerialNo(CompanyID, ContactID, ProjectNo, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, InstallationDate, "BSGB", CompanyLocation, PicklistCreatedBy);
                }
            }

            ArrayList values = new ArrayList();
            values.Add(new PositionData("Out"));
            values.Add(new PositionData("BSGB"));
            values.Add(new PositionData("Diff"));

            RptMain.DataSource = values;
            RptMain.DataBind();

            //DataTable dtDiff = ClsReportsV2.SerialInstallerReportDetails_SerialNo(CompanyID, ContactID, ProjectNo, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, InstallationDate, "Diff");

            //DataTable dtMerge = MergeTables(dt1, { dt2, dt3});

            //int Count = dtOut.Rows.Count > dtBSGB.Rows.Count ? dtOut.Rows.Count : dtBSGB.Rows.Count;

            
            //RptOut.DataSource = dtOut.Rows.Count > dtBSGB.Rows.Count ? dtOut : dtBSGB;
            //RptOut.DataBind();
            ModalPopupExtenderSerialNo.Show();
        }
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        string Page = Request.QueryString["Page"].ToString();

        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            string FileName = "SerialInstallerReportsDetails - " + lblStockItem.Text + "_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6 };

            string[] arrHeader = { "Stock Item", "Stock Model", "Category", "Net Out", "BSGB", "Diff" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;
                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            try
            {
                string CompanyID = Request.QueryString["CompanyID"].ToString();
                Label lblHeaderNumber = (Label)e.Row.FindControl("lblHeaderNumber");
                Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
                Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
                Label lblHeaderDate = (Label)e.Row.FindControl("lblHeaderDate");

                if (CompanyID == "1" || CompanyID == "2")
                {
                    lblHeaderNumber.Text = "Project No";
                    lblHeaderStatus.Text = "Project Status";
                    lblHeaderName.Text = "Installer Name";
                    lblHeaderDate.Text = "Install Booking Date";
                }
                else
                {
                    lblHeaderNumber.Text = "Invoice No";
                    lblHeaderStatus.Text = "Job Status";
                    lblHeaderName.Text = "Customer Name";
                    lblHeaderDate.Text = "Order Date";
                }
            }
            catch (Exception ex)
            {

            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            string StockItem = rowView["StockItem"].ToString();
            string StockItemID = rowView["StockItemID"].ToString();

            string ContactID = Request.QueryString["ContactID"].ToString();
            string ProjectNo = Request.QueryString["ProjectNo"].ToString();
            string CompanyID = Request.QueryString["CompanyID"].ToString();
            string StartDate = Request.QueryString["startdate"].ToString();
            string EndDate = Request.QueryString["enddate"].ToString();
            string Page = Request.QueryString["Page"].ToString();
            string ProjectStatus = Request.QueryString["PS"].ToString();
            string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
            string Category = Request.QueryString["Category"].ToString();
            string DateType = Request.QueryString["DateType"].ToString();
            string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

            HyperLink lblOut = (HyperLink)e.Row.FindControl("lblOut");
            HyperLink lblInstalled = (HyperLink)e.Row.FindControl("lblInstalled");
            HyperLink lblRevert = (HyperLink)e.Row.FindControl("lblRevert");

            HyperLink hypDetail = (HyperLink)e.Row.FindControl("hypDetail");

            //lblOut.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetailsProj.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Out" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&InstallationDate=" + InstallationDate;

            //lblInstalled.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetailsProj.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Installed" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&InstallationDate=" + InstallationDate;

            //lblRevert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetailsProj.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Revert" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&InstallationDate=" + InstallationDate;

            hypDetail.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetailsProj.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&DateType=" + DateType + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Audit" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&Category=" + Category + "&PicklistCreatedBy=" + PicklistCreatedBy;
        }
    }

    protected void RptMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string ContactID = Request.QueryString["ContactID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string InstallationDate = Request.QueryString["InstallationDate"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();
        string StockItemID = hndStockItemID.Value;

        Repeater rptOut = (Repeater)e.Item.FindControl("RptOut");
        HiddenField hndCol = (HiddenField)e.Item.FindControl("hndCol");

        DataTable dtInner = ClsReportsV2.SerialInstallerReportDetails_SerialNo(CompanyID, ContactID, ProjectNo, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, InstallationDate, hndCol.Value, CompanyLocation, PicklistCreatedBy);

        rptOut.DataSource = dtInner;
        rptOut.DataBind();
    }

    public class PositionData
    {
        private string Col;
        public PositionData(string Col)
        {
            this.Col = Col;
        }
        public string COL
        {
            get
            {
                return Col;
            }
        }
    }

    protected DataTable GetExcelData()
    {
        string ContactID = Request.QueryString["ContactID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        //string InstallationDate = Request.QueryString["InstallationDate"].ToString();
        string InstallationDate = "";
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        DataTable dt = new DataTable();

        dt = ClsReportsV2.SerialInstallerReportDetailsV2_Excel(CompanyID, ContactID, ProjectNo, "", DateType, StartDate, EndDate, ProjectStatus, Category, InstallationDate, CompanyLocation, PicklistCreatedBy);

        return dt;
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExcelData();

        string Page = Request.QueryString["Page"].ToString();

        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            string FileName = "SerialInstallerReportsDetails_DiffSerial_ - " + lblStockItem.Text + "_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3 };

            string[] arrHeader = { "Stock Item", "Stock Model", "Serial No" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }
}