﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_Details_SerialInstallerReportDetailsProj : System.Web.UI.Page
{
    #region GreenBot
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    #endregion
    public partial class RootObjectBS
    {
        [JsonProperty("Success")]
        public Success Success { get; set; }
    }

    public partial class Success
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Details")]
        public Details Details { get; set; }
    }

    public class Details
    {
        [JsonProperty("panels")]
        public Dictionary<string, Status> Panels { get; set; }

        [JsonProperty("inverters")]
        public Dictionary<string, Status> Inverters { get; set; }
    }

    public partial class Status
    {
        [JsonProperty("s")]
        public S S { get; set; }
    }

    public enum S { N, V, U };

    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            string PageName = Request.QueryString["Page"].ToString();
            //if (PageName == "TotalOut" || PageName == "TotalStockItemOut")
            //{
            //    lblPageName.Text = "Out";
            //}
            //else if (PageName == "TotalInstalled" || PageName == "TotalStockItemInstall")
            //{
            //    lblPageName.Text = "Installed";
            //}
            //else if (PageName == "TotalRevert" || PageName == "TotalStockItemRevert")
            //{
            //    lblPageName.Text = "Revert";
            //}
            //else
            //{
            //    lblStockItem.Text = Request.QueryString["StockItem"].ToString();
            //    lblPageName.Text = Request.QueryString["Page"].ToString();
            //}

            lblStockItem.Text = Request.QueryString["StockItem"].ToString();
            lblPageName.Text = Request.QueryString["Page"].ToString();
               
            BindVendor();
            BindGrid(0);
        }
    }

    public void BindVendor()
    {
        //ddlSearchInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlSearchInstaller.DataValueField = "ContactID";
        //ddlSearchInstaller.DataMember = "Contact";
        //ddlSearchInstaller.DataTextField = "Contact";
        //ddlSearchInstaller.DataBind();

        //DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
        //rptInstaller.DataSource = dtInstaller;
        //rptInstaller.DataBind();

        //if(Request.QueryString["CompanyID"].ToString() == "1")
        //{

        //}
        //ddlPicklistCreatedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        //ddlPicklistCreatedBy.DataValueField = "EmployeeID";
        //ddlPicklistCreatedBy.DataMember = "fullname";
        //ddlPicklistCreatedBy.DataTextField = "fullname";
        //ddlPicklistCreatedBy.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string InstallerID = Request.QueryString["InstallerID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        DataTable dt1 = new DataTable();

        if (Page == "Audit")
        {
            if(CompanyID == "1" || CompanyID == "4")
            {
                dt1 = ClsReportsV2.SerialInstallerDetailsProjV2(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
            }
            else if (CompanyID == "2")
            {
                dt1 = ClsReportsV2.SerialInstallerDetailsProjV2SM(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
            }

        }

        if (dt1.Rows.Count > 0)
        {
            int sum = Convert.ToInt32(dt1.Compute("SUM(MissingQty)", string.Empty));
            //int sum = Convert.ToInt32(dt1.Rows.Count);
            lblTotalPanels.Text = sum.ToString();
        }
        else
        {
            lblTotalPanels.Text = "0";
        }

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            lbtnExportV2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        //txtProjectNumber.Text = string.Empty;
        //ddlSearchInstaller.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewSerialNo")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            string PickListID = arg[0];
            string ProjectNo = arg[1];

            string StockItemID = Request.QueryString["StockItemID"].ToString();
            string InstallerID = Request.QueryString["InstallerID"].ToString();
            string CompanyID = Request.QueryString["CompanyID"].ToString();
            string StartDate = Request.QueryString["startdate"].ToString();
            string EndDate = Request.QueryString["enddate"].ToString();
            string Page = Request.QueryString["Page"].ToString();
            string ProjectStatus = Request.QueryString["PS"].ToString();
            string InstallationDate = Request.QueryString["InstallationDate"].ToString();
            string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
            string DateType = Request.QueryString["DateType"].ToString();

            string ModuleName = "";
            if (CompanyID == "1" || CompanyID == "2")
            {
                ModuleName = "PickList Out";
            }
            else if (CompanyID == "3")
            {
                ModuleName = "WholeSale Out";
            }

            DataTable dtSerialNo = ClsReportsV2.SerialInstallerDetailsSerialNoV2(CompanyID, ProjectNo, InstallerID, StockItemID, ModuleName, DateType, StartDate, EndDate, ProjectStatus, InstallationDate, PickListID);

            if (dtSerialNo.Rows.Count > 0)
            {
                Repeater3.DataSource = dtSerialNo;
                Repeater3.DataBind();
                ModalPopupExtenderPBSGB.Show();
            }
            else
            {
                Notification("Serial Number Not Found..");
            }
        }

        if (e.CommandName == "ViewBSGB")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');
            string Category = Request.QueryString["Category"].ToString();

            string PickListID = arg[0];
            string ProjectNo = arg[1];

            string BSGBFlag = "1";
            DataTable dtSerialNo = GetSerialNoFromGreenBot(ProjectNo, "arisesolar", "arisesolar1");
            if (dtSerialNo.Rows.Count == 0)
            {
                dtSerialNo = GetSerialNoFromBS(ProjectNo);
                BSGBFlag = "2";
            }

            hndProjectNo.Value = ProjectNo;
            hndBSGBFlag.Value = BSGBFlag;

            Repeater1.DataSource = dtSerialNo;
            Repeater1.DataBind();
            ModalPopupExtenderSerialNoBSGB.Show();
        }

        if (e.CommandName == "View Serial No")
        {
            string[] arg = e.CommandArgument.ToString().ToString().Split(';');
            hndID.Value = arg[0];
            hndStockItemID.Value = arg[1];
            hndStockCategoryID.Value = arg[2];
            hndPNo.Value = arg[3];

            ArrayList values = new ArrayList();
            values.Add(new PositionData("Out"));
            values.Add(new PositionData("BSGB"));
            values.Add(new PositionData("Diff"));

            RptMain.DataSource = values;
            RptMain.DataBind();

            ModalPopupExtenderSerialNo.Show();
        }
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        string Page = Request.QueryString["Page"].ToString();

        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            string FileName = "SerialInstallerReportDetailProj" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 4, 8, 3, 5, 12, 9, 10, 6, 7 };

            string[] arrHeader = { "Project Number", "Project Status", "Company Location", "Installer Name", "InstallBookingDate", "Stock Item", "Qty", "O. Qty", "Deduct On", "Deduct By" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;
                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            try
            {
                string CompanyID = Request.QueryString["CompanyID"].ToString();
                Label lblHeaderNumber = (Label)e.Row.FindControl("lblHeaderNumber");
                Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
                Label lblHeaderName = (Label)e.Row.FindControl("lblHeaderName");
                Label lblHeaderDate = (Label)e.Row.FindControl("lblHeaderDate");

                if (CompanyID == "1" || CompanyID == "2")
                {
                    lblHeaderNumber.Text = "Project No";
                    lblHeaderStatus.Text = "Project Status";
                    lblHeaderName.Text = "Installer Name";
                    lblHeaderDate.Text = "Install Booking Date";
                }
                else
                {
                    lblHeaderNumber.Text = "Invoice No";
                    lblHeaderStatus.Text = "Job Status";
                    lblHeaderName.Text = "Customer Name";
                    lblHeaderDate.Text = "Order Date";
                }
            }
            catch (Exception ex)
            {

            }
        }
    }

    #region APIDATA
    public DataTable GetSerialNoFromGreenBot(string ProjectNo, string Username, string Password)
    {
        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("Verification", typeof(string));
        dtSerialNo.Columns.Add("Category", typeof(string));

        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?RefNumber=" + ProjectNo);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                    Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails;

                    string SerialNo = JobSystemDetails.SerialNumbers;
                    string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

                    for (int i = 0; i < SerialNoList.Length; i++)
                    {
                        if (SerialNoList[i] != "")
                        {
                            //Status VerifiedStatus = null;

                            DataRow dr = dtSerialNo.NewRow();
                            dr["SerialNo"] = SerialNoList[i];
                            //dr["Verification"] = VerifiedStatus.S.ToString() == "V" ? "Verified" : VerifiedStatus.S.ToString() == "N" ? "Not Verified" : VerifiedStatus.S.ToString() == "U" ? "UnVerified" : "";
                            dr["Category"] = "Panel";

                            dtSerialNo.Rows.Add(dr);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Notification("Exception Error..");
        }

        return dtSerialNo;
    }

    public DataTable GetSerialNoFromBS(string ProjectNo)
    {
        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("Verification", typeof(string));
        dtSerialNo.Columns.Add("Category", typeof(string));

        try
        {
            Details GetJobDetailsFromBs = GetJobDetailsFromBS(ProjectNo);

            if (GetJobDetailsFromBs.Panels.Count > 0)
            {
                foreach (var item in GetJobDetailsFromBs.Panels)
                {
                    string SerialNo = item.Key;
                    Status VerifiedStatus = item.Value;

                    DataRow dr = dtSerialNo.NewRow();
                    dr["SerialNo"] = SerialNo;
                    dr["Verification"] = VerifiedStatus.S.ToString() == "V" ? "Verified" : VerifiedStatus.S.ToString() == "N" ? "Not Verified" : VerifiedStatus.S.ToString() == "U" ? "UnVerified" : "";
                    dr["Category"] = "Panel";

                    dtSerialNo.Rows.Add(dr);
                }

                if (GetJobDetailsFromBs.Inverters.Count > 0)
                {
                    foreach (var item in GetJobDetailsFromBs.Inverters)
                    {
                        string SerialNo = item.Key;
                        Status VerifiedStatus = item.Value;

                        DataRow dr = dtSerialNo.NewRow();
                        dr["SerialNo"] = SerialNo;
                        dr["Verification"] = VerifiedStatus.S.ToString() == "V" ? "Verified" : VerifiedStatus.S.ToString() == "N" ? "Not Verified" : VerifiedStatus.S.ToString() == "U" ? "UnVerified" : "";
                        dr["Category"] = "Inverter";

                        dtSerialNo.Rows.Add(dr);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Notification("Exception Error..");
        }

        return dtSerialNo;
    }

    public Details GetJobDetailsFromBS(string JobNumber)
    {
        Details ReturnDetails = new Details();
        try
        {
            var obj = new ClsBridgeSelect
            {
                //crmid = "872136"
                crmid = JobNumber
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/products";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<RootObjectBS> JsonData = client.Execute<RootObjectBS>(request);
            if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObjectBS jobData = JsonConvert.DeserializeObject<RootObjectBS>(JsonData.Content);

                ReturnDetails = jobData.Success.Details;
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return ReturnDetails;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    public class ClsBridgeSelect
    {
        public string crmid;
    }
    #endregion

    protected void lbtnSaveSerialNo_Click(object sender, EventArgs e)
    {
        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("ProjectNo", typeof(string));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("Varified", typeof(string));
        dtSerialNo.Columns.Add("StockCategoryID", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        string ProjectNo = hndProjectNo.Value;
        string BSGBFlag = hndBSGBFlag.Value;
        string CompanyID = Request.QueryString["CompanyID"].ToString();

        foreach (RepeaterItem item in Repeater1.Items)
        {
            Label lblCategory = (Label)item.FindControl("lblCategory");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label lblVerification = (Label)item.FindControl("lblVerification");

            DataRow dr = dtSerialNo.NewRow();
            dr["ProjectNo"] = ProjectNo;
            dr["SerialNo"] = lblSerialNo.Text;
            dr["BSGBFlag"] = BSGBFlag;
            dr["CompanyID"] = CompanyID;
            dr["Varified"] = lblVerification.Text == "Verified" ? "V" : lblVerification.Text == "Not Verified" ? "N" : lblVerification.Text == "UnVerified" ? "U" : "";
            dr["StockCategoryID"] = lblCategory.Text == "Panel" ? "1" : lblCategory.Text == "Inverter" ? "2" : "";
            dr["InstallerName"] = "";

            dtSerialNo.Rows.Add(dr);
        }

        int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSerialNoFromBSGB(dtSerialNo);
        string msg = "Total Record is " + dtSerialNo.Rows.Count + " and Updated Record is " + UpdateData;

        Notification(msg);
    }

    public class PositionData
    {
        private string Col;
        public PositionData(string Col)
        {
            this.Col = Col;
        }
        public string COL
        {
            get
            {
                return Col;
            }
        }
    }

    static string[] data;
    static string[] data1;
    protected void RptMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptOut = (Repeater)e.Item.FindControl("RptOut");
        HiddenField hndCol = (HiddenField)e.Item.FindControl("hndCol");

        string ID = hndID.Value;
        string StockItemID = hndStockItemID.Value;
        string Category = hndStockCategoryID.Value;
        string ProjectNo = hndPNo.Value;

        if(hndCol.Value == "Out")
        {
            DataTable dtInner = ClsReportsV2.tblMaintainHistory_GetOutSerialNo(ID, StockItemID, Category);

            data = null;
            data = dtInner.AsEnumerable().Select(s => s.Field<string>("SerialNo")).ToArray();
            rptOut.DataSource = data;
            rptOut.DataBind();
        }
        else if(hndCol.Value == "BSGB")
        {
            string BSGBFlag = "1";
            DataTable dtSerialNo = GetSerialNoFromGreenBot(ProjectNo, "arisesolar", "arisesolar1");
            if (dtSerialNo.Rows.Count == 0)
            {
                dtSerialNo = GetSerialNoFromBS(ProjectNo);
            }
            
            if (Request.QueryString["Category"] == "1")
            {
                data1 = null;
                data1 = dtSerialNo.AsEnumerable().Where(s => s.Field<string>("Category") == "Panel").Select(s => s.Field<string>("SerialNo")).ToArray();
            }
            else if(Request.QueryString["Category"] == "2")
            {
                data1 = null;
                data1 = dtSerialNo.AsEnumerable().Where(s => s.Field<string>("Category") == "Inverter").Select(s => s.Field<string>("SerialNo")).ToArray();
            }
            else
            {
                data1 = null;
                data1 = dtSerialNo.AsEnumerable().Select(s => s.Field<string>("SerialNo")).ToArray();
            }

            rptOut.DataSource = data1;
            rptOut.DataBind();
        }
        else
        {
            //string[] a = { "H01210100803553" };
            //string[] b = { "H01210100803630", "H01210100803654", "H01210100803655" };

            string[] DifferArray = data.Except(data1).ToArray();

            rptOut.DataSource = DifferArray;
            rptOut.DataBind();
        }
        
    }

    public string GetInstaller()
    {
        string Installer = "";
        //foreach (RepeaterItem item in rptInstaller.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnModule = (HiddenField)item.FindControl("hdnInstallerID");
        //    //Literal modulename = (Literal)item.FindControl("ltprojstatus");

        //    if (chkselect.Checked == true)
        //    {
        //        Installer += "," + hdnModule.Value.ToString();
        //    }
        //}
        //if (Installer != "")
        //{
        //    Installer = Installer.Substring(1);
        //}

        return Installer;
    }

    protected DataTable GetGridDataSerialNo()
    {
        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string InstallerID = Request.QueryString["InstallerID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        DataTable dt1 = new DataTable();

        if (Page == "Audit")
        {
            string Installer = GetInstaller();

            dt1 = ClsReportsV2.SerialInstallerDetailsProjV2_SerialNo(CompanyID, ProjectNo, InstallerID, StockItemID, DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);
        }

        return dt1;
    }

    protected DataTable GetExcelData()
    {
        string ContactID = Request.QueryString["InstallerID"].ToString();
        string ProjectNo = Request.QueryString["ProjectNo"].ToString();
        string CompanyID = Request.QueryString["CompanyID"].ToString();
        string StartDate = Request.QueryString["startdate"].ToString();
        string EndDate = Request.QueryString["enddate"].ToString();
        string Page = Request.QueryString["Page"].ToString();
        string ProjectStatus = Request.QueryString["PS"].ToString();
        string Category = Request.QueryString["Category"].ToString();
        string DateType = Request.QueryString["DateType"].ToString();
        string CompanyLocation = Request.QueryString["CompanyLocation"].ToString();
        string PicklistCreatedBy = Request.QueryString["PicklistCreatedBy"].ToString();

        DataTable dt = new DataTable();

        dt = ClsReportsV2.SerialInstallerReportDetailsV2(CompanyID, ContactID, ProjectNo, "", DateType, StartDate, EndDate, ProjectStatus, Category, CompanyLocation, PicklistCreatedBy);

        return dt;
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        DataTable dtSerialNo = GetGridDataSerialNo();
        DataTable dtStockItemWise = GetExcelData();
        //DataSet ds = new DataSet();

        dt.Columns.Remove("Projectnumber");
        dt.Columns.Remove("ID");
        dt.Columns.Remove("StockItemID");
        dt.Columns.Remove("StockCategoryID");
        dt.Columns.Remove("BSGBFlag");

        dt.Columns["ProjectStatus"].SetOrdinal(1);
        dt.Columns["CompanyLocation"].SetOrdinal(2);
        dt.Columns["StockItem"].SetOrdinal(5);
        dt.Columns["MissingQty"].SetOrdinal(6);
        dt.Columns["GivenQty"].SetOrdinal(7);

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtStockItemWise, "Stock Item Detail");
                wb.Worksheets.Add(dtSerialNo, "SerialNo Detail");
                wb.Worksheets.Add(dt, "SerialInstallerReportDetailProj");
                //wb.Worksheets.Add(dt);

                string FileName = "SerialInstallerReportDetailProj" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }
}