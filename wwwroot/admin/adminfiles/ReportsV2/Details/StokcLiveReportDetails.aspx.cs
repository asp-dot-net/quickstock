﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_Details_StokcLiveReportDetails : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindMultiselect();

            string Items = Request.QueryString["items"].ToString();
            lblPageName.Text = "Stock Live Report Details - " + Items;

            BindGrid(0);
        }
    }

    public void BindMultiselect()
    {
        // For Binding //
        string qr = "select DISTINCT modulename from tblMaintainHistory where modulename is not null";
        DataTable modulename = ClstblCustomers.query_execute(qr);
        lstSearchStatus.DataSource = modulename;
        lstSearchStatus.DataBind();
    }

    public DataTable MergeDt(DataTable dt)
    {
        string StartDate = Request.QueryString["SDate"].ToString();
        string EndDate = Request.QueryString["EDate"].ToString();
        string OpQty = Request.QueryString["OpQty"].ToString();

        //Set Property Read Only false in datatable Column
        foreach (DataColumn col in dt.Columns)
        {
            col.ReadOnly = false;
        }

        int Qty = 0;
        int CloseQty = 0;
        string ModuleName = "";
        string Section = "";

        for (int i = 1; i < dt.Rows.Count; i++)
        {
            Qty = Convert.ToInt32(dt.Rows[i]["Qty"].ToString());
            CloseQty = Convert.ToInt32(dt.Rows[i - 1]["ClosingQty"].ToString());
            ModuleName = dt.Rows[i]["modulename"].ToString();

            if (ModuleName == "Stock In" || ModuleName == "PickList Revert" || ModuleName == "WholeSale Revert" || ModuleName == "Transfer In" || ModuleName == "TransferOut Revert" || ModuleName == "Opening Stock" || ModuleName == "Stock Revert In")
            {
                CloseQty = CloseQty + Qty;
            }
            else if (ModuleName == "PickList Out" || ModuleName == "WholeSale Out" || ModuleName == "Transfer Out" || ModuleName == "TransferIn Revert" || ModuleName == "Broken Item" || ModuleName == "Defected Item" || ModuleName == "Missing Item")
            {
                CloseQty = CloseQty - Qty;
            }

            dt.Rows[i]["ClosingQty"] = CloseQty;
        }

        
        DataRow dr1 = dt.NewRow();
        dr1["CreatedDate"] = Convert.ToDateTime(EndDate).ToString("dd MMM yyyy"); //string
        dr1["SectionId"] = 0;
        dr1["Number"] = 0;
        dr1["modulename"] = "Closing Stock";
        dr1["Message"] = "";
        dr1["Qty"] = 0;
        if (dt.Rows.Count == 1)
        {
            dr1["ClosingQty"] = Convert.ToInt32(dt.Rows[0]["ClosingQty"].ToString()); ;
        }
        else
        {
            dr1["ClosingQty"] = CloseQty;
        }
        dt.Rows.Add(dr1);

        return dt;
    }

    protected DataTable GetGridData1()
    {
        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string CompanyLocationID = Request.QueryString["locID"].ToString();
        string StartDate = Request.QueryString["SDate"].ToString();
        string EndDate = Request.QueryString["EDate"].ToString();
        string OpQty = Request.QueryString["OpQty"].ToString();

        string ModuleName = getModuleName();

        DataTable dt1 = ClsReportsV2.SP_GetData_StockItemAuditReportV3_Details(StockItemID, CompanyLocationID, StartDate, EndDate, ModuleName, OpQty);

        if (dt1.Rows.Count > 0)
        {
            dt1 = MergeDt(dt1);
        }

        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        //txtProjectNumber.Text = string.Empty;
        //ddlSearchInstaller.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string StockItemID = Request.QueryString["StockItemID"].ToString();
        string CompanyLocationID = Request.QueryString["locID"].ToString();

        if (e.CommandName == "ViewSerialNo")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ModuleName = arg[0];
            string CreatedDate = arg[1];
            string SectionId = arg[2];


            DataTable dt = ClsReportsV2.tblMaintainHistory_GetSerialNoForStockLiveReport(ModuleName, CreatedDate, CompanyLocationID, StockItemID, SectionId);
            rptItems.DataSource = dt;
            rptItems.DataBind();
            //if (ProjectNo != "0" && PicklistId != "0")
            //{
            //    rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
            //    rptItems.DataBind();
            //}
            //else
            //{
            //    rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
            //    rptItems.DataBind();
            //}
            ModalPopupExtenderDetail.Show();
        }
        //BindGrid(0);
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        //DataTable dt = GetGridData1();

        //string[] columnNames = dt.Columns.Cast<DataColumn>()
        //                             .Select(x => x.ColumnName)
        //                             .ToArray();

        //Response.Clear();
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "StockWiseReport_StockSold" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 6, 0, 5, 4, 1, 2, 3 };
        //    string[] arrHeader = { "Employee", "ProjectNumber", "ProjectStatus", "Deposit Received", "NumberOfPanels", "Sys kW", "Total Price" };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    public string getModuleName()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnModuleName");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        return selectedItem;
    }
}