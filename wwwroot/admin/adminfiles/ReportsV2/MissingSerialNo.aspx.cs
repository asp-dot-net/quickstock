﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_MissingSerialNo : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropDown();

            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();

        //BindInstaller();

        //DataTable dtEmployee = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        //rptEmp.DataSource = dtEmployee;
        ////ddlEmployee.DataTextField = "fullname";
        ////ddlEmployee.DataValueField = "EmployeeID";
        //rptEmp.DataBind();
    }

    //public void BindInstaller()
    //{
    //    if (ddlCompany.SelectedValue == "1")
    //    {
    //        ddlInstaller.Items.Clear();
    //        ListItem listItem = new ListItem { Text = "Installer", Value = "" };
    //        ddlInstaller.Items.Add(listItem);

    //        DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
    //        ddlInstaller.DataSource = dtInstaller;
    //        ddlInstaller.DataTextField = "Contact";
    //        ddlInstaller.DataValueField = "ContactID";
    //        ddlInstaller.DataBind();
    //    }
    //    else if (ddlCompany.SelectedValue == "2")
    //    {
    //        ddlInstaller.Items.Clear();
    //        ListItem listItem = new ListItem { Text = "Installer", Value = "" };
    //        ddlInstaller.Items.Add(listItem);

    //        DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterSolarMiner();
    //        ddlInstaller.DataSource = dtInstaller;
    //        ddlInstaller.DataTextField = "Contact";
    //        ddlInstaller.DataValueField = "ContactID";
    //        ddlInstaller.DataBind();
    //    }
    //    else if (ddlCompany.SelectedValue == "4")
    //    {
    //        ddlInstaller.Items.Clear();
    //        ListItem listItem = new ListItem { Text = "Installer", Value = "" };
    //        ddlInstaller.Items.Add(listItem);

    //        DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
    //        ddlInstaller.DataSource = dtInstaller;
    //        ddlInstaller.DataTextField = "CompanyName";
    //        ddlInstaller.DataValueField = "UserId";
    //        ddlInstaller.DataBind();
    //    }
    //    else
    //    {
    //        ddlInstaller.Items.Clear();
    //        ListItem listItem = new ListItem { Text = "Customer", Value = "" };
    //        ddlInstaller.Items.Add(listItem);

    //        DataTable dtInstaller = ClstblContacts.tblCustType_SelectWholesaleVendor();
    //        ddlInstaller.DataSource = dtInstaller;
    //        ddlInstaller.DataTextField = "Customer";
    //        ddlInstaller.DataValueField = "CustomerID";
    //        ddlInstaller.DataBind();
    //    }

    //}

    protected DataTable GetGridData1()
    {
        DataTable dt = ClsReportsV2.MissingSerialNo_GetData(ddlCompany.SelectedValue, txtStockItemModel.Text, ddlCategory.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindHeader(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    public void BindHeader(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int Brisbane = Convert.ToInt32(dt.Compute("SUM(QLD)", string.Empty));
            int Melbourne = Convert.ToInt32(dt.Compute("SUM(VIC)", string.Empty));
            int Sydney = Convert.ToInt32(dt.Compute("SUM(NSW)", string.Empty));
            int Adelaide = Convert.ToInt32(dt.Compute("SUM(SA)", string.Empty));
            int Perth = Convert.ToInt32(dt.Compute("SUM(WA)", string.Empty));
            int Total = Convert.ToInt32(dt.Compute("SUM(Total)", string.Empty));

            lblBrisbane.Text = Convert.ToString(Brisbane);
            lblMelbourne.Text = Convert.ToString(Melbourne);
            lblSydney.Text = Convert.ToString(Sydney);
            lblAdelaide.Text = Convert.ToString(Adelaide);
            lblPerth.Text = Convert.ToString(Perth);
            lblTotal.Text = Convert.ToString(Total);

            lblBrisbane.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=1&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblBrisbane.Enabled = Brisbane != 0 ? true : false;

            lblMelbourne.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=2&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblMelbourne.Enabled = Melbourne != 0 ? true : false;

            lblSydney.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=3&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblSydney.Enabled = Sydney != 0 ? true : false;

            lblAdelaide.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=7&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblAdelaide.Enabled = Adelaide != 0 ? true : false;

            lblPerth.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=4&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblPerth.Enabled = Perth != 0 ? true : false;

            lblTotal.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/MissingSerialNoDetails.aspx?stockItemId=&companyId=" + ddlCompany.SelectedValue
                + "&locId=1,2,3,4,7&dateType=" + ddlDate.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&CategoryId=" + ddlCategory.SelectedValue;
            lblTotal.Enabled = Total != 0 ? true : false;
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "MissingSerialNo" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";


        int[] ColList = { 3, 1, 2, 4, 5, 6, 7, 8, 9, 10 };
        string[] arrHeader = { "Category", "Stock Item", "Stock Model", "Size", "Brisbane", "Melbourne", "Sydney", "Adelaide", "Perth", "Total" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStockItemModel.Text = string.Empty;
        ddlCategory.SelectedValue = "";
        ddlDate.SelectedValue = "1";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        //ddlIsDeduct.SelectedValue = "1";

        //foreach (RepeaterItem item in rptEmp.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    chkselect.Checked = false;
        //}

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName.ToString() == "Detailbrisbane")
        //{
        //    Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=QLD&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        //}
        //if (e.CommandName.ToString() == "DetailMelbourne")
        //{
        //    Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=VIC&CompanyId=" + ddlCompany.SelectedValue +"&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        //}
        //if (e.CommandName.ToString() == "DetailSydney")
        //{
        //    Response.Redirect("~/admin/adminfiles/reports/stockreceivedreportDetailpage.aspx?StockItemID=" + StockItemID + "&State=NSW&CompanyId=" + ddlCompany.SelectedValue + "&IsDeduct=" + ddlIsDeduct.SelectedValue + "&DateType=" + datetype + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&StockItem=" + StockItem + "&InstallerName=" + InstallerName);
        //}
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    //protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindInstaller();
    //    if (ddlCompany.SelectedValue == "3")
    //    {
    //        DivEmployee.Visible = true;
    //    }
    //    else
    //    {
    //        DivEmployee.Visible = false;
    //    }
    //}

    //protected string getEmployee()
    //{
    //    string selectedItem = "";
    //    foreach (RepeaterItem item in rptEmp.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnModule = (HiddenField)item.FindControl("hdnEmployeeID");

    //        if (chkselect.Checked == true)
    //        {
    //            selectedItem += "," + hdnModule.Value.ToString();
    //        }
    //    }
    //    if (selectedItem != "")
    //    {
    //        selectedItem = selectedItem.Substring(1);
    //    }
    //    return selectedItem;
    //}
}