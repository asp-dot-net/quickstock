﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataHistory.aspx.cs" Inherits="admin_adminfiles_ReportsV2_DataHistory"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script src="<%=SiteURL%>admin/theme/assets/js/select2.js"></script>
            <%--    <script src="<%=SiteURL%>admin/vendor/jquery/dist/jquery.min.js"></script>--%>


            <script language="javascript" type="text/javascript">
                function WaterMark(txtName, event) {
                    var defaultText = "Enter Username Here";
                }
            </script>
            <script>      
                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });
                });



                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');

                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });
                }

                function pageLoadedpro() {
                    callMultiCheckbox();
                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    //$('.datetimepicker1').datetimepicker({
                    //    format: 'DD/MM/YYYY'
                    //});


                }
                function callMultiCheckbox() {
                    var title = "";
                    $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }


                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }

            </script>
            <style>
                .txt_height {
                    height: 100px;
                }

                .tooltip {
                    min-width: 200px;
                }

                .braktext {
                    white-space: normal !important;
                }
            </style>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();

                    //$(".SearcStatus .dropdown dt a").on('click', function () {
                    //        $(".Location .dropdown dd ul").slideToggle('fast');
                    //    });
                    //    $(".SearcStatus .dropdown dd ul li a").on('click', function () {
                    //        $(".Location .dropdown dd ul").hide();
                    //    });

                    //    $(document).bind('click', function (e) {
                    //        var $clicked = $(e.target);
                    //        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    //});

                    // $(".dropdown dt a").on('click', function () {
                    //    $(".dropdown dd ul").slideToggle('fast');
                    //});

                    //$(".dropdown dd ul li a").on('click', function () {
                    //    $(".dropdown dd ul").hide();
                    //});
                    //$(document).bind('click', function (e) {
                    //    var $clicked = $(e.target);
                    //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    //});
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(".myval").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $(".myvalemployee").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });

                    $(".myvalproject").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    callMultiCheckbox();
                }
            </script>



            <div class="page-body headertopbox">
                <div class="card">
                    <div class="card-block">
                        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Data History
                    <asp:Label runat="server" ID="lblStockItem" />
                            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <%--  <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>
                            </div>
                            <h5></h5>
                        </h5>

                    </div>
                </div>

            </div>

            <div class="page-body padtopzero minheight500">
                <asp:Panel runat="server" ID="PanGridSearch" class="">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">

                                                <div class="input-group col-sm-2 max_width170" runat="server">
                                                    <asp:DropDownList ID="ddlOption" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="1">Retail</asp:ListItem>
                                                        <asp:ListItem Value="2">Wholesale</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group spical multiselect martop5 col-sm-2 max_width200 specail1_select SearcStatus">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Select</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddproject" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnModuleName" runat="server" Value='<%# Eval("modulename") %>' />
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("modulename")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170" runat="server">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Category</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                
                                                <div class="input-group col-sm-2 max_width170" runat="server">
                                                    <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <%--<asp:ListItem Value="">Date</asp:ListItem>--%>
                                                    <asp:ListItem Value="1">Created Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="input-group col-sm-1 max_width170 dnone">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <div class="input-group col-sm-1 max_width170 dnone">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">

                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click1"
                                                    CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </asp:Panel>


                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="table-responsive  BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="id" runat="server" CssClass="tooltip-demo text-center table GridviewScrollItem table-bordered Gridview"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" OnRowUpdating="GridView1_RowUpdating"
                                                OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Project/Inv Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="Projectnumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label39" runat="server" Text='<%#Eval("Projectnumber") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="InstallerName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInstallerName" runat="server"><%#Eval("InstallerName")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompanyLocation" runat="server"><%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Module Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="modulename">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblmodulename" runat="server"><%#Eval("modulename")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Category Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="StockCategory">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server"><%#Eval("StockCategory")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Stock Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="StockItem">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStockItem" runat="server"><%#Eval("StockItem")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Serial No" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="SerialNo" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSerailNo" runat="server" data-placement="top" data-original-title='<%#Eval("SerailNo")%>' data-toggle="tooltip"><%#Eval("SerailNo")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Message" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="150px" SortExpression="Message">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMessage" runat="server" data-placement="top" data-toggle="tooltip"
                                                                Text='<%#Eval("Message")%>' data-original-title='<%#Eval("Message")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Deduct On" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" SortExpression="DeductOn">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDeductOn" runat="server" Text='<%#Eval("DeductOn")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Created Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" SortExpression="CreatedDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label62" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <%--<asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LnkUpdate1" runat="server" CausesValidation="true" CommandName="Update" CommandArgument='<%# Eval("SerailNo") +","+ Eval("itemId") %>'
                                                                data-placement="top" data-original-title="Update" data-toggle="tooltip" CssClass="btn btn-primary btn-mini">                                                                       
                                                                          <i class="fa fa-edit"></i>Update
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="gvbtnClear" runat="server" CssClass="btn btn-success btn-mini" CommandName="Clear" CommandArgument='<%#Eval("SerailNo")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Clear
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("SerailNo") %>'>
                                                    <i class="fa fa-trash"></i> Delete
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        $(document).ready(function () {
            //$('.js-example-basic-multiple').select2();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        // For Multi Select //
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>

</asp:Content>
