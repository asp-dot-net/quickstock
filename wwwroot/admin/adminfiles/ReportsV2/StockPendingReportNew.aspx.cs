﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockPendingReportNew : System.Web.UI.Page
{
    static DataView dv;
    static DataView dv3;
    protected static string Siteurl;

    //Arise greenbot Account
    protected static string AriseUsername = "arisesolar";
    protected static string ArisePassword = "arisesolar1";

    //SM greenbot Account
    protected static string SMUsername = "mac.solarminer@gmail.com";
    protected static string SMPassword = "sminer234";

    //Wholesale greenbot Account
    protected static string WOUsername = "achieversenergy";
    protected static string WOPassword = "achievers1";

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }

    public class ClsBridgeSelect
    {
        public string crmid;
    }

    public partial class RootObjectBS
    {
        [JsonProperty("Success")]
        public Success Success { get; set; }
    }

    public partial class Success
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Details")]
        public Details Details { get; set; }
    }

    public class Details
    {
        [JsonProperty("crmid", NullValueHandling = NullValueHandling.Ignore)]
        public string Crmid { get; set; }

        [JsonProperty("panels")]
        public Dictionary<string, The03091220_C100084> Panels { get; set; }

        [JsonProperty("inverters")]
        public Dictionary<string, The03091220_C100084> Inverters { get; set; }
    }

    public enum The03091220_C100084 { N, V, U };
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords3.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords3.DataBind();

            ddlSMSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSMSelectRecords.DataBind();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();

            //if (ddlIsverify.SelectedValue == "1")
            //{
            //    ddlYesNo.SelectedValue = "1";
            //    DivYesNo.Visible = true;
            //}
            //else
            //{
            //    DivYesNo.Visible = false;
            //}

            //if (ddlIsverify1.SelectedValue == "1")
            //{
            //    ddlWYesNo.SelectedValue = "1";
            //    DivWYesNo.Visible = true;
            //}
            //else
            //{
            //    DivWYesNo.Visible = false;
            //}
            //if (ddlSMIsverify.SelectedValue == "1")
            //{
            //    ddlSMYesNO.SelectedValue = "1";
            //    DivSMYesNO.Visible = true;
            //}
            //else if (ddlSMIsverify.SelectedValue == "0")
            //{
            //    DivSMYesNO.Visible = true;
            //}
            //else
            //{
            //    DivSMYesNO.Visible = false;
            //}

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                BindCheckboxProjectTab();
                //BindDropDown();
                //DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                //string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                //ddllocationsearch.SelectedValue = CompanyLocationID;
                //ddllocationsearch.Enabled = false;
                //BindGrid(0);
            }

            if (Roles.IsUserInRole("Wholesale"))
            {
                TabProjectNo.Visible = false;
                BindCheckboxWTab();
                //BindGrid3(0);
                //BindDropDown3();

            }
            else
            {
                BindCheckboxProjectTab();
                //BindGrid(0);
            }

            //BindCheckbox();

            //BindDropDown();
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;

            BindAssignTo();

            //txtProjectNumber.Text = "897732,896638";
            //BindGrid(0);

            //TabContainer1.ActiveTabIndex = 2;
            //BindGridSM(0);
        }

    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlVerify.SelectedValue))
        {
            Isverify = ddlVerify.SelectedValue;
        }


        string selectedProjectStatusItem = "";
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedProjectStatusItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedProjectStatusItem != "")
        {
            selectedProjectStatusItem = selectedProjectStatusItem.Substring(1);
        }

        //string selectedInstallerItem = "";
        //foreach (RepeaterItem item in rptAriseInstaller.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnModule = (HiddenField)item.FindControl("hdnAriseInstallerID");
        //    //Literal modulename = (Literal)item.FindControl("ltprojstatus");

        //    if (chkselect.Checked == true)
        //    {
        //        selectedInstallerItem += "," + hdnModule.Value.ToString();
        //    }
        //}
        //if (selectedInstallerItem != "")
        //{
        //    selectedInstallerItem = selectedInstallerItem.Substring(1);
        //}

        string selectedLocationItem = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }

        //DataTable dt = Reports.InstallerwiseReport_QuickStock("883001,884208,887144", txtserailno.Text, txtstockitemfilter.Text, selectedLocationItem, selectedInstallerItem, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Isverify, ddlprojectwise.SelectedValue, selectedProjectStatusItem, ddlYesNo.SelectedValue);

        //DataTable dt = Reports.QuickStock_StockPendingReport_Updated_New(txtProjectNumber.Text, txtserailno.Text, txtstockitemfilter.Text, selectedLocationItem, selectedInstallerItem, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, selectedProjectStatusItem, Isverify, ddlYesNo.SelectedValue, ddlVerify.SelectedValue);

        DataTable dt = ClsReportsV2.StockPendingReport_GetDateV2(txtProjectNumber.Text, selectedLocationItem, ddlInstaller.SelectedValue, selectedProjectStatusItem, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Isverify, ddlIsDifference.SelectedValue, ddlYesNo.SelectedValue, ddlSearchAssignTo.SelectedValue, ddlIsDeduct.SelectedValue, ddlPicklistCount.SelectedValue);

        try
        {
            if (dt.Rows.Count > 0)
            {
                #region 
                //
                //for (int i = 0; i < GridView1.Rows.Count; i++)
                //{

                //    HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
                //    Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
                //    Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
                //    Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevert");
                //    Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
                //    Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblInverterrevert");
                //    Label lblpaneninstall = (Label)GridView1.Rows[i].FindControl("Label52");
                //    Label lblinverterIntall = (Label)GridView1.Rows[i].FindControl("Label752");
                //    Label lblpanelOut = (Label)GridView1.Rows[i].FindControl("Label452");
                //    Label lblinverterOut = (Label)GridView1.Rows[i].FindControl("Label4522");
                //    //LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
                //    LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");
                //    LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");


                //    //if (i != dt.Rows.Count - 1)
                //    //{
                //    //    if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
                //    //    {
                //    //        // gvbtnVerify.Visible = true;
                //    //    }
                //    //    else
                //    //    {
                //    //        // gvbtnVerify.Visible = false;
                //    //    }
                //    //}

                //    DataTable dt1 = null;
                //    dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
                //    if (dt1.Rows.Count > 0)
                //    {
                //        try
                //        {
                //            if (!string.IsNullOrEmpty(dt1.Rows[0]["VerifynoteIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedateIN"].ToString()))
                //            {
                //                // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                //                gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                //            }
                //        }
                //        catch { }
                //    }
                //}
                #endregion
            }
        }
        catch { }


        return dt;
    }

    protected void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(PanelStockDeducted)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(PanelInstalled)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int PanelRevert = Convert.ToInt32(dt.Compute("SUM(PanelRevert)", string.Empty));
            int PAudit = (PanelDiff - PanelRevert);
            int InverterStockDeducted = Convert.ToInt32(dt.Compute("SUM(InverterStockDeducted)", string.Empty));
            int SaleQtyInverter = Convert.ToInt32(dt.Compute("SUM(InverterInstalled)", string.Empty));
            int InverterDiff = (InverterStockDeducted - SaleQtyInverter);
            int InverteRevert = Convert.ToInt32(dt.Compute("SUM(InverterRevert)", string.Empty));
            int IAudit = (InverterDiff - InverteRevert);

            lblpout.Text = PanelStockDeducted.ToString();
            lblPInstalled.Text = SaleQtyPanel.ToString();
            lblPDifference.Text = PanelDiff.ToString();
            lblPRevert.Text = PanelRevert.ToString();
            lblPAudit.Text = PAudit.ToString();
            lblIOut.Text = InverterStockDeducted.ToString();
            lblIInstalled.Text = SaleQtyInverter.ToString();
            lblIDifference.Text = InverterDiff.ToString();
            lblIRevert.Text = InverteRevert.ToString();
            lblIAudit.Text = IAudit.ToString();
        }
        else
        {
            lblpout.Text = "0";
            lblPInstalled.Text = "0";
            lblPDifference.Text = "0";
            lblPRevert.Text = "0";
            lblPAudit.Text = "0";
            lblIOut.Text = "0";
            lblIInstalled.Text = "0";
            lblIDifference.Text = "0";
            lblIRevert.Text = "0";
            lblIAudit.Text = "0";
        }
    }

    protected void BindTotalWo(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(PanelStockDeducted)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(SaleQtyPanel)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int PanelRevert = Convert.ToInt32(dt.Compute("SUM(PanelRevert)", string.Empty));
            int PAudit = (PanelDiff - PanelRevert);
            int InverterStockDeducted = Convert.ToInt32(dt.Compute("SUM(InverterStockDeducted)", string.Empty));
            int SaleQtyInverter = Convert.ToInt32(dt.Compute("SUM(SaleQtyInverter)", string.Empty));
            int InverterDiff = (InverterStockDeducted - SaleQtyInverter);
            int InverteRevert = Convert.ToInt32(dt.Compute("SUM(InvertRevert)", string.Empty));
            int IAudit = (InverterDiff - InverteRevert);

            lblwPOut.Text = PanelStockDeducted.ToString();
            lblwPDifference.Text = PanelDiff.ToString();
            lblwInvPanel.Text = SaleQtyPanel.ToString();
            lblwPRevert.Text = PanelRevert.ToString();
            lblwPAudit.Text = PAudit.ToString();

            lblwIOut.Text = InverterStockDeducted.ToString();
            lblwInvInverter.Text = SaleQtyInverter.ToString();
            lblwIDifference.Text = InverterDiff.ToString();
            lblwIRevert.Text = InverteRevert.ToString();
            lblwIAudit.Text = IAudit.ToString();
        }
        else
        {
            lblwPDifference.Text = "0";
            lblwPRevert.Text = "0";
            lblwIDifference.Text = "0";
            lblwIRevert.Text = "0";
            lblwPOut.Text = "0";
            lblwInvPanel.Text = "0";
            lblwIOut.Text = "0";
            lblwInvInverter.Text = "0";
            lblwPAudit.Text = "0";
            lblwIAudit.Text = "0";
        }
    }

    protected DataTable GetGridData3()//wholesaleorderid wise
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlIsverify1.SelectedValue))
        {
            Isverify = ddlIsverify1.SelectedValue;
        }

        string selectedLocationItem = "";
        foreach (RepeaterItem item in rptLocationW.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }

        string selectedCustomerItem = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedCustomerItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedCustomerItem != "")
        {
            selectedCustomerItem = selectedCustomerItem.Substring(1);
        }

        string selectedJobStatusItem = "";
        foreach (RepeaterItem item in rptWJobStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnWJobStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedJobStatusItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedJobStatusItem != "")
        {
            selectedJobStatusItem = selectedJobStatusItem.Substring(1);
        }

        DataTable dt = ClsReportsV2.InstallerwiseReport_QuickStockWholesaleV2(txtInvoiceNo.Text, txtserailno3.Text, txtstockitemfilter3.Text, selectedLocationItem, selectedCustomerItem, ddldate3.SelectedValue, txtstartdate3.Text, txtenddate3.Text, Isverify, selectedJobStatusItem, ddlWYesNo.SelectedValue, ddlSearchAssignToW.SelectedValue);

        try
        {
            #region
            //
            for (int i = 0; i < GridView3.Rows.Count; i++)

            {
                HiddenField hndWholesaleorderID = (HiddenField)GridView3.Rows[i].FindControl("hndWholesaleorderID");
                Label lblpaneldiff = (Label)GridView3.Rows[i].FindControl("lblpaneldiff");
                Label lblwholepanelrevert = (Label)GridView3.Rows[i].FindControl("lblwholepanelrevert");
                Label lblinverterdiff = (Label)GridView3.Rows[i].FindControl("lblinverterdiff");
                Label lblwholeInverterrevert = (Label)GridView3.Rows[i].FindControl("lblwholeInverterrevert");
                Label lblpaneninstall = (Label)GridView3.Rows[i].FindControl("Label522");
                Label lblinverterIntall = (Label)GridView3.Rows[i].FindControl("Label7525");
                Label lblpanelOut = (Label)GridView3.Rows[i].FindControl("Label4521");
                Label lblinverterOut = (Label)GridView3.Rows[i].FindControl("Label45224");
                //LinkButton dvbtnVeriify = (LinkButton)GridView3.Rows[i].FindControl("gvbtnVerify1");
                LinkButton gvbtnView3 = (LinkButton)GridView3.Rows[i].FindControl("gvbtnView3");
                LinkButton dvbtnnote1 = (LinkButton)GridView3.Rows[i].FindControl("dvbtnnote1");
                DataTable dt1 = null;
                dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", hndWholesaleorderID.Value, "", "", "");
                if (dt1.Rows.Count > 0)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(dt1.Rows[0]["NoteDesIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["NoteDateIN"].ToString()))
                        {
                            // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                            dvbtnnote1.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                        }
                    }
                    catch { }
                }
            }

            #endregion
        }
        catch { }
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
        // bind();

    }

    public void bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {

            HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
            Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
            Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
            Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevert");
            Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
            Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblInverterrevert");
            Label lblpaneninstall = (Label)GridView1.Rows[i].FindControl("Label52");
            Label lblinverterIntall = (Label)GridView1.Rows[i].FindControl("Label752");
            Label lblpanelOut = (Label)GridView1.Rows[i].FindControl("Label452");
            Label lblinverterOut = (Label)GridView1.Rows[i].FindControl("Label4522");
            //LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
            LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");
            LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");


            //if (i != dt.Rows.Count - 1)
            //{
            //    if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
            //    {
            //       // gvbtnVerify.Visible = true;
            //    }
            //    else
            //    {
            //       // gvbtnVerify.Visible = false;
            //    }
            //}

            DataTable dt1 = null;
            dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
            if (dt1.Rows.Count > 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(dt1.Rows[0]["VerifynoteIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedateIN"].ToString()))
                    {
                        // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                        gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                    }
                }
                catch { }
            }
        }


    }

    public void BindGrid3(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        dv3 = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid3.Visible = false;
            divnopage3.Visible = false;
            div2.Visible = false;
            //divnopage2.Visible = false;
        }
        else
        {
            //divnopage2.Visible = true;
            div2.Visible = true;
            PanGrid3.Visible = true;
            GridView3.DataSource = dt;
            GridView3.DataBind();
            BindTotalWo(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords3.SelectedValue != string.Empty && ddlSelectRecords3.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords3.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage3.Visible = false;
                }
                else
                {
                    divnopage3.Visible = true;
                    //int iTotalRecords = (dv3.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv3.ToTable().Rows.Count);
                    int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords3.SelectedValue == "All")
                {
                    divnopage3.Visible = true;
                    ltrPage3.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                    //ltrPage3.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                }
            }

        }
        // bind3();
    }

    public void bind3()
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        for (int i = 0; i < GridView3.Rows.Count; i++)

        {
            HiddenField hndWholesaleorderID = (HiddenField)GridView3.Rows[i].FindControl("hndWholesaleorderID");
            Label lblpaneldiff = (Label)GridView3.Rows[i].FindControl("lblpaneldiff");
            Label lblwholepanelrevert = (Label)GridView3.Rows[i].FindControl("lblwholepanelrevert");
            Label lblinverterdiff = (Label)GridView3.Rows[i].FindControl("lblinverterdiff");
            Label lblwholeInverterrevert = (Label)GridView3.Rows[i].FindControl("lblwholeInverterrevert");
            Label lblpaneninstall = (Label)GridView3.Rows[i].FindControl("Label522");
            Label lblinverterIntall = (Label)GridView3.Rows[i].FindControl("Label7525");
            Label lblpanelOut = (Label)GridView3.Rows[i].FindControl("Label4521");
            Label lblinverterOut = (Label)GridView3.Rows[i].FindControl("Label45224");
            //LinkButton dvbtnVeriify = (LinkButton)GridView3.Rows[i].FindControl("gvbtnVerify1");
            LinkButton gvbtnView3 = (LinkButton)GridView3.Rows[i].FindControl("gvbtnView3");
            LinkButton dvbtnnote1 = (LinkButton)GridView3.Rows[i].FindControl("dvbtnnote1");
            DataTable dt1 = null;
            dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", hndWholesaleorderID.Value, "", "", "");
            if (dt1.Rows.Count > 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(dt1.Rows[0]["NoteDesIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["NoteDateIN"].ToString()))
                    {
                        // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                        dvbtnnote1.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                    }
                }
                catch { }
            }
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged3(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords3.SelectedValue) == "All")
        {
            GridView3.AllowPaging = false;
            BindGrid3(0);
        }
        else
        {
            GridView3.AllowPaging = true;
            GridView3.PageSize = Convert.ToInt32(ddlSelectRecords3.SelectedValue);
            BindGrid3(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        //GridView3.DataSource = dv3;
        //GridView3.DataBind();
        BindGrid3(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid3(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView3.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView3.PageIndex - 2;
            page[1] = GridView3.PageIndex - 1;
            page[2] = GridView3.PageIndex;
            page[3] = GridView3.PageIndex + 1;
            page[4] = GridView3.PageIndex + 2;
            page[5] = GridView3.PageIndex + 3;
            page[6] = GridView3.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView3.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView3.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView3.PageIndex == GridView3.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage3 = (Label)gvrow.Cells[0].FindControl("ltrPage3");
            if (dv3.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv3.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv3.ToTable().Rows.Count);
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage3.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    void lb_Command3(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid3(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command3);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        //DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeID("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "");
        //Response.Clear();
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 19, 22, 3, 4, 21, 16, 20 };
        //    string[] arrHeader = { "Project No.", "Installer Name", "Serial No.", "Pallet No.", "Category", "Stock Item", "Location" };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    protected void lbtnExport3_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();


        Export oExport = new Export();
        string FileName = "StockPendingReportWholeSale" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 2, 3, 5, 8, 4, 11, 9, 15, 13, 18, 12, 10, 16, 14, 19, 20, 22 };
        string[] arrHeader = { "Order No", "Invoice No", "Customer", "Deducted On", "Location", "P. out", "Inv. Panel", "P. Difference", "P. Revert", "P. Audit", "I. Out", "Inv. Inverter", "I. Difference", "I. Revert", "I. Audit", "AssignTo", "NewNotes" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        //txtstockitemfilter.Text = string.Empty;
        //txtserailno.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlIsDifference.SelectedValue = "1";
        ddlprojectwise.SelectedValue = "1";
        //lstProjectStatus.ClearSelection();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            //ddllocationsearch.Enabled = false;
        }

        ClearCheckBox();
        BindGrid(0);
    }

    protected void btnClearAll3_Click(object sender, EventArgs e)
    {
        txtstockitemfilter3.Text = string.Empty;
        txtserailno3.Text = string.Empty;
        txtInvoiceNo.Text = string.Empty;
        //ddllocationsearch3.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        ddldate3.SelectedValue = "";
        txtstartdate3.Text = string.Empty;
        txtenddate3.Text = string.Empty;
        ddlIsverify1.SelectedValue = "1";
        //ddlJobStatus.SelectedValue = "";
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch3.SelectedValue = CompanyLocationID;
            //ddllocationsearch3.Enabled = false;
        }

        ClearCheckBox2();
        BindGrid3(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }

        if (e.CommandName.ToLower() == "verify")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            }
            ModalPopupExtenderverify.Show();
        }

        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView1.Rows[rowIndex].FindControl("Label82") as LinkButton).Text;
            string PanelOut = (GridView1.Rows[rowIndex].FindControl("Label452") as Label).Text;

            string Projectid = (GridView1.Rows[rowIndex].FindControl("hndProjectID") as HiddenField).Value;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = PanelOut;

                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            //hndProjectID1.Value = Projectid;

            if (ProjectNo != "0" && PicklistId != "0")
            {
                //Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                //Repeater1.DataBind();

                DataTable dt = ClsReportsV2.tblStockSerialNo_GetSerialNoByPicklistIDCatID(PicklistId, "1");
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }

        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            // chkisactive.Visible = true;
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView1.Rows[rowIndex].FindControl("Label152") as LinkButton).Text;
            string InvertOut = (GridView1.Rows[rowIndex].FindControl("Label4522") as Label).Text;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = InvertOut;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            string Projectid = (GridView1.Rows[rowIndex].FindControl("hndProjectID") as HiddenField).Value;
            //hndProjectID1.Value = Projectid;

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                Repeater1.DataBind();

                DataTable dt = ClsReportsV2.tblStockSerialNo_GetSerialNoByPicklistIDCatID(PicklistId, "2");
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }

        if (e.CommandName.ToLower() == "note")
        {
            txtnotedesc.Text = "";
            txtnotedate.Text = "";
            hdnPickListId.Value = "";
            ddlAssignTo.SelectedValue = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            DataTable dt;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
                //dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                dt = ClstblPicklist.tbl_PickListLog_GetDataByID(PicklistId);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        txtnotedesc.Text = dt.Rows[0]["VerifynoteIN"].ToString();
                        DateTime notedt1 = Convert.ToDateTime(dt.Rows[0]["notedateIN"].ToString());
                        txtnotedate.Text = notedt1.ToString("dd/MM/yyyy");
                        if (dt.Rows[0]["AssignTo"].ToString() != "")
                        {
                            ddlAssignTo.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                        }
                        else
                        {
                            ddlAssignTo.SelectedValue = "";
                        }
                    }
                    catch { }
                }
                else
                {
                    txtnotedesc.Text = "";
                    txtnotedate.Text = "";
                    ddlAssignTo.SelectedValue = "";
                }
            }
            else
            {
                txtnotedesc.Text = "";
                txtnotedate.Text = "";
                ddlAssignTo.SelectedValue = "";
            }
            //txtnotedesc.Text = "";
            //txtnotedate.Text = "";
            ModalPopupExtenderNote.Show();
        }

        if (e.CommandName == "Email")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string InstallerEmail = arg[2];

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Project Detail";

            Server.Execute("~/mailtemplate/InstallerWiseEmail.aspx?ProjectNo=" + ProjectNo + "&PickList=" + PicklistId + "&WholesaleOrderID=0" + "&MAiltype=PickList", txtWriter);

            Utilities.SendMail(from, InstallerEmail, Subject, txtWriter.ToString());
        }

        if (e.CommandName == "ViewPInstalled")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string Panels = arg[2];

            txtPanel.Text = Panels;
            txtPanel.Focus();
            hndPInstalledPicklist.Value = PicklistId;
            hndPInstalledProjectNumber.Value = ProjectNo;
            hndPInstalledOldInstalled.Value = Panels;

            ModalPopupExtenderPInstalled.Show();
        }

        if (e.CommandName == "ViewIInstalled")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string Inverter = arg[2];

            txtInverter.Text = Inverter;
            txtInverter.Focus();
            hndIPickID.Value = PicklistId;
            hndIProjectNumber.Value = ProjectNo;
            hndIOldInstalled.Value = Inverter;

            ModalPopupExtenderIInstalled.Show();
        }

        if (e.CommandName == "Approve")
        {
            //string[] arg = new string[3];
            //arg = e.CommandArgument.ToString().Split(';');

            string PickListID = e.CommandArgument.ToString();

            VerifyPickListID.Value = PickListID;
            ModalPopupExtender2.Show();

        }

        //BindGrid(0);

        if (e.CommandName == "NewNote")
        {
            try
            {
                ddlAssignToNew.SelectedValue = "";
                ddlEmployees.SelectedValue = "";
                txtNotesNew.Text = "";
                hndNewNotesPicklistID.Value = "";
                hndMode.Value = "";

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string arg = e.CommandArgument.ToString();
                hndNewNotesPicklistID.Value = arg;

                BindNewAriseNotes(arg, userid);

                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }

        if (e.CommandName == "ViewPanels")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNumber = arg[0];
            string PicklistID = arg[1];

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string PanelReverted = (GridView1.Rows[rowIndex].FindControl("lblpanelrevert") as LinkButton).Text;
            hndDifference.Value = PanelReverted;

            DataTable dt = ClsReportsV2.tblMaintainHistory_GetRevertItemByPicklistID(PicklistID, "1");
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            ModalPopupExtenderRevert.Show();
        }

        if (e.CommandName == "ViewInverter")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNumber = arg[0];
            string PicklistID = arg[1];

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string PanelReverted = (GridView1.Rows[rowIndex].FindControl("lblInverterrevert") as LinkButton).Text;
            hndDifference.Value = PanelReverted;

            DataTable dt = ClsReportsV2.tblMaintainHistory_GetRevertItemByPicklistID(PicklistID, "2");
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            ModalPopupExtenderRevert.Show();
        }


        if (e.CommandName == "ViewP_BSGB")
        {
            string ProjectNumber = e.CommandArgument.ToString();

            DataTable dtSerialNo = ClsReportsV2.tblSerialNoFromBSGB_GetDataByProjectNo(ProjectNumber, "1");

            if (dtSerialNo.Rows.Count > 0)
            {
                Repeater3.DataSource = dtSerialNo;
                Repeater3.DataBind();
                ModalPopupExtenderPBSGB.Show();
            }
            else
            {
                Notification("Serial Number Not Found..");
            }

            //if (Flag == "1") // GreenBot
            //{
            //    DataTable dtSerialNo = ClsReportsV2.tblSerialNoFromBSGB_GetDataByProjectNo(ProjectNumber, Flag);

            //    if (dtSerialNo.Rows.Count > 0)
            //    {
            //        Repeater3.DataSource = dtSerialNo;
            //        Repeater3.DataBind();
            //        ModalPopupExtenderPBSGB.Show();
            //    }
            //    else
            //    {
            //        Notification("Serial Number Not Found..");
            //    }
            //}
            //else if (Flag == "2")
            //{
            //    try
            //    {
            //        Job_Response.Details GetJobDetailsFromBs = GetJobDetailsFromBS(ProjectNumber);

            //        //if (GetJobDetailsFromBs.Panels.Count > 0)
            //        //{
            //        //    Repeater3.DataSource = GetJobDetailsFromBs.Panels;
            //        //    Repeater3.DataBind();
            //        //    ModalPopupExtenderPBSGB.Show();
            //        //}
            //    }
            //    catch(Exception ex)
            //    {

            //    }

            //}

        }

        if (e.CommandName == "ViewI_BSGB")
        {
            string ProjectNumber = e.CommandArgument.ToString();

            DataTable dtSerialNo = ClsReportsV2.tblSerialNoFromBSGB_GetDataByProjectNo(ProjectNumber, "2");

            if (dtSerialNo.Rows.Count > 0)
            {
                Repeater3.DataSource = dtSerialNo;
                Repeater3.DataBind();
                ModalPopupExtenderPBSGB.Show();
            }
            else
            {
                Notification("Serial Number Not Found..");
            }
        }

        //Swipe Reverted Panel
        if (e.CommandName == "SwipeRevertPanel")
        {
            string arg = e.CommandArgument.ToString();

            ResetTextBox(); // Reset Model Pop-Up Textbox

            string PickListID = arg;
            string CategoryID = "1";
            hndCategoryID.Value = CategoryID;

            DataTable dtPanel = new DataTable();
            if (!string.IsNullOrEmpty(PickListID))
            {
                dtPanel = ClsReportsV2.SP_GetRevertedSerialNumber_ByPickListID(PickListID, CategoryID);
            }
            else
            {
                Notification("PicklistID is Null");
            }

            if (dtPanel.Rows.Count > 0)
            {
                RptRevertNew.DataSource = dtPanel;
                RptRevertNew.DataBind();

                ModalPopupExtenderRevertNew.Show();
            }
            else
            {
                Notification("Serial Numbert Not Found..");
            }
        }

        //Swipe Reverted Inverter
        if (e.CommandName == "SwipeRevertInverter")
        {
            string arg = e.CommandArgument.ToString();

            ResetTextBox(); // Reset Model Pop-Up Textbox

            string PickListID = arg;
            string CategoryID = "2";
            hndCategoryID.Value = CategoryID;

            DataTable dtInverter = new DataTable();
            if (!string.IsNullOrEmpty(PickListID))
            {
                dtInverter = ClsReportsV2.SP_GetRevertedSerialNumber_ByPickListID(PickListID, CategoryID);
            }
            else
            {
                Notification("PicklistID is Null");
            }

            if (dtInverter.Rows.Count > 0)
            {
                RptRevertNew.DataSource = dtInverter;
                RptRevertNew.DataBind();

                ModalPopupExtenderRevertNew.Show();
            }
            else
            {
                Notification("Serial Numbert Not Found..");
            }
        }
    }

    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage3")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
            rptItems.DataBind();
            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "verify")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            hndwholesaleorderID.Value = WholesaleOrderID;


            ModalPopupExtenderWholeSaleVerify.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            txtwholesaleprojNo.Text = "";
            string WholesaleOrderID = e.CommandArgument.ToString();
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView3.Rows[rowIndex].FindControl("lblpaneldiff") as LinkButton).Text;
            string PanelOut = (GridView3.Rows[rowIndex].FindControl("Label4521") as Label).Text;
            string WholesaleID = (GridView3.Rows[rowIndex].FindControl("hndWholesaleorderID") as HiddenField).Value;
            hndDifference1.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference1.Value = PanelOut;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive1.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive1.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive1.Visible = false;
            }

            Repeater2.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("3", "", "1", "", "", WholesaleOrderID, "", "", "");
            Repeater2.DataBind();

            ModalPopupExtender1.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            txtwholesaleprojNo.Text = "";
            string WholesaleOrderID = e.CommandArgument.ToString();

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView3.Rows[rowIndex].FindControl("lblinverterdiff") as LinkButton).Text;
            string InverterOut = (GridView3.Rows[rowIndex].FindControl("Label45224") as Label).Text;
            hndDifference1.Value = Cat_name;
            string WholesaleID = (GridView3.Rows[rowIndex].FindControl("hndWholesaleorderID") as HiddenField).Value;
            if (Cat_name == "0")
            {
                hndDifference1.Value = InverterOut;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive1.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive1.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive1.Visible = false;
            }
            Repeater2.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("3", "", "2", "", "", WholesaleOrderID, "", "", "");
            Repeater2.DataBind();

            ModalPopupExtender1.Show();
        }
        if (e.CommandName == "Wholesalenotedetail")
        {
            txtwholesaledate.Text = "";
            txtwholesalenote.Text = "";
            hndwholesaleorderID.Value = "";
            ddlSearchAssignToW.SelectedValue = "";

            DataTable dt = new DataTable();
            string WholesaleOrderID = e.CommandArgument.ToString();
            hndwholesaleorderID.Value = WholesaleOrderID;
            //dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
            dt = ClsReportsV2.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
            if (dt.Rows.Count > 0)
            {
                try
                {
                    DateTime wnotedate = Convert.ToDateTime(dt.Rows[0]["NoteDateIN"].ToString());
                    txtwholesaledate.Text = wnotedate.ToString("dd/MM/yyyy");
                    txtwholesalenote.Text = dt.Rows[0]["NoteDesIN"].ToString();
                    if (dt.Rows[0]["AssignTo"].ToString() != "")
                    {
                        ddlAssignToW.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                    }
                    else
                    {
                        ddlAssignToW.SelectedValue = "";
                    }
                }
                catch { }
            }
            else
            {
                txtwholesaledate.Text = "";
                txtwholesalenote.Text = "";
                ddlAssignToW.SelectedValue = "";
            }
            //txtwholesaledate.Text = "";
            //txtwholesalenote.Text = "";
            ModalPopupExtenderWholeNote.Show();
        }
        //if (e.CommandName == "Transfer")
        //{
        //    string WholesaleOrderID = e.CommandArgument.ToString();
        //    ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID,"1","","");
        //}
        if (e.CommandName == "Email")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string WholesaleOrderID = arg[0];
            string VendorEmail = arg[1];

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "WholeSale Detail";

            Server.Execute("~/mailtemplate/InstallerWiseEmail.aspx?ProjectNo=0" + "&PickList=0" + "&WholesaleOrderID=" + WholesaleOrderID + "&MAiltype=WholeSale", txtWriter);

            Utilities.SendMail(from, VendorEmail, Subject, txtWriter.ToString());
        }

        if (e.CommandName == "NewNote")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            try
            {
                ddlAssignToNewW.SelectedValue = "";
                txtNotesNewW.Text = "";
                hndNewNotesWholesaleID.Value = "";
                hndModeW.Value = "";

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string arg = e.CommandArgument.ToString();
                hndNewNotesWholesaleID.Value = arg;

                BindNewWholesaleNotes(arg, userid);
                ModalPopupExtenderNewNoteW.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }
        //BindGrid3(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label452");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label52");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label4522");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label752");

            //HiddenField hndprojectnumber = (HiddenField)e.Row.FindControl("hndProjectID");
            //Label lblpaneldiff = (Label)e.Row.FindControl("Label82");
            //Label lblpanelrevrt = (Label)e.Row.FindControl("lblpanelrevert");
            //Label lblinverterdiff = (Label)e.Row.FindControl("Label152");
            //Label lblinverterrevert = (Label)e.Row.FindControl("lblInverterrevert");
            //Label lblProjectNumber = (Label)e.Row.FindControl("Label11");            
            //// LinkButton gvbtnVerify = (LinkButton)e.Row.FindControl("gvbtnVerify");
            //LinkButton gvbtnView = (LinkButton)e.Row.FindControl("gvbtnView");
            //LinkButton btnviewrevert1 = (LinkButton)e.Row.FindControl("btnviewrevert1");
            //LinkButton btnviewrevert2 = (LinkButton)e.Row.FindControl("btnviewrevert2");
            //LinkButton gvbnNote = (LinkButton)e.Row.FindControl("gvbnNote");            
            //LinkButton gvbnEmail = (LinkButton)e.Row.FindControl("gvbnEmail");            
            //Label lbl = (Label)e.Row.FindControl("Label82");
            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            //if (string.IsNullOrEmpty(hndprojectnumber.Value))
            //{
            //    DataTable dt = new DataTable();
            //    dt = GetGridData1();
            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;


            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;



            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblpanelrevrt.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblinverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            // gvbtnVerify.Visible = false;
            //            gvbtnView.Visible = false;
            //            btnviewrevert1.Visible = false;
            //            btnviewrevert2.Visible = false;
            //            gvbnNote.Visible = false;
            //            imgdiv.Visible = false;
            //            gvbnEmail.Visible = false;

            //            lbltotpanel.Text = Convert.ToString(totpanel);
            //            totpanel = paneltotdiff - paneltotrevert;

            //            totinverter = invertertotdiff - invertertotrevert;
            //            lbltotInverter.Text = Convert.ToString(totinverter);
            //        }
            //    }

            //    if (string.IsNullOrEmpty(hndprojectnumber.Value))
            //    {
            //        lblProjectNumber.Text = "";
            //    }                
            //}            
        }
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label4521");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label522");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label45224");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label7525");

            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");
            //HiddenField hndWholesaleorder = (HiddenField)e.Row.FindControl("hndWholesaleorderID");
            //Label lblpaneldiff = (Label)e.Row.FindControl("lblpaneldiff");
            //Label lblwholepanelrevert = (Label)e.Row.FindControl("lblwholepanelrevert");
            //Label lblinverterdiff = (Label)e.Row.FindControl("lblinverterdiff");
            //Label lblwholeInverterrevert = (Label)e.Row.FindControl("lblwholeInverterrevert");
            ////LinkButton dvbtnVeriify = (LinkButton)e.Row.FindControl("gvbtnVerify1");
            //LinkButton gvbtnView3 = (LinkButton)e.Row.FindControl("gvbtnView3");
            //LinkButton btnviewholerevert = (LinkButton)e.Row.FindControl("btnviewholerevert");
            //LinkButton btnviewholerevert1 = (LinkButton)e.Row.FindControl("btnviewholerevert1");
            //LinkButton dvbtnnote1 = (LinkButton)e.Row.FindControl("dvbtnnote1");
            //LinkButton gvbnEmail1 = (LinkButton)e.Row.FindControl("gvbnEmail1");
            //if (string.IsNullOrEmpty(hndWholesaleorder.Value))
            //{
            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;

            //    DataTable dt = new DataTable();
            //    dt = GetGridData3();
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;


            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblwholepanelrevert.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblwholeInverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            //dvbtnVeriify.Visible = false;
            //            gvbtnView3.Visible = false;
            //            btnviewholerevert.Visible = false;
            //            btnviewholerevert1.Visible = false;
            //            dvbtnnote1.Visible = false;
            //            imgdiv.Visible = false;
            //            gvbnEmail1.Visible = false;
            //        }

            //    }

            //    totpanel = paneltotdiff - paneltotrevert;
            //    lbltotpanel1.Text = Convert.ToString(totpanel);

            //    totinverter = invertertotdiff - invertertotrevert;
            //    lbltotInverter1.Text = Convert.ToString(totinverter);

            //}

        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("WholeSale"))
        {
            TabContainer1.ActiveTabIndex = 0;
            BindCheckboxWTab();
            //BindGrid3(0);
            //BindDropDown3();
        }
        else
        {
            if (TabContainer1.ActiveTabIndex == 0)
            {
                BindCheckboxProjectTab();
                //BindGrid(0);
                //BindDropDown();
                //Response.Redirect("~/admin/adminfiles/reports/installerwisereport.aspx");
            }
            else if (TabContainer1.ActiveTabIndex == 1)
            {
                //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

                if (Roles.IsUserInRole("Warehouse"))
                {

                    //BindDropDown3();
                    BindCheckboxWTab();
                    //DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                    //string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                    //ddllocationsearch3.SelectedValue = CompanyLocationID;
                    //ddllocationsearch3.Enabled = false;
                    //BindGrid3(0);
                }
                else
                {
                    BindCheckboxWTab();
                    //BindGrid3(0);
                    //BindDropDown3();
                }
            }
            else if (TabContainer1.ActiveTabIndex == 2)
            {
                BindCheckboxSMTab();
                //BindGridSM(0);
            }
        }
    }

    protected void lnkverify_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_UpdateIsverify_ForInstallerReport(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            BindGrid(0);
        }

    }

    protected void btnwholesaleVerify1_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string wholesaleorderID = hndwholesaleorderID.Value;
        if (wholesaleorderID != "0")
        {
            ClstblrevertItem.tbl_WholesaleOrders_UpdateIsverify__ForInstallerReport(Convert.ToInt32(wholesaleorderID), Currendate, userid, "1");
            BindGrid3(0);
        }

    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

        Export oExport = new Export();
        string FileName = "StockPendingReportProject" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 22, 2, 38, 3, 4, 29, 5, 6,
                          7, 8, 9, 23, 10, 11, 12, 13, 14, 24,
                          15, 16, 17, 19, 28, 20, 32, 34, 35, 41, 42 };

        string[] arrHeader = { "PL Count", "ProjectNumber", "Install State", "Project Status", "Installer", "Installer Pick Up", "Install Date", "Deducted On", "Location", "P. out", "P. Installed", "P. BS/GB", "P. Difference", "P. Revert", "P. Audit", "I. Out", "I. Installed", "I. BS/GB", "I. Difference", "I. Revert", "I. Audit", "Note", "Verify Notes", "SystemDetails", "Assign To", "New Notes", "Assined Employee", "Employee Notes", "Employee All Notes" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

        BindGrid(0);
    }

    protected void lnksubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        //chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        //chkDifference.Checked = false;
        //    }

        //}
        //if (i > Convert.ToInt32(hndDifference.Value))
        //{                    
        //    ModalPopupExtenderRevert.Show();
        //    SetError();
        //}
        //else
        //{
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            HiddenField rpthndProjectid = (HiddenField)item.FindControl("rpthndProjectid");
            HiddenField rpthndPicklistId = (HiddenField)item.FindControl("rpthndPicklistId");
            if (chkDifference.Checked == true)
            {

                if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                {
                    DataTable dt = null;
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                    if (!string.IsNullOrEmpty(txtprojectno.Text))
                    {
                        string[] ProjPickId = txtprojectno.Text.Split('/');
                        dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjPickId[0].ToString());
                        if (dt.Rows.Count > 0)
                        {
                            string projectid = dt.Rows[0]["ProjectID"].ToString();
                            //string PicklistId = dt.Rows[0]["ID"].ToString();
                            string PicklistId = ProjPickId[1].ToString();

                            string LocationId = dt.Rows[0]["LocationId"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                            section = "Stock Deduct";
                            Message = "Stock Deduct For Project No:" + ProjPickId[0].ToString() + "& PicklistId:" + PicklistId + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            //ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");

                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            //    ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}

                        }
                    }
                    if (!string.IsNullOrEmpty(TextBox1.Text))
                    {
                        dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                            section = "Stock Deduct";
                            Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value, dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            //ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "2", userid, date.ToString());
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}
                            //section = "Wholesale Revert";
                            //Message = "Stock Add For WholesaleId:" + rpthndWholesaleOrderId.Value + "By administrator";
                            //ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                        }
                    }
                    //DataTable dtexist = ClstblProjects.tblStockSerialNo_Select_ByProjectIdandPicklistid(rpthndProjectid.Value, rpthndPicklistId.Value);
                    //if (dtexist.Rows.Count == 0)
                    //{
                    //    ClstblProjects.tbl_picklistlog_UpdateData(rpthndProjectid.Value, rpthndPicklistId.Value, "1", "", "");
                    //}
                }
            }
        }
        BindGrid(0);
        if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }
    }

    protected void chkDifference_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        //MsgError("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtenderRevert.Show();
    }

    protected void lnkwholeSaleSubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        int i = 0;
        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");

        //    if (chkDifference.Checked == true)
        //    {
        //        //chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        //chkDifference.Checked = false;
        //    }

        //}
        //if (i > Convert.ToInt32(hndDifference1.Value))
        //{

        //    SetError();
        //    ModalPopupExtender1.Show();
        //    // break;
        //}
        //else
        //{
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            HiddenField rpthndWholesaleOrderId = (HiddenField)item.FindControl("rpthndWholesaleOrderId");
            HiddenField hndInvoiceNo = (HiddenField)item.FindControl("hndInvoiceNo");
            if (chkDifference.Checked == true)
            {

                if ((!string.IsNullOrEmpty(txtwholesaleprojNo.Text)) || (!string.IsNullOrEmpty(TextBox2.Text)))
                {
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid1");
                    DataTable dt = null;
                    if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
                    {
                        dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                            section = "Stock Deduct";
                            Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblrevertItem.tbl_WholesaleOrders_UpdateData(txtwholesaleprojNo.Text, "2", userid, date.ToString());
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference1.Value))
                            //{
                            ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                            //}
                        }
                    }
                    if (!string.IsNullOrEmpty(TextBox2.Text))
                    {
                        //dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                        dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                        if (dt.Rows.Count > 0)
                        {
                            //dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                            string projectid = dt.Rows[0]["ProjectID"].ToString();
                            string PicklistId = dt.Rows[0]["ID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                            section = "Stock Deduct";
                            Message = "Stock Deduct For Project No:" + txtprojectno.Text + "& PicklistId:" + PicklistId + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblProjects.tbl_picklistlog_UpdateData(projectid, PicklistId, "2", date.ToString(), userid);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference1.Value))
                            //{
                            ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                            //}
                        }
                    }
                    DataTable dtexist = Clstbl_WholesaleOrders.tblStockSerialNo_Select_ByWholesaleOrderID(rpthndWholesaleOrderId.Value);
                    if (dtexist.Rows.Count == 0)
                    {
                        ClstblrevertItem.tbl_WholesaleOrders_UpdateData(rpthndWholesaleOrderId.Value, "1", "", "");
                    }
                }
            }
        }
        BindGrid3(0);

        // }

    }

    protected void chkDifference1_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference1.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        Notification("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtender1.Show();
    }

    protected void btnsavenote_Click(object sender, EventArgs e)
    {
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_NoteUpdateIN(Convert.ToInt32(PicklistId), txtnotedate.Text, txtnotedesc.Text);
            ClsReportsV2.tbl_PickListLog_Update_AssignToByID(PicklistId, ddlAssignTo.SelectedValue);
        }

        if (TabContainer1.ActiveTabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }

        txtnotedesc.Text = "";
        txtnotedate.Text = "";
        hdnPickListId.Value = "";
        ddlAssignTo.SelectedValue = "";
    }

    protected void btnwholesalesave_Click(object sender, EventArgs e)
    {
        string wholesaleorderID = hndwholesaleorderID.Value;
        if (wholesaleorderID != "0")
        {
            ClstblrevertItem.tbl_WholesaleOrders_NoteUpdateIN(Convert.ToInt32(wholesaleorderID), txtwholesalenote.Text, txtwholesaledate.Text);
            ClsReportsV2.tbl_WholesaleOrders_Update_AssignToByID(wholesaleorderID, ddlAssignToW.SelectedValue);
        }
        BindGrid3(0);
    }

    protected void txtwholesaleprojNo_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hnditemid1");

            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid1.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtwholesaleprojNo.Text = string.Empty;
        }
        if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
        {
            TextBox2.Text = string.Empty;
            txtwholesaleprojNo.Focus();
        }
        ModalPopupExtender1.Show();
    }

    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hnditemid1");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid1.Value, Categorynm, TextBox2.Text);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox2.Text = string.Empty;
            TextBox2.Focus();
            //Notification("Record with this model number already exists.");
        }
        if (!string.IsNullOrEmpty(TextBox2.Text))
        {
            txtwholesaleprojNo.Text = string.Empty;
            TextBox2.Focus();
        }
        ModalPopupExtender1.Show();
    }

    protected void txtprojectno_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        if (TabContainer1.ActiveTabIndex == 0) // Arise Projects
        {
            string[] ProjPickId = txtprojectno.Text.Split('/');
            //string ProjectNumber = txtprojectno.Text
            string ProjectNumber = "";
            string PickID = "";
            if (ProjPickId.Length > 1)
            {
                ProjectNumber = ProjPickId[0].ToString();
                PickID = ProjPickId[1].ToString();
            }
            else
            {
                txtprojectno.Text = string.Empty;
                txtprojectno.Focus();
                Notification("Please Select Picklist");
                ModalPopupExtenderRevert.Show();
                return;
            }

            DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
            string locationid = "";
            if (dt.Rows.Count > 0)
            {
                locationid = dt.Rows[0]["LocationId"].ToString();
            }

            if (string.IsNullOrEmpty(locationid))
            {
                fail1++;
            }

            foreach (RepeaterItem item in Repeater1.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
                HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
                Label CategoryName = (Label)item.FindControl("lblCategory");
                string Categorynm = "";
                if (CategoryName.Text == "Modules")
                {
                    Categorynm = "1";
                }
                else
                {
                    Categorynm = "2";
                }
                if (chkDifference.Checked == true)
                {
                    //int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid.Value, Categorynm, ProjPickId[0].ToString());
                    int exist = ClstblProjects.Exist_ItemId_tbl_PicklistItemDetail(hnditemid.Value, Categorynm, PickID);
                    if (exist == 0)
                    {
                        suc++;
                    }
                    else
                    {
                        fail++;
                    }
                }
            }
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            string[] ProjPickId = txtprojectno.Text.Split('/');
            //string ProjectNumber = txtprojectno.Text;
            string ProjectNumber = ProjPickId[0].ToString();
            string PickID = ProjPickId[1].ToString();

            if (ProjPickId.Length > 1)
            {
                ProjectNumber = ProjPickId[0].ToString();
                PickID = ProjPickId[1].ToString();
            }
            else
            {
                txtprojectno.Text = string.Empty;
                txtprojectno.Focus();
                Notification("Please Select Picklist");
                ModalPopupExtenderRevert.Show();
                return;
            }

            DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
            string locationid = "";
            if (dt.Rows.Count > 0)
            {
                locationid = dt.Rows[0]["LocationId"].ToString();
            }

            if (string.IsNullOrEmpty(locationid))
            {
                fail1++;
            }

            foreach (RepeaterItem item in Repeater1.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
                HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
                Label CategoryName = (Label)item.FindControl("lblCategory");
                string Categorynm = "";
                if (CategoryName.Text == "Modules")
                {
                    Categorynm = "1";
                }
                else
                {
                    Categorynm = "2";
                }
                if (chkDifference.Checked == true)
                {
                    int exist = ClstblProjects.Exist_ItemId_tblprojectSM(hnditemid.Value, Categorynm, ProjPickId[0].ToString());
                    if (exist == 0)
                    {
                        suc++;
                    }
                    else
                    {
                        fail++;
                    }
                }
            }
        }



        if (fail > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            //Notification("Record with this model number already exists.");

        }
        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }
        if (!string.IsNullOrEmpty(txtprojectno.Text))
        {
            TextBox1.Text = string.Empty;
            txtprojectno.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string InvoiceNumber = TextBox1.Text;
        DataTable dt1 = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(InvoiceNumber);
        string locationid = dt1.Rows[0]["CompanyLocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox1.Text = string.Empty;
        }
        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }

        if (!string.IsNullOrEmpty(TextBox1.Text))
        {
            txtprojectno.Text = string.Empty;
            TextBox1.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void chkisactive_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference");

            if (chkisactive.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void chkisactive1_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference1 = (CheckBox)item.FindControl("chkDifference1");

            if (chkisactive1.Checked == true)
            {
                chkDifference1.Checked = true;
            }
            else
            {
                chkDifference1.Checked = false;
            }
        }
        ModalPopupExtender1.Show();
    }

    public void BindCheckbox()
    {
        rptProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        rptProjectStatus.DataBind();

        //rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //rptAriseInstaller.DataBind();



        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();

        rptWJobStatus.DataSource = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
        rptWJobStatus.DataBind();

        rptLocationW.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocationW.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();

        rptSMLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptSMLocation.DataBind();

        rptSMProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        rptSMProjectStatus.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();
    }

    public void BindCheckboxProjectTab()
    {
        rptProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        rptProjectStatus.DataBind();

        //rptAriseInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //rptAriseInstaller.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();

        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();
    }

    public void BindAssignTo()
    {
        DataTable dt = ClsReportsV2.tblPendingAssign_GetData();
        ddlAssignTo.DataSource = dt;
        ddlAssignTo.DataValueField = "ID";
        ddlAssignTo.DataTextField = "Department";
        ddlAssignTo.DataBind();

        ddlSearchAssignTo.DataSource = dt;
        ddlSearchAssignTo.DataValueField = "ID";
        ddlSearchAssignTo.DataTextField = "Department";
        ddlSearchAssignTo.DataBind();

        ddlSearchAssignToSM.DataSource = dt;
        ddlSearchAssignToSM.DataValueField = "ID";
        ddlSearchAssignToSM.DataTextField = "Department";
        ddlSearchAssignToSM.DataBind();

        ddlSearchAssignToW.DataSource = dt;
        ddlSearchAssignToW.DataValueField = "ID";
        ddlSearchAssignToW.DataTextField = "Department";
        ddlSearchAssignToW.DataBind();

        ddlAssignToW.DataSource = dt;
        ddlAssignToW.DataValueField = "ID";
        ddlAssignToW.DataTextField = "Department";
        ddlAssignToW.DataBind();

        ddlAssignToNew.DataSource = dt;
        ddlAssignToNew.DataValueField = "ID";
        ddlAssignToNew.DataTextField = "Department";
        ddlAssignToNew.DataBind();

        ddlAssignToNewW.DataSource = dt;
        ddlAssignToNewW.DataValueField = "ID";
        ddlAssignToNewW.DataTextField = "Department";
        ddlAssignToNewW.DataBind();

        ddlEmployees.DataSource = ClstblEmployees.tblEmployees_Select();
        ddlEmployees.DataValueField = "EmployeeID";
        ddlEmployees.DataTextField = "fullname";
        ddlEmployees.DataBind();
    }

    public void BindCheckboxWTab()
    {
        rptWJobStatus.DataSource = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
        rptWJobStatus.DataBind();

        rptLocationW.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocationW.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();

    }

    public void BindCheckboxSMTab()
    {
        rptSMLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptSMLocation.DataBind();

        rptSMProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        rptSMProjectStatus.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptAriseInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void ClearCheckBox2()
    {
        foreach (RepeaterItem item in rptLocationW.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWJobStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    #region SM Tab
    protected DataTable GetGridDataSM()// SolerMiner
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlVerifySM.SelectedValue))
        {
            Isverify = ddlVerifySM.SelectedValue;
        }

        string selectedProjectStatusItem = "";
        foreach (RepeaterItem item in rptSMProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedProjectStatusItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedProjectStatusItem != "")
        {
            selectedProjectStatusItem = selectedProjectStatusItem.Substring(1);
        }

        string selectedInstallerItem = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedInstallerItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedInstallerItem != "")
        {
            selectedInstallerItem = selectedInstallerItem.Substring(1);
        }

        string selectedLocationItem = "";
        foreach (RepeaterItem item in rptSMLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnSMLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }

        DataTable dt = ClsReportsV2.StockPendingReport_GetData_SMV2(txtSMProjectNo.Text, selectedLocationItem, selectedInstallerItem, selectedProjectStatusItem, ddlSMDate.SelectedValue, txtSMStartDate.Text, txtSMendDate.Text, Isverify, ddlSMIsDifference.SelectedValue, ddlSMYesNO.SelectedValue, ddlSearchAssignToSM.SelectedValue, ddlIsDeductSM.SelectedValue, ddlPicklistCountSM.SelectedValue);

        //DataTable dt = ClsReportsV2.StockPendingReport_GetData_SMV2("211888,215748", selectedLocationItem, selectedInstallerItem, selectedProjectStatusItem, ddlSMDate.SelectedValue, txtSMStartDate.Text, txtSMendDate.Text, Isverify, ddlSMIsDifference.SelectedValue, ddlSMYesNO.SelectedValue, ddlSearchAssignToSM.SelectedValue, ddlIsDeductSM.SelectedValue, ddlPicklistCountSM.SelectedValue);

        return dt;
    }

    public void BindGridSM(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGridSm.Visible = false;
            divnopageSM.Visible = false;
            divtotSM.Visible = false;
            divSmTot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtotSM.Visible = false;
            PanGridSm.Visible = true;
            divSmTot.Visible = true;
            GridView_SM.DataSource = dt;
            GridView_SM.DataBind();
            BindTotalSM(dt);
            if (dt.Rows.Count > 0 && ddlSMSelectRecords.SelectedValue != string.Empty && ddlSMSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSMSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopageSM.Visible = false;
                }
                else
                {
                    divnopageSM.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView_SM.PageSize * (GridView_SM.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView_SM.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSMSelectRecords.SelectedValue == "All")
                {
                    divnopageSM.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
        // bind();

    }

    public void SMbind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {

            HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
            Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
            Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
            Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevert");
            Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
            Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblInverterrevert");
            Label lblpaneninstall = (Label)GridView1.Rows[i].FindControl("Label52");
            Label lblinverterIntall = (Label)GridView1.Rows[i].FindControl("Label752");
            Label lblpanelOut = (Label)GridView1.Rows[i].FindControl("Label452");
            Label lblinverterOut = (Label)GridView1.Rows[i].FindControl("Label4522");
            //LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
            LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");
            //LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");


            //if (i != dt.Rows.Count - 1)
            //{
            //    if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
            //    {
            //       // gvbtnVerify.Visible = true;
            //    }
            //    else
            //    {
            //       // gvbtnVerify.Visible = false;
            //    }
            //}

            //DataTable dt1 = null;
            //dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
            //if (dt1.Rows.Count > 0)
            //{
            //    try
            //    {
            //        if (!string.IsNullOrEmpty(dt1.Rows[0]["VerifynoteIN"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedateIN"].ToString()))
            //        {
            //            // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
            //            gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
            //        }
            //    }
            //    catch { }
            //}
        }


    }

    protected void GridView_SM_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView_SM.DataSource = sortedView;
        GridView_SM.DataBind();
    }

    protected void GridView_SM_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_SM.PageIndex = e.NewPageIndex;
        GridView_SM.DataSource = dv;
        GridView_SM.DataBind();
        BindGridSM(0);
    }

    protected void GridView_SM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "verify")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            }
            ModalPopupExtenderverify.Show();
        }

        if (e.CommandName.ToLower() == "viewrevertpanel")
        {
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];


            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView_SM.Rows[rowIndex].FindControl("Label82") as LinkButton).Text;
            string PanelOut = (GridView_SM.Rows[rowIndex].FindControl("Label452") as Label).Text;

            string Projectid = (GridView_SM.Rows[rowIndex].FindControl("hndSMProjectID") as HiddenField).Value;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = PanelOut;

                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            //hndProjectID1.Value = Projectid;

            if (ProjectNo != "0" && PicklistId != "0")
            {
                Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_SM("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }
        if (e.CommandName.ToLower() == "viewrevertinverter")
        {
            // chkisactive.Visible = true;
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            string Cat_name = (GridView_SM.Rows[rowIndex].FindControl("Label152") as LinkButton).Text;
            string InvertOut = (GridView_SM.Rows[rowIndex].FindControl("Label4522") as Label).Text;
            hndDifference.Value = Cat_name;
            if (Cat_name == "0")
            {
                hndDifference.Value = InvertOut;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }
            }
            if (Convert.ToInt32(Cat_name) > 0)
            {
                chkisactive.Visible = false;
            }
            if (Convert.ToInt32(Cat_name) < 0)
            {
                chkisactive.Visible = false;
            }
            string Projectid = (GridView_SM.Rows[rowIndex].FindControl("hndSMProjectID") as HiddenField).Value;
            //hndProjectID1.Value = Projectid;

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new_SM("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                Repeater1.DataBind();
            }

            ModalPopupExtenderRevert.Show();
        }
        if (e.CommandName.ToLower() == "note")
        {
            txtnotedesc.Text = "";
            txtnotedate.Text = "";
            ddlAssignTo.SelectedValue = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            DataTable dt;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
                //dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                dt = ClstblPicklist.tbl_PickListLog_GetDataByID(PicklistId);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        txtnotedesc.Text = dt.Rows[0]["VerifynoteIN"].ToString();
                        DateTime notedt1 = Convert.ToDateTime(dt.Rows[0]["notedateIN"].ToString());
                        txtnotedate.Text = notedt1.ToString("dd/MM/yyyy");
                        if (dt.Rows[0]["AssignTo"].ToString() != "")
                        {
                            ddlAssignTo.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                        }
                        else
                        {
                            ddlAssignTo.SelectedValue = "";
                        }
                    }
                    catch { }
                }
                else
                {
                    txtnotedesc.Text = "";
                    txtnotedate.Text = "";

                }
            }
            else
            {
                txtnotedesc.Text = "";
                txtnotedate.Text = "";

            }
            //txtnotedesc.Text = "";
            //txtnotedate.Text = "";
            ModalPopupExtenderNote.Show();
        }
        //if (e.CommandName == "Transfer")
        //{
        //    string[] arg = new string[2];
        //    arg = e.CommandArgument.ToString().Split(';');

        //    string ProjectNo = arg[0];
        //    string PicklistId = arg[1];
        //    ClstblProjects.tbl_picklistlog_UpdateData(ProjectNo, PicklistId,"1","","");
        //}
        if (e.CommandName == "Email")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            DataTable dtE = Reports.QuickStock_GetSMProjectDetailsByPNo(ProjectNo);

            string InstallerEmail = dtE.Rows[0]["ContEmail"].ToString();

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Project Detail";

            Server.Execute("~/mailtemplate/InstallerWiseEmail.aspx?ProjectNo=" + ProjectNo + "&PickList=" + PicklistId + "&WholesaleOrderID=0" + "&MAiltype=PickList", txtWriter);

            Utilities.SendMail(from, InstallerEmail, Subject, txtWriter.ToString());
        }

        if (e.CommandName == "ViewPInstalled")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string Panels = arg[2];

            txtPanel.Text = Panels;
            txtPanel.Focus();
            hndPInstalledPicklist.Value = PicklistId;
            hndPInstalledProjectNumber.Value = ProjectNo;
            hndPInstalledOldInstalled.Value = Panels;

            DataTable dtNotes = Reports.tbl_InstalledLog_GetNotesByID(PicklistId);

            if (dtNotes.Rows.Count > 0)
            {
                txtPInstalledNotes.Text = dtNotes.Rows[0]["Notes"].ToString();
            }

            ModalPopupExtenderPInstalled.Show();
        }

        if (e.CommandName == "ViewIInstalled")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            string Inverter = arg[2];

            txtInverter.Text = Inverter;
            txtInverter.Focus();
            hndIPickID.Value = PicklistId;
            hndIProjectNumber.Value = ProjectNo;
            hndIOldInstalled.Value = Inverter;

            DataTable dtNotes = Reports.tbl_InstalledLog_GetNotesByID(PicklistId);

            if (dtNotes.Rows.Count > 0)
            {
                txtIInstalledNotes.Text = dtNotes.Rows[0]["Notes"].ToString();
            }

            ModalPopupExtenderIInstalled.Show();
        }

        if (e.CommandName == "Approve")
        {
            //string[] arg = new string[3];
            //arg = e.CommandArgument.ToString().Split(';');

            string PickListID = e.CommandArgument.ToString();
            VerifyPickListID.Value = PickListID;
            ModalPopupExtender2.Show();

            //int UpdateApprovedFlag = Reports.tbl_Picklistlog_Update_ApprovedFlagByPickID(PickListID, "True");
            //BindGridSM(0);
        }

        //BindGridSM(0);

        if (e.CommandName == "NewNote")
        {
            try
            {
                ddlAssignToNew.SelectedValue = "";
                ddlEmployees.SelectedValue = "";
                txtNotesNew.Text = "";
                hndNewNotesPicklistID.Value = "";
                hndMode.Value = "";

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string arg = e.CommandArgument.ToString();
                hndNewNotesPicklistID.Value = arg;

                BindNewAriseNotes(arg, userid);

                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }

        if (e.CommandName == "ViewP_BSGB")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNumber = arg[0];
            string PicklistID = arg[1];

            //string[] SerialNoList = GetJobDetailsByProjectNo(ProjectNumber, SMUsername, SMPassword);
            DataTable dtSerialNo = ClsReportsV2.tblSerialNoFromBSGB_GetDataByProjectNo(ProjectNumber, "1");

            if (dtSerialNo.Rows.Count > 0)
            {
                Repeater3.DataSource = dtSerialNo;
                Repeater3.DataBind();
                ModalPopupExtenderPBSGB.Show();
            }
        }

        //Swipe Reverted Panel
        if (e.CommandName == "SwipeRevertPanel")
        {
            string arg = e.CommandArgument.ToString();

            ResetTextBox(); // Reset Model Pop-Up Textbox

            string PickListID = arg;
            string CategoryID = "1";
            hndCategoryID.Value = CategoryID;

            DataTable dtPanel = new DataTable();
            if (!string.IsNullOrEmpty(PickListID))
            {
                dtPanel = ClsReportsV2.SP_GetRevertedSerialNumber_ByPickListID(PickListID, CategoryID);
            }
            else
            {
                Notification("PicklistID is Null");
            }

            if (dtPanel.Rows.Count > 0)
            {
                RptRevertNew.DataSource = dtPanel;
                RptRevertNew.DataBind();

                ModalPopupExtenderRevertNew.Show();
            }
            else
            {
                Notification("Serial Numbert Not Found..");
            }
        }

        //Swipe Reverted Inverter
        if (e.CommandName == "SwipeRevertInverter")
        {
            string arg = e.CommandArgument.ToString();

            ResetTextBox(); // Reset Model Pop-Up Textbox

            string PickListID = arg;
            string CategoryID = "2";
            hndCategoryID.Value = CategoryID;

            DataTable dtInverter = new DataTable();
            if (!string.IsNullOrEmpty(PickListID))
            {
                dtInverter = ClsReportsV2.SP_GetRevertedSerialNumber_ByPickListID(PickListID, CategoryID);
            }
            else
            {
                Notification("PicklistID is Null");
            }

            if (dtInverter.Rows.Count > 0)
            {
                RptRevertNew.DataSource = dtInverter;
                RptRevertNew.DataBind();

                ModalPopupExtenderRevertNew.Show();
            }
            else
            {
                Notification("Serial Numbert Not Found..");
            }
        }
    }

    protected void GridView_SM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndSMProjectID = (HiddenField)e.Row.FindControl("hndSMProjectID");
            HiddenField hndSMProjectStatusID = (HiddenField)e.Row.FindControl("hndSMProjectStatusID");
            Label lblSMStatus = (Label)e.Row.FindControl("lblSMStatus");
            Label lblSMInstallerName = (Label)e.Row.FindControl("lblSMInstallerName");
            Label lblinstaller = (Label)e.Row.FindControl("lblinstaller");

            //SttblProjects objSolarMiner = ClstblProjects.tblProjects_SelectByProjectIDForSolarMiner(hndSMProjectID.Value);
            //lblSMInstallerName.Text = objSolarMiner.InstallerName;
            //lblinstaller.Text = objSolarMiner.InstallerName;

            //SttblProjects objPrjSts = ClstblProjects.tblProjects_SelectProjectStatusByProjectStatusIDForSolarMiner(hndSMProjectStatusID.Value);
            //lblSMStatus.Text = objPrjSts.ProjectStatus;
        }
    }

    protected void GridView_SM_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView_SM.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView_SM.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView_SM.PageIndex - 2;
            page[1] = GridView_SM.PageIndex - 1;
            page[2] = GridView_SM.PageIndex;
            page[3] = GridView_SM.PageIndex + 1;
            page[4] = GridView_SM.PageIndex + 2;
            page[5] = GridView_SM.PageIndex + 3;
            page[6] = GridView_SM.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView_SM.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView_SM.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView_SM.PageIndex == GridView_SM.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView_SM.PageSize * (GridView_SM.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView_SM.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView_SM_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_CommandSM);
        }
    }

    void lb_CommandSM(object sender, CommandEventArgs e)
    {
        GridView_SM.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGridSM(0);
    }

    protected void lbtnSMExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "StockPendingReportSMProject" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 22, 2, 3, 4, 29, 5, 6,
                          7, 8, 9, 23, 10, 11, 12, 13, 14, 24,
                          15, 16, 17, 28, 20, 32, 34, 35, 39, 40 };

        string[] arrHeader = { "PL Count", "ProjectNumber", "Project Status", "Installer", "Installer Pick Up", "Booked On", "Deducted On", "Location", "P. out", "P. Installed", "P. BS/GB", "P. Difference", "P. Revert", "P. Audit", "I. Out", "I. Installed", "I. BS/GB",
        "I. Difference", "I. Revert", "I. Audit", "Verify Notes", "SystemDetails", "AssignTo", "New Notes", "Assigned Employee" ,  "Employee Notes", "Employee All Notes" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);


    }

    protected void ddlSMSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView_SM.AllowPaging = false;
            BindGridSM(0);
        }
        else
        {
            GridView_SM.AllowPaging = true;
            GridView_SM.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGridSM(0);
        }
    }

    protected void lnkSMClearAll_Click1(object sender, EventArgs e)
    {
        txtSMProjectNo.Text = string.Empty;
        //txtSMstockitemfilter.Text = string.Empty;
        //txtSMSerialNo.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlSMDate.SelectedValue = "";
        txtSMStartDate.Text = string.Empty;
        txtSMendDate.Text = string.Empty;
        ddlIsDifference.SelectedValue = "0";
        ddlSMProjectWise.SelectedValue = "1";
        //lstProjectStatus.ClearSelection();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            //ddllocationsearch.Enabled = false;
        }

        ClearCheckBoxSM();
        BindGridSM(0);
    }

    public void ClearCheckBoxSM()
    {
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void lnkSMSearch_Click(object sender, EventArgs e)
    {
        BindGridSM(0);
    }

    #endregion

    //protected void ddlIsverify_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    //if (ddlIsverify.SelectedValue == "1")
    //    //{
    //    //    ddlYesNo.SelectedValue = "1";
    //    //    DivYesNo.Visible = true;
    //    //}
    //    //else if (ddlIsverify.SelectedValue == "2")
    //    //{
    //    //    ddlYesNo.SelectedValue = "1";
    //    //    DivYesNo.Visible = true;
    //    //}
    //    //else
    //    //{
    //    //    DivYesNo.Visible = false;
    //    //}
    //}

    protected void ddlIsverify1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIsverify1.SelectedValue == "1")
        {
            ddlWYesNo.SelectedValue = "1";
            DivWYesNo.Visible = true;
        }
        else
        {
            DivWYesNo.Visible = false;
        }
    }

    protected void lnkUpdatePanels_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtPanel.Text))
        {
            string PicklistID = hndPInstalledPicklist.Value;
            string ProjectNumber = hndPInstalledProjectNumber.Value;
            string OldInstalled = hndPInstalledOldInstalled.Value;
            string UserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            string Date = DateTime.Now.AddHours(14).ToString();

            int Succ = Reports.StockPending_Picklistlog_UpdateInstalled(txtPanel.Text, PicklistID, "1");

            int Succ2 = Reports.tbl_InstalledLog_Insert(ProjectNumber, PicklistID, UserID, Date, "1", OldInstalled, txtPanel.Text, txtPInstalledNotes.Text);
        }
        if (TabContainer1.ActiveTabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }
    }

    protected void lnkUpdateInverter_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtInverter.Text))
        {
            string PicklistID = hndIPickID.Value;
            string ProjectNumber = hndIProjectNumber.Value;
            string OldInstalled = hndIOldInstalled.Value;
            string UserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            string Date = DateTime.Now.AddHours(14).ToString();

            int Succ = Reports.StockPending_Picklistlog_UpdateInstalled(txtInverter.Text, PicklistID, "2");

            int Succ2 = Reports.tbl_InstalledLog_Insert(ProjectNumber, PicklistID, UserID, Date, "2", OldInstalled, txtInverter.Text, txtIInstalledNotes.Text);
        }

        if (TabContainer1.ActiveTabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }


    }

    protected void btnAddBSBG_Click(object sender, EventArgs e)
    {
        int success = 0;

        if (BS_GB_FileUploder.HasFile)
        {
            BS_GB_FileUploder.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\BSGB\\" + BS_GB_FileUploder.FileName);
            string connectionString = "";

            if (BS_GB_FileUploder.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\BSGB\\" + BS_GB_FileUploder.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

                //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\BSGB\\" + BS_GB_FileUploder.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }
            else if (BS_GB_FileUploder.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\BSGB\\" + BS_GB_FileUploder.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string ProjectNo = "";
                                string Panels = "";
                                string Inverter = "";

                                ProjectNo = dr["ProjectNo"].ToString();
                                Panels = dr["NumberofPanels"].ToString();
                                Inverter = dr["NumberofInverter"].ToString();

                                if (ProjectNo != null && ProjectNo != "")
                                {
                                    success = Reports.tbl_Picklistlog_UpdateBSGB(ProjectNo, Panels, Inverter);
                                }
                            }
                        }
                    }

                    //if (success2)
                    //{
                    //    Notification("Transaction Successful.");
                    //}
                    try
                    {
                        SiteConfiguration.deleteimage(BS_GB_FileUploder.FileName, "BSGB");
                    }
                    catch { }
                }
            }
        }
        BindGrid(0);
        // }
        //bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_GetStcDetails(ReferenceNo, PVDNumber, Status, Applied_date, Stc_Value_Excel);
        //BindGrid(0);
    }

    protected void btnUpdateSMData_Click(object sender, EventArgs e)
    {
        //int UpdateStatus = ClsDbData.USP_InsertUpdate_SM_tblProjectStatus();
        //int UpdateProNo = ClsDbData.USP_InsertUpdate_SM_tblProjectNos();

        int UpdateProject = ClsDbData.USP_InsertUpdate_tblProjects_SM();
    }

    protected void lbtnVerifyNotes_Click(object sender, EventArgs e)
    {
        string PickListID = VerifyPickListID.Value;

        if (PickListID != "")
        {
            int UpdateApprovedFlag = Reports.tbl_Picklistlog_Update_ApprovedFlagByPickID(PickListID, "1");
            int UpdateApprovedFlagNote = Reports.tbl_Picklistlog_Update_ApprovedFlagNoteByPickID(PickListID, txtVerifyNotes.Text);
        }

        if (TabContainer1.ActiveTabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }
    }

    protected void BindTotalSM(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int PanelStockDeducted = Convert.ToInt32(dt.Compute("SUM(PanelStockDeducted)", string.Empty));
            int SaleQtyPanel = Convert.ToInt32(dt.Compute("SUM(PanelInstalled)", string.Empty));
            int PanelDiff = (PanelStockDeducted - SaleQtyPanel);
            int PanelRevert = Convert.ToInt32(dt.Compute("SUM(PanelRevert)", string.Empty));
            int PAudit = (PanelDiff - PanelRevert);
            int InverterStockDeducted = Convert.ToInt32(dt.Compute("SUM(InverterStockDeducted)", string.Empty));
            int SaleQtyInverter = Convert.ToInt32(dt.Compute("SUM(InverterInstalled)", string.Empty));
            int InverterDiff = (InverterStockDeducted - SaleQtyInverter);
            int InverteRevert = Convert.ToInt32(dt.Compute("SUM(InverterRevert)", string.Empty));
            int IAudit = (InverterDiff - InverteRevert);

            lblSMpout.Text = PanelStockDeducted.ToString();
            lblSMPInstalled.Text = SaleQtyPanel.ToString();
            lblSMPDifference.Text = PanelDiff.ToString();
            lblSMPRevert.Text = PanelRevert.ToString();
            lblSMPAudit.Text = PAudit.ToString();
            lblSMIOut.Text = InverterStockDeducted.ToString();
            lblSMIInstalled.Text = SaleQtyInverter.ToString();
            lblSMIDifference.Text = InverterDiff.ToString();
            lblSMIRevert.Text = InverteRevert.ToString();
            lblSMIAudit.Text = IAudit.ToString();
        }
        else
        {
            lblSMpout.Text = "0";
            lblSMPInstalled.Text = "0";
            lblSMPDifference.Text = "0";
            lblSMPRevert.Text = "0";
            lblSMPAudit.Text = "0";
            lblSMIOut.Text = "0";
            lblSMIInstalled.Text = "0";
            lblSMIDifference.Text = "0";
            lblSMIRevert.Text = "0";
            lblSMIAudit.Text = "0";
        }
    }

    protected void GridView3_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void lbtnSaveNewNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string PickListID = hndNewNotesPicklistID.Value;
            string EnteredOn = DateTime.Now.AddHours(14).ToShortDateString();
            string EnteredBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            if (hndMode.Value != "Update")
            {

                if (PickListID != "")
                {
                    int ID = ClsReportsV2.tbl_ProjectsNotes_Insert(PickListID, "1", EnteredOn, EnteredBy);
                    if (ID != 0)
                    {
                        bool suc = ClsReportsV2.tbl_ProjectsNotes_Update(ID.ToString(), ddlAssignToNew.SelectedValue, txtNotesNew.Text);
                        bool sucEmployeeID = ClsReportsV2.tbl_ProjectsNotes_Update_EmployeeID(ID.ToString(), ddlEmployees.SelectedValue);

                        SetAdd1();
                        ddlAssignToNew.SelectedValue = "";
                        ddlEmployees.SelectedValue = "";
                        txtNotesNew.Text = "";

                        BindNewAriseNotes(PickListID, EnteredBy);
                        ModalPopupExtenderNewNote.Show();
                    }
                }
            }
            else
            {
                string ID = hndID.Value;
                if (ID != "")
                {
                    bool suc = ClsReportsV2.tbl_ProjectsNotes_Update(ID, ddlAssignToNew.SelectedValue, txtNotesNew.Text);
                    bool sucEmployeeID = ClsReportsV2.tbl_ProjectsNotes_Update_EmployeeID(ID, ddlEmployees.SelectedValue);

                    SetAdd1();

                    ddlAssignToNew.SelectedValue = "";
                    ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndMode.Value = "";

                    BindNewAriseNotes(PickListID, EnteredBy);
                    ModalPopupExtenderNewNote.Show();
                }
                //hndMode.Value = "";
            }
        }
        catch (Exception ex)
        {
            Notification("Inner Exception");
        }
    }

    public void BindNewAriseNotes(string PicklistID, string UserID)
    {
        DataTable dt = ClsReportsV2.tbl_ProjectsNotes_GetDataByID(PicklistID, "1", UserID);
        if (dt.Rows.Count > 0)
        {
            DivNewNotes.Visible = true;
            RptNewNotes.DataSource = dt;
            RptNewNotes.DataBind();
        }
        else
        {
            DivNewNotes.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNewNotes.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            DivNewNotes.Attributes.Add("style", "");
        }
    }

    protected void RptNewNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNewNote")
        {
            try
            {
                DataTable dt = ClsReportsV2.tbl_ProjectsNotes_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    ddlAssignToNew.SelectedValue = "";
                    ddlEmployees.SelectedValue = "";
                    txtNotesNew.Text = "";
                    hndID.Value = ID;
                    ddlAssignToNew.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                    ddlEmployees.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                    txtNotesNew.Text = dt.Rows[0]["Notes"].ToString();

                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNewNotes")
        {
            try
            {
                bool del = ClsReportsV2.tbl_ProjectsNotes_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindNewAriseNotes(hndNewNotesPicklistID.Value, userID);
                ModalPopupExtenderNewNote.Show();
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNote.Show();
    }

    public void BindNewWholesaleNotes(string ID, string UserID)
    {
        DataTable dt = ClsReportsV2.tbl_WholesaleNotes_GetDataByID(ID, UserID);
        if (dt.Rows.Count > 0)
        {
            DivNewNotesW.Visible = true;
            RptNewNotesW.DataSource = dt;
            RptNewNotesW.DataBind();
        }
        else
        {
            DivNewNotesW.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNewNotesW.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            DivNewNotesW.Attributes.Add("style", "");
        }
    }

    protected void btnSaveNewNotesW_Click(object sender, EventArgs e)
    {
        try
        {
            string WholesaleID = hndNewNotesWholesaleID.Value;
            string EnteredOn = DateTime.Now.AddHours(14).ToShortDateString();
            string EnteredBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            if (hndModeW.Value != "Update")
            {
                if (WholesaleID != "")
                {
                    int ID = ClsReportsV2.tbl_WholesaleNotes_Insert(WholesaleID, EnteredOn, EnteredBy);
                    if (ID != 0)
                    {
                        bool suc = ClsReportsV2.tbl_WholesaleNotes_Update(ID.ToString(), ddlAssignToNewW.SelectedValue, txtNotesNewW.Text);

                        SetAdd1();
                        ddlAssignToNewW.SelectedValue = "";
                        txtNotesNewW.Text = "";

                        BindNewWholesaleNotes(WholesaleID, EnteredBy);
                        ModalPopupExtenderNewNoteW.Show();
                    }
                    else
                    {
                        SetError1();
                        ModalPopupExtenderNewNoteW.Show();
                    }
                }
            }
            else
            {
                string ID = hndIDW.Value;
                if (ID != "")
                {
                    bool suc = ClsReportsV2.tbl_WholesaleNotes_Update(ID, ddlAssignToNewW.SelectedValue, txtNotesNewW.Text);

                    SetAdd1();

                    ddlAssignToNewW.SelectedValue = "";
                    txtNotesNewW.Text = "";
                    hndModeW.Value = "";

                    BindNewWholesaleNotes(WholesaleID, EnteredBy);
                    ModalPopupExtenderNewNoteW.Show();
                }
                //hndMode.Value = "";
            }
        }
        catch (Exception ex)
        {
            Notification("Inner Exception");
        }
    }

    protected void RptNewNotesW_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNewNote")
        {
            try
            {
                DataTable dt = ClsReportsV2.tbl_WholesaleNotes_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndModeW.Value = "Update";
                    ddlAssignToNewW.SelectedValue = "";
                    txtNotesNewW.Text = "";
                    hndIDW.Value = ID;
                    ddlAssignToNewW.SelectedValue = dt.Rows[0]["AssignTo"].ToString();
                    txtNotesNewW.Text = dt.Rows[0]["Notes"].ToString();

                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNewNotes")
        {
            try
            {
                bool del = ClsReportsV2.tbl_WholesaleNotes_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindNewWholesaleNotes(hndNewNotesWholesaleID.Value, userID);
                ModalPopupExtenderNewNoteW.Show();
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNoteW.Show();
    }

    protected void lnkSwipe_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        string Message1 = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        string[] ProjPickId;
        //string ProjectNumber = txtprojectno.Text;
        string ProjectNumber = "";
        string PickID = "";
        if (txtprojectno.Text != "")
        {
            ProjPickId = txtprojectno.Text.Split('/');
            //string ProjectNumber = txtprojectno.Text;
            ProjectNumber = ProjPickId[0].ToString();
            PickID = ProjPickId[1].ToString();
        }


        int i = 0;
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

            if (chkDifference.Checked == true)
            {
                //chkDifference.Checked = true;
                i++;
            }
            else
            {
                //chkDifference.Checked = false;
            }

        }
        if (i > Convert.ToInt32(hndDifference.Value))
        {
            ModalPopupExtenderRevert.Show();
            SetError();
        }
        else
        {
            string SerialNo = "";
            foreach (RepeaterItem item in Repeater1.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
                HiddenField rpthndProjectid = (HiddenField)item.FindControl("rpthndProjectid");
                HiddenField hdnPicklistid = (HiddenField)item.FindControl("rpthndPicklistId");

                if (chkDifference.Checked == true)
                {
                    if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                    {
                        DataTable dt = null;
                        //HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                        //HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");

                        if (SerialNo == "")
                        {
                            SerialNo = lblSerialNo.Text;
                        }
                        else
                        {
                            SerialNo += "," + lblSerialNo.Text;
                            //if (!string.IsNullOrEmpty(SerialNo))
                            //{
                            //    SerialNo = SerialNo.Substring(1).ToString();
                            //}
                        }


                        //if (!string.IsNullOrEmpty(ProjectNumber))
                        //{
                        //    dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
                        //    if (dt.Rows.Count > 0)
                        //    {
                        //        string projectid = dt.Rows[0]["ProjectID"].ToString();
                        //        //string PicklistId = dt.Rows[0]["ID"].ToString();
                        //        string PicklistId = PickID;
                        //        string Projectnumber = dt.Rows[0]["Projectnumber"].ToString();

                        //        //Update Swipe (+) in Project Number
                        //        bool Update1 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                        //        Message = "Stock Deduct For Project No: " + Projectnumber + " & PicklistId:" + PicklistId + " By administrator Swipe";

                        //        bool Update = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(PicklistId, Message, lblSerialNo.Text, "Picklist Out", hdnPicklistid.Value);

                        //        //Update Swipe (-) in Project Number
                        //        DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(PicklistId, "Picklist Out", SerialNo);
                        //        if (dt1.Rows.Count > 0)
                        //        {
                        //            bool Update2 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(rpthndProjectid.Value, hdnPicklistid.Value, dt1.Rows[0]["SerailNo"].ToString());

                        //            DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                        //            Message1 = "Stock Deduct For Project No: " + dt.Rows[0]["Projectnumber"].ToString() + " & PicklistId: " + hdnPicklistid.Value + " By administrator Swipe";

                        //            bool Update3 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(hdnPicklistid.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "Picklist Out", PicklistId);
                        //        }
                        //    }
                        //}
                        //if (!string.IsNullOrEmpty(TextBox1.Text))
                        //{
                        //    dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                        //    if (dt.Rows.Count > 0)
                        //    {
                        //        string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                        //        string InvoiceNo = dt.Rows[0]["InvoiceNo"].ToString();
                        //        ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);

                        //        //section = "Revert Item";
                        //        Message = "Stock Deduct For WholesaleOrderID: " + WholesaleOrderID + " Swipe";

                        //        bool s = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo(WholesaleOrderID, Message, lblSerialNo.Text, "PickList Out", hdnPicklistid.Value, "WholeSale Out");


                        //        //Update Swipe (-) in Wholesale Number
                        //        DataTable dt1 = Reports.tblMaintainHostory_GetSerialNo(WholesaleOrderID, "WholeSale Out", SerialNo);
                        //        if (dt1.Rows.Count > 0)
                        //        {
                        //            bool s1 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(rpthndProjectid.Value, hdnPicklistid.Value, dt1.Rows[0]["SerailNo"].ToString());

                        //            DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                        //            Message1 = "Stock Deduct For Project No: " + dtProjectNo.Rows[0]["Projectnumber"].ToString() + "& PicklistId: " + hdnPicklistid.Value + " By administrator Swipe";

                        //            //Message1 = "Stock Deduct For WholesaleOrderID: " + WholesaleOrderID + " Swipe";

                        //            bool s2 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgandModuleNameBySerialNo(hdnPicklistid.Value, Message1, dt1.Rows[0]["SerailNo"].ToString(), "WholeSale Out", WholesaleOrderID, "PickList Out");
                        //        }
                        //    }
                        //}
                    }
                }
            }
            BindGrid(0);
        }
    }

    public string[] GetJobDetailsByProjectNo(string JobNumber, string Username, string Password)
    {
        string[] SerialNoList = null;

        string SerialNo = GetJobDetails(JobNumber, Username, Password);

        SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

        return SerialNoList;
    }

    public string GetJobDetails(string JobNumber, string Username, string Password)
    {
        string SerialNo = "";
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?RefNumber=" + JobNumber);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    int index = jobData.lstJobData.Count;
                    //Job_Response.lstJobData lstJobData = jobData.lstJobData[index - 1]; // Root Object
                    Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                    Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

                    SerialNo = JobSystemDetails.SerialNumbers;
                }
            }

        }
        catch (Exception ex)
        {

        }

        return SerialNo;
    }

    public int GetNoOfPanels(string ProjectNo, string Username, string Password)
    {
        int NoOfPanel = 0;

        string SerialNo = GetJobDetails(ProjectNo, Username, Password);
        string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

        for (int i = 0; i < SerialNoList.Length; i++)
        {
            if (SerialNoList[i] != "")
            {
                NoOfPanel++;
            }
        }
        //NoOfPanel = SerialNoList.Length;

        return NoOfPanel;
    }

    protected void btnP_BSGB_Click(object sender, EventArgs e)
    {
        //DataTable dtProjectNo = ClsReportsV2.SP_GetGreenBotProjectNo_ByCompanyID("1");
        //if (dtProjectNo.Rows.Count > 0)
        //{
        //    int Suc = 0;
        //    for (int i = 0; i < dtProjectNo.Rows.Count; i++)
        //    {
        //        string ProjectNo = dtProjectNo.Rows[i]["Projectnumber"].ToString();

        //        if (!string.IsNullOrEmpty(ProjectNo))
        //        {
        //            int noOfPanels = GetNoOfPanels(ProjectNo, AriseUsername, ArisePassword);
        //            string NosP = "";
        //            string NosI = "";
        //            if (noOfPanels != 0)
        //            {
        //                NosP = noOfPanels.ToString();
        //                NosI = "1";
        //            }

        //            bool success = ClsReportsV2.tbl_Picklistlog_UpdateBSGB(ProjectNo, NosP, NosI);
        //            if (success)
        //            {
        //                Suc++;
        //            }
        //        }
        //    }
        //    Notification(Suc.ToString() + " Record Updated..");
        //}

        //int Suc1 = UpdateBridgeSelect();
        //Notification(Suc1.ToString() + " Record Updated..");

        //BindGrid(0);

        int Suc = ClsReportsV2.Bulk_InsertUpdate_tblPicklistlog_PBSGB();
        Notification(Suc.ToString() + " Record Updated..");

    }

    protected void btnP_BSGB_SM_Click(object sender, EventArgs e)
    {
        DataTable dtProjectNo = ClsReportsV2.SP_GetGreenBotProjectNo_ByCompanyID("2");

        if (dtProjectNo.Rows.Count > 0)
        {
            int Suc = 0;
            for (int i = 0; i < dtProjectNo.Rows.Count; i++)
            {
                string ProjectNo = dtProjectNo.Rows[i]["Projectnumber"].ToString();

                if (!string.IsNullOrEmpty(ProjectNo))
                {
                    int noOfPanels = GetNoOfPanels(ProjectNo, SMUsername, SMPassword);

                    string NosP = "";
                    string NosI = "";
                    if (noOfPanels != 0)
                    {
                        NosP = noOfPanels.ToString();
                        NosI = "1";
                    }

                    bool success = ClsReportsV2.tbl_Picklistlog_UpdateBSGB(ProjectNo, NosP, NosI);
                    if (success)
                    {
                        Suc++;
                    }
                }
            }

            //int noOfPanels = GetNoOfPanels("213156", SMUsername, SMPassword);

            //bool success = ClsReportsV2.tbl_Picklistlog_UpdateBSGB("213156", noOfPanels.ToString(), "1");
            //if (success)
            //{
            //    Suc++;
            //}

            //BindGridSM(0);
            //Notification(Suc.ToString() + " Record Updated..");
        }


    }

    protected void btnP_BSGB_WO_Click(object sender, EventArgs e)
    {
        DataTable dtReferenceNo = ClsReportsV2.SP_GetGreenBotProjectNo_ByCompanyID("3");

        if (dtReferenceNo.Rows.Count > 0)
        {
            int Suc = 0;
            for (int i = 0; i < dtReferenceNo.Rows.Count; i++)
            {
                string ReferenceNo = dtReferenceNo.Rows[i]["ReferenceNo"].ToString();
                string WholesaleOrderID = dtReferenceNo.Rows[i]["WholesaleOrderID"].ToString();

                if (!string.IsNullOrEmpty(ReferenceNo))
                {
                    int noOfPanels = GetNoOfPanels(ReferenceNo, WOUsername, WOPassword);

                    string NosP = "";
                    if (noOfPanels != 0)
                    {
                        NosP = noOfPanels.ToString();
                    }

                    bool success = ClsReportsV2.tbl_WholesaleOrders_UpdateNumberofPanels(WholesaleOrderID, NosP);
                    if (success)
                    {
                        Suc++;
                    }
                }
            }

            //int noOfPanels = GetNoOfPanels("213156", SMUsername, SMPassword);

            //bool success = ClsReportsV2.tbl_Picklistlog_UpdateBSGB("213156", noOfPanels.ToString(), "1");
            //if (success)
            //{
            //    Suc++;
            //}

            //BindGridSM(0);
            //Notification(Suc.ToString() + " Record Updated..");
        }
    }

    #region
    public int UpdateBridgeSelect()
    {
        int Suc = 0;

        DataTable dtBSProjectNo = ClsReportsV2.SP_GetBridgeSelectProjectNo_ByCompanyID("1"); // 1- Arise, else Wholesale
        if (dtBSProjectNo.Rows.Count > 0)
        {
            for (int i = 0; i < dtBSProjectNo.Rows.Count; i++)
            //for (int i = 0; i < 1; i++)
            {
                string ProjectNo = dtBSProjectNo.Rows[i]["Projectnumber"].ToString();
                //string ProjectNo = "803457";

                if (!string.IsNullOrEmpty(ProjectNo))
                {
                    Job_Response.NoOfPanelInverter noOfPanelInverter = GetNoOfPanels_BS(ProjectNo);
                    string NosP = noOfPanelInverter.NoOfPanel;
                    string NosI = noOfPanelInverter.NoOfInverter;

                    if (NosP == "0")
                    {
                        NosP = "";
                    }
                    if (NosI == "0")
                    {
                        NosI = "";
                    }

                    bool success = ClsReportsV2.tbl_Picklistlog_UpdateBSGB(ProjectNo, NosP, NosI);
                    if (success)
                    {
                        Suc++;
                    }
                }
            }

        }
        return Suc;
    }

    public Job_Response.NoOfPanelInverter GetNoOfPanels_BS(string ProjectNo)
    {
        Job_Response.NoOfPanelInverter noOfPanelInverter = new Job_Response.NoOfPanelInverter();
        try
        {
            Details jobDetails = GetJobDetailsFromBS(ProjectNo);
            //int NoOfPanel = jobDetails.Panels != null ? jobDetails.Panels.Count : 0;
            //int NoOfInverter = jobDetails.Inverters != null ? jobDetails.Inverters.Count : 0;

            //noOfPanelInverter.NoOfPanel = NoOfPanel.ToString();
            //noOfPanelInverter.NoOfInverter = NoOfInverter.ToString();
        }
        catch
        {

        }
        return noOfPanelInverter;
    }

    public Details GetJobDetailsFromBS(string JobNumber)
    {
        Details ReturnDetails = new Details();
        try
        {
            var obj = new ClsBridgeSelect
            {
                //crmid = "872136"
                crmid = JobNumber
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/products";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<RootObjectBS> JsonData = client.Execute<RootObjectBS>(request);
            if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObjectBS jobData = JsonConvert.DeserializeObject<RootObjectBS>(JsonData.Content);

                ReturnDetails = jobData.Success.Details;
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return ReturnDetails;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }
    #endregion

    #region New Reverted Panel/Inverter Swipe
    public void ResetTextBox()
    {
        txtProjectNumberRevert.Text = string.Empty;
        txtInvoiceNumberRevert.Text = string.Empty;
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in RptRevertNew.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

            if (chkSelectAll.Checked == true)
            {
                chkDifference.Checked = true;
            }
            else
            {
                chkDifference.Checked = false;
            }
        }
        ModalPopupExtenderRevertNew.Show();
    }
    #endregion

    protected void txtProjectNumberRevert_TextChanged(object sender, EventArgs e)
    {
        try
        {
            int fail = 0;
            int suc = 0;
            int fail1 = 0;
            int SelectSerialNo = 0;

            string[] ProjPickId;
            string ProjectNumber = "";
            string PickID = "";

            if (txtProjectNumberRevert.Text != "")
            {
                ProjPickId = txtProjectNumberRevert.Text.Split('/');
                ProjectNumber = ProjPickId[0].ToString();
                PickID = ProjPickId[1].ToString();
            }
            else
            {
                txtProjectNumberRevert.Text = string.Empty;
                txtProjectNumberRevert.Focus();
                Notification("Please Select Picklist");
                ModalPopupExtenderRevertNew.Show();
                return;
            }

            DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
            string locationid = "";
            if (dt.Rows.Count > 0)
            {
                locationid = dt.Rows[0]["LocationId"].ToString();
            }
            if (string.IsNullOrEmpty(locationid))
            {
                fail1++;
            }

            foreach (RepeaterItem item in RptRevertNew.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
                HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
                HiddenField hndCategoryID = (HiddenField)item.FindControl("hndCategoryID");

                string StockItemID = hnditemid.Value;
                string Categorynm = hndCategoryID.Value;

                if (chkDifference.Checked == true)
                {
                    int exist = ClstblProjects.Exist_ItemId_tbl_PicklistItemDetail(hnditemid.Value, Categorynm, PickID);
                    if (exist == 0)
                    {
                        suc++;
                    }
                    else
                    {
                        fail++;
                    }
                }
                else
                {
                    SelectSerialNo++;
                }
            }

            if (fail > 0)
            {
                txtProjectNumberRevert.Text = string.Empty;
                txtProjectNumberRevert.Focus();
                //Notification("Record with this model number already exists.");

            }
            if (fail1 > 0)
            {
                txtProjectNumberRevert.Text = string.Empty;
                txtProjectNumberRevert.Focus();
                Notification("Record with this number location is null.");

            }
            if (!string.IsNullOrEmpty(txtProjectNumberRevert.Text))
            {
                txtInvoiceNumberRevert.Text = string.Empty;
                txtProjectNumberRevert.Focus();
            }

            if (SelectSerialNo == RptRevertNew.Items.Count)
            {
                txtProjectNumberRevert.Text = string.Empty;
                txtProjectNumberRevert.Focus();
                Notification("Select Serial Number");
            }
        }
        catch (Exception ex)
        {
            Notification("Error...");
        }

        ModalPopupExtenderRevertNew.Show();

    }

    protected void lnkSwipeReverted_Click(object sender, EventArgs e)
    {
        string[] ProjPickId;
        string ToProjectNumber = "";
        string ToPickID = "";

        if (txtProjectNumberRevert.Text != "")
        {
            ProjPickId = txtProjectNumberRevert.Text.Split('/');
            ToProjectNumber = ProjPickId[0].ToString();
            ToPickID = ProjPickId[1].ToString();
        }
        else
        {
            txtProjectNumberRevert.Text = string.Empty;
            txtProjectNumberRevert.Focus();
            Notification("Please Select Picklist");
            ModalPopupExtenderRevertNew.Show();
            return;
        }

        int i = 0;
        foreach (RepeaterItem item in RptRevertNew.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

            if (chkDifference.Checked == false)
            {
                i++;
            }
        }
        if (i == RptRevertNew.Items.Count)
        {
            ModalPopupExtenderRevert.Show();
            Notification("Select Picklist..");
            return;
        }

        try
        {
            
            string SerialNo = "";
            foreach (RepeaterItem item in RptRevertNew.Items)
            {
                CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
                Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
                HiddenField rpthndProjectNumber = (HiddenField)item.FindControl("rpthndProjectNumber");
                HiddenField rpthndPicklistIdReverted = (HiddenField)item.FindControl("rpthndPicklistIdReverted");

                DataTable dtTo = new DataTable();
                string FromPickListID = rpthndPicklistIdReverted.Value;
                string FromProjectNumber = rpthndProjectNumber.Value;

                if (chkDifference.Checked == true)
                {
                    if (SerialNo == "")
                    {
                        SerialNo = lblSerialNo.Text;
                    }
                    else
                    {
                        SerialNo += "," + lblSerialNo.Text;
                    }

                    if (!string.IsNullOrEmpty(txtProjectNumberRevert.Text))
                    {
                        dtTo = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ToProjectNumber);
                        if (dtTo.Rows.Count > 0)
                        {
                            string ToProjectID = dtTo.Rows[0]["ProjectID"].ToString();

                            //Update Swipe(+) in Project Number
                            bool Update1 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(ToProjectID, ToPickID, lblSerialNo.Text);

                            string Message = "Stock Deduct For Project No: " + ToProjectNumber + " & PicklistId:" + ToPickID + " By administrator Swipe";

                            bool Update = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(ToPickID, Message, lblSerialNo.Text, "PickList Out", FromPickListID);

                            Message = "Stock Revert For Project No: " + ToProjectNumber + " & PicklistId:" + ToPickID + " By administrator Swipe";

                            bool Update2 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(ToPickID, Message, lblSerialNo.Text, "PickList Revert", FromPickListID);

                            bool Update3 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(ToPickID, "Broken Item", lblSerialNo.Text, "Broken Item", FromPickListID);

                            bool Update6 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(ToPickID, "Defected Item", lblSerialNo.Text, "Defected Item", FromPickListID);

                            //Update Swipe (-) in Project Number
                            DataTable dt1 = ClsReportsV2.tblMaintainHostory_GetSerialNo(ToPickID, "Picklist Out", SerialNo, hndCategoryID.Value);
                            if (dt1.Rows.Count > 0)
                            {
                                string UpdateSerialNo = dt1.Rows[0]["SerailNo"].ToString();

                                bool Update4 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(FromProjectNumber, FromPickListID, UpdateSerialNo);

                                //DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                                //Update Revert Log
                                //string Message1 = "Stock Revert For Project No: " + FromProjectNumber + " & PicklistId: " + FromPickListID + " By administrator Swipe";

                                //bool Update3 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(FromPickListID, Message1, UpdateSerialNo, "Picklist Revert", ToPickID);

                                //Update Deduct Log
                                string Message2 = "Stock Deduct For Project No: " + FromProjectNumber + " & PicklistId: " + FromPickListID + " By administrator Swipe";

                                bool Update5 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(FromPickListID, Message2, UpdateSerialNo, "Picklist Out", ToPickID);
                            }
                            else
                            {
                                //DataTable dt = ClsReportsV2.tblMaintainHostory_GetSerialNo(ToPickID, "Picklist Out", SerialNo, hndCategoryID.Value);

                                //if (dt.Rows.Count > 0)
                                //{
                                //    bool Update4 = ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(FromProjectNumber, FromPickListID, dt.Rows[0]["SerailNo"].ToString());

                                //    //DataTable dtProjectNo = ClstblProjects.tblPicklistlog_GetProjectNumberByProjectID(rpthndProjectid.Value);

                                //    string Message1 = "Stock Deduct For Project No: " + FromProjectNumber + " & PicklistId: " + FromPickListID + " By administrator Swipe";

                                //    bool Update3 = ClstblStockSerialNo.Update_tblMaintainHistory_SectionIdandMsgBySerialNo(FromPickListID, Message1, dt.Rows[0]["SerailNo"].ToString(), "Picklist Out", ToPickID);
                                //}
                            }
                        }

                        if (!string.IsNullOrEmpty(txtInvoiceNumberRevert.Text))
                        {

                        }
                    }
                }
            }

            SetAdd1();
        }
        catch(Exception ex)
        {
            SetError1();
        }
        if(TabContainer1.ActiveTabIndex == 0)
        {
            BindGrid(0);
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            BindGridSM(0);
        }

        hndCategoryID.Value = "";
    }
}