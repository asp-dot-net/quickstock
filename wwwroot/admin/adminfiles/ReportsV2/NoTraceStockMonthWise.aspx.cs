﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_NoTraceStockMonthWise : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //ddlCompany.SelectedValue = "2";
            BindCheckboxProjectTab();
            BindDropDown();

            PanGrid.Visible = false;
            divtot.Visible = false;

            BindFetchDate();

            if (Roles.IsUserInRole("PreInstaller"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                ddlEmployees.SelectedValue = stEmp.EmployeeID;
                //ddlEmployees.Enabled = false;
            }

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2022";
            //txtEndDate.Text = "31/01/2022";
            //////ddlInstaller.SelectedValue = "428700"; //Customer ID // Arise-SolarMiner
            //BindGrid(0);

            //ddlDateType.SelectedValue = "1";
            //txtStartDate.Text = "01/01/2020";
            //txtEndDate.Text = "30/06/2021";
            //ddlInstaller.SelectedValue = "531124"; //Customer ID // Arise-SolarMiner
            //BindGrid(0);
        }
    }

    protected DataTable GetGridData1()
    {
        string Installer = "";
        if (!string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            Installer += " and PL_Main.InstallerID = " + ddlInstaller.SelectedValue;
        }
        string LocationMain = "";
        string Location = "";
        if (!string.IsNullOrEmpty(ddlLocation.SelectedValue))
        {
            LocationMain += " and PL_Main.LocationId = " + ddlLocation.SelectedValue;
            Location += " and PL.LocationId = " + ddlLocation.SelectedValue;
        }

        DateTime StartDate = Convert.ToDateTime(txtStartDate.Text);
        DateTime EndDate = Convert.ToDateTime(txtEndDate.Text);
        string DateFilterMain = "";
        if (!string.IsNullOrEmpty(StartDate.Date.ToString()) || !string.IsNullOrEmpty(EndDate.Date.ToString()))
        {
            DateFilterMain += " ('" + StartDate.ToString("yyyy/MM/dd") + "' <= PL_Main.DeductOn or '" + StartDate.ToString("yyyy/MM/dd") + "'='')  and ('" + EndDate.ToString("yyyy/MM/dd") + "' >= PL_Main.DeductOn  or '" + EndDate.ToString("yyyy/MM/dd") + "'='') ";
        }

        string Month = "";
        string PendingAudit = "";
        string Total = "";
        for (var i = StartDate; i < EndDate; i = i.AddMonths(1))
        {
            var sD = new DateTime(i.Year, i.Month, 1);
            var eD = new DateTime(i.Year, i.Month, DateTime.DaysInMonth(i.Year, i.Month));
            var DateFilter = " ('" + sD.ToString("yyyy/MM/dd") + "' <= PL.DeductOn or '" + sD.ToString("yyyy/MM/dd") + "'='')  and ('" + eD.ToString("yyyy/MM/dd") + "' >= PL.DeductOn  or '" + eD.ToString("yyyy/MM/dd") + "'='') ";

            var DateFilterEx = " ('" + sD.ToString("yyyy/MM/dd") + "' <= PL_Main.DeductOn or '" + sD.ToString("yyyy/MM/dd") + "'='')  and ('" + eD.ToString("yyyy/MM/dd") + "' >= PL_Main.DeductOn  or '" + eD.ToString("yyyy/MM/dd") + "'='') ";

            Month += ", Count(distinct(case When" + DateFilterEx + "then SSN.SerialNo end)) as NetOut" + i.Month + i.Year + ", Count(DIstinct(Case When SSN.BSGBFlag IN(1, 4, 5) and" + DateFilterEx + "then SSN.SerialNo end)) as AriseInstall" + i.Month + i.Year + ", Count(DIstinct(Case When SSN.BSGBFlag = 2 and" + DateFilterEx + "then SSN.SerialNo end)) as SMInstall" + i.Month + i.Year + ", (Select Count(distinct SSN.SerialNo) From tbl_PickListLog PL Join tblStockSerialNo SSN on PL.ID = SSN.PicklistId Join tblStockItems SI on SSN.StockItemID = SI.StockItemID and SSN.IsActive = 1 and SI.StockCategoryID = 1 and PL.InstallerID = PL_Main.InstallerID and PL.Companyid = " + ddlCompany.SelectedValue + " and(case when PL.Companyid = 1 then (Select ProjectStatusID from arisesolar.dbo.tblProjects Where ProjectID = PL.ProjectID) when PL.Companyid = 2 then(Select ProjectStatusID from crmsolarminardb.dbo.tblProjects Where ProjectID = PL.ProjectID) end) Not In(3, 4, 6, 8) and (Case when Exists(select ProjectNo from arisesolar.dbo.tblProjectNoYN where ProjectNo = PL.ProjectNumber) then 'Yes' else 'No' End) = 'No' and(Case when Exists(select SerialNo from tblSerialNoFromBSGBNew where SerialNo = SSN.SerialNo) then 'Yes' else 'No' End) = 'No' " + Location + " and" + DateFilter + " and(case when PL.Companyid = 1 then (Select Convert(date, InstallBookingDate) from arisesolar.dbo.tblProjects Where ProjectID = PL.ProjectID) when PL.Companyid = 2 then (Select Convert(date, InstallBookingDate) from crmsolarminardb.dbo.tblProjects Where ProjectID = PL.ProjectID) end) < Convert(date, DATEADD(HOUR, 15, GETDATE())) ) as ScanRequired" + i.Month + i.Year + ", (select Count(distinct SSN.SerialNo) From tbl_PickListLog PL Join tblStockSerialNo SSN on PL.ID = SSN.PicklistId Join tblStockItems SI1 on SSN.StockItemID = SI1.StockItemID left join tbl_MissingSerialNo MS on SSN.SerialNo = MS.SerialNo Where PL.InstallerID is not null and SI1.StockCategoryID = 1 and SSN.IsActive = 1 and SSN.CreditFlag is not null and ISNULL(MS.CreditFlag, 0) != 1 and PL.Companyid = " + ddlCompany.SelectedValue + " and PL.InstallerID = PL_Main.InstallerID " + Location + " and" + DateFilter + ") as ActualCredit" + i.Month + i.Year;

            string installed = "";
            if (ddlCompany.SelectedValue == "1" || ddlCompany.SelectedValue == "4")
            {
                installed = "AriseInstall" + i.Month + i.Year;
            }
            else if (ddlCompany.SelectedValue == "2")
            {
                installed = "SMInstall" + i.Month + i.Year;
            }
            else
            {
                installed = "0";
            }
            //PendingAudit += ", (NetOut" + i.Month + i.Year + "-" + installed + "- ScanRequired" + i.Month + i.Year + "- ActualCredit" + i.Month + i.Year + ") as PA" + i.Month + i.Year;
            PendingAudit += ", (NetOut" + i.Month + i.Year + "-" + installed + "- ScanRequired" + i.Month + i.Year + "- ActualCredit" + i.Month + i.Year + ") as " + getFullMonthName(i.Month);
            Total += "(NetOut" + i.Month + i.Year + "-" + installed + "- ScanRequired" + i.Month + i.Year + "- ActualCredit" + i.Month + i.Year + ") +";
        }


        //if (!string.IsNullOrEmpty(txtStartDate.Text))
        //{
        //    Month += ", Count(distinct SSN.SerialNo) as NetOut, Count(DIstinct(Case When SSN.BSGBFlag IN(1, 4, 5) then SSN.SerialNo end)) as AriseInstall, Count(DIstinct(Case When SSN.BSGBFlag = 2 then SSN.SerialNo end)) as SMInstall, (Select Count(distinct SSN.SerialNo) From tbl_PickListLog PL Join tblStockSerialNo SSN on PL.ID = SSN.PicklistId Join tblStockItems SI on SSN.StockItemID = SI.StockItemID and SSN.IsActive = 1 and SI.StockCategoryID = 1 and PL.InstallerID = PL_Main.InstallerID and PL.Companyid = " + ddlCompany.SelectedValue + " and(case when PL.Companyid = 1 then (Select ProjectStatusID from arisesolar.dbo.tblProjects Where ProjectID = PL.ProjectID) when PL.Companyid = 2 then(Select ProjectStatusID from crmsolarminardb.dbo.tblProjects Where ProjectID = PL.ProjectID) end) Not In(3, 4, 6, 8) and (Case when Exists(select ProjectNo from arisesolar.dbo.tblProjectNoYN where ProjectNo = PL.ProjectNumber) then 'Yes' else 'No' End) = 'No' and(Case when Exists(select SerialNo from tblSerialNoFromBSGBNew where SerialNo = SSN.SerialNo) then 'Yes' else 'No' End) = 'No' " +Location + DateFilter+ " and(case when PL.Companyid = 1 then (Select Convert(date, InstallBookingDate) from arisesolar.dbo.tblProjects Where ProjectID = PL.ProjectID) when PL.Companyid = 2 then (Select Convert(date, InstallBookingDate) from crmsolarminardb.dbo.tblProjects Where ProjectID = PL.ProjectID) end) < Convert(date, DATEADD(HOUR, 15, GETDATE())) ) as ScanRequired, (select Count(distinct SSN.SerialNo) From tbl_PickListLog PL Join tblStockSerialNo SSN on PL.ID = SSN.PicklistId Join tblStockItems SI1 on SSN.StockItemID = SI1.StockItemID left join tbl_MissingSerialNo MS on SSN.SerialNo = MS.SerialNo Where PL.InstallerID is not null and SI1.StockCategoryID = 1 and SSN.IsActive = 1 and SSN.CreditFlag is not null and ISNULL(MS.CreditFlag, 0) != 1 and PL.Companyid = " + ddlCompany.SelectedValue + " and PL.InstallerID = PL_Main.InstallerID " + Location + DateFilter + ") as ActualCredit";
        //}

        string Extra = "";
        if (ddlPendingAudiYN.SelectedValue == "1")
        {
            Extra += " Where Total = 0";
        }
        else if (ddlPendingAudiYN.SelectedValue == "2")
        {
            Extra += " Where Total != 0";
        }

        string query = "Select * from(Select InstallerName " + PendingAudit + ", (" + Total + "0) Total from (Select InstallerID, InstallerName" + Month +
            " From tbl_PickListLog PL_Main Join tblStockSerialNo SSN on PL_Main.ID = SSN.PicklistId Join tblStockItems SI on SSN.StockItemID = SI.StockItemID" +
            " Where SSN.IsActive = 1 and SI.StockCategoryID = 1 and InstallerID is not null and PL_Main.Companyid = " + ddlCompany.SelectedValue + Installer + LocationMain + " and" + DateFilterMain + " Group By InstallerID, InstallerName) as tbl) as tbl" + Extra;

        //string query = "Select "
        //DataTable dt = ClsReportsV2.noTraceStockMonthWise(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue);

        DataTable dt = ClstblCustomers.query_execute(query);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            DataTable dtTotal = dt.Clone();
            dtTotal.Columns[0].ColumnName = "Month";
            DataRow dr = dtTotal.NewRow();
            for (int i = 1; i < dt.Columns.Count; i++)
            {
                dr[i] = Convert.ToInt32(dt.Compute("SUM(" + dt.Columns[i] + ")", string.Empty));
            }
            dtTotal.Rows.Add(dr);

            gvTotal.DataSource = dtTotal;
            gvTotal.DataBind();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
        {
            BindGrid(0);
        }
        else
        {
            Notification("Please Select Date.");
        }

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlCompany.SelectedValue = "1";
        ddlInstaller.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlPendingAudiYN.SelectedValue = "2";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    Dictionary<string, int> _columnIndiciesForAbcGridView = null;
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        string FileName = "No Trace Stock Month Wise_" + txtStartDate.Text + "_to_" + txtEndDate.Text + "_on_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";
        expertExcelV2(dt, FileName, "Month Wise");

        //Export oExport = new Export();
        //string[] columnNames = dt.Columns.Cast<DataColumn>()
        //                         .Select(x => x.ColumnName)
        //                         .ToArray();

        //string FileName = "AllNoTraceReport_Deduct_" + txtStartDate.Text + "_to_" + txtEndDate.Text + "_on_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        //int[] ColList = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
        //                12, 13, 14 };

        //string[] arrHeader = { "InstallerID", "Installer Name", "Net Out", "Arise BSGB", "SM BSGB", "WO BSGB", "Scan Required", "Other Inventory", "Other Wholesale", "Credit", "Non Resolve"
        //        , "With Installer", "Future Installation", "Pending Audit", "Manager Name" };

        //try
        //{
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception ex)
        //{
        //    Notification(ex.Message);
        //}

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        DataTable dtEmployee = ClstblEmployees.tblEmployees_SelectAll();
        ddlEmployees.DataSource = dtEmployee;
        ddlEmployees.DataTextField = "fullname";
        ddlEmployees.DataValueField = "EmployeeID";
        ddlEmployees.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("1");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

        }
        else if (ddlCompany.SelectedValue == "2")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("2");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "CompanyName";
            ddlInstaller.DataValueField = "UserId";
            ddlInstaller.DataBind();
        }

    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }

    protected void BindFetchDate()
    {
        string LastUpdatedOn = ClsDbData.tbl_APIFetchData_UtilitiesLastUpdateDate("Greenbot");
        lblUpdatedMsg.Text = "Fetched till " + LastUpdatedOn;
    }

    protected void expertExcelV2(DataTable dt, string fileName, string sheetName)
    {
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, sheetName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + fileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    //protected void TotalExportToExcel(object sender, EventArgs e)
    //{
    //    LinkButton btn = (LinkButton)sender as LinkButton;
    //    string sheetName = btn.CommandArgument.ToString();

    //    DataTable dt = getDataTotalExcel(sheetName);

    //    string FileName = "All " + sheetName + " Serial No_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

    //    expertExcelV2(dt, FileName, sheetName);
    //}

    protected string getFullMonthName(int month)
    {
        DateTime date = new DateTime(2020, month, 1);

        return date.ToString("MMMM");
    }

}