﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_NoTraceStockInverter : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlCompany.SelectedValue = "1";
            BindCheckboxProjectTab();
            BindDropDown();

            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;

            //ddlInstaller.SelectedValue = "57";

            //BindGrid(0);

            //DataTable dt = GetGridData1();
        }
    }

    protected DataTable GetGridData1()
    {
        DataTable dt = NoTraceStock.NoTraceStockInverter(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue, ddlScanReq.SelectedValue);
        
        //DataTable dt = NoTraceStock.NoTraceStockInverter(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlEmployees.SelectedValue, ddlPendingAudiYN.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            divtot.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int NetOut = Convert.ToInt32(dt.Compute("SUM(NetOut)", string.Empty));
            int BSGB = Convert.ToInt32(dt.Compute("SUM(SBBSGB)", string.Empty));
            int AriseBSGB = Convert.ToInt32(dt.Compute("SUM(AriseBSGB)", string.Empty));
            int SMBSGB = Convert.ToInt32(dt.Compute("SUM(SMBSGB)", string.Empty));
            int WOBSGB = Convert.ToInt32(dt.Compute("SUM(WOBSGB)", string.Empty));
            int ScanRequired = Convert.ToInt32(dt.Compute("SUM(ScanRequired)", string.Empty));
            int OtherInventory = Convert.ToInt32(dt.Compute("SUM(OtherInventory)", string.Empty));
            int OtherWholesale = Convert.ToInt32(dt.Compute("SUM(OtherWholesale)", string.Empty));
            int Credit = Convert.ToInt32(dt.Compute("SUM(Credit)", string.Empty));
            int NonResolved = Convert.ToInt32(dt.Compute("SUM(NonResolved)", string.Empty));
            int FutureInstallation = Convert.ToInt32(dt.Compute("SUM(FutureInstallation)", string.Empty));
            int PendingAudit = Convert.ToInt32(dt.Compute("SUM(PendingAudit)", string.Empty));
            int ActualAudit = Convert.ToInt32(dt.Compute("SUM(ActualAudit)", string.Empty));

            HypNetOut.Text = NetOut.ToString();
            HypBSGB.Text = BSGB.ToString();
            HypAriseBSGB.Text = AriseBSGB.ToString();
            HypSMBSGB.Text = SMBSGB.ToString();
            HypWOBSGB.Text = WOBSGB.ToString();
            HypScanRequired.Text = ScanRequired.ToString();
            HypOtherInventory.Text = OtherInventory.ToString();
            HypOtherWholesale.Text = OtherWholesale.ToString();
            HypCredit.Text = Credit.ToString();
            HypNonResolve.Text = NonResolved.ToString();
            HypFutureInstallation.Text = FutureInstallation.ToString();
            HypPendingAudit.Text = PendingAudit.ToString();
            HypActualAudit.Text = ActualAudit.ToString();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlPendingAudiYN.SelectedValue = "2";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
            //string ContactID = rowView["ContactID"].ToString();
            //string Companyid = ddlCompany.SelectedValue;

            //string ProjectStatus = GetProjectStatus();

            //HyperLink StockDeducted = (HyperLink)e.Row.FindControl("lblStockDeducted");
            //HyperLink Revert = (HyperLink)e.Row.FindControl("lblRevert");
            //HyperLink Installed = (HyperLink)e.Row.FindControl("lblSaleQty");

            //StockDeducted.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Contact + "&ContactID=" + ContactID + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text + "&PS=" + ProjectStatus + "&Category=" + ddlCategory.SelectedValue;
            //StockDeducted.Target = "_blank";

            //Installed.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Installed" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Installed.Target = "_blank";

            //Revert.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/StockPendingItemWiseDetails.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&CompanyLocation=" + CompanyLocation + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Revert" + "&CompanyID=" + Companyid + "&ProjectNo=" + txtProjectNumber.Text + "&InstallerID=" + ddlInstaller.SelectedValue + "&PS=" + ProjectStatus;
            //Revert.Target = "_blank";

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        Export oExport = new Export();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        string FileName = "AllNoTraceReport-Inverter_Deduct_" + txtStartDate.Text + "_to_" + txtEndDate.Text + "_on_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

        string[] arrHeader = { "InstallerID", "Installer Name", "Net Out", "SB BSGB",  "Arise BSGB", "SM BSGB", "WO BSGB", "Scan Required", "Other Inventory", "Other Wholesale", "Credit", "Non Resolve" , "Future Installation", "Pending Audit", "Manager Name" };

        try
        {
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindCheckboxProjectTab()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        DataTable dtEmployee = ClstblEmployees.tblEmployees_SelectAll();
        ddlEmployees.DataSource = dtEmployee;
        ddlEmployees.DataTextField = "fullname";
        ddlEmployees.DataValueField = "EmployeeID";
        ddlEmployees.DataBind();
    }

    public void BindDropDown()
    {
        if (ddlCompany.SelectedValue == "1")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("1");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

        }
        else if (ddlCompany.SelectedValue == "2")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("2");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }
        else if (ddlCompany.SelectedValue == "4")
        {
            ddlInstaller.Items.Clear();
            ListItem DefaultItem1 = new ListItem { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(DefaultItem1);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverterByCompanyID("4");
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "CompanyName";
            ddlInstaller.DataValueField = "UserId";
            ddlInstaller.DataBind();
        }

    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDropDown();
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (sender as LinkButton);

        string[] arg = lbtn.CommandArgument.Split(';');
        string InstallerID = arg[0];
        string InstallerName = arg[1];

        DataTable dtProjectWise = GetDataProjectWise(InstallerID);

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtProjectWise, "Project Wise");
                //wb.Worksheets.Add(dtProjectWise, "Pending Audit");
                //wb.Worksheets.Add(dtSerialNo, "UnTrace Panel Serial No");

                string FileName = InstallerName + "_No Trace Stock Inverter_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected DataTable GetDataProjectWise(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsReportsV2.NoTraceStockInverter_ProjectWise(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected void lbtnExportV3_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (sender as LinkButton);

        string[] arg = lbtn.CommandArgument.Split(';');
        string InstallerID = arg[0];
        string InstallerName = arg[1];

        DataTable dtStockItem = GetdtStockItem(InstallerID);

        DataTable dtProject = GetdtProject(InstallerID);
        
        AddDataInDtproject(dtProject);

        DataTable dtSerialNo = getDataSerialNoWise(InstallerID);

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtStockItem, "Stock Item Detail");
                wb.Worksheets.Add(dtProject, "Pending Audit");
                wb.Worksheets.Add(dtSerialNo, "UnTrace Panel Serial No");

                string FileName = InstallerName + "_No Trace Stock Inverter_From_" + txtStartDate.Text + "_To_" + txtEndDate.Text + "_On_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    protected DataTable GetdtStockItem(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = NoTraceStock.NoTraceStockInverter_StockItem(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected DataTable GetdtProject(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = NoTraceStock.NoTraceStockInverter_Project(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected void AddDataInDtproject(DataTable dtProjectWise)
    {
        try
        {
            // Add New
            for (int i = 0; i < dtProjectWise.Rows.Count; i++)
            {
                string ProjectNo = dtProjectWise.Rows[i]["Projectnumber"].ToString();

                DataTable dtProject = NoTraceStock.SP_TrackSerialNo_Project_Inverter(ddlCompany.SelectedValue, ProjectNo, "2");

                for (int j = 0; j < dtProject.Rows.Count; j++)
                {
                    string ColumnName1 = "Project " + (j + 1);
                    //string ColumnName2 = "Pick ID " + (j + 1);
                    string ColumnName3 = "No Panel " + (j + 1);
                    string ColumnName4 = "Project Status " + (j + 1);
                    DataColumnCollection columns = dtProjectWise.Columns;
                    DataColumn dcProject = new DataColumn(ColumnName1, typeof(Int32));
                    //DataColumn dcID = new DataColumn(ColumnName2, typeof(Int32));
                    DataColumn dcPanel = new DataColumn(ColumnName3, typeof(Int32));
                    DataColumn dcProjectStatus = new DataColumn(ColumnName4, typeof(string));
                    if (!columns.Contains(ColumnName1))
                    {
                        dtProjectWise.Columns.Add(dcProject);
                        //dtProjectWise.Columns.Add(dcID);
                        dtProjectWise.Columns.Add(dcPanel);
                        dtProjectWise.Columns.Add(dcProjectStatus);
                    }

                    //string ProjectNumber = dtProject.Rows[j]["ProjectNo"].ToString();
                    dtProjectWise.Rows[i][ColumnName1] = dtProject.Rows[j]["Projectnumber"];
                    //dtProjectWise.Rows[i][ColumnName2] = dtProject.Rows[j]["ID"];
                    dtProjectWise.Rows[i][ColumnName3] = dtProject.Rows[j]["UsedQty"];
                    dtProjectWise.Rows[i][ColumnName4] = dtProject.Rows[j]["ProjectStatus"];
                }
            }
        }
        catch (Exception ex)
        {

        }
        //return dtProjectWise;
    }

    protected DataTable getDataSerialNoWise(string installerId)
    {
        DataTable dt = new DataTable();
        string companyId = ddlCompany.SelectedValue;
        string projectNo = txtProjectNumber.Text;
        string locationId = ddlLocation.SelectedValue;
        string dateType = ddlDateType.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = NoTraceStock.NoTraceStockInverter_SerialNo(companyId, projectNo, installerId, locationId, dateType, startDate, endDate);

        return dt;
    }

    protected void btnInsertSerialNo_Click(object sender, EventArgs e)
    {
        int SucInsert = 0;
        DataTable dtSerialNo = GetDtSerialNo();
        if (dtSerialNo.Rows.Count > 0)
        {
            SucInsert = NoTraceStock.Bulk_Insert_tbl_SerialNo(dtSerialNo);
        }

        if (SucInsert > 0)
        {
            Notification("Transaction Successfull");
        }
    }

    public DataTable GetDtSerialNo()
    {
        DataTable dt = NoTraceStock.GreenbotAllData_InveterSerialNo();

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("CompanyID", typeof(int));
        dtSerialNo.Columns.Add("ProjectNo", typeof(int));
        dtSerialNo.Columns.Add("SerialNo", typeof(string));
        dtSerialNo.Columns.Add("CategoryId", typeof(int));
        dtSerialNo.Columns.Add("BSGBFlag", typeof(int));
        dtSerialNo.Columns.Add("InstallerName", typeof(string));

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string companyId = dt.Rows[i]["CompanyId"].ToString();
            string projectNo = dt.Rows[i]["ProjectNo"].ToString();
            string installerName = dt.Rows[i]["InstallerName"].ToString();
            string serialNo = dt.Rows[i]["FieldValue_LstCD"].ToString();

            string splitOprator = "\n";
            if (serialNo.Contains("\r"))
            {
                splitOprator = "\r";
            }
            
            //string[] serialNoList = System.Text.RegularExpressions.Regex.Split(serialNo, "\n");
            string[] serialNoList = System.Text.RegularExpressions.Regex.Split(serialNo, splitOprator);

            for (int j = 0; j < serialNoList.Length; j++)
            {
                if (serialNoList[j].Trim() != "")
                {
                    if(serialNoList[j].Trim() == "130AC2212220295")
                    {
                        string ss = "";
                    }

                    string sNo = serialNoList[j].Trim();
                    DataRow dr = dtSerialNo.NewRow();
                    dr["CompanyID"] = companyId;
                    dr["ProjectNo"] = projectNo;
                    dr["SerialNo"] = sNo;
                    dr["CategoryId"] = 2; // Inverter
                    dr["BSGBFlag"] = 1; // GreenBot
                    dr["InstallerName"] = installerName.Trim();

                    bool contains = dtSerialNo.AsEnumerable().Any(row => sNo.ToUpper() == row.Field<String>("SerialNo").ToUpper());
                    if (contains == false)
                    {
                        dtSerialNo.Rows.Add(dr);
                    }
                }
            }
        }
        return dtSerialNo;
    }

    protected void btnBSManual_Click(object sender, EventArgs e)
    {
        ddlAssignCompany.ClearSelection();
        txtAssignProjectNo.Text = string.Empty;
        txtAssignSerialNo.Text = string.Empty;
        MPUpdateSerialNo.Show();
    }

    protected void btnUpdateBSSerialNo_Click(object sender, EventArgs e)
    {
        try
        {
            string companyid = ddlAssignCompany.SelectedValue;
            string projectNo = txtAssignProjectNo.Text;
            //string serialNo = txtAssignSerialNo.Text;
            string[] serialNoList = txtAssignSerialNo.Text.Trim().Split('\n');

            int suc = 0;
            for (int i = 0; i < serialNoList.Length; i++)
            {
                string serialNo = serialNoList[i].Trim();
                if (!string.IsNullOrEmpty(serialNo))
                {
                    string Msg = NoTraceStock.tbl_BSAllSerialInverterSerialNo_Insert(projectNo, serialNo, companyid, "2");
                    //int sucInsert = ClsReportsV2.tbl_AssignSerialNoToProject_Insert(companyId, projectNo, serialNo, createdOn, createdBy);
                    //if (sucInsert > 0)
                    //{
                    //    suc++;
                    //}

                }
            }
            Notification("Transaction Successful.");
        }
        catch
        {
            Notification("Error");
        }
        //if(Msg == "Success")
        //{
        //    Notification("Transaction Successful.");
        //}
        //else if(Msg == "Serial No Exists")
        //{
        //    Notification(Msg);
        //}
        //else
        //{
        //    Notification("Error");
        //}
    }

    protected void btnNotes_Click(object sender, EventArgs e)
    {
        txtNotes.Text = string.Empty;
        txtSerialNo.Text = string.Empty;
        ModalPopupExtender1.Show();
    }

    protected void btnSaveNotes_Click(object sender, EventArgs e)
    {
        string ex = NoTraceStock.SerialNoNotes_Insert(txtSerialNo.Text.Trim(), txtNotes.Text.Trim(), "2");

        Notification(ex);
    }
}