﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_BrokenSerialNoReport : System.Web.UI.Page
{
    static DataView dv;
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            if (Roles.IsUserInRole("WholeSale"))
            {
                divprojNo.Visible = false;
            }

            binddropdown();
            BindLocation();
            BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {
        DataTable dt = ClsReportsV2.Sp_BrokenSerialNoHistory(txtProjectNo.Text, txtStockItem.Text, txtserailno.Text, ddlModuleName.SelectedValue, ddlLocation.SelectedValue, "1", txtStartDate.Text, txtEndDate.Text, ddlCategory.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                // SetDelete();
            }
            else
            {
                //  SetNoRecords();
                Notification("There are no items to show in this view.");
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            btnEmail.Visible = true;
            lbtnExport.Visible = true;
            divtot.Visible = true;


            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }

                if (dt.Rows.Count > 0)
                {
                    //int sum = Convert.ToInt32(dt.Compute("SUM(Qty)", string.Empty));

                    lblTotalPanels.Text = dt.Rows.Count.ToString();
                }
                else
                {
                    lblTotalPanels.Text = "0";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    public void BindLocation()
    {
        //rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //rptLocation.DataBind();

        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dt;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    public void binddropdown()
    {
        string qr = "select  DISTINCT modulename from tblMaintainHistory where modulename in('Defected Item','Broken Item','Faulty Item', 'Missing Item')";
        DataTable modulename = ClstblCustomers.query_execute(qr);

        //rptItems.DataSource = modulename;
        //rptItems.DataBind();

        ddlModuleName.DataSource = modulename;
        ddlModuleName.DataValueField = "modulename";
        ddlModuleName.DataTextField = "modulename";
        ddlModuleName.DataBind();

        DataTable dt = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dt;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Delete")
        {

            string SerialNo = e.CommandArgument.ToString();
            hdndelete.Value = SerialNo;
           // ModalPopupExtenderDelete.Show();

        }

        if (e.CommandName.ToString() == "Update")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(',');

            hdnserialno.Value = arg[0];
            string stockitemid = arg[1];
            txtserialno.Text = hdnserialno.Value;
            hdnstockitemid.Value = stockitemid;
            //ModalPopupExtenderUpdateSerialNo.Show();

        }
        if (e.CommandName.ToString() == "Clear")
        {
            string SerialNo = e.CommandArgument.ToString();
            bool clear = ClstblStockSerialNo.tblStockSerialNo_Clear(SerialNo);
            if (clear)
            {
                // SetDelete();
                Notification("Transaction Successful.");
                //PanSuccess.Visible = true;
            }
            else
            {
                Notification("Transaction Failed.");
            }
            BindGrid(0);
        }


        //int StockItemID = int.Parse(Request.QueryString["StockItemID"].ToString());
        //string StockLocationId = Request.QueryString["CompanyLocationId"].ToString();
        //if (e.CommandName.ToString() == "History")
        //{
        //    string SerailNo = e.CommandArgument.ToString();
        //    Response.Redirect("~/admin/adminfiles/stock/ViewSerialHistory.aspx?SerailNo=" + SerailNo + "&StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocationId);
        //}
        //BindGrid(0);
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);

    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockitem.aspx");
    }
    
    #region pagination
   
    #endregion

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //int StockItemID = int.Parse(Request.QueryString["StockItemID"].ToString());
            //string StockLocationId = Request.QueryString["CompanyLocationId"].ToString();
            //HiddenField hdnserialno = (HiddenField)e.Row.FindControl("hdnserialno");
            //HyperLink gvbtnView = (HyperLink)e.Row.FindControl("gvbtnView");
            //gvbtnView.NavigateUrl = SiteURL + "admin/adminfiles/stock/ViewSerialHistory.aspx?SerailNo=" + hdnserialno.Value + "&StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocationId;
            //gvbtnView.Target = "_blank";


            //Label Label39 = (Label)e.Row.FindControl("Label39");
            //HiddenField hndprojNumber = (HiddenField)e.Row.FindControl("hndprojNumber");
            //HiddenField hndPickid = (HiddenField)e.Row.FindControl("hndPickid");
            //Label Label79 = (Label)e.Row.FindControl("Label79");
            //HiddenField hndInvoiceNo = (HiddenField)e.Row.FindControl("hndInvoiceNo");
            //HiddenField hndWholesaleOrderID = (HiddenField)e.Row.FindControl("hndWholesaleOrderID");


            // Commented Part for the Exception //

            //if (string.IsNullOrEmpty(hndprojNumber.Value) && string.IsNullOrEmpty(hndPickid.Value))
            //{
            //    Label39.Text = "";
            //}
            //if (string.IsNullOrEmpty(hndInvoiceNo.Value) && string.IsNullOrEmpty(hndWholesaleOrderID.Value))
            //{
            //    Label79.Text = "";
            //}
        }
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtStockItem.Text = string.Empty;
        txtserailno.Text = string.Empty;
        lbtnExport.Visible = false;
        btnEmail.Visible = false;
        txtStartDate.Text = "";
        txtEndDate.Text = "";
        txtProjectNo.Text = "";

        ddlLocation.SelectedValue = "";
        ddlModuleName.SelectedValue = "";
        ddlCategory.SelectedValue = "";

        BindGrid(0);

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {

    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void lbtnExport_Click1(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        Export oExport = new Export();
        string FileName = "BrokenSerialHistoryReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 10, 1, 2, 3, 4, 11, 5, 6, 7 };

        string[] arrHeader = { "Name", "Proj/Inv/Tran/Order No", "PicklistID/WID", "Serial No", "Item Name", "Category", "ModuleName", "Location", "CreatedDate" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        //ModalPopupExtenderEEmail.Show();
    }

    protected void btnsendmail_Click(object sender, EventArgs e)
    {
        //string Emailto = txtemail.Text;

        //string ProjNo = txtProjNo.Text;
        ////string Wholesaleorderid = txtWholesaleorderid.Text;
        //string serialno = txtserailno.Text;
        //string Location = "";
        //if (lstlocation2.Items.Count > 0)//for save selected item Value
        //{
        //    for (int i = 0; i < lstlocation2.Items.Count; i++)
        //    {
        //        if (lstlocation2.Items[i].Selected)
        //        {
        //            Location += lstlocation2.Items[i].Value + ",";

        //        }
        //    }
        //}
        //if (Location != "")
        //{
        //    Location = Location.TrimEnd(',');
        //}

       // TextWriter txtWriter = new StringWriter() as TextWriter;
       // StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
       // string from = stU.from;
       // String Subject = "Email";

       //// Server.Execute("~/mailtemplate/HistoryMail.aspx?ProjectNumber=" + ProjNo + "&WholesaleOrderID=" + Wholesaleorderid + "&SerailNo=" + serialno + "&Modulename=" + lstlocation2.SelectedValue, txtWriter);
       // string[] multi = Emailto.Split(',');
       // foreach (string multiEmail in multi)
       // {
       //     Utilities.SendMail(from, multiEmail, Subject, txtWriter.ToString());
       //     //Utilities.SendMail("kiransawant198@gmail.com", "kiran.sawant@meghtechnologies.com", Subject, txtWriter.ToString());

       // }



    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string SerialNo = hdndelete.Value.ToString();
        bool succdelete = ClstblMaintainHistory.tblMaintainHistory_DeleteRecord_SerialNo(SerialNo);
        ClstblStockSerialNo.tblStockSerialNo_DeleteRecord_SerialNo(SerialNo);
        if (succdelete)
        {
            // SetDelete();
            Notification("Transaction Successful.");
            //PanSuccess.Visible = true;
        }
        else
        {
            Notification("Transaction Failed.");
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        //BindGrid(1);
        //PanAddUpdate.Visible = false;
    }

    protected void btnUpdateserialno_Click(object sender, EventArgs e)
    {
        string SerialNo = hdnserialno.Value;
        string StockITemid = hdnstockitemid.Value;
        if (!string.IsNullOrEmpty(txtupdateserno.Text))
        {
            bool suc = ClstblStockSerialNo.tblStockSerialNo_UpdateSerialno(SerialNo, StockITemid, txtupdateserno.Text);
            if (suc)
            {
                // SetDelete();
                Notification("Transaction Successful.");
                //PanSuccess.Visible = true;
            }
            else
            {
                Notification("Transaction Failed.");
            }
        }
        else
        {
            Notification("Please Enter Updated Serial No.");
        }
        BindGrid(0);

    }
}