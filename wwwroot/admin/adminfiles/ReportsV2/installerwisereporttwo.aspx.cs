using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_installerwisereporttwo : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv3;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMltiCheckBox();
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords3.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords3.DataBind();

            ddlSMSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSMSelectRecords.DataBind();

            //HideShow();
            //HideShowW();
            //HideShowSM();

            //DataTable dt = ClstblContacts.tblCustType_SelectVender();

            //txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            //txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                //BindDropDown();
                //DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                //string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                //ddllocationsearch.SelectedValue = CompanyLocationID;
                //ddllocationsearch.Enabled = false;
                //BindGrid(0);
            }

            //if (Roles.IsUserInRole("Wholesale"))
            //{
            //    TabProjectNo.Visible = false;
            //    //BindGrid3(0);
            //    BindDropDown3();
            //}

            //BindGrid(0);
            //BindDropDown();

            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }

    }

    public void BindDropDown()
    {
        //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddllocationsearch.DataSource = dt;
        //ddllocationsearch.DataTextField = "location";
        //ddllocationsearch.DataValueField = "CompanyLocationID";
        //ddllocationsearch.DataMember = "CompanyLocationID";
        //ddllocationsearch.DataBind();

        //ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //ddlInstaller.DataValueField = "ContactID";
        //ddlInstaller.DataMember = "Contact";
        //ddlInstaller.DataTextField = "Contact";
        //ddlInstaller.DataBind();

        //ddlSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        //ddlSearchStatus.DataValueField = "ProjectStatusID";
        //ddlSearchStatus.DataTextField = "ProjectStatus";
        //ddlSearchStatus.DataMember = "ProjectStatus";
        //ddlSearchStatus.DataBind();
        //lstProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        //lstProjectStatus.DataValueField = "ProjectStatusID";
        //lstProjectStatus.DataTextField = "ProjectStatus";
        //lstProjectStatus.DataMember = "ProjectStatus";
        //lstProjectStatus.DataBind();


        //lstSearchStatus.datasource = clstblprojectstatus.tblprojectstatus_selectactive();
        //lstSearchStatus.databind();

    }

    public void BindDropDown3()
    {
        //DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddllocationsearch3.DataSource = dt;
        //ddllocationsearch3.DataTextField = "location";
        //ddllocationsearch3.DataValueField = "CompanyLocationID";
        //ddllocationsearch3.DataMember = "CompanyLocationID";
        //ddllocationsearch3.DataBind();

        //DataTable dt2 = ClstblContacts.tblCustType_SelectWholesaleVendor();
        //ddlSearchVendor.DataSource = dt2;
        //ddlSearchVendor.DataTextField = "Customer";
        //ddlSearchVendor.DataValueField = "CustomerID";
        //ddlSearchVendor.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlIsverify.SelectedValue))
        {
            Isverify = ddlIsverify.SelectedValue;
        }

        string selectedLocationItem = "";
        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }


        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        string selectedAInstallerID = "";
        selectedAInstallerID = ddlInstaller.SelectedValue;
        //foreach (RepeaterItem item in rptAInstaller.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnAInstallerID");
        //    //Literal modulename = (Literal)item.FindControl("ltprojstatus");

        //    if (chkselect.Checked == true)
        //    {
        //        selectedAInstallerID += "," + hdnAInstallerID.Value.ToString();
        //    }
        //}
        //if (selectedAInstallerID != "")
        //{
        //    selectedAInstallerID = selectedAInstallerID.Substring(1);
        //}


        //DataTable dt = Reports.InstallerwiseReport_QuickStockShowAll(txtProjectNumber.Text, txtserailno.Text, txtstockitemfilter.Text, selectedLocationItem, selectedAInstallerID, selectedItem, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Isverify, ddlprojectwise.SelectedValue, ddlIsDifference.SelectedValue, ddlYesNo.SelectedValue);

        DataTable dt = ClsReportsV2.InstallerwiseReport_GetDateV2(txtProjectNumber.Text, selectedLocationItem, selectedAInstallerID, selectedItem, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Isverify, ddlIsDifference.SelectedValue, ddlYesNo.SelectedValue);

        try
        {

            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{

            //    HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
            //    Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
            //    Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
            //    Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevrt");
            //    Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
            //    Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblinverterrevert");
            //    LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
            //    LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");
            //    LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");

            //    if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
            //    {
            //        gvbtnVerify.Visible = true;
            //    }
            //    else
            //    {
            //        gvbtnVerify.Visible = false;
            //    }
            //    //if (!string.IsNullOrEmpty(dt.Rows[i]["IsVerify"].ToString()))
            //    //{
            //    //    if (dt.Rows[i]["IsVerify"].ToString() == "True")
            //    //    {
            //    //        gvbtnVerify.Visible = false;
            //    //    }
            //    //}
            //    DataTable dt1 = null;
            //    dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
            //    if (dt1.Rows.Count > 0)
            //    {
            //        try
            //        {
            //            if (!string.IsNullOrEmpty(dt1.Rows[0]["Verifynote"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedate"].ToString()))
            //            {
            //                // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
            //                gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
            //            }
            //        }
            //        catch { }
            //    }
            //}
            #region

            //int PanelStockDeducted = 0;
            //int SaleQtyPanel = 0;
            //int InverterStockDeducted = 0;
            //int SaleQtyInverter = 0;
            //int paneltotdiff = 0;
            //int paneltotrevert = 0;
            //int invertertotdiff = 0;
            //int invertertotrevert = 0;
            //int totPanelStockDeducted = 0;
            //int totSaleQtyPanel = 0;
            //int totInverterStockDeducted = 0;
            //int totSaleQtyInverter = 0;
            
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
                //PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
                //SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
                //InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
                //SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

                //paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
                //paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
                //invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
                //invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);
                

                //totPanelStockDeducted += PanelStockDeducted;
                //totSaleQtyPanel += SaleQtyPanel;
                //totInverterStockDeducted += InverterStockDeducted;
                //totSaleQtyInverter += SaleQtyInverter;
                
                //gvbtnVerify.Visible = false;
                //gvbnNote.Visible = false;
                //imgdiv.Visible = false;
                //gvbtnView.Visible = false;

            //}
            //lblPDifference.Text = paneltotdiff.ToString();
            //lblPRevert.Text = paneltotrevert.ToString();
            //lblIDifference.Text = invertertotdiff.ToString();
            //lblIRevert.Text = invertertotrevert.ToString();

            //lblpout.Text = totPanelStockDeducted.ToString();
            //lblPInstalled.Text = totSaleQtyPanel.ToString();
            //lblIOut.Text = totInverterStockDeducted.ToString();
            //lblIInstalled.Text = totSaleQtyInverter.ToString();
            //lblPAudit.Text = (paneltotdiff - paneltotrevert).ToString();
            //lblIAudit.Text = (invertertotdiff - invertertotrevert).ToString();
            #endregion
        }
        catch { }

        return dt;
    }

    protected void BindTotal(DataTable dt)
    {

        string PanelStockDeducted = dt.Compute("SUM(PanelStockDeducted)", string.Empty).ToString();
        lblpout.Text = PanelStockDeducted;

        string SaleQtyPanel = dt.Compute("SUM(PanelInstalled)", string.Empty).ToString();
        lblPInstalled.Text = SaleQtyPanel;

        string PanelDiff = (Convert.ToInt32(PanelStockDeducted) - Convert.ToInt32(SaleQtyPanel)).ToString();
        lblPDifference.Text = PanelDiff;

        string PanelRevert = dt.Compute("SUM(PanelRevert)", string.Empty).ToString();
        lblPRevert.Text = PanelRevert;

        lblPAudit.Text = (Convert.ToInt32(PanelDiff) - Convert.ToInt32(PanelRevert)).ToString();

        string InverterStockDeducted = dt.Compute("SUM(InverterStockDeducted)", string.Empty).ToString();
        lblIOut.Text = InverterStockDeducted;

        string SaleQtyInverter = dt.Compute("SUM(InverterInstalled)", string.Empty).ToString();
        lblIInstalled.Text = SaleQtyInverter;

        string InverterDiff = (Convert.ToInt32(InverterStockDeducted) - Convert.ToInt32(SaleQtyInverter)).ToString();
        lblIDifference.Text = InverterDiff;

        string InvertRevert = dt.Compute("SUM(InverterRevert)", string.Empty).ToString();
        lblIRevert.Text = InvertRevert;

        lblIAudit.Text = (Convert.ToInt32(InverterDiff) - Convert.ToInt32(InvertRevert)).ToString();
    }

    protected DataTable GetGridData3()//wholesaleorderid wise
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlIsverify1.SelectedValue))
        {
            Isverify = ddlIsverify1.SelectedValue;
        }

        string Status = "";
        foreach (RepeaterItem item in rptWStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnWStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Status += "," + hdnModule.Value.ToString();
            }
        }
        if (Status != "")
        {
            Status = Status.Substring(1);
        }

        string selectedWCustomerID = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnWCustomerID = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWCustomerID += "," + hdnWCustomerID.Value.ToString();
            }
        }
        if (selectedWCustomerID != "")
        {
            selectedWCustomerID = selectedWCustomerID.Substring(1);
        }

        string WLocationItem = "";
        foreach (RepeaterItem item in rptWLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                WLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (WLocationItem != "")
        {
            WLocationItem = WLocationItem.Substring(1);
        }

        DataTable dt = Reports.InstallerwiseReport_QuickStockWholesaleShowAll(txtInvoiceNo.Text, txtserailno3.Text, txtstockitemfilter3.Text, WLocationItem, selectedWCustomerID, Status, ddldate3.SelectedValue, txtstartdate3.Text, txtenddate3.Text, Isverify, ddlWIsDifference.SelectedValue, ddlWYesNo.SelectedValue);

        try
        {
            for (int i = 0; i < GridView3.Rows.Count; i++)
            {

                HiddenField hndWholesaleorderID = (HiddenField)GridView3.Rows[i].FindControl("hndWholesaleorderID");
                Label lblpaneldiff = (Label)GridView3.Rows[i].FindControl("Label823");
                Label lblwholepanelrevert = (Label)GridView3.Rows[i].FindControl("lblwholepanelrevert");
                Label lblinverterdiff = (Label)GridView3.Rows[i].FindControl("Label1526");
                Label lblwholeInverterrevert = (Label)GridView3.Rows[i].FindControl("lblwholeInverterrevert");
                LinkButton dvbtnVeriify = (LinkButton)GridView3.Rows[i].FindControl("dvbtnVeriify");
                LinkButton dvbtnnote1 = (LinkButton)GridView3.Rows[i].FindControl("dvbtnnote1");
                LinkButton gvbtnView3 = (LinkButton)GridView3.Rows[i].FindControl("gvbtnView3");


                if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblwholepanelrevert.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblwholeInverterrevert.Text))
                {
                    dvbtnVeriify.Visible = true;
                }
                else
                {
                    dvbtnVeriify.Visible = false;
                }
                if (!string.IsNullOrEmpty(dt.Rows[i]["IsVerify"].ToString()))
                {
                    if (dt.Rows[i]["IsVerify"].ToString() == "True")
                    {
                        dvbtnVeriify.Visible = false;
                    }
                }
                DataTable dt1 = null;
                dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", hndWholesaleorderID.Value, "", "", "");
                if (dt1.Rows.Count > 0)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(dt1.Rows[0]["NoteDes"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["NoteDate"].ToString()))
                        {
                            // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                            dvbtnnote1.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                        }
                    }
                    catch { }
                }
            }
            
        }
        catch(Exception ex)
        {
        }
        #region
        int PanelStockDeducted = 0;
        int SaleQtyPanel = 0;
        int InverterStockDeducted = 0;
        int SaleQtyInverter = 0;
        int paneltotdiff = 0;
        int paneltotrevert = 0;
        int invertertotdiff = 0;
        int invertertotrevert = 0;
        int totPanelStockDeducted = 0;
        int totSaleQtyPanel = 0;
        int totInverterStockDeducted = 0;
        int totSaleQtyInverter = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            totPanelStockDeducted += PanelStockDeducted;
            totSaleQtyPanel += SaleQtyPanel;
            totInverterStockDeducted += InverterStockDeducted;
            totSaleQtyInverter += SaleQtyInverter;

        }
        lblwPDifference.Text = paneltotdiff.ToString();
        lblwPRevert.Text = paneltotrevert.ToString();
        lblwIDifference.Text = invertertotdiff.ToString();
        lblwIRevert.Text = invertertotrevert.ToString();

        lblwPOut.Text = totPanelStockDeducted.ToString();
        lblwInvPanel.Text = totSaleQtyPanel.ToString();
        lblwIOut.Text = totInverterStockDeducted.ToString();
        lblwInvInverter.Text = totSaleQtyInverter.ToString();
        lblwPAudit.Text = (paneltotdiff - paneltotrevert).ToString();
        lblwIAudit.Text = (invertertotdiff - invertertotrevert).ToString();
        #endregion
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }

        // bind();
    }

    public void BindGrid3(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        dv3 = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid3.Visible = false;
            divnopage3.Visible = false;
            div3.Visible = false;
        }
        else
        {
            PanGrid3.Visible = true;
            div3.Visible = true;
            GridView3.DataSource = dt;
            GridView3.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords3.SelectedValue != string.Empty && ddlSelectRecords3.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords3.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage3.Visible = false;
                }
                else
                {
                    divnopage3.Visible = true;
                    int iTotalRecords = dv3.ToTable().Rows.Count;
                    int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords3.SelectedValue == "All")
                {
                    divnopage3.Visible = true;
                    ltrPage3.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
        // bind3();
    }

    public void bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {

            HiddenField hndpicklistId = (HiddenField)GridView1.Rows[i].FindControl("HiddenField1");
            Label lblProjectNumber = (Label)GridView1.Rows[i].FindControl("Label11");
            Label lblpaneldiff = (Label)GridView1.Rows[i].FindControl("Label82");
            Label lblpanelrevrt = (Label)GridView1.Rows[i].FindControl("lblpanelrevrt");
            Label lblinverterdiff = (Label)GridView1.Rows[i].FindControl("Label152");
            Label lblinverterrevert = (Label)GridView1.Rows[i].FindControl("lblinverterrevert");
            LinkButton gvbtnVerify = (LinkButton)GridView1.Rows[i].FindControl("gvbtnVerify");
            LinkButton gvbnNote = (LinkButton)GridView1.Rows[i].FindControl("gvbnNote");
            LinkButton gvbtnView = (LinkButton)GridView1.Rows[i].FindControl("gvbtnView");

            if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblpanelrevrt.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblinverterrevert.Text))
            {
                gvbtnVerify.Visible = true;
            }
            else
            {
                gvbtnVerify.Visible = false;
            }
            if (!string.IsNullOrEmpty(dt.Rows[i]["IsVerify"].ToString()))
            {
                if (dt.Rows[i]["IsVerify"].ToString() == "True")
                {
                    gvbtnVerify.Visible = false;
                }
            }
            DataTable dt1 = null;
            dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", lblProjectNumber.Text, "", "", hndpicklistId.Value);
            if (dt1.Rows.Count > 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(dt1.Rows[0]["Verifynote"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["notedate"].ToString()))
                    {
                        // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                        gvbnNote.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                    }
                }
                catch { }
            }
        }

    }

    public void bind3()
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        for (int i = 0; i < GridView3.Rows.Count; i++)
        {

            HiddenField hndWholesaleorderID = (HiddenField)GridView3.Rows[i].FindControl("hndWholesaleorderID");
            Label lblpaneldiff = (Label)GridView3.Rows[i].FindControl("Label823");
            Label lblwholepanelrevert = (Label)GridView3.Rows[i].FindControl("lblwholepanelrevert");
            Label lblinverterdiff = (Label)GridView3.Rows[i].FindControl("Label1526");
            Label lblwholeInverterrevert = (Label)GridView3.Rows[i].FindControl("lblwholeInverterrevert");
            LinkButton dvbtnVeriify = (LinkButton)GridView3.Rows[i].FindControl("dvbtnVeriify");
            LinkButton dvbtnnote1 = (LinkButton)GridView3.Rows[i].FindControl("dvbtnnote1");
            LinkButton gvbtnView3 = (LinkButton)GridView3.Rows[i].FindControl("gvbtnView3");


            if (Convert.ToInt32(lblpaneldiff.Text) <= Convert.ToInt32(lblwholepanelrevert.Text) && Convert.ToInt32(lblinverterdiff.Text) <= Convert.ToInt32(lblwholeInverterrevert.Text))
            {
                dvbtnVeriify.Visible = true;
            }
            else
            {
                dvbtnVeriify.Visible = false;
            }
            if (!string.IsNullOrEmpty(dt.Rows[i]["IsVerify"].ToString()))
            {
                if (dt.Rows[i]["IsVerify"].ToString() == "True")
                {
                    dvbtnVeriify.Visible = false;
                }
            }
            DataTable dt1 = null;
            dt1 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", hndWholesaleorderID.Value, "", "", "");
            if (dt1.Rows.Count > 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(dt1.Rows[0]["NoteDes"].ToString()) || !string.IsNullOrEmpty(dt1.Rows[0]["NoteDate"].ToString()))
                    {
                        // gvbnNote.CssClass = "btn btn-success btn-mini Excel";
                        dvbtnnote1.Attributes.Add("style", "background-color: #218838;border-color:#218838;");
                    }
                }
                catch { }
            }
        }


    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged3(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords3.SelectedValue) == "All")
        {
            GridView3.AllowPaging = false;
            BindGrid3(0);
        }
        else
        {
            GridView3.AllowPaging = true;
            GridView3.PageSize = Convert.ToInt32(ddlSelectRecords3.SelectedValue);
            BindGrid3(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        GridView3.DataSource = dv3;
        GridView3.DataBind();
        BindGrid3(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid3(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }



        }
        catch { }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView3.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView3.PageIndex - 2;
            page[1] = GridView3.PageIndex - 1;
            page[2] = GridView3.PageIndex;
            page[3] = GridView3.PageIndex + 1;
            page[4] = GridView3.PageIndex + 2;
            page[5] = GridView3.PageIndex + 3;
            page[6] = GridView3.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView3.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView3.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView3.PageIndex == GridView3.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage3 = (Label)gvrow.Cells[0].FindControl("ltrPage3");
            if (dv3.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv3.ToTable().Rows.Count;
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage3.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    void lb_Command3(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid3(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command3);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        //DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeID("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "");
        //Response.Clear();
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 19, 22, 3, 4, 21, 16, 20 };
        //    string[] arrHeader = { "Project No.", "Installer Name", "Serial No.", "Pallet No.", "Category", "Stock Item", "Location" };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    protected void lbtnExport3_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtstockitemfilter.Text = string.Empty;
        txtserailno.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        //ddlSearchStatus.SelectedValue = "";
        //lstProjectStatus.ClearSelection();
        ddlIsverify.SelectedValue = "";
        ddlprojectwise.SelectedValue = "1";
        //lstProjectStatus.SelectedIndex = -1;

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            //ddllocationsearch.Enabled = false;
        }
        ClearCheckBox();
        BindGrid(0);
    }

    protected void btnClearAll3_Click(object sender, EventArgs e)
    {
        txtstockitemfilter3.Text = string.Empty;
        txtserailno3.Text = string.Empty;
        txtInvoiceNo.Text = string.Empty;
        //ddllocationsearch3.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        ddldate3.SelectedValue = "";
        txtstartdate3.Text = string.Empty;
        txtenddate3.Text = string.Empty;
        //ddlstatus.SelectedValue = "";
        ddlIsverify1.SelectedValue = "";
        //lstProjectStatus.SelectedIndex = -1;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {


            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch3.SelectedValue = CompanyLocationID;
            //ddllocationsearch3.Enabled = false;


        }
        BindGrid3(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        else if (e.CommandName.ToLower() == "verify")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            }
            ModalPopupExtenderverify.Show();
        }
        else if (e.CommandName.ToLower() == "note")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            DataTable dt;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
                dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        txtnotedesc.Text = dt.Rows[0]["Verifynote"].ToString();
                        //txtnotedate.Text = dt.Rows[0]["notedate"].ToString();
                        DateTime notedt1 = Convert.ToDateTime(dt.Rows[0]["notedate"].ToString());
                        txtnotedate.Text = notedt1.ToString("dd/MM/yyyy");
                    }
                    catch { }
                }
                else
                {
                    txtnotedesc.Text = "";
                    txtnotedate.Text = "";
                }
            }
            else
            {
                txtnotedesc.Text = "";
                txtnotedate.Text = "";
            }
            //txtnotedesc.Text = "";
            //txtnotedate.Text = "";
            ModalPopupExtenderNote.Show();
        }

        BindGrid(0);
    }

    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage3")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();

            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
            rptItems.DataBind();
            ModalPopupExtenderDetail.Show();
            BindGrid3(0);
        }
        else if (e.CommandName.ToLower() == "verify")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            hndwholesaleorderID.Value = WholesaleOrderID;


            ModalPopupExtenderWholeSaleVerify.Show();
        }
        else if (e.CommandName == "Wholesalenotedetail")
        {
            DataTable dt = new DataTable();
            string WholesaleOrderID = e.CommandArgument.ToString();
            hndwholesaleorderID.Value = WholesaleOrderID;
            dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
            if (dt.Rows.Count > 0)
            {
                try
                {
                    DateTime wnotedate = Convert.ToDateTime(dt.Rows[0]["NoteDate"].ToString());
                    txtwholesaledate.Text = wnotedate.ToString("dd/MM/yyyy");
                    txtwholesalenote.Text = dt.Rows[0]["NoteDes"].ToString();
                }
                catch { }
            }
            else
            {
                txtwholesaledate.Text = "";
                txtwholesalenote.Text = "";
            }
            //txtwholesaledate.Text = "";
            //txtwholesalenote.Text = "";
            ModalPopupExtenderWholeNote.Show();
        }

    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("WholeSale"))
        {
            TabContainer1.ActiveTabIndex = 0;

            //BindGrid3(0);
            BindDropDown3();
        }
        else
        {
            if (TabContainer1.ActiveTabIndex == 0)
            {
                //BindGrid(0);
                //BindDropDown();
                Response.Redirect("~/admin/adminfiles/reports/installerwisereporttwo.aspx");
            }
            else if (TabContainer1.ActiveTabIndex == 1)
            {
                PanGrid3.Visible = false;
                divnopage3.Visible = false;
                div3.Visible = false;
                //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

                if (Roles.IsUserInRole("Warehouse"))
                {

                    BindDropDown3();
                    //DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                    //string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                    //ddllocationsearch3.SelectedValue = CompanyLocationID;
                    //ddllocationsearch3.Enabled = false;
                    //BindGrid3(0);
                   
                }
                else
                {
                    //BindGrid3(0);
                    //BindDropDown3();
                }
            }
            else if (TabContainer1.ActiveTabIndex == 2)
            {
                PanGridSM.Visible = false;
                divnopageSM.Visible = false;
                divSMtot.Visible = false;
                //BindGridSM(0);
            }
        }
    }

    protected void btnsavenote_Click(object sender, EventArgs e)
    {
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_NoteUpdate(Convert.ToInt32(PicklistId), txtnotedate.Text, txtnotedesc.Text);
        }
        BindGrid(0);
    }

    protected void lnkverify_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string PicklistId = hdnPickListId.Value;
        if (PicklistId != "0")
        {
            ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
        }
        BindGrid(0);
    }

    protected void btnwholesalesave_Click(object sender, EventArgs e)
    {
        string wholesaleorderID = hndwholesaleorderID.Value;
        if (wholesaleorderID != "0")
        {
            ClstblrevertItem.tbl_WholesaleOrders_NoteUpdate(Convert.ToInt32(wholesaleorderID), txtwholesalenote.Text, txtwholesaledate.Text);
        }
        BindGrid3(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //HyperLink btnprojdeduct = (HyperLink)e.Row.FindControl("btnprojdeduct");
            //HyperLink btntransferdeduct = (HyperLink)e.Row.FindControl("btntransferdeduct");
            //HyperLink btnwholesalededuct = (HyperLink)e.Row.FindControl("btnwholesalededuct");
            //HyperLink btnProjnotdeduct = (HyperLink)e.Row.FindControl("btnProjnotdeduct");

            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label452");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label52");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label4522");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label752");
            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            //HiddenField hndpicklistId = (HiddenField)e.Row.FindControl("HiddenField1");
            //Label lblProjectNumber = (Label)e.Row.FindControl("Label11");
            //Label lblpaneldiff = (Label)e.Row.FindControl("Label82");
            //Label lblpanelrevrt = (Label)e.Row.FindControl("lblpanelrevrt");
            //Label lblinverterdiff = (Label)e.Row.FindControl("Label152");
            //Label lblinverterrevert = (Label)e.Row.FindControl("lblinverterrevert");
            //LinkButton gvbtnVerify = (LinkButton)e.Row.FindControl("gvbtnVerify");
            //LinkButton gvbnNote = (LinkButton)e.Row.FindControl("gvbnNote");
            //LinkButton gvbtnView = (LinkButton)e.Row.FindControl("gvbtnView");

            //if (string.IsNullOrEmpty(lblProjectNumber.Text))
            //{
            //    DataTable dt = new DataTable();
            //    dt = GetGridData1();
            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;


            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;
            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblpanelrevrt.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblinverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            gvbtnVerify.Visible = false;
            //            gvbnNote.Visible = false;
            //            imgdiv.Visible = false;
            //            gvbtnView.Visible = false;


            //        }
            //    }
            //}

        }
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label4521");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label522");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label45224");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label7525");
            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            //HiddenField hndWholesaleorderID = (HiddenField)e.Row.FindControl("hndWholesaleorderID");
            //Label lblpaneldiff = (Label)e.Row.FindControl("Label823");
            //Label lblwholepanelrevert = (Label)e.Row.FindControl("lblwholepanelrevert");
            //Label lblinverterdiff = (Label)e.Row.FindControl("Label1526");
            //Label lblwholeInverterrevert = (Label)e.Row.FindControl("lblwholeInverterrevert");
            //LinkButton dvbtnVeriify = (LinkButton)e.Row.FindControl("dvbtnVeriify");
            //LinkButton dvbtnnote1 = (LinkButton)e.Row.FindControl("dvbtnnote1");
            //LinkButton gvbtnView3 = (LinkButton)e.Row.FindControl("gvbtnView3");



            //if (string.IsNullOrEmpty(hndWholesaleorderID.Value))
            //{


            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;

            //    DataTable dt = new DataTable();
            //    dt = GetGridData3();
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;

            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblwholepanelrevert.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblwholeInverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            dvbtnnote1.Visible = false;
            //            dvbtnVeriify.Visible = false;
            //            gvbtnView3.Visible = false;
            //            imgdiv.Visible = false;
            //        }
            //    }
            //}



        }

    }

    protected void btnwholesaleVerify1_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string wholesaleorderID = hndwholesaleorderID.Value;
        if (wholesaleorderID != "0")
        {
            ClstblrevertItem.tbl_WholesaleOrders_UpdateIsverify(Convert.ToInt32(wholesaleorderID), Currendate, userid, "1");
        }
        BindGrid3(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "InstallerWiseReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        
        int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 , 13, 14, 15, 16 , 17 };
        string[] arrHeader = { "ProjectNumber", "Proj Status", "Installer", "Booked On", "Deducted On", "Location", "P. out", "P. Installed", "P. Difference", "P. Revert", "P. Audit", "I. Out", "I. Installed", "I. Difference", "I. Revert", "I. Audit" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

    }

    protected void lbtnExport3_Click1(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        Export oExport = new Export();
        string FileName = "StockVerificationWholesale" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        int[] ColList = { 0, 1, 5, 7, 6, 4, 10, 8, 12, 18, 11, 9, 13, 19 };
        string[] arrHeader = { "Order No.", "Invoice No.", "Customer", "Status", "Deducted On", "Location", "P.Out", "Inv.Panel", "P. Revert", "P. Audit", "I.Out", "Inv.Inverter", "I. Revert", "I. Audit" };
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    //protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{

    //}

    public void BindMltiCheckBox()
    {
        lstSearchLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        lstSearchLocation.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectByActive();
        lstSearchStatus.DataBind();

        //rptAInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        //rptAInstaller.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();

        rptWLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptWLocation.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();

        DataTable dt = new DataTable();
        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("Status", typeof(String));

        dt.Rows.Add(new object[] { 1, "Pending" });
        dt.Rows.Add(new object[] { 2, "JobBooked" });
        dt.Rows.Add(new object[] { 3, "Void" });
        dt.Rows.Add(new object[] { 4, "PVD Applied" });
        dt.Rows.Add(new object[] { 5, "Approved" });
        dt.Rows.Add(new object[] { 6, "Failed" });
        dt.Rows.Add(new object[] { 7, "Blank" });

        rptWStatus.DataSource = dt;
        rptWStatus.DataBind();

        rptSMselectlocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptSMselectlocation.DataBind();

        rptSMProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        rptSMProjectStatus.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        //foreach (RepeaterItem item in rptAInstaller.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    chkselect.Checked = false;
        //}

        foreach (RepeaterItem item in rptWStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void ClearCheckBoxSM()
    {
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptSMselectlocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    #region SolerMiner TAB
    protected void btnSMSearch_Click(object sender, EventArgs e)
    {
        BindGridSM(0);
    }

    protected void btnSMClearAll_Click1(object sender, EventArgs e)
    {
        txtSMProjectNo.Text = string.Empty;
        //txtSMstockitemfilter.Text = string.Empty;
        //txtSMserailno.Text = string.Empty;
        //ddllocationsearch.SelectedValue = "";
        //ddlInstaller.SelectedValue = "";
        ddlSMDate.SelectedValue = "";
        txtSMStartDate.Text = string.Empty;
        txtSMEndDate.Text = string.Empty;
       
        ddlSMIsverify.SelectedValue = "10";
        ddlSMIsDifference.SelectedValue = "0";
        ddlSMYesNo.SelectedValue = "0";
        

        //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
            //DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            //string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            //ddllocationsearch.SelectedValue = CompanyLocationID;
            //ddllocationsearch.Enabled = false;
        }
        ClearCheckBoxSM();
        BindGridSM(0);
    }

    protected void ddlSMSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSMSelectRecords.SelectedValue) == "All")
        {
            GridViewSM.AllowPaging = false;
            BindGridSM(0);
        }
        else
        {
            GridViewSM.AllowPaging = true;
            GridViewSM.PageSize = Convert.ToInt32(ddlSMSelectRecords.SelectedValue);
            BindGridSM(0);
        }
    }

    public void BindGridSM(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGridSM.Visible = false;
            divnopageSM.Visible = false;
            divSMtot.Visible = false;
        }
        else
        {
            PanGridSM.Visible = true;
            divSMtot.Visible = true;
            GridViewSM.DataSource = dt;
            GridViewSM.DataBind();
            BindTotalSM(dt);
            if (dt.Rows.Count > 0 && ddlSMSelectRecords.SelectedValue != string.Empty && ddlSMSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSMSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopageSM.Visible = false;
                }
                else
                {
                    divnopageSM.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridViewSM.PageSize * (GridViewSM.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridViewSM.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPageSM.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopageSM.Visible = true;
                    ltrPageSM.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }

        // bind();
    }

    protected DataTable GetGridDataSM()     //SolerMiner
    {
        string Isverify = "";
        if (!string.IsNullOrEmpty(ddlSMIsverify.SelectedValue))
        {
            Isverify = ddlSMIsverify.SelectedValue;
        }

        string selectedLocationItem = "";
        foreach (RepeaterItem item in rptSMselectlocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }


        string selectedItem = "";
        foreach (RepeaterItem item in rptSMProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        string selectedSMInstallerID = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedSMInstallerID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (selectedSMInstallerID != "")
        {
            selectedSMInstallerID = selectedSMInstallerID.Substring(1);
        }

        //DataTable dt = Reports.InstallerwiseReport_QuickStockShowAllSolerMiner(txtSMProjectNo.Text, txtSMserailno.Text, txtSMstockitemfilter.Text, selectedLocationItem, selectedSMInstallerID, selectedItem, ddlSMDate.SelectedValue, txtSMStartDate.Text, txtSMEndDate.Text, Isverify, ddlSMprojectwise.SelectedValue, ddlSMIsDifference.SelectedValue, ddlSMYesNo.SelectedValue);

        DataTable dt = Reports.InstallerwiseReport_GetDate_SM(txtSMProjectNo.Text, selectedLocationItem, selectedSMInstallerID, selectedItem, ddlSMDate.SelectedValue, txtSMStartDate.Text, txtSMEndDate.Text, Isverify, ddlSMIsDifference.SelectedValue, ddlSMYesNo.SelectedValue);


        return dt;
    }

    protected void BindTotalSM(DataTable dt)
    {

        string PanelStockDeducted = dt.Compute("SUM(PanelStockDeducted)", string.Empty).ToString();
        lblSMpout.Text = PanelStockDeducted;

        string SaleQtyPanel = dt.Compute("SUM(PanelInstalled)", string.Empty).ToString();
        lblSMPInstalled.Text = SaleQtyPanel;

        string PanelDiff = (Convert.ToInt32(PanelStockDeducted) - Convert.ToInt32(SaleQtyPanel)).ToString();
        lblSMPDifference.Text = PanelDiff;

        string PanelRevert = dt.Compute("SUM(PanelRevert)", string.Empty).ToString();
        lblSMPRevert.Text = PanelRevert;

        lblSMPAudit.Text = (Convert.ToInt32(PanelDiff) - Convert.ToInt32(PanelRevert)).ToString();

        string InverterStockDeducted = dt.Compute("SUM(InverterStockDeducted)", string.Empty).ToString();
        lblSMIOut.Text = InverterStockDeducted;

        string SaleQtyInverter = dt.Compute("SUM(InverterInstalled)", string.Empty).ToString();
        lblSMIInstalled.Text = SaleQtyInverter;

        string InverterDiff = (Convert.ToInt32(InverterStockDeducted) - Convert.ToInt32(SaleQtyInverter)).ToString();
        lblSMIDifference.Text = InverterDiff;

        string InvertRevert = dt.Compute("SUM(InverterRevert)", string.Empty).ToString();
        lblSMIRevert.Text = InvertRevert;

        lblSMIAudit.Text = (Convert.ToInt32(InverterDiff) - Convert.ToInt32(InvertRevert)).ToString();
    }

    protected void lbtnSMExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "InstallerWiseReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";


        int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
        string[] arrHeader = { "ProjectNumber", "Proj Status", "Installer", "Booked On", "Deducted On", "Location", "P. out", "P. Installed", "P. Difference", "P. Revert", "P. Audit" , "I. Out", "I. Installed", "I. Difference", "I. Revert" , "I. Audit"};
        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

    }

    protected void GridViewSM_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridDataSM();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridViewSM.DataSource = sortedView;
        GridViewSM.DataBind();
    }

    protected void GridViewSM_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSM.PageIndex = e.NewPageIndex;
        GridViewSM.DataSource = dv;
        GridViewSM.DataBind();
        BindGridSM(0);
    }

    protected void GridViewSM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        else if (e.CommandName.ToLower() == "verify")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
            }
            ModalPopupExtenderverify.Show();
        }
        else if (e.CommandName.ToLower() == "note")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];
            hdnPickListId.Value = PicklistId;
            DataTable dt;
            if (ProjectNo != "0" && PicklistId != "0")
            {
                //ClstblrevertItem.tbl_PickListLog_UpdateIsverify(Convert.ToInt32(PicklistId), Currendate, userid, "1");
                dt = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        txtnotedesc.Text = dt.Rows[0]["Verifynote"].ToString();
                        //txtnotedate.Text = dt.Rows[0]["notedate"].ToString();
                        DateTime notedt1 = Convert.ToDateTime(dt.Rows[0]["notedate"].ToString());
                        txtnotedate.Text = notedt1.ToString("dd/MM/yyyy");
                    }
                    catch { }
                }
                else
                {
                    txtnotedesc.Text = "";
                    txtnotedate.Text = "";
                }
            }
            else
            {
                txtnotedesc.Text = "";
                txtnotedate.Text = "";
            }
            //txtnotedesc.Text = "";
            //txtnotedate.Text = "";
            ModalPopupExtenderNote.Show();
        }

        BindGridSM(0);
    }

    protected void GridViewSM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //HyperLink btnprojdeduct = (HyperLink)e.Row.FindControl("btnprojdeduct");
            //HyperLink btntransferdeduct = (HyperLink)e.Row.FindControl("btntransferdeduct");
            //HyperLink btnwholesalededuct = (HyperLink)e.Row.FindControl("btnwholesalededuct");
            //HyperLink btnProjnotdeduct = (HyperLink)e.Row.FindControl("btnProjnotdeduct");

            //Label PanelStockDeducted1 = (Label)e.Row.FindControl("Label452");
            //Label SaleQtyPanel1 = (Label)e.Row.FindControl("Label52");
            //Label InverterStockDeducted1 = (Label)e.Row.FindControl("Label4522");
            //Label SaleQtyInverter1 = (Label)e.Row.FindControl("Label752");
            //Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            //HiddenField hndpicklistId = (HiddenField)e.Row.FindControl("HiddenField1");
            //Label lblProjectNumber = (Label)e.Row.FindControl("Label11");
            //Label lblpaneldiff = (Label)e.Row.FindControl("Label82");
            //Label lblpanelrevrt = (Label)e.Row.FindControl("lblpanelrevrt");
            //Label lblinverterdiff = (Label)e.Row.FindControl("Label152");
            //Label lblinverterrevert = (Label)e.Row.FindControl("lblinverterrevert");
            //LinkButton gvbtnVerify = (LinkButton)e.Row.FindControl("gvbtnVerify");
            //LinkButton gvbnNote = (LinkButton)e.Row.FindControl("gvbnNote");
            //LinkButton gvbtnView = (LinkButton)e.Row.FindControl("gvbtnView");

            //if (string.IsNullOrEmpty(lblProjectNumber.Text))
            //{
            //    DataTable dt = new DataTable();
            //    dt = GetGridData1();
            //    int PanelStockDeducted = 0;
            //    int SaleQtyPanel = 0;
            //    int InverterStockDeducted = 0;
            //    int SaleQtyInverter = 0;
            //    int paneltotdiff = 0;
            //    int paneltotrevert = 0;
            //    int invertertotdiff = 0;
            //    int invertertotrevert = 0;
            //    int totpanel = 0;
            //    int totinverter = 0;
            //    int totPanelStockDeducted = 0;
            //    int totSaleQtyPanel = 0;
            //    int totInverterStockDeducted = 0;
            //    int totSaleQtyInverter = 0;


            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PanelStockDeducted = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            //        SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["SaleQtyPanel"]);
            //        InverterStockDeducted = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);
            //        SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["SaleQtyInverter"]);

            //        paneltotdiff += (PanelStockDeducted - SaleQtyPanel);
            //        paneltotrevert += Convert.ToInt32(dt.Rows[i]["PanelRevert"]);
            //        invertertotdiff += (InverterStockDeducted - SaleQtyInverter);
            //        invertertotrevert += Convert.ToInt32(dt.Rows[i]["InvertRevert"]);

            //        totPanelStockDeducted += PanelStockDeducted;
            //        totSaleQtyPanel += SaleQtyPanel;
            //        totInverterStockDeducted += InverterStockDeducted;
            //        totSaleQtyInverter += SaleQtyInverter;
            //        if (i == dt.Rows.Count - 1)
            //        {
            //            lblpaneldiff.Text = paneltotdiff.ToString();
            //            lblpanelrevrt.Text = paneltotrevert.ToString();
            //            lblinverterdiff.Text = invertertotdiff.ToString();
            //            lblinverterrevert.Text = invertertotrevert.ToString();

            //            PanelStockDeducted1.Text = totPanelStockDeducted.ToString();
            //            SaleQtyPanel1.Text = totSaleQtyPanel.ToString();
            //            InverterStockDeducted1.Text = totInverterStockDeducted.ToString();
            //            SaleQtyInverter1.Text = totSaleQtyInverter.ToString();

            //            gvbtnVerify.Visible = false;
            //            gvbnNote.Visible = false;
            //            imgdiv.Visible = false;
            //            gvbtnView.Visible = false;


            //        }
            //    }
            //}

        }
    }

    void lb_CommandSM(object sender, CommandEventArgs e)
    {
        GridViewSM.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGridSM(0);
    }

    protected void GridViewSM_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_CommandSM);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_CommandSM);
        }
    }

    protected void GridViewSM_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridViewSM.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridViewSM.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridViewSM.PageIndex - 2;
            page[1] = GridViewSM.PageIndex - 1;
            page[2] = GridViewSM.PageIndex;
            page[3] = GridViewSM.PageIndex + 1;
            page[4] = GridViewSM.PageIndex + 2;
            page[5] = GridViewSM.PageIndex + 3;
            page[6] = GridViewSM.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridViewSM.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridViewSM.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridViewSM.PageIndex == GridViewSM.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridViewSM.PageSize * (GridViewSM.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridViewSM.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }

        }
        catch { }
    }
    #endregion

    protected void ddlIsDifference_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideShow();
    }

    protected void ddlWIsDifference_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideShowW();
    }

    protected void ddlSMIsDifference_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideShowSM();
    }

    public void HideShow()
    {
        if(ddlIsDifference.SelectedValue == "1")
        {
            DivYesNo.Visible = true;
            ddlYesNo.SelectedValue = "1";
        }
        else
        {
            DivYesNo.Visible = false;
        }
    }

    public void HideShowW()
    {
        if (ddlWIsDifference.SelectedValue == "1")
        {
            DivWYesNo.Visible = true;
            ddlWYesNo.SelectedValue = "1";
        }
        else
        {
            DivWYesNo.Visible = false;
        }
    }

    public void HideShowSM()
    {
        if (ddlSMIsDifference.SelectedValue == "1")
        {
            DivSMYesNo.Visible = true;
            ddlSMYesNo.SelectedValue = "1";
        }
        else
        {
            DivSMYesNo.Visible = false;
        }
    }

    protected void btnUpdateSMData_Click(object sender, EventArgs e)
    {
        //int UpdateStatus = ClsDbData.USP_InsertUpdate_SM_tblProjectStatus();
        //int UpdateProNo = ClsDbData.USP_InsertUpdate_SM_tblProjectNos();
        //int UpdatePro = ClsDbData.USP_InsertUpdate_tbl_SMProjectsRVerification();
        int UpdateProject = ClsDbData.USP_InsertUpdate_tblProjects_SM();
    }
}