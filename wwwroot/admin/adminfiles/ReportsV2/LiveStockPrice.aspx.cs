﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_LiveStockPrice : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static int STTotal = 0;

    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            BindDropDown();
            
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;

            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();

        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtcategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    protected DataTable GetGridData1()
    {
        string LocationID = getLocation();
       
        DataTable dt1 = ClsReportsV2.LiveStockPrice_GetData(txtStockItem.Text, txtStockModel.Text, ddlCategory.SelectedValue, LocationID, txtOrderNo.Text);

        return dt1;
    }

    public void BindTotal(DataTable dt1)
    {
        if (dt1.Rows.Count > 0)
        {
            int CurrentQty = Convert.ToInt32(dt1.Compute("SUM(StockQuantity)", string.Empty));
            int USDCount = Convert.ToInt32(dt1.Compute("SUM(USDCount)", string.Empty));
            decimal USDPrice = Convert.ToDecimal(dt1.Compute("AVG(USDPrice)", "USDCount > 0") == DBNull.Value ? "0" : dt1.Compute("AVG(USDPrice)", "USDCount > 0"));
            decimal USDKW = Convert.ToDecimal(dt1.Compute("AVG(USDKW)", "USDCount > 0") == DBNull.Value ? "0" : dt1.Compute("AVG(USDKW)", "USDCount > 0"));

            int AUDCount = Convert.ToInt32(dt1.Compute("SUM(AUDCount)", string.Empty));
            decimal AUDPrice = Convert.ToDecimal(dt1.Compute("AVG(AUDPrice)", "AUDCount > 0") == DBNull.Value ? "0" : dt1.Compute("AVG(AUDPrice)", "AUDCount > 0"));
            decimal AUDKW = Convert.ToDecimal(dt1.Compute("AVG(AUDKW)", "AUDCount > 0") == DBNull.Value ? "0" : dt1.Compute("AVG(AUDKW)", "AUDCount > 0"));

            totlblCurrentqty.Text = Convert.ToString(CurrentQty);

            if(USDCount == 0)
            {
                lblTotUSD.Text = Convert.ToString(USDCount);
            }
            else
            {
                lblTotUSD.Text = Convert.ToString(USDCount) + "/" + USDPrice.ToString("F") + "/" + USDKW.ToString("F");
            }

            if (AUDCount == 0)
            {
                lblTotAUD.Text = Convert.ToString(AUDCount);
            }
            else
            {
                lblTotAUD.Text = Convert.ToString(AUDCount) + "/" + AUDPrice.ToString("F") + "/" + AUDKW.ToString("F");
            }
        }

        //===================================Total Details Page Linked=======================================
        //string Category = ddlCategory.SelectedValue;
        //string StockItem = txtStockItem.Text;
        //string StockModel = txtStockModel.Text;
        //string LocationID = getLocation();
        
        //if (StockOrdered != 0)
        //{
        //    totlblStockOrdered.Enabled = true;
        //    totlblStockOrdered.NavigateUrl = "~/admin/adminfiles/reports/Details/DetailsOrderQuatntity.aspx?Page=StockOrdered&StockItem=" + StockItem + "&IsActive=" + IsActive + "&Location=" + LocationID + "&Category=" + Category + "&StockModel=" + StockModel;
        //    totlblStockOrdered.Target = "_blank";
        //}
        //==============================================End====================================================
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
            //if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
            //{
            //    ltrPage.Text = "Showing " + ((GridView1.Rows.Count)) + " entries";
            //    //BindGrid(0);
            //}
        }

        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockItemID"].ToString()))
            {
                PanGrid.Visible = false;
                divnopage.Visible = false;
                SetNoRecords();
            }
        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //string LocationID = getLocation();
        //string StockItem = txtStockItem.Text;
        //string StockModel = txtStockModel.Text;

        //if (LocationID == "" && StockItem == "" && StockModel == "")
        //{
        //    //SetAdd1();
        //    //SetError1();
        //    MyfunManulMsg("Please Select Location or Enter Stock Items Name");
        //}
        //else if (LocationID.Length > 1 && StockItem == "")
        //{
        //    MyfunManulMsg("Enter Stock Items Name");
        //}
        //else
        //{
        //    BindGrid(0);
        //}

        //BindScript();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = "Live Stock Price " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string[] arrHeader = { "Stock Item", "Stock Model", "Size", "Location", "Current Qty", "USD", "AUD", "USD Amount", "AUD Amount" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
        //PrepareGridViewForExport(PanGrid);
        ////BindGrid(0);
        //string timestemp = System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss");
        //string attachment = "attachment; filename=StockWiseReport" + timestemp + ".xls";

        //Response.ClearContent();
        //Response.AddHeader("content-disposition", attachment);
        //Response.ContentType = "application/ms-excel";

        //StringWriter sw = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(sw);
        //PanGrid.RenderControl(htw);
        //////============
        //string style = @"<style> .classext { mso-number-format:\@; } </script> ";
        //Response.Write(style);
        //////============
        //Response.Write(sw.ToString());
        //Response.End();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MyfunManulMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStockItem.Text = string.Empty;
        txtOrderNo.Text = string.Empty;
        ddlCategory.SelectedValue = "";
        txtStockModel.Text = string.Empty;

        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        //BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "viewpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", PicklistId);
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", ProjectNo, "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        if (e.CommandName.ToLower() == "stockordered")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];

            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_stockorderquantity.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }

        }
        else if (e.CommandName.ToLower() == "wholesaleorder")
        {

            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];
            string StockLocation = arg[1];

            if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
            {
                Response.Redirect("stockwisereport_WholesaleOrder.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
            }
        }
        else if (e.CommandName.ToLower() == "StockSold")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string StockItemID = arg[0];
                string StockLocation = arg[1];

                if (!string.IsNullOrEmpty(StockItemID) && !string.IsNullOrEmpty(StockLocation))
                {
                    Response.Redirect("stockwisereport_Stocksold.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocation);
                }
            }
            catch { }
        }

        if (e.CommandName == "UpdateWholeSaleTag")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string StockItemID = arg[0];
            string StockLocation = arg[1];

            bool Update = ClsReportsV2.tblStockItemsLocation_UpdateWholeSaleTag(StockItemID, StockLocation);
        }

        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            //String SMStockItemID = rowView["SMStockItemID"].ToString();
        }
    }

    public string getLocation()
    {
        string LocationID = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnAInstallerID = (HiddenField)item.FindControl("hdnCompanyLocationID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                LocationID += "," + hdnAInstallerID.Value.ToString();
            }
        }
        if (LocationID != "")
        {
            LocationID = LocationID.Substring(1);
        }

        return LocationID;
    }

    public void Notification(string msg)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
        }
        catch (Exception e)
        {

        }
    }
}