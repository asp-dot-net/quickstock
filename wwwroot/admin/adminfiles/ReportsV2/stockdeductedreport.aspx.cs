using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_stockdeductedreport : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv3;
    static DataView dv4;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            if (Roles.IsUserInRole("Wholesale"))
            {
                ddlisdeduct.SelectedValue = "2";
            }

            BindCheckBox();
            HideCheckBox();

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }

            BindDropDown();
            BindGrid(0);
        }
    }

    protected DataTable getGridInfo(DataTable dt)
    {
        try
        {
            dt.Columns.Add(new System.Data.DataColumn("ProjectStatus1", typeof(String)));
            dt.Columns.Add(new System.Data.DataColumn("InstallerName1", typeof(String)));
            dt.Columns.Add(new System.Data.DataColumn("ProjectNumber1", typeof(String)));

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                string ProjectNumber = dt.Rows[i]["PRID"].ToString();
                string ProjectStatusId = dt.Rows[i]["projectStatusId"].ToString();
                dt.Rows[i]["ProjectNumber1"] = dt.Rows[i]["ProjectNumber"].ToString() + " / " + dt.Rows[i]["ProjectID"].ToString();


                if (!string.IsNullOrEmpty(ProjectNumber))
                {
                    if (ddlisdeduct.SelectedValue == "0")
                    {
                        SttblProjects objSolarMiner = ClstblProjects.tblProjects_SelectByProjectIDForSolarMiner(ProjectNumber);

                        SttblProjects objAriseSolar = ClstblProjects.tblProjects_SelectByProjectID(ProjectNumber);

                        if (objAriseSolar.ProjectStatus != "")
                        {
                            SttblProjects objPrjSts = ClstblProjects.tblProjects_SelectProjectStatusByProjectStatusID(ProjectStatusId);

                            dt.Rows[i]["ProjectStatus1"] = objPrjSts.ProjectStatus;
                            dt.Rows[i]["InstallerName1"] = objAriseSolar.InstallerName;
                        }
                        else if (objSolarMiner.ProjectStatus != "")
                        {
                            SttblProjects objPrjSts = ClstblProjects.tblProjects_SelectProjectStatusByProjectStatusIDForSolarMiner(ProjectStatusId);
                            dt.Rows[i]["ProjectStatus1"] = objPrjSts.ProjectStatus;
                            dt.Rows[i]["InstallerName1"] = objSolarMiner.InstallerName;
                        }
                        
                    }
                    else if (ddlisdeduct.SelectedValue == "3")
                    {
                        SttblProjects obj = ClstblProjects.tblProjects_SelectByProjectIDForSolarMiner(ProjectNumber);

                        SttblProjects objPrjSts = ClstblProjects.tblProjects_SelectProjectStatusByProjectStatusIDForSolarMiner(ProjectStatusId);

                        dt.Rows[i]["ProjectStatus1"] = objPrjSts.ProjectStatus;
                        dt.Rows[i]["InstallerName1"] = obj.InstallerName;
                    }
                    else if (ddlisdeduct.SelectedValue == "1")
                    {
                        SttblProjects obj = ClstblProjects.tblProjects_SelectByProjectID(ProjectNumber);

                        SttblProjects objPrjSts = ClstblProjects.tblProjects_SelectProjectStatusByProjectStatusID(ProjectStatusId);

                        dt.Rows[i]["ProjectStatus1"] = objPrjSts.ProjectStatus;
                        dt.Rows[i]["InstallerName1"] = obj.InstallerName;
                    }
                   
                }

                if(dt.Rows[i]["ProjectStatus"].ToString() != "")
                {
                    dt.Rows[i]["ProjectStatus1"] = dt.Rows[i]["ProjectStatus"].ToString();
                    dt.Rows[i]["InstallerName1"] = dt.Rows[i]["InstallerName"].ToString();
                }

                if (ddlisdeduct.SelectedValue == "2")
                {
                    dt.Rows[i]["ProjectStatus1"] = dt.Rows[i]["ProjectStatus"].ToString();
                    dt.Rows[i]["InstallerName1"] = dt.Rows[i]["InstallerName"].ToString();
                }

            }

        }
        catch (Exception ex)
        {

        }
        return dt;
    }

    public void BindDropDown()
    {
        lstSearchLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        lstSearchLocation.DataBind();


        //DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        //ddlStockCategoryID.DataSource = dtStockCategory;
        //ddlStockCategoryID.DataTextField = "StockCategory";
        //ddlStockCategoryID.DataValueField = "StockCategoryID";
        //ddlStockCategoryID.DataBind();
    }

    protected DataTable GetGridData1()
    {
        // For Multiple Selectio for Location //
        string selectedLocationItem = "";
        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedLocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedLocationItem != "")
        {
            selectedLocationItem = selectedLocationItem.Substring(1);
        }

        // For Multiple Selection for Project Status //
        string selectedProjectStatus = "";
        foreach (RepeaterItem item in lstProjecSts.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedProjectStatus += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedProjectStatus != "")
        {
            selectedProjectStatus = selectedProjectStatus.Substring(1);
        }

        string selectedSMInstaller = "";
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnSMInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedSMInstaller += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedSMInstaller != "")
        {
            selectedSMInstaller = selectedSMInstaller.Substring(1);
        }

        string selectedAInstaller = "";
        foreach (RepeaterItem item in rptAInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnAInstallerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedAInstaller += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedSMInstaller != "")
        {
            selectedAInstaller = selectedAInstaller.Substring(1);
        }

        string selectedWCustomer = "";
        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnWCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedWCustomer += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedSMInstaller != "")
        {
            selectedWCustomer = selectedWCustomer.Substring(1);
        }

        DataTable dt1;

        if (ddlisdeduct.SelectedItem.Text == "Solar Miner Project")
        {
            dt1 = ClstblProjects.tblProjects_SelectWarehousePickListWise1ForStockDeductedReport("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, selectedLocationItem, ddlDeductedOrNot.SelectedValue, ddlisdeduct.SelectedValue, selectedSMInstaller, selectedWCustomer, txtStockItem.Text, selectedProjectStatus);
        }
        else
        {

            dt1 = ClstblProjects.tblProjects_SelectWarehousePickListWise1("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, selectedLocationItem, ddlDeductedOrNot.SelectedValue, ddlisdeduct.SelectedValue, selectedAInstaller, selectedWCustomer, txtStockItem.Text, selectedProjectStatus);
        }
        return dt1;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    // int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }


            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
        // bind();

        int SaleQtyPanel = 0;
        int SaleQtyInverter = 0;
        int totSaleQtyPanel = 0;
        int totSaleQtyInverter = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            SaleQtyPanel = Convert.ToInt32(dt.Rows[i]["PanelStockDeducted"]);
            SaleQtyInverter = Convert.ToInt32(dt.Rows[i]["InverterStockDeducted"]);

            totSaleQtyPanel += SaleQtyPanel;
            totSaleQtyInverter += SaleQtyInverter;
        }
        lblTotalPanels.Text = totSaleQtyPanel.ToString();
        lblTotalInverters.Text = totSaleQtyInverter.ToString();

        //lblTotalPanels.Text = totSaleQtyPanel.ToString();
        //lblTotalInverters.Text = totSaleQtyInverter.ToString();

    }

    public void bind()
    {
        //DataTable dt = new DataTable();
        //dt = GetGridData1();


    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewrevertpanel")
        {
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0]; // projectnumber or Invoice number
            string Projectid = arg[1];  //projectid or wholesaleid
            DataTable dt = null;
            DataTable dt1 = null;
            int exists1 = ClstblProjects.tbl_PickListLog_SelectbyprojectId(Projectid);
            int exists = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectbyID(Projectid);

            if (exists1 == 1)
            {

                dt = ClstblProjects.tbl_PickListLog_getpicklistId(Projectid);
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                int rowIndex = gvr.RowIndex;
                string PicklistId = dt.Rows[0]["Id"].ToString();
                string Cat_name = (GridView1.Rows[rowIndex].FindControl("lblpanelQty") as Label).Text;

                hndDifference.Value = Cat_name;


                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }



                if (ProjectNo != "0" && PicklistId != "0")
                {


                    if (ddlisdeduct.SelectedValue == "0")
                    {
                        DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "1", "", "", ProjectNo, "", "", PicklistId);

                        if (dt2.Rows.Count != 0)
                        {
                            Repeater1.DataSource = dt2;
                            Repeater1.DataBind();
                        }
                        else
                        {
                            Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_newSolarMiner("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                            Repeater1.DataBind();
                        }
                    }
                    // For AriseSolar //
                    if (ddlisdeduct.SelectedValue == "1")
                    {
                        Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                        Repeater1.DataBind();
                    }
                    else if (ddlisdeduct.SelectedValue == "3")
                    {
                        // For SolarMiner //
                        Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_newSolarMiner("1", "", "1", "", "", ProjectNo, "", "", PicklistId);
                        Repeater1.DataBind();
                    }


                }


                chkisactive.Checked = false;

                ModalPopupExtenderRevert.Show();


            }

            else if (exists == 1)
            {
                txtwholesaleprojNo.Text = "";
                string WholesaleOrderID = Projectid.ToString();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                int rowIndex = gvr.RowIndex;

                string Cat_name = (GridView1.Rows[rowIndex].FindControl("lblpanelQty") as Label).Text;
                hndDifference1.Value = Cat_name;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive1.Visible = false;
                }
                Repeater2.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("3", "", "1", "", "", WholesaleOrderID, "", "", "");
                Repeater2.DataBind();

                chkisactive1.Checked = false;
                ModalPopupExtender1.Show();

            }
            else
            {
                Notification("No Data Found");

            }


        }

        if (e.CommandName == "viewrevertinverter")
        {
            txtprojectno.Text = "";
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0]; // projectnumber or Invoice number
            string Projectid = arg[1];  //projectid or wholesaleid
            DataTable dt = null;
            DataTable dt1 = null;
            int exists1 = ClstblProjects.tbl_PickListLog_SelectbyprojectId(Projectid);
            int exists = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectbyID(Projectid);

            if (exists1 == 1)
            {
                dt = ClstblProjects.tbl_PickListLog_getpicklistId(Projectid);
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                int rowIndex = gvr.RowIndex;
                string PicklistId = dt.Rows[0]["Id"].ToString();
                string Cat_name = (GridView1.Rows[rowIndex].FindControl("lblinverterQty") as Label).Text;

                hndDifference.Value = Cat_name;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive.Visible = false;
                }

                if (ProjectNo != "0" && PicklistId != "0")
                {

                    // All //
                    if (ddlisdeduct.SelectedValue == "0")
                    {
                        DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "2", "", "", ProjectNo, "", "", PicklistId);

                        if (dt2.Rows.Count != 0)
                        {
                            Repeater1.DataSource = dt2;
                            Repeater1.DataBind();
                        }
                        else
                        {
                            Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_newSolarMiner("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                            Repeater1.DataBind();
                        }
                    }
                    // Arise Solar //
                    else if (ddlisdeduct.SelectedValue == "1")
                    {
                        Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                        Repeater1.DataBind();
                    }
                    else if (ddlisdeduct.SelectedValue == "3")
                    {
                        // Solar Miner //
                        Repeater1.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_newSolarMiner("1", "", "2", "", "", ProjectNo, "", "", PicklistId);
                        Repeater1.DataBind();
                    }

                }
                chkisactive.Checked = false;
                ModalPopupExtenderRevert.Show();


            }
            else if (exists == 1)
            {
                txtwholesaleprojNo.Text = "";
                string WholesaleOrderID = Projectid.ToString();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                int rowIndex = gvr.RowIndex;

                string Cat_name = (GridView1.Rows[rowIndex].FindControl("lblinverterQty") as Label).Text;
                hndDifference1.Value = Cat_name;
                if (!Roles.IsUserInRole("Administrator"))
                {
                    chkisactive1.Visible = false;
                }
                Repeater2.DataSource = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly_new("3", "", "2", "", "", WholesaleOrderID, "", "", "");
                Repeater2.DataBind();

                chkisactive1.Checked = false;
                ModalPopupExtender1.Show();

            }
            else
            {
                Notification("No Data Found");

            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        ddllocationsearch.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        //ddlinstallername.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        //lstlocationsearch.SelectedIndex = -1;
        //lstProjectStatus.SelectedIndex = -1;

        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        HideCheckBox();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        try
        {
            Export oExport = new Export();
            string FileName = "DailyStockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            // ddlSelectRecords.SelectedValue = "All"; 

            //DataTable Dt2 = getGridInfo(dt);

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                               .Select(x => x.ColumnName)
                               .ToArray();

            if (ddlisdeduct.SelectedValue == "0")   //All
            {
                int[] ColList = { 1, 18, 2, 13, 6, 4, 3, 5, 11, 12, 7, 8 };
                string[] arrHeader = { "Project No.", "CompanyName", "Project", "Project Status", "InstallBookingDate", "Installer Name", "Details", "Store Name", "Panels", "Inverter", "Deduct", "Deduct By" };
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
            else if (ddlisdeduct.SelectedValue == "1") // Arise
            {
                int[] ColList = { 2, 14, 3, 4, 9, 7, 6, 8, 12, 13, 10, 11 };
                string[] arrHeader = { "Project No.", "CompanyName", "Project", "Project Status", "InstallBookingDate", "Installer Name", "Details", "Store Name", "Panels", "Inverter", "Deduct", "Deduct By" };
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
            else if (ddlisdeduct.SelectedValue == "3") // SolarMiner
            {
                int[] ColList = { 1, 16, 2, 4, 9, 5, 7, 8, 14, 15, 10, 11 };
                string[] arrHeader = { "Project No.", "CompanyName", "Project", "Project Status", "InstallBookingDate", "Installer Name", "Details", "Store Name", "Panels", "Inverter", "Deduct", "Deduct By" };
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
            else if (ddlisdeduct.SelectedValue == "2") // Wholesale
            {
                int[] ColList = { 1, 17, 2, 13, 6, 4, 3, 5, 11, 12, 7, 8 };
                string[] arrHeader = { "Project No.", "CompanyName", "Project", "Project Status", "InstallBookingDate", "Installer Name", "Details", "Store Name", "Panels", "Inverter", "Deduct", "Deduct By" };
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }

        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlSearchDate.SelectedValue = "2";
        ddlSearchState.SelectedValue = "";
        ddlDeductedOrNot.SelectedValue = "True";
        ddlisdeduct.SelectedValue = "0";
        //ddlSearchVendor.ClearSelection();
        //ddlinstallername.ClearSelection();
        ddlStockCategoryID.SelectedValue = "";
        //ddlStockItem.SelectedValue = "";
        txtStockItem.Text = "";
        ddlprojectstatus.SelectedValue = "";
        lstProjectStatus.ClearSelection();
        //lstlocationsearch.ClearSelection();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {

            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch.SelectedValue = CompanyLocationID;
            ddllocationsearch.Enabled = false;
        }

        foreach (RepeaterItem item in lstSearchLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        HideCheckBox();
        ClearCheckBox();
        BindGrid(0); ;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
        }
    }

    protected void chkisactive_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference");

            if (chkisactive.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void chkDifference_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        //MsgError("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtenderRevert.Show();
    }

    protected void txtprojectno_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid.Value, Categorynm, txtprojectno.Text);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number is already exists.");

        }
        if (!string.IsNullOrEmpty(txtprojectno.Text))
        {
            TextBox1.Text = string.Empty;
            txtprojectno.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox1.Text = string.Empty;
        }
        if (!string.IsNullOrEmpty(TextBox1.Text))
        {
            txtprojectno.Text = string.Empty;
            TextBox1.Focus();
        }
        ModalPopupExtenderRevert.Show();
    }

    protected void lnksubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        int i = 0;
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");

        //    if (chkDifference.Checked == true)
        //    {
        //        //chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        //chkDifference.Checked = false;
        //    }

        //}
        //if (i > Convert.ToInt32(hndDifference.Value))
        //{
        //    ModalPopupExtenderRevert.Show();
        //    SetError();
        //}
        //else
        //{
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            HiddenField rpthndProjectid = (HiddenField)item.FindControl("rpthndProjectid");
            HiddenField rpthndPicklistId = (HiddenField)item.FindControl("rpthndPicklistId");
            if (chkDifference.Checked == true)
            {

                if ((!string.IsNullOrEmpty(txtprojectno.Text)) || (!string.IsNullOrEmpty(TextBox1.Text)))
                {
                    DataTable dt = null;
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid");
                    if (!string.IsNullOrEmpty(txtprojectno.Text))
                    {
                        dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(txtprojectno.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string projectid = dt.Rows[0]["ProjectID"].ToString();
                            string PicklistId = dt.Rows[0]["ID"].ToString();
                            string LocationId = dt.Rows[0]["LocationId"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                            section = "Stock Deduct";
                            Message = "Stock Deduct For Project No:" + txtprojectno.Text + "& PicklistId:" + PicklistId + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblProjects.tbl_picklistlog_UpdateData(projectid, rpthndPicklistId.Value, "2", date.ToString(), userid);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");

                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            //    ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}

                        }
                    }
                    if (!string.IsNullOrEmpty(TextBox1.Text))
                    {
                        dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(TextBox1.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                            section = "Stock Deduct";
                            Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value, dt.Rows[0]["CompanyLocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Stock Revert For Project No:" + rpthndProjectid.Value + "& PicklistId:" + rpthndPicklistId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndPicklistId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblrevertItem.tbl_WholesaleOrders_UpdateData(WholesaleOrderID, "2", userid, date.ToString());
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference.Value))
                            //{
                            ClstblProjects.tbl_PickListLog_Update_Ispartialflag(rpthndPicklistId.Value);
                            //}
                            //section = "Wholesale Revert";
                            //Message = "Stock Add For WholesaleId:" + rpthndWholesaleOrderId.Value + "By administrator";
                            //ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                        }
                    }
                    DataTable dtexist = ClstblProjects.tblStockSerialNo_Select_ByProjectIdandPicklistid(rpthndProjectid.Value, rpthndPicklistId.Value);
                    if (dtexist.Rows.Count == 0)
                    {
                        ClstblProjects.tbl_picklistlog_UpdateData(rpthndProjectid.Value, rpthndPicklistId.Value, "1", "", "");
                    }
                }
            }
        }
        // }
        BindGrid(0);
    }

    protected void chkisactive1_CheckedChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference1");

            if (chkisactive1.Checked == true)
            {
                chksalestagrep.Checked = true;
            }
            else
            {
                chksalestagrep.Checked = false;
            }
        }
        ModalPopupExtender1.Show();
    }

    protected void chkDifference1_CheckedChanged(object sender, EventArgs e)
    {
        //int i = 0;
        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");

        //    if (chkDifference.Checked == true)
        //    {
        //        chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        chkDifference.Checked = false;
        //    }
        //    if (i > Convert.ToInt32(hndDifference1.Value))
        //    {
        //        chkDifference.Enabled = false;
        //        chkDifference.Checked = false;
        //        //MsgError("Can't check more than" + hndDifference.Value + "CheckBox");
        //        // break;
        //    }
        //}
        //ModalPopupExtender1.Show();

    }

    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string ProjectNumber = TextBox2.Text;
        DataTable dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(ProjectNumber);
        string locationid = dt.Rows[0]["LocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hnditemid1");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                int exist = ClstblProjects.Exist_ItemId_tblproject(hnditemid1.Value, Categorynm, TextBox2.Text);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            TextBox2.Text = string.Empty;
            TextBox2.Focus();
            //Notification("Record with this model number already exists.");
        }

        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }

        if (!string.IsNullOrEmpty(TextBox2.Text))
        {
            txtwholesaleprojNo.Text = string.Empty;
            TextBox2.Focus();
        }
        ModalPopupExtender1.Show();
    }

    protected void txtwholesaleprojNo_TextChanged(object sender, EventArgs e)
    {
        int fail = 0;
        int suc = 0;
        int fail1 = 0;

        string InvoiceNumber = txtwholesaleprojNo.Text;
        DataTable dt1 = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(InvoiceNumber);
        string locationid = dt1.Rows[0]["CompanyLocationId"].ToString();

        if (string.IsNullOrEmpty(locationid))
        {
            fail1++;
        }

        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            HiddenField hnditemid1 = (HiddenField)item.FindControl("hnditemid1");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            Label CategoryName = (Label)item.FindControl("lblCategory");
            string Categorynm = "";
            if (CategoryName.Text == "Modules")
            {
                Categorynm = "1";
            }
            else
            {
                Categorynm = "2";
            }
            if (chkDifference.Checked == true)
            {
                DataTable dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                int exist = ClstblProjects.Exist_ItemId_WholesaleOrderID(hnditemid1.Value, Categorynm, WholesaleOrderID);
                if (exist == 0)
                {
                    suc++;
                }
                else
                {
                    fail++;
                }
            }
        }
        if (fail > 0)
        {
            txtwholesaleprojNo.Text = string.Empty;
        }

        if (fail1 > 0)
        {
            txtprojectno.Text = string.Empty;
            txtprojectno.Focus();
            Notification("Record with this number location is null.");

        }

        if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
        {
            TextBox2.Text = string.Empty;
            txtwholesaleprojNo.Focus();
        }

        ModalPopupExtender1.Show();
    }

    protected void lnkwholeSaleSubmit_Click(object sender, EventArgs e)
    {
        string section = "";
        string Message = "";
        DateTime date = DateTime.Now;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        int i = 0;
        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");

        //    if (chkDifference.Checked == true)
        //    {
        //        //chkDifference.Checked = true;
        //        i++;
        //    }
        //    else
        //    {
        //        //chkDifference.Checked = false;
        //    }

        //}
        //if (i > Convert.ToInt32(hndDifference1.Value))
        //{

        //    SetError();
        //    ModalPopupExtender1.Show();
        //    // break;
        //}
        //else
        //{
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chkDifference = (CheckBox)item.FindControl("chkDifference1");
            Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
            HiddenField rpthndWholesaleOrderId = (HiddenField)item.FindControl("rpthndWholesaleOrderId");
            HiddenField hndInvoiceNo = (HiddenField)item.FindControl("hndInvoiceNo");
            if (chkDifference.Checked == true)
            {

                if ((!string.IsNullOrEmpty(txtwholesaleprojNo.Text)) || (!string.IsNullOrEmpty(TextBox2.Text)))
                {
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hnditemid = (HiddenField)item.FindControl("hnditemid1");
                    DataTable dt = null;
                    if (!string.IsNullOrEmpty(txtwholesaleprojNo.Text))
                    {
                        dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                        if (dt.Rows.Count > 0)
                        {
                            string WholesaleOrderID = dt.Rows[0]["WholesaleOrderID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_WholesaleOrderID(WholesaleOrderID, lblSerialNo.Text);
                            section = "Stock Deduct";
                            Message = "Stock Deduct For WholesaleOrderID:" + WholesaleOrderID + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, WholesaleOrderID, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblrevertItem.tbl_WholesaleOrders_UpdateData(txtwholesaleprojNo.Text, "2", userid, date.ToString());
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference1.Value))
                            //{
                            ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                            //}
                        }
                    }
                    if (!string.IsNullOrEmpty(TextBox2.Text))
                    {
                        //dt = ClstblrevertItem.tbl_WholesaleOrders_GetwholesaleorderIDByInvoiceNo(txtwholesaleprojNo.Text);
                        dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                        if (dt.Rows.Count > 0)
                        {
                            //dt = ClstblProjects.tblProjects_GetProjectIdByProjectNumnber(TextBox2.Text);
                            string projectid = dt.Rows[0]["ProjectID"].ToString();
                            string PicklistId = dt.Rows[0]["ID"].ToString();
                            ClstblStockSerialNo.Update_tblStockSerialNo_ProjectIdandPickListId(projectid, PicklistId, lblSerialNo.Text);

                            section = "Stock Deduct";
                            Message = "Stock Deduct For Project No:" + txtprojectno.Text + "& PicklistId:" + PicklistId + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, PicklistId, lblSerialNo.Text, section, Message, date);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hnditemid.Value.ToString(), dt.Rows[0]["LocationId"].ToString(), "1");

                            section = "Revert Item";
                            Message = "Wholesale revert for Invoice No:" + hndInvoiceNo.Value + " and Order Number:" + rpthndWholesaleOrderId.Value + "By administrator";
                            ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, rpthndWholesaleOrderId.Value, lblSerialNo.Text, section, Message, date);
                            ClstblProjects.tbl_picklistlog_UpdateData(projectid, PicklistId, "2", date.ToString(), userid);
                            ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(hnditemid.Value, hdnStockLocationID.Value, "1");
                            //if (i == Convert.ToInt32(hndDifference1.Value))
                            //{
                            ClstblrevertItem.tbl_WholesaleOrders_Update_Ispartialflag(rpthndWholesaleOrderId.Value);
                            //}
                        }
                    }
                    DataTable dtexist = Clstbl_WholesaleOrders.tblStockSerialNo_Select_ByWholesaleOrderID(rpthndWholesaleOrderId.Value);
                    if (dtexist.Rows.Count == 0)
                    {
                        ClstblrevertItem.tbl_WholesaleOrders_UpdateData(rpthndWholesaleOrderId.Value, "1", "", "");
                    }
                }
            }
        }
        BindGrid(0);
        // }


    }

    protected void ddlisdeduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideCheckBox();
        ClearCheckBox();
        BindGrid(0);
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  ddlStockItem.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Text = "Select Item";
        item1.Value = "";
        // ddlStockItem.Items.Add(item1);

        DataTable dtStockItem = ClstblStockItems.tblStockItems_SelectbyAsc(ddlStockCategoryID.SelectedValue.ToString());
        //ddlStockItem.DataSource = dtStockItem;
        //ddlStockItem.DataTextField = "StockItem";
        //ddlStockItem.DataValueField = "StockItemID";
        //ddlStockItem.DataBind();
    }

    protected void lnkWholeSalerevert_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = string.Empty;
        List<string> SerialNoArrayList = new List<string>();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        int counttick = 0;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        foreach (RepeaterItem item in Repeater2.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference1");
            HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
            HiddenField hdnWholesaleOrderID = (HiddenField)item.FindControl("rpthndWholesaleOrderId");
            if (chksalestagrep.Checked == true)
            {
                counttick++;
                SerialNoArrayList.Add(hdnSerialNo.Value);
                WholesaleOrderID = hdnWholesaleOrderID.Value;
            }
        }
        var SerialNoList = String.Join(",", SerialNoArrayList);
        if (counttick > 0)
        {
            if (!string.IsNullOrEmpty(WholesaleOrderID))
            {
                Sttbl_WholesaleOrders stWholesaleID = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
                DataTable dt3 = Reports.ViewRevertedStockItems_GroupBy_OnlyDirectItems("", WholesaleOrderID, "", SerialNoList.ToString());
                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        bool suc = ClsProjectSale.tblStockItems_RevertStock(dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), dt3.Rows[i]["StockLocationID"].ToString());
                        //int suc1 = ClstblrevertItem.tblRevertItem_InsertData(stproj.ProjectNumber, ProjectID, dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), PicklistID);
                    }
                }

                DataTable dt = Reports.ViewRevertedStockItems_GroupByStockItem("", WholesaleOrderID, "", SerialNoList.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString());
                        int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString(), stOldQty.StockQuantity, dt.Rows[i]["QTY"].ToString(), userid, dt.Rows[i]["WholesaleOrderID"].ToString(), "", "");
                        Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    }
                }

                foreach (RepeaterItem item in Repeater2.Items)
                {
                    CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference1");

                    //HiddenField hdnDirectLogIn = (HiddenField)item.FindControl("hdnDirectLogIn");
                    HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
                    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hnditemid1");
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hdnWholesaleOrderID = (HiddenField)item.FindControl("rpthndWholesaleOrderId");
                    //HiddenField hdnSRid = (HiddenField)item.FindControl("hdnSRid");

                    if (chksalestagrep.Checked == true)
                    {

                        bool success = ClstblStockSerialNo.tblStockSerialNo_Revertall_ByWholesaleIDSerialNo(hdnWholesaleOrderID.Value, hdnSerialNo.Value);
                        //if (hdnDirectLogIn.Value == "1")
                        //{
                        string Section = "Wholesale revert";
                        string Message = "Wholesale revert for Invoice No:" + stWholesaleID.InvoiceNo + " and Order Number:" + WholesaleOrderID;
                        ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, hdnWholesaleOrderID.Value, hdnSerialNo.Value, Section, Message, Currendate);
                        ClstblrevertItem.tbl_StockRevertItems_DeleteBySerialNo(hdnSerialNo.Value);
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hdnStockItemID.Value, hdnStockLocationID.Value, "1");
                        //DataTable dtrid = ClstblrevertItem.tbl_StockRevertItems_SelectDataBySRid(hdnSRid.Value);
                        //if (dtrid.Rows.Count == 0)
                        //{
                        //    ClstblrevertItem.tbl_StockRevert_DeleteByid(hdnSRid.Value);
                        //}
                        //}
                    }
                }

                DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("3", "", "", "", "", WholesaleOrderID, "", "", "");
                if (dt2.Rows.Count == 0)
                {
                    //it changes deduct flag if no serial number is available
                    bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsDeduct_Date_User(WholesaleOrderID, "0", "", "");
                    bool success2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(WholesaleOrderID, "false");
                }
            }

        }
        else
        {

        }

    }

    protected void lnkRevert_Click(object sender, EventArgs e)
    {
        string PicklistID = string.Empty;
        string ProjectID = string.Empty;
        List<string> SerialNoArrayList = new List<string>();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        int counttick = 0;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        foreach (RepeaterItem item in Repeater1.Items)
        {
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference");
            HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
            HiddenField hdnProjectID = (HiddenField)item.FindControl("rpthndProjectid");
            HiddenField hdnPicklistid = (HiddenField)item.FindControl("rpthndPicklistId");
            if (chksalestagrep.Checked == true)
            {
                counttick++;
                SerialNoArrayList.Add(hdnSerialNo.Value);
                PicklistID = hdnPicklistid.Value;
                ProjectID = hdnProjectID.Value;
            }
        }
        var SerialNoList = String.Join(",", SerialNoArrayList);
        if (counttick > 0)
        {
            if (!string.IsNullOrEmpty(ProjectID) && !string.IsNullOrEmpty(PicklistID))
            {
                SttblProjects stproj = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                DataTable dt3 = Reports.ViewRevertedStockItems_GroupBy_OnlyDirectItems(PicklistID, "", "", SerialNoList.ToString());
                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        bool suc = ClsProjectSale.tblStockItems_RevertStock(dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), dt3.Rows[i]["StockLocationID"].ToString());
                        int suc1 = ClstblrevertItem.tblRevertItem_InsertData(stproj.ProjectNumber, ProjectID, dt3.Rows[i]["StockItemID"].ToString(), dt3.Rows[i]["QTY"].ToString(), PicklistID);
                    }
                }

                DataTable dt = Reports.ViewRevertedStockItems_GroupByStockItem(PicklistID, "", "", SerialNoList.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString());
                        int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", dt.Rows[i]["StockItemID"].ToString(), dt.Rows[i]["StockLocationID"].ToString(), stOldQty.StockQuantity, dt.Rows[i]["QTY"].ToString(), userid, dt.Rows[i]["ProjectID"].ToString());
                        ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    }
                }

                foreach (RepeaterItem item in Repeater1.Items)
                {
                    CheckBox chksalestagrep = (CheckBox)item.FindControl("chkDifference");

                    //HiddenField hdnDirectLogIn = (HiddenField)item.FindControl("hdnDirectLogIn");
                    HiddenField hdnSerialNo = (HiddenField)item.FindControl("hdnSerialNo");
                    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hnditemid");
                    HiddenField hdnStockLocationID = (HiddenField)item.FindControl("hdnStockLocationID");
                    HiddenField hdnProjectID = (HiddenField)item.FindControl("rpthndProjectid");
                    HiddenField hdnPicklistid = (HiddenField)item.FindControl("rpthndPicklistId");
                    //HiddenField hdnSRid = (HiddenField)item.FindControl("hdnSRid");

                    if (chksalestagrep.Checked == true)
                    {

                        bool success = ClstblStockSerialNo.tblStockSerialNo_Revertall_ByPickListIDSerialNo(hdnPicklistid.Value, hdnSerialNo.Value);
                        //if (hdnDirectLogIn.Value == "1")
                        //{                       
                        string Section = "Revert Item";
                        string Message = "Stock Revert For Project No:" + stproj.ProjectNumber + " & PicklistId:" + PicklistID;
                        ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, hdnPicklistid.Value, hdnSerialNo.Value, Section, Message, Currendate);
                        ClstblrevertItem.tbl_StockRevertItems_DeleteBySerialNo(hdnSerialNo.Value);
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(hdnStockItemID.Value, hdnStockLocationID.Value, "1");
                        //    DataTable dtrid = ClstblrevertItem.tbl_StockRevertItems_SelectDataBySRid(hdnSRid.Value);
                        //    if (dtrid.Rows.Count == 0)
                        //    {
                        //        ClstblrevertItem.tbl_StockRevert_DeleteByid(hdnSRid.Value);
                        //    }
                        //}
                    }
                }




                DataTable dt2 = Reports.SerialNumberWiseReport_ByProjNoNotRevertedOnly("1", "", "", "", "", stproj.ProjectNumber, "", "", PicklistID);
                if (dt2.Rows.Count == 0)
                {
                    //it changes deduct flag if no serial number is available
                    ClstblProjects.tblProjects_tbl_PicklistLog_RevertIsDeductStatus(ProjectID, PicklistID, "False", "", "");
                }
            }

        }
        else
        {
            //ModalPopupExtenderConfirmRevert.Hide();
            //ModalPopupRevertedItem.Show();
        }
        //BindGrid(0);
    }

    protected void ddlProjectType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void lstSearchLocation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void lstProjecSts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    public void HideCheckBox()
    {
        if (ddlisdeduct.SelectedValue == "0")    //All
        {
            DivProjectStutus.Visible = false;
            DivAInstaller.Visible = false;
            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;
        }
        else if (ddlisdeduct.SelectedValue == "1")    //Arise Project
        {
            DivProjectStutus.Visible = true;
            DivAInstaller.Visible = true;

            DivSMInstaller.Visible = false;
            DivWCustomer.Visible = false;

            BindAriseSolarProjectStatus();
        }
        else if (ddlisdeduct.SelectedValue == "3")    //SolarMiner Project
        {
            DivProjectStutus.Visible = true;
            DivSMInstaller.Visible = true;

            DivAInstaller.Visible = false;
            DivWCustomer.Visible = false;

            BindSolarMinerProjectStatus();
        }
        else if (ddlisdeduct.SelectedValue == "2")    //Wholesale
        {
            DivWCustomer.Visible = true;

            DivProjectStutus.Visible = false;
            DivAInstaller.Visible = false;
            DivSMInstaller.Visible = false;
        }
    }

    public void BindCheckBox()
    {
        rptAInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        rptAInstaller.DataBind();

        rptSMInstaller.DataSource = ClstblContacts.tblContacts_SelectInverterSolarMiner();
        rptSMInstaller.DataBind();

        rptWCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptWCustomer.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptSMInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptAInstaller.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptWCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    public void BindAriseSolarProjectStatus()
    {
        lstProjecSts.DataSource = null;
        lstProjecSts.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstProjecSts.DataBind();
    }

    public void BindSolarMinerProjectStatus()
    {
        lstProjecSts.DataSource = null;
        lstProjecSts.DataSource = ClstblProjectStatus.tblProjectStatusSolarMiner_SelectActive();
        lstProjecSts.DataBind();
    }

    protected void btnUpdateSMData_Click(object sender, EventArgs e)
    {
        int UpdateStatus = ClsDbData.USP_InsertUpdate_SM_tblProjectStatus();
        int UpdateProNo = ClsDbData.USP_InsertUpdate_SM_tblProjectNos();
    }
}