﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_OldRemoveJobs : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropDown();
            BindStatus();
            BindInstallerCustomer();

            BindGrid(0);
        }
    }

    public void BindGridColumn()
    {
        int totalCount = GridView1.Columns.Count - 2;

        if (ddlDateType.SelectedValue == "5")
        {
            for (int i = 0; i < totalCount - 5; i++)
            {
                GridView1.Columns[i].Visible = false;
            }
            for (int i = totalCount - 5; i < totalCount; i++)
            {
                GridView1.Columns[i].Visible = true;
            }
        }
        else
        {
            for (int i = totalCount - 5; i < totalCount; i++)
            {
                GridView1.Columns[i].Visible = false;
            }
            for (int i = 0; i < totalCount - 5; i++)
            {
                GridView1.Columns[i].Visible = true;
            }
        }
    }


    public void BindDropDown()
    {
        DataTable dtLoaction = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dtLoaction;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();


    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlCompany.SelectedValue = "1";
        txtProjectNumber.Text = string.Empty;
        ddlLocation.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        ddlYesNo.SelectedValue = "";
        ddlProjectStatus.SelectedValue = "";
        ddlOldRemoveSystem.SelectedValue = "";
        ddlSearchStatus.SelectedValue = "";

        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    #region Msg/Script Function
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MyfunManulMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void Notification(string msg)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
        }
        catch (Exception e)
        {

        }
    }
    #endregion

    #region GridView Comman Method
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "submit")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');
            string projectNo = arg[0];
            string editFlag = arg[1];
            string UpdatedOn = System.DateTime.Now.AddHours(15).ToString();

            bool update = false;
            if (projectNo != "")
            {
                update = ClsReportsV2.SP_OldRemovejobs_Update_oldPanelSubmitFlag(projectNo, editFlag, UpdatedOn, ddlCompany.SelectedValue);
            }

            if (update)
            {
                Notification("Transaction Successfull..");
                BindGrid(0);
            }
        }

        if (e.CommandName.ToLower() == "notes")
        {
            string projectNo = e.CommandArgument.ToString();
            hndProjectNo.Value = projectNo;
            hndMode.Value = "";
            txtNotes.Text = string.Empty;
            ddlStock.ClearSelection();
            txtNoPanels.Text = string.Empty;

            BindNotes(projectNo, ddlCompany.SelectedValue);
            ModalPopupExtenderNewNote.Show();
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            #region Bind Header Name

            #endregion
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            //String SMStockItemID = rowView["SMStockItemID"].ToString();


        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    #endregion

    #region Bind Grid Method
    protected DataTable GetGridData1()
    {
        DataTable dt = ClsReportsV2.SP_OldRemovejobs(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlProjectStatus.SelectedValue, ddlInstaller.SelectedValue, ddlLocation.SelectedValue, ddlYesNo.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlOldRemoveSystem.SelectedValue, ddlSearchStatus.SelectedValue);

        return dt;
    }

    public void BindTotal(DataTable dt)
    {
        string Panels = dt.Compute("SUM(RemovePanels)", string.Empty).ToString();
        lblTotalPanels.Text = Panels;

        string INPanels = dt.Compute("SUM(RemovePanels)", "oldPanelSubmitFlag='1'").ToString();
        lblINTotalPanels.Text = INPanels;

        //string OutPanels = dt.Compute("AVG(NoOfPanel)", string.Empty).ToString();
        string OutPanels = dt.Rows[0]["NoOfPanel"].ToString();
        lblOUTTotalPanels.Text = OutPanels;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            BindGridColumn();
            PanGrid.Visible = true;
            divtot.Visible = true;
            panel.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
        }
    }
    #endregion

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindInstallerCustomer();
        BindStatus();
    }

    protected void BindInstallerCustomer()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Customer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Customer";
            ddlInstaller.DataValueField = "CustomerID";
            ddlInstaller.DataBind();
        }
        else
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();
        }

        ddlSaveInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlSaveInstaller.DataTextField = "Contact";
        ddlSaveInstaller.DataValueField = "ContactID";
        ddlSaveInstaller.DataBind();
    }

    protected void BindStatus()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Job Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "JobStatusType";
            ddlProjectStatus.DataValueField = "Id";
            ddlProjectStatus.DataBind();
        }
        else
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Project Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = ClstblProjectStatus.tblProjectStatus_SelectActive();
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "ProjectStatus";
            ddlProjectStatus.DataValueField = "ProjectStatusID";
            ddlProjectStatus.DataBind();
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "Old Remove Jobs " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            
            if (ddlDateType.SelectedValue == "5")
            {
                int[] ColList = { 15, 16, 17, 18, 19 };
                string[] arrHeader = { "No Of Panel", "Name", "Panel Brand", "Amount", "Receipt No" };

                //only change file extension to .xls for excel file
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
            else
            {
                int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                string[] arrHeader = { "Project No", "Project Status", "Location", "Customer", "Installer", "InstallBookingDate", "System Details", "Actule Panel", "Remove Old System", "Status", "Used Panel", "Notes" };

                //only change file extension to .xls for excel file
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnSaveNotes_Click(object sender, EventArgs e)
    {
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string notes = txtNotes.Text;
        string companyId = ddlCompany.SelectedValue;
        string projectNo = hndProjectNo.Value;
        string stockOn = ddlStock.SelectedValue;
        string NoOfPanels = txtNoPanels.Text;

        if (hndMode.Value == "Update")
        {
            bool update = ClsReportsV2.tbl_RemoveOldSystemNotes_Update(hndID.Value, notes, stockOn, NoOfPanels);
            if (update)
            {
                SetAdd1();
                txtNotes.Text = string.Empty;
                ddlStock.ClearSelection();
                txtNoPanels.Text = string.Empty;
                hndMode.Value = "";
            }
        }
        else
        {
            int id = ClsReportsV2.tbl_RemoveOldSystemNotes_Insert(companyId, projectNo, createdBy, createdOn);
            if (id > 0)
            {
                bool update = ClsReportsV2.tbl_RemoveOldSystemNotes_Update(id.ToString(), notes, stockOn, NoOfPanels);

                SetAdd1();
                txtNotes.Text = string.Empty;
                ddlStock.ClearSelection();
                txtNoPanels.Text = string.Empty;
            }
        }

        BindNotes(projectNo, companyId);
        ModalPopupExtenderNewNote.Show();
        BindGrid(0);
    }

    public void BindNotes(string ProjectID, string CompanyID)
    {
        string userId = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClsReportsV2.tbl_RemoveOldSystemNotes_GetDataByProjectNo(ProjectID, CompanyID, userId);
        if (dt.Rows.Count > 0)
        {
            DivNotes.Visible = true;
            RptNewNotes.DataSource = dt;
            RptNewNotes.DataBind();
        }
        else
        {
            DivNotes.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivNotes.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            //DivNotes.Attributes.Add("style", "");
            DivNotes.Attributes.Add("style", "overflow-x: scroll");
        }
    }

    protected void RptNewNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditNotes")
        {
            try
            {
                DataTable dt = ClsReportsV2.tbl_RemoveOldSystemNotes_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    txtNotes.Text = "";
                    ddlStock.ClearSelection();
                    txtNoPanels.Text = string.Empty;
                    hndID.Value = ID;
                    txtNotes.Text = dt.Rows[0]["Notes"].ToString();
                    txtNoPanels.Text = dt.Rows[0]["NoOfPanels"].ToString();
                    try
                    {
                        ddlStock.SelectedValue = dt.Rows[0]["StockOn"].ToString();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }

        if (e.CommandName == "DeleteNotes")
        {
            try
            {
                bool del = ClsReportsV2.tbl_RemoveOldSystemNotes_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
                string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindNotes(hndProjectNo.Value, ddlCompany.SelectedValue);
                ModalPopupExtenderNewNote.Show();
                BindGrid(0);
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        ModalPopupExtenderNewNote.Show();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Reset();
        HidePanels();
        BindGrid(0);
    }

    public void InitAdd()
    {
        HidePanels();
        Reset();
        PanGrid.Visible = false;
        panel.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        lnkSold.Visible = false;
        PanAddUpdate.Visible = true;

        btnSave.Visible = true;
        btnUpdate.Visible = false;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        HidePanels();
        Reset();
        PanGrid.Visible = false;
        panel.Visible = false;
        Panel4.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        lnkSold.Visible = false;
        PanAddUpdate.Visible = true;

        btnSave.Visible = false;
        btnUpdate.Visible = true;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        lnkSold.Visible = true;
        PanAddUpdate.Visible = false;
        PanAddUpdateSold.Visible = false;
        Panel4.Visible = true;
    }

    public void Reset()
    {
        ddlAddCompany.ClearSelection();
        txtProjectNo.Text = string.Empty;
        ddlRemoveOldSystem.ClearSelection();
        txtPanels.Text = string.Empty;
        txtModel.Text = string.Empty;
        txtWatts.Text = string.Empty;
        txtNoOfPanels.Text = "0";
        txtSystemCapacity.Text = string.Empty;
        ddlSaveInstaller.ClearSelection();
    }

    protected void lnkReset_Click(object sender, EventArgs e)
    {
        InitAdd();
        Reset();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string companyId = ddlAddCompany.SelectedValue;
        string projectNo = txtProjectNo.Text;
        string removeOldSystem = ddlRemoveOldSystem.SelectedValue;
        string panelBrand = txtPanels.Text;
        string model = txtModel.Text;
        string watts = txtWatts.Text;
        string noOfPanels = txtNoOfPanels.Text;
        string systemCapacity = txtSystemCapacity.Text;
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string InstallerId = ddlSaveInstaller.SelectedValue;

        string existsMsg = ClsReportsV2.RemoveOldSystem_Exists(companyId, projectNo);
        if (existsMsg == "0")
        {
            int id = ClsReportsV2.tbl_RemoveOldSystem_Insert(createdBy, createdOn);
            if (id > 0)
            {
                bool update = ClsReportsV2.tbl_RemoveOldSystem_Update(id.ToString(), companyId, projectNo, removeOldSystem, panelBrand, model, watts, noOfPanels, systemCapacity);
                bool update1 = ClsReportsV2.tbl_RemoveOldSystem_Update_InstallerId(id.ToString(), InstallerId);

                SetAdd1();
                HidePanels();
                Reset();
                BindGrid(0);
            }
        }
        else
        {
            InitAdd();
            Notification(existsMsg);
        }

    }

    protected void gvbtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;

        string[] arg = lbtn.CommandArgument.Split(';');

        string id = arg[0];
        string EditFlag = arg[1];

        if (EditFlag != "2")
        {
            DataTable dt = ClsReportsV2.tbl_RemoveOldSystem_SelectByID(id);
            InitUpdate();
            hndProjectId.Value = id;
            if (dt.Rows.Count > 0)
            {
                ddlAddCompany.SelectedValue = dt.Rows[0]["CompanyId"].ToString();
                txtProjectNo.Text = dt.Rows[0]["ProjectNo"].ToString();
                ddlRemoveOldSystem.SelectedValue = dt.Rows[0]["RemoveOldSystem"].ToString();
                txtPanels.Text = dt.Rows[0]["PanelBrand"].ToString();
                txtModel.Text = dt.Rows[0]["Model"].ToString();
                txtWatts.Text = dt.Rows[0]["Watts"].ToString();
                txtNoOfPanels.Text = dt.Rows[0]["NoOfPanels"].ToString();
                txtSystemCapacity.Text = dt.Rows[0]["SystemCapacity"].ToString();
            }
        }
        else if (EditFlag == "2")
        {
            DataTable dt = ClsReportsV2.tbl_RemoveOldSystemSold_SelectByID(id);

            ResetSold();
            InitSold();
            lblAddUpdateSold.Text = "Update";
            lnkSoldSave.Visible = false;
            lnkSoldUpdate.Visible = true;
            hndProjectIdSold.Value = id;
            if (dt.Rows.Count > 0)
            {
                txtSoldNoOfPanels.Text = dt.Rows[0]["NoOfPanel"].ToString();
                txtSoldName.Text = dt.Rows[0]["Name"].ToString();
                txtSoldPanelBrand.Text = dt.Rows[0]["PanelBrand"].ToString();
                txtSoldAmount.Text = dt.Rows[0]["Amount"].ToString();
                txtSoldReceiptNo.Text = dt.Rows[0]["ReceiptNo"].ToString();
            }
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string companyId = ddlAddCompany.SelectedValue;
        string projectNo = txtProjectNo.Text;
        string removeOldSystem = ddlRemoveOldSystem.SelectedValue;
        string panelBrand = txtPanels.Text;
        string model = txtModel.Text;
        string watts = txtWatts.Text;
        string noOfPanels = txtNoOfPanels.Text;
        string systemCapacity = txtSystemCapacity.Text;

        string id = hndProjectId.Value;

        string existsMsg = ClsReportsV2.RemoveOldSystem_ExistsEdit(companyId, projectNo, id);
        if (existsMsg == "0")
        {
            bool update = ClsReportsV2.tbl_RemoveOldSystem_Update(id, companyId, projectNo, removeOldSystem, panelBrand, model, watts, noOfPanels, systemCapacity);

            SetAdd1();
            HidePanels();
            Reset();
            BindGrid(0);
        }
        else
        {
            InitAdd();
            Notification(existsMsg);
        }
    }

    protected void lnkSold_Click(object sender, EventArgs e)
    {
        ResetSold();
        InitSold();
        lblAddUpdateSold.Text = "Add";
        lnkSoldSave.Visible = true;
        lnkSoldUpdate.Visible = false;
    }

    public void InitSold()
    {
        PanGrid.Visible = false;
        lnkAdd.Visible = false;
        lnkSold.Visible = false;
        PanAddUpdate.Visible = false;
        Panel4.Visible = false;
        panel.Visible = false;

        lnkBack.Visible = true;
        PanAddUpdateSold.Visible = true;
    }

    public void ResetSold()
    {
        txtSoldNoOfPanels.Text = string.Empty;
        txtSoldName.Text = string.Empty;
        txtSoldPanelBrand.Text = string.Empty;
        txtSoldAmount.Text = string.Empty;
        txtSoldReceiptNo.Text = string.Empty;
    }

    protected void lnkSoldReset_Click(object sender, EventArgs e)
    {
        InitSold();
        ResetSold();
    }

    protected void lnkSoldSave_Click(object sender, EventArgs e)
    {
        string noOfPanel = txtSoldNoOfPanels.Text;
        string name = txtSoldName.Text;
        string panelBrand = txtSoldPanelBrand.Text;
        string amount = txtSoldAmount.Text;
        string receiptNo = txtSoldReceiptNo.Text;
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();

        int id = ClsReportsV2.tbl_RemoveOldSystemSold_Insert(createdBy, createdOn);
        if (id > 0)
        {
            bool update = ClsReportsV2.tbl_RemoveOldSystemSold_Update(id.ToString(), noOfPanel, name, panelBrand, amount, receiptNo);

            SetAdd1();
            HidePanels();
            Reset();
            BindGrid(0);
        }
        else
        {
            SetError1();
        }
    }

    protected void lnkSoldUpdate_Click(object sender, EventArgs e)
    {
        string noOfPanel = txtSoldNoOfPanels.Text;
        string name = txtSoldName.Text;
        string panelBrand = txtSoldPanelBrand.Text;
        string amount = txtSoldAmount.Text;
        string receiptNo = txtSoldReceiptNo.Text;

        string id = hndProjectIdSold.Value;

        bool update = ClsReportsV2.tbl_RemoveOldSystemSold_Update(id, noOfPanel, name, panelBrand, amount, receiptNo);

        if(update)
        {
            SetAdd1();
        }
        else
        {
            SetError1();
        }
        
        HidePanels();
        ResetSold();
        BindGrid(0);
    }

    protected void gvbtnDelete_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;

        string id = lbtn.CommandArgument;

        bool Del = ClsReportsV2.tbl_RemoveOldSystemSold_DeleteByID(id);

        if(Del)
        {
            SetAdd1();
            BindGrid(0);
        }
        else
        {
            SetError1();
        }
    }
}