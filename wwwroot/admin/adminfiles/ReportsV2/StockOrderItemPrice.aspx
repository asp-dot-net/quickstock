﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockOrderItemPrice.aspx.cs"
    Inherits="admin_adminfiles_ReportsV2_StockOrderItemPrice"
     MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style>
                .modal-dialog1 {
                    margin-left: -300px;
                    margin-right: -300px;
                    width: 985px;
                }

                .focusred {
                    border-color: #FF5F5F !important;
                }

                .padd_btm10 {
                    padding-bottom: 15px;
                }

                .height100 {
                    height: 100px;
                }

                .autocomplete_completionListElement {
                    z-index: 9999999 !important;
                }
            </style>

            <script type="text/javascript">


                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();

                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //alert("dgfdg2");


                    $(".AriseInstaller .dropdown dt a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
                    });
                    $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").hide();
                    });

                    $(".Location .dropdown dt a").on('click', function () {
                        $(".Location .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Location .dropdown dd ul li a").on('click', function () {
                        $(".Location .dropdown dd ul").hide();
                    });

                }
                function pageLoaded() {

                    callMultiCheckbox1();

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true,
                    });

                    $(".myvalinvoiceissued").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});
                    //$(".myval").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();


                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                }


                function stopRKey(evt) {
                    var evt = (evt) ? evt : ((event) ? event : null);
                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
                }
                document.onkeypress = stopRKey;

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
            </script>

            <script>

                $(document).ready(function () {

                });
                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            // alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
            </script>

            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <script>
                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddProjectStatus.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }

            </script>
            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Order Item Price
                        <div class="pull-right">
                        </div>
                    </h5>
                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlVender" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" >
                                                            <asp:ListItem Value="">Vender</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    
                                                    <div class="input-group col-sm-3 max_width170">
                                                        <asp:DropDownList ID="ddlPurchaseCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" >
                                                            <asp:ListItem Value="">Company</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="form-group spical multiselect AriseInstaller martop5 col-sm-2 max_width170 specail1_select" id="DivAriseInstaller" runat="server" visible="false">
                                                        <dl class="dropdown ">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida" id="spanselect">
                                                                        <asp:Label ID="lblProjectStatus" runat="server" /></span>
                                                                    <p class="multiSel"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddProjectStatus" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="rptProjectStatus" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnProjectStatus" runat="server" Value='<%# Eval("ProjectStatus") %>' />
                                                                                    <asp:HiddenField ID="hdnProjectStatusID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />


                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <%-- </span>--%>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltProjectStatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:TextBox ID="txtStockItem" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtStockItem"
                                                            WatermarkText="Stock Item/Model" />
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlCurrency" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Currency</asp:ListItem>
                                                            <asp:ListItem Value="1">USD</asp:ListItem>
                                                            <asp:ListItem Value="2">AUD</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Stock Ordered</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
                            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            
            <div class="finalgrid">
                 <asp:Panel ID="panel" runat="server" >                  
                    <div class="page-header card" id="DivTotal" runat="server">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Avg Price:&nbsp;</b><asp:Literal ID="lblAvgPrice" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="StockOrderItemID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Order No" SortExpression="StockOrderID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStockOrderID" runat="server" Text='<%#Eval("StockOrderID")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Stock Item" SortExpression="StockItem">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStockItem" runat="server" Text='<%#Eval("StockItem")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <%--<asp:TemplateField HeaderText="Stock Model" SortExpression="StockModel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStockModel" runat="server" Text='<%#Eval("StockModel")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                
                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompanyLocation" runat="server" Text='<%#Eval("CompanyLocation")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Pur. Company" SortExpression="PurchaseCompanyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPurCompany" runat="server" Text='<%#Eval("PurchaseCompanyName")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Vendor" SortExpression="Vendor">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVendor" runat="server" Text='<%#Eval("Vendor")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Currency" SortExpression="Currency">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("Currency")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Unit Price" SortExpression="UnitPrice">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text='<%#Eval("UnitPrice")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Quantity" SortExpression="OrderQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrderQuantity" runat="server" Text='<%#Eval("OrderQuantity")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Order Date" SortExpression="DateOrdered">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDateOrdered" runat="server" Text='<%#Eval("DateOrdered")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypDetail" runat="server" CausesValidation="false"  
                                                             CssClass="btn btn-primary btn-mini" Target="_blank" >
                                                            <i class="fa fa-link"></i>Detail
                                                            </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>--%>

                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

        </ContentTemplate>
        <Triggers>
           
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {
            HighlightControlToValidate();


        });



        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }

        // For Multi Select //
        $(".AriseInstaller .dropdown dt a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").hide();
        });

        $(".Location .dropdown dt a").on('click', function () {
            $(".Location .dropdown dd ul").slideToggle('fast');
        });
        $(".Location .dropdown dd ul li a").on('click', function () {
            $(".Location .dropdown dd ul").hide();
        });



        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>
</asp:Content>