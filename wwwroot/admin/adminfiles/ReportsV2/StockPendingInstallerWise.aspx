﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockPendingInstallerWise.aspx.cs" 
    Inherits="admin_adminfiles_ReportsV2_StockPendingInstallerWise" 
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style>
                .modal-dialog1 {
                    margin-left: -300px;
                    margin-right: -300px;
                    width: 985px;
                }

                .focusred {
                    border-color: #FF5F5F !important;
                }

                .padd_btm10 {
                    padding-bottom: 15px;
                }

                .height100 {
                    height: 100px;
                }

                .autocomplete_completionListElement {
                    z-index: 9999999 !important;
                }
            </style>

            <script type="text/javascript">


                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();

                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //alert("dgfdg2");


                    $(".AriseInstaller .dropdown dt a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
                    });
                    $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").hide();
                    });

                    $(".Location .dropdown dt a").on('click', function () {
                        $(".Location .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Location .dropdown dd ul li a").on('click', function () {
                        $(".Location .dropdown dd ul").hide();
                    });

                }
                function pageLoaded() {

                    callMultiCheckbox3();
                    callMultiCheckbox1();

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true,
                    });

                    $(".myvalinvoiceissued").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});
                    //$(".myval").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox3();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                }


                function stopRKey(evt) {
                    var evt = (evt) ? evt : ((event) ? event : null);
                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
                }
                document.onkeypress = stopRKey;

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
            </script>

            <script>

                $(document).ready(function () {

                });
                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            // alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
            </script>

            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <script>
               
                function callMultiCheckbox3() {
                    var title = "";
                    $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel2').show();
                        $('.multiSel2').html(html);
                        $(".hida2").hide();
                    }
                    else {
                        $('#spanselect2').show();
                        $('.multiSel2').hide();
                    }

                }

                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddProjectStatus.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }

            </script>
            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Pending Report - Installer Wise
                        <div class="pull-right">
                        </div>
                    </h5>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">

                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" >
                                                            <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                            <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                            <asp:ListItem Value="3">Wholesale</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers, Custom" ValidChars="," />
                                                    </div>

                                                    <%--<div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlProjectStatus" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Project Status</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>--%>

                                                    <div class="form-group spical multiselect AriseInstaller martop5 col-sm-2 max_width170 specail1_select" id="DivAriseInstaller" runat="server">
                                                        <dl class="dropdown ">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida" id="spanselect">
                                                                        <asp:Label ID="lblProjectStatus" runat="server" /></span>
                                                                    <p class="multiSel"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddProjectStatus" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="rptProjectStatus" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnProjectStatus" runat="server" Value='<%# Eval("ProjectStatus") %>' />
                                                                                    <asp:HiddenField ID="hdnProjectStatusID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />


                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <%-- </span>--%>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltProjectStatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:TextBox ID="txtStockItem" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtStockItem"
                                                            WatermarkText="Stock Item/Model" />
                                                    </div>

                                                     <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="form-group spical multiselect Location martop5 col-sm-2 max_width170 specail1_select" id="DivLocation" runat="server" visible="false">
                                                        <dl class="dropdown ">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida2" id="spanselect2">Location</span>
                                                                    <p class="multiSel2"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddLocation" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="rptLocation" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                    <asp:HiddenField ID="hdnLocationID" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    
                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlIsDifference" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" 
                                                            class="myval" >
                                                            <asp:ListItem Value="">Is Difference</asp:ListItem>
                                                            <asp:ListItem Value="1">Diff-Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">Diff-No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170" runat="server">
                                                        <asp:DropDownList ID="ddlAudit" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Is Audit</asp:ListItem>
                                                            <asp:ListItem Value="1">Audit-Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">Audit-No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    
                                                    <div class="input-group col-sm-1 max_width170" runat="server">
                                                        <asp:DropDownList ID="ddlInstallation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="0">Installation</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
                            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div>
                <div class="card shadownone brdrgray" id="divtot" runat="server">
                    <div class="card-block">
                        <div class="table-responsive BlockStructure">
                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"></th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Out</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Installed</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Difference</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Revert</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Audit</th>
                                    </tr>
                                    <tr class="brd_ornge">
                                        <td align="left" valign="top">Total</td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypOut" runat="server" Target="_blank"
                                              />
                                        </td>
                                        <td align="left" valign="top">
                                                <asp:HyperLink ID="HypInstalled" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:Literal ID="lblPDifference" runat="server"></asp:Literal></td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypRevert" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:Literal ID="lblPAudit" runat="server"></asp:Literal></td>

                                        <%--<td align="left" valign="top">
                                                            <asp:Literal ID="lblTotal" runat="server"></asp:Literal></td>--%>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField SortExpression="Contact">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderName" runat="server" >
                                                        </asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label11" runat="server" data-toggle="tooltip" data-placement="Top" title=""
                                                            data-original-title='<%#Eval("Contact")%>'>
                                                           <%#Eval("Contact")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Out" SortExpression="StockDeducted">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="Label452" runat="server" Width="30px" Text='<%#Eval("StockDeducted")%>'>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink ID="lblStockDeducted" runat="server" Text='<%#Eval("StockDeducted")%>' Enabled='<%# Eval("StockDeducted").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Installed" SortExpression="SaleQty">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="Label52" runat="server" Width="30px" Text='<%#Eval("SaleQty")%>'>
                                                            
                                                        </asp:Label>--%>
                                                        <asp:HyperLink ID="lblSaleQty" runat="server" Text='<%#Eval("SaleQty")%>' Enabled='<%# Eval("SaleQty").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Difference" SortExpression="Diff">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label82" runat="server" Text='<%#Eval("Diff")%>'>                                                                                                                                                          
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Revert" SortExpression="StockRevert">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblpanelrevert1" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("Revert"))%>'>
                                                        </asp:Label>--%>
                                                        <asp:HyperLink ID="lblRevert" runat="server" Text='<%#Eval("StockRevert")%>' Enabled='<%# Eval("StockRevert").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Audit" SortExpression="Audit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpanelAudit" runat="server" Text='<%# Eval("Audit") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypDetail" runat="server" CausesValidation="false"  
                                                             CssClass="btn btn-primary btn-mini" Target="_blank" 
                                                            NavigateUrl='<%# "~/admin/adminfiles/ReportsV2/Details/StockPendingInstallerWiseDetails.aspx?Contact=" + Eval("Contact") 
                                                                + "&ContactID=" + Eval("ContactID") + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" 
                                                                + "&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&StockItemModel=" + txtStockItem.Text 
                                                                + "&PS=" + GetProjectStatus() + "&Category=" + ddlCategory.SelectedValue + "&InstallationDate=" + ddlInstallation.SelectedValue 
                                                                + "&CompanyLocation=" + ddlLocation.SelectedValue %>' >
                                                            <i class="fa fa-link"></i>Detail
                                                            </asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

        </ContentTemplate>
        <Triggers>
           
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {
            HighlightControlToValidate();


        });



        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }

        // For Multi Select //
        $(".AriseInstaller .dropdown dt a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").hide();
        });

        $(".Location .dropdown dt a").on('click', function () {
            $(".Location .dropdown dd ul").slideToggle('fast');
        });
        $(".Location .dropdown dd ul li a").on('click', function () {
            $(".Location .dropdown dd ul").hide();
        });



        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>
</asp:Content>




