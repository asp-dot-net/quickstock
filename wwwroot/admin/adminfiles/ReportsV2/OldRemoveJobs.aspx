﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OldRemoveJobs.aspx.cs" Inherits="admin_adminfiles_ReportsV2_OldRemoveJobs"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .height100 {
            height: 100px;
        }

        .table tbody .brd_ornge td {
            border-bottom: 4px solid #ff784f;
        }

        .sbtn {
            background-color: #53a93f !important;
            border-color: #53a93f !important;
            color: #fff;
        }

        .Withvalidation {
            display: flex;
        }
    </style>
    <style>
        .btn_right {
            float: right;
        }

            .btn_right li {
                float: left;
                padding-left: 15px;
                font-size: 12px;
                font-weight: 400;
            }

                .btn_right li div {
                    max-width: 140px;
                    line-height: 15px;
                }

        .font_Custom {
            font-size: 15px;
        }

        .padd_btm10 {
            padding-bottom: 15px;
        }
    </style>

    <script type="text/javascript">

        function doMyAction() {

           <%-- $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();
            });--%>
        }

        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            $(".Location .dropdown dt a").on('click', function () {
                $(".Location .dropdown dd ul").slideToggle('fast');
            });
            $(".Location .dropdown dd ul li a").on('click', function () {
                $(".Location .dropdown dd ul").hide();
            });

            //callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");

            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

        }

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }

    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }

    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <div class="page-header card">
                <div class="card-block">
                    <h5>Old Remove Jobs
                        <div id="hbreadcrumb" class="pull-right">
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" CausesValidation="false" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                            <asp:LinkButton ID="lnkSold" runat="server" OnClick="lnkSold_Click" CausesValidation="false" CssClass="btn btn-primary purple btnaddicon"> Sold</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                        </div>
                    </h5>
                </div>
            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <div class="searchfinal">
                        <div class="card shadownone brdrgray pad10">
                            <div class="card-block">
                                <asp:Panel ID="PanSerach" runat="server" DefaultButton="btnSearch">
                                    <div class="inlineblock martop5">
                                        <div class="row">
                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                    <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                    <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:TextBox ID="txtProjectNumber" placeholder="ProjectNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                    WatermarkText="ProjectNo" />
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlProjectStatus" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Project Status</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Installer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Submit</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlOldRemoveSystem" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Remove Old System</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                    <asp:ListItem Value="">Status</asp:ListItem>
                                                    <asp:ListItem Value="1">Delivered to Warehouse</asp:ListItem>
                                                    <asp:ListItem Value="2">Dont Want</asp:ListItem>
                                                    <asp:ListItem Value="3">With Installer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-1 max_width170">
                                                <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Date</asp:ListItem>
                                                    <asp:ListItem Value="1">Deposit Received</asp:ListItem>
                                                    <asp:ListItem Value="2">Active Date</asp:ListItem>
                                                    <asp:ListItem Value="3">InstallBooking Date</asp:ListItem>
                                                    <asp:ListItem Value="4">Submit Date</asp:ListItem>
                                                    <asp:ListItem Value="5">Sold Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                    CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                            </div>
                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>

                                <div class="datashowbox inlineblock">
                                    <div class="row">

                                        <div class="input-group col-sm-2 martop5 max_width170">
                                            <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="excel export" class="btn btn-success btn-xs excel fullwidth"
                                                CausesValidation="false" Style="background-color: #218838; border-color: #218838" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> excel </asp:LinkButton>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Item
                                    <asp:HiddenField runat="server" ID="hndProjectId" />
                                </h5>
                            </div>
                            <div class="card-block padleft25">
                                <div class="form-horizontal row martop5">
                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Company
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:DropDownList ID="ddlAddCompany" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" Width="190px"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                    <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                    <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlAddCompany" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="lblProjectNumber">
                                                Project No
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtProjectNo" runat="server" CssClass="form-control" placeholder="Project No." Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtProjectNo" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                    ControlToValidate="txtProjectNo" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Remove Old System
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:DropDownList ID="ddlRemoveOldSystem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" Width="190px"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                    <%--<asp:ListItem Value="4">Solar Bridge</asp:ListItem>--%>
                                                </asp:DropDownList>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlRemoveOldSystem" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Panels Brand
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtPanels" runat="server" CssClass="form-control" placeholder="Panels" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                    ControlToValidate="txtPanels" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Model
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control" placeholder="Model" Width="200px"></asp:TextBox>
                                                <%--&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                    ControlToValidate="txtModel" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Watts/Panels
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtWatts" runat="server" CssClass="form-control" placeholder="Watts/Panels" Width="200px"></asp:TextBox>
                                                <%--&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic"
                                                    ControlToValidate="txtWatts" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2" style="margin-top: 10px">
                                        <span class="name disblock">
                                            <label>
                                                No of Panels
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtNoOfPanels" Text="0" runat="server" CssClass="form-control" placeholder="No of Panels" Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNoOfPanels" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic" InitialValue="0"
                                                    ControlToValidate="txtNoOfPanels" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2" style="margin-top: 10px">
                                        <span class="name disblock">
                                            <label>
                                                System Capacity
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSystemCapacity" runat="server" CssClass="form-control" placeholder="System Capacity" Width="200px"></asp:TextBox>
                                                <%--&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic"
                                                    ControlToValidate="txtWatts" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2" style="margin-top: 10px">
                                        <span class="name disblock">
                                            <label>
                                                Installer
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                               <asp:DropDownList ID="ddlSaveInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Installer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <%-- <div class="form-horizontal row" style="margin-top: 15px!important">
                                    <div class="col-sm-12">
                                        <span class="name disblock">
                                            <label>
                                                Notes
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="TextBox1" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                    placeholder="Notes" Height="100px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                    FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                </cc1:FilteredTextBoxExtender>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                    FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </span>
                                    </div>
                                </div>--%>

                                <div class="form-group row" style="margin-top: 15px!important">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-xs sbtn" ValidationGroup="Req" OnClick="btnSave_Click">
                                        <i class="fa fa-save"></i> Save
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="btnUpdate" Visible="false" runat="server" CssClass="btn btn-xs sbtn" ValidationGroup="Req" OnClick="btnUpdate_Click">
                                         <i class="fa fa-save"></i> Update
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lnkReset" runat="server" data-placement="left" OnClick="lnkReset_Click"
                                            CausesValidation="false" CssClass="btn btn-primary btnclear"><i class="fa-refresh fa"></i>Reset</asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="PanAddUpdateSold" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdateSold" runat="server" Text=""></asp:Label>
                                    Sold Item
                                    <asp:HiddenField runat="server" ID="hndProjectIdSold" />
                                </h5>
                            </div>
                            <div class="card-block padleft25">
                                <div class="form-horizontal row martop5">
                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="Label1">
                                                No Of Panels
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSoldNoOfPanels" runat="server" CssClass="form-control" placeholder="No Of Panels" Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSoldNoOfPanels" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="dynamic"
                                                    ControlToValidate="txtSoldNoOfPanels" ErrorMessage="*" ValidationGroup="ReqSold" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="Label2">
                                                Name
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSoldName" runat="server" CssClass="form-control" placeholder="Name" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="dynamic"
                                                    ControlToValidate="txtSoldName" ErrorMessage="*" ValidationGroup="ReqSold" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="Label3">
                                                Panel Brand
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSoldPanelBrand" runat="server" CssClass="form-control" placeholder="Panel Brand" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="dynamic"
                                                    ControlToValidate="txtSoldPanelBrand" ErrorMessage="*" ValidationGroup="ReqSold" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="Label4">
                                                Amount
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSoldAmount" runat="server" CssClass="form-control" placeholder="Amount" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="dynamic"
                                                    ControlToValidate="txtSoldAmount" ErrorMessage="*" ValidationGroup="ReqSold" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="Label5">
                                                Receipt No
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate Withvalidation">
                                                <asp:TextBox ID="txtSoldReceiptNo" runat="server" CssClass="form-control" placeholder="Receipt No" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="dynamic"
                                                    ControlToValidate="txtSoldReceiptNo" ErrorMessage="*" ValidationGroup="ReqSold" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                    </div>

                                </div>

                                <div class="form-group row" style="margin-top: 15px!important">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:LinkButton ID="lnkSoldSave" runat="server" CssClass="btn btn-xs sbtn" ValidationGroup="ReqSold" OnClick="lnkSoldSave_Click">
                                        <i class="fa fa-save"></i> Save
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lnkSoldUpdate" Visible="false" runat="server" CssClass="btn btn-xs sbtn" ValidationGroup="ReqSold" OnClick="lnkSoldUpdate_Click">
                                         <i class="fa fa-save"></i> Update
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lnkSoldReset" runat="server" data-placement="left" OnClick="lnkSoldReset_Click"
                                            CausesValidation="false" CssClass="btn btn-primary btnclear"><i class="fa-refresh fa"></i>Reset</asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="finalgrid">
                    <asp:Panel ID="panel" runat="server">
                        <div class="page-header card" id="divtot" runat="server">
                            <div class="card-block brd_ornge">
                                <div class="printorder" style="font-size: medium">
                                    <b>Total Remove Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                    &nbsp;&nbsp;
                                    <b>Total IN Panels:&nbsp;</b><asp:Literal ID="lblINTotalPanels" runat="server"></asp:Literal>
                                    &nbsp;&nbsp;
                                    <b>Total OUT Panels:&nbsp;</b><asp:Literal ID="lblOUTTotalPanels" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="ProjectNumber" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                                AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectNumber").ToString() %>','tr<%# Eval("ProjectNumber").ToString() %>');">
                                                                <img id='imgdiv<%# Eval("ProjectNumber").ToString() %>' src="../../../images/icon_plus.png" />
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="ProjectNumber" HeaderText="ProjectNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProjectNo" runat="server" Text='<%#Eval("ProjectNumber")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="ProjectStatus" HeaderText="Project Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProjectStatus" runat="server" Text='<%#Eval("ProjectStatus")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="CompanyLocation" HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompanyLocation" runat="server" Text='<%#Eval("CompanyLocation")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="Customer" HeaderText="Customer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCustomer" runat="server" Text='<%#Eval("Customer")%>' Width="150px"
                                                                data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip" data-placement="top">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="Installer" HeaderText="Installer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInstaller" runat="server" Text='<%#Eval("Installer")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField SortExpression="InstallBookingDate" HeaderText="Booking Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInstallBookingDate" runat="server" Text='<%#Eval("InstallBookingDate")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Details" SortExpression="SystemDetails">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSystemDetails" runat="server" Text='<%#Eval("SystemDetails")%>' Width="200px"
                                                                data-original-title='<%#Eval("SystemDetails")%>' data-toggle="tooltip" data-placement="top">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Number of Panels installed on customer house"
                                                                SortExpression="NoOfPanels">Actual Panels</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNoOfPanels" runat="server" Text='<%#Eval("NoOfPanels")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Remove Old Sys" SortExpression="OldRemoveOldSystem">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOldRemoveOldSystem" runat="server" Text='<%#Eval("OldRemoveOldSystem")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Number of Panels return by installer from customer house"
                                                                SortExpression="RemovePanels">Used Panel</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRemovePanels" runat="server" Text='<%#Eval("RemovePanels")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="No Of Panel" SortExpression="NoOfPanel">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNoOfPanel" runat="server" Text='<%#Eval("NoOfPanel")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Name" SortExpression="Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Panel Brand" SortExpression="PanelBrand">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPanelBrand" runat="server" Text='<%#Eval("PanelBrand")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Receipt No" SortExpression="ReceiptNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReceiptNo" runat="server" Text='<%#Eval("ReceiptNo")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbkSubmitTrue" runat="server" CommandName="Submit" CausesValidation="false" CssClass="btn btn-primary btn-mini"
                                                                Style="color: white" Visible='<%# Eval("oldPanelSubmitFlag").ToString() != "0" && Eval("EditFlag").ToString() != "2" ? true : false %>' CommandArgument='<%# Eval("ProjectID") + ";" + Eval("EditFlag") %>'>
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Submit
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lbkSubmitFalse" runat="server" CommandName="Submit" CausesValidation="false" CssClass="btn btn-danger btn-mini"
                                                                Style="color: white" Visible='<%# Eval("oldPanelSubmitFlag").ToString() == "0" && Eval("EditFlag").ToString() != "2" ? true : false %>' CommandArgument='<%# Eval("ProjectID") + ";" + Eval("EditFlag") %>'>
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> Submit
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="gvbtnEdit" runat="server" CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                                Visible='<%# Eval("EditFlag").ToString() != "0" ? true : false %>' CommandArgument='<%# Eval("ProjectID")  + ";" + Eval("EditFlag") %>' OnClick="gvbtnEdit_Click">
                                                              <i class="fa fa-edit"></i> Edit
                                                            </asp:LinkButton>

                                                            <!--Start Add New Notes -->
                                                            <asp:LinkButton ID="gvlnkNotes" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Notes"
                                                                CommandArgument='<%#Eval("ProjectNumber") %>' CausesValidation="false" Visible='<%# Eval("EditFlag").ToString() != "2" ? true : false %>'>
                                                                            <i class="btn-label fa fa-edit"></i> Notes
                                                            </asp:LinkButton>
                                                            <!--End Add New Notes -->

                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" OnClick="gvbtnDelete_Click"
                                                                CommandArgument='<%# Eval("ProjectNumber") %>' Visible='<%# Roles.IsUserInRole("Administrator") && Eval("EditFlag").ToString() == "2" ? true : false %>'
                                                                CausesValidation="false"
                                                                OnClientClick="return confirm('Are you sure you want to delete?');">                                                                          
                                                                            <i class="btn-label fa fa-close"></i> Delete
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("ProjectNumber") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("ProjectNumber") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 50px;"><b>Notes</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>

                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <!-- Start New Notes Popup-->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderNewNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="ModelNewNote" TargetControlID="Button7"
                CancelControlID="LinkButton9">
            </cc1:ModalPopupExtender>
            <div id="ModelNewNote" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalNewNote">Project Note
                                <span style="float: right" class="printorder" />
                                <asp:LinkButton ID="LinkButton14" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <asp:HiddenField runat="server" ID="hndProjectNo" />
                                                <div class="input-group col-sm-6 padd_btm10">
                                                    <asp:DropDownList ID="ddlStock" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Delivered to Warehouse</asp:ListItem>
                                                        <asp:ListItem Value="2">Dont Want</asp:ListItem>
                                                        <asp:ListItem Value="3">With Installer</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-6 padd_btm10">
                                                    <asp:TextBox ID="txtNoPanels" runat="server" placeholder="No Of Panels" CssClass="form-control m-b"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hndMode" />
                                                <asp:HiddenField runat="server" ID="hndID" />
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="lbtnSaveNotes" CssClass="btn btn-info" CausesValidation="false" runat="server" Text="Save" OnClick="lbtnSaveNotes_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10" runat="server" id="DivNotes">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Stock</th>
                                                            <th>No Of Panel</th>
                                                            <th>Entered On</th>
                                                            <th>Entered By</th>
                                                            <th>Notes</th>
                                                            <th></th>
                                                        </tr>
                                                        <asp:Repeater ID="RptNewNotes" runat="server" OnItemCommand="RptNewNotes_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%# Container.ItemIndex + 1 %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("StockOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("NoOfPanels") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("CreatedOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("CreatedBy") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("Notes") %>
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                        <asp:LinkButton ID="lbtnEditNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="EditNotes" CommandArgument='<%#Eval("Id") %>'
                                                                            CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top" Visible='<%# Eval("EditFlag").ToString() == "1" || Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                                            <i class="btn-label fa fa-pencil"></i> Edit
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="DeleteNotes"
                                                                            CommandArgument='<%# Eval("Id") %>' Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'
                                                                            CausesValidation="false"
                                                                            OnClientClick="return confirm('Are you sure you want to delete?');">                                                                          
                                                                            <i class="btn-label fa fa-close"></i> Delete
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />
            <!--End New Notes Popup-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {

        }
    </script>

</asp:Content>
