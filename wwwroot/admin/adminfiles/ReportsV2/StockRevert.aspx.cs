﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockRevert : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropDown();
            BindInstallerCustomer();
            BindStatus();

            //ddlDate.SelectedValue = "1";
            txtStartDate.Text = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
            txtEndDate.Text = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");

            //txtStartDate.Text = "01/06/2021";
            //txtEndDate.Text = "30/06/2021";

            BindGrid(0);
            BindGrid1(0);
        }
    }

    public void BindDropDown()
    {
        DataTable dtLoaction = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dtLoaction;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindGrid1(0);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        //ddlCompany.SelectedValue = "1";
        txtProjectNumber.Text = string.Empty;
        ddlProjectStatus.SelectedValue = "";
        ddlLocation.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        //ddlDate.SelectedValue = "1";
        txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
        txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
        ddlEmployee.SelectedValue = "";
        ddlCategory.SelectedValue = "";

        BindGrid(0);
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

        try
        {
            string FileName = "";
            string[] arrHeader = null;
            if (ddlCompany.SelectedValue == "1")
            {
                FileName = "StockDeduct_Arise_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "ID", "Project", "Project Status", "Intaller", "Store Name", "Qty", "Revert On", "Revert By" };
            }
            else if (ddlCompany.SelectedValue == "2")
            {
                FileName = "StockDeduct_SolarMiner_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "ID", "Project", "Project Status", "Intaller", "Store Name", "Qty", "Revert On", "Revert By" };
            }
            else if (ddlCompany.SelectedValue == "3")
            {
                FileName = "StockDeduct_Wholesale" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Invoice No", "WholesaleID", "Delivery Option", "Job Status", "Customer", "Store Name", "Qty", "Revert On", "Revert By" };
            }
            else
            {
                FileName = "StockDeduct_All_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                arrHeader = new string[] { "Project No", "ID", "Project", "Project Status", "Intaller", "Store Name", "Qty", "Revert On", "Revert By" };
            }

            Export oExport = new Export();

            int[] ColList = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    #region Msg/Script Function
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MyfunManulMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void Notification(string msg)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
        }
        catch (Exception e)
        {

        }
    }
    #endregion

    #region GridView Comman Method
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "WClaim")
        {
            string [] arg = e.CommandArgument.ToString().Split(';');
            string projectNo = arg[0];
            string pickId = arg[1];
            
            string claimBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            string claimOn = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");

            DataTable dt = ClsReportsV2.Sp_StockRevert_DetailsByPickId(ddlCompany.SelectedValue, pickId, "1", txtStartDate.Text, txtEndDate.Text, ddlCategory.SelectedValue);

            int updateData = ClsReportsV2.tbl_WholesaleClaimSerialNo_BulkInsert(dt, claimBy, claimOn);

            if (updateData > 0)
            {
                Notification("SerialNo Claim.");
                BindGrid(0);
            }
        }
        
        if (e.CommandName == "WClaimRevert")
        {
            string [] arg = e.CommandArgument.ToString().Split(';');
            string projectNo = arg[0];
            string pickId = arg[1];
            
            DataTable dt = ClsReportsV2.Sp_StockRevert_DetailsByPickId(ddlCompany.SelectedValue, pickId, "1", txtStartDate.Text, txtEndDate.Text, ddlCategory.SelectedValue);

            bool SuccDelete = ClsReportsV2.tbl_WholesaleClaimSerialNo_BulkDelete(dt);

            if (SuccDelete)
            {
                Notification("Claim SerialNo Reverted.");
                BindGrid(0);
            }
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            #region Bind Header Name
            Label lbHeaderNo = (Label)e.Row.FindControl("lbHeaderNo");
            Label lbHeaderProject = (Label)e.Row.FindControl("lbHeaderProject");
            Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
            Label lblHeaderInstaller = (Label)e.Row.FindControl("lblHeaderInstaller");
            Label lblHeaderBooking = (Label)e.Row.FindControl("lblHeaderBooking");

            if (ddlCompany.SelectedValue == "3")
            {
                lbHeaderNo.Text = "Invoice No";
                lbHeaderProject.Text = "Delivery Option";
                lblHeaderStatus.Text = "Job Status";
                lblHeaderInstaller.Text = "Customer";
            }
            else
            {
                lbHeaderNo.Text = "Project No";
                lbHeaderProject.Text = "Project";
                lblHeaderStatus.Text = "Project Status";
                lblHeaderInstaller.Text = "Intaller";
            }
            #endregion
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            //String SMStockItemID = rowView["SMStockItemID"].ToString();


        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    #endregion

    #region Bind Grid Method
    protected DataTable GetGridData1()
    {
        DataTable dt1 = ClsReportsV2.Sp_StockRevert(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlProjectStatus.SelectedValue, ddlLocation.SelectedValue, ddlInstaller.SelectedValue, "1", txtStartDate.Text, txtEndDate.Text, ddlEmployee.SelectedValue, ddlCategory.SelectedValue);

        return dt1;
    }

    public void BindTotal(DataTable dt)
    {
        string Qty = dt.Compute("SUM(Qty)", string.Empty).ToString();
        lblQty.Text = Qty;
    }
    
    public void BindTotal1(DataTable dt)
    {
        string Qty = dt.Compute("SUM(Qty)", string.Empty).ToString();
        lblItemQty.Text = Qty;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
        }
    }
    #endregion

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindInstallerCustomer();
        BindStatus();
    }

    protected void BindInstallerCustomer()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Customer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Customer";
            ddlInstaller.DataValueField = "CustomerID";
            ddlInstaller.DataBind();

            divEmployee.Visible = true;
        }
        else
        {
            ddlInstaller.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Installer", Value = "" };
            ddlInstaller.Items.Add(listItem);

            DataTable dtInstaller = ClstblContacts.tblContacts_GetInstallers();
            ddlInstaller.DataSource = dtInstaller;
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataBind();

            divEmployee.Visible = false;
        }
    }

    protected void BindStatus()
    {
        if (ddlCompany.SelectedValue == "3")
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Job Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "JobStatusType";
            ddlProjectStatus.DataValueField = "Id";
            ddlProjectStatus.DataBind();
        }
        else
        {
            ddlProjectStatus.Items.Clear();
            ListItem listItem = new ListItem() { Text = "Project Status", Value = "" };
            ddlProjectStatus.Items.Add(listItem);

            DataTable dtStatus = ClstblProjectStatus.tblProjectStatus_SelectActive();
            ddlProjectStatus.DataSource = dtStatus;
            ddlProjectStatus.DataTextField = "ProjectStatus";
            ddlProjectStatus.DataValueField = "ProjectStatusID";
            ddlProjectStatus.DataBind();
        }
    }

    protected DataTable GetGridData_SerialNo()
    {
        DataTable dt1 = ClsReportsV2.Sp_StockRevert_SerialNo(ddlCompany.SelectedValue, txtProjectNumber.Text, ddlProjectStatus.SelectedValue, ddlLocation.SelectedValue, ddlInstaller.SelectedValue, "1", txtStartDate.Text, txtEndDate.Text, ddlEmployee.SelectedValue, ddlCategory.SelectedValue);

        return dt1;
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData1();
        DataTable dtSerialNo = GetGridData_SerialNo();
        DataTable dtItemWise = GetGridData2();
        //DataSet ds = new DataSet();

        dt.Columns["CompanyLocation"].ColumnName = "StoreName";

        string FileName = "";
        if (ddlCompany.SelectedValue == "1")
        {
            FileName = "StockDeduct_Arise_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        }
        else if (ddlCompany.SelectedValue == "2")
        {
            FileName = "StockDeduct_SolarMiner_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        }
        else if (ddlCompany.SelectedValue == "3")
        {
            FileName = "StockDeduct_Wholesale" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            dt.Columns["Projectnumber"].ColumnName = "Invoice No";
            dt.Columns["ID"].ColumnName = "WholesaleID";
            dt.Columns["Project"].ColumnName = "Delivery Option";
            dt.Columns["ProjectStatus"].ColumnName = "Job Status";
            dt.Columns["InstallerName"].ColumnName = "Customer";
        }
        else
        {
            FileName = "StockDeduct_All_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        }


        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Stock Revert");
                wb.Worksheets.Add(dtSerialNo, "SerialNo Detail");
                wb.Worksheets.Add(dtItemWise, "Item Wise");
                //wb.Worksheets.Add(dt);

                //string FileName = "SerialInstallerReportDetailProj" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
        catch (Exception ex)
        {
            Notification(ex.Message);
        }
    }

    #region Item Grid
    public void BindGrid1(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData2();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid1.Visible = false;
            divnopage1.Visible = false;
        }
        else
        {
            PanGrid1.Visible = true;
            GridView2.DataSource = dt;
            GridView2.DataBind();
            BindTotal1(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage1.Visible = false;
                }
                else
                {
                    divnopage1.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage1.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage1.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage1.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }
        }
    }

    protected DataTable GetGridData2()
    {
        string InstallerName = "";
        if(ddlInstaller.SelectedValue != "")
        {
            InstallerName = ddlInstaller.SelectedItem.Text;
        }
        DataTable dt1 = ClsReportsV2.Sp_StockRevertItemWise(ddlCompany.SelectedValue, InstallerName, "1", txtStartDate.Text, txtEndDate.Text, ddlCategory.SelectedValue, txtStockItem.Text, ddlLocation.SelectedValue);

        return dt1;
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData2();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView2.DataSource = sortedView;
        GridView2.DataBind();
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid1(0);
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        

        //BindGrid1(0);
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    #region Bind Header Name
        //    Label lbHeaderNo = (Label)e.Row.FindControl("lbHeaderNo");
        //    Label lbHeaderProject = (Label)e.Row.FindControl("lbHeaderProject");
        //    Label lblHeaderStatus = (Label)e.Row.FindControl("lblHeaderStatus");
        //    Label lblHeaderInstaller = (Label)e.Row.FindControl("lblHeaderInstaller");
        //    Label lblHeaderBooking = (Label)e.Row.FindControl("lblHeaderBooking");

        //    if (ddlCompany.SelectedValue == "3")
        //    {
        //        lbHeaderNo.Text = "Invoice No";
        //        lbHeaderProject.Text = "Delivery Option";
        //        lblHeaderStatus.Text = "Job Status";
        //        lblHeaderInstaller.Text = "Customer";
        //    }
        //    else
        //    {
        //        lbHeaderNo.Text = "Project No";
        //        lbHeaderProject.Text = "Project";
        //        lblHeaderStatus.Text = "Project Status";
        //        lblHeaderInstaller.Text = "Intaller";
        //    }
        //    #endregion
        //}

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            //String SMStockItemID = rowView["SMStockItemID"].ToString();


        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView2.PageIndex - 2;
            page[1] = GridView2.PageIndex - 1;
            page[2] = GridView2.PageIndex;
            page[3] = GridView2.PageIndex + 1;
            page[4] = GridView2.PageIndex + 2;
            page[5] = GridView2.PageIndex + 3;
            page[6] = GridView2.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView2.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView2.PageIndex == GridView2.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                int iTotalRecords = (dv.ToTable().Rows.Count);
                int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage1.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage1.Text = "";
            }
        }
    }

    protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command1);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command1);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command1);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command1);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command1);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command1);
        }
    }

    void lb_Command1(object sender, CommandEventArgs e)
    {
        GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid1(0);
    }
    #endregion
}