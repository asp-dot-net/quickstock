<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="installerwisereporttwo.aspx.cs" Inherits="admin_adminfiles_ReportsV2_installerwisereporttwo" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .padd_btm10 {
            padding-bottom: 15px;
        }

        .height100 {
            height: 100px;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            $(".selectlocation .dropdown dt a").on('click', function () {
                $(".selectlocation .dropdown dd ul").slideToggle('fast');
            });
            $(".selectlocation .dropdown dd ul li a").on('click', function () {
                $(".selectlocation .dropdown dd ul").hide();
            });

            $(".SMselectlocation .dropdown dt a").on('click', function () {
                $(".SMselectlocation .dropdown dd ul").slideToggle('fast');
            });
            $(".SMselectlocation .dropdown dd ul li a").on('click', function () {
                $(".SMselectlocation .dropdown dd ul").hide();
            });

            $(".Wselectlocation .dropdown dt a").on('click', function () {
                $(".Wselectlocation .dropdown dd ul").slideToggle('fast');
            });
            $(".Wselectlocation .dropdown dd ul li a").on('click', function () {
                $(".Wselectlocation .dropdown dd ul").hide();
            });

            $(".ProjectStatus .dropdown dt a").on('click', function () {
                $(".ProjectStatus .dropdown dd ul").slideToggle('fast');
            });
            $(".ProjectStatus .dropdown dd ul li a").on('click', function () {
                $(".ProjectStatus .dropdown dd ul").hide();
            });

            $(".AInstaller .dropdown dt a").on('click', function () {
                $(".AInstaller .dropdown dd ul").slideToggle('fast');
            });
            $(".AInstaller .dropdown dd ul li a").on('click', function () {
                $(".AInstaller .dropdown dd ul").hide();
            });

            $(".WStatus .dropdown dt a").on('click', function () {
                $(".WStatus .dropdown dd ul").slideToggle('fast');
            });
            $(".WStatus .dropdown dd ul li a").on('click', function () {
                $(".WStatus .dropdown dd ul").hide();
            });

            $(".WCustomer .dropdown dt a").on('click', function () {
                $(".WCustomer .dropdown dd ul").slideToggle('fast');
            });
            $(".WCustomer .dropdown dd ul li a").on('click', function () {
                $(".WCustomer .dropdown dd ul").hide();
            });

            $(".SMProjectStatus .dropdown dt a").on('click', function () {
                $(".SMProjectStatus .dropdown dd ul").slideToggle('fast');
            });
            $(".SMProjectStatus .dropdown dd ul li a").on('click', function () {
                $(".SMProjectStatus .dropdown dd ul").hide();
            });

            $(".SMInstaller .dropdown dt a").on('click', function () {
                $(".SMInstaller .dropdown dd ul").slideToggle('fast');
            });
            $(".SMInstaller .dropdown dd ul li a").on('click', function () {
                $(".SMInstaller .dropdown dd ul").hide();
            });

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });

            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox2();
            //});

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox4();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox5();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox6();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox11();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox13();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox13();
            });
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");
            callMultiCheckbox();
            callMultiCheckbox1();
            //callMultiCheckbox2();
            callMultiCheckbox4();
            callMultiCheckbox5();
            callMultiCheckbox6();
            callMultiCheckbox11();
            callMultiCheckbox12();
            callMultiCheckbox13();
            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();



            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //  callMultiCheckbox();

            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox();
            //});
            //$(".myval").select2({
            //    minimumResultsForSearch: -1
            //});
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }


            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });
            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox2();
            //});

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox4();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox5();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox6();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox11();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox12();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox13();
            });

        }

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>

    <script type="text/javascript">
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel1').show();
                $('.multiSel1').html(html);
                $(".hida1").hide();
            }
            else {
                $('#spanselect1').show();
                $('.multiSel1').hide();
            }

        }

        function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddlLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel2').show();
                $('.multiSel2').html(html);
                $(".hida2").hide();
            }
            else {
                $('#spanselect2').show();
                $('.multiSel2').hide();
            }
        }
        <%--function callMultiCheckbox2() {
            var title = "";
            $("#<%=ddlAInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel3').show();
                $('.multiSel3').html(html);
                $(".hida3").hide();
                console.log(html);
            }
            else {
                $('#spanselect3').show();
                $('.multiSel3').hide();
            }

        }--%>

        function callMultiCheckbox4() {
            var title = "";
            $("#<%=ddWStatus.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel4').show();
                $('.multiSel4').html(html);
                $(".hida4").hide();
            }
            else {
                $('#spanselect4').show();
                $('.multiSel4').hide();
            }

        }
        function callMultiCheckbox5() {
            var title = "";
            $("#<%=ddWCustomer.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel5').show();
                $('.multiSel5').html(html);
                $(".hida5").hide();
                console.log(html);
            }
            else {
                $('#spanselect5').show();
                $('.multiSel5').hide();
            }

        }
        function callMultiCheckbox6() {
            var title = "";
            $("#<%=ddWLocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel6').show();
                $('.multiSel6').html(html);
                $(".hida6").hide();
            }
            else {
                $('#spanselect6').show();
                $('.multiSel6').hide();
            }
        }

        function callMultiCheckbox11() {
            var title = "";
            $("#<%=ddSMselectlocation.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel11').show();
                $('.multiSel11').html(html);
                $(".hida11").hide();
            }
            else {
                $('#spanselect11').show();
                $('.multiSel11').hide();
            }

        }
        function callMultiCheckbox12() {
            var title = "";
            $("#<%=ddSMProjectStatus.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel12').show();
                $('.multiSel12').html(html);
                $(".hida12").hide();
            }
            else {
                $('#spanselect12').show();
                $('.multiSel12').hide();
            }

        }
        function callMultiCheckbox13() {
            var title = "";
            $("#<%=ddSMInstaller.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel13').show();
                $('.multiSel13').html(html);
                $(".hida13").hide();
            }
            else {
                $('#spanselect13').show();
                $('.multiSel13').hide();
            }

        }
    </script>

    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox1();
            });
			});
            function formValidate() {
                if (typeof (Page_Validators) != "undefined") {
                    for (var i = 0; i < Page_Validators.length; i++) {
                        // alert("2");
                        if (!Page_Validators[i].isvalid) {
                            $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        }
                        else {
                            $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        }
                    }
                }
            }


    </script>
    <script>
            $(document).ready(function () {
                //$('.js-example-basic-multiple').select2();
                $('.mutliSelect input[type="checkbox"]').on('click', function () {
                    callMultiCheckbox();
                });

                $('.mutliSelect input[type="checkbox"]').on('click', function () {
                    callMultiCheckbox1();
                });
				});
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Verification Report
                        <div class="pull-right">
                            <asp:Button Text="Fetch SM Data" ID="btnUpdateSMData" runat="server" CssClass="btn btn-warning dnone" OnClick="btnUpdateSMData_Click" />
                        </div>
                    </h5>


                </div>
            </div>
            <div class="col-md-12" id="divright" runat="server">
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                    <cc1:TabPanel ID="TabProjectNo" runat="server" HeaderText="Project">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel4">
                                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">



                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                                        <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                                            WatermarkText="Project No." />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers" />--%>
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtserailno"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                                            WatermarkText="Stock/Model" />
                                                                    </div>
                                                                    <%--<div class="input-group col-sm-1 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>--%>
                                                                    <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida2" id="spanselect2">Location</span>
                                                                                    <p class="multiSel2"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddlLocation" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="lstSearchLocation" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <%--<div class="input-group col-sm-2 max_width170 martop5" style="width: 120px" id="div1" runat="server">
                                                                        

                                                                        <asp:ListBox ID="lstProjectStatus" runat="server" ToolTip="Project Stutus" CausesValidation="false" SelectionMode="Multiple" CssClass="js-example-basic-multiple" Width="200px"></asp:ListBox>
                                                                    </div>--%>


                                                                    <div class="form-group spical ProjectStatus multiselect martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida1" id="spanselect1">Status</span>
                                                                                    <p class="multiSel1"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddproject" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnProjectStatusID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />

                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>



                                                                    <div class="input-group col-sm-2 martop5 max_width170" id="div2" runat="server" visible="false">
                                                                        <asp:DropDownList ID="ddlprojectwise" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                                            <%-- <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>--%>
                                                                            <asp:ListItem Value="1" Selected="True">PickList Wise</asp:ListItem>
                                                                            <asp:ListItem Value="2">Project wise</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <%--<div class="form-group spical multiselect AInstaller martop5 col-sm-1 max_width200 specail1_select" id="DivAInstaller" runat="server" visible="false">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida3" id="spanselect3">Installer</span>
                                                                                    <p class="multiSel3"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddlAInstaller" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptAInstaller" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnAInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                                                    <asp:HiddenField ID="hdnAInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                   
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltAInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>--%>
                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:DropDownList ID="ddlIsverify" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="10">IsVerify</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 max_width170"><%-- OnSelectedIndexChanged="ddlIsDifference_SelectedIndexChanged" AutoPostBack="true"--%>
                                                                        <asp:DropDownList ID="ddlIsDifference" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                                            >
                                                                            <asp:ListItem Value="0">Is Difference</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170" runat="server" id="DivYesNo">
                                                                        <asp:DropDownList ID="ddlYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="0">Is Audit</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                            <asp:ListItem Value="2">InstallBooked</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-1 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport" />
                                            <asp:PostBackTrigger ControlID="btnClearAll" />
                                            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>
                            <div>
                                <div class="card shadownone brdrgray" id="divtot" runat="server">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 122px;"><a href="#"></a></th>--%>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"><a href="#"></a></th>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"><a href="#">Customer</a></th>--%>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Proj Status</a></th>--%>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Installer</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Booked On</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Deducted On</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Location</a></th>--%>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Out</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Installed</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Diff</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Revert</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Audit</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">I. Out</th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Installed</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Diff</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Revert</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Audit</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"></th>
                                                    </tr>
                                                    <tr class="brd_ornge">
                                                        <%--<td align="left" valign="top"></td>--%>
                                                        <td align="left" valign="top">Total</td>

                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblpout" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblPInstalled" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblPDifference" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblPRevert" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblPAudit" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblIOut" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblIInstalled" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblIDifference" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblIRevert" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblIAudit" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblTotal" runat="server"></asp:Literal></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="finalgrid">
                                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                                    <div>
                                        <div id="PanGrid" runat="server">
                                            <div class="card shadownone brdrgray">
                                                <div class="card-block">
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                                            OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                    <ItemTemplate>
                                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                                            <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                                            <%--<img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />--%>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("ID")%>' />
                                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                             <%#Eval("ProjectNumber")+"/"+Eval("ID")%>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <%--<asp:TemplateField HeaderText="Customer" SortExpression="CustomerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblcustname" runat="server" Width="60px">
                                                                                        <%#Eval("CustomerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>--%>

                                                                <asp:TemplateField HeaderText="Proj Status" SortExpression="ProjectStatus">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label2344" runat="server" Width="60px">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Installer" SortExpression="InstallerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label44" runat="server" Width="60px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Booked On" SortExpression="InstallBookedDate" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4287" runat="server" Width="25px">
                                                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label47" runat="server" Width="25px">
                                                                                        <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <%-- <asp:TemplateField HeaderText="Installation Completed" SortExpression="InstallCompleted">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label67" runat="server" Width="20px">
                                                                                        <%#Eval("InstallCompleted","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label94" runat="server" Width="25px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Out" SortExpression="PanelStockDeducted" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label452" runat="server" Width="25px">
                                                                                        <%#Eval("PanelStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Installed" SortExpression="SaleQtyPanel" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label52" runat="server" Width="30px">
                                                                                        <%#Eval("PanelInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Diff" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label82" runat="server" Width="25px" Text= '<%#Eval("PanelDiff")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Revert" SortExpression="PanelRevert" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblpanelrevrt" runat="server" Width="30px" Text='<%#Eval("PanelRevert")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Audit" SortExpression="PAudit" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblpAudit" runat="server" Width="30px" Text='<%#Eval("PAudit")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Out" SortExpression="InverterStockDeducted" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4522" runat="server" Width="25px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Installed" SortExpression="SaleQtyInverter" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label752" runat="server" Width="25px">
                                                                                        <%#Eval("InverterInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Diff" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label152" runat="server" Width="25px" Text='<%#Eval("InverterDiff")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Revert" SortExpression="InvertRevert" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblinverterrevert" runat="server" Width="25px" Text='<%#Eval("InverterRevert")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Audit" SortExpression="IAudit" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIAudit" runat="server" Width="30px" Text='<%#Eval("IAudit")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage1" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-primary btn-mini" CommandName="Verify" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Verify" data-toggle="tooltip" data-placement="top" Visible='<%#Eval("Isverify").ToString()=="True"?false:true%>'>                                                                          
                                              <i class="btn-label fa fa-close"></i> Verify
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="gvbnNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Note" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                              <i class="btn-label fa fa-edit"></i> Note
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                                    <ItemTemplate>
                                                                        <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                            <td colspan="98%" class="details">
                                                                                <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                                        <tr>
                                                                                            <td style="width: 50px;"><b>Date</b>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label2" runat="server" Width="80px"><%#Eval("notedate","{0:dd MMM yyyy }")%></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 50px;"><b>Note</b>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblProject11" runat="server" Width="50px"><%#Eval("Verifynote")%></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 50px;"><b>System Details</b>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label3" runat="server" Width="50px"
                                                                                                 data-toggle="tooltip" data-placement="Top" title="" data-original-title='<%#Eval("SystemDetails")%>'>
                                                                                                    <%#Eval("SystemDetails")%></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>

                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopage">
                                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabWholesale" runat="server" HeaderText="Wholesale">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel7">
                                    <asp:UpdatePanel ID="updatepanel3" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div6" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div7" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label2" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div8" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div9" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel8" runat="server" DefaultButton="btnSearch3">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">
                                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control m-b" placeholder="Invoice No."></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtInvoiceNo"
                                                                            WatermarkText="Invoice No." />
                                                                        <%--       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers, Custom" ValidChars="," />
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:TextBox ID="txtstockitemfilter3" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtstockitemfilter3"
                                                                            WatermarkText="Stock Item/Model" />
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:TextBox ID="txtserailno3" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtserailno3"
                                                                            WatermarkText="Serial No./Pallet No." />
                                                                    </div>
                                                                    <%--<div class="input-group col-sm-1 max_width170">
                                                                        <asp:DropDownList ID="ddlstatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                            AppendDataBoundItems="true">
                                                                            <asp:ListItem Value="">Status</asp:ListItem>
                                                                            <asp:ListItem Value="1">Pending</asp:ListItem>
                                                                            <asp:ListItem Value="2">JobBooked</asp:ListItem>
                                                                            <asp:ListItem Value="3">Void</asp:ListItem>
                                                                            <asp:ListItem Value="4">PVD Applied</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>--%>
                                                                    <div class="form-group spical multiselect WStatus martop5 col-sm-1 max_width200 specail1_select" id="Div1" runat="server">
                                                                        <dl class="dropdown ">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida4" id="spanselect4">Status</span>
                                                                                    <p class="multiSel4"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddWStatus" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptWStatus" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Eval("Status") %>' />
                                                                                                    <asp:HiddenField ID="hdnWStatusID" runat="server" Value='<%# Eval("ID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltWStatus" Text='<%# Eval("Status")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <%--<div class="input-group col-sm-1 martop5 max_width170" id="div10" runat="server">
                                                                        <asp:DropDownList ID="ddllocationsearch3" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>--%>
                                                                    <div class="form-group spical multiselect Wselectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida6" id="spanselect6">Location</span>
                                                                                    <p class="multiSel6"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddWLocation" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptWLocation" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <%--<div class="input-group col-sm-1 max_width170">
                                                                        <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>--%>
                                                                    <div class="form-group spical multiselect WCustomer martop5 col-sm-2 max_width200 specail1_select" id="DivWCustomer" runat="server">
                                                                        <dl class="dropdown ">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida5" id="spanselect5">Customer</span>
                                                                                    <p class="multiSel5"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddWCustomer" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptWCustomer" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnWCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                                    <asp:HiddenField ID="hdnWCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltWCustomer" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:DropDownList ID="ddlIsverify1" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">IsVerify</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 max_width170"> <%--OnSelectedIndexChanged="ddlWIsDifference_SelectedIndexChanged" AutoPostBack="true"--%>
                                                                        <asp:DropDownList ID="ddlWIsDifference" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                                            >
                                                                            <asp:ListItem Value="">Is Difference</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170" runat="server" id="DivWYesNo">
                                                                        <asp:DropDownList ID="ddlWYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Is Audit</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 max_width170">
                                                                        <asp:DropDownList ID="ddldate3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-1 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtstartdate3" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="date datetimepicker1 col-sm-1 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtenddate3" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSearch3" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSearch3_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>
                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnClearAll3" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll3_Click" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSelectRecords3" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged3"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnExport3" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                                        CausesValidation="false" OnClick="lbtnExport3_Click1" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnExport3" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div>
                                        <div class="card shadownone brdrgray" id="div3" runat="server">
                                            <div class="card-block">
                                                <div class="table-responsive BlockStructure">
                                                    <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                                        <tbody>
                                                            <tr>
                                                                <%--<th class="brdrgrayleft" align="center" scope="col" style="width: 122px;"><a href="#"></a></th>--%>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"><a href="#">Order No.</a></th>
                                                                <%--<th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Invoice No.</a></th>--%>
                                                                <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Customer</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Status</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Deducted On</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Location</a></th>--%>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Out</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Inv. Panel</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Diff</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Revert</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Audit</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">I. Out</th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Inv. Inverter</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Diff</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Revert</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Audit</a></th>
                                                                <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"></th>
                                                            </tr>
                                                            <tr class="brd_ornge">
                                                                <%-- <td align="left" valign="top"></td>--%>
                                                                <td align="left" valign="top">Total</td>
                                                                <%--<td align="left" valign="top"></td>--%>
                                                                <%--<td align="left" valign="top"></td>
                                                                <td align="left" valign="top"></td>
                                                                <td align="left" valign="top"></td>
                                                                <td align="left" valign="top"></td>--%>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwPOut" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwInvPanel" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwPDifference" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwPRevert" runat="server"></asp:Literal></td> 
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwPAudit" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwIOut" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwInvInverter" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwIDifference" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwIRevert" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="lblwIAudit" runat="server"></asp:Literal></td>
                                                                <td align="left" valign="top">
                                                                    <asp:Literal ID="Literal9" runat="server"></asp:Literal></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="finalgrid">
                                        <asp:Panel ID="panel9" runat="server" CssClass="xsroll">
                                            <div>
                                                <div id="PanGrid3" runat="server">
                                                    <div class="card shadownone brdrgray">
                                                        <div class="card-block">
                                                            <div class="table-responsive BlockStructure">
                                                                <asp:GridView ID="GridView3" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                                    OnSorting="GridView3_Sorting" OnPageIndexChanging="GridView3_PageIndexChanging" OnRowCommand="GridView3_RowCommand" OnRowDataBound="GridView3_RowDataBound"
                                                                    OnDataBound="GridView3_DataBound" AllowSorting="true" OnRowCreated="GridView3_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="20px">
                                                                            <ItemTemplate>
                                                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">
                                                                                    <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                                                    <%--<img id='imgdiv<%# Eval("WholesaleOrderID") %>' src="../../../images/icon_plus.png" />--%>
                                                                                </a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Order No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="WholesaleOrderID" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hndWholesaleorderID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                <asp:Label ID="Label61" runat="server" Width="30px">
                                                                                        <%#Eval("WholesaleOrderID")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="InvoiceNo" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label70" runat="server" Width="30px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vendor" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label71" runat="server" Width="100px">
                                                                                        <%#Eval("Vendor")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="left" SortExpression="Status" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6689" runat="server" Width="80px">
                                                                                        <%#Eval("Status")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Deducted On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label63" runat="server" Width="80px">
                                                                                        <%#Eval("StockDeductDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label67" runat="server" Width="30px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="P. Out" SortExpression="PanelStockDeducted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label4521" runat="server" Width="30px">
                                                                                        <%#Eval("PanelStockDeducted")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Inv. Panel" SortExpression="SaleQtyPanel">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label522" runat="server" Width="30px">
                                                                                        <%#Eval("SaleQtyPanel")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="P. Diff">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label823" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("PanelStockDeducted"))-Convert.ToInt32(Eval("SaleQtyPanel"))%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="P. Revert" SortExpression="PanelRevert" HeaderStyle-Width="25px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblwholepanelrevert" runat="server" Width="25px" Text='<%#Eval("PanelRevert")%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="P. Audit" SortExpression="PAudit" HeaderStyle-Width="25px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblwholepanelAudit" runat="server" Width="25px" Text='<%#Eval("PAudit")%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="I. Out" SortExpression="InverterStockDeducted">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label45224" runat="server" Width="30px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Inv. Inverter" SortExpression="SaleQtyInverter">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label7525" runat="server" Width="30px">
                                                                                        <%#Eval("SaleQtyInverter")%></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="I. Diff">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1526" runat="server" Width="30px" Text='<%#Convert.ToInt32(Eval("InverterStockDeducted"))-Convert.ToInt32(Eval("SaleQtyInverter"))%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="I. Revert" SortExpression="InvertRevert" HeaderStyle-Width="25px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblwholeInverterrevert" runat="server" Width="25px" Text='<%#Eval("InvertRevert")%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="I. Audit" SortExpression="IAudit" HeaderStyle-Width="25px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblwholeInverterAudit" runat="server" Width="25px" Text='<%#Eval("IAudit")%>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="gvbtnView3" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage3" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="dvbtnVeriify" runat="server" CssClass="btn btn-success btn-mini" CommandName="Verify" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-close"></i> Verify
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="dvbtnnote1" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Wholesalenotedetail" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> Note
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                                            <ItemTemplate>
                                                                                <tr id='tr<%# Eval("WholesaleOrderID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                                    <td colspan="98%" class="details">
                                                                                        <div id='div<%# Eval("WholesaleOrderID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                                            <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                                                <tr>
                                                                                                    <td style="width: 50px;"><b>Date</b>
                                                                                                    </td>
                                                                                                    <td style="width: 100px;">
                                                                                                        <asp:Label ID="lblwholesaledate" runat="server" Width="80px"><%#Eval("NoteDate","{0:dd MMM yyyy }")%></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 50px;"><b>Note</b>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblwholesaleNote" runat="server" Width="50px"><%#Eval("NoteDes")%></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>

                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle />
                                                                    <PagerTemplate>
                                                                        <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        <div class="pagination">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                        </div>
                                                                    </PagerTemplate>
                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                </asp:GridView>

                                                            </div>
                                                            <div class="paginationnew1" runat="server" id="divnopage3">
                                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage3" style="width: 100%; border-collapse: collapse;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabSMProject" runat="server" HeaderText="SolarMiner Project">
                        <ContentTemplate>
                            <div class="page-body padtopzero">
                                <asp:Panel runat="server" ID="Panel2">
                                    <asp:UpdatePanel ID="updatepanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="messesgarea">
                                                <div class="alert alert-success" id="Div4" runat="server" visible="false">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div5" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="Label1" runat="server"
                                                        Text="Transaction Failed."></asp:Label></strong>
                                                </div>
                                                <div class="alert alert-danger" id="Div10" runat="server" visible="false">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                                </div>
                                                <div class="alert alert-info" id="Div11" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                </div>
                                            </div>
                                            <div class="searchfinal">
                                                <div class="card shadownone brdrgray pad10">
                                                    <div class="card-block">
                                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSMSearch">
                                                            <div class="inlineblock martop5">
                                                                <div class="row">

                                                                    <div class="input-group col-sm-1 martop5 max_width170">
                                                                        <asp:TextBox ID="txtSMProjectNo" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>

                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSMProjectNo" FilterType="Numbers, Custom" ValidChars="," />
                                                                    </div>

                                                                    <div class="form-group spical multiselect SMselectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida11" id="spanselect11">Location</span>
                                                                                    <p class="multiSel11"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddSMselectlocation" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptSMselectlocation" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                                    <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>

                                                                    <div class="form-group spical SMProjectStatus multiselect martop5 col-sm-1 max_width200 specail1_select">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida12" id="spanselect12">Status</span>
                                                                                    <p class="multiSel12"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddSMProjectStatus" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptSMProjectStatus" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnProjectStatusID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />

                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>

                                                                    <div class="form-group spical multiselect SMInstaller martop5 col-sm-2 max_width200 specail1_select" id="Div13" runat="server">
                                                                        <dl class="dropdown ">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida13" id="spanselect13">Installer</span>
                                                                                    <p class="multiSel13"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddSMInstaller" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="rptSMInstaller" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnSMInstaller" runat="server" Value='<%# Eval("Contact") %>' />
                                                                                                    <asp:HiddenField ID="hdnSMInstallerID" runat="server" Value='<%# Eval("ContactID") %>' />


                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltAInstaller" Text='<%# Eval("Contact")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    
                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlSMIsverify" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="10">IsVerify</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 max_width170">  <%--OnSelectedIndexChanged="ddlSMIsDifference_SelectedIndexChanged" AutoPostBack="true"--%>
                                                                        <asp:DropDownList ID="ddlSMIsDifference" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                                           >
                                                                            <asp:ListItem Value="0">Is Difference</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1 max_width170" runat="server" id="DivSMYesNo">
                                                                        <asp:DropDownList ID="ddlSMYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="0">Is Audit</asp:ListItem>
                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2 max_width170">
                                                                        <asp:DropDownList ID="ddlSMDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="0">Date</asp:ListItem>
                                                                            <asp:ListItem Value="1">Deducted</asp:ListItem>
                                                                            <asp:ListItem Value="2">InstallBooked</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtSMStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                                        <div class="input-group sandbox-container">
                                                                            <asp:TextBox ID="txtSMEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                            <div class="input-group-addon">
                                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSMSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                                            CausesValidation="false" OnClick="btnSMSearch_Click"></asp:LinkButton>
                                                                        <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                    </div>

                                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                                        <asp:LinkButton ID="btnSMClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnSMClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                        <div class="datashowbox inlineblock">
                                                            <div class="row">

                                                                <div class="input-group col-sm-1 martop5 max_width170">
                                                                    <asp:DropDownList ID="ddlSMSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSMSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                                    <asp:LinkButton ID="lbtnSMExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnSMExport_Click"
                                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbtnSMExport" />
                                            <asp:PostBackTrigger ControlID="btnSMClearAll" />
                                           
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>
                            <div>
                                <div class="card shadownone brdrgray" id="divSMtot" runat="server">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 122px;"><a href="#"></a></th>--%>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"><a href="#"></a></th>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"><a href="#">Customer</a></th>--%>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Proj Status</a></th>--%>
                                                        <%-- <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Installer</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Booked On</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Deducted On</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">Location</a></th>--%>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Out</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Installed</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Diff</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Revert</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">P. Audit</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">I. Out</th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Installed</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Diff</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Revert</a></th>
                                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"><a href="#">I. Audit</a></th>
                                           
                                                    </tr>
                                                    <tr class="brd_ornge">
                                                        <%--<td align="left" valign="top"></td>--%>
                                                        <td align="left" valign="top">Total</td>
                                                        <%-- <td align="left" valign="top"></td>--%>
                                                        <%-- <td align="left" valign="top"></td>
                                                        <td align="left" valign="top"></td>
                                                        <td align="left" valign="top"></td>
                                                        <td align="left" valign="top"></td>
                                                        <td align="left" valign="top"></td>--%>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMpout" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMPInstalled" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMPDifference" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMPRevert" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMPAudit" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMIOut" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMIInstalled" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMIDifference" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMIRevert" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top">
                                                            <asp:Literal ID="lblSMIAudit" runat="server"></asp:Literal></td>
                                                       
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="finalgrid">
                                <asp:Panel ID="panel5" runat="server" CssClass="xsroll">
                                    <div>
                                        <div id="PanGridSM" runat="server">
                                            <div class="card shadownone brdrgray">
                                                <div class="card-block">
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridViewSM" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                            OnSorting="GridViewSM_Sorting" OnPageIndexChanging="GridViewSM_PageIndexChanging" OnRowCommand="GridViewSM_RowCommand" OnRowDataBound="GridViewSM_RowDataBound"
                                                            OnDataBound="GridViewSM_DataBound" AllowSorting="True" OnRowCreated="GridViewSM_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                    <ItemTemplate>
                                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                                            <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                                            <%--<img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />--%>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Project No." SortExpression="ProjectNumber">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("ID")%>' />
                                                                        <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                        <asp:Label ID="Label11" runat="server" Width="30px">
                                                                             <%#Eval("ProjectNumber")+"/"+Eval("ID")%>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                               
                                                                <asp:TemplateField HeaderText="Proj Status" SortExpression="ProjectStatus">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label2344" runat="server" Width="60px">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Installer" SortExpression="InstallerName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label44" runat="server" Width="60px">
                                                                                        <%#Eval("InstallerName")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Booked On" SortExpression="InstallBookedDate" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4287" runat="server" Width="25px">
                                                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Deducted On" SortExpression="DeductOn" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label47" runat="server" Width="25px">
                                                                                        <%#Eval("DeductOn","{0:dd MMM yyyy }")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <%-- <asp:TemplateField HeaderText="Installation Completed" SortExpression="InstallCompleted">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label67" runat="server" Width="20px">
                                                                                        <%#Eval("InstallCompleted","{0:dd MMM yyyy }")%></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label94" runat="server" Width="25px">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Out" SortExpression="PanelStockDeducted" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label452" runat="server" Width="25px">
                                                                                        <%#Eval("PanelStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Installed" SortExpression="PanelInstalled" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label52" runat="server" Width="30px">
                                                                                        <%#Eval("PanelInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Diff" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label82" runat="server" Width="25px" Text='<%#Eval("PanelDiff")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Revert" SortExpression="PanelRevert" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblpanelrevrt" runat="server" Width="30px" Text='<%#Eval("PanelRevert")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="P. Audit" SortExpression="PAudit" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPAudit" runat="server" Width="30px" Text='<%#Eval("PAudit")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Out" SortExpression="InverterStockDeducted" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label4522" runat="server" Width="25px">
                                                                                        <%#Eval("InverterStockDeducted")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Installed" SortExpression="SaleQtyInverter" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label752" runat="server" Width="25px">
                                                                                        <%#Eval("InverterInstalled")%></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Diff" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label152" runat="server" Width="25px" Text='<%#Eval("InverterDiff")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="I. Revert" SortExpression="InverterRevert" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblinverterrevert" runat="server" Width="25px" Text='<%#Eval("InverterRevert")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="I. Audit" SortExpression="IAudit" HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIAudit" runat="server" Width="25px" Text='<%#Eval("IAudit")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnView" runat="server" CssClass="btn btn-success btn-mini" CommandName="viewpage1" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="View" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-eye"></i> View
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-primary btn-mini" CommandName="Verify" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Verify" data-toggle="tooltip" data-placement="top" Visible='<%#Eval("Isverify").ToString()=="True"?false:true%>'>                                                                          
                                              <i class="btn-label fa fa-close"></i> Verify
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="gvbnNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="Note" CommandArgument='<%#Eval("ProjectNumber") + ";" +Eval("ID")%>' CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top">
                                              <i class="btn-label fa fa-edit"></i> Note
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                                    <ItemTemplate>
                                                                        <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                            <td colspan="98%" class="details">
                                                                                <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                                        <tr>
                                                                                            <td style="width: 50px;"><b>Date</b>
                                                                                            </td>
                                                                                            <td style="width: 100px;">
                                                                                                <asp:Label ID="Label2" runat="server" Width="80px"><%#Eval("notedate","{0:dd MMM yyyy }")%></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 50px;"><b>Note</b>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblProject11" runat="server" Width="50px"><%#Eval("Verifynote")%></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 50px;"><b>System Details</b>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblSysDetailsSM" runat="server" Width="50px"
                                                                                                 data-toggle="tooltip" data-placement="Top" title="" data-original-title='<%#Eval("SystemDetails")%>'>
                                                                                                    <%#Eval("SystemDetails")%></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>

                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopageSM">
                                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPageSM" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>

                </cc1:TabContainer>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="10%" align="center">Index No.</th>
                                                    <th width="25%" align="center">Serial No.</th>
                                                    <th width="15%" align="left">Category</th>
                                                    <th align="50%">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left"><%#Container.ItemIndex+1 %></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialNo") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCategory" runat="server"><%#Eval("CategoryName") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--Project--%>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="ModelNote" TargetControlID="btnNull1"
                CancelControlID="LinkButton6">
            </cc1:ModalPopupExtender>
            <div id="ModelNote" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel1">Project Note
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-12 padd_btm10">
                                                    <div class="date datetimepicker1 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:HiddenField ID="hdnPickListId" runat="server" />
                                                            <asp:TextBox ID="txtnotedate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtnotedesc" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="btnsavenote" CssClass="btn btn-info POPupLoader" runat="server" Text="Save" OnClick="btnsavenote_Click"></asp:LinkButton>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />

            <asp:Button ID="btnverify" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderverify" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_verify" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btnverify">
            </cc1:ModalPopupExtender>
            <div id="modal_verify" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth">Verify
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                            </h5>
                        </div>
                        <div class="modal-body ">Are You Sure You want to Verify?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:Button ID="lnkverify" runat="server" OnClick="lnkverify_Click" class="btn btn-danger POPupLoader" Text="Ok" />
                            <asp:Button ID="LinkButton7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
            <%--Wholesale--%>
            <cc1:ModalPopupExtender ID="ModalPopupExtenderWholeNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="WholeSaleNote" TargetControlID="btnNull2"
                CancelControlID="LinkButton8">
            </cc1:ModalPopupExtender>
            <div id="WholeSaleNote" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel2">WholeSale Note
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton8" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-12 padd_btm10">
                                                    <div class="date datetimepicker1 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:HiddenField ID="hndwholesaleorderID" runat="server" />
                                                            <asp:TextBox ID="txtwholesaledate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtwholesalenote" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="btnwholesalesave" CssClass="btn btn-info POPupLoader" runat="server" Text="Save" OnClick="btnwholesalesave_Click"></asp:LinkButton>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />

            <asp:Button ID="btnwholesaleverify" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderWholeSaleVerify" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_wholesaleVerify" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btnwholesaleverify">
            </cc1:ModalPopupExtender>
            <div id="modal_wholesaleVerify" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth">Wholesale Verify
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                            </h5>
                        </div>
                        <div class="modal-body ">Are You Sure You want to Verify?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:Button ID="btnwholesaleVerify1" runat="server" OnClick="btnwholesaleVerify1_Click" class="btn btn-danger POPupLoader" Text="Ok" />
                            <asp:Button ID="btncancel" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                        </div>
                    </div>
                </div>

            </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

                //$(".dropdown dt a").on('click', function () {
                //    $(".dropdown dd ul").slideToggle('fast');

                //});

                //$(".dropdown dd ul li a").on('click', function () {
                //    $(".dropdown dd ul").hide();
                //});
                //$(document).bind('click', function (e) {
                //    var $clicked = $(e.target);
                //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                //});
                $('.mutliSelect input[type="checkbox"]').on('click', function () {
                    callMultiCheckbox();
                });

                $('.mutliSelect input[type="checkbox"]').on('click', function () {
                    callMultiCheckbox1();
                });
                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });

                $(document).ready(function () {
                    HighlightControlToValidate();

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});


                });




                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
                function HighlightControlToValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            $('#' + Page_Validators[i].controltovalidate).blur(function () {
                                var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                                if (validatorctrl != null && !validatorctrl.isvalid) {
                                    $(this).css("border-color", "#FF5F5F");
                                }
                                else {
                                    $(this).css("border-color", "#B5B5B5");
                                }
                            });
                        }
                    }
                }
                function getValidatorUsingControl(controltovalidate) {
                    var length = Page_Validators.length;
                    for (var j = 0; j < length; j++) {
                        if (Page_Validators[j].controltovalidate == controltovalidate) {
                            return Page_Validators[j];
                        }
                    }
                    return null;
                }
    </script>
    <script type="text/javascript">
                $(document).ready(function () {

                    //gridviewScroll();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                });
                $("#nav").on("click", "a", function () {
                    $('#content').animate({ opacity: 0 }, 500, function () {
                        //gridviewScroll();
                        $('#content').delay(250).animate({ opacity: 1 }, 500);
                    });
                });

                function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
                }


                // For Multi Select //
                $(".selectlocation .dropdown dt a").on('click', function () {
                    $(".selectlocation .dropdown dd ul").slideToggle('fast');
                });
                $(".selectlocation .dropdown dd ul li a").on('click', function () {
                    $(".selectlocation .dropdown dd ul").hide();
                });

                $(".ProjectStatus .dropdown dt a").on('click', function () {
                    $(".ProjectStatus .dropdown dd ul").slideToggle('fast');
                });
                $(".ProjectStatus .dropdown dd ul li a").on('click', function () {
                    $(".ProjectStatus .dropdown dd ul").hide();
                });

                $(".SMProjectStatus .dropdown dt a").on('click', function () {
                    $(".SMProjectStatus .dropdown dd ul").slideToggle('fast');
                });
                $(".SMProjectStatus .dropdown dd ul li a").on('click', function () {
                    $(".SMProjectStatus .dropdown dd ul").hide();
                });

                $(".SMInstaller .dropdown dt a").on('click', function () {
                    $(".SMInstaller .dropdown dd ul").slideToggle('fast');
                });
                $(".SMInstaller .dropdown dd ul li a").on('click', function () {
                    $(".SMInstaller .dropdown dd ul").hide();
                });

                $(".AInstaller .dropdown dt a").on('click', function () {
                    $(".AInstaller .dropdown dd ul").slideToggle('fast');
                });
                $(".AInstaller .dropdown dd ul li a").on('click', function () {
                    $(".AInstaller .dropdown dd ul").hide();
                });

                $(".WStatus .dropdown dt a").on('click', function () {
                    $(".WStatus .dropdown dd ul").slideToggle('fast');
                });
                $(".WStatus .dropdown dd ul li a").on('click', function () {
                    $(".WStatus .dropdown dd ul").hide();
                });

                $(".WCustomer .dropdown dt a").on('click', function () {
                    $(".WCustomer .dropdown dd ul").slideToggle('fast');
                });
                $(".WCustomer .dropdown dd ul li a").on('click', function () {
                    $(".WCustomer .dropdown dd ul").hide();
                });

                $(".Wselectlocation .dropdown dt a").on('click', function () {
                    $(".Wselectlocation .dropdown dd ul").slideToggle('fast');
                });
                $(".Wselectlocation .dropdown dd ul li a").on('click', function () {
                    $(".Wselectlocation .dropdown dd ul").hide();
                });

                $(".SMselectlocation .dropdown dt a").on('click', function () {
                    $(".SMselectlocation .dropdown dd ul").slideToggle('fast');
                });
                $(".SMselectlocation .dropdown dd ul li a").on('click', function () {
                    $(".SMselectlocation .dropdown dd ul").hide();
                });


                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });




    </script>

</asp:Content>
