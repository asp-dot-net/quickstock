﻿using System;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_DataHistory : System.Web.UI.Page
{
    static DataView dv;
    protected string SiteURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindMultiselect();

            //ddlDate.SelectedValue = "1";
            //txtStartDate.Text = System.DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
            //txtEndDate.Text = System.DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
            //BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {
        string opt = ddlOption.SelectedValue;
        string moduleName = getModuleName();
        string category = ddlCategory.SelectedValue;
        string state = ddlState.SelectedValue;
        string dateType = ddlDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        DataTable dt = ClsReportsV2.SP_DataHistoryV2(opt, moduleName, category, state, dateType, startDate, endDate);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                // SetDelete();
            }
            else
            {
                //  SetNoRecords();
                Notification("There are no items to show in this view.");
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    public void BindMultiselect()
    {
        string qr = "Select DISTINCT modulename from tblMaintainHistory where modulename is not null Union All select DISTINCT ModuleName from tblOtherSerialNosOfUnknownOrders";
        DataTable modulename = ClstblCustomers.query_execute(qr);
        lstSearchStatus.DataSource = modulename;
        lstSearchStatus.DataBind();

        DataTable dtCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
        
        DataTable dtState = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlState.DataSource = dtState;
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "CompanyLocationID";
        ddlState.DataBind();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);

    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockitem.aspx");
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //int StockItemID = int.Parse(Request.QueryString["StockItemID"].ToString());
            //string StockLocationId = Request.QueryString["CompanyLocationId"].ToString();
            //HiddenField hdnserialno = (HiddenField)e.Row.FindControl("hdnserialno");
            //HyperLink gvbtnView = (HyperLink)e.Row.FindControl("gvbtnView");
            //gvbtnView.NavigateUrl = SiteURL + "admin/adminfiles/stock/ViewSerialHistory.aspx?SerailNo=" + hdnserialno.Value + "&StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocationId;
            //gvbtnView.Target = "_blank";
            //Label Label39 = (Label)e.Row.FindControl("Label39");
            //HiddenField hndprojNumber = (HiddenField)e.Row.FindControl("hndprojNumber");
            //HiddenField hndPickid = (HiddenField)e.Row.FindControl("hndPickid");
            //Label Label79 = (Label)e.Row.FindControl("Label79");
            //HiddenField hndInvoiceNo = (HiddenField)e.Row.FindControl("hndInvoiceNo");
            //HiddenField hndWholesaleOrderID = (HiddenField)e.Row.FindControl("hndWholesaleOrderID");

            //if (string.IsNullOrEmpty(hndprojNumber.Value) && string.IsNullOrEmpty(hndPickid.Value))
            //{
            //    Label39.Text = "";
            //}
            //if (string.IsNullOrEmpty(hndInvoiceNo.Value) && string.IsNullOrEmpty(hndWholesaleOrderID.Value))
            //{
            //    Label79.Text = "";
            //}
        }
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        ddlOption.SelectedValue = "1";
        ddlCategory.SelectedValue = "";
        ddlState.SelectedValue = "";
        ddlDate.SelectedValue = "1";
        txtStartDate.Text = System.DateTime.Now.AddHours(15).ToString("dd/mm/yyyy");
        txtEndDate.Text = System.DateTime.Now.AddHours(15).ToString("dd/mm/yyyy");

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void lbtnExport_Click1(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        Export oExport = new Export();
        string FileName = "DataHistory" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        string[] arrHeader = { "Project/InvoiceNo", "Installer Name", "Location", "Module Name", "Category Name", "Stock Item", "SerialNo", "Message", "Deduct On", "Created dDate" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.CSV, FileName);
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {


    }

    public string getModuleName()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnModuleName");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnModule.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        return selectedItem;
    }
}