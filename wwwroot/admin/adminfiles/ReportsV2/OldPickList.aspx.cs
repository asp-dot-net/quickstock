﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_OldPickList : System.Web.UI.Page
{
    static int MaxAttributePickList = 1;
    static DataView dv;
    protected static string Siteurl;
    protected DataTable rptTablePickList;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();

            BindDropdown();
            HidePanels();
            BindGrid(0);

            MaxAttributePickList = 1;
            BindRptPickList();
        }
    }

    public void BindDropdown()
    {
        DataTable dtLocation = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataSource = dtLocation;
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dtPickList = ClstblPicklist.Sp_OldPickList_getData(ddlCompany.SelectedValue, txtProjectNo.Text, ddlLocation.SelectedValue, txtEndDate.Text);
        return dtPickList;
    }

    public void HidePanels()
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanSearch.Visible = true;
    }

    public void InitUpdate()
    {
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        PanSearch.Visible = false;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            // HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            PanGridSearch.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGridSearch.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //divnopage.Visible = true;
            ////PanNoRecord.Visible = false;
            ////Response.Write("yes" + ddlSelectRecords.SelectedValue + "All" );
            ////Response.End();
            //if (dt.Rows.Count > 0 && 25 > dt.Rows.Count)
            //{
            //    if (Convert.ToInt32(25) < dt.Rows.Count)
            //    {
            //        //========label Hide
            //        divnopage.Visible = false;
            //    }
            //    else
            //    {
            //        divnopage.Visible = true;
            //        int iTotalRecords = dv.ToTable().Rows.Count;
            //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            //        if (iEndRecord > iTotalRecords)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        if (iStartsRecods == 0)
            //        {
            //            iStartsRecods = 1;
            //        }
            //        if (iEndRecord == 0)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            //    }
            //}
            //else
            //{
            //    if (25 > dt.Rows.Count)
            //    {
            //        divnopage.Visible = true;
            //        ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
            //    }
            //}

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                }
            }

        }
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hnd1.Value = id;
        DataTable dt = ClstblPicklist.tbl_PicklistItemDetail_GetDataByPickID(id);

        if (dt.Rows.Count > 0)
        {
            txtPickListAgainReson.Text = dt.Rows[0]["Reasons"].ToString();
            txtPicklistAgainNotes.Text = dt.Rows[0]["Note"].ToString();
            MaxAttributePickList = dt.Rows.Count;
            rptPickList.DataSource = dt;
            rptPickList.DataBind();

            InitUpdate();
            //btnSavePickList.Visible = false;
            //btnUpdatePicklist.Visible = true;
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Select")
        {
            hndLocation.Value = e.CommandArgument.ToString();
            //btnSavePickList.Visible = false;
            //btnUpdatePicklist.Visible = true;

            //string PickListID = e.CommandArgument.ToString();
            //hnd1.Value = PickListID;
            //DataTable dt = ClstblPicklist.tbl_PicklistItemDetail_GetDataByPickID(PickListID);

            //if (dt.Rows.Count > 0)
            //{
            //    txtPickListAgainReson.Text = dt.Rows[0]["Reasons"].ToString();
            //    txtPicklistAgainNotes.Text = dt.Rows[0]["Note"].ToString();
            //    MaxAttributePickList = dt.Rows.Count;
            //    rptPickList.DataSource = dt;
            //    rptPickList.DataBind();


            //    btnUpdatePicklist.Visible = true;
            //}
        }

        //if (e.CommandName.ToString() == "Delete")
        //{
        //    string PickListID = e.CommandArgument.ToString();
        //    //int tblMaintanHistoryLogExist = ClstblPicklist.tblMaintainHistory_ExistsBySectionID(PickListID);

        //    //if (tblMaintanHistoryLogExist == 0) //Write Delete Code
        //    //{
        //    //    lblMassage.Text = "Are You Sure Delete This Picklist?";
        //    //    hndPickDelete.Value = PickListID;
        //    //    lnkDeletePickList.Visible = true;
        //    //    ModalPopupExtenderDelete.Show();
        //    //}
        //    //else
        //    //{
        //    //    lblMassage.Text = "Picklist Log Already Exists..";
        //    //    lnkDeletePickList.Visible = false;
        //    //    ModalPopupExtenderDelete.Show();
        //    //}
        //}
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;

            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;
                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;
            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;
            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {

                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {

            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
    }

    public void SetAdd()
    {
        Reset();
        Notification("Transaction Successful!");
        //PanSuccess.Visible = true;
        //PanAlreadExists.Visible = false;
    }

    public void SetDelete()
    {
        Reset();
        Notification("Transaction Successful!");
        //PanSuccess.Visible = true;
    }

    public void SetError()
    {
        Reset();
        Notification("Transaction Failed!");
    }

    public void Reset()
    {
        txtPickListAgainReson.Text = string.Empty;
        txtPicklistAgainNotes.Text = string.Empty;
        MaxAttributePickList = 1;
        BindRptPickList();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //string ProjectNo = txtProjectNo.Text;
        //string CompanyID = ddlCompany.SelectedValue;

        //if (ProjectNo != "" && CompanyID != "")
        //{
        //    if (CompanyID == "1")
        //    {
        //        SttblProjects stPro = ClstblPicklist.tblProjects_SelectByProjectNumber(ProjectNo);
        //        BindRptPickListWithdata(stPro, ProjectNo, CompanyID);
        //    }
        //    else if (CompanyID == "2")
        //    {
        //        SttblProjects stPro = ClstblPicklist.SM_tblProjects_SelectByProjectNumber(ProjectNo);
        //        BindRptPickListWithdata(stPro, ProjectNo, CompanyID);
        //    }
        //    else
        //    {
        //        Notification("Please Select Company..");
        //    }
        //}
        //else
        //{
        //    if (ProjectNo == "")
        //    {
        //        Notification("Please Select ProjectNo..");
        //    }
        //    else if (CompanyID == "")
        //    {
        //        Notification("Please Select Company..");
        //    }
        //    else
        //    {
        //        Notification("Please Select Company and Project No..");
        //    }
        //}
        BindGrid(0);
    }

    public void BindRptPickListWithdata(SttblProjects stPro, string ProjectNo, string CompanyID)
    {
        int PickListExist = ClstblPicklist.tblPickListLog_ExistsByProjectNo_CompanyId(ProjectNo, CompanyID);

        if (PickListExist != 0)
        {
            MaxAttributePickList = 1;
            BindRptPickList();
            BindPickList();
        }
        else
        {

            DataTable dtStock = new DataTable();
            dtStock.Columns.AddRange(new DataColumn[5] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int)), new DataColumn("PicklistitemId", typeof(int))});


            if (!string.IsNullOrEmpty(stPro.PanelBrandID))
            {
                if (Convert.ToInt32(stPro.PanelBrandID) != 0)
                {
                    int StockItemIDPanel = ClstblPicklist.tblStockItems_GetFixItemIDByStockItemID(CompanyID, stPro.PanelBrandID);
                    dtStock.Rows.Add(StockItemIDPanel, stPro.NumberPanels, 1, 0);
                }

            }
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID))
            {
                if (Convert.ToInt32(stPro.InverterDetailsID) != 0)
                {
                    int StockItemIDInverter = ClstblPicklist.tblStockItems_GetFixItemIDByStockItemID(CompanyID, stPro.InverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty, 2, 0);
                }

            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID))
            {
                if (Convert.ToInt32(stPro.SecondInverterDetailsID) != 0)
                {
                    int StockItemIDInverter = ClstblPicklist.tblStockItems_GetFixItemIDByStockItemID(CompanyID, stPro.SecondInverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty2, 2, 0);
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID))
            {
                if (Convert.ToInt32(stPro.ThirdInverterDetailsID) != 0)
                {
                    int StockItemIDInverter = ClstblPicklist.tblStockItems_GetFixItemIDByStockItemID(CompanyID, stPro.ThirdInverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty3, 2, 0);
                }
            }

            if (dtStock.Rows.Count > 0)
            {
                MaxAttributePickList = dtStock.Rows.Count;
                rptPickList.DataSource = dtStock;
                rptPickList.DataBind();
            }
            else
            {
                MaxAttributePickList = 1;
            }
        }
    }

    public void BindScript()
    {
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        ddlCompany.SelectedValue = "1";
        txtProjectNo.Text = string.Empty;
        ddlLocation.SelectedValue = "";
        txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        BindGrid(0);
    }

    public void SetReset()
    {
        txtProjectNo.Text = string.Empty;
        ddlCompany.SelectedValue = "";
        Reset();
    }

    #region PickList Item
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*]{2})",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);

        return Convert.ToInt32(match.Value);
    }

    protected void rptPickList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        LinkButton litremove = (LinkButton)item.FindControl("litremovePicklistItem");

        //litremove.Visible = e.Item.ItemIndex == 0 ? false : true;

        //Bind DropDown
        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hndStockCategoryID");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        if (hdnStockCategoryId.Value != "")
        {
            ddlStockItem.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Value = "";
            listItem.Text = "Select";
            ddlStockItem.Items.Add(listItem);

            DataTable dtStockItem = ClstblPicklist.tblStockItems_Select_Category(hdnStockCategoryId.Value);

            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

            try
            {
                ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
                ddlStockItem.SelectedValue = hdnStockItem.Value;
            }
            catch (Exception ex)
            {

            }
        }

        //Wallate Qty Installaer wise
        //TextBox txtWallateQty = (TextBox)e.Item.FindControl("txtWallateQty");
        //string Accreditation = "";
        //string wallQty = "0";
        //txtWallateQty.Text = "0";
        //if (ddlInstaller.SelectedValue != string.Empty)
        //{
        //    SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller.SelectedValue);
        //    Accreditation = st.Accreditation;
        //    if (!string.IsNullOrEmpty(Accreditation))
        //    {
        //        DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlInstaller.SelectedValue);
        //        if (ddlStockItem.SelectedValue != string.Empty)
        //        {
        //            if (dtAcno.Rows.Count > 0)
        //            {
        //                if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
        //                {
        //                    string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
        //                    DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        wallQty = dt.Rows[0]["Qty"].ToString();
        //                        txtWallateQty.Text = wallQty;
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            // MsgError("Please select Item");
        //        }
        //    }
        //}
    }

    protected void ddlStockCategoryID_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptPickList.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        HiddenField hdnStockCategoryId = (HiddenField)item.FindControl("hndStockCategoryID");

        ddlStockItem.Items.Clear();
        ListItem listItem = new ListItem();
        listItem.Value = "";
        listItem.Text = "Select";
        ddlStockItem.Items.Add(listItem);

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue;

            DataTable dtStockItem = ClstblPicklist.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

        }
    }

    protected void BindRptPickList()
    {
        if (rptTablePickList == null)
        {
            rptTablePickList = new DataTable();
            rptTablePickList.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("StockItemID", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("Qty", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("type", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("PicklistitemId", Type.GetType("System.String"));
        }

        for (int i = rptTablePickList.Rows.Count; i < MaxAttributePickList; i++)
        {
            DataRow dr = rptTablePickList.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "0";
            dr["type"] = "";
            dr["PicklistitemId"] = "";
            //dr["PicklstItemId"] = "";
            rptTablePickList.Rows.Add(dr);
        }

        rptPickList.DataSource = rptTablePickList;
        rptPickList.DataBind();

        //============Do not Delete Start========================================
        int y = 0;
        foreach (RepeaterItem item1 in rptPickList.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            LinkButton lbremove1 = (LinkButton)item1.FindControl("litremovePicklistItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptPickList.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptPickList.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("litremovePicklistItem");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End==========================================
    }

    protected void AddmoreAttributePickList()
    {
        MaxAttributePickList = MaxAttributePickList + 1;
        BindAddedAttributePicklist();
        BindRptPickList();
    }

    protected void BindAddedAttributePicklist()
    {
        rptTablePickList = new DataTable();
        rptTablePickList.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("StockItemID", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("Qty", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("type", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("PicklistitemId", Type.GetType("System.String"));
        //rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptPickList.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            //HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField PicklistitemId = (HiddenField)item.FindControl("PicklistitemId");

            //HiddenField hdnlistitemId = (HiddenField)item.FindControl("hndPckListItemId");
            DataRow dr = rptTablePickList.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;
            dr["PicklistitemId"] = PicklistitemId.Value;
            //if (hdnlistitemId != null)
            //{
            //    if (hdnlistitemId.Value != null && hdnlistitemId.Value != "")
            //    {
            //        dr["PicklstItemId"] = hdnlistitemId.Value;
            //    }
            //}
            rptTablePickList.Rows.Add(dr);
        }
    }

    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        AddmoreAttributePickList();
    }

    protected void litremovePicklistItem_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((LinkButton)sender).ClientID);

        RepeaterItem item = rptPickList.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;
        //int pnlerror = 0;
        //int inverror = 0;
        foreach (RepeaterItem item1 in rptPickList.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            //HiddenField hndPickLIstItemId = (HiddenField)item1.FindControl("hndPickLIstItemId");

            LinkButton lbremove1 = (LinkButton)item1.FindControl("litremovePicklistItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptPickList.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptPickList.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("litremovePicklistItem");
                    lbremove.Visible = false;
                }
            }


        }
    }

    #endregion

    protected void btnUpdatePicklist_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        SttblProjects stPro = new SttblProjects();
        if (hndLocation.Value == "1")
        {
            stPro = ClstblPicklist.tblProjects_SelectByProjectNumber(txtProjectNo.Text);
        }
        else if (hndLocation.Value == "2")
        {
            stPro = ClstblPicklist.SM_tblProjects_SelectByProjectNumber(txtProjectNo.Text);
        }

        //string PicklistID = hndPickListID.Value;

        string PicklistID = hnd1.Value;
        string Reason = txtPickListAgainReson.Text;
        string Notes = txtPicklistAgainNotes.Text;

        string InstallBookingDate = stPro.InstallBookingDate;
        string Installer = stPro.Installer;
        string InstallerName = stPro.InstallerName;
        string Designer = stPro.Designer;
        string Electrician = stPro.Electrician;
        string ProjectID = stPro.ProjectID;
        string CompanyID = ddlCompany.SelectedValue;

        if (PicklistID != "")
        {
            bool UpdatePicklistData = ClstblPicklist.tblPickListLog_Update(PicklistID, Reason, Notes, InstallBookingDate, Installer, InstallerName, Designer, Electrician, UpdatedBy);

            bool loc = ClstblPicklist.tbl_PickListLog_Update_locationId_CompanyWise(PicklistID, stPro.StockAllocationStore, CompanyID, stPro.Project, stPro.RoofType, stPro.RoofTypeID);
            bool suc1 = ClstblPicklist.tbl_picklistlog_updatePicklistType(PicklistID, "2");//1)Draft (2) PickLIst

            ClstblPicklist.tbl_PicklistItemsDetail_Delete(PicklistID);

            foreach (RepeaterItem item in rptPickList.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                    int suc = ClstblPicklist.tbl_PicklistItemDetail_Insert(PicklistID, ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, stPro.StoreName, ddlStockCategoryID.SelectedValue);

                    //string stockitem = ddlStockItem.SelectedItem.Text;                          
                    if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                    {
                        if (ddlStockCategoryID.SelectedValue == "1")
                        {
                            if (stPro.NumberPanels != null && stPro.NumberPanels != "")
                            {
                                bool s2 = ClstblPicklist.tbl_picklistlog_UpdateIPanel(PicklistID, stPro.NumberPanels);
                            }
                            else
                            {
                                bool s2 = ClstblPicklist.tbl_picklistlog_UpdateIPanel(PicklistID, "0");
                            }
                        }
                        if (ddlStockCategoryID.SelectedValue == "2")
                        {
                            int FirstInv = 0;
                            int Secondinv = 0;
                            int thirdinv = 0;

                            if (stPro.inverterqty != null && stPro.inverterqty != "")
                            {
                                FirstInv = Convert.ToInt32(stPro.inverterqty);
                            }
                            if (stPro.inverterqty2 != null && stPro.inverterqty2 != "")
                            {
                                Secondinv = Convert.ToInt32(stPro.inverterqty2);
                            }
                            if (stPro.inverterqty3 != null && stPro.inverterqty3 != "")
                            {
                                thirdinv = Convert.ToInt32(stPro.inverterqty3);
                            }
                            int TotalInv = FirstInv + Secondinv + thirdinv;
                            bool s2 = ClstblPicklist.tbl_picklistlog_UpdateInverter(PicklistID, TotalInv.ToString());
                        }
                    }
                }
            }

            Notification("Transaction Successfull..");
            BindPickList();
            Reset();
            ///btnSavePickList.Visible = true;
            // btnUpdatePicklist.Visible = false;
            HidePanels();
            BindGrid(0);
        }

    }

    public void BindPickList()
    {
        BindGrid(0);
    }

    protected void lnkDeletePickList_Click(object sender, EventArgs e)
    {
        string PicklistID = hnd1.Value;
        //string PicklistID = GridView1.DataKeys.ToString();

        if (PicklistID != "")
        {
            try
            {
                ClstblPicklist.tbl_PickListLog_DeleteByID(PicklistID);
                ClstblPicklist.tbl_PicklistItemsDetail_Delete(PicklistID);
                Notification("Transaction Successfull..");
                BindPickList();

                string ProjectID = Request.QueryString["proid"];
                SttblProjects stPro = ClstblPicklist.tblProjects_SelectByProjectNumber(txtProjectNo.Text);
                BindRptPickListWithdata(stPro, txtProjectNo.Text, ddlCompany.SelectedValue);
            }
            catch (Exception ex)
            {
                Notification("Transaction Failed..");
            }
        }
        else
        {
            Notification("Transaction Failed..");
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string PickListID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hnd1.Value = PickListID;

        int tblMaintanHistoryLogExist = ClstblPicklist.tblMaintainHistory_ExistsBySectionID(PickListID);

        if (tblMaintanHistoryLogExist == 0) //Write Delete Code
        {
            lblMassage.Text = "Are You Sure Delete This Picklist?";
            hndPickDelete.Value = PickListID;
            lnkDeletePickList.Visible = true;
            ModalPopupExtenderDelete.Show();
        }
        else
        {
            lblMassage.Text = "Picklist Log Already Exists..";
            lnkDeletePickList.Visible = false;
            ModalPopupExtenderDelete.Show();
        }

        ModalPopupExtenderDelete.Show();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void lbtnCancel_Click(object sender, EventArgs e)
    {
        Reset();
        HidePanels();
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "Old Picklist_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        int[] ColList = { 1, 0, 13, 2, 3, 4,
            5, 6, 7, 8, 9 };

        string[] arrHeader = { "Project No", "Picklist ID", "Project Status", "Picklist Type", "Generated Date", "Install Date", "Installer Name", "System Detail", "Reason", "Created By", "Deduct On" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }
}
