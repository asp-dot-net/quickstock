﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NoTraceStock.aspx.cs" Inherits="admin_adminfiles_ReportsV2_NoTraceStock"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style>
                .modal-dialog1 {
                    margin-left: -300px;
                    margin-right: -300px;
                    width: 985px;
                }

                .focusred {
                    border-color: #FF5F5F !important;
                }

                .padd_btm10 {
                    padding-bottom: 15px;
                }

                .height100 {
                    height: 100px;
                }

                .autocomplete_completionListElement {
                    z-index: 9999999 !important;
                }
            </style>

            <script type="text/javascript">


                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        //ShowProgress();
                    });
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();

                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //alert("dgfdg2");


                    $(".AriseInstaller .dropdown dt a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
                    });
                    $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").hide();
                    });

                    $(".Location .dropdown dt a").on('click', function () {
                        $(".Location .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Location .dropdown dd ul li a").on('click', function () {
                        $(".Location .dropdown dd ul").hide();
                    });

                }
                function pageLoaded() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true,
                    });

                    $(".myvalinvoiceissued").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});
                    //$(".myval").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox3();
                    //});

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox1();
                    //});
                }


                function stopRKey(evt) {
                    var evt = (evt) ? evt : ((event) ? event : null);
                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
                }
                document.onkeypress = stopRKey;

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
            </script>

            <script>

                $(document).ready(function () {

                });
                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            // alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
            </script>

            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <script>

                <%--function callMultiCheckbox3() {
                    var title = "";
                    $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel2').show();
                        $('.multiSel2').html(html);
                        $(".hida2").hide();
                    }
                    else {
                        $('#spanselect2').show();
                        $('.multiSel2').hide();
                    }

                }--%>

                <%--function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddProjectStatus.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }--%>

            </script>
            <div class="page-header card">
                <div class="card-block">
                    <h5>No Trace Stock -
                        <asp:Label ID="lblUpdatedMsg" runat="server" />
                        <div class="pull-right" runat="server" visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                            <asp:Button Text="Non Resolve" runat="server" ID="btnOpenPopUp" class="btn btn-warning" OnClick="btnOpenPopUp_Click" />
                            <asp:Button Text="Credit" runat="server" ID="btnOpenCreditPopUp" class="btn btn-warning" OnClick="btnOpenCreditPopUp_Click" visible='<%# Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager") ? true : false %>' />
                            <asp:Button Text="Assign ProjectNo" runat="server" ID="btnAssignProjOpenPopUp" class="btn btn-warning" OnClick="btnAssignProjOpenPopUp_Click" />
                            <asp:Button Text="Update BS Manual" runat="server" ID="btnUpdateBSManual" class="btn btn-warning" OnClick="btnUpdateBSManual_Click" />
                            <asp:Button Text="Notes" runat="server" ID="btnNotes" class="btn btn-warning" OnClick="btnNotes_Click" Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>' />
                        </div>
                    </h5>

                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">

                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                            <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                            <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                            <%--<asp:ListItem Value="3">Wholesale</asp:ListItem>--%>
                                                            <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers, Custom" ValidChars="," />
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Manager</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlPendingAudiYN" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Resolve ALL</asp:ListItem>
                                                            <asp:ListItem Value="1">Resolve Yes</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">Resolve No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <%--<asp:ListItem Value="">Date</asp:ListItem>--%>
                                                            <asp:ListItem Value="1" Selected="True">Deducted</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;" runat="server"
                                                    visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                    <asp:LinkButton ID="lbtnFetch" runat="server" class="btn btn-primary btn-xs btnclear fullWidth"
                                                        OnClick="lbtnFetch_Click" CausesValidation="false"><i class="fa fa-refresh"></i> Fetch </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                            <asp:PostBackTrigger ControlID="lbtnFetch" />
                            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
                            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div>
                <div class="card shadownone brdrgray" id="divtot" runat="server">
                    <div class="card-block">
                        <div class="table-responsive BlockStructure">
                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"></th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert">Net Out</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Found in Arise BSGB">Arise BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Found in SolarMiner BSGB">SM BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Found in Wholesale BSGB">WO BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Panel not scan in project">Scan Required</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo not found in QuickStock">Other Inventory</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo used from Wholesale deduct">Other Wholesale</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo Credit given to Installer">Credit</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Can't find SerialNo">Non Resolve</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo with Installer">With Installer</th>
                                        <%--<th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="">Other Revert</th>--%>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Net Out - BSGB - Credit">Future Installation</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;"
                                            data-toggle="tooltip" data-placement="Top" data-original-title="Net Out - BSGB - Scan Required - Credit">Pending Audit</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;" >Actual Audit</th>
                                    </tr>
                                    <tr class="brd_ornge">
                                        <td align="left" valign="top">Total</td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypNetOut" runat="server" OnClick="TotalExportToExcel" CommandArgument="Net Out" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypAriseBSGB" runat="server" OnClick="TotalExportToExcel" CommandArgument="Arise BSGB" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypSMBSGB" runat="server" OnClick="TotalExportToExcel" CommandArgument="SM BSGB" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypWOBSGB" runat="server" OnClick="TotalExportToExcel" CommandArgument="WO BSGB" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="lbtnScanRequired" runat="server" OnClick="lbtnScanRequired_Click" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypOtherInventory" runat="server" OnClick="TotalExportToExcel" CommandArgument="Other Inventory" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypWholesale" runat="server"/>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypCredit" runat="server" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypNonResolve" runat="server" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypWithInstaller" runat="server" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypPendingInstallation" runat="server" />
                                        </td>
                                        <td align="left" valign="top">
                                            <%--<asp:HyperLink ID="HypPendingAudit" runat="server" Target="_blank" />--%>
                                            <asp:LinkButton ID="HypPendingAudit" runat="server" OnClick="HypPendingAudit_Click" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="HypActualAudit" runat="server" />
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="InstallerID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Installer Name" SortExpression="InstallerName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallerName" runat="server" data-toggle="tooltip" data-placement="Top" title=""
                                                            data-original-title='<%#Eval("InstallerName")%>'>
                                                           <%#Eval("InstallerName")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"
                                                            SortExpression="NetOut">
                                                           Net Out
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblNetOut" runat="server" Text='<%#Eval("NetOut")%>' Enabled='<%# Eval("NetOut").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Found in Arise BSGB" SortExpression="AriseInstall">
                                                           Arise BSGB
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblAriseInstall" runat="server" Text='<%#Eval("AriseInstall")%>' Enabled='<%# Eval("AriseInstall").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Found in SolarMiner BSGB" SortExpression="SMInstall">
                                                           SM BSGB
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblSMBSGB" runat="server" Text='<%#Eval("SMInstall")%>' Enabled='<%# Eval("SMInstall").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Found in Wholesale BSGB" SortExpression="WOInstall">
                                                           WO BSGB
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblWOBSGB" runat="server" Text='<%#Eval("WOInstall")%>' Enabled='<%# Eval("WOInstall").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Panel not scan in project" SortExpression="ScanRequired">
                                                           Scan Required
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScanRequired" runat="server" Text='<%#Eval("ScanRequired")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo not found in QuickStock" SortExpression="OtherInventory">
                                                           Other Inventory
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOtherInventory" runat="server" Text='<%#Eval("OtherInventory")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo used from Wholesale deduct" SortExpression="Wholesale">
                                                           Other Wholesale
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblWholesale" runat="server" Text='<%#Eval("OtherWholesale")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo Credit given to Installer" SortExpression="Credit">
                                                           Credit
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Can't find SerialNo" SortExpression="NonResolved">
                                                           Non Resolve
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNonResolved" runat="server" Text='<%#Eval("NonResolved")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo With Installer" SortExpression="WithInstaller">
                                                           With Installer
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HypWithInstaller" runat="server" Text='<%#Eval("WithInstaller")%>' Enabled='<%# Eval("WithInstaller").ToString() == "0" ? false:true%>'
                                                            NavigateUrl='<%# "~/admin/adminfiles/ReportsV2/Details/WithInstallerDetails.aspx?installerName=" + Eval("InstallerName") 
                                                                + "&installerId=" + Eval("InstallerID") + "&companyId=" + ddlCompany.SelectedValue + "&projNo=" + txtProjectNumber.Text
                                                                + "&location=" + ddlLocation.SelectedValue + "&dateType=" + ddlDateType.SelectedValue + "&startDate=" + txtStartDate.Text
                                                                + "&endDate=" + txtEndDate.Text %>'
                                                            Target="_blank" />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Net Out - BSGB - Credit" SortExpression="PendingInstallation">
                                                           Future Installation
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPendingInstallation" runat="server" Text='<%#Eval("PendingInstallation")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Net Out - BSGB - Scan Required - Credit" SortExpression="PendingAudit">
                                                           Pending Audit
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPendingAudit" runat="server" Text='<%#Eval("PendingAudit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText ="Actual Audit" SortExpression ="ActualAudit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActualAudit" runat="server" Text='<%#Eval("ActualAudit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <%--<asp:HyperLink ID="hypDetail" runat="server" CausesValidation="false"
                                                            CssClass="btn btn-primary btn-mini" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetails.aspx?Contact=" + Eval("Contact") 
                                                                + "&ContactID=" + Eval("ContactID") + "&DateType=" + ddlDateType.SelectedValue + "&startdate=" + txtStartDate.Text + "&enddate=" + txtEndDate.Text + "&Page=Out" 
                                                                + "&CompanyID=" + ddlCompany.SelectedValue + "&ProjectNo=" + txtProjectNumber.Text + "&PS=" + GetProjectStatus() 
                                                                + "&CompanyLocation=" + ddlLocation.SelectedValue + "&Category=" + ddlCategory.SelectedValue 
                                                                + "&PicklistCreatedBy=" + ddlPicklistCreatedBy.SelectedValue %>'>
                                                            <i class="fa fa-link"></i>Detail
                                                        </asp:HyperLink>--%>

                                                        <asp:LinkButton ID="lbtnExportV2" runat="server" class="btn btn-success btn-mini Excel" OnClick="lbtnExportV2_Click" CommandArgument='<%#Eval("InstallerID") + ";" + Eval("InstallerName")%>'
                                                            CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>

                                                        <asp:LinkButton ID="lbtnNotes" runat="server" class="btn btn-warning btn-mini" OnClick="lbtnNotes_Click" CommandArgument='<%#Eval("InstallerID")%>'
                                                            CausesValidation="false"><i class="fa fa-edit"></i> Note </asp:LinkButton>

                                                      <%--  <asp:LinkButton ID="lbtnNotesGren" runat="server" class="btn btn-success btn-mini" OnClick="lbtnNotes_Click" CommandArgument='<%#Eval("InstallerID")%>'
                                                            CausesValidation="false" Visible='<%# Eval("NotesFlag").ToString() != "0" ? true : false %>'><i class="fa fa-edit"></i> Note </asp:LinkButton>--%>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupMissingLog" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divMissingLog" TargetControlID="Button9" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divMissingLog" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Missing Log</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">

                                        <div class="col-sm-8">
                                            <span class="name disblock">
                                                <label>
                                                    Missing
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlMissing" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" Width="90%">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="Missing By Installer">Missing By Installer</asp:ListItem>
                                                        <asp:ListItem Value="Missed By Transpotation">Missed By Transpotation</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlMissing" Display="Dynamic" ValidationGroup="MissingItem"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtSerialNo" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" placeholder="multiple serial no eg: STAKD1254634,STAKD1254634" ValidationGroup="MissingItem"></asp:TextBox>
                                                    <%--<cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSerialNo"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSerialNo"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>--%>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtSerialNo" Display="Dynamic" ValidationGroup="MissingItem"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSave" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                OnClick="btnSave_Click" ValidationGroup="MissingItem" CausesValidation="true"></asp:Button>
                            <asp:Button ID="btncancelpvd" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button9" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupCreditSerialNo" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divCreditSerialNo" TargetControlID="Button3" CancelControlID="Button2">
            </cc1:ModalPopupExtender>
            <div id="divCreditSerialNo" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Credit Log</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Credit
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlCreditedIn" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" Width="90%">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Other Inventory</asp:ListItem>
                                                        <asp:ListItem Value="2">Wholesale</asp:ListItem>
                                                        <asp:ListItem Value="3">Purchase</asp:ListItem>
                                                        <asp:ListItem Value="4">OtherRevert</asp:ListItem>
                                                        <asp:ListItem Value="5">Bad Credit</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlCreditedIn" Display="Dynamic" ValidationGroup="CreditItem" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtCreditSerialNo" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" placeholder="multiple serial no eg: STAKD1254634,STAKD1254634" ValidationGroup="CreditItem"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtCreditSerialNo"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars=" ">
                                                    </cc1:FilteredTextBoxExtender>
                                                    <%--<cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtCreditSerialNo"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>--%>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtCreditSerialNo" Display="Dynamic" ValidationGroup="CreditItem" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt" runat="server" id="DivCreditAgainst">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Credit against
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate" style="padding-bottom: 10px;">
                                                    <asp:DropDownList ID="ddlCreditAgainst" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" Width="90%">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Pending Audit</asp:ListItem>
                                                        <asp:ListItem Value="2">Non Resolve</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlCreditAgainst" Display="Dynamic" ValidationGroup="CreditItem" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtCreditAgainst" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" placeholder="multiple serial no eg: STAKD1254634,STAKD1254634" ValidationGroup="CreditItem"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtCreditAgainst" Display="Dynamic" ValidationGroup="CreditItem" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtCreditAgainst"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars=" ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSaveCreedit" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                OnClick="btnSaveCreedit_Click" ValidationGroup="CreditItem" CausesValidation="true"></asp:Button>
                            <asp:Button ID="Button2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button3" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="MPAssignProjectNo" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divAssign" TargetControlID="Button5" CancelControlID="Button4">
            </cc1:ModalPopupExtender>
            <div id="divAssign" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Assign Project No</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-4">
                                            <span class="name disblock">
                                                <label>
                                                    Company
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlAssignCompany" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" Width="90%">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                        <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                        <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlAssignCompany" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            <span class="name disblock">
                                                <label>
                                                    Project No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAssignProjectNo" runat="server" CssClass="form-control" ValidationGroup="ReqAssign"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtAssignProjectNo" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>

                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtAssignProjectNo" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="getAllProjectNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAssignSerialNo" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" ValidationGroup="ReqAssign"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtAssignSerialNo" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnAssign" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                ValidationGroup="ReqAssign" CausesValidation="true" OnClick="btnAssign_Click"></asp:Button>
                            <asp:Button ID="Button4" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button5" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="mpNotes" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divNotes" TargetControlID="Button7" CancelControlID="Button6">
            </cc1:ModalPopupExtender>
            <div id="divNotes" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Notes</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Project No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtNotesProjectNo" runat="server" CssClass="form-control" ValidationGroup="ReqNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtNotesProjectNo" Display="Dynamic" ValidationGroup="ReqNotes"></asp:RequiredFieldValidator>

                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtNotesProjectNo" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="getAllProjectNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-6">
                                            <span class="name disblock">
                                                <label>
                                                    Stock With
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlStockWith" runat="server" aria-controls="DataTables_Table_0" CssClass="form-control"
                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockWith_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">With Installer</asp:ListItem>
                                                        <asp:ListItem Value="2">With Customer</asp:ListItem>
                                                        <asp:ListItem Value="3">With Transportation</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlStockWith" Display="Dynamic" ValidationGroup="ReqNotes"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-6" runat="server" id="DivDepo" visible="false">
                                            <span class="name disblock">
                                                <label>
                                                    Transport Depo
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                   <asp:DropDownList ID="ddlTransportDepo" runat="server" aria-controls="DataTables_Table_0" CssClass="form-control"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Notes
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" ValidationGroup="ReqNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtNotes" Display="Dynamic" ValidationGroup="ReqNotes"></asp:RequiredFieldValidator>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSaveNotes" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                ValidationGroup="ReqNotes" CausesValidation="true" OnClick="btnSaveNotes_Click"></asp:Button>
                            <asp:Button ID="Button6" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndInsatllerId" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="div1" TargetControlID="Button10" CancelControlID="Button8">
            </cc1:ModalPopupExtender>
            <div id="div1" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">
                            Update BSGB Manual
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Project No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtProject" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnBSGBManual" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Fatch"
                                CausesValidation="false" OnClick="btnBSGBManual_Click"></asp:Button>
                            <asp:Button ID="Button8" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button10" Style="display: none;" runat="server" />

            <asp:Button ID="Button1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="div2" TargetControlID="Button1" CancelControlID="Button12">
            </cc1:ModalPopupExtender>
            <div id="div2" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Serial No Notes</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtNotesSerialNo" runat="server" CssClass="form-control" ValidationGroup="ReqSerialNoNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtNotesSerialNo" Display="Dynamic" ValidationGroup="ReqSerialNoNotes"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Notes
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtSerialNoNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" ValidationGroup="ReqSerialNoNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtSerialNoNotes" Display="Dynamic" ValidationGroup="ReqSerialNoNotes"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSaveSerialNoNotes" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                ValidationGroup="ReqSerialNoNotes" CausesValidation="true" OnClick="btnSaveSerialNoNotes_Click"></asp:Button>
                            <asp:Button ID="Button12" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="GridView1" />
            <asp:PostBackTrigger ControlID="lbtnScanRequired" />
            <asp:PostBackTrigger ControlID="HypPendingAudit" />
            <asp:PostBackTrigger ControlID="HypNetOut" />
            <asp:PostBackTrigger ControlID="HypAriseBSGB" />
            <asp:PostBackTrigger ControlID="HypSMBSGB" />
            <asp:PostBackTrigger ControlID="HypWOBSGB" />
            <asp:PostBackTrigger ControlID="HypOtherInventory" />
            <asp:PostBackTrigger ControlID="HypWholesale" />
            <asp:PostBackTrigger ControlID="HypCredit" />
            <asp:PostBackTrigger ControlID="HypNonResolve" />
            <asp:PostBackTrigger ControlID="HypWithInstaller" />
            <asp:PostBackTrigger ControlID="HypPendingInstallation" />

        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {
            HighlightControlToValidate();


        });



        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }

        // For Multi Select //
        $(".AriseInstaller .dropdown dt a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").hide();
        });

        $(".Location .dropdown dt a").on('click', function () {
            $(".Location .dropdown dd ul").slideToggle('fast');
        });
        $(".Location .dropdown dd ul li a").on('click', function () {
            $(".Location .dropdown dd ul").hide();
        });

        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>
</asp:Content>
