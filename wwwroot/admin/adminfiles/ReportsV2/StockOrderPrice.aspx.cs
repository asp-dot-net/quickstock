﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_StockOrderPrice : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropDown();

            //ddlDateType.SelectedValue = "2";
            //txtStartDate.Text = "01/10/2021";
            //txtEndDate.Text = "31/10/2021";
            //txtStockItem.Text = "Jinko";
            BindGrid(0);
        }

    }

    public string GetProjectStatus()
    {
        string ProjectStatus = "";
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnProjectStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                ProjectStatus += "," + hdnModule.Value.ToString();
            }
        }
        if (ProjectStatus != "")
        {
            ProjectStatus = ProjectStatus.Substring(1);
        }

        return ProjectStatus;
    }

    protected DataTable GetGridData1()
    {
        DataTable dt = new DataTable();

        dt = ClsReportsV2.SP_StockOrderPrice_GetDataV2(ddlVender.SelectedValue, ddlPurchaseCompany.SelectedValue, txtStockItem.Text, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtOrderNo.Text, ddlGSTType.SelectedValue, ddlCategory.SelectedValue);

        return dt;
    }

    public void BindHeader(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            //string Qty = dt.Compute("SUM(OrderQuantity)", string.Empty).ToString();
            //lblQty.Text = Qty;

            //string USD = dt.Compute("SUM(USDAmount)", string.Empty).ToString();
            //lblUSD.Text = USD != "" ? Convert.ToDecimal(USD).ToString("F") : "0.00";

            //decimal AvgPrice = (USD != "" ? Convert.ToDecimal(USD) : 0) / Convert.ToDecimal(Qty == "0.00" || Qty == "0" ? "1" : Qty);
            //lblAvgPrice.Text = AvgPrice.ToString("F");

            //string KW = dt.Compute("SUM(KW)", string.Empty).ToString();
            //lblKW.Text = KW;

            //string ReceiveQty = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            //lblReceiveQty.Text = ReceiveQty;

            //string ReceiveUSD = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("USDAmount")).ToString("F");
            //lblReceiveUSD.Text = ReceiveUSD;

            //decimal ReceiveAvgPrice = (ReceiveUSD != "" ? Convert.ToDecimal(ReceiveUSD) : 0) / Convert.ToDecimal(ReceiveQty == "0.00" || ReceiveQty == "0" ? "1" : ReceiveQty);
            //lblReceiveAvgPrice.Text = ReceiveAvgPrice.ToString("F");

            //string ReceiveKW = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("KW")).ToString("F");
            //lblReceiveKW.Text = ReceiveKW;

            //string NotReceiveQty = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            //lblNotReceiveQty.Text = NotReceiveQty;

            //string NotReceiveUSD = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("USDAmount")).ToString("F");
            //lblNotReceiveUSD.Text = NotReceiveUSD;

            //decimal NotReceiveAvgPrice = (NotReceiveUSD != "" ? Convert.ToDecimal(NotReceiveUSD) : 0) / Convert.ToDecimal(NotReceiveQty == "0.00" || NotReceiveQty == "0" ? "1" : NotReceiveQty);
            //lblNotReceiveAvgPrice.Text = NotReceiveAvgPrice.ToString("F");

            //string NotReceiveKW = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("KW")).ToString();
            //lblNotReceiveKW.Text = NotReceiveKW;

            //string paidQty = dt.AsEnumerable().Where(y => y.Field<int>("FullPayment") == 1).Sum(x => x.Field<int>("OrderQuantity")).ToString("F");
            //lblPaidQty.Text = paidQty;

            //string paidUSD = dt.AsEnumerable().Where(y => y.Field<int>("FullPayment") == 1).Sum(x => x.Field<decimal>("USDAmount")).ToString("F");
            //lblPaidUSD.Text = paidUSD;

            //decimal PaidUSDAvgPrice = (paidUSD != "" ? Convert.ToDecimal(paidUSD) : 0) / Convert.ToDecimal(paidQty == "0.00" || paidQty == "0" ? "1" : paidQty);
            //lblPaidUSDAvgPrice.Text = PaidUSDAvgPrice.ToString("F");

            //string paidAUD = dt.AsEnumerable().Where(y => y.Field<int>("FullPayment") == 1).Sum(x => x.Field<decimal>("Deposit_amountAsd")).ToString("F");
            //lblPaidAUD.Text = paidAUD;

            //decimal PaidAUDAvgPrice = (paidAUD != "" ? Convert.ToDecimal(paidAUD) : 0) / Convert.ToDecimal(paidQty == "0.00" || paidQty == "0" ? "1" : paidQty);
            //lblPaidAUDAvgPrice.Text = PaidAUDAvgPrice.ToString("F");

            //string paidKW = dt.AsEnumerable().Where(y => y.Field<int>("FullPayment") == 1).Sum(x => x.Field<decimal>("KW")).ToString("F");
            //lblPaidKW.Text = paidKW;

            //Total Qty
            string totalQty = dt.Compute("SUM(OrderQuantity)", string.Empty).ToString();
            lblTotalQty.Text = totalQty;

            //Total USD Qty
            string totalUSDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD").Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblTotalUSDQty.Text = totalUSDQty;

            // Toatl USD Amount
            string totalUSD = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD").Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblTotalUSDAmount.Text = totalUSD != "" ? Convert.ToDecimal(totalUSD).ToString("F") : "0.00";

            // Avg USD Without GST
            decimal avgUSD = (totalUSD != "" ? Convert.ToDecimal(totalUSD) : 0) / Convert.ToDecimal(totalUSDQty == "0.00" || totalUSDQty == "0" ? "1" : totalUSDQty);
            lblAvgUSD.Text = avgUSD.ToString("F");

            //Total USD Price Par Watt
            int USDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD").Count();

            string USDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD").Sum(x => x.Field<decimal>("PriceParWatt")).ToString();
            lblUSDPriceParWatt.Text = USDPriceParWatt != "" && USDPriceParWatt != "0" ? (Convert.ToDecimal(USDPriceParWatt) / USDPriceParWattCount).ToString("F") : "0.00";

            // USD KW
            string USDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD").Sum(x => x.Field<decimal>("KW")).ToString();
            lblTotalUSDKW.Text = USDKw.ToString();

            //Total AUD Qty
            string totalAUDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD").Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblTotalAUDQty.Text = totalAUDQty;

            // Toatl AUD Amount
            string totalAUD = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD").Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblTotalAUDAmount.Text = totalAUD != "" ? Convert.ToDecimal(totalAUD).ToString("F") : "0.00";

            // Avg AUD Without GST
            decimal avgAUD = (totalAUD != "" ? Convert.ToDecimal(totalAUD) : 0) / Convert.ToDecimal(totalAUDQty == "0.00" || totalAUDQty == "0" ? "1" : totalAUDQty);
            lblAvgAUD.Text = avgAUD.ToString("F");

            //Total AUD Price Par Watt
            int AUDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD").Count();

            string AUDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD").Sum(x => x.Field<decimal>("PriceParWatt")).ToString();
            lblAUDPriceParWatt.Text = AUDPriceParWatt != "" && AUDPriceParWatt != "0" ? (Convert.ToDecimal(AUDPriceParWatt) / AUDPriceParWattCount).ToString("F") : "0.00";

            // AUD KW
            string AUDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD").Sum(x => x.Field<decimal>("KW")).ToString();
            lblTotalAUDKW.Text = AUDKw.ToString();

            //Received Count
            // Received Qty
            string totalReceiveQty = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblTotalReceivedQty.Text = totalReceiveQty;

            // Received USD Qty
            string receivedUSDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblReceivedUSDQty.Text = receivedUSDQty;

            // Received USD Amount
            string receivedUSDAmt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblReceivedUSDAmt.Text = receivedUSDAmt != "" ? Convert.ToDecimal(receivedUSDAmt).ToString("F") : "0.00";

            // Avg Received USD Without GST
            decimal avgReceivedUSD = (receivedUSDAmt != "" ? Convert.ToDecimal(receivedUSDAmt) : 0) / Convert.ToDecimal(receivedUSDQty == "0.00" || receivedUSDQty == "0" ? "1" : receivedUSDQty);
            lblAvgReceivedUSD.Text = avgReceivedUSD.ToString("F");

            // Receved USD Price Par Watt
            int receivedUSDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Count();

            string receivedUSDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("PriceParWatt")).ToString();
            lblReceivedUSDPriceParWatt.Text = receivedUSDPriceParWatt != "" && receivedUSDPriceParWatt != "0" ? (Convert.ToDecimal(receivedUSDPriceParWatt) / receivedUSDPriceParWattCount).ToString("F") : "0.00";

            // Received USD KW
            string receivedUSDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("KW")).ToString();
            lblReceivedUSDKW.Text = receivedUSDKw.ToString();

            // Received AUD Qty
            string receivedAUDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblReceivedAUDQty.Text = receivedAUDQty;

            // Received AUD Amount
            string receivedAUDAmt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblReceivedAUDAmt.Text = receivedAUDAmt != "" ? Convert.ToDecimal(receivedAUDAmt).ToString("F") : "0.00";

            // Avg Received AUD Without GST
            decimal avgReceivedAUD = (receivedAUDAmt != "" ? Convert.ToDecimal(receivedAUDAmt) : 0) / Convert.ToDecimal(receivedAUDQty == "0.00" || receivedAUDQty == "0" ? "1" : receivedAUDQty);
            lblAvgReceivedAUD.Text = avgReceivedAUD.ToString("F");

            // Receved AUD Price Par Watt
            int receivedAUDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Count();

            string receivedAUDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("PriceParWatt")).ToString();
            lblReceivedAUDPriceParWatt.Text = receivedAUDPriceParWatt != "" && receivedAUDPriceParWatt != "0" ? (Convert.ToDecimal(receivedAUDPriceParWatt) / receivedAUDPriceParWattCount).ToString("F") : "0.00";

            // Received AUD KW
            string receivedAUDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") != null).Sum(x => x.Field<decimal>("KW")).ToString();
            lblReceivedAUDKW.Text = receivedAUDKw.ToString();

            //Pending Count
            // Pending Qty
            string totalPendingQty = dt.AsEnumerable().Where(y => y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblTotalPendingQty.Text = totalPendingQty;

            // Pending USD Qty
            string pendingUSDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblPendingUSDQty.Text = pendingUSDQty;

            // Pending USD Amount
            string pendingUSDAmt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblPendingUSDAmt.Text = pendingUSDAmt != "" ? Convert.ToDecimal(pendingUSDAmt).ToString("F") : "0.00";

            // Avg Pending USD Without GST
            decimal avgPendingUSD = (pendingUSDAmt != "" ? Convert.ToDecimal(pendingUSDAmt) : 0) / Convert.ToDecimal(pendingUSDQty == "0.00" || pendingUSDQty == "0" ? "1" : pendingUSDQty);
            lblAvgPendingUSD.Text = avgPendingUSD.ToString("F");

            // Pending USD Price Par Watt
            int pengingUSDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Count();

            string pengingUSDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("PriceParWatt")).ToString();

            lblPendingUSDPriceParWatt.Text = pengingUSDPriceParWatt != "" && pengingUSDPriceParWatt != "0" ? (Convert.ToDecimal(pengingUSDPriceParWatt) / pengingUSDPriceParWattCount).ToString("F") : "0.00";

            // Pending USD KW
            string pendingUSDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "USD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("KW")).ToString();
            lblPendingUSDKW.Text = pendingUSDKw.ToString();

            // Pending AUD Qty
            string pendingAUDQty = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<int>("OrderQuantity")).ToString();
            lblPendingAUDQty.Text = pendingAUDQty;

            // Pending AUD Amount
            string pendingAUDAmt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("USDAmount")).ToString();
            lblPendingAUDAmt.Text = pendingAUDAmt != "" ? Convert.ToDecimal(pendingAUDAmt).ToString("F") : "0.00";

            // Avg Pending AUD Without GST
            decimal avgPendingAUD = (pendingAUDAmt != "" ? Convert.ToDecimal(pendingAUDAmt) : 0) / Convert.ToDecimal(pendingAUDQty == "0.00" || pendingAUDQty == "0" ? "1" : pendingAUDQty);
            lblAvgPendingAUD.Text = avgPendingAUD.ToString("F");

            // Pending AUD Price Par Watt
            int pengingAUDPriceParWattCount = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Count();

            string pengingAUDPriceParWatt = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("PriceParWatt")).ToString();
            lblPendingAUDPriceParWatt.Text = pengingAUDPriceParWatt != "" && pengingAUDPriceParWatt != "0" ? (Convert.ToDecimal(pengingAUDPriceParWatt) / pengingAUDPriceParWattCount).ToString("F") : "0.00";

            // Pending AUD KW
            string pendingAUDKw = dt.AsEnumerable().Where(y => y.Field<string>("Currency") == "AUD" && y.Field<Nullable<DateTime>>("ActualDelivery") == null).Sum(x => x.Field<decimal>("KW")).ToString();
            lblPendingAUDKW.Text = pendingAUDKw.ToString();
        }
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            DivTotal.Visible = false;
            divnopage.Visible = false;
            //divnopage1.Visible = false;
        }
        else
        {
            //divnopage1.Visible = true;
            PanGrid.Visible = true;
            DivTotal.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindHeader(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + ((dt.Rows.Count) - 1) + " entries";
                    ltrPage.Text = "Showing " + ((dt.Rows.Count)) + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInverterFunction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlVender.SelectedValue = "";
        ddlPurchaseCompany.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtStockItem.Text = string.Empty;
        txtOrderNo.Text = string.Empty;
        ddlGSTType.SelectedValue = "";
        ddlCategory.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView rowView = (DataRowView)e.Row.DataItem;
            //string Contact = rowView["Contact"].ToString();
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                //GridViewRow gvrow = GridView1.BottomPagerRow;
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    //int iTotalRecords = (dv.ToTable().Rows.Count) - 1;
                    int iTotalRecords = (dv.ToTable().Rows.Count);
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        Export oExport = new Export();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        string FileName = "Stock Order Price_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        //int[] ColList = { 10, 2, 4, 5, 6, 7, 8, 9,
        //                    14, 11, 12, 13, 15, 16 };

        //string[] arrHeader = { "Order No", "Stock Item", "Location", "PurCompany", "Vendor", "Unit Price", "Quantity", "Order Date",    
        //                        "GST Type", "Amt(USD)", "Paid(USD)", "Rem(USD)", "Paid(AUD)", "Paid(AUD) with GST" };

        int[] ColList = { 10, 9, 5, 6, 4, 2, 8, 12,
                          15, 13, 17, 18
                          , 19, 20 };

        string[] arrHeader = { "Order No", "Order Date", "Pur. Company", "Vendor", "Location", "Stock Item", "Quantity", "Currency",
                                "Amount", "Paid(USD)", "AvgPriceUnpaid(USD)", "AvgPricePaid(USD)"
                                , "AvgPricePaidAUD", "AvgPricePaidAUD" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void BindDropDown()
    {
        DataTable dtVender = ClstblContacts.tblCustType_SelectVender();
        ddlVender.DataSource = dtVender;
        ddlVender.DataTextField = "Customer";
        ddlVender.DataValueField = "CustomerID";
        ddlVender.DataBind();

        DataTable dtPurComp = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        ddlPurchaseCompany.DataSource = dtPurComp;
        ddlPurchaseCompany.DataTextField = "PurchaseCompanyName";
        ddlPurchaseCompany.DataValueField = "Id";
        ddlPurchaseCompany.DataBind();

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataSource = dtStockCategory;
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataBind();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptProjectStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }
}