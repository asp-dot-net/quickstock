using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_ReportsV2_InstalledSerailNo : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropdown();

            ddlCategory.SelectedValue = "1";

            BindGrid(0);
        }
    }

    public void BindDropdown()
    {
        ddlCategory.DataSource = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlCategory.DataValueField = "StockCategoryID";
        ddlCategory.DataTextField = "StockCategory";
        ddlCategory.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_Select_all();
        ddlInstaller.DataValueField = "fullname";
        ddlInstaller.DataTextField = "fullname";
        ddlInstaller.DataBind();

        ddlLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataBind();
    }

    protected DataTable GetGridData1()
    {
        DataTable dt = new DataTable();

        dt = ClsReportsV2.SP_InstalledSerailNo(ddlCategory.SelectedValue, txtStockItem.Text, ddlInstaller.SelectedValue, ddlSerialNoFlag.SelectedValue, ddlLocation.SelectedValue);

        BindTotal(dt);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            lbtnExport2.Visible = true;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int Out = Convert.ToInt32(dt.Compute("SUM(Unused)", string.Empty));
            int BSGB = Convert.ToInt32(dt.Compute("SUM(UsedBSGB)", string.Empty));
            int Diff = Convert.ToInt32(dt.Compute("SUM(Diff)", string.Empty));
            //int Revert = Convert.ToInt32(dt.Compute("SUM(Revert)", string.Empty));
            //int Audit = Convert.ToInt32(dt.Compute("SUM(Audit)", string.Empty));
            lblTotalOut.Text = Out.ToString();
            lblTotalBSGB.Text = BSGB.ToString();
            HypDifference.Text = Diff.ToString();

            //HypDifference.Enabled = HypDifference.Text == "0" ? false : true;
            //HypDifference.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetailsProj.aspx?StockItem=" + "Total" + "&StockItemID=&DateType=" + DateType + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Audit" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&InstallationDate=" + InstallationDate + "&Category=" + Category;

        }
        else
        {
            lblTotalOut.Text = "0";
            lblTotalBSGB.Text = "0";
            HypDifference.Text = "0";
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStockItem.Text = string.Empty;
        ddlCategory.ClearSelection();

        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View SerialNo")
        {
            string arg = e.CommandArgument.ToString();
            string StockItemID = arg;
            hndStockItemID.Value = arg;

            DataTable dtSerialNo = getSerialNoDetails(StockItemID);

            RptSerialNo.DataSource = dtSerialNo;
            RptSerialNo.DataBind();
            ModalPopupExtenderSerialNo.Show();
        }
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        //DataTable dt = GetGridData1();

        //try
        //{
        //    Export oExport = new Export();
        //    string[] columnNames = dt.Columns.Cast<DataColumn>()
        //                             .Select(x => x.ColumnName)
        //                             .ToArray();

        //    string FileName = "Installed Serail No - " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        //    int[] ColList = { 1, 2, 3, 4, 5, 6 };

        //    string[] arrHeader = { "Stock Item", "Stock Model", "Category", "Unused", "UsedBSGB", "Diff" };
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception ex)
        //{

        //}


        try
        {
            DataTable dt = GetGridData1();
            dt.Columns.Remove("StockItemID");
            DataTable dt1 = GetSerialNoStockLocationWise();

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Installed SerailNo");
                wb.Worksheets.Add(dt1, "Installed SerailNo Location");

                string FileName = "Installed Serail No - " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            //Notification(ex.Message);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;
                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)e.Row.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)e.Row.FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            try
            {
                //string CompanyID = Request.QueryString["CompanyID"].ToString();
                Label lblName = (Label)e.Row.FindControl("lblName");

                if (ddlSerialNoFlag.SelectedValue == "1") //Unused
                {
                    lblName.Text = "Unused Qty";
                }
                else
                {
                    lblName.Text = "Missing Qty";
                }
            }
            catch (Exception ex)
            {

            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            string StockItem = rowView["StockItem"].ToString();
            string StockItemID = rowView["StockItemID"].ToString();

            //HyperLink lblOut = (HyperLink)e.Row.FindControl("lblOut");
            //HyperLink lblInstalled = (HyperLink)e.Row.FindControl("lblInstalled");
            //HyperLink lblRevert = (HyperLink)e.Row.FindControl("lblRevert");

            //HyperLink hypDetail = (HyperLink)e.Row.FindControl("hypDetail");

            //hypDetail.NavigateUrl = "~/admin/adminfiles/ReportsV2/Details/SerialInstallerReportDetailsProj.aspx?StockItem=" + StockItem + "&StockItemID=" + StockItemID + "&DateType=" + DateType + "&startdate=" + StartDate + "&enddate=" + EndDate + "&Page=Audit" + "&CompanyID=" + CompanyID + "&ProjectNo=" + ProjectNo + "&InstallerID=" + ContactID + "&PS=" + ProjectStatus + "&CompanyLocation=" + CompanyLocation + "&InstallationDate=" + InstallationDate + "&Category=" + Category;
        }
    }

    public DataTable getSerialNoDetails(string StockItemID)
    {
        DataTable dt = ClsReportsV2.SP_InstalledSerailNo_Details(StockItemID, ddlInstaller.SelectedValue, ddlSerialNoFlag.SelectedValue, ddlLocation.SelectedValue);

        return dt;
    }

    protected void lbtnExcel_Click(object sender, EventArgs e)
    {
        DataTable dt = getSerialNoDetails(hndStockItemID.Value);

        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            string FileName = "Installed Serail No Details - " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4 };

            string[] arrHeader = { "Company", "ProjectNo", "Installer", "SerialNo" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception ex)
        {

        }
    }

    protected DataTable GetSerialNoStockLocationWise()
    {
        DataTable dt = new DataTable();

        dt = ClsReportsV2.SP_InstalledSerailNoLocation(ddlCategory.SelectedValue, txtStockItem.Text, ddlInstaller.SelectedValue, ddlSerialNoFlag.SelectedValue, ddlLocation.SelectedValue);

        return dt;
    }
}