﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageReturnStock.aspx.cs" Inherits="admin_adminfiles_ReportsV2_ManageReturnStock"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .sbtn {
            background-color: #53a93f !important;
            border-color: #53a93f !important;
            color: #fff;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .table tbody .brd_ornge td {
            border-bottom: 4px solid #ff784f;
        }

        .padd_btm10 {
            padding-bottom: 15px;
        }

        .height100 {
            height: 100px;
        }
    </style>
    <%--   <script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    //alert("hide");
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
            <%--$('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                ShowProgress();
            });--%>
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            //alert("1");
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            //alert("dgfdg2");


            //$(".dropdown dt a").on('click', function () {
            //    $(".dropdown dd ul").slideToggle('fast');
            //});

            //$(".dropdown dd ul li a").on('click', function () {
            //    $(".dropdown dd ul").hide();
            //});
            //callMultiCheckbox();

            //$(document).bind('click', function (e) {
            //    var $clicked = $(e.target);
            //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            //});


        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            //alert("dgfdg3");
            //callMultiCheckbox1();
            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            //callMultiCheckbox();

            //$('.datetimepicker1').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});
            //  callMultiCheckbox();


            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });

            $("[data-toggle=tooltip]").tooltip();
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            //$('.mutliSelect input[type="checkbox"]').on('click', function () {
            //    callMultiCheckbox1();
            //});
        }


        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
        <%--function callMultiCheckbox1() {
            var title = "";
            $("#<%=ddCompany.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel1').show();
                $('.multiSel1').html(html);
                $(".hida1").hide();
            }
            else {
                $('#spanselect1').show();
                $('.multiSel1').hide();
            }

        }--%>

</script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>


    <script>

        $(document).ready(function () {

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Query
                        <div id="hbreadcrumb" class="pull-right">
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" CausesValidation="false" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                        </div>
                    </h5>
                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <%--<asp:UpdatePanel ID="updatepanel1" runat="server">--%>
                    <%--<ContentTemplate>--%>
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-info" id="Div16" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                    <div class="searchfinal ">
                        <div class="card shadownone brdrgray pad10">
                            <div class="card-block">
                                <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                    <div class="inlineblock martop5">
                                        <div class="row">
                                            <div class="input-group col-sm-2 martop5 max_width170" runat="server" id="DivSearchCompany">
                                                <asp:DropDownList ID="ddlSearchCompany" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchCompany_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                    <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                    <asp:ListItem Value="3">Wholesale</asp:ListItem>
                                                    <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:TextBox ID="txtSerachProjectNo" runat="server" placeholder="Project No." CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSerachProjectNo"
                                                    WatermarkText="Project No." />
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Status</asp:ListItem>
                                                    <asp:ListItem Value="New" Selected="True">New</asp:ListItem>
                                                    <asp:ListItem Value="Progress">Progress</asp:ListItem>
                                                    <asp:ListItem Value="Solved">Solved</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5">
                                                <asp:DropDownList ID="ddlSearchCategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Category</asp:ListItem>
                                                    <asp:ListItem Value="Less Installed">Less Installed</asp:ListItem>
                                                    <asp:ListItem Value="Return">Return</asp:ListItem>
                                                    <asp:ListItem Value="Transfer">Transfer</asp:ListItem>
                                                    <asp:ListItem Value="Stock With Installer">Stock With Installer</asp:ListItem>
                                                    <asp:ListItem Value="Stock With Customer">Stock With Customer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Installer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Date</asp:ListItem>
                                                    <asp:ListItem Value="1">Entered Date</asp:ListItem>
                                                    <asp:ListItem Value="2">Solved Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                    CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                            </div>
                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>

                                <div class="datashowbox inlineblock">
                                    <div class="row">

                                        <div class="input-group col-sm-2 martop5 max_width170">
                                            <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Item
                                </h5>
                            </div>
                            <div class="card-block padleft25">
                                <div class="form-horizontal row martop5">
                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Company
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlCompany" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                    <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                    <asp:ListItem Value="3">Wholesale</asp:ListItem>
                                                    <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlCompany" ErrorMessage="*" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                Stock Category
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlCategory" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="Less Installed">Less Installed</asp:ListItem>
                                                    <asp:ListItem Value="Return">Return</asp:ListItem>
                                                    <asp:ListItem Value="Transfer">Transfer</asp:ListItem>
                                                    <asp:ListItem Value="Stock With Installer">Stock With Installer</asp:ListItem>
                                                    <asp:ListItem Value="Stock With Customer">Stock With Customer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label runat="server" id="lblProjectNumber">
                                                Project Number
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtProjectNo" runat="server" CssClass="form-control" placeholder="Project No."></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtProjectNo" FilterMode="ValidChars" ValidChars="/" FilterType="Numbers, Custom"></cc1:FilteredTextBoxExtender>
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtProjectNo" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectPickNoByProjectNo"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2" runat="server" id="DivToProjectNo" visible="false">
                                        <span class="name disblock">
                                            <label runat="server" id="lblToProjectNumber">
                                                To Project No
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtToProjetNo" runat="server" CssClass="form-control" placeholder="To Project No."></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtToProjetNo" FilterMode="ValidChars" ValidChars="/" FilterType="Numbers, Custom"></cc1:FilteredTextBoxExtender>
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtToProjetNo" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectPickNoByProjectNo"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                No of Panels
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtPanels" Text="0" runat="server" CssClass="form-control" placeholder="No of Panels"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtPanels" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                            </div>
                                        </span>
                                    </div>

                                    <div class="col-sm-2">
                                        <span class="name disblock">
                                            <label>
                                                No of Inverter
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtInverter" Text="0" runat="server" CssClass="form-control" placeholder="No of Inverter"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtInverter" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-horizontal row" style="margin-top: 15px!important">
                                    <div class="col-sm-12">
                                        <span class="name disblock">
                                            <label>
                                                Notes
                                            </label>
                                        </span>
                                        <span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                    placeholder="Notes" Height="100px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                    FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                </cc1:FilteredTextBoxExtender>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                    FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row" style="margin-top: 15px!important">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-xs sbtn" ValidationGroup="Req"
                                            OnClick="btnSave_Click">
                                        <i class="fa fa-save"></i> Save
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="btnUpdate" Visible="false" runat="server" CssClass="btn btn-xs sbtn" CausesValidation="false"
                                            OnClick="btnUpdate_Click">
                                         <i class="fa fa-save"></i> Update
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lnkReset" runat="server" data-placement="left" OnClick="lnkReset_Click"
                                            CausesValidation="false" CssClass="btn btn-primary btnclear"><i class="fa-refresh fa"></i>Reset</asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" OnRowDeleting="GridView1_RowDeleting"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                                OnDataBound="GridView1_DataBound" AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25"
                                                OnSelectedIndexChanging="GridView1_SelectedIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("ID") %>','tr<%# Eval("ID") %>');">
                                                                <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />--%>
                                                                <img id='imgdiv<%# Eval("ID") %>' src="../../../images/icon_plus.png" />
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblID" runat="server">
                                                                                        <%#Eval("ID")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Project No" SortExpression="ProjectNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProjectNo" runat="server">
                                                                                        <%#Eval("ProjectNo")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Picklist ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPicklistID" runat="server">
                                                                                        <%#Eval("PicklistID")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="To Proj. No" SortExpression="ToProjectNo">
                                                        <ItemTemplate>
                                                            <%--<asp:Label ID="lblProjectNo" runat="server">
                                                                                        <%#Eval("ToProjectNo") + "/" + Eval("ToPickListID")%></asp:Label>--%>
                                                            <asp:Label ID="lblToProjectNo" runat="server">
                                                                                        <%#Eval("ToProjectNo")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Project Status" SortExpression="ProjectStatus">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProjectStatus" runat="server">
                                                                                        <%#Eval("ProjectStatus")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompanyLocation" runat="server">
                                                                                        <%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Installer" SortExpression="Installer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInstaller" runat="server">
                                                                                        <%#Eval("Installer")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="System Details" SortExpression="SystemDetails">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSystemDetails" runat="server" Width="200px">
                                                                                        <%#Eval("SystemDetails")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="200px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategory" runat="server">
                                                                                        <%#Eval("Category")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Panles" SortExpression="Panles">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPanles" runat="server">
                                                                                        <%#Eval("Panles") + "/" + Eval("PanelCount")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Inverter" SortExpression="Inverter">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInverter" runat="server">
                                                                                        <%#Eval("Inverter") + "/" + Eval("InverterCount")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Notes">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNotes" runat="server" Width="100px"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title='<%#Eval("Notes")%>'>
                                                                                        <%#Eval("Notes")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkQueryStatus" Text='<%#Eval("QueryStatus")%>' runat="server"
                                                                CommandArgument='<%#Eval("ID")%>' CommandName="UpdateStatus" />
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Entered On" SortExpression="EnteredOn">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEnteredOn" runat="server">
                                                                                        <%#Eval("EnteredOn")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Entered By" SortExpression="EnteredBy">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEnteredBy" runat="server">
                                                                                        <%#Eval("EnteredBy")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Picklist Gen.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPickListGenName" runat="server">
                                                                                        <%#Eval("PickListGenName")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Assign To">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAssignTo" runat="server">
                                                                                        <%#Eval("AssignToName")%></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
                                                                Visible='<%# Roles.IsUserInRole("Administrator") || Eval("userid").ToString() == userid ? true : false %>'>
                                                                            <i class="fa fa-edit"></i> Edit
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("ID")%>'
                                                                Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                                <i class="fa fa-trash"></i> Delete
                                                            </asp:LinkButton>

                                                            <!--Start Add New Notes -->
                                                            <asp:LinkButton ID="gvlnkNotes" runat="server" CssClass="btn btn-warning btn-mini" CommandName="NewNote"
                                                                CommandArgument='<%#Eval("PickListID") + ";" + Eval("ID") %>' CausesValidation="false">
                                                                            <i class="btn-label fa fa-edit"></i> New Note
                                                            </asp:LinkButton>
                                                            <!--End Add New Notes -->

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="left" CssClass="verticaaline" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("ID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("ID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 5%;"><b>Status Notes</b>
                                                                                </td>
                                                                                <td style="width: 45%;">
                                                                                    <asp:Label ID="Label3" runat="server" Width="80px"><%# Eval("StatusNotes") %></asp:Label>
                                                                                </td>
                                                                                <td style="width: 5%;"><b>New Notes</b>
                                                                                </td>
                                                                                <td style="width: 45%;">
                                                                                    <asp:Label ID="Label4" runat="server"><%# Eval("NewNotes") %></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 5%;"><b>Assined Employee</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label1" runat="server" Width="80px"><%# Eval("AssignEmployee") %></asp:Label>
                                                                                </td>
                                                                                <td style="width: 5%;"><b>Employee Notes</b>
                                                                                </td>
                                                                                <td style="width: 80%;">
                                                                                    <asp:Label ID="Label2" runat="server" Width="80px"><%# Eval("EmployeeNotes") %></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderUpdateStatus" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStatus" TargetControlID="Button9" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divStatus" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="HiddenField3" runat="server" />

                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Status</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Status
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlStatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="New">New</asp:ListItem>
                                                        <asp:ListItem Value="Progress">Progress</asp:ListItem>
                                                        <asp:ListItem Value="Solved">Solved</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Notes
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtStutusNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        placeholder="Notes" Height="100px"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtStutusNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtStutusNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnUpdateStatus" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Update" OnClick="btnUpdateStatus_Click"></asp:Button>
                            <asp:Button ID="btncancelpvd" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button9" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndQueryID" runat="server" />

            <!--Danger Modal Templates-->
            <asp:Button ID="btndelate" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="LinkButton6" OkControlID="btnOKMobile" TargetControlID="btndelate">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog">
                    <div class=" modal-content">

                        <div class="modal-header text-center">
                            <h5 class="modal-title" id="H2">Delete</h5>
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton6" runat="server" class="close" data-dismiss="modal">  <span aria-hidden="true">x</span></asp:LinkButton>
                            </div>
                        </div>

                        <label id="Label21" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer" style="text-align: center">
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" class="btn btn-danger">OK</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="hndDeleteID" runat="server" />

            <!-- Start New Notes Popup-->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderNewNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="ModelNewNote" TargetControlID="Button7"
                CancelControlID="LinkButton9">
            </cc1:ModalPopupExtender>
            <div id="ModelNewNote" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalNewNote">Project Note
                                <span style="float: right" class="printorder" />
                                <asp:LinkButton ID="LinkButton14" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <asp:HiddenField runat="server" ID="hndNewNotesPicklistID" />
                                                <asp:HiddenField runat="server" ID="hndNewQueryID" />
                                                <div class="input-group col-sm-6 padd_btm10">
                                                    <asp:DropDownList ID="ddlAssignToNew" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                        <asp:ListItem Value="">Assign To</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-6 padd_btm10">
                                                    <asp:DropDownList ID="ddlEmployees" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:TextBox ID="txtNotesNew" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotesNew"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotesNew"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hndMode" />
                                                <asp:HiddenField runat="server" ID="hndID" />
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="lbtnSaveNewNotes" CssClass="btn btn-info" CausesValidation="false" runat="server" Text="Save" OnClick="lbtnSaveNewNotes_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10" runat="server" id="DivNewNotes">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Entered On</th>
                                                            <th>Entered By</th>
                                                            <th>Assign To</th>
                                                            <th>Employee</th>
                                                            <th>Notes</th>
                                                            <th></th>
                                                        </tr>
                                                        <asp:Repeater ID="RptNewNotes" runat="server" OnItemCommand="RptNewNotes_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%# Container.ItemIndex + 1 %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("EnteredOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("EnteredBy") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("AssignTo") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("Employee") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("Notes") %>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lbtnEditNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="EditNewNote" CommandArgument='<%#Eval("ID") %>'
                                                                            CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top" Visible='<%# Eval("EditFlag").ToString() == "1" || Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                                            <i class="btn-label fa fa-pencil"></i> Edit
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="DeleteNewNotes"
                                                                            CommandArgument='<%# Eval("ID") %>' Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'
                                                                            CausesValidation="false" data-original-title="Not Verify" data-toggle="tooltip" data-placement="top"
                                                                            OnClientClick="return confirm('Are you sure you want to delete?');">                                                                          
                                                                            <i class="btn-label fa fa-close"></i> Delete
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />
            <!--End New Notes Popup-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        //$(".dropdown dt a").on('click', function () {
        //    $(".dropdown dd ul").slideToggle('fast');

        //});

        //$(".dropdown dd ul li a").on('click', function () {
        //    $(".dropdown dd ul").hide();
        //});
        //$(document).bind('click', function (e) {
        //    var $clicked = $(e.target);
        //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        //});


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });


        });

        $(".selectCompany .dropdown dt a").on('click', function () {
            $(".selectCompany .dropdown dd ul").slideToggle('fast');
        });

        $(".selectCompany .dropdown dd ul li a").on('click', function () {
            $(".selectCompany .dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });

        function callMultiCheckbox() {


        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>

