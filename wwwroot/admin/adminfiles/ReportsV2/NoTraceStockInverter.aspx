﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NoTraceStockInverter.aspx.cs" Inherits="admin_adminfiles_ReportsV2_NoTraceStockInverter"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style>
                .modal-dialog1 {
                    margin-left: -300px;
                    margin-right: -300px;
                    width: 985px;
                }

                .focusred {
                    border-color: #FF5F5F !important;
                }

                .padd_btm10 {
                    padding-bottom: 15px;
                }

                .height100 {
                    height: 100px;
                }

                .autocomplete_completionListElement {
                    z-index: 9999999 !important;
                }
            </style>

            <script type="text/javascript">


                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        //ShowProgress();
                    });
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();

                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    //alert("dgfdg2");


                    $(".AriseInstaller .dropdown dt a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
                    });
                    $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
                        $(".AriseInstaller .dropdown dd ul").hide();
                    });

                    $(".Location .dropdown dt a").on('click', function () {
                        $(".Location .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Location .dropdown dd ul li a").on('click', function () {
                        $(".Location .dropdown dd ul").hide();
                    });

                }
                function pageLoaded() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true,
                    });

                    $(".myvalinvoiceissued").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});
                    //$(".myval").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox3();
                    //});

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox1();
                    //});
                }


                function stopRKey(evt) {
                    var evt = (evt) ? evt : ((event) ? event : null);
                    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
                }
                document.onkeypress = stopRKey;

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
            </script>

            <script>

                $(document).ready(function () {

                });
                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            // alert("2");
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
            </script>

            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <script>

                <%--function callMultiCheckbox3() {
                    var title = "";
                    $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel2').show();
                        $('.multiSel2').html(html);
                        $(".hida2").hide();
                    }
                    else {
                        $('#spanselect2').show();
                        $('.multiSel2').hide();
                    }

                }--%>

                <%--function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddProjectStatus.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }--%>

            </script>
            <div class="page-header card">
                <div class="card-block">
                    <h5>No Trace Stock - Inverter
                        <div class="pull-right" runat="server" visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                            <asp:Button Text="Insert SerialNo" runat="server" ID="btnInsertSerialNo" class="btn btn-warning" OnClick="btnInsertSerialNo_Click" />
                            <asp:Button Text="BS Manual SN" runat="server" ID="btnBSManual" class="btn btn-primary" OnClick="btnBSManual_Click" />
                            <asp:Button Text="Notes" runat="server" ID="btnNotes" class="btn btn-warning" OnClick="btnNotes_Click" Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>' />
                        </div>
                    </h5>
                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="Panel4">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">

                        <ContentTemplate>
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="Div16" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <div class="searchfinal">
                                <div class="card shadownone brdrgray pad10">
                                    <div class="card-block">
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnSearch">
                                            <div class="inlineblock martop5">
                                                <div class="row">

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                            <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                            <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                            <%--<asp:ListItem Value="3">Wholesale</asp:ListItem>--%>
                                                            <asp:ListItem Value="4">Solar Bridge</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5 max_width170">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers, Custom" ValidChars="," />
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 max_width170">
                                                        <asp:DropDownList ID="ddlEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Manager</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlPendingAudiYN" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Resolve ALL</asp:ListItem>
                                                            <asp:ListItem Value="1">Resolve Yes</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">Resolve No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlScanReq" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Scan Req</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 max_width170">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <%--<asp:ListItem Value="">Date</asp:ListItem>--%>
                                                            <asp:ListItem Value="1" Selected="True">Deducted</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                        <div class="input-group sandbox-container">
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                            <div class="input-group-addon">
                                                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                            CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                    </div>
                                                    <div class="input-group martop5 col-sm-1 max_width170">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <div class="datashowbox inlineblock">
                                            <div class="row">

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click"
                                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExport" />
                          <%--  <asp:PostBackTrigger ControlID="lbtnFetch" />--%>
                            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
                            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <div>
                <div class="card shadownone brdrgray" id="divtot" runat="server">
                    <div class="card-block">
                        <div class="table-responsive BlockStructure">
                            <table class="tooltip-demo table table-bordered nowrap dataTable" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 150px;"></th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Net Out</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">SB BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Arise BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">SM BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">WO BSGB</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Scan Required</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Other Inventory</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Other Wholesale</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Credit</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Non Resolve</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Future Installation</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Pending Audit</th>
                                        <th class="brdrgrayleft" align="center" scope="col" style="width: 10px;">Actual Audit</th>
                                        <%--data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                    </tr>
                                    <tr class="brd_ornge">
                                        <td align="left" valign="top">Total</td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypNetOut" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypBSGB" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypAriseBSGB" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypSMBSGB" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypWOBSGB" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypScanRequired" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypOtherInventory" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypOtherWholesale" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypCredit" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypNonResolve" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypFutureInstallation" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypPendingAudit" runat="server" Target="_blank" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:HyperLink ID="HypActualAudit" runat="server" Target="_blank" />
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="InstallerID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                            AllowSorting="True" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Installer Name" SortExpression="InstallerName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallerName" runat="server" data-toggle="tooltip" data-placement="Top" title=""
                                                            data-original-title='<%#Eval("InstallerName")%>'>
                                                           <%#Eval("InstallerName")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="NetOut">
                                                           Net Out
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lblNetOut" runat="server" Text='<%#Eval("NetOut")%>' Enabled='<%# Eval("NetOut").ToString() == "0" ? false:true%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="SBBSGB">
                                                           SB BSGB
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypSBBSGB" runat="server" Text='<%#Eval("SBBSGB")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="AriseBSGB">
                                                           Arise BSGB
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypAriseBSGB" runat="server" Text='<%#Eval("AriseBSGB")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="SMBSGB">
                                                           SM BSGB
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypSMBSGB" runat="server" Text='<%#Eval("SMBSGB")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="WOBSGB">
                                                           WO BSGB
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypWOBSGB" runat="server" Text='<%#Eval("WOBSGB")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField SortExpression="ScanRequired" HeaderText="Scan Required">
                                                    
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypScanRequired" runat="server" Text='<%#Eval("ScanRequired")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="OtherInventory">
                                                           Other Inventory
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypOtherInventory" runat="server" Text='<%#Eval("OtherInventory")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="OtherWholesale">
                                                           Other Wholesale
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypOtherWholesale" runat="server" Text='<%#Eval("OtherWholesale")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="SerialNo Credit given to Installer" SortExpression="Credit">
                                                           Credit
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" data-toggle="tooltip" data-placement="Top" data-original-title="Can't find SerialNo" SortExpression="NonResolved">
                                                           Non Resolve
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNonResolved" runat="server" Text='<%#Eval("NonResolved")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="FutureInstallation">
                                                           Future Installation
                                                        </asp:LinkButton>
                                                        <%-- data-toggle="tooltip" data-placement="Top" data-original-title="Stock dedect - Stock revert"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypFutureInstallation" runat="server" Text='<%#Eval("FutureInstallation")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:LinkButton runat="server" SortExpression="PendingAudit">
                                                           Pending Audit
                                                        </asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypPendingAudit" runat="server" Text='<%#Eval("PendingAudit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Actual Audit" SortExpression="ActualAudit">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypActualAudit" runat="server" Text='<%#Eval("ActualAudit")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="brdrgrayleft" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="lbtnExportV2" runat="server" class="btn btn-success btn-mini Excel" OnClick="lbtnExportV2_Click" CommandArgument='<%#Eval("InstallerID") + ";" + Eval("InstallerName")%>'
                                                            CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>--%>

                                                        <asp:LinkButton ID="lbtnExportV3" runat="server" class="btn btn-success btn-mini Excel" OnClick="lbtnExportV3_Click" CommandArgument='<%#Eval("InstallerID") + ";" + Eval("InstallerName")%>'
                                                            CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>

                                                       <%-- <asp:LinkButton ID="lbtnNotes" runat="server" class="btn btn-warning btn-mini" OnClick="lbtnNotes_Click" CommandArgument='<%#Eval("InstallerID")%>'
                                                            CausesValidation="false"><i class="fa fa-edit"></i> Note </asp:LinkButton>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>

                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <asp:Button ID="Button5" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="MPUpdateSerialNo" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divUpdate" TargetControlID="Button5" CancelControlID="Button4">
            </cc1:ModalPopupExtender>
            <div id="divUpdate" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Serial No</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-4">
                                            <span class="name disblock">
                                                <label>
                                                    Company
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlAssignCompany" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true" Width="90%">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                                        <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                        <%--<asp:ListItem Value="4">Solar Bridge</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="ddlAssignCompany" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            <span class="name disblock">
                                                <label>
                                                    Project No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAssignProjectNo" runat="server" CssClass="form-control" ValidationGroup="ReqAssign"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtAssignProjectNo" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>

                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtAssignProjectNo" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="getAllProjectNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAssignSerialNo" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" ValidationGroup="ReqAssign"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtAssignSerialNo" Display="Dynamic" ValidationGroup="ReqAssign"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnUpdateBSSerialNo" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                ValidationGroup="ReqAssign" CausesValidation="true" OnClick="btnUpdateBSSerialNo_Click"></asp:Button>
                            <asp:Button ID="Button4" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>


            <asp:Button ID="Button1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="div1" TargetControlID="Button1" CancelControlID="Button3">
            </cc1:ModalPopupExtender>
            <div id="div1" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Serial No Notes</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Serial No
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtSerialNo" runat="server" CssClass="form-control" ValidationGroup="ReqNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtSerialNo" Display="Dynamic" ValidationGroup="ReqNotes"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5" style="padding-top: 5pt">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Notes
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        Height="100px" ValidationGroup="ReqNotes"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                        ControlToValidate="txtNotes" Display="Dynamic" ValidationGroup="ReqNotes"></asp:RequiredFieldValidator>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSaveNotes" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Save"
                                ValidationGroup="ReqNotes" CausesValidation="true" OnClick="btnSaveNotes_Click"></asp:Button>
                            <asp:Button ID="Button3" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {
            HighlightControlToValidate();


        });



        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }

        // For Multi Select //
        $(".AriseInstaller .dropdown dt a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").slideToggle('fast');
        });
        $(".AriseInstaller .dropdown dd ul li a").on('click', function () {
            $(".AriseInstaller .dropdown dd ul").hide();
        });

        $(".Location .dropdown dt a").on('click', function () {
            $(".Location .dropdown dd ul").slideToggle('fast');
        });
        $(".Location .dropdown dd ul li a").on('click', function () {
            $(".Location .dropdown dd ul").hide();
        });

        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>
</asp:Content>
