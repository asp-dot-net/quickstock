<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="adminsiteconfiguration.aspx.cs" Inherits="admin_adminfiles_utilities_adminsiteconfiguration"
     Theme="admin" Culture="en-us" UICulture="en-us"
    EnableEventValidation="true" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="breadcrumb">
        Manage Utilities
    </div>
    <div id="midArea">
        <div id="midAreaTop">
            <span id="midAreaRight"></span><span id="midAreaLeft"></span>
        </div>
        <div id="midAreaCont">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <h2 class="Blue">
                            Manage SiteConfiguration</h2>
                    </td>
                </tr>
            </table>
            <div class="hr">
            </div>
            <asp:Panel ID="PanSuccess" runat="server" CssClass="tick">
                <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanError" runat="server" CssClass="cross">
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanNoRecord" runat="server" CssClass="cross">
                <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
            </asp:Panel>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <%-- END    : Select Panels --%>
                        <%-- START  : ADD / UPDATE Panels --%>
                        <asp:Panel ID="PanAddUpdate" runat="server" Visible="false" CssClass="PanAddUpdate">
                            <table cellpadding="5" cellspacing="0" border="1" width="100%" class="tblDisplay widthPc"
                                summary="">
                                <tr>
                                    <td colspan="2">
                                        <h2 class="Blue">
                                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                            Site Configuration</h2>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="widthLeft" valign="top">
                                        Level
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtlevel" runat="server" Width="200" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="widthLeft" valign="top">
                                        Currency Digit
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrencyDigit" runat="server" Width="200" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Symbol
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtsymbol" runat="server"  Width="200" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Category Large
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCatLarge" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Category Medium
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCatMedium" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Category Small
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCatSmall" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Product Large
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProductLarge" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr class="odd">
                                    <td valign="top">
                                        Product Medium
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProductMedium" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                  <tr class="odd">
                                    <td valign="top">
                                        Product Small
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProductSmall" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td valign="top">
                                        Banner Large
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBannerLarge" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr class="odd">
                                    <td valign="top">
                                        Banner Medium
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBannerMedium" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                  <tr class="odd">
                                    <td valign="top">
                                        Banner Small
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBannerSmall" runat="server"  Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/admin/images/save.gif"
                                            OnClick="btnUpdate_Click" Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%-- END    : ADD / UPDATE Panels --%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="midAreaBot">
            <span id="midBotRight"></span><span id="midBotLeft"></span>
        </div>
    </div>
</asp:Content>
