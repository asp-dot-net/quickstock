﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="Changepassword.aspx.cs" Inherits="admin_adminfiles_Masters_Editprofile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style>
.toast-error2{background-color:#fff!important;
 background: #fff!important; padding:10px; border:1px solid #eaeaea; border-radius:3px;
    border-left: 6px solid #e74c3c!important;}
</style>

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">Change Password
                </h2>
            </div>
        </div>
    </div>
    <div id="PanAddUpdate" runat="server" visible="true">
        <div class="content animate-panel">
            <div>
                <div class="row">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel">
                                <div class="panel-heading">
                                    <%--<div class="panel-tools">
                                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                        <a class="closebox"><i class="fa fa-times"></i></a>
                                    </div>--%>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                  Change Password
                                </div>
                                <div class="panel-body">
                                    <asp:Panel ID="PanSuccess" runat="server" Visible="false" CssClass="alert alert-success">

                            <strong>Success ! </strong>
                            &nbsp;
                            <asp:Label ID="lblSuccess" runat="server"
                                Text="Your password has been Successfully Changed."></asp:Label>

                        </asp:Panel>
                        <asp:Panel ID="Panfail" runat="server" Visible="false">
                            <div class="toast-error2">
                                <strong>
                                    <asp:Literal ID="Literal2" runat="server" Text="Failure !"></asp:Literal>
                                    <%-- Success !--%>
                                </strong>
                                <br />
                                <asp:Label ID="Label3" runat="server"
                                    Text="Password is Incorrect. Please try again."></asp:Label>
                              </div>
                        </asp:Panel>
                                   <asp:Panel runat="server" >
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <asp:Label ID="lblPassword" runat="server" class="col-sm-2 control-label">
                                                <strong>Current Password</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" MaxLength="20" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="dynamic" ValidationGroup="ChangePassword11"
                                                            ControlToValidate="CurrentPassword" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                <strong>New Password</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" MaxLength="20" class="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="NewPassword"
                                                            ErrorMessage="Minimum password length is 6" Style="color: red;" Display="Dynamic" ValidationExpression=".{6}.*"
                                                            ValidationGroup="ChangePassword11" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" ValidationGroup="ChangePassword11"
                                                            ControlToValidate="NewPassword" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                <strong>Confirm Password</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" MaxLength="20" class="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regConfirmPassword" runat="server" ControlToValidate="ConfirmNewPassword"
                                                            ErrorMessage="Minimum password length is 6" Style="color: red;" Display="Dynamic" ValidationExpression=".{6}.*"
                                                            ValidationGroup="ChangePassword11" />
                                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ValidationGroup="ChangePassword11"
                                                            ErrorMessage="Password MisMatch" Style="color: red;" ControlToCompare="NewPassword" Display="Dynamic"
                                                            ControlToValidate="ConfirmNewPassword"></asp:CompareValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic" ValidationGroup="ChangePassword11"
                                                            ControlToValidate="ConfirmNewPassword" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                       
                                                        <asp:LinkButton CssClass="btn btn-primary savewhiteicon" ValidationGroup="ChangePassword11" CausesValidation="true"
                                                            ID="ChangePasswordPushButton" runat="server" OnClick="ChangePasswordPushButton_Click"
                                                            Text="Update & Save" />
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                       </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function doMyAction() {
            $(".selectpicker").chosen();
            $('#ctl00_ContentPlaceHolder_ChangePassword1_ChangePasswordContainerID_ChangePasswordPushButton').click(function () {

                formValidate();
            });

        }
        $(document).ready(function () {
            doMyAction();

        });



    </script>

</asp:Content>

