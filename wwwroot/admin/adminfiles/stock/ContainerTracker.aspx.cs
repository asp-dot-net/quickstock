﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_ContainerTracker : System.Web.UI.Page
{
    static DataView dv;
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (Roles.IsUserInRole("WholeSale"))
        {
            divprojNo.Visible = false;
        }
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindTransCompany();
            BindGrid(0);
            //BindVendor();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    public void BindDropDown()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();

        rptLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptLocation.DataBind();

        rptVender.DataSource = ClstblStockOrders.tbl_PurchaseCompany_GetAll();
        rptVender.DataBind();

        //ddlSearchVendor.DataSource = dt;
        //ddlSearchVendor.DataTextField = "Customer";
        //ddlSearchVendor.DataValueField = "CustomerID";
        //ddlSearchVendor.DataBind();

        //ddlloc.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddlloc.DataTextField = "location";
        //ddlloc.DataValueField = "CompanyLocationID";
        //ddlloc.DataMember = "CompanyLocationID";
        //ddlloc.DataBind();
    }

    public void BindTransCompany()
    {
        DataTable dt = ClstblStockOrders.tbl_Transport_CompMaster_GetData();
        ddlSearchTransCompName.DataSource = dt;
        ddlSearchTransCompName.DataValueField = "Trans_Comp_Id";
        ddlSearchTransCompName.DataTextField = "Trans_Company_Name";
        ddlSearchTransCompName.DataBind();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "UpdateDate")
        {
            string[] arg = new string[6];
            arg = e.CommandArgument.ToString().Split(';');

            hdnOrderID.Value = arg[0];
            
            if (!string.IsNullOrEmpty(arg[1]))
            {
                txtETD.Text = Convert.ToDateTime(arg[1]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtETD.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(arg[2]))
            {
                txtETA.Text = Convert.ToDateTime(arg[2]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtETA.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(arg[3]))
            {
                txtTelexReleaseDate.Text = Convert.ToDateTime(arg[3]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtTelexReleaseDate.Text = string.Empty;
            }

            if (!string.IsNullOrEmpty(arg[4]))
            {
                txtTransportDelDate.Text = Convert.ToDateTime(arg[4]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtTransportDelDate.Text = string.Empty;
            }

            if (!string.IsNullOrEmpty(arg[5]))
            {
                txtTargetDate.Text = Convert.ToDateTime(arg[5]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtTargetDate.Text = string.Empty;
            }


            ModalPopupExtenderNote.Show();
            //hdnserialno.Value = arg[0];
            //string stockitemid = arg[1];
            //txtserialno.Text = hdnserialno.Value;
            //hdnstockitemid.Value = stockitemid;
            //ModalPopupExtenderUpdateSerialNo.Show();
        }

    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);

    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockitem.aspx");
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblEtd = (Label)e.Row.FindControl("lblETD");
            Label lblETA = (Label)e.Row.FindControl("lblETA");
            string DefaulVal = "TBA";
            if (lblETA.Text == null || lblETA.Text == "")
            {
                lblETA.Text = DefaulVal;
            }
            if (lblEtd.Text == null || lblEtd.Text == "")
            {
                lblEtd.Text = DefaulVal;
            }

        }
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtProjNo.Text = string.Empty;
        txtserailno.Text = string.Empty;
        //txtWholesaleorderid.Text = string.Empty;
        lbtnExport.Visible = false;
        
        txtStartDate.Text = "";
        txtEndDate.Text = "";
        //ddlSearchVendor.ClearSelection();
        //ddlSearchVendor.SelectedValue = "";
        //ddlloc.ClearSelection();
        //ddlloc.SelectedValue = "";
        txtStockSize.Text = "";
        txtStockManufacturer.Text = "";
        ddlSearchTransCompName.SelectedValue = "";
        txtContainderNo.Text = "";
        ClearMultiCheckBox();

        BindGrid(0);

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {

    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected DataTable GetGridData()
    {
        string DataType = "";
        string Val = "";
        if (string.IsNullOrEmpty(txtStartDate.Text) && string.IsNullOrEmpty(txtEndDate.Text))
        {
            DataType = "";
            Val = ddlDate.SelectedValue;
        }
        else
        {
            DataType = ddlDate.SelectedValue;
        }

        #region Location
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }
        #endregion

        #region Vender
        string Vender = "";
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnid");

            if (chkselect.Checked == true)
            {
                Vender += "," + hdnModule.Value.ToString();
            }
        }
        if (Vender != "")
        {
            Vender = Vender.Substring(1);
        }
        #endregion

        int OrderBy = 1;
        if (ddlDate.SelectedValue == "3")
        {
            OrderBy = 3;
        }

        DataTable dt = ClstblStockOrders.tblStockOrders_selectforContainerTrackerNew(txtProjNo.Text, DataType, txtStartDate.Text, txtEndDate.Text, txtstockitem.Text, Location, Vender, ddlStockOrderStatus.SelectedValue, Val, txtStockSize.Text, txtStockManufacturer.Text, ddlSearchTransCompName.SelectedValue, OrderBy, txtContainderNo.Text
          );

        //DataTable dt = ClstblStockItems.tblStockItemsGetDataBySearch(txtSearchStockItem.Text, txtSearchManufacturer.Text, ddlcategorysearch.SelectedValue.ToString(), ddllocationsearch.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), ddlActive.SelectedValue.ToString(), ddlSalesTag.SelectedValue.ToString(), txtExpiryDate.Text);

        return dt;
    }

    protected void lbtnExport_Click1(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

        Export oExport = new Export();
        string FileName = "ContainerTrackerReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        
        int[] ColList = { 12, 2, 13, 8, 4, 18,
                          11, 3, 6, 21, 14, 7 ,
                          17, 15, 16, 19, 10 };

        //string[] arrHeader = { "P.Company", "OrderNo", "ExpectedDelivery", ,"Itemname", "ExtraNotes" };
        string[] arrHeader = { "Location", "OrderNo", "ManualOrdeNo", "P.Company", "Containerno", "Target Date", "ETD", "ETA", "TelexReleaseDate", "TransportDeldate", "StockOrderStatus", "ExtraNotes", "StockOrderQuantity", "StockSize", "StockManufacturer", "TransCo Name", "Order Items" };

        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        // BindGrid(0);
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                // SetDelete();
            }
            else
            {
                //  SetNoRecords();
                Notification("There are no items to show in this view.");
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            //btnEmail.Visible = true;
            lbtnExport.Visible = true;
            divtot.Visible = true;


            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }

                //if (dt.Rows.Count > 0)
                //{
                //    //int sum = Convert.ToInt32(dt.Compute("SUM(Qty)", string.Empty));

                //    lblTotalPanels.Text = dt.Rows.Count.ToString();
                //}
                //else
                //{
                //    lblTotalPanels.Text = "0";
                //}
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnSave_Click1(object sender, EventArgs e)
    {
        string StockOrderID = hdnOrderID.Value;
        if (StockOrderID != null && StockOrderID != "")
        {
            bool s1 = ClstblStockOrders.tblStockOrders_UpateDateForContainer(StockOrderID, txtETD.Text, txtETA.Text, txtTelexReleaseDate.Text, txtTransportDelDate.Text);

            bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(StockOrderID, txtTargetDate.Text);

            if (s1)
            {
                Notification("UpdatedSuccessfully..");
                ModalPopupExtenderNote.Hide();
            }
            else
            {
                Notification("Errror..");
            }
            BindGrid(0);
        }
    }

    protected void ClearMultiCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }
}