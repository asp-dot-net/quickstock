﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_StockOrderDelivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string StockOrderID = Request.QueryString["StockOrderID"].ToString();
            Binddata(StockOrderID);
        }
    }

    public void Binddata(string StockOrderID)
    {
        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);

        if (st.CompanyLocation != string.Empty)
        {
            lblStockLocation.Text = st.CompanyLocation;
        }
        else
        {
            lblStockLocation.Text = "-";
        }
        if (st.Vendor != string.Empty)
        {
            lblVendor.Text = st.Vendor;
        }
        else
        {
            lblVendor.Text = "-";
        }
        if (st.BOLReceived != string.Empty)
        {
            lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
        }
        else
        {
            lblBOLReceived.Text = "-";
        }
        if (st.ManualOrderNumber != string.Empty)
        {
            lblManualOrderNo.Text = st.ManualOrderNumber;
        }
        else
        {
            lblManualOrderNo.Text = "-";
        }
        if (st.ExpectedDelivery != string.Empty)
        {
            lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
        }
        else
        {
            lblExpectedDelevery.Text = "-";
        }
        if (st.PurchaseCompanyName != string.Empty)
        {
            lblpcompany.Text = st.PurchaseCompanyName;
        }
        else
        {
            lblpcompany.Text = "-";
        }
        if (st.VendorInvoiceNo != string.Empty)
        {
            lblInvoiceNo.Text = st.VendorInvoiceNo;
        }
        else
        {
            lblInvoiceNo.Text = "-";
        }
        if (st.StockContainer != string.Empty)
        {
            lblContainerNo.Text = st.StockContainer;
        }
        else
        {
            lblContainerNo.Text = "-";
        }
        if (st.ItemNameValue != string.Empty)
        {
            lblItemname.Text = st.ItemNameValue;
        }
        else
        {
            lblItemname.Text = "-";
        }
        if (st.Deleveryvalue != string.Empty)
        {
            lblDeliveryType.Text = st.Deleveryvalue;
        }
        else
        {
            lblDeliveryType.Text = "-";
        }

        if (st.TransCharges != string.Empty)
        {
            lbltranscharges.Text = st.TransCharges;
        }
        else
        {
            lbltranscharges.Text = "-";
        }
        if (st.Notes != string.Empty)
        {
            lblNotes.Text = st.Notes;
        }
        else
        {
            lblNotes.Text = "-";
        }
        if (st.StockFrom != string.Empty)
        {
            lblStockFrom.Text = st.StockFrom;
        }
        else
        {
            lblStockFrom.Text = "-";
        }

        DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(StockOrderID);
        if (dt.Rows.Count > 0)
        {
            rptOrder.DataSource = dt;
            rptOrder.DataBind();
            trOrderItem.Visible = true;
        }
        else
        {
            trOrderItem.Visible = false;
        }
    }

    protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int CategoryID;
        int OrderQuantity;
        HiddenField hfStockOrderID = (HiddenField)e.Item.FindControl("hfStockOrderID");
        HiddenField StockCategoryID = (HiddenField)e.Item.FindControl("hfStockCategoryID");
        HiddenField hdnDeliveryOrderStatus = (HiddenField)e.Item.FindControl("hdnDeliveryOrderStatus");
        HiddenField hfStockItemID = (HiddenField)e.Item.FindControl("hfStockItemID");
        HiddenField hfStockCategoryID = (HiddenField)e.Item.FindControl("hfStockCategoryID");
        LinkButton btnDelivery = (LinkButton)e.Item.FindControl("btnDelivery");
        // ImageButton checkimag = (ImageButton)e.Item.FindControl("checkimag");
        HyperLink lnkExcelUploded = (HyperLink)e.Item.FindControl("lnkExcelUploded");
        LinkButton lnkrevert = (LinkButton)e.Item.FindControl("lnkrevert");
        Label lblOrderQuantity = (Label)e.Item.FindControl("lblOrderQuantity");
        DataTable dtGetData = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(Convert.ToString(hfStockOrderID.Value));

        if (hdnDeliveryOrderStatus.Value == "1")
        {
            DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hfStockOrderID.Value, hfStockCategoryID.Value, hfStockItemID.Value);
            DataTable dt1 = ClstblStockOrders.tbl_OtherQtyLog_ByStockOrderID(hfStockOrderID.Value, hfStockItemID.Value);
            int Qty = 0;
            if (dt1.Rows.Count > 0)
            {
                string Quantity = dt1.Compute("SUM(Quantity)", string.Empty).ToString();
                Qty = Convert.ToInt32(Quantity);
            }
            if (Convert.ToInt32(lblOrderQuantity.Text) == dt.Rows.Count)
            {
                btnDelivery.Visible = false;
                //checkimag.Visible = true;
                lnkExcelUploded.Visible = true;
                lnkrevert.Visible = true;
            }
            else if (Convert.ToInt32(lblOrderQuantity.Text) == Qty)
            {
                btnDelivery.Visible = false;
                //checkimag.Visible = true;
                lnkExcelUploded.Visible = true;
                lnkrevert.Visible = true;
            }
            else
            {
                btnDelivery.Visible = true;
                //checkimag.Visible = false;
                lnkExcelUploded.Visible = false;
                lnkrevert.Visible = false;
            }
        }
        else if (hdnDeliveryOrderStatus.Value == "0")
        {
            DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hfStockOrderID.Value, hfStockCategoryID.Value, hfStockItemID.Value);
            DataTable dt1 = ClstblStockOrders.tbl_OtherQtyLog_ByStockOrderID(hfStockOrderID.Value, hfStockItemID.Value);
            int Qty = 0;
            if (dt1.Rows.Count > 0)
            {
                string Quantity = dt1.Compute("SUM(Quantity)", string.Empty).ToString();
                Qty = Convert.ToInt32(Quantity);
            }

            if (Convert.ToInt32(lblOrderQuantity.Text) == dt.Rows.Count)
            {
                btnDelivery.Visible = false;
                lnkExcelUploded.Visible = true;
                lnkrevert.Visible = true;

            }
            else if (Convert.ToInt32(lblOrderQuantity.Text) == Qty)
            {
                btnDelivery.Visible = false;
                //checkimag.Visible = true;
                lnkExcelUploded.Visible = true;
                lnkrevert.Visible = true;
            }
            else
            {
                //btnDelivery.Enabled = false;
                btnDelivery.Enabled = true;
                lnkExcelUploded.Visible = false;
                lnkrevert.Visible = false;
            }
        }
        else
        {
            btnDelivery.Visible = false;
        }

        HiddenField hndStockLocation = (HiddenField)e.Item.FindControl("hndStockLocation");

        if(hndStockLocation.Value == "AUS")
        {
            btnDelivery.Visible = false;
        }

        //1 For Module 2 For Inveerter
        //if (dtGetData.Rows.Count > 0)
        //{
        //    foreach (DataRow dr in dtGetData.Rows)
        //    {
        //        int DeliveryOrderStatus = Convert.ToInt32(dr["DeliveryOrderStatus"].ToString());
        //        if (DeliveryOrderStatus == 1)
        //        {
        //            // divdelivery.Visible = true;
        //            if (Convert.ToInt32(StockCategoryID.Value) == 1)
        //            {

        //                //DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
        //                DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(dr["StockOrderID"].ToString(), "1", dr["StockItemID"].ToString());
        //                //foreach (DataRow row in dtGetData.Rows)
        //                //{
        //                //get the values here
        //                CategoryID = Convert.ToInt32(dr["StockCategoryID"].ToString());
        //                OrderQuantity = Convert.ToInt32(dr["OrderQuantity"].ToString());
        //                    //if (CategoryID == 1)
        //                    //{
        //                        if (OrderQuantity == dt.Rows.Count)
        //                        {
        //                            btnDelivery.Visible = false;
        //                            //checkimag.Visible = true;
        //                            lnkExcelUploded.Visible = true;
        //                            lnkrevert.Visible = true;
        //                        }
        //                        else
        //                        {
        //                            btnDelivery.Visible = true;
        //                            //checkimag.Visible = false;
        //                            lnkExcelUploded.Visible = false;
        //                            lnkrevert.Visible = false;
        //                        }
        //                    //}
        //                //}

        //            }
        //            else if (Convert.ToInt32(StockCategoryID.Value) == 2)
        //            {
        //                DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

        //                foreach (DataRow row in dtGetData.Rows)
        //                {
        //                    //get the values here
        //                    CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
        //                    OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
        //                    if (CategoryID == 2)
        //                    {
        //                        if (OrderQuantity == dt.Rows.Count)
        //                        {
        //                            btnDelivery.Visible = false;
        //                            //checkimag.Visible = true;
        //                            lnkExcelUploded.Visible = true;
        //                            lnkrevert.Visible = true;
        //                        }
        //                        else
        //                        {
        //                            btnDelivery.Visible = true;
        //                            //checkimag.Visible = false;
        //                            lnkExcelUploded.Visible = false;
        //                            lnkrevert.Visible = false;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else if (DeliveryOrderStatus == 0)
        //        {
        //            // divdelivery.Visible = false;
        //            // btnDelivery.Visible = false;
        //            if (Convert.ToInt32(StockCategoryID.Value) == 1)
        //            {

        //                DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
        //                foreach (DataRow row in dtGetData.Rows)
        //                {
        //                    //get the values here
        //                    CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
        //                    OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
        //                    if (CategoryID == 1)
        //                    {
        //                        if (OrderQuantity == dt.Rows.Count)
        //                        {
        //                            btnDelivery.Visible = false;
        //                            //checkimag.Visible = true;
        //                            //lnkExcelUploded.Visible = true;
        //                            //lnkrevert.Visible = true;
        //                        }
        //                        else
        //                        {
        //                            btnDelivery.Enabled = false;
        //                            //checkimag.Visible = false;
        //                            //lnkExcelUploded.Visible = false;
        //                            //lnkrevert.Visible = false;
        //                        }
        //                    }
        //                }

        //            }
        //            else if (Convert.ToInt32(StockCategoryID.Value) == 2)
        //            {
        //                DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

        //                foreach (DataRow row in dtGetData.Rows)
        //                {
        //                    //get the values here
        //                    CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
        //                    OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
        //                    if (CategoryID == 2)
        //                    {
        //                        if (OrderQuantity == dt.Rows.Count)
        //                        {
        //                            btnDelivery.Visible = false;
        //                            //checkimag.Visible = true;
        //                            //lnkExcelUploded.Visible = true;
        //                            //lnkrevert.Visible = true;
        //                        }
        //                        else
        //                        {
        //                           btnDelivery.Enabled = false;
        //                            // checkimag.Visible = false;
        //                            //lnkExcelUploded.Visible = false;
        //                            //lnkrevert.Visible = false;
        //                        }
        //                    }
        //                }
        //            }

        //        }
        //        else
        //        {
        //             btnDelivery.Visible = false;
        //        }
        //    }
        //}
    }

    protected void rptOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delivered")
        {
            ModalPopupdeliver.Show();
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string StockOrderID = commandArgs[0];
            string StockOrderItemID = commandArgs[1];
            hndStockOrderItemID.Value = StockOrderItemID;


            hndid.Value = StockOrderID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
        }

        if (e.CommandName == "Revert")
        {
            ModalPopupRevert.Show();
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string StockOrderID = commandArgs[0];
            string StockOrderItemID = commandArgs[1];
            hndStockOrderItemID.Value = StockOrderItemID;
            hndid.Value = StockOrderID;
        }
    }

    protected void btndeliver_Click(object sender, EventArgs e)
    {
        //string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Section = "";
        string Message = "";
        //string[] SerialNoID = new string[] { };
        //List<string> SerialNoID = new List<string>();
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

        int success1 = 0;
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;

        if (FileUpload1.HasFile)
        {
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
            string connectionString = "";

            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    // string employeeid = string.Empty;
                    //string flag = "true";
                    // SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

                    //DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum(hndid.Value);
                    DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum_ByStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr2 = dt3.Rows[0];
                    string qty = dr2["Qty"].ToString();

                    //DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                    DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr1 = dt2.Rows[0];
                    string stockid = dr1["StockItemID"].ToString();
                    int count = 0;
                    int exist = 0;
                    var ExistSerialNo = new List<string>();
                    List<string> NovalueList = new List<string>();
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        //Response.Write(dr.Depth);
                        //Response.End();
                        int blank = 0;
                        int flagcolNames = 0;

                        if (dr.HasRows)
                        {
                            var columns = new List<string>();
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                columns.Add(dr.GetName(i));
                                if (!string.IsNullOrEmpty(dr.GetName(i)))
                                {
                                    string attributevalue0 = "Serial No";
                                    string attributevalue1 = "Pallet";
                                    int j = 0;
                                    string columnname = dr.GetName(i).ToString();
                                    if (columnname.StartsWith(attributevalue0) == true || columnname.StartsWith(attributevalue1) == true)
                                    {
                                        flagcolNames = 0;
                                    }
                                    else
                                    {
                                        flagcolNames = 1;
                                        break;
                                    }
                                }
                            }

                            if (flagcolNames == 0)
                            {
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";
                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();

                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank = 1;
                                        break;
                                    }
                                    if (blank != 1)
                                    {
                                        exist = ClstblStockOrders.tblStockSerialNo_Exist(SerialNo);
                                        if (exist == 0)
                                        {
                                            count++;
                                        }
                                        else
                                        {
                                            sttblStockSerialNo stitem = ClstblStockOrders.tblStockSerialNo_Getdata_BySerialNo(SerialNo);
                                            //blank = 1;                                           
                                            ExistSerialNo.Add(stitem.SerialNo.ToString());
                                            NovalueList.Add("exist");
                                            //  break;
                                        }

                                    }
                                }
                            }
                            else
                            {
                                ExcelFileHeader();
                            }
                            //Response.Write();
                        }
                    }
                    //Response.Write(count);
                    //Response.End();
                    if ((NovalueList != null) && (!NovalueList.Any()))
                    {
                        PanSerialNo.Visible = false;
                        if ((count == Convert.ToInt32(qty)) && (!NovalueList.Any()))
                        {

                            using (DbDataReader dr = command.ExecuteReader())
                            {

                                if (dr.HasRows)
                                {
                                    //SetAdd1();

                                    //if (flag == "false")
                                    //{
                                    //    PanEmpty.Visible = true;
                                    //}
                                    //else
                                    //{

                                    try
                                    {
                                        while (dr.Read())
                                        {

                                            int blank2 = 0;
                                            string SerialNo = "";
                                            string Pallet = "";

                                            SerialNo = dr["Serial No"].ToString();
                                            Pallet = dr["Pallet"].ToString();
                                            if (SerialNo == "" && Pallet == "")
                                            {
                                                blank2 = 1;
                                            }
                                            if (blank2 != 1)
                                            {
                                                if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                                {
                                                    try
                                                    {
                                                        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                                        // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                                        //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                                        //Response.End();
                                                        // success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);

                                                        string StockOrderID = hndid.Value;
                                                        Section = "Stock Order";
                                                        Message = "Stock In for Order Number:" + StockOrderID;

                                                        //success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID);
                                                        success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID_ForPortal(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID, "");
                                                        int maintaninid = ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, StockOrderID, SerialNo, Section, Message, Currendate);
                                                        ClstblMaintainHistory.tblMaintainHistory_Updateinoutflag(Convert.ToString(maintaninid), "");
                                                        ClstblStockOrders.tblStockSerialNo_UpdateStockOrderItemID(success1.ToString(), hndStockOrderItemID.Value);

                                                    }
                                                    catch (Exception ex123)
                                                    {

                                                    }
                                                }
                                                if (success1 > 0)
                                                {
                                                    SetAdd1();
                                                    string StockOrderID = Request.QueryString["StockOrderID"].ToString();
                                                    Binddata(StockOrderID);

                                                }
                                                else
                                                {
                                                    SetError1();
                                                }
                                            }
                                            //}
                                        }
                                    }
                                    catch { }
                                }
                            }
                            // bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, null, "1");
                            DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(Request.QueryString["StockOrderID"].ToString());
                            bool success = false;
                            bool success2 = false;
                            if (dtstock.Rows.Count == Convert.ToInt32(st.OrderQuantity))
                            {
                                success2 = ClstblStockOrders.tblStockOrders_Update_DeliveredOnly(hndid.Value, "1");//this flag denotes that all excel are uploded for stockorder
                                success = ClstblStockOrders.tblStockOrders_Update_ExcelUploaded(hndid.Value, "true");//this flag is updated when all excels are uploaded for all items.
                                SttblStockOrders st1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
                                if (!string.IsNullOrEmpty(st1.StockFromNew) && st1.StockFromNew == "Local")
                                {
                                    ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(hndid.Value, "1");
                                }

                            }
                            ClstblStockOrders.tblStockOrderItems_Update_ItemDeliveredOnly(hndStockOrderItemID.Value, "1");//This is to only show that excel is uploaded for particular stockorderitem
                            ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(hndid.Value, "1");
                            if (success && success2)
                            {
                                //if (receive != string.Empty)
                                //{
                                //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                                //}

                                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                                //string[] SerialNoIDSingle = SerialNoID.ToArray();
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                                        //  ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                SetError();
                            }


                        }
                        else
                        {
                            //if (exist == 0)
                            //{
                            RecordMisMatch();
                            // }
                            //if (exist == 1)
                            //{
                            //    SerialNoExist();
                            //}
                        }
                    }
                    else
                    {
                        PanSerialNo.Visible = true;
                        int sizeOfList = ExistSerialNo.Count;
                        List<string> NovalueListmsg = new List<string>();
                        for (int i = 0; i < sizeOfList; i++)
                        {
                            string Something = Convert.ToString(ExistSerialNo[i]) + ",";
                            string error = Something.TrimEnd(',');
                            NovalueListmsg.Add(Something);


                            //Response.Write(Convert.ToString(orderList[i]) + "<br \\");
                        }
                        ltrerror.Text = "Duplicate Serial No:<br/>" + string.Join("<br/>", NovalueListmsg).TrimEnd(',');
                    }

                }
            }
            try
            {
                SiteConfiguration.deleteimage(FileUpload1.FileName, "Delivery");
            }
            catch { }
        }
        else
        {
            //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
            //if (success)
            //{
            //    //if (receive != string.Empty)
            //    //{
            //    //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
            //    //}

            //    //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
            //    DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
            //            ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
            //            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
            //        }
            //    }
            //    SetAdd1();
            //}
            //else
            //{
            //    SetError();
            //}
        }

        lnkBack.Visible = true;
        txtdatereceived.Text = string.Empty;
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void RecordMisMatch()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunRecordMismatch();", true);
    }

    public void SerialNoExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSerialNoExist();", true);
    }

    public void ExcelFileHeader()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunExcelFileHeader();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    public void MyfunManualMsg(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyfunManulMsg('{0}');", msg), true);
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        // lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        // PanGrid.Visible = true;
        PanGridSearch.Visible = true;


    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockorder.aspx");
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string StockOrderItemID = hndStockOrderItemID.Value;
        string StockOrderID = hndid.Value;

        //DataTable dt1 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderItemID(StockOrderItemID);
        //if (dt1.Rows.Count > 0)
        //{
        //    string SerialNo = "";
        //    foreach (DataRow dtserial in dt1.Rows)
        //    {
        //        SerialNo = dtserial["SerialNo"].ToString();
        //        bool sucess = ClstblMaintainHistory.tblMaintainHistory_Delete_BySerailNo(SerialNo);
        //    }
        //}

        bool sucess = ClstblMaintainHistory.Delate_tblMaintainHistoryByStockOrderItemID(StockOrderItemID);

        if (!string.IsNullOrEmpty(StockOrderItemID))
        {
            ClstblStockOrders.tblStockOrderItems_Update_ItemDeliveredOnly(StockOrderItemID, "0");
            ClstblStockOrders.tblStockSerialNo_DeleteBYStockOrderItemID(StockOrderItemID);
        }
        if (!string.IsNullOrEmpty(StockOrderID))
        {
            Binddata(StockOrderID);
        }

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);
        if (st.verifyStatus == "0")//once stock order is verified it should not allow to change delivered flag on reverting excel files
        {
            DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(StockOrderID);
            if (dtstock.Rows.Count != Convert.ToInt32(st.OrderQuantity))
            {
                bool success2 = ClstblStockOrders.tblStockOrders_Update_DeliveredOnly(StockOrderID, "0");//when all excel are reverted 
                bool success = ClstblStockOrders.tblStockOrders_Update_ExcelUploaded(hndid.Value, "false");//when all excel are reverted 
            }
        }

    }

    protected void btnUpdateExcel_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            try
            {
                //Save Excel in local Folder
                FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
                string connectionString = "";

                if (FileUpload1.FileName.EndsWith(".xls"))
                {
                    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
                }
                else if (FileUpload1.FileName.EndsWith(".xlsx"))
                {
                    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=2'";
                }

                //get Comman Data=============================================
                string StockOrderID = hndid.Value;
                string Section = "Stock Order";
                string Message = "Stock In for Order Number:" + StockOrderID;

                DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                DataRow dr1 = dt2.Rows[0];
                string StockItemID = dr1["StockItemID"].ToString();

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                string StockOrderItemID = hndStockOrderItemID.Value;
                SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
                //===========================================================


                //Read Excel==================================================================================================================
                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    //Get SheetName------------------------------------
                    DataTable tbl = connection.GetSchema("Tables");
                    string sheetName = "";
                    if (tbl.Rows.Count > 0)
                    {
                        sheetName = tbl.Rows[0]["TABLE_NAME"].ToString();
                    }
                    //-------------------------------------------------

                    using (DbCommand command = connection.CreateCommand())
                    {
                        if (sheetName != "")
                        {
                            command.CommandText = "SELECT * FROM [" + sheetName + "]";
                            command.CommandType = CommandType.Text;
                        }
                        else
                        {
                            command.CommandText = "SELECT * FROM [Sheet1$]";
                            command.CommandType = CommandType.Text;
                        }

                        DataTable dtSerialNo = new DataTable();
                        dtSerialNo.Columns.AddRange(new DataColumn[12] {
                            new DataColumn("StockOrderID", typeof(Int32)),
                            new DataColumn("Section", typeof(string)),
                            new DataColumn("Message", typeof(string)),
                            new DataColumn("SerialNo", typeof(string)),
                            new DataColumn("Pallet", typeof(string)),
                            new DataColumn("StockItemID", typeof(Int32)),
                            new DataColumn("userid", typeof(string)),
                            new DataColumn("IsActive", typeof(string)),
                            new DataColumn("CompanyLoactionID", typeof(string)),
                            new DataColumn("Currendate", typeof(DateTime)),
                            new DataColumn("inoutflag", typeof(string)),
                            new DataColumn("StockOrderItemID", typeof(Int32))
                        });

                        //Record for Exists Serial No
                        var ExistSerialNo = new List<string>();

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            int blank = 0;
                            int exists = 0;
                            if (dr.HasRows)
                            {
                                var columns1 = dr.GetName(0);
                                var columns2 = dr.GetName(1);

                                while (dr.Read())
                                {
                                    string SerialNo = dr[columns1].ToString();
                                    string Pallet = dr[columns2].ToString();

                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank = 1;
                                        break;
                                    }

                                    if (blank != 1)
                                    {
                                        int exist = ClstblStockOrders.tblStockSerialNo_Exist(SerialNo);
                                        if (exist == 1)
                                        {
                                            exists++;
                                            ExistSerialNo.Add(SerialNo.ToString());
                                        }
                                        else // Exists Serial No
                                        {
                                            //sttblStockSerialNo stitem = ClstblStockOrders.tblStockSerialNo_Getdata_BySerialNo(SerialNo);
                                            ////blank = 1;                                           
                                            //ExistSerialNo.Add(stitem.SerialNo.ToString());
                                            //NovalueList.Add("exist");
                                            //  break;

                                            //try
                                            //{

                                            //success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID_ForPortal(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID, "");
                                            //int maintaninid = ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, StockOrderID, SerialNo, Section, Message, Currendate);
                                            //ClstblMaintainHistory.tblMaintainHistory_Updateinoutflag(Convert.ToString(maintaninid), "");
                                            //ClstblStockOrders.tblStockSerialNo_UpdateStockOrderItemID(success1.ToString(), hndStockOrderItemID.Value);

                                        }
                                        dtSerialNo.Rows.Add(Convert.ToInt32(StockOrderID), Section, Message, SerialNo, Pallet, Convert.ToInt32(StockItemID), userid, null, "", Currendate, null, StockOrderItemID);

                                    }
                                }

                                if (dtSerialNo.Rows.Count != 0)
                                {
                                    if (exists == 0)
                                    {
                                        PanSerialNo.Visible = false;

                                        DataTable dtQty = ClstblStockOrders.tblStockOrderItems_QtySum_ByStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                                        DataRow dr2 = dtQty.Rows[0];
                                        string qty = dr2["Qty"].ToString();

                                        if (qty == (dtSerialNo.Rows.Count).ToString())
                                        {
                                            string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                                            try
                                            {
                                                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(constr))
                                                {
                                                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("USP_InsertUpdate_tblMaintanHistory_tblStockSerialNo_InsertBulk"))
                                                    {
                                                        cmd.CommandType = CommandType.StoredProcedure;
                                                        cmd.Connection = con;
                                                        cmd.Parameters.AddWithValue("@tblMaintanHistory_tblStockSerialNo", dtSerialNo);
                                                        con.Open();
                                                        cmd.ExecuteNonQuery();
                                                        con.Close();
                                                        Notification("Transaction Successful.");


                                                        DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(Request.QueryString["StockOrderID"].ToString());
                                                        bool success = false;
                                                        bool success2 = false;
                                                        if (dtstock.Rows.Count == Convert.ToInt32(st.OrderQuantity))
                                                        {
                                                            success2 = ClstblStockOrders.tblStockOrders_Update_DeliveredOnly(hndid.Value, "1");//this flag denotes that all excel are uploded for stockorder
                                                            success = ClstblStockOrders.tblStockOrders_Update_ExcelUploaded(hndid.Value, "true");//this flag is updated when all excels are uploaded for all items.
                                                            SttblStockOrders st1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
                                                            if (!string.IsNullOrEmpty(st1.StockFromNew) && st1.StockFromNew == "Local")
                                                            {
                                                                ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(hndid.Value, "1");
                                                            }

                                                        }
                                                        ClstblStockOrders.tblStockOrderItems_Update_ItemDeliveredOnly(hndStockOrderItemID.Value, "1");//This is to only show that excel is uploaded for particular stockorderitem
                                                        ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(hndid.Value, "1");
                                                        if (success && success2)
                                                        {


                                                            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);

                                                            if (dt.Rows.Count > 0)
                                                            {
                                                                foreach (DataRow row in dt.Rows)
                                                                {
                                                                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                                                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");

                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            SetError();
                                                        }
                                                    }
                                                }
                                            }
                                            catch
                                            {
                                                Notification("Transaction Failed.");
                                            }
                                        }
                                        else
                                        {
                                            Notification("Serial No Qty & Stock Order Qty did not match");
                                        }
                                    }
                                    else
                                    {
                                        //Notification("Serial No Exist");
                                        PanSerialNo.Visible = true;
                                        int sizeOfList = ExistSerialNo.Count;
                                        List<string> NovalueListmsg = new List<string>();
                                        for (int i = 0; i < sizeOfList; i++)
                                        {
                                            string Something = Convert.ToString(ExistSerialNo[i]) + ",";
                                            string error = Something.TrimEnd(',');
                                            NovalueListmsg.Add(Something);


                                            //Response.Write(Convert.ToString(orderList[i]) + "<br \\");
                                        }
                                        ltrerror.Text = "Duplicate Serial No:<br/>" + string.Join("<br/>", NovalueListmsg).TrimEnd(',');
                                    }
                                }
                                else
                                {
                                    Notification("File is null");
                                }

                            }
                        }
                    }
                }
                //=============================================================================================================================

                Binddata(StockOrderID);
                //delete Excel from local Folder
                SiteConfiguration.deleteimage(FileUpload1.FileName, "Delivery");
            }
            catch (Exception ex)
            {
                Notification(ex.Message);
            }
        }
        else
        {
            Notification("Please Select File..");
        }
    }

    public void Notification(string msg)
    {
        try
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
        }
        catch (Exception e)
        {

        }
    }

    //[WebMethod]
    //public static string UploadExcel(string FileName, string Extension)
    //{
    //    var Data = new Result();
    //    string Result = "Success Test " +  FileName + " " + Extension;

    //    Data = new Result
    //    {
    //        result = Result
    //    };

    //    return JsonConvert.SerializeObject(Data);
    //}

    //public class Result
    //{
    //    public string result { get; set; }
    //}
}