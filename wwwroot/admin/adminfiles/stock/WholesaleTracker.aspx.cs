using Newtonsoft.Json;
using RestSharp;
using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_Wholesale : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string mode = "";
    protected string SiteURL;
    static DataView dv;

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    public class ClsBridgeSelect
    {
        public string crmid;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {
            //txtreferenceno.Text = "265637,274665";
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees sttblEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            if (Roles.IsUserInRole("Administrator") || userid == "41934448-5395-4ce3-a919-83d8a9ba69bb")
            {
                Fddl.Visible = true;
                Fbtn.Visible = true;
                FbtnSTC.Visible = true;
            }
            else
            {
                Fddl.Visible = false;
                Fbtn.Visible = false;
                FbtnSTC.Visible = false;
            }

            
            if (Roles.IsUserInRole("Warehouse"))
            {
                BindLocation();
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["State"].ToString();
                ddlSearchState.SelectedValue = CompanyLocationID;
                ddlSearchState.Enabled = false;
            }
            else
            { BindLocation(); }
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();


            BindPVDStatus();

            BindSearchEmployee();
            BindVendor();
            BindTransportType();
            BindSolarType();
            BindInstallType();
            BindJobStatus();
            BindInstallerName();
            BindGrid(0);
            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Wholesale")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else if ((Roles.IsUserInRole("WarehouseManager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //  GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }
        }

        ModeAddUpdate();

    }
    //Bind Employee Dropdown
    public void BindSearchEmployee()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlSearchEmployee.DataSource = dt;
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();
    }

    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlStockLocation.DataSource = dt;
        ddlStockLocation.DataTextField = "location";
        ddlStockLocation.DataValueField = "CompanyLocationID";
        ddlStockLocation.DataBind();

        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();

        //ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataSource = dt;
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "CompanyLocationID";
        ddlSearchState.DataBind();
    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlVendor.DataSource = dt;
        ddlVendor.DataTextField = "Customer";
        ddlVendor.DataValueField = "CustomerID";
        ddlVendor.DataBind();

        ddlSearchVendor.DataSource = dt;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();
    }

    public void BindPVDStatus()
    {
        //DataTable dt = new DataTable();
        //dt.Columns.Add("PVDStatusID", typeof(int));
        //dt.Columns.Add("PVDStatus", typeof(string));

        //dt.Rows.Add(8, "Traded");
        //dt.Rows.Add(1, "Pending");
        //dt.Rows.Add(5, "Approved");
        //dt.Rows.Add(6, "Failed");
        //dt.Rows.Add(7, "Blank");
        //dt.Rows.Add(9, "Complaince");

        //rptPVDStatus.DataSource = dt;
        //rptPVDStatus.DataBind();

        

        DataTable dt = Clstbl_WholesaleOrders.tbl_GBSTCStatus_Select();
        rptPVDStatus.DataSource = dt;
        rptPVDStatus.DataBind();

        ddlstatuspvd.DataSource = dt;
        ddlstatuspvd.DataTextField = "PVDStatus";
        ddlstatuspvd.DataValueField = "PVDStatusID";
        ddlstatuspvd.DataBind();
    }

    public void BindSolarType()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblSolarType_Select();
        ddlSolarType.DataSource = dt;
        ddlSolarType.DataTextField = "SolarTypeName";
        ddlSolarType.DataValueField = "Id";
        ddlSolarType.DataBind();
    }

    public void BindInstallType()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblInstallType_Select();
        ddlinstallertype.DataSource = dt;
        ddlinstallertype.DataTextField = "InstallTypeName";
        ddlinstallertype.DataValueField = "Id";
        ddlinstallertype.DataBind();
    }

    public void BindJobStatus()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblJobStatus_Select("1");
        //ddljobstatus.DataSource = dt;
        //ddljobstatus.DataTextField = "JobStatusType";
        //ddljobstatus.DataValueField = "Id";
        //ddljobstatus.DataBind();
        //ddljobstatus.SelectedValue = "1";

        //lstJobStatus.DataSource = dt;
        //lstJobStatus.DataTextField = "JobStatusType";
        //lstJobStatus.DataValueField = "Id";
        //lstJobStatus.DataBind();
        //ddljobstatus.SelectedValue = "1";

        ddlJobStatusUpdate.DataSource = dt;
        ddlJobStatusUpdate.DataTextField = "JobStatusType";
        ddlJobStatusUpdate.DataValueField = "Id";
        ddlJobStatusUpdate.DataBind();

        rptJobStatus.DataSource = dt;
        rptJobStatus.DataBind();
    }

    public void BindInstallerName()
    {
        ddlinstallername.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlinstallername.DataValueField = "ContactID";
        ddlinstallername.DataMember = "Contact";
        ddlinstallername.DataTextField = "Contact";
        ddlinstallername.DataBind();
    }

    public void BindTransportType()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblTransportType_Selectwithoutparameter();
        ddlTransporttype.DataSource = dt;
        ddlTransporttype.DataTextField = "TransportTypeName";
        ddlTransporttype.DataValueField = "Id";
        ddlTransporttype.DataBind();
    }

    protected DataTable GetGridData()
    {
        //string PVDStatus = lstPVDStatus.SelectedValue;
        string PVDStatus = "";
        foreach (RepeaterItem item in rptPVDStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnPVDStatusID = (HiddenField)item.FindControl("hdnPVDStatusID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                PVDStatus += "," + hdnPVDStatusID.Value.ToString();
            }
        }
        if (PVDStatus != "")
        {
            PVDStatus = PVDStatus.Substring(1);
        }

        //string JobStatus = lstJobStatus.SelectedValue;
        string JobStatus = "";
        foreach (RepeaterItem item in rptJobStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnJobStatusTypeID = (HiddenField)item.FindControl("hdnJobStatusTypeID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                JobStatus += "," + hdnJobStatusTypeID.Value.ToString();
            }
        }
        if (JobStatus != "")
        {
            JobStatus = JobStatus.Substring(1);
        }

        DataTable dt = new DataTable();
        dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectforTracker(ddlShow.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), txtreferenceno.Text.Trim(), ddlSearchState.SelectedValue, ddlDeliveredOrNot.SelectedValue, txtpvdnumber.Text, PVDStatus, ddlTransporttype.SelectedValue, ddlinstallertype.SelectedValue, ddlinstallername.SelectedValue, ddlSolarType.SelectedValue, JobStatus, ddlSearchjobtype.SelectedValue, ddlSearchdeliveryoption.SelectedValue, ddlSearchEmployee.SelectedValue, ddlApproved.SelectedValue, ddlSearchPaymentStatus.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

        BindTotal(dt);
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            //====================================STC Status Wise Sum.===============================================
            //string PendingSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Pending").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string ApprSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Approved").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string TradedSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Traded").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string ApPVD = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "PVD Applied").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string FailedSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Failed").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string BlankSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Blank").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string VoidSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Void").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //===========================================End=========================================================

            lblPVDTotal.Text = dt.Compute("Count(GB_STCStatus)", string.Empty).ToString();

            string Panels = dt.Compute("SUM(PanelQty)", string.Empty).ToString();
            lblTotalPanels.Text = Panels == "" ? "0" : Panels;

            string Inverter = dt.Compute("SUM(InverterQty)", string.Empty).ToString();
            lblTotalInverters.Text = Inverter == "" ? "0" : Inverter;

            string Amount = dt.Compute("SUM(InvoiceAmount)", string.Empty).ToString();
            lblTotalAmount.Text = Amount == "" ? "0" : Amount;

            //string Pending = dt.Compute("Count(Status)", "Status='Pending'").ToString();
            //lblTotPending.Text = Pending == "" ? "0" : Pending + "/" + PendingSTC;

            //lblTotApproved.Text = dt.Select("Status = 'Approved'").Length.ToString() + "/" + ApprSTC;

            //lblTotTraded.Text = dt.Select("Status = 'Traded'").Length.ToString() + "/" + TradedSTC;

            ////lblTotPVDApplied.Text = dt.Select("Status = 'PVD Applied'").Length.ToString() + "/" + ApPVD;

            //lblTotFailed.Text = dt.Select("Status = 'Failed'").Length.ToString() + "/" + FailedSTC;

            //lblTotBlank.Text = dt.Select("Status = 'Blank'").Length.ToString() + "/" + BlankSTC;

            //lblTotVoid.Text = dt.Select("Status = 'Void'").Length.ToString() + "/" + VoidSTC;

            //lblPVDTotal.Text = dt.Compute("Count(Status)", string.Empty).ToString();

            //lblJobStatusVoid.Text = dt.Select("JobStatusId = '3'").Length.ToString();

            //lblComplaince.Text = dt.Select("JobStatusId = '9'").Length.ToString();

            int STC = 0;
            int TotalSTC = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int STCNumber = 0;
                if (!string.IsNullOrEmpty(dt.Rows[i]["STCNumber"].ToString()))
                {
                    STCNumber = Convert.ToInt32(dt.Rows[i]["STCNumber"]);
                }

                STC = STCNumber;
                TotalSTC += STC;
            }

            lblTotalSTC.Text = TotalSTC.ToString();

            dt.Columns.Add("Stc_Value_Excel1", typeof(int));
            dt.Columns.Add("TotalSTCValue", typeof(decimal));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["Stc_Value_Excel1"] = dt.Rows[i]["Stc_Value_Excel"].ToString() == "" ? 0 : dt.Rows[i]["Stc_Value_Excel"];

                int STCNumber = 0;
                if (!string.IsNullOrEmpty(dt.Rows[i]["STCNumber"].ToString()))
                {
                    STCNumber = Convert.ToInt32(dt.Rows[i]["STCNumber"]);
                }

                dt.Rows[i]["TotalSTCValue"] = Convert.ToDecimal(STCNumber) * Convert.ToDecimal(dt.Rows[i]["StcValue"].ToString() == "" ? 0 : dt.Rows[i]["StcValue"]);
            }

            string TotalSTCValue = dt.Compute("SUM(TotalSTCValue)", string.Empty).ToString();
            if (!TotalSTCValue.Contains("0.00"))
            {
                decimal avg = (Convert.ToDecimal(TotalSTCValue) / TotalSTC);
                lblAvg.Text = avg.ToString("F");
            }
            else
            {
                lblAvg.Text = "0.00";
            }
            

            //GB Status
            string NewSubmissionSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "New Submission").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblNewSubmission.Text = dt.Select("GB_STCStatus = 'New Submission'").Length.ToString() + "/" + NewSubmissionSTC;

            string SubmittoTradeSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Submit to Trade").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblSubmittoTrade.Text = dt.Select("GB_STCStatus = 'Submit to Trade'").Length.ToString() + "/" + SubmittoTradeSTC;

            string PendingApprovalSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Pending Approval").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblPendingApproval.Text = dt.Select("GB_STCStatus = 'Pending Approval'").Length.ToString() + "/" + PendingApprovalSTC;

            string ReadyToCreateSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Ready To Create").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblReadyToCreate.Text = dt.Select("GB_STCStatus = 'Ready To Create'").Length.ToString() + "/" + ReadyToCreateSTC;

            string CERFailedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "CER Failed").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCERFailed.Text = dt.Select("GB_STCStatus = 'CER Failed'").Length.ToString() + "/" + CERFailedSTC;

            string CannotrecreateSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Cannot re-create").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCannotrecreate.Text = dt.Select("GB_STCStatus = 'Cannot re-create'").Length.ToString() + "/" + CannotrecreateSTC;

            string NotYetSubmittedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Not Yet Submitted").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblNotYetSubmitted.Text = dt.Select("GB_STCStatus = 'Not Yet Submitted'").Length.ToString() + "/" + NotYetSubmittedSTC;

            string ComplianceIssuesSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Compliance Issues").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblComplianceIssues.Text = dt.Select("GB_STCStatus = 'Compliance Issues'").Length.ToString() + "/" + ComplianceIssuesSTC;

            string CERApprovedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "CER Approved").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCERApproved.Text = dt.Select("GB_STCStatus = 'CER Approved'").Length.ToString() + "/" + CERApprovedSTC;

            string UnderReviewSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Under Review").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblUnderReview.Text = dt.Select("GB_STCStatus = 'Under Review'").Length.ToString() + "/" + UnderReviewSTC;

            string BlankSTC = dt.AsEnumerable().Where(y => y.Field<Nullable<Int32>>("GB_STCStatusID") == 10).Sum(x => x.Field<Nullable<Int32>>("Stc_Value_Excel1")).ToString();
            lblBlank.Text = dt.Select("GB_STCStatusID = '10'").Length.ToString() + "/" + BlankSTC;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {

        int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(txtVendorInvoiceNo.Text);
        if (existaddress != 1)
        {
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            // string StockCategoryID = ddlStockCategoryID.SelectedValue.ToString();

            string DateOrdered = DateTime.Now.AddHours(14).ToString();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            string Notes = txtNotes.Text;
            string DeliveryDate = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            string ReferenceNo = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;

            var vals = "";
            var vals1 = "";

            if (ddlStockLocation.SelectedValue != string.Empty)
            {
                vals = StockLocation_Text.Split(':')[0];
                vals1 = StockLocation_Text.Split(':')[1];
            }
            string state = Convert.ToString(vals);
            int success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Insert(DateOrdered, st.EmployeeID, Notes, StockLocation, vendor, DeliveryDate, ReferenceNo, ExpectedDelivery);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_OrderNumber(success.ToString(), success.ToString());
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_VendorInvoiceNo(success.ToString(), txtVendorInvoiceNo.Text);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_DeliveryOption(success.ToString(), ddljobtype.SelectedValue, ddljobtype.SelectedItem.Text, txtinvoiceamt.Text, txtpvdno.Text, ddlstatus.SelectedValue, ddlstatus.SelectedItem.Text, ddldeliveryoption.SelectedItem.Text, ddldeliveryoption.SelectedValue);

            foreach (RepeaterItem item in rptattribute.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");

                    string StockItem = ddlStockItem.SelectedValue.ToString();
                    string OrderQuantity = txtOrderQuantity.Text;
                    string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                    CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                    if (!chkdelete.Checked)
                    {
                        if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                        {
                            int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                            //Response.Write(success1);

                        }
                    }
                }
            }
            // Response.End();

            //--- do not chage this code start------
            if (success > 0)
            {
                // SetAdd();
            }
            else
            {
                SetError();
            }
        }
        else
        {
            PAnAddress.Visible = true;
            //BindGrid(0);
            //lblexistame.Visible = true;
            //lblexistame.Text = "Record with this Address already exists ";
        }
        BindGrid(0);
        // Reset();
        mode = "Add";
        //--- do not chage this code end------

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
        if (exist == 0)
        {
            string Notes = txtNotes.Text;
            string BOLReceived = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            var vals = StockLocation_Text.Split(':')[0];
            var vals1 = StockLocation_Text.Split(':')[1];
            string state = Convert.ToString(vals);
            string ManualOrderNumber = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;

            bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_DeliveryOption(id1, ddljobtype.SelectedValue, ddljobtype.SelectedItem.Text, txtinvoiceamt.Text, txtpvdno.Text, ddlstatus.SelectedValue, ddlstatus.SelectedItem.Text, ddldeliveryoption.SelectedItem.Text, ddldeliveryoption.SelectedValue);
            Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_whole_Delete(id1);
            //Response.Write(rptattribute.Items.Count);
            //Response.End();
            foreach (RepeaterItem item in rptattribute.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
                    string StockItem = ddlStockItem.SelectedValue.ToString();
                    string OrderQuantity = txtOrderQuantity.Text;
                    string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                    CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");

                    if (!chkdelete.Checked)
                    {

                        if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                        {
                            int success1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Insert(id1, StockItem, OrderQuantity, StockOrderItem, state, ddlStockCategoryID.SelectedValue);
                        }
                    }

                    if (chkdelete.Checked)
                    {
                        Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Delete(hdnWholesaleOrderItemID.Value);
                    }
                }
            }
            //--- do not chage this code Start------
            if (success)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            //InitUpdate();
            PAnAddress.Visible = true;
        }
        BindScript();
        BindGrid(0);
        Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);



        //--- do not chage this code end------
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
        mode = "Edit";

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnWholesaleOrderID2.Value = id;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(id);
        ddlStockLocation.SelectedValue = st.CompanyLocationID;
        try
        {
            ddlVendor.SelectedValue = st.CustomerID;
        }
        catch { }
        try
        {
            txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
        }
        catch { }
        txtNotes.Text = st.Notes;
        txtManualOrderNumber.Text = st.ReferenceNo;
        try
        {
            txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
        }
        catch { }
        txtVendorInvoiceNo.Text = st.InvoiceNo;
        ddljobtype.SelectedValue = st.JobTypeID;
        ddldeliveryoption.SelectedValue = st.DeliveryOptionID;
        try
        {
            txtinvoiceamt.Text = SiteConfiguration.ChangeCurrency_Val(st.InvoiceAmount);
        }
        catch { }
        txtpvdno.Text = st.PVDNumber;
        ddlstatus.SelectedValue = st.StatusID;

        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(id);


        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;

        }
        ModeAddUpdate();
        //MaxAttribute = 1;
        //bindrepeater();
        //BindAddedAttribute();
        //--- do not chage this code start------
        //lnkBack.Visible = true;
        InitUpdate();

        if (st.IsDeduct == "1")
        {
            divfirstrow.Enabled = false;
            divrepeater.Enabled = false;
            divnote.Enabled = false;
        }
        else
        {
            divfirstrow.Enabled = true;
            divrepeater.Enabled = true;
            divnote.Enabled = true;
        }

        //--- do not change this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.ToString() == "Delivered")
        {
            ModalPopupdeliver.Show();
            string ID = e.CommandArgument.ToString();
            hndid.Value = ID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();

        }

        if (e.CommandName == "Revert")
        {
            ModalPopupRevert2.Show();
            hndid2.Value = e.CommandArgument.ToString();
        }

        if (e.CommandName == "Update")
        {
            hndWholesaleOrderID.Value = e.CommandArgument.ToString();
            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(hndWholesaleOrderID.Value);
            txtpvd.Text = st.PVDNumber;
            ddlstatuspvd.SelectedValue = st.GB_STCStatusID;

            DataTable dtwholesaleisrevert = ClstblStockSerialNo.tblStockSerialNo_Select_WholesaleOrderID(hndWholesaleOrderID.Value);
            DataTable dt1 = Clstbl_WholesaleOrders.tblJobStatus_Select("2");
            if (dtwholesaleisrevert.Rows.Count > 0)
            {
                ddlJobStatusUpdate.Items.Clear();
                ListItem listItem = new ListItem() { Text = "Select", Value = "" };
                ddlJobStatusUpdate.Items.Add(listItem);
                ddlJobStatusUpdate.DataSource = dt1;
                ddlJobStatusUpdate.DataTextField = "JobStatusType";
                ddlJobStatusUpdate.DataValueField = "Id";
                ddlJobStatusUpdate.DataBind();
            }

            if (st.JobStatusId != null || st.JobStatusId != "")
            {
                try
                {
                    ddlJobStatusUpdate.SelectedValue = st.JobStatusId.ToString();
                }
                catch(Exception ex) { }
            }
            //txtpvd.Text = string.Empty;
            //ddlstatuspvd.SelectedValue = "";
            ModalPopupExtenderUpdatePVDStatus.Show();
        }

        if (e.CommandName == "ViewPDF")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            Telerik_reports.generate_WholesalePicklist(WholesaleOrderID);
        }

        if (e.CommandName == "SendEmail")
        {
            try
            {
                string WholesaleOrderID = e.CommandArgument.ToString();

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                Sttbl_WholesaleOrders st2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
                SttblContacts st3 = ClstblContacts.tblContacts_SelectByCustomerID(st2.CustomerID);
                String Subject = "Wholesale Order Number: " + WholesaleOrderID + " has been dispatched.";

                Server.Execute("~/mailtemplate/WholesaleAttachment.aspx?WholesaleorderID=" + WholesaleOrderID, txtWriter);


                Telerik_reports.generate_WholesalePicklist(WholesaleOrderID, "Save");

                string FileName = WholesaleOrderID + "_Wholesale.pdf";

                string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SavePDF2\\", FileName);

                Utilities.SendMailWithAttachmentForAchieversEnergy(from, st3.ContEmail, Subject, txtWriter.ToString(), fullPath, FileName);

                //Utilities.SendMailWithAttachmentForAchieversEnergy(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), fullPath, FileName);

                SiteConfiguration.deleteimage(FileName, "SavePDF2");

                Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(WholesaleOrderID, "true");

                Notification("Email sent successfully to the Customer.");
            }
            catch (Exception ex)
            {
                Notification("Email not sent.");
            }
        }

        if (e.CommandName == "print")
        {
            string StockOrderID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/order.aspx?id=" + StockOrderID);
        }

        if (e.CommandName.ToLower() == "detail")
        {
            ModalPopupExtenderDetail.Show();
            hndWholesaleOrderID.Value = e.CommandArgument.ToString();
            string StockOrderID = e.CommandArgument.ToString();

            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(StockOrderID);

            if (st.CompanyLocation != string.Empty)
            {
                lblStockLocation.Text = st.CompanyLocation;
            }
            else
            {
                lblStockLocation.Text = "-";
            }
            if (st.vendor != string.Empty)
            {
                lblVendor.Text = st.vendor;
            }
            else
            {
                lblVendor.Text = "-";
            }
            if (st.BOLReceived != string.Empty)
            {
                lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
            }
            else
            {
                lblBOLReceived.Text = "-";
            }
            if (st.ReferenceNo != string.Empty)
            {
                lblManualOrderNo.Text = st.ReferenceNo;
            }
            else
            {
                lblManualOrderNo.Text = "-";
            }
            if (st.ExpectedDelivery != string.Empty)
            {
                lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
            }
            else
            {
                lblExpectedDelevery.Text = "-";
            }
            if (st.Notes != string.Empty)
            {
                lblNotes.Text = st.Notes;
            }
            else
            {
                lblNotes.Text = "-";
            }
            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(StockOrderID);
            if (dt.Rows.Count > 0)
            {
                rptOrder.DataSource = dt;
                rptOrder.DataBind();
                trOrderItem.Visible = true;
            }
            else
            {
                trOrderItem.Visible = false;
            }
        }

        if (e.CommandName == "Approve")
        {
            hndWholesaleOrderID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderverify.Show();

        }

        if (e.CommandName == "PO")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            //ModalPopupExtenderverify.Show();
            bool sucess = Clstbl_WholesaleOrders.tbl_WholesaleOrders_UpdatePoforTracker(WholesaleOrderID, "1");
            BindGrid(0);
        }

        if (e.CommandName == "Note")
        {

            hndWholesaleOrderID.Value = e.CommandArgument.ToString();
            string WholesaleOrderID = e.CommandArgument.ToString();
            //Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(hndWholesaleOrderID.Value);
            //txtpvd.Text = st.PVDNumber;
            //ddlstatuspvd.SelectedValue = st.StatusID;
            //txtpvd.Text = string.Empty;
            //ddlstatuspvd.SelectedValue = "";
            BindGridNotes(WholesaleOrderID);
            ModalPopupExtenderNote.Show();
        }

        if (e.CommandName == "UpdateSTCValue")
        {
            string [] arg = e.CommandArgument.ToString().Split(';');
            hndWholesaleOrderID.Value = arg[0];
            txtSTCValue.Text = arg[1];
            ModalPopupExtender3.Show();
        }

        //BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindScript();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        MaxAttribute = 1;
        bindrepeater();
        BindAddedAttribute();
        BindScript();
        PanGridSearch.Visible = false;
        PanGrid.Visible = false;
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Reset();
        InitAdd();
        mode = "Add";
        if (mode == "Add")
        {
            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
        }
        PanGrid.Visible = false;
        divfirstrow.Enabled = true;
        divrepeater.Enabled = true;
        divnote.Enabled = true;
        //  PanGridSearch.Visible = false;
        BindScript();
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        // Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        Notification("Transaction Failed.");
        //PanError.Visible = true;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;

        PanAddUpdate.Visible = true;
        rptattribute.DataSource = null;
        rptattribute.DataBind();
        //rptviewdata.DataSource = null;
        //rptattribute.DataBind();
        ddlStockLocation.ClearSelection();
        ddlVendor.ClearSelection();
        txtBOLReceived.Text = "";
        txtNotes.Text = "";
        txtManualOrderNumber.Text = string.Empty;
        txtExpectedDelivery.Text = string.Empty;
        //rptviewdata.Visible = false;

        ddlstockcategory.ClearSelection();
        //txtstockitem.Text = string.Empty;
        //txtbrand.Text = string.Empty;
        //txtmodel.Text = string.Empty;
        //txtseries.Text = string.Empty;
        //txtminstock.Text = string.Empty;
        //chksalestag.Checked = false;
        //chkisactive.Checked = false;
        //txtdescription.Text = string.Empty;
        //txtStockSize.Text = string.Empty;
        txtVendorInvoiceNo.Text = string.Empty;
        ddljobtype.SelectedValue = "";
        ddldeliveryoption.SelectedValue = "";
        txtreferenceno.Text = string.Empty;
        txtinvoiceamt.Text = string.Empty;
        txtpvdno.Text = string.Empty;
        ddlstatus.SelectedValue = "";
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            txtqty.Text = "0";
        }
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            txtOrderQuantity.Text = "";
            ddlStockItem.SelectedValue = "";
            ddlStockCategoryID.SelectedValue = "";
            txtAvailableQuantity.Text = string.Empty;
        }

    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void ddlStockLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            ddlStockItem.Items.Clear();

            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            if (ddlStockLocation.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "")
            {
                DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
                ddlStockItem.DataSource = dtStockItem;
                ddlStockItem.DataTextField = "StockItem";
                ddlStockItem.DataValueField = "StockItemID";
                ddlStockItem.DataBind();
                txtOrderQuantity.Text = string.Empty;
                txtAvailableQuantity.Text = string.Empty;
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {

        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");

        if (ddlStockLocation.SelectedValue != "")
        {
            ddlStockItem.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();
            if (ddlStockItem.SelectedValue == "")
            {
                txtAvailableQuantity.Text = string.Empty;
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");

        if (ddlStockLocation.SelectedValue != "")
        {
            if (ddlStockCategoryID.SelectedValue != "")
            {
                DataTable dtstock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockLocation.SelectedValue, ddlStockItem.SelectedValue);
                if (dtstock.Rows.Count > 0)
                {
                    txtAvailableQuantity.Text = dtstock.Rows[0]["StockQuantity"].ToString();
                }
                else
                {
                    txtAvailableQuantity.Text = string.Empty;
                }
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        //  BindScript();
    }

    protected void btnAddUpdateRow_Click(object sender, EventArgs e)
    {
        InitUpdate();
        AddmoreAttribute();
    }

    protected void rptattribute_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");
        Button btnRevertScannedItem = (Button)item.FindControl("btnRevertScannedItem");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtOrderQuantity");
        TextBox txtAvailableQuantity = (TextBox)e.Item.FindControl("txtAvailableQuantity");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");

        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(hdnStockCategory.Value, ddlStockLocation.SelectedValue.ToString());
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();
        HiddenField hdnOrderQuantity = (HiddenField)e.Item.FindControl("hdnOrderQuantity");
        HiddenField hdnAvailableQuantity = (HiddenField)e.Item.FindControl("hdnAvailableQuantity");
        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtOrderQuantity.Text = hdnOrderQuantity.Value;
        txtAvailableQuantity.Text = hdnAvailableQuantity.Value;
        //RequiredFieldValidator RequiredFieldValidator11 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator11");
        //RequiredFieldValidator RequiredFieldValidator1 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator1");
        //RequiredFieldValidator RequiredFieldValidator19 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator19");

        //if (e.Item.ItemIndex == 0)
        //{
        //    //btnDelete.Visible = false;
        //    chkdelete.Visible = false;
        //    litremove.Visible = false;
        //    //RequiredFieldValidator11.Visible = true;
        //    // RequiredFieldValidator1.Visible = true;
        //    //   RequiredFieldValidator19.Visible = true;
        //}
        //else
        //{
        //    //btnDelete.Visible = true;
        //    chkdelete.Visible = true;
        //    litremove.Visible = true;

        //    //RequiredFieldValidator11.Visible = false;
        //    //RequiredFieldValidator1.Visible = false;
        //    //  RequiredFieldValidator19.Visible = false;
        //}

        try//it should above code where remove btn ibdex=0 is made visible false
        {
            DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
            //DataTable dtstock2 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(hdnStockOrderID2.Value);
            //SttblStockOrderItems st = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockOrderItemID.Value);
            //if (dtstock.Rows.Count == Convert.ToInt32(hdnOrderQuantity.Value))
            //{
            //    PanelRepeater.Enabled = false;
            //    AddButtonDisable = AddButtonDisable + 1;

            //    // ddlStockCategoryID.Enabled = false;
            //}
            //else
            //{
            //    PanelRepeater.Enabled = true;
            //    //AddButtonDisable = 0;

            //    //   ddlStockCategoryID.Enabled = true;
            //}
            if (dtstock.Rows.Count > 0)
            {
                litremove.Visible = false;
                btnRevertScannedItem.Visible = true;
            }
            else
            {
                if (e.Item.ItemIndex == 0)
                {
                    //btnDelete.Visible = false;
                    chkdelete.Visible = false;
                    litremove.Visible = false;
                    //RequiredFieldValidator11.Visible = true;
                    // RequiredFieldValidator1.Visible = true;
                    //   RequiredFieldValidator19.Visible = true;
                }
                else
                {
                    //btnDelete.Visible = true;
                    chkdelete.Visible = true;
                    litremove.Visible = true;

                    //RequiredFieldValidator11.Visible = false;
                    //RequiredFieldValidator1.Visible = false;
                    //  RequiredFieldValidator19.Visible = false;
                }
                btnRevertScannedItem.Visible = false;
            }
        }
        catch { }
    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("AvailableQty", Type.GetType("System.String"));
        rpttable.Columns.Add("WholesaleOrderItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["OrderQuantity"] = txtOrderQuantity.Text;
            dr["AvailableQty"] = txtAvailableQuantity.Text;
            dr["WholesaleOrderItemID"] = hdnWholesaleOrderItemID.Value;
            dr["type"] = hdntype.Value;

            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
            rpttable.Columns.Add("AvailableQty", Type.GetType("System.String"));
            rpttable.Columns.Add("WholesaleOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["OrderQuantity"] = "";
            dr["AvailableQty"] = "";
            dr["WholesaleOrderItemID"] = "";
            dr["type"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            HiddenField hdnStockCategory = (HiddenField)item1.FindControl("hdnStockCategory");
            HiddenField hdnStockItem = (HiddenField)item1.FindControl("hdnStockItem");
            Button btnRevertScannedItem = (Button)item1.FindControl("btnRevertScannedItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            try
            {
                DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
                if (dtstock.Rows.Count > 0)
                {
                    lbremove1.Visible = false;
                    btnRevertScannedItem.Visible = true;
                }
                else
                {
                    lbremove1.Visible = true;
                    btnRevertScannedItem.Visible = false;
                }
            }
            catch
            {
                lbremove1.Visible = true;
            }
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    protected void ibtnaddvendor_click(object sender, EventArgs e)
    {

        if (txtCompany.Text != string.Empty)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string employeeid = st.EmployeeID;

            int success = ClstblCustomers.tblCustomers_Insert("", employeeid, "false", "6", "3", "", "", "", "1", employeeid, "", "", "", "", "", txtCompany.Text, "", "", "", "", "", "", "", "", "", "", "australia", "", "", "", "", "", "", "", "", "", "", "false", "", "", "", "", "", "", "", "");
            //Here 6 is custtyype id for wholesale customers binded it will be 6 but for vendors in stock order it is 13 
            //ddlvendor searches for 13 for stockorder but for wholesale it will be 6.
            int succontacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", txtContFirst.Text.Trim(), txtContLast.Text.Trim(), "", "", employeeid, employeeid);
            int succustinfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "customer entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(succontacts), employeeid, "1");

            if (Convert.ToString(success) != string.Empty)
            {
                ModalPopupExtenderVendor.Hide();
                BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(success);
                Notification("There are no items to show in this view");
            }
            else
            {
                Notification("Transaction Failed.");
            }
        }
    }

    protected void btnNewStock_Click(object sender, EventArgs e)
    {
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        chkDashboard.Checked = false;

        PanAddUpdate.Visible = true;
        ModalPopupExtenderStock.Show();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlstockcategory.Items.Clear();
        ddlstockcategory.Items.Add(item8);

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dtStockCategory;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindScript();
    }

    protected void ibtnAddStock_Click(object sender, EventArgs e)
    {
        InitAdd();
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();

        int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
        //ClstblStockItems.wholesaleitemitemNpanels(stockcategory);

        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hyplocationid = (HiddenField)item.FindControl("hyplocationid");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            if (txtqty.Text != "" || hyplocationid.Value != "")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
            }
        }
        if (success > 0)
        {
            ModalPopupExtenderStock.Hide();
            Notification("There are no items to show in this view");
        }
        else
        {
            ModalPopupExtenderStock.Show();
            Notification("Transaction Failed.");
        }
        BindScript();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        //  ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btndeliver_Click(object sender, EventArgs e)
    {

        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        if (receive != string.Empty)
        {
            Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_ActualDelivery(hndid.Value, receive);
        }

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        if (success)
        {
            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hndid.Value);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                    ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                }
            }
            //SetDelete();

            int success1 = 0;
            //Response.Expires = 400;
            //Server.ScriptTimeout = 1200;

            if (FileUpload1.HasFile)
            {
                Notification("There are no items to show in this view");
                FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
                string connectionString = "";

                //if (FileUpload1.FileName.EndsWith(".csv"))
                //{
                //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
                //}
                // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

                //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


                if (FileUpload1.FileName.EndsWith(".xls"))
                {
                    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
                }
                else if (FileUpload1.FileName.EndsWith(".xlsx"))
                {
                    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

                }


                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    using (DbCommand command = connection.CreateCommand())
                    {
                        // Cities$ comes from the name of the worksheet

                        command.CommandText = "SELECT * FROM [Sheet1$]";
                        command.CommandType = CommandType.Text;

                        // string employeeid = string.Empty;
                        //string flag = "true";
                        // SttblStockOrders st = Clstbl_WholesaleOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
                        DataTable dt2 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hndid.Value);
                        DataRow dr1 = dt2.Rows[0];
                        string stockid = dr1["StockItemID"].ToString();
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                Notification("There are no items to show in this view");
                                //if (flag == "false")
                                //{
                                //    PanEmpty.Visible = true;
                                //}
                                //else
                                //{
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";

                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();
                                    if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                    {
                                        //try
                                        //{

                                        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                        // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                        //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                        //Response.End();
                                        success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);
                                        //}
                                        //catch { }
                                    }
                                    if (success1 > 0)
                                    {
                                        Notification("There are no items to show in this view");
                                    }
                                    else
                                    {
                                        Notification("Transaction Failed.");
                                    }
                                    //}
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            SetError();
        }
        txtdatereceived.Text = string.Empty;

        // txtserialno.Text = string.Empty;


        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnNewVendor_Click1(object sender, EventArgs e)
    {

        ModalPopupExtenderVendor.Show();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        txtCompany.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        BindScript();
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchEmployee.SelectedValue = "";
        ddlShow.SelectedValue = "";
        ddlDue.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        txtreferenceno.Text = string.Empty;
        ddlDeliveredOrNot.SelectedValue = "False";
        txtpvdnumber.Text = string.Empty;
        //ddlpvdstatus.SelectedValue = "";
        lstPVDStatus.ClearSelection();
        ddlTransporttype.SelectedValue = "";
        ddlinstallertype.SelectedValue = "";
        ddlinstallername.SelectedValue = "";
        //ddljobstatus.SelectedValue = "";
        lstJobStatus.ClearSelection();
        ddlSolarType.SelectedValue = "";
        ddlSearchjobtype.SelectedValue = "";
        ddlSearchdeliveryoption.SelectedValue = "";
        ddlApproved.SelectedValue = "";
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindLocation();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["State"].ToString();
            ddlSearchState.SelectedValue = CompanyLocationID;
            ddlSearchState.Enabled = false;
        }

        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void btnDelivered_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_Cancelled(id, Convert.ToString(true));
        Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_DeleteWholesaleOrderID(id);
        Clstbl_WholesaleOrders.tbl_WholesaleOrders_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            // SetDelete();
            Notification("Transaction Successful.");
            //PanSuccess.Visible = true;
        }
        else
        {
            Notification("Transaction Failed.");
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        //BindGrid(1);
        PanAddUpdate.Visible = false;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton btnVerify = (LinkButton)e.Row.FindControl("gvbtnVerify");
            LinkButton btnVerify1 = (LinkButton)e.Row.FindControl("gvbtnVerify1");
            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("LnkUpdate1");
            HiddenField gvOrderId = (HiddenField)e.Row.FindControl("hdnStockOrderID");
            HiddenField hndDeduct1 = (HiddenField)e.Row.FindControl("hndDeduct");

            Label LblNotes = (Label)e.Row.FindControl("lblNotes");
            LinkButton LinkButton9 = (LinkButton)e.Row.FindControl("LinkButton9");
            LinkButton gvbtnNote = (LinkButton)e.Row.FindControl("gvbtnNote");
            if (gvOrderId.Value != null && gvOrderId.Value != "")
            {
                DataTable dtNotes = Clstbl_WholesaleOrders.tbl_WholesaleOrdersNotes_SelectByWholesaleOrderId(gvOrderId.Value);
                if (dtNotes.Rows.Count > 0)
                {
                    LblNotes.Text = dtNotes.Rows[0]["Note"].ToString();
                    gvbtnNote.Visible = true;
                }
                else
                {
                    LinkButton9.Visible = true;
                }
            }

            if ((Roles.IsUserInRole("Accountant")))
            {
                btnVerify.Enabled = false;
                btnVerify1.Enabled = false;
                gvbtnUpdate.Enabled = false;
                anpanel.Visible = false;
            }

        }
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;


        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            HiddenField hdnStockCategory = (HiddenField)item1.FindControl("hdnStockCategory");
            HiddenField hdnStockItem = (HiddenField)item1.FindControl("hdnStockItem");
            Button btnRevertScannedItem = (Button)item1.FindControl("btnRevertScannedItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            try
            {
                DataTable dtstock = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderID(hdnWholesaleOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
                if (dtstock.Rows.Count > 0)
                {
                    lbremove1.Visible = false;
                    btnRevertScannedItem.Visible = true;
                }
                else
                {
                    lbremove1.Visible = true;
                    btnRevertScannedItem.Visible = false;
                }
            }
            catch
            {
                lbremove1.Visible = true;
            }
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void txtVendorInvoiceNo_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtVendorInvoiceNo.Text))
        {
            int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByInvoiceNo(txtVendorInvoiceNo.Text);
            if (exist == 1)
            {
                Notification("Field with this value already exists in the records.");
                txtVendorInvoiceNo.Text = string.Empty;
            }
        }
    }

    protected void ReferenceNo_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtManualOrderNumber.Text))
        {
            int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistsByReferenceNo(txtManualOrderNumber.Text, txtVendorInvoiceNo.Text);
            if (exist == 1)
            {
                Notification("Field with this value already exists in the records.");
                txtManualOrderNumber.Text = string.Empty;
            }
        }
    }

    protected void gvbtnPrint_Click(object sender, EventArgs e)
    {
        Telerik_reports.generate_WholesalePicklist(hndWholesaleOrderID.Value);
    }

    protected void btnOK3btnOK3_Click(object sender, EventArgs e)
    {
        string ID = hndid2.Value;
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsDeduct_Date_User(ID, "0", "", "");
        bool success2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustEmailFlag(ID, "false");

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(ID);

        DataTable dtQty = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectQty(ID);
        if (success)
        {
            DataTable dt1 = Clstbl_WholesaleOrders.tblStockSerialNo_Select_ByWholesaleOrderID(ID);
            if (dt1.Rows.Count > 0)
            {
                string SerialNo = "";
                foreach (DataRow dtserial in dt1.Rows)
                {
                    SerialNo = dtserial["SerialNo"].ToString();

                    string Section = "Wholesale IN";
                    string Message = "Wholesale revert for Order Number:" + ID;

                    ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, ID, SerialNo, Section, Message, Currendate);
                    ClstblStockSerialNo.tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo(SerialNo, "0", "0");
                }
            }

            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(ID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", row["StockItemID"].ToString(), st.CompanyLocationID, stOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, row["WholesaleOrderID"].ToString(), row["WholesaleOrderItemID"].ToString(), st.InvoiceNo);
                    Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    ClsProjectSale.tblStockItems_RevertStock(row["StockItemID"].ToString(), row["OrderQuantity"].ToString(), st.CompanyLocationID.ToString());
                }
                //Response.End();
            }
            //SetDelete();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
    }

    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddljobtype.SelectedValue == "2")//stc
        {
            RequiredFieldValidator12.Enabled = true;
        }
        else
        {
            RequiredFieldValidator12.Enabled = false;
        }
    }

    protected void btnRevertScannedItem_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        HiddenField hdnStockItem = (HiddenField)item.FindControl("hdnStockItem");
        ModalPopupExtenderRevertScannedItem.Show();
        hndStockItemID3.Value = hdnStockItem.Value;
    }

    protected void btnOK4_Click(object sender, EventArgs e)
    {
        DataTable dt1 = ClstblStockSerialNo.tblStockSerialNoCount_ByWholesaleOrderIDStockItemID(hdnWholesaleOrderID2.Value, hndStockItemID3.Value);
        if (dt1.Rows.Count > 0)
        {
            string SerialNo = "";
            foreach (DataRow dtserial in dt1.Rows)
            {
                SerialNo = dtserial["SerialNo"].ToString();
                ClstblStockSerialNo.tblStockSerialNo_updateIsActiveWholesaleOrderID_ByStockSerialNo(SerialNo, "0", "0");
            }
        }

        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(hdnWholesaleOrderID2.Value);

        // Notification("Item Reverted Successfully.");//write notification above binding repeater again
        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;

        }
    }

    protected void btnUpdatePVD_Click(object sender, EventArgs e)
    {
        bool sucess = Clstbl_WholesaleOrders.tbl_WholesaleOrders_UpdatePVDStatus(hndWholesaleOrderID.Value, txtpvd.Text, ddlstatuspvd.SelectedValue, ddlstatuspvd.SelectedItem.Text);

        bool UpdateJobStatus = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_JobStatus(hndWholesaleOrderID.Value, ddlJobStatusUpdate.SelectedValue);

        BindGrid(0);
        Notification("Transaction Successful.");
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void btnaddmodule_Click(object sender, EventArgs e)
    {
        //int success = 0;
        bool success2 = false;

        if (ModuleFileUpload.HasFile)
        {
            ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\StockDeductPanel\\" + ModuleFileUpload.FileName);
            string connectionString = "";

            if (ModuleFileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockDeductPanel\\" + ModuleFileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (ModuleFileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockDeductPanel\\" + ModuleFileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string StcID = "";
                                string PVDNumber = "";
                                string PVDStaus = "";
                                string Applied_Date = "";
                                string Stc_Value = "";
                                //string Update_Date = "";
                                string StatusId = "";
                                string NumberofPanels = "";
                                StcID = dr["StcID"].ToString();
                                PVDNumber = dr["PVDNumber"].ToString();
                                PVDStaus = dr["PVDStaus"].ToString();
                                Applied_Date = dr["Applied_Date"].ToString();
                                Stc_Value = dr["Stc_Value"].ToString();
                                NumberofPanels = dr["NumberofPanels"].ToString();
                                //Update_Date = dr["Update_Date"].ToString();
                                //if (PVDStaus != null && PVDStaus != "")
                                //{
                                //    if (PVDStaus.Equals("Pending"))
                                //    {
                                //        StatusId = "1";
                                //    }
                                //    if (PVDStaus.Equals("JobBooked"))
                                //    {
                                //        StatusId = "2";
                                //    }
                                //    if (PVDStaus.Equals("Void"))
                                //    {
                                //        StatusId = "3";
                                //    }
                                //    if (PVDStaus.Equals("PVD Applied"))
                                //    {
                                //        StatusId = "4";
                                //    }
                                //    if (PVDStaus.Equals("Approved"))
                                //    {
                                //        StatusId = "5";
                                //    }
                                //    if (PVDStaus.Equals("Failed"))
                                //    {
                                //        StatusId = "6";
                                //    }
                                //    if (PVDStaus.Equals("Blank"))
                                //    {
                                //        StatusId = "7";
                                //    }
                                //    if (PVDStaus.Equals("Traded"))
                                //    {
                                //        StatusId = "8";
                                //    }
                                //    if (PVDStaus.Equals("Complaince"))
                                //    {
                                //        StatusId = "9";
                                //    }
                                //}
                                if (StcID != null && StcID != "")
                                {
                                    int exist = Clstbl_WholesaleOrders.tbl_WholesaleOrders_ExistStcId(StcID);

                                    if (exist == 1)
                                    {
                                        success2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_GetStcDetails(StcID, PVDNumber, StatusId, Applied_Date, Stc_Value, PVDStaus, NumberofPanels);
                                    }
                                }
                            }
                        }
                    }

                    if (success2)
                    {
                        Notification("Transaction Successful.");
                    }
                    try
                    {
                        SiteConfiguration.deleteimage(ModuleFileUpload.FileName, "PVDStatus");
                    }
                    catch { }
                }
            }
        }
        BindGrid(0);
        // }
        //bool success = Clstbl_WholesaleOrders.tbl_WholesaleOrders_GetStcDetails(ReferenceNo, PVDNumber, Status, Applied_date, Stc_Value_Excel);
        //BindGrid(0);
    }

    protected void lnkapproved_Click(object sender, EventArgs e)
    {
        string OrderId = hndWholesaleOrderID.Value;
        if (OrderId != null && OrderId != "")
        {
            bool s1 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_UpdateIsaaproved(OrderId);
            if (s1)
            {
                //MsgError("Approved");
                //Notification("Approved..");
            }
            else
            {
                // MsgError("Error");
                //Notification("Error..");
            }
        }
        BindGrid(0);
    }

    protected void btnSaveNotes_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderNote.Show();

        string WholesaleOrderID = hndWholesaleOrderID.Value;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (WholesaleOrderID != "")
        {
            int success = Clstbl_WholesaleOrders.tbl_WholesaleOrdersNotes_Insert(DateTime.Now.ToString(), txtWholesaleNotes.Text, WholesaleOrderID);
            if (success > 0)
            {
                bool s1 = Clstbl_WholesaleOrders.tbl_WholesaleOrdersNotes_Update_userid(Convert.ToString(success), userid);
            }
        }

        txtWholesaleNotes.Text = String.Empty;

        BindGridNotes(WholesaleOrderID);
        BindGrid(0);
        ModalPopupExtenderNote.Hide();
    }

    public void BindGridNotes(string WholesaleOrderID)
    {
        DataTable dt = new DataTable();
        dt = Clstbl_WholesaleOrders.tbl_WholesaleOrdersNotes_SelectByWholesaleOrderId(WholesaleOrderID);
        //dv = new DataView(dt);

        if (dt.Rows.Count != 0)
        {
            GridViewNote.DataSource = dt;
            GridViewNote.DataBind();
            PanGridNotes.Visible = true;
        }
        else
        {
            PanGridNotes.Visible = false;
        }
    }

    protected void GridViewNote_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);
        //--- do not chage this code end------
    }

    protected void GridViewNote_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.ToString() == "Delete")
        {
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();

            if (ID != "")
            {
                bool succ = Clstbl_WholesaleOrders.tbl_WholesaleOrdersNotes_DeleteByid(ID);
            }

            BindGridNotes(hndWholesaleOrderID.Value);
            ModalPopupExtenderNote.Show();
            BindGrid(0);
        }
    }

    protected void GridViewNote_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblname = (Label)e.Row.FindControl("lblusernm");
            HiddenField hndunm = (HiddenField)e.Row.FindControl("hnduserid");
            if (hndunm.Value != null && hndunm.Value != "")
            {
                SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(hndunm.Value);
                lblname.Text = stEmployeeid.EmpFirst;
            }
        }
    }

    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "WholesaleorderTracker_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 26, 3, 4, 5, 6, 39,
                              7, 8, 9, 10, 11,
                              13, 3, 14, 16, 28, 17,
                              0, 18, 19, 20, 21,
                              22, 23, 40, 24, 3, 27, 25, 15, 37, 41, 42 };

            string[] arrHeader = { "Invoice No", "Invoice Date","Stock For","Job Type","STC ID","STC", "STC Price", "STC Amount",
                                   "Customer", "Qty", "D. Qty", "Received Date", "PVD Number",
                                   "Pvd Status", "Job Status", "STC REC", "STC Panels", "Employee Name", "Notes",
                                   "Id", "TransportType", "InstallType", "Consign/Person", "InstallerName",
                                   "Install Date", "Amount", "Net Amount", "SolarType", "Job Status", "Delivery Option", "Items Ordered", "STC Applied", "GB Status", "PO", "PaymentStatus" };

            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            //}
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnGBBS_Click(object sender, EventArgs e)
    {
        DataTable dtReferenceNo = ClsReportsV2.SP_GetGreenBotProjectNo_ByCompanyID("3");

        if (ddlGBBS.SelectedValue == "1")
        {
            DataTable dt = ClsDbData.tbl_APIFetchData_UtilitiesBySource("GreenBot");
            DataTable dtSTCDetails = new DataTable();
            dtSTCDetails.Columns.Add("JobID", typeof(int));
            dtSTCDetails.Columns.Add("PVDNo", typeof(string));
            dtSTCDetails.Columns.Add("CalculatedSTC", typeof(string));
            dtSTCDetails.Columns.Add("STCStatus", typeof(string));
            dtSTCDetails.Columns.Add("STCSubmissionDate", typeof(string));
            dtSTCDetails.Columns.Add("NoofPanel", typeof(string));
            dtSTCDetails.Columns.Add("CompanyID", typeof(int));
            dtSTCDetails.Columns.Add("ProjectNo", typeof(string));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string CompanyID = dt.Rows[i]["CompanyID"].ToString();
                string FromDate = dt.Rows[i]["FromDate"].ToString();
                string ToDate = dt.Rows[i]["ToDate"].ToString();
                string username = dt.Rows[i]["username"].ToString();
                string password = dt.Rows[i]["password"].ToString();

                //string CompanyID = "3";
                //string FromDate = "29/03/2021";
                //string ToDate = "30/03/2021";
                //string username = "achieversenergy";
                //string password = "achievers1";

                dtSTCDetails = GetAllGreenbotDetails(FromDate, ToDate, username, password, dtSTCDetails, CompanyID);
            }

            if (dtSTCDetails.Rows.Count > 0)
            {
                int UpdateData = ClsDbData.Bulk_InsertUpdate_tblSTCDetails(dtSTCDetails);
                int Suc = ClsDbData.tbl_WholesaleOrders_Merge_STCStatus();

                string msg = "Total Record is " + dtSTCDetails.Rows.Count + " and Updated Record is " + UpdateData;
                Notification(msg);

                string msg1 = Suc + " Status are Updated..!";
                Notification(msg1);
            }
            
        }
        else if (ddlGBBS.SelectedValue == "2")
        {
            int UpdateCount = UpdateBridgeSelect(dtReferenceNo);
            Notification(UpdateCount.ToString() + " Record Updated..");
        }
        else
        {
            Notification("Please Select GreenBot/BridgeSelect");
        }
    }

    #region Green Bot
    public DataTable GetAllGreenbotDetails(string FromDate, string ToDate, string Username, string Password, DataTable dtSTCDetails, string CompanyID)
    {
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + FromDate + "&ToDate=" + ToDate);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    for (int i = 0; i < jobData.lstJobData.Count; i++)
                    {
                        Job_Response.lstJobData lstJobData = jobData.lstJobData[i]; // Root Object
                        //Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details
                        //Job_Response.InstallerView InstallerView = lstJobData.InstallerView; // get Child object Details
                        
                        // Now Data Table For Wholesale
                        Job_Response.JobSTCDetails JobSTCDetails = lstJobData.JobSTCDetails;
                        Job_Response.JobSTCStatusData JobSTCStatusData = lstJobData.JobSTCStatusData;
                        Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails;

                        string ProjectNo = lstJobData.BasicDetails.RefNumber;
                        string JobID = lstJobData.BasicDetails.JobID;
                        
                        string FailedAccreditationCode = JobSTCDetails.FailedAccreditationCode;
                        string CalculatedSTC = JobSTCStatusData.CalculatedSTC;
                        string STCStatus = JobSTCStatusData.STCStatus;
                        string STCSubmissionDate = JobSTCStatusData.STCSubmissionDate;
                        string NoofPanel = JobSystemDetails.NoOfPanel;

                        if (JobID == "348385")
                        {
                            string ss = "text";
                        }

                        DataRow dr = dtSTCDetails.NewRow();
                        dr["JobID"] = JobID;
                        dr["PVDNo"] = FailedAccreditationCode;
                        dr["CalculatedSTC"] = CalculatedSTC;
                        dr["STCStatus"] = STCStatus;
                        dr["STCSubmissionDate"] = STCSubmissionDate;
                        dr["NoofPanel"] = NoofPanel;
                        dr["CompanyID"] = Convert.ToInt32(CompanyID);
                        dr["ProjectNo"] = ProjectNo;
                        dtSTCDetails.Rows.Add(dr);
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return dtSTCDetails;
    }

    public int GetNoOfPanels(string JobNo, string Username, string Password)
    {
        int NoOfPanel = 0;

        string SerialNo = GetJobDetails(JobNo, Username, Password);
        string[] SerialNoList = System.Text.RegularExpressions.Regex.Split(SerialNo, "\r\n");

        for (int i = 0; i < SerialNoList.Length; i++)
        {
            if (SerialNoList[i] != "")
            {
                NoOfPanel++;
            }
        }
        //NoOfPanel = SerialNoList.Length;
        return NoOfPanel;
    }

    public string GetJobDetails(string JobNumber, string Username, string Password)
    {
        string SerialNo = "";
        try
        {
            var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
            var request = new RestRequest(Method.POST);
            //request.AddParameter("Username", "arisesolar");
            //request.AddParameter("Password", "arisesolar1");
            request.AddParameter("Username", Username);
            request.AddParameter("Password", Password);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?JobId=" + JobNumber);
                request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                client.AddDefaultHeader("Content-Type", "application/json");
                client.AddDefaultHeader("Accept", "application/json");
                IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Job_Response.RootObject jobData = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

                    if(jobData.Status.ToString() != "Jobs not found")
                    {
                        int index = jobData.lstJobData.Count;
                        //Job_Response.lstJobData lstJobData = jobData.lstJobData[index - 1]; // Root Object
                        Job_Response.lstJobData lstJobData = jobData.lstJobData[0]; // Root Object
                        Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

                        SerialNo = JobSystemDetails.SerialNumbers;
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return SerialNo;
    }
    #endregion Green Bot

    #region Bridge Select
    public int UpdateBridgeSelect(DataTable dt)
    {
        int Suc = 0;

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string ReferenceNo = dt.Rows[i]["ReferenceNo"].ToString();
                string WholesaleOrderID = dt.Rows[i]["WholesaleOrderID"].ToString();

                if (!string.IsNullOrEmpty(ReferenceNo))
                {
                    Job_Response.NoOfPanelInverter noOfPanelInverter = GetNoOfPanels_BS(ReferenceNo);
                    string NosP = noOfPanelInverter.NoOfPanel;
                    string NosI = noOfPanelInverter.NoOfInverter;

                    if (NosP == "0")
                    {
                        NosP = "0";
                    }
                    if (NosI == "0")
                    {
                        NosI = "0";
                    }

                    bool success = ClsReportsV2.tbl_WholesaleOrders_UpdateNumberofPanels(WholesaleOrderID, NosP);
                    if (success)
                    {
                        Suc++;
                    }
                }
            }


        }
        return Suc;
    }

    public Job_Response.NoOfPanelInverter GetNoOfPanels_BS(string ProjectNo)
    {
        Job_Response.NoOfPanelInverter noOfPanelInverter = new Job_Response.NoOfPanelInverter();
        try
        {
            Job_Response.Details jobDetails = GetJobDetailsFromBS(ProjectNo);
            int NoOfPanel = jobDetails.Panels != null ? jobDetails.Panels.Count : 0;
            int NoOfInverter = jobDetails.Inverters != null ? jobDetails.Inverters.Count : 0;

            noOfPanelInverter.NoOfPanel = NoOfPanel.ToString();
            noOfPanelInverter.NoOfInverter = NoOfInverter.ToString();
        }
        catch(Exception ex)
        {

        }
        return noOfPanelInverter;
    }

    public Job_Response.Details GetJobDetailsFromBS(string JobNumber)
    {
        Job_Response.Details ReturnDetails = new Job_Response.Details();
        try
        {
            var obj = new ClsBridgeSelect
            {
                //crmid = "872136"
                crmid = JobNumber
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/products";

            //string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/status";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            var client = new RestSharp.RestClient(URL);
            var request = new RestRequest(Method.POST);
            client.AddDefaultHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", DATA, ParameterType.RequestBody);

            IRestResponse<Job_Response.RootObjectBS> JsonData = client.Execute<Job_Response.RootObjectBS>(request);
            if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Job_Response.RootObjectBS jobData = JsonConvert.DeserializeObject<Job_Response.RootObjectBS>(JsonData.Content);

                Job_Response.Success Success = jobData.Success; // Root Object
                Job_Response.Details Details = Success.Details; // get Child object Details

                ReturnDetails = Details;
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
        }

        return ReturnDetails;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }
    #endregion

    protected void lnkUpdateSTC_Click(object sender, EventArgs e)
    {
        bool Suc = ClsDbData.tbl_WholesaleOrders_Update_STC();

        if(Suc)
        {
            Notification("Updated...");
        }
        else
        {
            Notification("Error...");
        }
    }

    protected void btnUpdateSTC_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hndWholesaleOrderID.Value;

        if(!string.IsNullOrEmpty(WholesaleOrderID))
        {
            bool UpdateSTC = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_StcValue(WholesaleOrderID, txtSTCValue.Text);

            if (UpdateSTC)
            {
                Notification("Updated...");
                BindGrid(0);
            }
            else
            {
                Notification("Error...");
                ModalPopupExtender3.Show();
            }
        }
    }

    protected void gvlbtnPayment_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = sender as LinkButton;
        string invoiceNo = lbtn.CommandArgument;
        hndInvoiceNo.Value = invoiceNo;

        //BindSTCRemaining(invoiceNo);
        hndMode.Value = "";
        ddlPayment.ClearSelection();
        txtTransferInvoiceNo.Text = string.Empty;
        txtAmount.Text = string.Empty;
        BindPayment(invoiceNo);
        mpPayment.Show();
    }

    public void BindPayment(string invoiceNo)
    {
        BindSTCRemaining(invoiceNo);
        string userId = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = Clstbl_WholesaleOrders.tbl_POInvoicePayment_GetDataByInvoiceNo(invoiceNo, userId);
        
        if (dt.Rows.Count > 0)
        {
            DivPayment.Visible = true;
            RptPayment.DataSource = dt;
            RptPayment.DataBind();
        }
        else
        {
            DivPayment.Visible = false;
        }

        if (dt.Rows.Count > 3)
        {
            DivPayment.Attributes.Add("style", "overflow: scroll; height: 200px;");
        }
        else
        {
            //DivNotes.Attributes.Add("style", "");
            DivPayment.Attributes.Add("style", "overflow-x: scroll");
        }
    }

    protected void lbtnSavePayment_Click(object sender, EventArgs e)
    {
        string invoiceNo = hndInvoiceNo.Value;
        string paymentTypeId = ddlPayment.SelectedValue;
        string paymentType = ddlPayment.SelectedItem.Text;
        string transferInvoiceNo = ddlPayment.SelectedValue == "3" ? txtTransferInvoiceNo.Text : "";
        string amount = txtAmount.Text;
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();

        int id = 0;
        if (hndMode.Value == "Update")
        {
            id = !string.IsNullOrEmpty(hndPaymentId.Value) ? Convert.ToInt32(hndPaymentId.Value) : 0;
        }
        else
        {
            id = Clstbl_WholesaleOrders.tbl_POInvoicePayment_Insert(createdBy, createdOn);
        }
        if (id > 0)
        {
            bool update = Clstbl_WholesaleOrders.tbl_POInvoicePayment_Update(id.ToString(), invoiceNo, paymentTypeId, paymentType, transferInvoiceNo, amount);

            SetAdd1();
            HidePanels();
            //Reset();
            //BindGrid(0);

            ddlPayment.ClearSelection();
            txtTransferInvoiceNo.Text = string.Empty;
            txtAmount.Text = string.Empty;
            BindPayment(invoiceNo);
            mpPayment.Show();
        }
    }

    protected void RptPayment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "EditPayment")
        {
            try
            {
                DataTable dt = Clstbl_WholesaleOrders.tbl_POInvoicePayment_SelectByID(ID);

                if (dt.Rows.Count > 0)
                {
                    hndMode.Value = "Update";
                    ddlPayment.ClearSelection();
                    txtTransferInvoiceNo.Text = string.Empty;
                    txtAmount.Text = string.Empty;

                    hndPaymentId.Value = ID;
                    txtTransferInvoiceNo.Text = dt.Rows[0]["TransferInvoiceNo"].ToString();
                    txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                    try
                    {
                        ddlPayment.SelectedValue = dt.Rows[0]["PaymentTypeId"].ToString();

                        //if(dt.Rows[0]["PaymentTypeId"].ToString() == "3")
                        //{

                        //}
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "transfer", "transfer('" + dt.Rows[0]["PaymentTypeId"].ToString() + "');", true);
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                SetError();
            }
        }

        if (e.CommandName == "DeletePayment")
        {
            try
            {
                bool del = Clstbl_WholesaleOrders.tbl_POInvoicePayment_DeleteByID(ID);

                if (del)
                {
                    SetAdd1();
                }
                else
                {
                    SetError();
                }
                string userID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                BindPayment(hndInvoiceNo.Value);
                mpPayment.Show();
            }
            catch (Exception ex)
            {
                SetError();
            }
        }
        mpPayment.Show();
    }

    public void BindSTCRemaining(string invoiceNo)
    {
        string stcRemianing = Clstbl_WholesaleOrders.GetSTCRemaining(invoiceNo);
        lblSTCRemaining.Text = stcRemianing;
    }

    protected void lnkInvoicePdf_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = sender as LinkButton;
        string wholesaleOrderId = lbtn.CommandArgument;

        Telerik_reports.Generate_STCInvoice(wholesaleOrderId);

    }

    protected void lnkPaymentStatus_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = sender as LinkButton;
        string[] arg = lbtn.CommandArgument.Split(',');

        hndWholesaleId.Value = arg[0];
        ddlPaymentStatus.ClearSelection();
        ddlPaymentStatus.SelectedValue = arg[1];

        mpPaymentStatus.Show();
    }

    protected void btnSavePaymentStatus_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees sttblEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        string WholesaleId = hndWholesaleId.Value;
        string PaymentStatus = ddlPaymentStatus.SelectedValue;
        string CreatedBy = sttblEmp.EmployeeID;
        string CreatedOn = DateTime.Now.AddHours(14).ToString();

        bool Update = Clstbl_WholesaleOrders.tbl_POInvoicePayment_Update_PaymentStatus(WholesaleId, PaymentStatus, CreatedBy, CreatedOn);

        if(Update)
        {
            SetAdd1();
            ddlPaymentStatus.ClearSelection();
            mpPaymentStatus.Hide();
        }
    }
}