﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServiceOrder.aspx.cs" Inherits="admin_adminfiles_stock_ServiceOrder" 
    MasterPageFile="~/admin/templates/MasterPageAdmin.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style type="text/css">
                .table {
                    margin-bottom: 0;
                }

                /*.bcolor {
                     border-color: red;
                 }*/
            </style>
            <script type="text/javascript">
                function isNumberKey(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }
            </script>
            <script>

                //function DoFocus(fld) {
                //    fld.className = 'form-control modaltextbox bcolor';
                //}

                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });

                function toaster(msg = "Record Exist.") {
                    //alert("54345");
                    notifymsg(msg, 'inverse');
                }

                function notifymsg(message, type) {
                    $.growl({
                        message: message
                    }, {
                            type: type,
                            allow_dismiss: true,
                            label: 'Cancel',
                            className: 'btn-xs btn-inverse',
                            placement: {
                                from: 'top',
                                align: 'right'
                            },
                            delay: 30000,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            },
                            offset: {
                                x: 30,
                                y: 30
                            }
                        });
                }

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/

                    //   $('#datetimepicker145').datetimepicker({
                    // format: 'DD-MM-YYYY LT'
                    //});
                    //  $(".myval").select2({
                    //      //placeholder: "select",
                    //      allowclear: true,
                    //      minimumResultsForSearch: -1
                    //  });
                    $(function () {
                        $('#datetimepicker01').datetimepicker();
                    });
                    $(function () {
                        $('#datetimepicker02').datetimepicker(
                            { format: 'DD-MM-YYYY hh:mm:ss' }
                        );
                    });

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    })

                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    })





                    function toaster(msg) {
                        //alert("54345");
                        notifymsg(msg, 'inverse');
                    }

                    $(document).ready(function () {
                        $('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });

                        $('[data-toggle="tooltip"]').tooltip({
                            trigger: 'hover'
                        });

                        function toaster(msg) {
                            //alert("54345");
                            notifymsg(msg, 'inverse');
                        }
                        //$(".myval").select2({
                        //    //placeholder: "select",
                        //    allowclear: true,
                        //    minimumResultsForSearch: -1
                        //});
                    });

                    callMultiCheckbox1();
                    callMultiCheckbox2();
                    callMultiCheckbox3();

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox2();
                    });
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox3();
                    });

                    $(".selectlocation .dropdown dt a").on('click', function () {
                        $(".selectlocation .dropdown dd ul").slideToggle('fast');
                    });
                    $(".PurchaseCompany .dropdown dt a").on('click', function () {
                        $(".PurchaseCompany .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Vender .dropdown dt a").on('click', function () {
                        $(".Vender .dropdown dd ul").slideToggle('fast');
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }
            </script>
            <script type="text/javascript">
                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddlLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }
                function callMultiCheckbox2() {
                    var title = "";
                    $("#<%=ddCompany.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel1').show();
                        $('.multiSel1').html(html);
                        $(".hida1").hide();
                    }
                    else {
                        $('#spanselect1').show();
                        $('.multiSel1').hide();
                    }

                }
                function callMultiCheckbox3() {
                    var title = "";
                    $("#<%=ddVender.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel2').show();
                        $('.multiSel2').html(html);
                        $(".hida2").hide();
                    }
                    else {
                        $('#spanselect2').show();
                        $('.multiSel2').hide();
                    }

                }
            </script>
            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Service Order
                                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                <%--<a href="#" id="a1" runat="server" class="btn  btn-warning" data-toggle="modal" data-target="#myModalpanel">Min Qty</a> &nbsp;&nbsp; --%>
                            </div>
                        </h5>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    //$(".myval").select2({
                    //    //placeholder: "select",
                    //    allowclear: true,
                    //    minimumResultsForSearch: -1
                    //});


                }
            </script>


            <script>
                function doMyAction() {

                    $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();


                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });



                }

            </script>

            <script>
                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                        function (isConfirm) {
                            if (isConfirm) {
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                eval(defaultAction);

                                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                return true;
                            } else {
                                // swal("Cancelled", "Your imaginary file is safe :)", "error");
                                return false;
                            }
                        });
                }
            </script>


            <div class="finaladdupdate printorder">

                <div id="PanAddUpdate" runat="server" visible="true">

                    <div class="panel-body animate-panel padtopzero stockorder">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Stock Order
                                </h5>
                            </div>
                            <%--  <style>
                                .div_block{ display:block; float:none; margin-top:-10px;}
                                .margin_set1{margin-top:30px;}
                                .set_w99 .select2-container--default .select2-selection--single .select2-selection__rendered{width:99%;}

                            </style>--%>
                            <div class="card-block" style="padding: 12px 25px!important;">
                                <div class="form-horizontal">
                                    <asp:Panel runat="server" ID="panel234">
                                        <div class="form-group row dnone">

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label64" runat="server">
                                                           Enter Order No</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txorderno" runat="server" Placeholder="Order No" CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-2" style="padding-top: 20px;">
                                                <asp:Button CssClass="btn btn-info purple" ID="btnOrderNo" runat="server" OnClick="btnOrderNo_Click"
                                                    Text="Get Data" CausesValidation="false" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label72" runat="server">
                                                           Next Order No</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="TextBox4" runat="server" Placeholder="Next Order No" Enabled="false" CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row">

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblLastName" runat="server">
                                             Vendor</asp:Label>

                                                <div class="input-group selectboxman icon_text">
                                                    <asp:DropDownList ID="ddlVendor" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <span class="input-group-addon" id="basic-addon1">
                                                        <asp:LinkButton ID="btnNewVendor" runat="server" CausesValidation="false" OnClick="btnNewVendor_Click2"
                                                            CssClass="btn btn-into btn-mini">
                                                                    <i class="fa fa-user"></i>
                                                        </asp:LinkButton></span>

                                                    <asp:HiddenField ID="hdnvendor" runat="server" />

                                                </div>
                                                <div>
                                                    <asp:LinkButton ID="lnEditVendor" runat="server" Text="Edit Data" CausesValidation="false" OnClick="lnEditVendor_Click"
                                                        Visible="false"></asp:LinkButton>
                                                </div>
                                                <div class="div_block">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                        ControlToValidate="ddlVendor" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="usericonbox">

                                                    <%-- <asp:ImageButton ID="btnNewVendor" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add Vendor"
                                                    OnClick="btnNewVendor_Click" ImageUrl="~/images/addvendor.png" />--%>

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                    ControlToValidate="ddlVendor" Display="Dynamic" ValidationGroup="addorder"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Lblpurchasecompany" runat="server">
                                             Purchase Company</asp:Label>

                                                <div class="input-group selectboxman icon_text">
                                                    <asp:DropDownList ID="ddlpurchasecompany" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <span class="input-group-addon" id="basic-addon1">
                                                        <asp:LinkButton ID="lnkpurchasecompany" runat="server" CausesValidation="false" OnClick="lnkpurchasecompany_Click"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Purchase Company" CssClass="btn btn-into btn-mini">
                                                                    <i class="fa fa-user"></i>
                                                        </asp:LinkButton></span>


                                                </div>
                                                <div class="div_block">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="dynamic"
                                                        ControlToValidate="ddlpurchasecompany" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="usericonbox">

                                                    <%-- <asp:ImageButton ID="btnNewVendor" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add Vendor"
                                                    OnClick="btnNewVendor_Click" ImageUrl="~/images/addvendor.png" />--%>

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                    ControlToValidate="ddlVendor" Display="Dynamic" ValidationGroup="addorder"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblFirstName" runat="server">
                                                      Manual Order No</asp:Label>
                                                <div>
                                                    <div class="input-group selectboxman icon_text">
                                                        <asp:TextBox ID="txtManualOrderNumber" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        <span class="input-group-addon" id="basic-addon1">
                                                            <asp:LinkButton ID="btnNewStock" runat="server" CausesValidation="false" OnClick="btnNewStock_Click"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Stock" CssClass="btn btn-default btn-mini">
                                                                                <i class="fa fa-plus-square"></i>
                                                            </asp:LinkButton></span>
                                                    </div>
                                                    <div class="div_block">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="dynamic"
                                                            ControlToValidate="txtManualOrderNumber" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label1" runat="server">
                                                           Claim Invoice No</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtVendorInvoiceNo" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtVendorInvoiceNo" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <span>Container No </span>
                                                <div>
                                                    <asp:TextBox ID="txtstockcontainer" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtstockcontainer" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <span>Stock Order Status</span>
                                                <asp:DropDownList ID="ddlOrdStatus" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                    <asp:ListItem Value="">Stock Order Status</asp:ListItem>
                                                    <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">OnHold</asp:ListItem>
                                                    <asp:ListItem Value="2">Void</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-sm-2">
                                                <span>Stock From Outside?</span>
                                                <asp:DropDownList ID="ddlorderstatusnew" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlorderstatusnew_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="0">Local</asp:ListItem>
                                                    <asp:ListItem Value="1">International</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="" CssClass="comperror"
                                                    ControlToValidate="ddlorderstatusnew" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="col-sm-2">
                                                <span>Delivery Type</span>
                                                <asp:DropDownList ID="ddldeleverytype" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Warehouse</asp:ListItem>
                                                    <asp:ListItem Value="2">Direct</asp:ListItem>
                                                    <asp:ListItem Value="3">Third Party</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="" CssClass="comperror"
                                                    ControlToValidate="ddldeleverytype" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label24" runat="server">
                                                 Payment Status
                                                </asp:Label>
                                                <div>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlitemname" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Text="Order Booked" Value="4" Selected="True" />
                                                            <asp:ListItem Text="Dep Paid" Value="1" />
                                                            <asp:ListItem Text="Final Payment" Value="2" />
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlitemname" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label73" runat="server">
                                                 Payment Method
                                                </asp:Label>
                                                <div>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlPMethod" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Text="Cash" Value="1" />
                                                            <asp:ListItem Text="Credit" Value="2" />
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPMethod" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label3" runat="server">
                                                 Stock Location</asp:Label>
                                                <div>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlStockLocation" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockitem" OnSelectedIndexChanged="ddlStockLocation_SelectedIndexChanged1">
                                                            <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlStockLocation" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <span>Freight Charge</span>
                                                <div>
                                                    <asp:TextBox ID="txtcharges" runat="server" MaxLength="200" CssClass="form-control modaltextbox" Text="0.00"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtcharges" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row ">

                                            <div class="col-sm-2 custom_datepicker">
                                                <asp:Label ID="lblTargetDate" runat="server" class="disblock">
                                               Target Arrival Date</asp:Label>
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtTargetDate" runat="server" class="form-control" placeholder="Target Date"
                                                        OnTextChanged="txtTargetDate_TextChanged" AutoPostBack="true">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtTargetDate" Display="Dynamic" ValidateRequestMode="Enabled" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2 custom_datepicker">
                                                <asp:Label ID="Label26" runat="server" class="disblock">
                                               ETD</asp:Label>
                                                <div class="input-group sandbox-container">

                                                    <asp:TextBox ID="txtBOLReceived" runat="server" class="form-control" AutoPostBack="true" OnTextChanged="txtBOLReceived_TextChanged" placeholder="Bol Received Date">
                                                    </asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtBOLReceived" Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-2 custom_datepicker">
                                                <asp:Label ID="Label27" runat="server" class="disblock">
                                              ETA  <%--Expected Delevery--%></asp:Label>
                                                <div class="input-group sandbox-container">

                                                    <asp:TextBox ID="txtExpectedDelivery" runat="server" class="form-control" placeholder="Expected Delivery Date"
                                                        OnTextChanged="txtExpectedDelivery_TextChanged" AutoPostBack="true">
                                                    </asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                                <div class="div_block">
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="dynamic"
                                                        ControlToValidate="txtExpectedDelivery" InitialValue="" ErrorMessage="" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2 custom_datepicker" style="display: none;">
                                                <span>Stock From Outside?</span>
                                                <asp:DropDownList ID="ddlorderstatus" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="0">Local</asp:ListItem>
                                                    <asp:ListItem Value="1">International</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="ddlorderstatus" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="col-sm-2" id="DivPaperWork" runat="server" visible="false">
                                                <asp:Label ID="Label29" runat="server" class="disblock">
                                               Paperwork Arrival </asp:Label>
                                                <div class="bowser_div">
                                                    <input type="text" class="form-control input-lg" id="fpR" placeholder="Paperwork Arrival file">
                                                    <span class="input-group-btn">
                                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                    </span>
                                                </div>
                                                <%--</div>--%>
                                                <asp:HyperLink ID="lblpaperwork" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                <asp:FileUpload ID="ModuleFileUpload" runat="server" class="file" />
                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="ModuleFileUpload"
                                    ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                    Display="Dynamic" ErrorMessage=".xls only" class="error_text"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                    ControlToValidate="ModuleFileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="col-sm-2" id="DivTransCompName" runat="server" visible="false">
                                                <asp:Label ID="Label30" runat="server">
                                                 Tras.Company Name</asp:Label>
                                                <div>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlTransCompName" runat="server" AppendDataBoundItems="true"
                                                            ari-contraols="DataTables_Table_0" CssClass="myvalstockitem">
                                                            <asp:ListItem Value="">Select</asp:ListItem>

                                                        </asp:DropDownList>

                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass=""
                                                 ControlToValidate="ddlStockLocation" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2 custom_datepicker" id="Divtranscompdate" runat="server" visible="false">
                                                <asp:Label ID="Label31" runat="server" class="disblock">
                                               Sent to Transoprt Co.</asp:Label>
                                                <div class="input-group sandbox-container">

                                                    <asp:TextBox ID="txttranscompdate" runat="server" class="form-control" placeholder="Sent to Transoprt Co">
                                                    </asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row marginbtm0" id="divint" runat="server" visible="false" style="margin-top: -7px;">

                                            <div class="col-sm-2 custom_datepicker">
                                                <asp:Label ID="Label32" runat="server" class="disblock">
                                              Telex Release Date</asp:Label>
                                                <div class="input-group sandbox-container">

                                                    <asp:TextBox ID="tcttelaxrelease" runat="server" class="form-control" placeholder=" Telex Release">
                                                    </asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label33" runat="server" class="disblock">
                                              Telex Release  </asp:Label>
                                                <div class="bowser_div">
                                                    <input type="text" class="form-control input-lg" placeholder=" Telex Release">
                                                    <span class="input-group-btn">
                                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                    </span>
                                                </div>
                                                <%--</div>--%>
                                                <asp:HyperLink ID="lbltelexdoc" runat="server" Target="_blank" Visible="false"> </asp:HyperLink>
                                                <asp:FileUpload ID="fptelexupload" runat="server" class="file" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label28" runat="server" class="disblock">
                                           Transport Co.Delivery date <%-- WH.  Confirm Arrival Date--%></asp:Label>
                                                <div class="input-group date" id='datetimepicker02'>

                                                    <asp:TextBox ID="tctconfArrival" runat="server" class="form-control" placeholder="Confirm Arrival Date">
                                                    </asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label34" runat="server">
                                                           Supplier Invoice No</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="tctsupplierinvoiceno" runat="server" Placeholder="Supplier Invoice No" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>


                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label35" runat="server">
                                                           Total PI Amt.</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtpiamt" runat="server" Placeholder="Total PI Amt." MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>


                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label36" runat="server">
                                                          Trans. Co's Job No.</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txttrnasportjobno" runat="server" Placeholder=" Transport Co's Job No." MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>


                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row marginbtm0" id="divint2" runat="server" visible="false" style="margin-top: -7px;">

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblCoTaxInvNo" runat="server" class="disblock">
                                               Trans. Co's Tax Inv No </asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtTaxinvoiceNo" runat="server" Placeholder="  Trans. Co's Tax Inv No" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label37" runat="server" class="disblock">
                                               Trans. Co's Tax Inv  </asp:Label>
                                                <div class="bowser_div">
                                                    <input type="text" class="form-control input-lg" placeholder="Transport Co's Service Inv File.">

                                                    <span class="input-group-btn">
                                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                    </span>
                                                </div>
                                                <%--</div>--%>
                                                <asp:HyperLink ID="lnkfptranporttaxinv" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                <asp:FileUpload ID="fptranporttaxinv" runat="server" class="file" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label75" runat="server" class="disblock">
                                               Trans. Co's Tax Inv Amount </asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtTaxinvoiceAmount" runat="server" Placeholder="Trans. Co's Tax Inv Amt" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label38" runat="server">
                                  Trans. Co's DISB.Service Inv.</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txttransdisbinvoices" runat="server" Placeholder="Transport Co's Service Invoices." CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label39" runat="server" class="disblock">
                                               Trans. Co's DISB.ServiceFiles  </asp:Label>

                                                <div class="bowser_div">
                                                    <input type="text" class="form-control input-lg" placeholder=" Trans. Co's DISB.ServiceFiles.">

                                                    <span class="input-group-btn">
                                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                    </span>
                                                </div>
                                                <%--</div>--%>
                                                <asp:HyperLink ID="lbldisttransinv" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                <asp:FileUpload ID="fptransdisbinvoices" runat="server" class="file" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label76" runat="server">
                                  Trans. Co's DISB.Service Inv Amt.</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txttransdisbinvoicesAmount" runat="server" Placeholder="Transport Co's Service Inv Amt." CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row marginbtm0" id="divIntdata3" visible="false" runat="server">

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label40" runat="server" class="disblock">
                                              Pickup email date from W/H</asp:Label>
                                                <div class='input-group date' id='datetimepicker01'>
                                                    <asp:TextBox ID="tctpckemildatefromwh" runat="server" class="form-control" placeholder="Pickup email date from W/H">
                                                    </asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                                <%--<div class="input-group date sandbox-container" id="demodate">
                                                   
                                                <asp:TextBox ID="tctpckemildatefromwh" runat="server" class="form-control" placeholder="Pickup email date from W/H">
                                                    </asp:TextBox>                                                    
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>--%>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label41" runat="server">
                                 Storage Charge Amount</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtstrchargeamt" runat="server" Placeholder=" Storage Charge Amount." CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label42" runat="server">
                                  Store Charge Inv. No</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtstorechargeinvno" runat="server" Placeholder="Store Charge Inv. No." CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="Label43" runat="server" class="disblock">
                                               Store Charge Inv.  </asp:Label>
                                                <div class="bowser_div">
                                                    <input type="text" class="form-control input-lg" placeholder="Store Charge Inv. files">

                                                    <span class="input-group-btn">
                                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                    </span>
                                                    <asp:HyperLink ID="lblstorechargeinv" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                </div>
                                                <%--</div>--%>
                                                <asp:FileUpload ID="fpstorechargeinvno" runat="server" class="file" />
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label44" runat="server">
                                 Reason For Store Charge</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtstorechargereason" runat="server" Placeholder=" Reason For Store Charge." CssClass="form-control modaltextbox"></asp:TextBox>

                                                </div>
                                            </div>


                                        </div>



                                        <div class="form-group row">
                                            <div class="col-sm-2 custom_datepicker">
                                                <asp:Label ID="Label63" runat="server" class="disblock">
                                                    Payment DueDate
                                                </asp:Label>
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtpmtduedate" runat="server" class="form-control" placeholder="Payment DueDate">
                                                    </asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2 dnone">
                                                <asp:Label ID="Label78" runat="server">
                                                Vendor Discount</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtVendorDiscount" runat="server" Placeholder="Vendor Discount" CssClass="form-control modaltextbox"
                                                        Text="0.00"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label80" runat="server">
                                                Currency</asp:Label>
                                                <div>
                                                    <asp:DropDownList ID="ddlCurrency" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" OnTextChanged="ddlCurrency_TextChanged">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">USD</asp:ListItem>
                                                        <asp:ListItem Value="2">AUD</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label79" runat="server">
                                                GST Type</asp:Label>
                                                <div>
                                                    <asp:DropDownList ID="ddlGSTType" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">With GST</asp:ListItem>
                                                        <asp:ListItem Value="2">Without GST</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label81" runat="server">
                                                Order For</asp:Label>
                                                <div>
                                                    <asp:DropDownList ID="ddlSaveOrderFor" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Enabled="false">
                                                        <asp:ListItem Value="">Order For</asp:ListItem>
                                                        <asp:ListItem Value="1">Retail</asp:ListItem>
                                                        <asp:ListItem Value="2">Wholesale</asp:ListItem>
                                                        <asp:ListItem Value="3" Selected="True">Service</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </asp:Panel>
                                    <%--  <div class="graybgarea">--%>
                                    <div class="row" style="margin-top: -7px;">
                                        <div class="col-sm-11 set_w99">
                                            <asp:HiddenField ID="hdnStockOrderID2" runat="server" />
                                            <asp:HiddenField ID="hdnActualDElivery2" runat="server" />

                                            <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_OnItemDataBound">
                                                <ItemTemplate>
                                                    <asp:Panel ID="PanelRepeater" runat="server">
                                                        <div class="form-group row">
                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Stock </br> Category
                                                                    </label>
                                                                </span>

                                                                <span>
                                                                    <asp:HiddenField ID="hdnStockCategory" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" class="myvalstockitem"
                                                                            AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator146" runat="server" ErrorMessage=""
                                                                            ValidationGroup="Req" ControlToValidate="ddlStockCategoryID" Display="Dynamic" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Stock </br> Item</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" class="myvalstockitem"
                                                                            AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Total </br> Quantity</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hdnOrderQuantity" runat="server" Value='<%#Eval("OrderQuantity") %>' />
                                                                    <asp:TextBox ID="txtOrderQuantity" runat="server" MaxLength="8" CssClass="form-control modaltextbox" OnTextChanged="txtOrderQuantity_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator56" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtOrderQuantity" ValidationGroup="Req" ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="txtOrderQuantity" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                </span>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Unit</br>  Price </label>
                                                                </span>

                                                                <span>
                                                                    <asp:HiddenField ID="hndUnitPrice" runat="server" Value='<%#Eval("UnitPrice") %>' />
                                                                    <asp:TextBox ID="txtUnitPrice" runat="server" MaxLength="8" CssClass="form-control modaltextbox" AutoPostBack="true"
                                                                        OnTextChanged="txtUnitPrice_TextChanged"></asp:TextBox>

                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtUnitPrice" ValidationGroup="Req" ForeColor="#FF5370"></asp:RegularExpressionValidator>--%>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="txtUnitPrice" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                </span>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Amt - Discount </br> Without GST</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hndamount" runat="server" Value='<%#Eval("Amount") %>' />
                                                                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="8" CssClass="form-control modaltextbox" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^\d*\.?\d+$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtAmount" ValidationGroup="Req" ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Amt - Discount </br> With GST</label>
                                                                </span>
                                                                <span>
                                                                    <asp:HiddenField ID="hndAmountGST" runat="server" Value='<%#Eval("AmountGST") %>' />
                                                                    <asp:TextBox ID="txtAmountGST" runat="server" MaxLength="8" CssClass="form-control modaltextbox" Enabled="false"></asp:TextBox>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Model </br> No</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hdnmdlno" runat="server" Value='<%#Eval("ModelNo") %>' />
                                                                    <asp:TextBox ID="txtmodelno" runat="server" CssClass="form-control modaltextbox" Text='<%# Eval("ModelNo") %>' Enabled="false"></asp:TextBox>

                                                                    <%--<asp:TextBox ID="txtmodelno" runat="server"  CssClass="form-control modaltextbox" Enabled="false" ></asp:TextBox>--%>

                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^\d*\.?\d+$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtAmount" ValidationGroup="Req"  ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req"  ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Expiry </br> Date</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hdnExpiryDate" runat="server" Value='<%#Eval("ExpiryDate","{0:dd MMM yyyy}") %>' />
                                                                    <asp:TextBox ID="txtexpdate" runat="server" CssClass="form-control modaltextbox" Enabled="false"></asp:TextBox>

                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^\d*\.?\d+$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtAmount" ValidationGroup="Req"  ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req"  ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="padding_top30">
                                                                    <span class="name ">
                                                                        <!--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%# Eval("StockOrderItemID") %>' />
                                                                            <asp:CheckBox ID="chkdelete" runat="server" />
                                                                            <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                                <span></span>
                                                                            </label>
                                                                            <br />
                                                                            <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />-->
                                                                        <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                            CausesValidation="false" /><br />

                                                                    </span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>
                                        <div class="col-sm-1">
                                            <%--<asp:Panel runat="server" ID="PanelAddMore">--%>
                                            <div class="" style="margin-top: 25px;">
                                                <asp:Button ID="btnaddnew" OnClick="btnAddRow_Click" runat="server" Text="Add" CssClass="btn btn-info redreq"
                                                    CausesValidation="false" />
                                            </div>
                                            <%--</asp:Panel>--%>
                                        </div>
                                        <a href="#" id="anpanel" runat="server" style="display: none" class="btn  btn-warning" data-toggle="modal" data-target="#myModalpanel">Payment</a>
                                    </div>
                                    <%--   </div>  --%>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label id="Label4" runat="server" class="disblock">
                                                    Comments</label>
                                            </span>
                                            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="100px" Width="100%"
                                                CssClass="form-control modaltextbox">
                                            </asp:TextBox>
                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                            </cc1:FilteredTextBoxExtender>
                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label id="Label45" runat="server" class="disblock">
                                                    Extra Notes</label>
                                            </span>
                                            <asp:TextBox ID="txtExtraNotes" runat="server" TextMode="MultiLine" Height="100px" Width="100%"
                                                CssClass="form-control modaltextbox">
                                            </asp:TextBox>
                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtExtraNotes"
                                                FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                            </cc1:FilteredTextBoxExtender>
                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtExtraNotes"
                                                FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                    </div>

                                    <div class="form-group checkareanew row" style="display: none;">
                                        <div class="col-sm-3 rightalign right-text">
                                            <asp:Label ID="Label23" runat="server" class="control-label">
                                               Stock From Outside?</asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="checkbox-fade fade-in-primary d-">

                                                <label for="<%=chkDeliveryOrderStatus.ClientID %>">
                                                    <asp:CheckBox ID="chkDeliveryOrderStatus" runat="server" />
                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                    <span class="text">&nbsp;</span>
                                                </label>

                                            </div>

                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row">

                                        <div class="col-sm-12 text-center">
                                            <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req"
                                                Text="Add" />
                                            <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" ValidationGroup="Req"
                                                Text="Save" Visible="false" />
                                            <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                CausesValidation="false" Text="Reset" />
                                            <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                CausesValidation="false" Text="Cancel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanSerialNo" runat="server" visible="false">
                                <i class="icon-info-sign"></i>
                                <asp:Literal ID="ltrerror" runat="server"></asp:Literal></literal></strong>
                            </div>
                        </div>


                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Order No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                        WatermarkText="Order No" />
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSearchOrderNo" FilterType="Numbers" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                        WatermarkText="Stock Item" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtstockitemfilter" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStockItemList"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtStockContainerFilter" runat="server" placeholder="Container No." CssClass="form-control m-b" MaxLength="50"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStockContainerFilter"
                                                        WatermarkText="Container No." />
                                                </div>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlloc" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Location</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Location</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddlLocation" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptLocation" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlStockOrderStatus" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Stock Order Status</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                                        <asp:ListItem Value="0">OnHold</asp:ListItem>
                                                        <asp:ListItem Value="2">Void</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlpurchasecompanysearch" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Purchase Company</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect PurchaseCompany martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida1" id="spanselect1">Company</span>
                                                                <p class="multiSel1"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddCompany" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptCompany" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnPurchaseCompanyName" runat="server" Value='<%# Eval("PurchaseCompanyName") %>' />
                                                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("Id") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("PurchaseCompanyName")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Vendor</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect Vender martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida2" id="spanselect2">Vender</span>
                                                                <p class="multiSel2"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddVender" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptVender" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                <asp:HiddenField ID="hdnCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlitemnamesearch" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Payment Status</asp:ListItem>
                                                        <asp:ListItem Text="Order Booked" Value="4" />
                                                        <asp:ListItem Text="Dep Paid" Value="1" />
                                                        <asp:ListItem Text="Final Payment" Value="2" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlPaymentMethod" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Payment Method</asp:ListItem>
                                                        <asp:ListItem Text="Cash" Value="1" />
                                                        <asp:ListItem Text="Credit" Value="2" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDelevery" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Delivery Type </asp:ListItem>
                                                        <asp:ListItem Text="Warehouse" Value="1" />
                                                        <asp:ListItem Text="Direct" Value="2" />
                                                        <asp:ListItem Text="Third Party" Value="3" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Show</asp:ListItem>
                                                        <asp:ListItem Value="False" Selected="True">Not Delivered</asp:ListItem>
                                                        <asp:ListItem Value="True">Delivered</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlLocalInternation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Order Type</asp:ListItem>
                                                        <asp:ListItem Value="0">Local</asp:ListItem>
                                                        <asp:ListItem Value="1">International</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlSearchTransCompName" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">TransCompName</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlPartial" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Partial</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2" Selected="True">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchGSTType" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">GST Type</asp:ListItem>
                                                        <asp:ListItem Value="1">With GST</asp:ListItem>
                                                        <asp:ListItem Value="2">Without GST</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170 dnone">
                                                    <asp:DropDownList ID="ddlOrderFor" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Order For</asp:ListItem>
                                                        <asp:ListItem Value="1">Retail</asp:ListItem>
                                                        <asp:ListItem Value="2">Wholesale</asp:ListItem>
                                                        <asp:ListItem Value="3" Enabled="true">Service</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170" style="display: none;">
                                                    <asp:DropDownList ID="ddlDue" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Due</asp:ListItem>
                                                        <asp:ListItem Value="0">Due Today</asp:ListItem>
                                                        <asp:ListItem Value="1">Due Tomorrow</asp:ListItem>
                                                        <asp:ListItem Value="2">OverDue</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                        <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                        <asp:ListItem Value="3">Expected Date</asp:ListItem>
                                                        <asp:ListItem Value="4">Received Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                        <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <%--<div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnUpdateOrderStatus" runat="server" CausesValidation="false" CssClass="btn btn-info  fullWidth" Text="UpdateStatus" OnClick="btnUpdateOrderStatus_Click" />

                                                </div>--%>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>

                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="datashowbox inlineblock">
                                        <div class="row">

                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="true" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>


                                            </div>
                                            <%--<div class="input-group martop5" style="width: 200px; padding: 0 5px;">
                                                <asp:LinkButton ID="btnExcelNew" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="true" OnClick="btnExcelNew_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Stock Prediction Excel </asp:LinkButton>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <%-- <div class="form-group">
                <div class='input-group date' id='datetimepicker01'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>--%>

                <div class="finalgrid ">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <asp:HiddenField ID="hdnStockOrderID1" runat="server" />
                                    <div class="card-block">
                                        <div class="table-responsive BlockStructure">

                                            <asp:GridView ID="GridView1" DataKeyNames="StockOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("OrderNumber") %>','tr<%# Eval("OrderNumber") %>');">
                                                                <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />--%>
                                                                <img id='imgdiv<%# Eval("OrderNumber") %>' src="../../../images/icon_plus.png" />
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkord" class="checkbox-fade fade-in-primary d-" Style="margin: 0;" runat="server" Visible='<%#Eval("PendingQty").ToString() == "0" ? false : true %>'></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Order No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="OrderNumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Width="10px" CssClass="gridmainspan">
                                                                <asp:HiddenField ID="hdnIsTelexUpload" runat="server" Value='<%# Eval("IsTelexUpload") %>' />
                                                                <asp:HiddenField ID="hdnIspaperUpload" runat="server" Value='<%# Eval("IspaperUpload") %>' />
                                                                <asp:HiddenField ID="hdnStockOrderID" runat="server" Value='<%# Eval("StockOrderID") %>' />
                                                                <asp:HiddenField ID="hdnactualdelivery" runat="server" Value='<%# Eval("ActualDelivery") %>' />
                                                                <asp:HiddenField ID="hdnOrderStatus" runat="server" Value='<%# Eval("DeliveryOrderStatus") %>' />
                                                                <%#Eval("OrderNumber")%>
                                                                <asp:HyperLink ID="hlDetails1" runat="server" Width="10px" NavigateUrl='<%# "~/admin/adminfiles/reports/order.aspx?id="+ Eval("StockOrderID").ToString()%>' Visible="false">
                                                                    <%#Eval("OrderNumber")%>
                                                                </asp:HyperLink>

                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockOrderStatus" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="StockOrderStatus">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ordst" runat="server" Width="80px" data-placement="top" data-toggle="tooltip">
                                                <%#Eval("StockOrderStatus1")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="VendorInvoiceNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinvoiceno" runat="server" Width="80px" data-placement="top" data-original-title='<%#Eval("VendorInvoiceNo")%>' data-toggle="tooltip">
                                                <%#Eval("VendorInvoiceNo")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="P.Company" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="PurchaseCompany">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnPurchaseCompanyName" Value='<%#Eval("PurchaseCompanyId")%>' />
                                                            <asp:Label ID="lblPurchaseCompanyName" runat="server" Width="100px"><%#Eval("PurchaseCompanyName")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CompanyLocation" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinvoiceno566" runat="server" Width="80px" data-placement="top" data-original-title='<%#Eval("CompanyLocation")%>' data-toggle="tooltip">
                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                SortExpression="CompanyLocation">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPurchaseCompanyName45" runat="server" Width="100px"><%#Eval("CompanyLocation")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField--%>
                                                    <asp:TemplateField HeaderText="Ordered Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="DateOrdered">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "DateOrdered", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Exp.DeliveryDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="ExpectedDelivery">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblordDate" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "ExpectedDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Manual Order #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ManualOrderNumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label5" runat="server" Width="120px" data-placement="top" data-original-title='<%#Eval("ManualOrderNumber")%>' data-toggle="tooltip">
                                                <%#Eval("ManualOrderNumber")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Container No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StockContainer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label54" runat="server" Width="120px" data-placement="top" data-original-title='<%#Eval("StockContainer")%>' data-toggle="tooltip">
                                                <%#Eval("StockContainer")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Items Ordered" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StockOrderItem">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("StockOrderItem")%>' data-toggle="tooltip"
                                                                Width="260px"><%#Eval("StockOrderItem")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Vendor" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                        SortExpression="Vendor">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label7" runat="server" data-placement="top" data-original-title='<%#Eval("Vendor")%>' data-toggle="tooltip"
                                                                Width="170px"><%#Eval("Vendor")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Payment Status" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Itemname">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnItemName" Value='<%#Eval("ItemNameId")%>' />
                                                            <asp:Label ID="lblItemName" runat="server" Width="50px"><%#Eval("ItemNameValue")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Payment Method" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Itemname">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnPaymentMethod" Value='<%#Eval("PaymentMethodID")%>' />
                                                            <asp:Label ID="lblPaymentMethod" runat="server" Width="50px"><%#Eval("PaymentMethod")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                     <asp:TemplateField HeaderText="Currency" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Currency">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrency" runat="server" ><%#Eval("Currency")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Qty">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnQty" Value='<%#Eval("Qty")%>' />
                                                            <asp:Label ID="Label8" runat="server" Width="50px" Text='<%#Eval("Qty")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <%--<asp:TemplateField HeaderText="Received Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="ReceivedQty">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnReceivedQty" Value='<%#Eval("ReceivedQty")%>' />
                                                            <asp:Label ID="lblReceivedQty" runat="server" Width="50px" Text='<%#Eval("ReceivedQty")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="Pending Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="PendingQty">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnPendingQty" Value='<%#Eval("PendingQty")%>' />
                                                            <%--<asp:Label ID="lblPendingQty" runat="server" Width="50px" Text='<%#Eval("PendingQty")%>'></asp:Label>--%>
                                                            <asp:LinkButton ID="lbtnPendingQty" Width="50px" runat="server" Text='<%#Eval("PendingQty")%>'
                                                                CommandName="ViewPendingQty" CommandArgument='<%#Eval("StockOrderID")%>'
                                                                Enabled='<%#Eval("PendingQty").ToString() == "0" ? false : true %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amount Without GST" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Amount">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnAmount" Value='<%#Eval("Amount")%>' />
                                                            <asp:Label ID="lblAmount" runat="server" Width="50px">
                                                        <%#Eval("Amount")%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Amount With GST" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Amount">
                                                        <ItemTemplate>
                                                            <%--<asp:HiddenField runat="server" ID="hdnAmount" Value='<%#Eval("Amount")%>' />--%>
                                                            <asp:Label ID="lblGSTAmount" runat="server" Width="50px">
                                                        <%#Eval("GSTAmount")%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hypDetail" runat="server" CausesValidation="false" CommandName="detail" CommandArgument='<%# Eval("StockOrderID") %>'
                                                                data-placement="top" data-original-title="Detail" data-toggle="tooltip" CssClass="btn btn-primary btn-mini"
                                                                Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/stock/StockOrderDelivery.aspx?StockOrderID=" + Eval("StockOrderID") %>'>
                                                                          <i class="fa fa-link"></i>Detail
                                                            </asp:HyperLink>
                                                            <%--  <img runat="server" id="imgr" src="../../images/icon_delivered.png" visible=' <%# Eval("Delivered").ToString() == "False" ? false : true%>' />--%>
                                                            <%--<asp:LinkButton ID="btnDelivered" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-maroon btn-mini" Visible='<%#Eval("DeliveryOrderStatus").ToString() == "1" ? Eval("Delivered").ToString() == "False" ? true : false : false %>'
                                                        data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" OnClientClick="yes" CommandArgument='<%#Eval("StockOrderID")%>'> <i class="fa fa-truck"></i> Delivery</asp:LinkButton>--%>
                                                            <asp:LinkButton ID="btnRevert" runat="server" Text="Revert" CausesValidation="false" CssClass="btn btn-inverse btn-mini" Visible='<%# string.IsNullOrEmpty(Eval("ActualDelivery").ToString()) ? false : true%>'
                                                                data-toggle="tooltip" data-placement="top" title="Revert" CommandName="Revert" CommandArgument='<%#Eval("StockOrderID")%>'> <i class="fa fa-retweet"></i> Revert</asp:LinkButton>


                                                            <%--<asp:Image runat="server"  ImageUrl="~/images/revert_deactive.png" />--%>
                                                            <%--	<span class="btn btn-default btn-mini" ID="imgko" Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'  data-toggle="tooltip" data-placement="top" title="Deactive Revert" runat="server"><i class="fa fa-retweet"></i> Revert</span>--%>
                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" Visible="false"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" CssClass="btn btn-info btn-mini">
                                                            <i class="fa fa-edit"></i>Edit
                                                            </asp:LinkButton>

                                                            <!--DELETE Modal Templates-->

                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("StockOrderID")%>'
                                                                Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                        <i class="fa fa-trash"></i> Delete
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="gvbtnPmt" runat="server" Visible="false" CssClass="btn btn-info btn-mini" CausesValidation="false" CommandName="Payment" CommandArgument='<%#Eval("StockOrderID")%>'>
                        <i class="fa fa-trash"></i> Payment
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="gvbtViewUpload" runat="server" CssClass="btn btn-info btn-mini" CausesValidation="false" CommandName="ViewUpload" CommandArgument='<%#Eval("StockOrderID")%>'>
                        <i class="fa fa-eye"></i> View
                                                            </asp:LinkButton>

                                                            <asp:HyperLink ID="lnkExcelUploded" runat="server" CausesValidation="false" CssClass="btn btn-success btn-mini"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Excel Uploded" Style="font-size: 10px!important; color: white"
                                                                Visible='<%# Eval("DeliveryOrderStatus").ToString() == "1" ? Eval("Qty").ToString() == Eval("UploadSerialCount").ToString() ? true:false : false %>'>
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Excel Uploaded
                                                            </asp:HyperLink>

                                                            <asp:HyperLink ID="lnkExcelPending" runat="server" CausesValidation="false" CssClass="btn btn-warning btn-mini"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Excel Uploded" Style="font-size: 10px!important; color: white"
                                                                Visible='<%# Eval("DeliveryOrderStatus").ToString() == "1" ? Eval("Qty").ToString() != Eval("UploadSerialCount").ToString() ? true:false : false %>'>
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> Excel Pending
                                                            </asp:HyperLink>

                                                            <%--<asp:HyperLink ID="lnkTelexPending" runat="server" CausesValidation="false" CssClass="btn btn-success btn-mini" Visible="false"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="TelexUploded" Style="font-size: 10px!important; color: white">
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Telex
                                                            </asp:HyperLink>

                                                            <asp:HyperLink ID="lnkTelexUpload" runat="server" CausesValidation="false" CssClass="btn btn-warning btn-mini" Visible="false"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Telex Uploded" Style="font-size: 10px!important; color: white">
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> Telex
                                                            </asp:HyperLink>--%>

                                                           <%-- <asp:HyperLink ID="lnkpaperpending" runat="server" CausesValidation="false" CssClass="btn btn-success btn-mini" Visible="false"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="PaperWorkUploded" Style="font-size: 10px!important; color: white">
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Paperwork                                                                         </asp:HyperLink>

                                                            <asp:HyperLink ID="lnkpaperupload" runat="server" CausesValidation="false" CssClass="btn btn-warning btn-mini" Visible="false"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Paper Uploded" Style="font-size: 10px!important; color: white">
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i>Paperwork 
                                                            </asp:HyperLink>--%>

                                                           <%-- <asp:LinkButton ID="lnkSubmit" runat="server" CausesValidation="false" CommandName="Submit" CommandArgument='<%# Eval("StockOrderID") %>'
                                                                CssClass="btn btn-primary btn-mini">
                                                                          <i class="fa fa-save"></i>Submit
                                                            </asp:LinkButton>--%>

                                                            <asp:LinkButton ID="lnkPurchaseOrder" runat="server" CausesValidation="false" CommandName="Purchase Order" CommandArgument='<%# Eval("StockOrderID") %>'
                                                                CssClass="btn btn-primary btn-mini">
                                                                          <i class="fa fa-save"></i>PDF
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="verticaaline" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("OrderNumber") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("OrderNumber") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 15%;"><b>Stock For</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="lblstockfor" runat="server">
                                                                                     <%#Eval("CompanyLocation")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;"><b>Delivery Date</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="lbldeliverydate" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ActualDelivery", "{0:dd MMM yyyy}")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;"><b>Delivered By</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="lbldeliveredby" runat="server" Text='<%#Eval("DeliveredByName")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Delivery Type</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDeleveryvalue" runat="server" Text='<%#Eval("Deleveryvalue")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%;"><b>Comfirm Arrival Date</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label50" runat="server">
                                                                                     <%#Eval("arrivaldate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                </td>

                                                                                <td style="width: 15%;"><b>Transoprt Co. Sent Date</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label51" runat="server">
                                                                                     <%#Eval("transportcompsentdate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                </td>

                                                                                <td style="width: 15%;"><b>Telex Release Date</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label52" runat="server">
                                                                                     <%#Eval("telex_Release_date","{0:dd MMM yyyy}")%></asp:Label>
                                                                                </td>

                                                                                <td style="width: 15%;"><b>Supplier Invoice No</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label53" runat="server">
                                                                                     <%#Eval("supplier_invlice_no")%></asp:Label>
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td style="width: 10%;">
                                                                                    <b>Total PI Amt.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label55" runat="server" Text='<%#Eval("total_pi_amt")%>'></asp:Label>
                                                                                </td>

                                                                                <td style="width: 10%;">
                                                                                    <b>Trans. Co's Job No.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label56" runat="server" Text='<%#Eval("transport_comp_jobno")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Trans. Co's Service Inv.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label57" runat="server" Text='<%#Eval("transport_comp_service_invno")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Trans. Co's DISB.Service Inv.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label58" runat="server" Text='<%#Eval("transport_comp_service_dist_invno")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>

                                                                                <td style="width: 10%;">
                                                                                    <b>Pickup email date from W/H.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label59" runat="server" Text='<%#Eval("pckemailwhdate")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Storage Charge Amount</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label60" runat="server" Text='<%#Eval("storage_charge_amt")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Store Charge Invoive No</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label61" runat="server" Text='<%#Eval("storage_charge_invno")%>'></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <b>Reason For Store Charge</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="Label62" runat="server" Text='<%#Eval("storage_change_Reason")%>'></asp:Label>
                                                                                </td>

                                                                            </tr>
                                                                </td>

                                                                </table>

                                                                    </div>
                                                                
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Danger Modal Templates-->
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="btnCancelDetail">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="width: 60%">
                    <div class="modal-content">
                        <div class="color-line "></div>
                        <div class="modal-header printorder">
                            <div style="float: right">
                                <asp:Button ID="btnCancelDetail" Width="65px" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />

                                <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-mini" OnClientClick="javascript:printContent();" CausesValidation="false">
                                              <i class="fa fa-print"></i> Print
                                </asp:LinkButton>
                            </div>

                            <h4 class="modal-title" id="myModalLabel">Stock Order Detail</h4>

                        </div>
                        <h4 id="myModalLabel1" style="display: none" class="row-title"><i class="typcn typcn-th-small"></i>Stock Order Detail</h4>
                        <div class="modal-body paddnone">
                            <div class="panel-body" id="detailsid" runat="server">
                                <div class="formainline">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered table-striped">
                                                <tr>
                                                    <td width="200px" valign="top">Stock Location
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Vendor
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">BOL Received
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Manual Order No
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Expected Delevery
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Notes
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trOrderItem" runat="server">
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered table-striped">
                                                            <tr class="graybgarea">
                                                                <td colspan="4">
                                                                    <h4>Stock Order Item Detail</h4>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Stock Category</b></td>
                                                                <td><b>Quantity</b></td>
                                                                <td><b>Stock Item</b></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <asp:Repeater ID="rptOrder" runat="server" OnItemDataBound="rptOrder_ItemDataBound" OnItemCommand="rptOrder_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <asp:HiddenField runat="server" ID="hfStockOrderID" Value='<%#Eval("StockOrderID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hfStockCategoryID" Value='<%#Eval("StockCategoryID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hfStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                                        <td><%#Eval("StockOrderItem") %></td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblOrderQuantity" Text='<%#Eval("OrderQuantity") %>'></asp:Label></td>
                                                                        <td><%#Eval("StockItem") %></td>
                                                                        <%--Visible='<%#Eval("DeliveryOrderStatus").ToString() == "1" ?true : false %>'--%>
                                                                        <td>
                                                                            <asp:LinkButton ID="btnDelivery" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-maroon btn-mini"
                                                                                data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" OnClientClick="yes" CommandArgument='<%#Eval("StockOrderID") +","+ Eval("StockOrderItemID")%>'> <i class="fa fa-truck"></i> Delivery</asp:LinkButton>
                                                                            <asp:ImageButton ID="checkimag" Visible="false" runat="server" Enabled="false" Height="20px" Width="40px" ImageUrl="~/images/check.png" /></td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupdeliver" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divdeliver" TargetControlID="btnNull1"
                CancelControlID="btnCancel1">
            </cc1:ModalPopupExtender>
            <div id="divdeliver" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid" runat="server" />
                <asp:HiddenField ID="hndStockOrderItemID" runat="server" />
                <div class="modal-dialog" style="width: 300px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancel1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H1">Delivery Date</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label25" runat="server" class="control-label ">
                                                <strong>Delivery Date</strong></asp:Label>
                                        </span>
                                        <div class="input-group date datetimepicker1 col-sm-12">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtdatereceived" runat="server" class="form-control">                                             
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqfieldtxtdatereceived" runat="server" ForeColor="Red" ControlToValidate="txtdatereceived" ErrorMessage="" ValidationGroup="adddate"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label22" runat="server" class="control-label ">
                                                <strong>Serial No.</strong></asp:Label>
                                        </span>
                                        <span class="">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block; padding: 0; margin-top: 5px; margin-bottom: 5px;" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="adddate" ValidationExpression="^.+(.xls|.XLS)$"
                                                Display="Dynamic" ForeColor="Red" ErrorMessage="Upload .xls file only"></asp:RegularExpressionValidator>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ForeColor="Red" ControlToValidate="FileUpload1" ErrorMessage="" ValidationGroup="adddate" ></asp:RequiredFieldValidator>--%>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>--%>
                                            <%--<div class="col-sm-12" style="padding:0; margin-top:5px;">                            
                                            <asp:TextBox ID="txtserialno" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>--%>
                                        </span>
                                    </div>



                                    <div runat="server" class="form-group marginleft col-sm-12" style="text-align: center; margin-bottom: 0;">
                                        <asp:Button ID="btndeliver" runat="server" Text="Save" OnClick="btndeliver_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="adddate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderStock" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStock" TargetControlID="btnNULLStock"
                CancelControlID="btnCancelStock">
            </cc1:ModalPopupExtender>
            <div id="divStock" class="modal_popup Addstock_popup" runat="server" style="display: none">
                <div class="modal-dialog addcustomermodal" style="width: 800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Stock Item</h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label12" runat="server" class="control-label">
                                                           Stock&nbsp;Category</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server" ErrorMessage="" ValidationGroup="stock"
                                                        ControlToValidate="ddlstockcategory" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label13" runat="server" class="control-label">
                                                            Stock&nbsp;Item</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label16" runat="server" class="control-label">
                                                            Brand</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label2" runat="server" class="control-label">
                                                       Stock&nbsp;Model</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label17" runat="server" class="control-label">
                                                               Stock&nbsp;Series</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label18" runat="server" class="control-label">
                                                          Stock&nbsp;Size</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                        ControlToValidate="txtStockSize" Display="Dynamic" ErrorMessage="Enter valid digit"
                                                        ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label5" runat="server" class="control-label">
                                                 Min.&nbsp;Stock</asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtminstock" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group checkareanew row" id="sales" runat="server" visible="false">
                                                <div class="col-md-6 rightalign">
                                                    <asp:Label ID="Label19" runat="server" class=" control-label">
                                                           Sales&nbsp;Tag</asp:Label>
                                                </div>
                                                <div class="col-sm-4 checkbox-info checkbox">
                                                    <asp:CheckBox ID="chksalestag" runat="server" />
                                                    <label for="<%=chksalestag.ClientID %>">
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group checkareanew row">
                                                <div class="col-md-6 rightalign ">
                                                    <asp:Label ID="Label11" runat="server" class="control-label">
                                                               Is Active?</asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox-fade fade-in-primary d-">

                                                        <label for="<%=chkisactive.ClientID %>">
                                                            <asp:CheckBox ID="chkisactive" runat="server" />
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span class="text">&nbsp;</span>
                                                        </label>

                                                    </div>

                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group checkareanew row">
                                                <div class="col-md-6 rightalign">
                                                    <asp:Label ID="Label20" runat="server" class="control-label">
                                                              Is&nbsp;Dashboard?</asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox-fade fade-in-primary d-">

                                                        <label for="<%=chkDashboard.ClientID %>">
                                                            <asp:CheckBox ID="chkDashboard" runat="server" />
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span class="text">&nbsp;</span>
                                                        </label>

                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="col-md-12" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="name form-group">

                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Location </strong>
                                                    </label>

                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Quantity</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Sales&nbsp;Tag</strong>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                            <ItemTemplate>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <span>
                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <asp:HiddenField ID="hndlocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <span>
                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                                            ControlToValidate="txtqty" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4 right-text checkbox-info checkbox ">
                                                                    <div class="thkmartop">
                                                                        <div class="checkbox-fade fade-in-primary d-">
                                                                            <label for="<%# Container.FindControl("chksalestagrep").ClientID %>">
                                                                                <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                                <span class="text">&nbsp;</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <div id="Div4" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Description</label>
                                            </span><span>
                                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                    Width="100%" Style="resize: none;">
                                                </asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <%--<div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Is&nbsp;Dashboard?</label>
                                                            </span><span>
                                                                <asp:CheckBox ID="chkDashboard" runat="server" />

                                                                <label for="<%=chkDashboard.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>--%>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group ">
                                        <div class="col-sm-8 col-sm-offset-4 text-right">
                                            <div style="float: right">
                                                <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                    CssClass="btn btn-primary savewhiteicon POPupLoader" ValidationGroup="stock" />
                                                <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger" data-dismiss="modal"><span aria-hidden="true">Close</span></button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <%--   <div class="formainline">

                                    <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label12" runat="server" class="col-sm-6 control-label"> Stock&nbsp;Category</asp:Label>

                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlstockcategory" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Item</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                                            Brand</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Model</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Series</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Size</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[0-9]*$" ValidationGroup="stock"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtStockSize" Display="Dynamic"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                                             Min.&nbsp;Stock</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="comperror" ValidationGroup="stock"
                                                            ControlToValidate="txtminstock" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[0-9]*$"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chksalestag.ClientID %>">
                                                            <asp:CheckBox ID="chksalestag" runat="server" />
                                                            <span class="text">Sales&nbsp;Tag</span>
                                                        </label>


                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chkisactive.ClientID %>">
                                                            <asp:CheckBox ID="chkisactive" runat="server" />
                                                            <span class="text">Is&nbsp;Active?</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Stock&nbsp;Location
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                &nbsp;&nbsp;   Stock&nbsp;Quantity
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass=""
                                                                                            ControlToValidate="txtqty" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock" Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>



                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">
                                                            Description</label>

                                                        <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                            Width="100%" Style="resize: none;">
                                                        </asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-12">

                                                        <label for="<%=chkDashboard.ClientID %>">
                                                            <asp:CheckBox ID="chkDashboard" runat="server" />
                                                            <span class="text">Is&nbsp;Dashboard?</span>
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="row" style="text-align: center">
                                            <div class="form-group marginleft">
                                                <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />
                                            </div>
                                        </div>
                                    </div>

                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button1" Style="display: none;" runat="server" />


                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                        DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                        CancelControlID="btnCancelVndr">
                    </cc1:ModalPopupExtender>
                    <div id="div1" runat="server" style="display: none;" class="modal_popup">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">

                                    <h4 class="modal-title" id="H2">Add New Vendor</h4>
                                    <div style="float: right">
                                        <button id="Button2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body paddnone">
                                    <div class="panel-body">
                                        <div class="formainline">

                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label14" runat="server" class="col-sm-4 control-label">
                                                <strong>Customer</strong></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                    --%>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label15" runat="server" class="col-sm-4 control-label">
                                                <strong>Name</strong></asp:Label>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox2" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox3" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group center-text col-sm-12">
                                                <asp:Button ID="Button3" runat="server" Text="Add" OnClick="ibtnaddvendor_click" Visible="false"
                                                    CssClass="btn btn-primary savewhiteicon POPupLoader" ValidationGroup="vendor" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button4" Style="display: none;" runat="server" />
                    <!--Danger Modal Templates-->
                    <asp:Button ID="Button5" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                        PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
                    </cc1:ModalPopupExtender>
                    <div id="Div2" runat="server" style="display: none" class="modal_popup">

                        <div class="modal-dialog">
                            <div class=" modal-content">

                                <div class="modal-header text-center">
                                    <h5 class="modal-title" id="H2">Delete</h5>
                                    <div style="float: right">
                                        <asp:LinkButton ID="LinkButton6" runat="server" class="close" data-dismiss="modal">  <span aria-hidden="true">x</span></asp:LinkButton>
                                    </div>
                                </div>



                                <label id="Label21" runat="server"></label>
                                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                                <div class="modal-footer" style="text-align: center">
                                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>

                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <!--End Danger Modal Templates-->
                </div>
            </div>
            <asp:Button ID="btnNULLStock" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupExtenderVendor" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                CancelControlID="btnCancelVndr">
            </cc1:ModalPopupExtender>
            <div id="divVendor" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">

                            <h4 class="modal-title" id="H2">
                                <asp:Label ID="lblModelHeader" runat="server" />
                                Vendor</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label9" runat="server" class="col-sm-4 control-label">
                                                <strong>Customer</strong></asp:Label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                            --%>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label10" runat="server" class="col-sm-4 control-label">
                                                <strong>Name:</strong></asp:Label>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContFirst" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContLast" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label66" runat="server" class="col-sm-4 control-label">
                                                <strong>Credit:</strong></asp:Label>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtCredit" runat="server" placeholder="Credit in Days" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group center-text col-sm-12">
                                        <div class=" col-sm-12  text-center">
                                            <div style="float: none">
                                                <asp:Button ID="ibtnAddVendor" runat="server" Text="Add" OnClick="ibtnaddvendor_click"
                                                    CssClass="btn btn-primary savewhiteicon POPupLoader" ValidationGroup="vendor" />
                                                <asp:Button ID="ibtnUpdateVendor" runat="server" Text="Update" OnClick="ibtnUpdateVendor_Click"
                                                    CssClass="btn btn-primary savewhiteicon POPupLoader" ValidationGroup="vendor" Visible="false" />
                                                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close" id="btnCancelVndr" runat="server">
                                                    <span aria-hidden="true">Close</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNullVndr" Style="display: none;" runat="server" />
            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup">

                <div class="modal-dialog">
                    <div class=" modal-content ">
                        <div class="modal-header">
                            <h5 class="modal-title" id="H2">Delete</h5>
                            <%--  <div style="float: right">
                                <asp:LinkButton ID="LinkButton7" runat="server" class="close" data-dismiss="modal"> <span aria-hidden="true">Cancel</span></asp:LinkButton>
                            </div>--%>
                        </div>


                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn btn-danger" data-dismiss="modal"> <span aria-hidden="true">Cancel</span></asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />




            <cc1:ModalPopupExtender ID="ModalAlertExpire" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divalertexpiry" TargetControlID="Button8"
                CancelControlID="Button7">
            </cc1:ModalPopupExtender>
            <div id="divalertexpiry" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 400px;">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Alert</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <asp:Label runat="server" ID="lblexpire" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <button id="Button7" runat="server" type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>
                    </div>

                </div>
            </div>
            <asp:Button ID="Button8" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupRevert2" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="DivRevertPopup2" TargetControlID="btnNull2" CancelControlID="btnCancel2">
            </cc1:ModalPopupExtender>
            <div id="DivRevertPopup2" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid2" runat="server" />
                <asp:HiddenField ID="hndStockOrderItemID2" runat="server" />
                <div class="modal-dialog" style="width: 340px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Reset Stock Order</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <asp:Label runat="server" ID="lblalert" Text="Are you sure you want to reset stock order?"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnOK3" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnOK3_Click"></asp:Button>
                            <asp:Button ID="btnCancel2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>

                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divModalNote" TargetControlID="Button12" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divModalNote" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Payment Details</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="card">
                                <div class="card-block">
                                    <div class="form-group row marginbtm0">
                                        <div class="col-sm-4 custom_datepicker">
                                            <asp:Label ID="Label46" runat="server" class="disblock">
                                               Deposit Date</asp:Label>
                                            <div class="input-group sandbox-container">

                                                <asp:TextBox ID="tctdepodate" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="Label47" runat="server" class="disblock">
                                                Rate</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtRate" onkeypress="return isNumberKey(event)" runat="server" class="form-control">
                                                </asp:TextBox>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row marginbtm0">

                                        <div class="col-md-4">
                                            <asp:Label ID="Label48" runat="server" class="disblock">
                                                Amount USD</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtAmountUsd" OnTextChanged="txtAmountUsd_TextChanged" onkeypress="return isNumberKey(event)" AutoPostBack="true" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="Label49" runat="server" class="disblock">
                                                Amount AUD</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtaud" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>

                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label65" runat="server" class="disblock">
                                                Payment Notes</asp:Label>
                                                <div class="input-group">

                                                    <asp:TextBox ID="txtpaymentnotes" TextMode="MultiLine" Rows="3" Columns="6" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                    <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <asp:Panel ID="panel3" runat="server" CssClass="xsroll">
                                <div id="PanGridNotes" runat="server">
                                    <div class="card" style="max-height: 230px; overflow: auto">
                                        <div class="card-block">
                                            <div class="table-responsive BlockStructure">
                                                <asp:HiddenField ID="hndpmtDetailsId1" runat="server"></asp:HiddenField>
                                                <asp:GridView ID="GridViewNote" DataKeyNames="stock_payment_DetailsId" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" AutoGenerateColumns="false" OnRowCommand="GridViewNote_RowCommand" OnRowUpdating="GridViewNote_RowUpdating" OnRowEditing="GridViewNote_RowEditing" OnRowDeleting="GridViewNote_RowDeleting" OnSelectedIndexChanged="GridViewNote_SelectedIndexChanged">
                                                    <Columns>
                                                        <%--<asp:TemplateField ItemStyle-Width="20px">
                                                                            <ItemTemplate>--%>
                                                        <%--<a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">--%>
                                                        <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />

                                                                                </a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>


                                                        <asp:TemplateField HeaderText="DepositDate" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                            SortExpression="DepositDate">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndpmtDetailsId" runat="server" Value='<%# Eval("stock_payment_DetailsId") %>' />
                                                                <asp:Label ID="lblNoteDate" runat="server" Width="50px"><%#Eval("DepositDate","{0:dd MMM yyyy}")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Deposit_amountUsd" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Deposit_amountUsd">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Note1" runat="server" Width="80px">
                                                                                <%#Eval("Deposit_amountUsd")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Deposit_amountAsd" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Deposit_amountAsd">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Note" runat="server" Width="80px">
                                                                                <%#Eval("Deposit_amountAsd")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Rate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="UsdRate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Rate" runat="server" Width="80px">
                                                                                <%#Eval("UsdRate")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Payment Note" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="UsdRate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="PaymentNotes" runat="server" Width="80px">
                                                                                <%#Eval("PaymentNotes")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- <asp:TemplateField HeaderText="Added By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Added By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblusernm" runat="server" Width="80px">
                                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("stock_payment_DetailsId")%>'>
                                                                    <i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnEdit" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Edit" CommandArgument='<%#Eval("stock_payment_DetailsId")%>'>
                                                                    <i class="fa fa-trash"></i> Edit
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSave" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Save" Visible="false" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="btnEdit" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Edit" Visible="false" OnClick="btnEdit_Click"></asp:Button>
                            <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button12" Style="display: none;" runat="server" />

            <asp:Button ID="Button6" Style="display: none;" runat="server" />
            <asp:Button ID="Button9" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="div3" TargetControlID="Button9" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="div3" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">View All Uploads</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="card">
                                <div class="card-block">
                                    <div class="form-group row marginbtm0">

                                        <div class="col-md-3">
                                            <asp:Label ID="Label68" runat="server" class="disblock">
                                                Paperwork Arrival</asp:Label>
                                            <div class="input-group">
                                                <asp:HyperLink ID="hplpaperwork" runat="server" Visible="false"></asp:HyperLink>
                                                <%--<asp:TextBox ID="TextBox5" onkeypress="return isNumberKey(event)"    runat="server" class="form-control">
                                                    </asp:TextBox> --%>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="Label69" runat="server" class="disblock">
                                                Telex Release</asp:Label>
                                            <div class="input-group">
                                                <asp:HyperLink ID="hplTelexUpload" runat="server" Visible="false"></asp:HyperLink>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="Label70" runat="server" class="disblock">
                                                Trans. Co's DISB.Service Inv</asp:Label>
                                            <div class="input-group">

                                                <asp:HyperLink ID="hpltransportdistinv" runat="server" Visible="false"></asp:HyperLink>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group row marginbtm0">
                                        <div class="col-md-3">
                                            <asp:Label ID="Label67" runat="server" class="disblock">
                                                Store Charge Inv.</asp:Label>
                                            <div class="input-group">

                                                <asp:HyperLink ID="lblstoragechargeinv" runat="server" Visible="false"></asp:HyperLink>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="Label71" runat="server" class="disblock">
                                                Trans. Co's Tax Inv.</asp:Label>
                                            <div class="input-group">

                                                <asp:HyperLink ID="transcoinv" runat="server" Visible="false"></asp:HyperLink>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer" style="justify-content: center;">
                                <%--<asp:Button ID="Button9" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Save" Visible="false" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="Button10" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Edit"  Visible="false" OnClick="btnEdit_Click"></asp:Button>--%>
                                <asp:Button ID="Button13" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button14" Style="display: none;" runat="server" />


            <asp:Button ID="Button10" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderViewDetails" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divDetails" TargetControlID="Button10" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divDetails" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 1000px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">View Quantity Details</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="modal-body paddnone">
                                <div class="panel-body" id="Div5" runat="server">
                                    <div class="formainline">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered table-striped">
                                                    <tr>
                                                        <td><b>Stock Item</b></td>
                                                        <td><b>Quantity</b></td>
                                                        <td><b>Received Quantity</b></td>
                                                        <td><b>Pending Quantity</b></td>
                                                    </tr>
                                                    <asp:Repeater ID="rptViewDetails" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%#Eval("StockOrderItem") %>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("OrderQuantity") %>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("ReceivedQty") %>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("PendingQty") %>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer" style="justify-content: center;">
                                <asp:Button ID="Button15" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button16" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderSubmit" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divSubmit" TargetControlID="Button10" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divSubmit" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 1000px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Submit Reason</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="modal-body paddnone">
                                <div class="panel-body" id="Div7" runat="server">
                                    <div class="formainline">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:HiddenField ID="hndOrderID" runat="server" />
                                                <asp:Label ID="Label74" runat="server" class="disblock">
                                                Reason</asp:Label>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtReason" TextMode="MultiLine" Rows="5" Columns="6" runat="server" class="form-control" Height="125px">
                                                    </asp:TextBox>
                                                    <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer" style="justify-content: center;">
                                <asp:Button ID="btnSaveSubmit" runat="server" class="btn btn-primary" OnClick="btnSaveSubmit_Click" Text="Submit"></asp:Button>
                                <asp:Button ID="Button17" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Danger Modal Templates-->
            <asp:Button ID="Button18" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="Div6" DropShadow="false" CancelControlID="LinkButton7" TargetControlID="Button18">
            </cc1:ModalPopupExtender>
            <div id="Div6" runat="server" style="display: none" class="modal_popup">

                <div class="modal-dialog">
                    <div class=" modal-content">

                        <div class="modal-header text-center">
                            <h5 class="modal-title" id="H2">Update</h5>
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton7" runat="server" class="close" data-dismiss="modal">  <span aria-hidden="true">x</span></asp:LinkButton>
                            </div>
                        </div>



                        <label id="Label77" runat="server"></label>
                        <div class="modal-body ">Are You Sure You Went to Update Stock Order with this Loaction ?</div>
                        <div class="modal-footer" style="text-align: center">
                            <asp:LinkButton ID="lnkUpdateConfirm" runat="server" class="btn btn-danger" OnClick="lnkUpdateConfirm_Click" CausesValidation="false">Update</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="HiddenField2" runat="server" />
            <!--End Danger Modal Templates-->

            <!-- The Modal -->
    <div class="modal" id="myModalpanel">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Update Min Qty</h4>
                    <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="addexcel_start">
                        <ul>
                            <li class="margin_right20">
                                <div class="input-group margin_bottom0">
                                    <input type="text" class="form-control input-lg" disabled placeholder="Upload Excel File" style="width: 220px;">
                                    <span class="input-group-btn">
                                        <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                    </span>
                                </div>
                                <div style="font-size: 12px; padding-top: 3px;"><%--Excel Sheet Format--%></div>
                                <asp:FileUpload ID="FileUpload" runat="server" class="file" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="FileUpload"
                                    ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                    Display="Dynamic" ErrorMessage=".xls, .xlsx only" class="error_text"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                    ControlToValidate="FileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>
                            </li>

                            <li>
                                <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnUpdateMinQty" runat="server"
                                    Text="Update" ValidationGroup="success1" OnClick="btnUpdateMinQty_Click" />
                                <asp:HyperLink ToolTip="Download" ID="hypDownloadExcel" class="pull-right" CssClass="btn btn-blue" 
                                    runat="server" NavigateUrl="~/userfiles/Format/MinQty.xlsx" 
                                    Style="padding-left: 5px; padding-right: 5px; margin-left: 5px;">
                                        <i class="fa fa-download"></i>
                                </asp:HyperLink>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <asp:PostBackTrigger ControlID="btndeliver" />
            <%--    <asp:PostBackTrigger ControlID="btnaddnew" />        --%>
            <%-- <asp:PostBackTrigger ControlID="btnReset" />    --%>
            <%-- <asp:PostBackTrigger ControlID="btnCancel" />  --%>
            <%-- <asp:PostBackTrigger ControlID="btnAdd" />   --%>
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="txtManualOrderNumber" />
            <asp:PostBackTrigger ControlID="ibtnAddVendor" />
            <asp:PostBackTrigger ControlID="ibtnUpdateVendor" />
            <asp:PostBackTrigger ControlID="ibtnAddStock" />
            <%--<asp:PostBackTrigger ControlID="txtBOLReceived" />--%>

            <asp:PostBackTrigger ControlID="btnAdd" />
            <%--<asp:AsyncPostBackTrigger ControlID="ddlVendor" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger ControlID="ddlorderstatusnew" EventName="SelectedIndexChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="ModuleFileUpload" EventName="click" />--%>
            <asp:PostBackTrigger ControlID="chkDeliveryOrderStatus" />
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%--<asp:PostBackTrigger ControlID="btnExcelNew" />--%>
            <asp:PostBackTrigger ControlID="btnUpdateMinQty" />
            <asp:PostBackTrigger ControlID="Gridview1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>