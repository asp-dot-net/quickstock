﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_Outstanding : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            SiteURL = st.siteurl;

            BindFilter();
            //ddlCustomer.SelectedValue = "55321";
            BindGrid(0);
        }
    }

    public void BindFilter()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlCustomer.DataSource = dt;
        ddlCustomer.DataTextField = "Customer";
        ddlCustomer.DataValueField = "CustomerID";
        ddlCustomer.DataBind();
        
        DataTable dt1 = Clstbl_WholesaleOrders.UnReconciledType_Get();
        ddlUnReconciledType.DataSource = dt1;
        ddlUnReconciledType.DataTextField = "ReconciledType";
        ddlUnReconciledType.DataValueField = "id";
        ddlUnReconciledType.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        dt = Clstbl_WholesaleOrders.Sp_OutstandingReport(ddlCustomer.SelectedValue, ddlCreditAmount.SelectedValue, ddlJobType.SelectedValue, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlDelivered.SelectedValue);

        return dt;
    }

    public void BindTotal(DataTable dt)
    {
        if(dt.Rows.Count > 0)
        {
            string CreditAmt = dt.Compute("SUM(CreditAmount)", string.Empty).ToString();
            string TotInvoiceAmt = dt.Compute("SUM(InvoiceAmount)", string.Empty).ToString();
            string xeroTotalAmt = dt.Compute("SUM(xeroTotalAmt)", string.Empty).ToString();
            string xeroInvAmtDue = dt.Compute("SUM(xeroInvAmtDue)", string.Empty).ToString();
            string STCOutRight = dt.Compute("SUM(STCOutRight)", string.Empty).ToString();
            string UnreconciledInvAmt = dt.Compute("SUM(UnreconciledInvAmt)", string.Empty).ToString();
            string STCValue = dt.Compute("SUM(STCValue)", string.Empty).ToString();
            string ActualOutstanding = dt.Compute("SUM(ActualOutstanding)", string.Empty).ToString();
            string ActualXeroOutstanding = dt.Compute("SUM(ActualXeroOutstanding)", string.Empty).ToString();
            string OverDueAmt = dt.Compute("SUM(OverDueAmt)", string.Empty).ToString();
            string OverLimitAmt = dt.Compute("SUM(OverLimitAmt)", string.Empty).ToString();

            lblTotCreditAmt.Text = CreditAmt;
            lblTotInvoiceAmt.Text = TotInvoiceAmt;
            lblTotXeroAmt.Text = xeroTotalAmt;
            lblTotXeroAmtDue.Text = xeroInvAmtDue;
            lblTotSTCOutRight.Text = STCOutRight;
            lblTotUnreconciledInvAmt.Text = UnreconciledInvAmt;
            lblTotSTCValue.Text = STCValue;
            lblTotActualOutstanding.Text = ActualOutstanding;
            //lblTotXeroActualOutstanding.Text = ActualXeroOutstanding;
            lblTotOverdueAmt.Text = OverDueAmt;
            lblTotOverLimitAmt.Text = OverLimitAmt;
        }
    }

    public void BindGrid(int deleteFlag)
    {
        divtot.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "detail")
        {
            string StockOrderID = e.CommandArgument.ToString();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetAdd()
    {
        Notification("There are no items to show in this view");
    }

    public void SetError()
    {
        Notification("Transaction Failed.");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlCustomer.ClearSelection();
        ddlCreditAmount.ClearSelection();
        ddlJobType.ClearSelection();
        ddlDateType.ClearSelection();
        ddlDelivered.ClearSelection();
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            try
            {
                GridViewRow gvrow = e.Row;
                Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
                lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
                int[] page = new int[7];
                page[0] = GridView1.PageIndex - 2;
                page[1] = GridView1.PageIndex - 1;
                page[2] = GridView1.PageIndex;
                page[3] = GridView1.PageIndex + 1;
                page[4] = GridView1.PageIndex + 2;
                page[5] = GridView1.PageIndex + 3;
                page[6] = GridView1.PageIndex + 4;
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > GridView1.PageCount)
                        {
                            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                            lnkbtn.Visible = false;
                        }
                        else
                        {
                            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                            lnkbtn.Text = Convert.ToString(page[i]);
                            lnkbtn.CommandName = "PageNo";
                            lnkbtn.CommandArgument = lnkbtn.Text;

                        }
                    }
                }
                if (GridView1.PageIndex == 0)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                    lnkbtn.Visible = false;

                }
                if (GridView1.PageIndex == GridView1.PageCount - 1)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                    lnkbtn.Visible = false;
                    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                    lnkbtn.Visible = false;

                }
                Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
                if (dv.ToTable().Rows.Count > 0)
                {
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }

                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
                else
                {
                    ltrPage.Text = "";
                }
            }
            catch { }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "Outstanding Report " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

            string[] arrHeader = { "Customer", "Credit Amt", "Invoice Amt", "Xero Amt", "Xero Amt Due", "STC OutRight", "Unreconciled Inv Amt", "STC Value", "STC Remaining", "Actual Outstanding", "Xero Actual Outstanding", "Overdue Amt", "Over Limit Amt", "Pending Inv Days" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnSave_Click(object sender, EventArgs e)
    {
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string InvoiceNo = txtInvoiceNo.Text.Trim();
        string Amount = txtAmount.Text;
        string rec = ddlUnReconciledType.SelectedValue;

        int id = Clstbl_WholesaleOrders.tbl_UnreconciledInvAmt_Insert(createdBy, createdOn);
        if (id > 0)
        {
            bool update = Clstbl_WholesaleOrders.tbl_UnreconciledInvAmt_Update(id.ToString(), InvoiceNo, Amount);
            bool update1 = Clstbl_WholesaleOrders.tbl_UnreconciledInvAmt_Update_Type(id.ToString(), rec);

            SetAdd1();
            txtInvoiceNo.Text = string.Empty;
            txtAmount.Text = string.Empty;
            BindGrid(0);
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        txtInvoiceNo.Text = string.Empty;
        txtAmount.Text = string.Empty;
        ddlUnReconciledType.ClearSelection();
        ModalPopupExtenderAmount.Show();
    }
}