﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="XeroAmountDifference.aspx.cs" Inherits="admin_adminfiles_stock_XeroAmountDifference"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script src="<%=SiteURL%>admin/theme/assets/js/select2.js"></script>
            <%--    <script src="<%=SiteURL%>admin/vendor/jquery/dist/jquery.min.js"></script>--%>


            <script language="javascript" type="text/javascript">
                function WaterMark(txtName, event) {
                    var defaultText = "Enter Username Here";
                }
            </script>
            <script>      
                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });
                });



                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');

                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });
                }

                function pageLoadedpro() {
                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });




                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();

                    //$('.datetimepicker1').datetimepicker({
                    //    format: 'DD/MM/YYYY'
                    //});


                }

                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }

            </script>
            <style>
                .txt_height {
                    height: 100px;
                }

                .tooltip {
                    min-width: 200px;
                }

                .braktext {
                    white-space: normal !important;
                }
            </style>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();

                    //$(".SearcStatus .dropdown dt a").on('click', function () {
                    //        $(".Location .dropdown dd ul").slideToggle('fast');
                    //    });
                    //    $(".SearcStatus .dropdown dd ul li a").on('click', function () {
                    //        $(".Location .dropdown dd ul").hide();
                    //    });

                    //    $(document).bind('click', function (e) {
                    //        var $clicked = $(e.target);
                    //        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    //});

                    // $(".dropdown dt a").on('click', function () {
                    //    $(".dropdown dd ul").slideToggle('fast');
                    //});

                    //$(".dropdown dd ul li a").on('click', function () {
                    //    $(".dropdown dd ul").hide();
                    //});
                    //$(document).bind('click', function (e) {
                    //    var $clicked = $(e.target);
                    //    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    //});
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(".myval").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $(".myvalemployee").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });

                    $(".myvalproject").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });


                }
            </script>

            <div class="page-body headertopbox">
                <div class="card">
                    <div class="card-block">
                        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Xero Amount Difference
                    <asp:Label runat="server" ID="lblStockItem" />
                            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <%--  <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>
                            </div>
                            <h5></h5>
                        </h5>

                    </div>
                </div>

            </div>

            <div class="page-body padtopzero minheight500">
                <asp:Panel runat="server" ID="PanGridSearch" class="">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">

                                                <div class="input-group col-sm-2 max_width170" runat="server">
                                                    <asp:TextBox ID="txtInvoiceNo" placeholder="Invoice No" runat="server" class="form-control"></asp:TextBox>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlCustomer" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlCompanyLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Locations</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlDiff" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Diff</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Order Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Delivery Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">

                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click1"
                                                    CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </asp:Panel>

                <div class="finalgrid">
                    <asp:Panel ID="panel" runat="server" >                  
                    <div class="page-header card" id="DivTotal" runat="server">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <style>
                                    .headertable{
                                        font-weight:bold;
                                    }
                                </style>

                                <table>
                                    <tr>
                                        <td class="headertable">Invoice Amt</td><td class="headertable">: </td>
                                        <td>&nbsp;<asp:Literal ID="lblTotalInvoiceAmt" runat="server"></asp:Literal></td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td class="headertable">Xero Amt</td><td class="headertable">: </td>
                                        <td>&nbsp;<asp:Literal ID="lblTotalXeroAmt" runat="server"></asp:Literal></td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td class="headertable">Total Diff</td><td class="headertable">: </td>
                                        <td>&nbsp;<asp:Literal ID="lblAmtDiff" runat="server"></asp:Literal></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="table-responsive  BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="InvoiceNo" runat="server" CssClass="tooltip-demo text-center table GridviewScrollItem table-bordered Gridview"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" OnRowUpdating="GridView1_RowUpdating"
                                                OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="InvoiceNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Eval("InvoiceNo") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Invoice Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="DateOrdered">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDateOrdered" runat="server" Width="100px"> <%#Eval("DateOrdered") %></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompanyLocation" runat="server"><%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                        SortExpression="Customer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCustomer" runat="server" data-placement="top" data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip"
                                                                Width="170px"><%#Eval("Customer")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amt" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                        SortExpression="InvoiceAmount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInvoiceAmount" runat="server"><%#Eval("InvoiceAmount")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Xero Amt" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                        SortExpression="xeroTotalAmt">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblxeroTotalAmt" runat="server"><%#Eval("xeroTotalAmt")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Diff" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                        SortExpression="AmtDiff">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAmtDiff" runat="server"><%#Eval("AmtDiff")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="HypEdit" runat="server" CssClass="btn btn-primary btn-mini" style="color: WHITE;"
                                                                Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/stock/Wholesale.aspx?Mode=Edit&OrderID=" + Eval("WholesaleOrderID") %>'><i class="fa fa-edit"></i>Edit
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        $(document).ready(function () {
            //$('.js-example-basic-multiple').select2();


        });

        // For Multi Select //
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>

</asp:Content>
