﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_ServiceOrder : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static int AddButtonDisable = 0;
    protected string mode = "";
    protected string SiteURL;
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    static DataView dv;
    HiddenField hdn1 = new HiddenField();
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindLocation();
            BindVendor();
            BindPurchaseCompany();
            BindTransCompany();

            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")))
            //{
            //    btnUpdateOrderStatus.Visible = true;
            //}
            //else
            //{
            //    btnUpdateOrderStatus.Visible = false;

            //}

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Installation Manager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else if (Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Wholesale"))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //  GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }

            //txtSearchOrderNo.Text = "3760";
            BindGrid(0);
        }

        ModeAddUpdate();
    }

    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlStockLocation.DataSource = dt;

        ddlStockLocation.DataTextField = "location";
        ddlStockLocation.DataValueField = "CompanyLocationID";
        //ListItem lst = new ListItem("Unknown", "1500");
        //ddlStockLocation.Items.Insert(ddlStockLocation.Items.Count -1, lst);
        ddlStockLocation.DataBind();

        ddlStockLocation.SelectedValue = "8";

        rptLocation.DataSource = dt;
        rptLocation.DataBind();

        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();
    }

    public void BindTransCompany()
    {
        DataTable dt = ClstblStockOrders.tbl_Transport_CompMaster_GetData();
        ddlTransCompName.DataSource = dt;
        ddlTransCompName.DataValueField = "Trans_Comp_Id";
        ddlTransCompName.DataTextField = "Trans_Company_Name";
        ddlTransCompName.DataBind();

        ddlSearchTransCompName.DataSource = dt;
        ddlSearchTransCompName.DataValueField = "Trans_Comp_Id";
        ddlSearchTransCompName.DataTextField = "Trans_Company_Name";
        ddlSearchTransCompName.DataBind();
    }

    public void BindPurchaseCompany()
    {
        DataTable dt = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        ddlpurchasecompany.DataSource = dt;
        ddlpurchasecompany.DataTextField = "PurchaseCompanyName";
        ddlpurchasecompany.DataValueField = "Id";
        ddlpurchasecompany.DataBind();

        //ddlpurchasecompanysearch.DataSource = dt;
        //ddlpurchasecompanysearch.DataTextField = "PurchaseCompanyName";
        //ddlpurchasecompanysearch.DataValueField = "Id";
        //ddlpurchasecompanysearch.DataBind();

        rptCompany.DataSource = dt;
        rptCompany.DataBind();


    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();
        ddlVendor.DataSource = dt;
        ddlVendor.DataTextField = "Customer";
        ddlVendor.DataValueField = "CustomerID";
        ddlVendor.DataBind();

        //ddlSearchVendor.DataSource = dt;
        //ddlSearchVendor.DataTextField = "Customer";
        //ddlSearchVendor.DataValueField = "CustomerID";
        //ddlSearchVendor.DataBind();

        //ListItem lst = new ListItem("Unknown", "1500");
        //ddlloc.Items.Insert(ddlloc.Items.Count - 1, lst);
        //ddlloc.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddlloc.DataTextField = "location";
        //ddlloc.DataValueField = "CompanyLocationID";
        //ddlloc.DataMember = "CompanyLocationID";
        //ddlloc.DataBind();

        rptVender.DataSource = dt;
        rptVender.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        #region getCheckbox Item
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }

        string PCompany = "";
        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                PCompany += "," + hdnModule.Value.ToString();
            }
        }
        if (PCompany != "")
        {
            PCompany = PCompany.Substring(1);
        }

        string Vender = "";
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Vender += "," + hdnModule.Value.ToString();
            }
        }
        if (Vender != "")
        {
            Vender = Vender.Substring(1);
        }
        #endregion

        dt = ClstblStockOrders.tblStockOrders_SelectBySearchNewQuickStock(ddlShow.SelectedValue, ddlDue.SelectedValue, Vender, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), Location, "", txtstockitemfilter.Text, ddlLocalInternation.SelectedValue, txtStockContainerFilter.Text, PCompany, ddlitemnamesearch.SelectedValue, ddlDelevery.SelectedValue, ddlStockOrderStatus.SelectedValue, ddlPaymentMethod.SelectedValue, ddlSearchTransCompName.SelectedValue, ddlPartial.SelectedValue, ddlSearchGSTType.SelectedValue, "3");

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view.");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + (dt.Rows.Count) + " entries";
                }
            }

        }
        bind(dt);
    }

    public void bind(DataTable dt)
    {
        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockOrderID"].ToString()))
            {
                PanGrid.Visible = false;
                divnopage.Visible = false;
                Notification("There are no items to show in this view.");
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int exist = ClstblStockOrders.tblStockOrders_Exists_VendorInvoiceNo(txtVendorInvoiceNo.Text);
        int existManualNo = ClstblStockOrders.tblStockOrders_Exists_ManualOrderNo(txtManualOrderNumber.Text);

        int existContainerNo = 0;
        if (ddlorderstatusnew.SelectedValue != "0")
        {
            existContainerNo = ClstblStockOrders.tblStockOrders_Exists_StockContainer(txtstockcontainer.Text);
        }

        if (exist == 0)
        {
            //if (existManualNo == 0)
            //{
            if (existContainerNo == 0)
            {
                string StockLocation = ddlStockLocation.SelectedValue.ToString();
                // string StockCategoryID = ddlStockCategoryID.SelectedValue.ToString();

                string DateOrdered = DateTime.Now.AddHours(14).ToString();

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

                string Notes = txtNotes.Text;

                string BOLReceived = "";
                if (txtBOLReceived.Text != "")
                {
                    BOLReceived = Convert.ToDateTime(txtBOLReceived.Text).ToString("yyyy/MM/dd");
                }


                // DateTime BOLReceived = DateTime.ParseExact(txtBOLReceived.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //string BOLReceived = DateTime.ParseExact(txtBOLReceived.Text.ToString(),
                //              "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture).ToShortDateString();
                string vendor = ddlVendor.SelectedValue.ToString();
                string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
                string ManualOrderNumber = txtManualOrderNumber.Text;
                //string purchasecompany = ddlpurchasecompany.SelectedValue.ToString();
                //string itemnameid = ddlitemname.SelectedItem.ToString();
                //string itemnamevalue = ddlitemname.SelectedValue.ToString();
                //string ExpectedDelivery = txtExpectedDelivery.Text;

                string ExpectedDelivery = "";
                if (txtExpectedDelivery.Text != "")
                {
                    ExpectedDelivery = Convert.ToDateTime(txtExpectedDelivery.Text).ToString("yyyy/MM/dd");
                }
                //string ExpectedDelivery=DateTime.ParseExact(txtExpectedDelivery.Text.ToString(),
                //              "yyyy-MM-dd HH:mm:ss.fff",CultureInfo.InvariantCulture).ToShortDateString();
                //DateTime ExpectedDelivery = DateTime.ParseExact(txtExpectedDelivery.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string ArrivalDate = tctconfArrival.Text;
                string transportcompId = ddlTransCompName.SelectedValue;
                string telexreleasedate = tcttelaxrelease.Text;
                string supplierinvoiceno = tctsupplierinvoiceno.Text;
                string totalpi_amt = txtpiamt.Text;
                string transportcompjobno = txttrnasportjobno.Text;
                // string transport_comp_service_invno = txttransportserviceinvoiceno.Text;
                string transport_comp_service_dist_invno = txttransdisbinvoices.Text;
                string pckemailwhdate = tctpckemildatefromwh.Text;
                string storage_charge_amt = txtstrchargeamt.Text;
                string storage_change_Reason = txtstorechargeinvno.Text;

                string extranotes = txtExtraNotes.Text;
                //string transcompdate =;
                string TransCompSentdate = txttranscompdate.Text;
                string storage_charge_invno = txtstorechargeinvno.Text;
                string pmtDueDate = txtpmtduedate.Text;
                //string Notes =;
                var vals = "";
                var vals1 = "";

                if (ddlStockLocation.SelectedValue != string.Empty)
                {
                    vals = StockLocation_Text.Split(':')[0];
                    vals1 = StockLocation_Text.Split(':')[1];
                }

                string state = Convert.ToString(vals);
                //string CompanyLocation = Convert.ToString(vals1).Substring(1);
                //int OrderStatus = 0;
                //if (chkDeliveryOrderStatus.Checked == true)
                //{
                //    OrderStatus = 1;
                //}

                int SucNewFields = ClstblStockOrders.tblStockOrders_update_NewFields(ArrivalDate, transportcompId, telexreleasedate, supplierinvoiceno, totalpi_amt, transportcompjobno, "", transport_comp_service_dist_invno, pckemailwhdate, storage_charge_amt, storage_change_Reason, extranotes, Notes, TransCompSentdate, storage_charge_invno);
                if (SucNewFields > 0)
                {
                    bool s1 = ClstblStockOrders.tblStockOrders_Update_userid(Convert.ToString(SucNewFields), userid);
                    bool scl = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(Convert.ToString(SucNewFields), pmtDueDate);
                    if (ddlOrdStatus.SelectedValue != null && ddlOrdStatus.SelectedValue != "")
                    {
                        bool ord = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(Convert.ToString(SucNewFields), ddlOrdStatus.SelectedValue);
                    }
                }

                SttblStockOrders st1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(Convert.ToString(SucNewFields));
                if (fptranporttaxinv.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("TransportTaxInvoice", st1.transport_comp_service_invno);
                    string map = SucNewFields + fptranporttaxinv.FileName;
                    bool s1 = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoice(Convert.ToString(SucNewFields), map);
                    ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportTaxInvoice\\") + map);
                    SiteConfiguration.UploadPDFFile("TransportTaxInvoice", map);
                    SiteConfiguration.deleteimage(map, "TransportTaxInvoice");

                }
                //else
                //{
                //    SiteConfiguration.DeletePDFFile("TransportTaxInvoice", st1.transport_comp_service_invno);
                //    bool s1 = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoice(Convert.ToString(SucNewFields), "");
                //}


                if (ModuleFileUpload.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("arrivaldatedoc", st1.arrivaldoc);
                    string map = SucNewFields + ModuleFileUpload.FileName;
                    bool s1 = ClstblStockOrders.tblStockOrders_UpdateArriveDoc(Convert.ToString(SucNewFields), map);
                    ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\arrivaldatedoc\\") + map);
                    SiteConfiguration.UploadPDFFile("arrivaldatedoc", map);
                    SiteConfiguration.deleteimage(map, "arrivaldatedoc");
                    bool IsPaperWorkUpload = ClstblStockOrders.tblstockorders_Upload_IspaperUpload(Convert.ToString(SucNewFields), "1");
                }
                else
                {
                    SiteConfiguration.DeletePDFFile("arrivaldatedoc", st1.arrivaldatedoc);
                    bool s1 = ClstblStockOrders.tblStockOrders_UpdateArriveDoc(Convert.ToString(SucNewFields), "");
                    bool IsPaperWorkUpload = ClstblStockOrders.tblstockorders_Upload_IspaperUpload(Convert.ToString(SucNewFields), "0");
                }

                if (fptelexupload.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("TelexReleasedoc", st1.TelexReleasedoc);
                    string map = SucNewFields + fptelexupload.FileName;
                    bool s1 = ClstblStockOrders.tblStockOrders_TelexReleasedoc(Convert.ToString(SucNewFields), map);
                    fptelexupload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TelexReleasedoc\\") + map);
                    SiteConfiguration.UploadPDFFile("TelexReleasedoc", map);
                    SiteConfiguration.deleteimage(map, "TelexReleasedoc");
                    bool IsTelexUpload = ClstblStockOrders.tblstockorders_UpdateTelexFlag(Convert.ToString(SucNewFields), "1");

                }
                else
                {
                    SiteConfiguration.DeletePDFFile("TelexReleasedoc", st1.TelexReleasedoc);
                    bool s1 = ClstblStockOrders.tblStockOrders_TelexReleasedoc(Convert.ToString(SucNewFields), "");
                    bool IsTelexUpload = ClstblStockOrders.tblstockorders_UpdateTelexFlag(Convert.ToString(SucNewFields), "0");
                }

                if (fptransdisbinvoices.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("TransportDistInvoice", st1.transport_comp_service_dist_doc);
                    string map = SucNewFields + fptransdisbinvoices.FileName;
                    bool s1 = ClstblStockOrders.tblStockOrders_TransportDistInvoice(Convert.ToString(SucNewFields), map);
                    fptransdisbinvoices.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportDistInvoice\\") + map);
                    SiteConfiguration.UploadPDFFile("TransportDistInvoice", map);
                    SiteConfiguration.deleteimage(map, "TransportDistInvoice");
                }
                else
                {
                    SiteConfiguration.DeletePDFFile("TransportDistInvoice", st1.TransportDistInvoice);
                    bool s1 = ClstblStockOrders.tblStockOrders_TransportDistInvoice(Convert.ToString(SucNewFields), "");
                }

                if (fpstorechargeinvno.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("storagechanrgeinv", st1.storage_charge_invdoc);
                    string map = SucNewFields + ModuleFileUpload.FileName;
                    bool s1 = ClstblStockOrders.tblStockOrders_storagechanrgeinv(Convert.ToString(SucNewFields), map);
                    fpstorechargeinvno.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\storagechanrgeinv\\") + map);
                    SiteConfiguration.UploadPDFFile("storagechanrgeinv", map);
                    SiteConfiguration.deleteimage(map, "storagechanrgeinv");
                }
                else
                {
                    SiteConfiguration.DeletePDFFile("storagechanrgeinv", st1.storage_charge_invdoc);
                    bool s1 = ClstblStockOrders.tblStockOrders_storagechanrgeinv(Convert.ToString(SucNewFields), "");
                }

                bool pmtdate = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(Convert.ToString(SucNewFields), txtpmtduedate.Text);
                bool OrderDate = ClstblStockOrders.tblStockOrders_UpdatepmtOrderedDate(Convert.ToString(SucNewFields), DateOrdered);
                bool Orderby = ClstblStockOrders.tblStockOrders_UpdateOrderedBy(Convert.ToString(SucNewFields), st.EmployeeID);
                bool success = ClstblStockOrders.tblStockOrders_Update(Convert.ToString(SucNewFields), Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
                //int success = ClstblStockOrders.tblStockOrders_Insert(DateOrdered, st.EmployeeID, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
                bool success5 = ClstblStockOrders.tblStockOrders_Update(SucNewFields.ToString(), Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);

                // ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
                bool ordnumber = ClstblStockOrders.tblStockOrders_Update_OrderNumber(SucNewFields.ToString(), SucNewFields.ToString());
                bool veninvno = ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(SucNewFields.ToString(), txtVendorInvoiceNo.Text);
                bool deliverystatus = ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(success.ToString(), ddlorderstatus.SelectedValue);
                bool f = ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(SucNewFields.ToString(), ddlorderstatusnew.SelectedValue);
                bool g = ClstblStockOrders.tblStockOrders_Update_DeliveryOrderStatusNew(SucNewFields.ToString(), ddlorderstatusnew.SelectedValue);
                bool h = ClstblStockOrders.tblStockOrders_Update_StockContainer(SucNewFields.ToString(), txtstockcontainer.Text);
                bool j = ClstblStockOrders.tblStockOrders_Update_PurchaseCompanyID(SucNewFields.ToString(), Convert.ToInt32(ddlpurchasecompany.SelectedValue));
                bool k = ClstblStockOrders.tblStockOrders_Update_ItemName(SucNewFields.ToString(), Convert.ToInt32(ddlitemname.SelectedValue), ddlitemname.SelectedItem.ToString());
                bool y = ClstblStockOrders.tblStockOrders_Update_DeleveryType(SucNewFields.ToString(), Convert.ToInt32(ddldeleverytype.SelectedValue), ddldeleverytype.SelectedItem.ToString());
                bool b = ClstblStockOrders.tblStockOrders_Update_TransCharges(SucNewFields.ToString(), txtcharges.Text);

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    if (item.Visible != false)
                    {
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                        TextBox ExpiryDate = (TextBox)item.FindControl("txtexpdate");
                        TextBox txtmodelNo = (TextBox)item.FindControl("txtmodelno");
                        TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");
                        string StockItem = ddlStockItem.SelectedValue.ToString();
                        string OrderQuantity = txtOrderQuantity.Text;
                        //String Amount = txtAmount.Text;
                        string Amount = "0";
                        if (!String.IsNullOrEmpty(txtAmount.Text))
                        {
                            Amount = txtAmount.Text;
                        }

                        //if (ddlorderstatus.SelectedValue == "0")
                        //{
                        //    int GSTamt = Convert.ToInt32(txtAmount.Text) / 10;
                        //    Amount = Convert.ToString(Convert.ToInt32(txtAmount.Text) + GSTamt);
                        //}
                        string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                        if (!chkdelete.Checked)
                        {
                            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "" && txtAmount.Text != "")
                            {
                                int success1 = ClstblStockOrders.tblStockOrderItems_Insert(Convert.ToString(SucNewFields), ddlStockItem.SelectedValue, OrderQuantity, Amount, StockOrderItem, state);
                                bool s1 = ClstblStockOrders.tblStockOrderItems_updateExpAndMondelNo(Convert.ToString(success1), ExpiryDate.Text, txtmodelNo.Text);

                                bool s2 = ClstblStockOrders.tblStockOrderItems_UpdateUnitPrice(Convert.ToString(success1), txtUnitPrice.Text);
                            }
                        }
                    }
                }

                //Change by Suresh on 01-05-2020
                bool updatePaymentMethod = ClstblStockOrders.tblStockOrders_Update_PaymentMethod(SucNewFields.ToString(), ddlPMethod.SelectedValue);

                bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(SucNewFields.ToString(), txtTargetDate.Text);

                //add Transfer Com Tax Invoice No
                bool UpdateTaxInvNo = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoiceNo(SucNewFields.ToString(), txtTaxinvoiceNo.Text);

                bool UpdateTaxInvAmt = ClstblStockOrders.tblStockOrders_Update_Inv_InvDist_Amount(SucNewFields.ToString(), txtTaxinvoiceAmount.Text, txttransdisbinvoicesAmount.Text);

                bool UpdateVendorDisCount = ClstblStockOrders.tblStockOrders_Update_Discount(SucNewFields.ToString(), txtVendorDiscount.Text);

                bool UpdateGSTType = ClstblStockOrders.tblStockOrders_Update_GST_Type(SucNewFields.ToString(), ddlGSTType.SelectedValue);

                // 1) USD 2) AUD
                bool UpdateCurrency = ClstblStockOrders.tblStockOrders_Update_Currency(SucNewFields.ToString(), ddlCurrency.SelectedValue);

                // Update Order For = 1) Retail, 2) Wholesale, 3) Service
                bool UpdateOrderFor = ClstblStockOrders.Update_tblStockOrders_OrderFor(SucNewFields.ToString(), ddlSaveOrderFor.SelectedValue);

                //--- do not chage this code start------
                if (SucNewFields > 0)
                {
                    SetAdd();
                }
                else
                {
                    SetError();
                }
                BindGrid(0);
                // Reset();
                mode = "Add";
                //--- do not chage this code end------
            }
            else
            {
                Notification("Container No. already exist.");
                txtstockcontainer.Text = string.Empty;
                txtstockcontainer.Focus();
            }
            //}
            //else
            //{
            //    Notification("Manual Order No. already exist.");
            //    txtManualOrderNumber.Text = string.Empty;
            //    txtManualOrderNumber.Focus();
            //}
        }
        else
        {
            Notification("Vendor Invoice No. already exist.");
            txtVendorInvoiceNo.Text = string.Empty;
            txtVendorInvoiceNo.Focus();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        int exist = ClstblStockOrders.tblStockOrders_ExistsByIdVendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
        int existManualNo = ClstblStockOrders.tblStockOrders_ExistsByIdManualOrderNo(id1, txtManualOrderNumber.Text);

        int existContainerNo = 0;
        if (ddlorderstatusnew.SelectedValue != "0")
        {
            existContainerNo = ClstblStockOrders.tblStockOrders_ExistsByIdStockContainer(id1, txtstockcontainer.Text);
        }
        SttblStockOrders st1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(Convert.ToString(id1));

        if (exist == 0)
        {
            //if(existManualNo==0)
            //{
            if (existContainerNo == 0)
            {
                DataTable dt = ClstblStockOrders.tblStockOrders_Get_Qty_UploadQty(id1);
                int Qty = 0;
                int UpdateQty = 0;
                if (dt.Rows.Count > 0)
                {
                    Qty = Convert.ToInt32(dt.Rows[0]["Qty"].ToString());
                    UpdateQty = Convert.ToInt32(dt.Rows[0]["UploadSerialCount"].ToString());
                }

                int Flag = 0;
                if (Qty == UpdateQty)
                {
                    if (st1.CompanyLocationID != ddlStockLocation.SelectedValue)
                    {
                        Flag++;
                    }
                }

                if (Flag == 0)
                {
                    string Notes = txtNotes.Text;
                    string BOLReceived = txtBOLReceived.Text;
                    string vendor = ddlVendor.SelectedValue.ToString();
                    string StockLocation = ddlStockLocation.SelectedValue.ToString();
                    string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
                    var vals = StockLocation_Text.Split(':')[0];
                    var vals1 = StockLocation_Text.Split(':')[1];
                    string state = Convert.ToString(vals);
                    string ManualOrderNumber = txtManualOrderNumber.Text;
                    string ExpectedDelivery = txtExpectedDelivery.Text;
                    int OrderStatus = 0;
                    string ArrivalDate = tctconfArrival.Text;
                    string transportcompId = ddlTransCompName.SelectedValue;
                    string telexreleasedate = tcttelaxrelease.Text;
                    string supplierinvoiceno = tctsupplierinvoiceno.Text;
                    string totalpi_amt = txtpiamt.Text;
                    string transportcompjobno = txttrnasportjobno.Text;
                    string transport_comp_service_invno = "";
                    string transport_comp_service_dist_invno = txttransdisbinvoices.Text;
                    string pckemailwhdate = tctpckemildatefromwh.Text;
                    string storage_charge_amt = txtstrchargeamt.Text;
                    string storage_change_Reason = txtstorechargeinvno.Text;

                    string extranotes = txtExtraNotes.Text;
                    //string transcompdate =;
                    string TransCompSentdate = txttranscompdate.Text;
                    string storage_charge_invno = txtstorechargeinvno.Text;
                    bool s = ClstblStockOrders.tblStockOrders_updat_NewFields(id1, ArrivalDate, transportcompId, telexreleasedate, supplierinvoiceno, totalpi_amt, transportcompjobno, transport_comp_service_invno, transport_comp_service_dist_invno, pckemailwhdate, storage_charge_amt, storage_change_Reason, extranotes, Notes, TransCompSentdate, storage_charge_invno);
                    //if (chkDeliveryOrderStatus.Checked == true)
                    //{
                    //    OrderStatus = 1;
                    //}
                    if (ddlOrdStatus.SelectedValue != null && ddlOrdStatus.SelectedValue != "")
                    {
                        bool ord = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(id1, ddlOrdStatus.SelectedValue);
                    }
                    bool pmtdate = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(id1, txtpmtduedate.Text);
                    bool success = ClstblStockOrders.tblStockOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
                    ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
                    //ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, Convert.ToInt32(ddlorderstatus.SelectedValue));
                    ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, ddlorderstatusnew.SelectedValue);
                    ClstblStockOrders.tblStockOrders_Update_DeliveryOrderStatusNew(id1, ddlorderstatusnew.SelectedValue);
                    ClstblStockOrders.tblStockOrders_Update_StockContainer(id1, txtstockcontainer.Text);
                    ClstblStockOrders.tblStockOrders_Update_PurchaseCompanyByID(id1, Convert.ToInt32(ddlpurchasecompany.SelectedValue));
                    ClstblStockOrders.tblStockOrders_Update_ItemNameByID(id1, Convert.ToInt32(ddlitemname.SelectedValue), ddlitemname.SelectedItem.ToString());
                    ClstblStockOrders.tblStockOrders_Update_DeleveryType(id1, Convert.ToInt32(ddldeleverytype.SelectedValue), ddldeleverytype.SelectedItem.ToString());
                    ClstblStockOrders.tblStockOrders_Update_TransCharges(id1, txtcharges.Text);
                    if (fptranporttaxinv.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("TransportTaxInvoice ", st1.transport_comp_service_invno);
                        string map = id1 + fptranporttaxinv.FileName;
                        bool s1 = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoice(Convert.ToString(id1), map);
                        ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportTaxInvoice\\") + map);
                        SiteConfiguration.UploadPDFFile("TransportTaxInvoice", map);
                        SiteConfiguration.deleteimage(map, "TransportTaxInvoice");
                    }

                    if (ModuleFileUpload.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("arrivaldatedoc", st1.arrivaldatedoc);
                        string map = id1 + ModuleFileUpload.FileName;
                        bool IsPaperWorkUpload = ClstblStockOrders.tblstockorders_Upload_IspaperUpload(Convert.ToString(id1), "1");
                        bool s1 = ClstblStockOrders.tblStockOrders_UpdateArriveDoc(Convert.ToString(id1), map);
                        ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\arrivaldatedoc\\") + map);
                        SiteConfiguration.UploadPDFFile("arrivaldatedoc", map);
                        SiteConfiguration.deleteimage(map, "arrivaldatedoc");
                    }


                    if (fptelexupload.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("TelexReleasedoc", st1.TelexReleasedoc);
                        string map = id1 + fptelexupload.FileName;
                        bool s1 = ClstblStockOrders.tblStockOrders_TelexReleasedoc(Convert.ToString(id1), map);
                        fptelexupload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TelexReleasedoc\\") + map);
                        SiteConfiguration.UploadPDFFile("TelexReleasedoc", map);
                        SiteConfiguration.deleteimage(map, "TelexReleasedoc");
                        bool IsTelexUpload = ClstblStockOrders.tblstockorders_UpdateTelexFlag(Convert.ToString(id1), "1");
                    }

                    if (fptransdisbinvoices.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("TransportDistInvoice", st1.TransportDistInvoice);
                        string map = id1 + fptransdisbinvoices.FileName;
                        bool s1 = ClstblStockOrders.tblStockOrders_TransportDistInvoice(Convert.ToString(id1), map);
                        fptransdisbinvoices.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportDistInvoice\\") + map);
                        SiteConfiguration.UploadPDFFile("TransportDistInvoice", map);
                        SiteConfiguration.deleteimage(map, "TransportDistInvoice");
                    }

                    if (fpstorechargeinvno.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("storagechanrgeinv", st1.arrivaldoc);
                        string map = id1 + fpstorechargeinvno.FileName;
                        bool s1 = ClstblStockOrders.tblStockOrders_storagechanrgeinv(Convert.ToString(id1), map);
                        fpstorechargeinvno.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\storagechanrgeinv\\") + map);
                        SiteConfiguration.UploadPDFFile("storagechanrgeinv", map);
                        SiteConfiguration.deleteimage(map, "storagechanrgeinv");
                    }

                    ClstblStockOrders.tblStockOrderItems_whole_Delete(id1);
                    foreach (RepeaterItem item in rptattribute.Items)
                    {
                        if (item.Visible != false)
                        {
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
                            string StockItem = ddlStockItem.SelectedValue.ToString();
                            string OrderQuantity = txtOrderQuantity.Text;
                            TextBox ExpiryDate = (TextBox)item.FindControl("txtexpdate");
                            TextBox txtmodelNo = (TextBox)item.FindControl("txtmodelno");
                            TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");
                            string Amount = "0";
                            if (!String.IsNullOrEmpty(txtAmount.Text))
                            {
                                Amount = txtAmount.Text;
                            }

                            //if (ddlorderstatus.SelectedValue == "0")
                            //{
                            //    //int amt = Convert.ToInt32(txtAmount.Text);
                            //    decimal GSTamt = Convert.ToDecimal(txtAmount.Text) / 10;
                            //    Amount = Convert.ToString(Convert.ToDecimal(txtAmount.Text) + GSTamt);
                            //}

                            string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                            CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");

                            if (!chkdelete.Checked)
                            {

                                if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "" && txtAmount.Text != "")
                                {
                                    int success1 = ClstblStockOrders.tblStockOrderItems_Insert(id1, StockItem, OrderQuantity, Amount, StockOrderItem, state);
                                    bool s1 = ClstblStockOrders.tblStockOrderItems_updateExpAndMondelNo(Convert.ToString(success1), ExpiryDate.Text, txtmodelNo.Text);

                                    bool s2 = ClstblStockOrders.tblStockOrderItems_UpdateUnitPrice(Convert.ToString(success1), txtUnitPrice.Text);

                                }
                            }

                            if (chkdelete.Checked)
                            {
                                ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
                            }
                        }
                    }

                    bool updatePaymentMethod = ClstblStockOrders.tblStockOrders_Update_PaymentMethod(id1.ToString(), ddlPMethod.SelectedValue);

                    bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(id1.ToString(), txtTargetDate.Text);

                    //Update Tax Invoice No
                    bool UpdateTaxInvNo = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoiceNo(id1.ToString(), txtTaxinvoiceNo.Text);

                    bool UpdateTaxInvAmt = ClstblStockOrders.tblStockOrders_Update_Inv_InvDist_Amount(id1.ToString(), txtTaxinvoiceAmount.Text, txttransdisbinvoicesAmount.Text);

                    bool UpdateVendorDisCount = ClstblStockOrders.tblStockOrders_Update_Discount(id1.ToString(), txtVendorDiscount.Text);

                    bool UpdateGSTType = ClstblStockOrders.tblStockOrders_Update_GST_Type(id1.ToString(), ddlGSTType.SelectedValue);

                    //1) USD  2) AUD
                    bool UpdateCurrency = ClstblStockOrders.tblStockOrders_Update_Currency(id1.ToString(), ddlCurrency.SelectedValue);

                    // Update Order For = 1) Retail, 2) Wholesale, 3) Service
                    bool UpdateOrderFor = ClstblStockOrders.Update_tblStockOrders_OrderFor(id1.ToString(), ddlSaveOrderFor.SelectedValue);

                    //--- do not chage this code Start------
                    if (success)
                    {
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    //Reset();
                    BindScript();
                    BindGrid(0);
                }
                else
                {
                    //Notification("Select..");

                    ModalPopupExtender4.Show();
                }

            }
            else
            {
                Notification("Container No. already exist.");
                txtstockcontainer.Text = string.Empty;
                txtstockcontainer.Focus();
            }
            //}
            //else
            //{
            //    Notification("Manual Order No. already exist.");
            //    txtManualOrderNumber.Text = string.Empty;
            //    txtManualOrderNumber.Focus();
            //}  
        }
        else
        {
            //InitUpdate();
            // PAnAddress.Visible = true;
            Notification("Vendor Invoice No. already exist.");
            txtVendorInvoiceNo.Text = string.Empty;
            txtVendorInvoiceNo.Focus();
            //txtVendorInvoiceNo.Attributes.Add("onFocus", "DoFocus(this);");
        }

        //Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

        //--- do not chage this code end------
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        AddButtonDisable = 0;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
        mode = "Edit";

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(id);
        try
        {
            ddlorderstatusnew.SelectedValue = st.DeliveryOrderStatusNew;
        }
        catch (Exception esx)
        {

            // throw;
        }
        try
        {
            ddlOrdStatus.SelectedValue = st.StockOrderStatus;
        }
        catch (Exception esxloo)
        {

            // throw;
        }
        try
        {
            ddlStockLocation.SelectedValue = st.CompanyLocationID;
        }
        catch { }
        try
        {
            if (st.CustomerID != null && st.CustomerID != "")
            {
                lnEditVendor.Visible = true;
                btnNewVendor.Visible = false;
                hdnvendor.Value = st.CustomerID;
                ddlVendor.SelectedValue = st.CustomerID;
            }
            else
            {
                btnNewVendor.Visible = true;
                lnEditVendor.Visible = false;

            }

        }
        catch { }


        ddlDate.SelectedValue = st.DeliveryOrderStatus;

        if (st.BOLReceived != null && st.BOLReceived != "")
        {
            txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
        }
        else
        {
            txtBOLReceived.Text = "";
        }

        txtNotes.Text = st.Notes;
        txtManualOrderNumber.Text = st.ManualOrderNumber;

        if (st.ExpectedDelivery != null && st.ExpectedDelivery != "")
        {
            txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
        }
        else
        {
            txtExpectedDelivery.Text = "";
        }


        txtVendorInvoiceNo.Text = st.VendorInvoiceNo;
        ddlorderstatus.SelectedValue = st.DeliveryOrderStatus;
        try
        {
            if (st.DeliveryOrderStatusNew != null && st.DeliveryOrderStatusNew != "")
            {
                ddlorderstatusnew.SelectedValue = st.DeliveryOrderStatusNew;
                if (ddlorderstatusnew.SelectedValue == "0")
                {
                    divint.Visible = false;
                    DivPaperWork.Visible = false;
                    DivTransCompName.Visible = false;
                    Divtranscompdate.Visible = false;
                    divint2.Visible = false;
                    divIntdata3.Visible = false;

                }
                else if (ddlorderstatusnew.SelectedValue == "1")
                {

                    divint.Visible = true;
                    DivPaperWork.Visible = true;
                    DivTransCompName.Visible = true;
                    Divtranscompdate.Visible = true;
                    divint2.Visible = true;
                    divIntdata3.Visible = true;
                }
            }

        }
        catch (Exception ex)
        {

        }

        txtstockcontainer.Text = st.StockContainer;
        hdnStockOrderID2.Value = id;
        hdnActualDElivery2.Value = st.ActualDelivery;
        txtcharges.Text = st.TransCharges;
        try
        {
            ddlitemname.SelectedValue = st.ItemNameId;
        }
        catch (Exception ex) { }
        ddlpurchasecompany.SelectedValue = st.PurchaseCompanyId;
        try
        {
            ddldeleverytype.SelectedValue = st.DeleveryId;
        }
        catch (Exception ex) { }
        if (st.arrivaldate != null && st.arrivaldate != "")
        {
            tctconfArrival.Text = st.arrivaldate;
        }
        if (st.transport_comp_Id != null && st.transport_comp_Id != "")
        {
            ddlTransCompName.SelectedValue = st.transport_comp_Id;

        }
        if (st.transportcompsentdate != null && st.transportcompsentdate != "")
        {
            txttranscompdate.Text = Convert.ToDateTime(st.transportcompsentdate).ToShortDateString();
        }

        if (st.telex_Release_date != null && st.telex_Release_date != "")
        {
            tcttelaxrelease.Text = Convert.ToDateTime(st.telex_Release_date).ToShortDateString();
        }

        if (st.pmtDueDate != null && st.pmtDueDate != "")
        {
            txtpmtduedate.Text = Convert.ToDateTime(st.pmtDueDate).ToShortDateString();
        }

        tctsupplierinvoiceno.Text = st.supplier_invlice_no;
        txtpiamt.Text = st.total_pi_amt;
        txttrnasportjobno.Text = st.transport_comp_jobno;
        // txttransportserviceinvoiceno.Text = st.transport_comp_service_invno;
        txttransdisbinvoices.Text = st.transport_comp_service_dist_invno;
        if (st.pckemailwhdate != null && st.pckemailwhdate != "")
        {
            tctpckemildatefromwh.Text = st.pckemailwhdate;
            //tctpckemildatefromwh.Text = Convert.ToDateTime(st.pckemailwhdate).ToShortDateString();
        }
        txtstrchargeamt.Text = st.storage_charge_amt;
        txtstorechargeinvno.Text = st.storage_charge_invno;
        txtstorechargereason.Text = st.storage_change_Reason;
        txtExtraNotes.Text = st.extranotes;
        if (st.transport_comp_service_invno != null && st.transport_comp_service_invno != "")
        {
            lnkfptranporttaxinv.Visible = true;
            lnkfptranporttaxinv.Text = st.transport_comp_service_invno;
            lnkfptranporttaxinv.NavigateUrl = pdfURL + "TransportTaxInvoice/" + st.transport_comp_service_invno;
        }

        if (st.arrivaldatedoc != null && st.arrivaldatedoc != "")
        {
            lblpaperwork.Visible = true;
            lblpaperwork.Text = st.arrivaldatedoc;
            lblpaperwork.NavigateUrl = pdfURL + "arrivaldatedoc/" + st.arrivaldatedoc;
        }
        //if (st.arrivaldoc != null && st.arrivaldoc != "")
        //{
        //    lblpaperwork.Visible = true;
        //    lblpaperwork.Text = st.arrivaldoc;
        //    lblpaperwork.NavigateUrl = pdfURL  + "arrivaldatedoc/" + st.arrivaldoc;
        //}
        if (st.TelexReleasedoc != null && st.TelexReleasedoc != "")
        {
            lbltelexdoc.Visible = true;
            lbltelexdoc.Text = st.TelexReleasedoc;
            lbltelexdoc.NavigateUrl = pdfURL + "TelexReleasedoc/" + st.TelexReleasedoc;
        }
        if (st.TransportDistInvoice != null && st.TransportDistInvoice != "")
        {
            lbldisttransinv.Visible = true;
            lbldisttransinv.Text = st.TransportDistInvoice;
            lbldisttransinv.NavigateUrl = pdfURL + "TransportDistInvoice/" + st.TransportDistInvoice;
        }
        if (st.storagechanrgeinv != null && st.storagechanrgeinv != "")
        {
            lblstorechargeinv.Visible = true;
            lblstorechargeinv.Text = st.storagechanrgeinv;
            lblstorechargeinv.NavigateUrl = pdfURL + "storagechanrgeinv/" + st.storagechanrgeinv;
        }


        DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(id);
        //if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        {
            if (!string.IsNullOrEmpty(st.ActualDelivery))
            {
                //panel234.Enabled = false;
                //txtNotes.Enabled = false;

                panel234.Enabled = true;
                txtNotes.Enabled = true;
                //ddlStockLocation.Enabled = false;
            }
            else
            {
                panel234.Enabled = true;
                txtNotes.Enabled = true;
            }
        }
        else
        {
            panel234.Enabled = true;
            txtNotes.Enabled = true;
        }

        //Changes By Suresh On 01-05-2020
        DataTable dtPaymentMethod = ClstblStockOrders.tblStockOrders_SelectPaymentMethod_ByStockOrderID(id);
        if (dtPaymentMethod.Rows.Count > 0)
        {
            if (dtPaymentMethod.Rows[0]["PaymentMethodID"].ToString() != "0")
            {
                ddlPMethod.SelectedValue = dtPaymentMethod.Rows[0]["PaymentMethodID"].ToString();
            }
            else
            {
                ddlPMethod.SelectedValue = "";
            }

        }
        else
        {
            ddlPMethod.SelectedValue = "";
        }

        DataTable dtTargetDate = ClstblStockOrders.tblStockOrders_SelectTargetDate_ByStockOrderID(id);
        if (dtTargetDate.Rows.Count > 0)
        {
            if (dtTargetDate.Rows[0]["TargetDate"].ToString() != "")
            {
                txtTargetDate.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(dtTargetDate.Rows[0]["TargetDate"]));
            }
        }





        txtTaxinvoiceNo.Text = st.transport_comp_service_invoiceno;

        txtTaxinvoiceAmount.Text = st.transport_comp_service_invoice_Amount;
        txttransdisbinvoicesAmount.Text = st.transport_comp_service_dist_invno_Amount;

        if (st.VendorDiscount != "")
        {
            txtVendorDiscount.Text = Convert.ToDecimal(st.VendorDiscount).ToString("F");
        }
        else
        {
            txtVendorDiscount.Text = "0.00";
        }

        if (st.GST_Type != "")
        {
            ddlGSTType.SelectedValue = st.GST_Type;
        }
        else
        {
            ddlGSTType.SelectedValue = "";
        }

        if (st.Currency != "")
        {
            ddlCurrency.SelectedValue = st.Currency;
        }
        else
        {
            ddlCurrency.SelectedValue = "";
        }

        txorderno.Text = st.OrderNumber;
        txorderno.ReadOnly = true;
        btnOrderNo.Enabled = false;

        // Changes by Suresh on 11 MAY 2020 // Always Bind Last of all code
        DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(id);
        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;
        }
        if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        {
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Purchase Manager"))
            {
                btnaddnew.Visible = false;
                //foreach (RepeaterItem item1 in rptattribute.Items)
                //{
                //    DropDownList ddlStockCategoryID = (DropDownList)item1.FindControl("ddlStockCategoryID");
                //    DropDownList ddlStockItem = (DropDownList)item1.FindControl("ddlStockItem");
                //    TextBox txtOrderQuantity = (TextBox)item1.FindControl("txtOrderQuantity");
                //    TextBox txtAmount = (TextBox)item1.FindControl("txtAmount");
                //    //Button btnaddnew = (Button)item1.FindControl("btnaddnew");

                //    ddlStockCategoryID.Attributes.Add("disabled", "disabled");
                //    ddlStockItem.Attributes.Add("disabled", "disabled");
                //    txtOrderQuantity.ReadOnly = true;
                //    //txtAmount.ReadOnly = true;
                //}

                for (int i = 0; i < rptattribute.Items.Count; i++)
                {
                    RepeaterItem item = rptattribute.Items[i];
                    string ScannedFlag = dt.Rows[i]["ScannedFlag"].ToString();

                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                    //Button btnaddnew = (Button)item1.FindControl("btnaddnew");

                    if (ScannedFlag == "1")
                    {
                        ddlStockCategoryID.Attributes.Add("disabled", "disabled");
                        ddlStockItem.Attributes.Add("disabled", "disabled");
                        txtOrderQuantity.ReadOnly = true;
                        //txtAmount.ReadOnly = true;
                    }
                    else
                    {
                        ddlStockCategoryID.Attributes.Remove("disabled");
                        ddlStockItem.Attributes.Remove("disabled");
                        txtOrderQuantity.ReadOnly = false;
                        //txtAmount.ReadOnly = false;
                    }

                }
            }
        }
        else
        {
            btnaddnew.Visible = true;
        }

        try
        {
            if (st.OrderFor != string.Empty)
            {
                ddlSaveOrderFor.SelectedValue = st.OrderFor;
            }
            else
            {
                ddlSaveOrderFor.SelectedValue = "";
            }
        }
        catch
        {

        }

        ModeAddUpdate();
        //MaxAttribute = 1;
        //bindrepeater();
        //BindAddedAttribute();
        //--- do not chage this code start------
        //lnkBack.Visible = true;
        InitUpdate();
        //--- do not chage this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        //int index = Convert.ToInt32(e.CommandArgument);
        //GridViewRow row = GridView1.Rows[index];

        //HiddenField hdnField = (HiddenField)row.FindControl("hdnStockOrderID1");
        if (e.CommandName.ToString() == "Delivered")
        {
            //Response.Write("cbv");
            //Response.End();
            ModalPopupdeliver.Show();
            // Button lnkBtn = (Button)e.CommandSource;    // the button
            //GridViewRow myRow = (GridViewRow)lnkBtn.Parent.Parent;  // the row
            //GridView myGrid = (GridView)sender; // the gridview
            //GridViewRow gvr = (GridViewRow)lnkBtn.NamingContainer;
            string ID = e.CommandArgument.ToString();
            hndid.Value = ID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            //hdnStockOrderID
            //int id = (int)GridView1.DataKeys[gvr.RowIndex].Value;
            //string ID = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, stEmployeeid.EmployeeID, "1");
            //SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);
            //if (success)
            //{
            //    DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
            //            ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
            //            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
            //        }
            //    }
            //    SetDelete();
            //}
            //else
            //{
            //    SetError();
            //}
            //BindGrid(0);

        }

        if (e.CommandName == "Revert")
        {
            ModalPopupRevert2.Show();
            string StockOrderID = e.CommandArgument.ToString();
            hndid2.Value = StockOrderID;
        }

        if (e.CommandName == "print")
        {
            string StockOrderID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/order.aspx?id=" + StockOrderID);
        }

        //if (e.CommandName.ToLower() == "detail")
        //{
        //    //ModalPopupExtenderDetail.Show();
        //    string StockOrderID = e.CommandArgument.ToString();

        //    Response.Redirect("~/admin/adminfiles/stock/StockOrderDelivery.aspx?StockOrderID=" + StockOrderID);   
        //}
        if (e.CommandName == "Payment")
        {
            string OrderId = e.CommandArgument.ToString();
            hdnStockOrderID1.Value = OrderId;
            btnSave.Visible = true;
            btnEdit.Visible = false;
            txtpaymentnotes.Text = "";
            txtaud.Text = "";
            DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetDataByStockOrderId(OrderId);
            if (dt != null && dt.Rows.Count > 0)
            {
                GridViewNote.DataSource = dt;
                GridViewNote.DataBind();

            }
            else
            {
                PanGridNotes.Visible = false;
            }
            ModalPopupExtenderNote.Show();

        }

        if (e.CommandName == "ViewUpload")
        {
            string OrderId = e.CommandArgument.ToString();
            if (OrderId != null && OrderId != "")
            {
                SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(OrderId);

                if (st.transport_comp_service_invno != null && st.transport_comp_service_invno != "")
                {
                    transcoinv.Visible = true;
                    transcoinv.Text = st.transport_comp_service_invno;
                    transcoinv.NavigateUrl = pdfURL + "TransportTaxInvoice/" + st.transport_comp_service_invno;
                }
                if (st.arrivaldatedoc != null && st.arrivaldatedoc != "")
                {
                    hplpaperwork.Visible = true;
                    hplpaperwork.Text = st.arrivaldatedoc;
                    hplpaperwork.NavigateUrl = pdfURL + "arrivaldatedoc/" + st.arrivaldatedoc;
                }
                if (st.TelexReleasedoc != null && st.TelexReleasedoc != "")
                {
                    hplTelexUpload.Visible = true;
                    hplTelexUpload.Text = st.TelexReleasedoc;
                    hplTelexUpload.NavigateUrl = pdfURL + "TelexReleasedoc/" + st.TelexReleasedoc;
                }
                if (st.TransportDistInvoice != null && st.TransportDistInvoice != "")
                {
                    hpltransportdistinv.Visible = true;
                    hpltransportdistinv.Text = st.TransportDistInvoice;
                    hpltransportdistinv.NavigateUrl = pdfURL + "TransportDistInvoice/" + st.TransportDistInvoice;
                }
                if (st.storagechanrgeinv != null && st.storagechanrgeinv != "")
                {
                    lblstoragechargeinv.Visible = true;
                    lblstoragechargeinv.Text = st.storagechanrgeinv;
                    lblstoragechargeinv.NavigateUrl = pdfURL + "storagechanrgeinv/" + st.storagechanrgeinv;
                }
            }
            ModalPopupExtender3.Show();
        }

        if (e.CommandName == "ViewPendingQty")
        {
            string StockOrderID = e.CommandArgument.ToString();
            DataTable dt = ClstblStockOrders.tblStckOrderItems_Get_StockItembyOrderID(StockOrderID);

            rptViewDetails.DataSource = dt;
            rptViewDetails.DataBind();

            ModalPopupExtenderViewDetails.Show();
        }

        if (e.CommandName == "Submit")
        {
            string StockOrderID = e.CommandArgument.ToString();
            //DataTable dt = ClstblStockOrders.tblStckOrderItems_Get_StockItembyOrderID(StockOrderID);
            hndOrderID.Value = StockOrderID;
            //rptViewDetails.DataSource = dt;
            //rptViewDetails.DataBind();

            ModalPopupExtenderSubmit.Show();
        }

        if (e.CommandName == "Purchase Order")
        {
            string StockOrderID = e.CommandArgument.ToString();

            Telerik_reports.Generate_PurchaseOrder(StockOrderID);
        }

        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindScript();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        MaxAttribute = 1;
        bindrepeater();
        BindAddedAttribute();
        BindScript();
        PanGridSearch.Visible = false;
        PanGrid.Visible = false;
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Reset();
        ibtnAddVendor.Visible = true;
        ibtnUpdateVendor.Visible = false;
        InitAdd();
        mode = "Add";
        if (mode == "Add")
        {
            string qr = "select ordernumber from tblstockorders order by stockorderid desc";
            DataTable dt = ClstblCustomers.query_execute(qr);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["ordernumber"].ToString() != null && dt.Rows[0]["ordernumber"].ToString() != "")
                {
                    int ordno = Convert.ToInt32(dt.Rows[0]["ordernumber"].ToString());
                    TextBox4.Text = Convert.ToString(ordno + 1);
                }
            }
            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
        }
        PanGrid.Visible = false;
        //  PanGridSearch.Visible = false;
        BindScript();
        ddlStockLocation.SelectedValue = "8";
        ddlSaveOrderFor.SelectedValue = "3";

    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    //public void ExcelFileHeader()
    //{
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunExcelFileHeader();", true);
    //}

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        Notification("Transaction Successful.");
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();

        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        //BindGrid(0);
        ClearAllContent();
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        Notification("Transaction Failed.");
        //PanError.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
        BindGSTType();
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
        if (lblAddUpdate.Text == "Update Stock Order")
        {
            lnkAdd.Visible = false;
            lnkBack.Visible = true;
        }

    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        lbltelexdoc.Text = "";
        lbldisttransinv.Text = "";
        lblstorechargeinv.Text = "";
        lblpaperwork.Text = "";
        lnkfptranporttaxinv.Text = "";
        PanAddUpdate.Visible = true;
        rptattribute.DataSource = null;
        rptattribute.DataBind();
        //rptviewdata.DataSource = null;
        //rptattribute.DataBind();
        ddlStockLocation.ClearSelection();
        ddlVendor.ClearSelection();
        txtBOLReceived.Text = "";
        txtNotes.Text = "";
        txtManualOrderNumber.Text = string.Empty;
        txtExpectedDelivery.Text = string.Empty;
        //rptviewdata.Visible = false;

        ddlPMethod.SelectedValue = "";
        ddlSaveOrderFor.SelectedValue = "";

        ddlstockcategory.ClearSelection();
        //txtstockitem.Text = string.Empty;
        //txtbrand.Text = string.Empty;
        //txtmodel.Text = string.Empty;
        //txtseries.Text = string.Empty;
        //txtminstock.Text = string.Empty;
        //chksalestag.Checked = false;
        //chkisactive.Checked = false;
        //txtdescription.Text = string.Empty;
        //txtStockSize.Text = string.Empty;
        txtVendorInvoiceNo.Text = string.Empty;
        txtstockcontainer.Text = string.Empty;
        ddlorderstatus.SelectedValue = "";
        txtstockitemfilter.Text = string.Empty;
        ddlLocalInternation.SelectedValue = "";
        ddldeleverytype.SelectedValue = "";


        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            txtqty.Text = "0";
        }

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            txtAmount.Text = "0";
            txtOrderQuantity.Text = "";
            ddlStockItem.SelectedValue = "";
            ddlStockCategoryID.SelectedValue = "";
        }

        tctconfArrival.Text = "";
        try
        {
            ddlTransCompName.SelectedValue = "";
        }
        catch (Exception ex)
        { }

        tcttelaxrelease.Text = "";
        tctsupplierinvoiceno.Text = "";
        txtpiamt.Text = "";
        txttrnasportjobno.Text = "";
        //txttransportserviceinvoiceno.Text = "";
        txttransdisbinvoices.Text = "";
        tctpckemildatefromwh.Text = "";
        txtstrchargeamt.Text = "";
        txtstorechargeinvno.Text = "";

        txtExtraNotes.Text = "";
        //string transcompdate =;
        txttranscompdate.Text = "";
        txtstorechargeinvno.Text = "";
        txtpmtduedate.Text = "";
        //txtSearchOrderNo.Text = "";
        txtTargetDate.Text = "";
        //fpR.Dispose();
    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;

        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
            Label72.Visible = false;
            TextBox4.Visible = false;

        }
    }

    protected void ddlStockLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

            ddlStockItem.Items.Clear();

            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            if (ddlStockLocation.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "")
            {

                DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
                ddlStockItem.DataSource = dtStockItem;
                ddlStockItem.DataTextField = "StockItem";
                ddlStockItem.DataValueField = "StockItemID";
                ddlStockItem.DataBind();
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(
        //             PanGridSearch,
        //             this.GetType(),
        //             "MyAction",
        //             "doMyAction();",
        //             true);

        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

        if (ddlStockLocation.SelectedValue != "")
        {
            ddlStockItem.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalAlertExpire.Hide();
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        DateTime CurrentDate = Convert.ToDateTime(DateTime.Now.AddHours(14).ToString("dd/MM/yyyy"));
        DateTime CurrentDate_30Dayplus = CurrentDate.AddDays(30);
        TextBox txtModelName = (TextBox)item.FindControl("txtmodelno");
        TextBox txtExpDate = (TextBox)item.FindControl("txtexpdate");
        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue))
        {
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlStockItem.SelectedValue);
            if (!string.IsNullOrEmpty(st.ExpiryDate))
            {

                DateTime ExpiryDate = Convert.ToDateTime(st.ExpiryDate);
                txtExpDate.Text = ExpiryDate.ToShortDateString();
                if (ExpiryDate <= CurrentDate_30Dayplus)
                {
                    ModalAlertExpire.Show();
                    if (ExpiryDate < CurrentDate)
                    {
                        lblexpire.Text = "Selected Stock Item is already expired on " + ExpiryDate.ToString("dd MMM yyyy") + ".";
                    }
                    else
                    {
                        lblexpire.Text = "Selected Stock Item is going to expire on " + ExpiryDate.ToString("dd MMM yyyy") + ".";
                    }
                }
            }
            if (!string.IsNullOrEmpty(st.StockModel))
            {
                txtModelName.Text = st.StockModel;

            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        //  BindScript();
    }

    protected void btnAddUpdateRow_Click(object sender, EventArgs e)
    {
        InitUpdate();
        AddmoreAttribute();
    }

    protected void rptattribute_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtOrderQuantity");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");
        TextBox txtUnitPrice = (TextBox)e.Item.FindControl("txtUnitPrice");
        TextBox txtAmountGST = (TextBox)e.Item.FindControl("txtAmountGST");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdnStockOrderItemID = (HiddenField)e.Item.FindControl("hdnStockOrderItemID");
        HiddenField hdnmdlno = (HiddenField)e.Item.FindControl("hdnmdlno");
        HiddenField hdnExpiryDate = (HiddenField)e.Item.FindControl("hdnExpiryDate");
        HiddenField hndUnitPrice = (HiddenField)e.Item.FindControl("hndUnitPrice");
        HiddenField hndAmountGST = (HiddenField)e.Item.FindControl("hndAmountGST");

        TextBox txtexpdate = (TextBox)e.Item.FindControl("txtexpdate");
        TextBox txtmodelno = (TextBox)e.Item.FindControl("txtmodelno");

        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(hdnStockCategory.Value, ddlStockLocation.SelectedValue.ToString());
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        DataTable dtStock = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(hdnStockItem.Value, ddlStockLocation.SelectedValue);
        if (dtStock.Rows.Count > 0)
        {
            if (dtStock.Rows[0]["SalesTag"].ToString() == "False")
            {
                ListItem P1 = new ListItem(); // Example List
                P1.Text = dtStock.Rows[0]["StockItem"].ToString();
                P1.Value = hdnStockItem.Value;
                ddlStockItem.Items.Add(P1);
            }
        }

        HiddenField hdnOrderQuantity = (HiddenField)e.Item.FindControl("hdnOrderQuantity");
        HiddenField hdnAmount = (HiddenField)e.Item.FindControl("hndamount");
        Panel PanelRepeater = (Panel)e.Item.FindControl("PanelRepeater");

        if (string.IsNullOrEmpty(hdnAmount.Value))
        {
            hdnAmount.Value = "0";
        }

        if (string.IsNullOrEmpty(hndUnitPrice.Value))
        {
            hndUnitPrice.Value = "0";
        }

        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtOrderQuantity.Text = hdnOrderQuantity.Value;
        txtAmount.Text = hdnAmount.Value;
        string Modelno = hdnmdlno.Value;
        txtmodelno.Text = Modelno;
        txtexpdate.Text = hdnExpiryDate.Value;
        txtUnitPrice.Text = hndUnitPrice.Value;

        decimal Amount = 0;
        decimal AmountGST = 0;
        if (txtAmount.Text != "")
        {
            Amount = Convert.ToDecimal(txtAmount.Text);
            if (ddlCurrency.SelectedValue == "2")
            {
                AmountGST = Amount + (Amount * Convert.ToDecimal("0.10"));
            }
            else
            {
                AmountGST = Amount;
            }
        }

        txtAmountGST.Text = AmountGST.ToString("F");

        btnaddnew.Attributes.Remove("disabled");
        litremove.Attributes.Remove("disabled");

        if (e.Item.ItemIndex == 0)
        {
            //btnDelete.Visible = false;
            chkdelete.Visible = false;
            litremove.Visible = false;
            //RequiredFieldValidator11.Visible = true;
            // RequiredFieldValidator1.Visible = true;
            //   RequiredFieldValidator19.Visible = true;
        }
        else
        {
            //btnDelete.Visible = true;
            chkdelete.Visible = true;
            litremove.Visible = true;

            //RequiredFieldValidator11.Visible = false;
            //RequiredFieldValidator1.Visible = false;
            //  RequiredFieldValidator19.Visible = false;
        }

        try
        {
            DataTable dtstock = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hdnStockOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
            DataTable dtstock2 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(hdnStockOrderID2.Value);
            //SttblStockOrderItems st = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockOrderItemID.Value);
            if (dtstock.Rows.Count == Convert.ToInt32(hdnOrderQuantity.Value))
            {
                //PanelRepeater.Enabled = false;
                AddButtonDisable = AddButtonDisable + 1;
                litremove.Attributes.Add("disabled", "");
                btnaddnew.Attributes.Add("disabled", "");
                // ddlStockCategoryID.Enabled = false;
            }
            else
            {
                PanelRepeater.Enabled = true;
                litremove.Attributes.Remove("disabled");
                btnaddnew.Attributes.Remove("disabled");

                //AddButtonDisable = 0;

                //   ddlStockCategoryID.Enabled = true;
            }

            if (dtstock.Rows.Count > 0)
            {
                litremove.Attributes.Add("disabled", "");
                btnaddnew.Attributes.Add("disabled", "");
            }
            else
            {
                litremove.Attributes.Remove("disabled");
                btnaddnew.Attributes.Remove("disabled");
            }
            //if (dtstock2.Rows.Count > 0)
            //{
            //    litremove.Attributes.Add("disabled", "");
            //    btnaddnew.Attributes.Add("disabled", "");
            //}
            //else
            //{
            //    litremove.Attributes.Remove("disabled");
            //    btnaddnew.Attributes.Remove("disabled");
            //}
        }
        catch { }

        // HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");

        //Response.Write(hdnStockCategory.Value);
        //Response.End();

    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        rpttable.Columns.Add("Amount", Type.GetType("System.String"));
        rpttable.Columns.Add("ModelNo", Type.GetType("System.String"));
        rpttable.Columns.Add("ExpiryDate", Type.GetType("System.String"));
        rpttable.Columns.Add("UnitPrice", Type.GetType("System.String"));
        rpttable.Columns.Add("AmountGST", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");
            TextBox txtAmountGST = (TextBox)item.FindControl("txtAmountGST");

            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdnmdlno = (HiddenField)item.FindControl("hdnmdlno");
            HiddenField hdnExpiryDate = (HiddenField)item.FindControl("hdnExpiryDate");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField hndUnitPrice = (HiddenField)item.FindControl("hndUnitPrice");
            string date = " ";
            string Model = "";
            if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlStockItem.SelectedValue);

                if (st.ExpiryDate != null && st.ExpiryDate != "")
                {
                    DateTime ExpiryDate = Convert.ToDateTime(st.ExpiryDate);
                    date = ExpiryDate.ToShortDateString();
                }
                Model = st.StockModel;
            }
            //TextBox txtexpdate = (TextBox)item.FindControl("txtexpdate");
            //TextBox txtmodelno = (TextBox)item.FindControl("txtmodelno");
            DataRow dr = rpttable.NewRow();
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["OrderQuantity"] = txtOrderQuantity.Text;
            dr["Amount"] = txtAmount.Text;
            dr["StockOrderItemID"] = hdnStockOrderItemID.Value;
            dr["type"] = hdntype.Value;
            dr["ModelNo"] = Model;
            dr["ExpiryDate"] = date;
            dr["UnitPrice"] = txtUnitPrice.Text;
            dr["AmountGST"] = txtAmountGST.Text;

            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
            rpttable.Columns.Add("Amount", Type.GetType("System.String"));
            rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("ModelNo", Type.GetType("System.String"));
            rpttable.Columns.Add("ExpiryDate", Type.GetType("System.String"));
            rpttable.Columns.Add("UnitPrice", Type.GetType("System.String"));
            rpttable.Columns.Add("AmountGST", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["OrderQuantity"] = "";
            dr["Amount"] = "";
            dr["StockOrderItemID"] = "";
            dr["type"] = "";
            dr["ModelNo"] = "";
            dr["ExpiryDate"] = "";
            dr["UnitPrice"] = "";
            dr["AmountGST"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    protected void ibtnaddvendor_click(object sender, EventArgs e)
    {

        if (txtCompany.Text != string.Empty)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string employeeid = st.EmployeeID;

            int success = ClstblCustomers.tblCustomers_Insert("", employeeid, "false", "13", "3", "", "", "", "1", employeeid, "", "", "", "", "", txtCompany.Text, "", "", "", "", "", "", "", "", "", "", "australia", "", "", "", "", "", "", "", "", "", "", "false", "", "", "", "", "", "", "", "");

            int succontacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", txtContFirst.Text.Trim(), txtContLast.Text.Trim(), "", "", employeeid, employeeid);

            int succustinfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "customer entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(succontacts), employeeid, "1");

            if (Convert.ToString(succontacts) != string.Empty)
            {
                bool s1 = ClstblContacts.tblContacts_updateCustomerCredit(Convert.ToString(succontacts), txtCredit.Text);
                txtCredit.Text = "";
            }
            if (Convert.ToString(success) != string.Empty)
            {
                ModalPopupExtenderVendor.Hide();
                BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(success);
                Notification("Transaction Successful.");
            }
            else
            {
                Notification("Transaction Failed.");

            }
        }

    }

    protected void btnNewStock_Click(object sender, EventArgs e)
    {
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        chkDashboard.Checked = false;

        PanAddUpdate.Visible = true;
        ModalPopupExtenderStock.Show();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlstockcategory.Items.Clear();
        ddlstockcategory.Items.Add(item8);

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dtStockCategory;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindScript();
    }

    protected void ibtnAddStock_Click(object sender, EventArgs e)
    {
        InitAdd();
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();

        int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hyplocationid = (HiddenField)item.FindControl("hyplocationid");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            if (txtqty.Text != "" || hyplocationid.Value != "")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
            }
        }
        if (success > 0)
        {
            ModalPopupExtenderStock.Hide();
            Notification("Transaction Successful.");
        }
        else
        {
            ModalPopupExtenderStock.Show();
            Notification("Transaction Failed.");
        }
        BindScript();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btndeliver_Click(object sender, EventArgs e)
    {

        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Section = "";
        string Message = "";
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        //if (receive != string.Empty)
        //{
        //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
        //}

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        //if (success)
        //{
        //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
        //if (dt.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
        //        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
        //        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
        //    }
        //}
        //SetDelete();

        int success1 = 0;
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;

        if (FileUpload1.HasFile)
        {
            // Notification("Transaction Successful.");
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
            string connectionString = "";

            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}
            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            //else if (FileUpload1.FileName.EndsWith(".xlsx"))
            //{
            //    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            //}


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    // string employeeid = string.Empty;
                    //string flag = "true";
                    // SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

                    //DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum(hndid.Value);
                    DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum_ByStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr2 = dt3.Rows[0];
                    string qty = dr2["Qty"].ToString();

                    //DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                    DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr1 = dt2.Rows[0];
                    string stockid = dr1["StockItemID"].ToString();
                    int count = 0;
                    int exist = 0;
                    var ExistSerialNo = new List<string>();
                    List<string> NovalueList = new List<string>();
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        //Response.Write(dr.Depth);
                        //Response.End();
                        int blank = 0;
                        int flagcolNames = 0;

                        if (dr.HasRows)
                        {
                            var columns = new List<string>();
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                columns.Add(dr.GetName(i));
                                if (!string.IsNullOrEmpty(dr.GetName(i)))
                                {
                                    string attributevalue0 = "Serial No";
                                    string attributevalue1 = "Pallet";
                                    int j = 0;
                                    string columnname = dr.GetName(i).ToString();
                                    if (columnname.StartsWith(attributevalue0) == true || columnname.StartsWith(attributevalue1) == true)
                                    {
                                        flagcolNames = 0;
                                    }
                                    else
                                    {
                                        flagcolNames = 1;
                                        break;
                                    }
                                }
                            }

                            if (flagcolNames == 0)
                            {
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";
                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();

                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank = 1;
                                        break;
                                    }
                                    if (blank != 1)
                                    {
                                        exist = ClstblStockOrders.tblStockSerialNo_Exist(SerialNo);
                                        if (exist == 0)
                                        {
                                            count++;
                                        }
                                        else
                                        {
                                            sttblStockSerialNo stitem = ClstblStockOrders.tblStockSerialNo_Getdata_BySerialNo(SerialNo);
                                            //blank = 1;
                                            ExistSerialNo.Add(stitem.SerialNo.ToString());
                                            NovalueList.Add("exist");
                                            //  break;
                                        }

                                    }
                                }
                            }
                            else
                            {
                                //ExcelFileHeader();
                            }
                            //Response.Write();
                        }
                    }
                    //Response.Write(count);
                    //Response.End();
                    if ((NovalueList != null) && (!NovalueList.Any()))
                    {
                        PanSerialNo.Visible = false;
                        if ((count == Convert.ToInt32(qty)) && (!NovalueList.Any()))
                        {

                            using (DbDataReader dr = command.ExecuteReader())
                            {

                                if (dr.HasRows)
                                {
                                    // Notification("Transaction Successful.");

                                    //if (flag == "false")
                                    //{
                                    //    PanEmpty.Visible = true;
                                    //}
                                    //else
                                    //{


                                    while (dr.Read())
                                    {

                                        int blank2 = 0;
                                        string SerialNo = "";
                                        string Pallet = "";

                                        SerialNo = dr["Serial No"].ToString();
                                        Pallet = dr["Pallet"].ToString();
                                        if (SerialNo == "" && Pallet == "")
                                        {
                                            blank2 = 1;
                                        }
                                        if (blank2 != 1)
                                        {
                                            if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                            {
                                                //try
                                                //{

                                                //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                                // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                                //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                                //Response.End();
                                                // success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);

                                                string StockOrderID = hndid.Value;
                                                Section = "Stock Order";
                                                Message = "Stock In for Order Number:" + StockOrderID;

                                                //success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID);
                                                success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID_ForPortal(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID, "");
                                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, StockOrderID, SerialNo, Section, Message, Currendate);
                                                //}
                                                //catch { }
                                            }
                                            if (success1 > 0)
                                            {
                                                Notification("Transaction Successful.");
                                            }
                                            else
                                            {
                                                Notification("Transaction Failed.");
                                            }
                                        }
                                        //}
                                    }
                                }
                            }
                            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
                            if (success)
                            {
                                if (receive != string.Empty)
                                {
                                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                                }

                                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                SetError();
                            }


                        }
                        else
                        {
                            //if (exist == 0)
                            //{
                            Notification("Quantity and No. of excel record didn't match.");
                            Notification("Quantity and No. of excel record didn't match.");
                            // }
                            //if (exist == 1)
                            //{
                            //    SerialNoExist();
                            //}
                        }
                    }
                    else
                    {
                        PanSerialNo.Visible = true;
                        int sizeOfList = ExistSerialNo.Count;
                        List<string> NovalueListmsg = new List<string>();
                        for (int i = 0; i < sizeOfList; i++)
                        {
                            string Something = "Serial No: " + Convert.ToString(ExistSerialNo[i]) + ",<br/>";
                            string error = Something.TrimEnd(',');
                            NovalueListmsg.Add(Something);


                            //Response.Write(Convert.ToString(orderList[i]) + "<br \\");
                        }
                        ltrerror.Text = string.Join(",", NovalueListmsg).TrimEnd(',') + "Are already Exists";
                    }

                }
            }
        }
        else
        {
            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
            if (success)
            {
                if (receive != string.Empty)
                {
                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                }

                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                    }
                }
                Notification("Transaction Successful.");
            }
            else
            {
                SetError();
            }

        }

        //}
        //else
        //{
        //    SetError();
        //}
        txtdatereceived.Text = string.Empty;

        // txtserialno.Text = string.Empty;


        BindGrid(0);
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnNewVendor_Click1(object sender, EventArgs e)
    {

        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        txtCompany.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        BindScript();
        ModalPopupExtenderVendor.Show();
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ClearCheckBox();
        ClearAllContent();
    }

    protected void ClearAllContent()
    {
        ddlShow.SelectedValue = "False";
        //ddlpurchasecompanysearch.SelectedValue = "";
        ddlitemnamesearch.SelectedValue = "";
        ddlDue.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        //ddlloc.SelectedValue = "";
        txtstockitemfilter.Text = string.Empty;
        ddlLocalInternation.SelectedValue = "";
        txtStockContainerFilter.Text = string.Empty;
        ddlDelevery.SelectedValue = "";
        ddlSearchTransCompName.SelectedValue = "";
        ddlPartial.SelectedValue = "2";
        ddlOrderFor.SelectedValue = "";

        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }

            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void btnDelivered_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblStockOrders.tblStockOrders_Update_Cancelled(id, Convert.ToString(true));
        ClstblStockOrders.tblStockOrderItems_DeleteStockOrderID(id);
        ClstblStockOrders.tblStockOrders_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
            Notification("Transaction Successful.");
            //PanSuccess.Visible = true;
        }
        else
        {
            Notification("Transaction Failed.");
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnStockOrderID = (HiddenField)e.Row.FindControl("hdnStockOrderID");
            string StockOrderID = hdnStockOrderID.Value;
            Label lblAmount = (Label)e.Row.FindControl("lblAmount");
            Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            Label lblqty = (Label)e.Row.FindControl("Label8");
            HyperLink hypDetail = (HyperLink)e.Row.FindControl("hypDetail");
            HyperLink lnkExcelUploded = (HyperLink)e.Row.FindControl("lnkExcelUploded");
            HyperLink lnkExcelPending = (HyperLink)e.Row.FindControl("lnkExcelPending");

            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            Image imgdiv = (Image)e.Row.FindControl("imgdiv");

            HiddenField Actualdelivery = (HiddenField)e.Row.FindControl("hdnactualdelivery");

            if (!string.IsNullOrEmpty(StockOrderID))
            {
                HiddenField hdnactualdelivery = (HiddenField)e.Row.FindControl("hdnactualdelivery");

                if (!string.IsNullOrEmpty(hdnactualdelivery.Value))
                {
                    //Newly Added Condition on 11-May-2020
                    if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Purchase Manager"))
                    {
                        gvbtnUpdate.Visible = true;
                    }
                    else
                    {
                        gvbtnUpdate.Visible = false;
                    }

                }
                else
                {

                    gvbtnUpdate.Visible = true;
                }
            }
        }
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int CategoryID;
        int OrderQuantity;
        HiddenField hfStockOrderID = (HiddenField)e.Item.FindControl("hfStockOrderID");
        HiddenField hfStockItemID = (HiddenField)e.Item.FindControl("hfStockItemID");
        HiddenField StockCategoryID = (HiddenField)e.Item.FindControl("hfStockCategoryID");
        LinkButton btnDelivery = (LinkButton)e.Item.FindControl("btnDelivery");
        ImageButton checkimag = (ImageButton)e.Item.FindControl("checkimag");
        Label lblOrderQuantity = (Label)e.Item.FindControl("lblOrderQuantity");
        DataTable dtGetData = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(Convert.ToString(hfStockOrderID.Value));
        //1 For Module 2 For Inveerter
        if (dtGetData.Rows.Count > 0)
        {
            foreach (DataRow dr in dtGetData.Rows)
            {
                int DeliveryOrderStatus = Convert.ToInt32(dr["DeliveryOrderStatus"].ToString());
                if (DeliveryOrderStatus == 1)
                {
                    // divdelivery.Visible = true;
                    if (Convert.ToInt32(StockCategoryID.Value) == 1)
                    {

                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 1)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }

                    }
                    else if (Convert.ToInt32(StockCategoryID.Value) == 2)
                    {
                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 2)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // divdelivery.Visible = false;
                    btnDelivery.Visible = false;
                }
            }
        }
    }

    protected void rptOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delivered")
        {
            ModalPopupdeliver.Show();
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string StockOrderID = commandArgs[0];
            string StockOrderItemID = commandArgs[1];
            hndStockOrderItemID.Value = StockOrderItemID;


            hndid.Value = StockOrderID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        // DataTable dt = ClstblStockOrders.tblStockOrders_SelectBySearchExcel(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), ddlloc.SelectedValue, "", txtstockitemfilter.Text, ddlLocalInternation.SelectedValue, txtStockContainerFilter.Text);
        //Response.Clear();
        //dt.Constraints = false;
        int count = dt.Rows.Count;
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "StockOrder_New" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = {   1, 2, 3, 4,
                                5, 6, 43, 7, 46, 8,
                                9, 10, 11, 40, 12,
                                5, 17, 18, 19,

                                33, 44, 13, 14, 16, 45,

                                20, 21, 22,
                                23, 24, 25,
                                26, 27,
                                28, 29, 30, 31
                             };

            string[] arrHeader = { "OrderNumber","StockOrderStatus", "InvoiceNumber","PCompany",                                                                                "CompanyLocation","OrderedDate", "TETA", "ETA", "WareHouseDate", "ManualOrderNo",
                                    "ContainerNo","ItemsOrdered", "Vendor", "PaymentStatus", "PaymentMethod",
                                    "StockFor", "DeliveryDate", "DeliveredBy", "DeliveryType",

                                     "OrderType", "GST Type", "TotalOrderQty", "PendingQty", "Amount", "Partial",

                                    "ComfirmArrivalDate", "TransoprtCoSentDate", "TelexReleaseDate",
                                    "SupplierInvoiceNo", "TotalPIAmt","TransCosJobNo.",
                                    "TransCosServiceInv", "TransCosDISBServiceInv",
                                    "PickupEmailDateFromWH", "StorageChargeAmount", "StoreChargeInvoiveNo", "ReasonForStoreCharge"

                };

            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            //}
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void btnOK3_Click(object sender, EventArgs e)
    {
        string Section = "";
        string Message = "";
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string ID = hndid2.Value;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, "", "0");
        bool success2 = ClstblStockOrders.tblStockOrders_Update_ExcelUploaded(ID, "false");
        ClstblStockOrders.tblStockOrders_Update_ActualDelivery(ID, "");

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);

        DataTable dtQty = ClstblStockOrders.tblStockOrderItems_SelectQty(ID);
        string OrderQuantity = "";
        string StockOrderID = "";
        string StockItemID = "";
        if (dtQty.Rows.Count > 0)
        {
            OrderQuantity = dtQty.Rows[0]["OrderQuantity"].ToString();
            StockOrderID = dtQty.Rows[0]["StockOrderID"].ToString();
            StockItemID = dtQty.Rows[0]["StockItemID"].ToString();
        }
        if (success)
        {
            DataTable dt1 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(ID);
            if (dt1.Rows.Count > 0)
            {
                string SerialNo = "";
                foreach (DataRow dtserial in dt1.Rows)
                {
                    SerialNo = dtserial["SerialNo"].ToString();
                    bool sucess = ClstblMaintainHistory.tblMaintainHistory_Delete_BySerailNo(SerialNo);
                }
            }

            ClstblStockOrders.tblStockSerialNo_Delete(ID);
            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //Response.Write(row["OrderQuantity"].ToString() + "==" + row["StockItemID"].ToString());
                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    ClstblStockOrders.tblStockOrderItems_Update_ItemDeliveredOnly(row["StockItemID"].ToString(), "0");
                    //Response.Write(StOldQty.StockQuantity);
                    //Response.End();
                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, (0 - Convert.ToInt32(row["OrderQuantity"].ToString())).ToString(), userid, "0");
                    ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                }
                //Response.End();
            }
            SetDelete();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
    }

    protected void lnkpurchasecompany_Click(object sender, EventArgs e)
    {

    }

    protected void txtOrderQuantity_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];

        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
        TextBox txtAmountGST = (TextBox)item.FindControl("txtAmountGST");

        HiddenField hdnStockCategory = (HiddenField)item.FindControl("hdnStockCategory");
        HiddenField hdnStockItem = (HiddenField)item.FindControl("hdnStockItem");

        if (!string.IsNullOrEmpty(txtOrderQuantity.Text) && (!string.IsNullOrEmpty(hdnStockOrderID2.Value)))
        {
            DataTable dtstock = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hdnStockOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);

            if (Convert.ToInt32(txtOrderQuantity.Text) < dtstock.Rows.Count)
            {
                Notification("Enter more then " + dtstock.Rows.Count + " Quantity");

            }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;


        try
        {
            TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");

            decimal Amount = 0;
            decimal AmountGST = 0;
            if (txtOrderQuantity.Text != "" && txtUnitPrice.Text != "")
            {
                Amount = Convert.ToDecimal(txtOrderQuantity.Text) * Convert.ToDecimal(txtUnitPrice.Text);

                if (ddlCurrency.SelectedValue == "2")
                {
                    AmountGST = Amount + (Amount * Convert.ToDecimal("0.10"));
                }
                else
                {
                    AmountGST = Amount;
                }
            }

            txtAmount.Text = Amount.ToString("F");
            txtAmountGST.Text = AmountGST.ToString("F");
        }
        catch (Exception ex)
        {
            string Msg = "Error: " + ex.Message;
            Notification(Msg);
        }
    }

    protected void btnNewVendor_Click(object sender, EventArgs e)
    {

    }

    protected void btnSavePmt_Click(object sender, EventArgs e)
    {

    }

    protected void GridViewPmt_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void GridViewPmt_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string id1 = hdnStockOrderID1.Value;
        if (id1 != null && id1 != "")
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            int s1 = ClstblStockOrders.Tblstockorder_PaymentDetail_Insert(id1, tctdepodate.Text, txtRate.Text, txtAmountUsd.Text, txtaud.Text, userid);
            if (s1 > 0)
            {
                bool pmtNo = ClstblStockOrders.Tblstockorder_PaymentDetail_updatepaymentNotes(Convert.ToString((s1)), txtpaymentnotes.Text);
                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(Convert.ToString((s1)));
                if (dt != null && dt.Rows.Count > 0)
                {
                    PanGridNotes.Visible = true;
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();

                }
                else
                {
                    PanGridNotes.Visible = false;
                    Notification("No data Found..");
                }
                SetAdd();

            }
            else
            {
                SetError();
            }
            tctdepodate.Text = "";
            txtRate.Text = ""; txtAmountUsd.Text = "";
            txtaud.Text = "";
            txtpaymentnotes.Text = "";

        }
    }

    protected void GridViewNote_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridViewNote_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();

            if (ID != "")
            {
                bool succ = ClstblStockOrders.Tblstockorder_PaymentDetail_DeleteByStockOrderId(ID);
                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(ID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();

                }
                else
                {
                    ModalPopupExtenderNote.Hide();
                }
            }
            //BindGridNotes(hndWholesaleOrderID.Value);

            //BindGrid(0);
        }
        if (e.CommandName == "Edit")
        {
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            btnEdit.Visible = true;
            btnSave.Visible = false;
            hndpmtDetailsId1.Value = ID;
            DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(ID);
            if (dt.Rows.Count > 0)
            {
                PanGridNotes.Visible = true;
                txtRate.Text = dt.Rows[0]["UsdRate"].ToString();
                if (dt.Rows[0]["DepositDate"].ToString() != null && dt.Rows[0]["DepositDate"].ToString() != "")
                {
                    DateTime dt1 = Convert.ToDateTime(dt.Rows[0]["DepositDate"].ToString());
                    tctdepodate.Text = dt1.ToShortDateString();
                }
                txtAmountUsd.Text = dt.Rows[0]["Deposit_amountUsd"].ToString();
                txtaud.Text = dt.Rows[0]["Deposit_amountAsd"].ToString();
                txtpaymentnotes.Text = dt.Rows[0]["PaymentNotes"].ToString();
            }
            else
            {
                PanGridNotes.Visible = false;
            }
            ModalPopupExtenderNote.Show();
            //BindGrid(0);
        }
    }

    protected void txtAmountUsd_TextChanged(object sender, EventArgs e)
    {
        decimal rate = 0;
        decimal Aud = 0;
        decimal usd = 0;
        if (txtAmountUsd.Text != null && txtAmountUsd.Text != "")
        {
            usd = Convert.ToDecimal(txtAmountUsd.Text);
        }
        if (txtRate.Text != null && txtRate.Text != "")
        {
            rate = Convert.ToDecimal(txtRate.Text);

        }
        Aud = usd / rate;
        txtaud.Text = Convert.ToString(Aud);

        ModalPopupExtenderNote.Show();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (hndpmtDetailsId1.Value != null && hndpmtDetailsId1.Value != "")
        {
            bool s1 = ClstblStockOrders.Tblstockorder_PaymentDetail_Update(hndpmtDetailsId1.Value, txtRate.Text, txtaud.Text, txtAmountUsd.Text, tctdepodate.Text);
            bool s2 = ClstblStockOrders.Tblstockorder_PaymentDetail_updatepaymentNotes(hndpmtDetailsId1.Value, txtpaymentnotes.Text);
            if (s1)
            {
                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(hndpmtDetailsId1.Value);
                if (dt.Rows.Count > 0)
                {
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();
                    tctdepodate.Text = "";
                    txtRate.Text = ""; txtAmountUsd.Text = "";
                    txtaud.Text = "";
                    txtpaymentnotes.Text = "";
                    SetAdd();
                }
                else
                {
                    tctdepodate.Text = "";
                    txtRate.Text = ""; txtAmountUsd.Text = "";
                    txtaud.Text = "";
                    txtpaymentnotes.Text = "";
                    SetError();
                }

            }
            ModalPopupExtenderNote.Hide();

        }
        BindGrid(0);
    }

    protected void GridViewNote_SelectedIndexChanged(object sender, EventArgs e)
    {

        //   string id = GridViewNote.DataKeys[e.NewSelectedIndex].Value.ToString();
    }

    protected void GridViewNote_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void GridViewNote_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }

    protected void btnOrderNo_Click(object sender, EventArgs e)
    {
        if (txorderno.Text != null && txorderno.Text != "")
        {
            int ExistOrderno = ClstblStockOrders.tblStockOrders_ExistsByOrderNo(txorderno.Text);
            if (ExistOrderno == 1)
            {
                SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderNumber(txorderno.Text);
                //try
                //{
                //    ddlStockLocation.SelectedValue = st.CompanyLocationID;
                //}
                //catch { }
                ddlStockLocation.SelectedValue = "8";

                try
                {
                    ddlVendor.SelectedValue = st.CustomerID;
                }
                catch { }

                try
                {
                    ddlpurchasecompany.SelectedValue = st.PurchaseCompanyId;
                }
                catch (Exception ecc)
                {
                    //throw;
                }

                txtManualOrderNumber.Text = st.ManualOrderNumber;

                try
                {
                    ddlorderstatusnew.SelectedValue = st.DeliveryOrderStatusNew;
                }
                catch (Exception esx)
                {
                    // throw;
                }

                try
                {
                    ddldeleverytype.SelectedValue = st.DeleveryId;
                    ddlOrdStatus.SelectedValue = st.StockOrderStatus;
                    ddlPMethod.SelectedValue = st.PaymentMethodID;
                }
                catch (Exception ex) { }
                txtcharges.Text = st.TransCharges;


                //try
                //{
                //    ddlDate.SelectedValue = st.DeliveryOrderStatus;
                //}
                //catch (Exception ex)
                //{ }

                //if (st.BOLReceived != null && st.BOLReceived != "")
                //{
                //    txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
                //}
                //if (st.ExpectedDelivery != null && st.ExpectedDelivery != "")
                //{
                //    txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
                //}
                //txtNotes.Text = st.Notes;

                //txtExtraNotes.Text = st.extranotes;
                //txtVendorInvoiceNo.Text = st.VendorInvoiceNo;
                ////ddlorderstatus.SelectedValue = st.DeliveryOrderStatus;

                //txtstockcontainer.Text = st.StockContainer;
                //hdnStockOrderID2.Value = st.orderid;
                //hdnActualDElivery2.Value = st.ActualDelivery;
                //try
                //{
                //    ddlitemname.SelectedValue = st.ItemNameId;
                //}
                //catch (Exception ex) { }
                //try
                //{
                //    ddlpurchasecompany.SelectedValue = st.PurchaseCompanyId;
                //}
                //catch (Exception ecc)
                //{

                //    //throw;
                //}

                //if (st.arrivaldate != null && st.arrivaldate != "")
                //{
                //    tctconfArrival.Text = st.arrivaldate;

                //}
                //if (st.transport_comp_Id != null && st.transport_comp_Id != "")
                //{
                //    ddlTransCompName.SelectedValue = st.transport_comp_Id;

                //}
                //if (st.transportcompsentdate != null && st.transportcompsentdate != "")
                //{
                //    txttranscompdate.Text = Convert.ToDateTime(st.transportcompsentdate).ToShortDateString();
                //}
                //if (st.telex_Release_date != null && st.telex_Release_date != "")
                //{
                //    tcttelaxrelease.Text = Convert.ToDateTime(st.telex_Release_date).ToShortDateString();
                //}
                //if (st.pmtDueDate != null && st.pmtDueDate != "")
                //{
                //    txtpmtduedate.Text = Convert.ToDateTime(st.pmtDueDate).ToShortDateString();
                //}
                //tctsupplierinvoiceno.Text = st.supplier_invlice_no;
                //txtpiamt.Text = st.total_pi_amt;
                //txttrnasportjobno.Text = st.transport_comp_jobno;
                //// txttransportserviceinvoiceno.Text = st.transport_comp_service_invno;
                //txttransdisbinvoices.Text = st.transport_comp_service_dist_invno;
                //if (st.pckemailwhdate != null && st.pckemailwhdate != "")
                //{
                //    tctpckemildatefromwh.Text = st.pckemailwhdate;
                //}
                //txtstrchargeamt.Text = st.storage_charge_amt;
                //txtstorechargeinvno.Text = st.storage_charge_invno;
                //txtstorechargereason.Text = st.storage_change_Reason;
                //txtExtraNotes.Text = st.extranotes;

                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(st.orderid);
                if (dt.Rows.Count > 0)
                {
                    rptattribute.DataSource = dt;
                    rptattribute.DataBind();
                    MaxAttribute = dt.Rows.Count;

                }
            }
            else
            {
                Notification("Order No Not Found..");
                txtSearchOrderNo.Text = "";
                txtSearchOrderNo.Focus();
            }
            txtSearchOrderNo.Text = "";
        }
    }

    protected void ddlorderstatusnew_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        if (ddlorderstatusnew.SelectedValue != null && ddlorderstatusnew.SelectedValue != "")
        {
            if (ddlorderstatusnew.SelectedValue == "0")
            {
                divint.Visible = false;
                DivPaperWork.Visible = false;
                DivTransCompName.Visible = false;
                Divtranscompdate.Visible = false;
                divint2.Visible = false;
                divIntdata3.Visible = false;
            }
            else if (ddlorderstatusnew.SelectedValue == "1")
            {

                divint.Visible = true;
                DivPaperWork.Visible = true;
                DivTransCompName.Visible = true;
                Divtranscompdate.Visible = true;
                divint2.Visible = true;
                divIntdata3.Visible = true;
            }
        }
        ModeAddUpdate();
    }

    //protected void btnUpdateOrderStatus_Click(object sender, EventArgs e)
    //{
    //    bool s1 = false;

    //    string Location = "";
    //    foreach (RepeaterItem item in rptLocation.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
    //        //Literal modulename = (Literal)item.FindControl("ltprojstatus");

    //        if (Location == "")
    //        {
    //            if (chkselect.Checked == true)
    //            {
    //                Location += "," + hdnModule.Value.ToString();
    //            }
    //        }
    //    }
    //    if (Location != "")
    //    {
    //        Location = Location.Substring(1);
    //    }

    //    //if(ddlloc)
    //    foreach (GridViewRow row in GridView1.Rows)
    //    {
    //        HiddenField hdnStockOrderID = (HiddenField)row.FindControl("hdnStockOrderID");
    //        CheckBox chk = (CheckBox)row.FindControl("chkord");
    //        if (chk.Checked)
    //        {
    //            if (hdnStockOrderID.Value != null && hdnStockOrderID.Value != "")
    //            {
    //                if (ddlStockOrderStatus.SelectedValue != null && ddlStockOrderStatus.SelectedValue != "")
    //                {
    //                    s1 = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(hdnStockOrderID.Value, ddlStockOrderStatus.SelectedValue);
    //                }
    //            }
    //            if (Location != null && Location != "")
    //            {
    //                bool s2 = ClstblStockOrders.tblStockOrders_UpdateCompanyLocationId(hdnStockOrderID.Value, Location);
    //            }
    //        }

    //    }
    //    if (s1)
    //    {
    //        SetAdd();
    //    }
    //    else
    //    {
    //        SetError();
    //    }
    //    BindGrid(0);
    //}

    protected void btnNewVendor_Click2(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedValue != "")
        {
            lblModelHeader.Text = "Update";
            hdnvendor.Value = ddlVendor.SelectedValue;
            ibtnAddVendor.Visible = false;

            DataTable dt = ClstblCustomers.tblCustomer_GetCustomerDataByCustomerID(ddlVendor.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                txtCompany.Text = dt.Rows[0]["Customer"].ToString();
                txtContFirst.Text = dt.Rows[0]["ContFirst"].ToString();
                txtContLast.Text = dt.Rows[0]["ContLast"].ToString();
                txtCredit.Text = dt.Rows[0]["Credit"].ToString();
                txtCompany.ReadOnly = true;
                txtContFirst.ReadOnly = true;
                txtContLast.ReadOnly = true;
            }
            else
            {
                txtCompany.Text = string.Empty;
                txtContFirst.Text = string.Empty;
                txtContLast.Text = string.Empty;
                txtCredit.Text = string.Empty;
            }

            ibtnUpdateVendor.Visible = true;

        }
        else
        {
            lblModelHeader.Text = "Add New";
            ibtnAddVendor.Visible = true;
            ibtnUpdateVendor.Visible = false;
            txtCompany.Text = string.Empty;
            txtContFirst.Text = string.Empty;
            txtContLast.Text = string.Empty;
            txtCredit.Text = string.Empty;
        }

        ModalPopupExtenderVendor.Show();
        //lnkBack.Visible = true;
        //lnkAdd.Visible = false;
        //PanAddUpdate.Visible = true;
        //PanGridSearch.Visible = false;
        //txtCompany.Text = string.Empty;
        //txtContFirst.Text = string.Empty;
        //txtContLast.Text = string.Empty;
        //txtCredit.Text = string.Empty;
        BindScript();
    }

    protected void txtBOLReceived_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        if (ddlVendor.SelectedValue != null && ddlVendor.SelectedValue != "")
        {
            string qr = "select Credit from tblContacts where CustomerId= " + ddlVendor.SelectedValue;
            DataTable dtc = ClstblCustomers.query_execute(qr);
            if (dtc.Rows.Count > 0)
            {
                String Credit = dtc.Rows[0]["Credit"].ToString();
                //SttblContacts stc = ClstblContacts.tblContacts_SelectByContactID(ddlVendor.SelectedValue);
                if (Credit != null && Credit != "")
                {
                    int credit = Convert.ToInt32(Credit);
                    if (txtBOLReceived.Text != null && txtBOLReceived.Text != "")
                    {
                        DateTime dt = Convert.ToDateTime(txtBOLReceived.Text).AddDays(credit);
                        txtpmtduedate.Text = Convert.ToString(dt.ToShortDateString());
                    }
                }
            }
        }
    }

    protected void lnEditVendor_Click(object sender, EventArgs e)
    {
        string CustId = hdnvendor.Value;
        ibtnAddVendor.Visible = false;
        ibtnUpdateVendor.Visible = true;
        // Button6.Visible = true;
        if (CustId != null && CustId != "")
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByCustomerID(CustId);
            SttblCustomers s1t1 = ClstblCustomers.tblCustomers_SelectByCustomerID(CustId);
            lnkBack.Visible = true;
            lnkAdd.Visible = false;
            PanAddUpdate.Visible = true;
            PanGridSearch.Visible = false;
            txtCompany.Text = s1t1.Customer;
            txtContFirst.Text = st.ContFirst;
            txtContLast.Text = st.ContLast;
            if (st.Credit != null && st.Credit != "")
            {
                txtCredit.Text = st.Credit;
            }
            BindScript();
            ModalPopupExtenderVendor.Show();
        }
    }

    protected void Button6_Click(object sender, EventArgs e)
    {


        //  }
    }

    protected void ibtnUpdateVendor_Click(object sender, EventArgs e)
    {
        string CustId = hdnvendor.Value;
        if (CustId != null && CustId != "")
        {
            bool s1 = ClstblContacts.tblContacts_CustomerCredit(Convert.ToString(CustId), txtCredit.Text);

            if (s1)
            {
                txtCredit.Text = "";
                //BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(CustId);
                Notification("Transaction Successful.");
            }
        }
        ModalPopupExtenderVendor.Hide();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    protected void lbtnPendingQty_Click(object sender, EventArgs e)
    {

    }

    protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        txtpmtduedate.Text = "";
        if (txtBOLReceived.Text != "")
        {
            string qr = "select Credit from tblContacts where CustomerId= " + ddlVendor.SelectedValue;
            DataTable dtc = ClstblCustomers.query_execute(qr);
            if (dtc.Rows.Count > 0)
            {
                String Credit = dtc.Rows[0]["Credit"].ToString();
                //SttblContacts stc = ClstblContacts.tblContacts_SelectByContactID(ddlVendor.SelectedValue);
                if (Credit != null && Credit != "")
                {
                    int credit = Convert.ToInt32(Credit);
                    if (ddlVendor.SelectedValue != null && ddlVendor.SelectedValue != "")
                    {
                        DateTime dt = Convert.ToDateTime(txtBOLReceived.Text).AddDays(credit);
                        txtpmtduedate.Text = Convert.ToString(dt.ToShortDateString());
                    }
                }
            }
        }
    }

    protected void txtTargetDate_TextChanged(object sender, EventArgs e)
    {
        if (txtTargetDate.Text != string.Empty)
        {
            txtExpectedDelivery.Text = string.Empty;

            CheckLocation();
        }

        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void txtExpectedDelivery_TextChanged(object sender, EventArgs e)
    {
        if (txtExpectedDelivery.Text != string.Empty)
        {
            txtTargetDate.Text = string.Empty;
            CheckLocation();
        }
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void btnSaveSubmit_Click(object sender, EventArgs e)
    {
        string Date = DateTime.Now.AddHours(14).ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string OrderID = hndOrderID.Value;

        if (OrderID != "")
        {
            bool Suc = ClstblStockOrders.tblStockOrder_UpdateSubmit(OrderID, Date, userid, txtReason.Text);

            if (Suc)
            {
                BindGrid(0);
                Notification("Transaction Successful..");
            }
            else
            {
                Notification("Error..");
            }
        }
    }

    protected void CheckLocation()
    {
        if (ddlStockLocation.SelectedValue == "8") // AUS
        {
            ddlStockLocation.SelectedValue = "";
            ddlStockLocation.Focus();
        }
    }

    protected void ddlStockLocation_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlStockLocation.SelectedValue == "8") // AUS
        {
            if (txtTargetDate.Text != string.Empty || txtExpectedDelivery.Text != string.Empty)
            {
                ddlStockLocation.SelectedValue = "";
                Notification("Please Remove target date or ETA then select AUS");
            }
        }

        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void lnkUpdateConfirm_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        SttblStockOrders st1 = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(Convert.ToString(id1));

        if (!string.IsNullOrEmpty(id1))
        {
            string Notes = txtNotes.Text;
            string BOLReceived = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            var vals = StockLocation_Text.Split(':')[0];
            var vals1 = StockLocation_Text.Split(':')[1];
            string state = Convert.ToString(vals);
            string ManualOrderNumber = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;
            int OrderStatus = 0;
            string ArrivalDate = tctconfArrival.Text;
            string transportcompId = ddlTransCompName.SelectedValue;
            string telexreleasedate = tcttelaxrelease.Text;
            string supplierinvoiceno = tctsupplierinvoiceno.Text;
            string totalpi_amt = txtpiamt.Text;
            string transportcompjobno = txttrnasportjobno.Text;
            string transport_comp_service_invno = "";
            string transport_comp_service_dist_invno = txttransdisbinvoices.Text;
            string pckemailwhdate = tctpckemildatefromwh.Text;
            string storage_charge_amt = txtstrchargeamt.Text;
            string storage_change_Reason = txtstorechargeinvno.Text;

            string extranotes = txtExtraNotes.Text;
            //string transcompdate =;
            string TransCompSentdate = txttranscompdate.Text;
            string storage_charge_invno = txtstorechargeinvno.Text;
            bool s = ClstblStockOrders.tblStockOrders_updat_NewFields(id1, ArrivalDate, transportcompId, telexreleasedate, supplierinvoiceno, totalpi_amt, transportcompjobno, transport_comp_service_invno, transport_comp_service_dist_invno, pckemailwhdate, storage_charge_amt, storage_change_Reason, extranotes, Notes, TransCompSentdate, storage_charge_invno);
            //if (chkDeliveryOrderStatus.Checked == true)
            //{
            //    OrderStatus = 1;
            //}
            if (ddlOrdStatus.SelectedValue != null && ddlOrdStatus.SelectedValue != "")
            {
                bool ord = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(id1, ddlOrdStatus.SelectedValue);
            }
            bool pmtdate = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(id1, txtpmtduedate.Text);
            bool success = ClstblStockOrders.tblStockOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
            ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
            //ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, Convert.ToInt32(ddlorderstatus.SelectedValue));
            ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, ddlorderstatusnew.SelectedValue);
            ClstblStockOrders.tblStockOrders_Update_DeliveryOrderStatusNew(id1, ddlorderstatusnew.SelectedValue);
            ClstblStockOrders.tblStockOrders_Update_StockContainer(id1, txtstockcontainer.Text);
            ClstblStockOrders.tblStockOrders_Update_PurchaseCompanyByID(id1, Convert.ToInt32(ddlpurchasecompany.SelectedValue));
            ClstblStockOrders.tblStockOrders_Update_ItemNameByID(id1, Convert.ToInt32(ddlitemname.SelectedValue), ddlitemname.SelectedItem.ToString());
            ClstblStockOrders.tblStockOrders_Update_DeleveryType(id1, Convert.ToInt32(ddldeleverytype.SelectedValue), ddldeleverytype.SelectedItem.ToString());
            ClstblStockOrders.tblStockOrders_Update_TransCharges(id1, txtcharges.Text);
            if (fptranporttaxinv.HasFile)
            {
                SiteConfiguration.DeletePDFFile("TransportTaxInvoice ", st1.transport_comp_service_invno);
                string map = id1 + fptranporttaxinv.FileName;
                bool s1 = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoice(Convert.ToString(id1), map);
                ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportTaxInvoice\\") + map);
                SiteConfiguration.UploadPDFFile("TransportTaxInvoice", map);
                SiteConfiguration.deleteimage(map, "TransportTaxInvoice");
            }

            if (ModuleFileUpload.HasFile)
            {
                SiteConfiguration.DeletePDFFile("arrivaldatedoc", st1.arrivaldatedoc);
                string map = id1 + ModuleFileUpload.FileName;
                bool IsPaperWorkUpload = ClstblStockOrders.tblstockorders_Upload_IspaperUpload(Convert.ToString(id1), "1");
                bool s1 = ClstblStockOrders.tblStockOrders_UpdateArriveDoc(Convert.ToString(id1), map);
                ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\arrivaldatedoc\\") + map);
                SiteConfiguration.UploadPDFFile("arrivaldatedoc", map);
                SiteConfiguration.deleteimage(map, "arrivaldatedoc");
            }


            if (fptelexupload.HasFile)
            {
                SiteConfiguration.DeletePDFFile("TelexReleasedoc", st1.TelexReleasedoc);
                string map = id1 + fptelexupload.FileName;
                bool s1 = ClstblStockOrders.tblStockOrders_TelexReleasedoc(Convert.ToString(id1), map);
                fptelexupload.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TelexReleasedoc\\") + map);
                SiteConfiguration.UploadPDFFile("TelexReleasedoc", map);
                SiteConfiguration.deleteimage(map, "TelexReleasedoc");
                bool IsTelexUpload = ClstblStockOrders.tblstockorders_UpdateTelexFlag(Convert.ToString(id1), "1");
            }

            if (fptransdisbinvoices.HasFile)
            {
                SiteConfiguration.DeletePDFFile("TransportDistInvoice", st1.TransportDistInvoice);
                string map = id1 + fptransdisbinvoices.FileName;
                bool s1 = ClstblStockOrders.tblStockOrders_TransportDistInvoice(Convert.ToString(id1), map);
                fptransdisbinvoices.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\TransportDistInvoice\\") + map);
                SiteConfiguration.UploadPDFFile("TransportDistInvoice", map);
                SiteConfiguration.deleteimage(map, "TransportDistInvoice");
            }

            if (fpstorechargeinvno.HasFile)
            {
                SiteConfiguration.DeletePDFFile("storagechanrgeinv", st1.arrivaldoc);
                string map = id1 + fpstorechargeinvno.FileName;
                bool s1 = ClstblStockOrders.tblStockOrders_storagechanrgeinv(Convert.ToString(id1), map);
                fpstorechargeinvno.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\storagechanrgeinv\\") + map);
                SiteConfiguration.UploadPDFFile("storagechanrgeinv", map);
                SiteConfiguration.deleteimage(map, "storagechanrgeinv");
            }

            ClstblStockOrders.tblStockOrderItems_whole_Delete(id1);
            foreach (RepeaterItem item in rptattribute.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                    HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
                    string StockItem = ddlStockItem.SelectedValue.ToString();
                    string OrderQuantity = txtOrderQuantity.Text;
                    TextBox ExpiryDate = (TextBox)item.FindControl("txtexpdate");
                    TextBox txtmodelNo = (TextBox)item.FindControl("txtmodelno");
                    string Amount = "0";
                    if (!String.IsNullOrEmpty(txtAmount.Text))
                    {
                        Amount = txtAmount.Text;
                    }

                    //if (ddlorderstatus.SelectedValue == "0")
                    //{
                    //    //int amt = Convert.ToInt32(txtAmount.Text);
                    //    decimal GSTamt = Convert.ToDecimal(txtAmount.Text) / 10;
                    //    Amount = Convert.ToString(Convert.ToDecimal(txtAmount.Text) + GSTamt);
                    //}

                    string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                    CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");

                    if (!chkdelete.Checked)
                    {

                        if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "" && txtAmount.Text != "")
                        {
                            int success1 = ClstblStockOrders.tblStockOrderItems_Insert(id1, StockItem, OrderQuantity, Amount, StockOrderItem, state);
                            bool s1 = ClstblStockOrders.tblStockOrderItems_updateExpAndMondelNo(Convert.ToString(success1), ExpiryDate.Text, txtmodelNo.Text);

                        }
                    }

                    if (chkdelete.Checked)
                    {
                        ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
                    }
                }
            }

            bool updatePaymentMethod = ClstblStockOrders.tblStockOrders_Update_PaymentMethod(id1.ToString(), ddlPMethod.SelectedValue);

            bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(id1.ToString(), txtTargetDate.Text);

            //Update Tax Invoice No
            bool UpdateTaxInvNo = ClstblStockOrders.tblStockOrders__UpdateTransportTaxInvoiceNo(id1.ToString(), txtTaxinvoiceNo.Text);

            bool UpdateTaxInvAmt = ClstblStockOrders.tblStockOrders_Update_Inv_InvDist_Amount(id1.ToString(), txtTaxinvoiceAmount.Text, txttransdisbinvoicesAmount.Text);

            bool UpdateLocation = ClstblStockOrders.SP_UpdateLocation_MH_SSN(id1.ToString(), ddlStockLocation.SelectedValue);

            //--- do not chage this code Start------
            if (success)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            //Reset();
            BindScript();
            BindGrid(0);
        }
    }

    protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
    {
        try
        {
            int index = GetControlIndex(((TextBox)sender).ClientID);
            RepeaterItem item = rptattribute.Items[index];

            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            TextBox txtAmountGST = (TextBox)item.FindControl("txtAmountGST");

            decimal Amount = 0;
            decimal AmountGST = 0;
            if (txtOrderQuantity.Text != "" && txtUnitPrice.Text != "")
            {
                Amount = Convert.ToDecimal(txtOrderQuantity.Text) * Convert.ToDecimal(txtUnitPrice.Text);

                if (ddlCurrency.SelectedValue == "2")
                {
                    AmountGST = Amount + (Amount * Convert.ToDecimal("0.10"));
                }
                else
                {
                    AmountGST = Amount;
                }
            }

            txtAmount.Text = Amount.ToString("F");
            txtAmountGST.Text = AmountGST.ToString("F");

        }
        catch (Exception ex)
        {
            string Msg = "Error: " + ex.Message;
            Notification(Msg);
        }
    }

    //protected void btnExcelNew_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();

    //    #region getCheckbox Item
    //    string Location = "";
    //    foreach (RepeaterItem item in rptLocation.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
    //        //Literal modulename = (Literal)item.FindControl("ltprojstatus");

    //        if (chkselect.Checked == true)
    //        {
    //            Location += "," + hdnModule.Value.ToString();
    //        }
    //    }
    //    if (Location != "")
    //    {
    //        Location = Location.Substring(1);
    //    }

    //    string PCompany = "";
    //    foreach (RepeaterItem item in rptCompany.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnModule = (HiddenField)item.FindControl("hdnId");
    //        //Literal modulename = (Literal)item.FindControl("ltprojstatus");

    //        if (chkselect.Checked == true)
    //        {
    //            PCompany += "," + hdnModule.Value.ToString();
    //        }
    //    }
    //    if (PCompany != "")
    //    {
    //        PCompany = PCompany.Substring(1);
    //    }

    //    string Vender = "";
    //    foreach (RepeaterItem item in rptVender.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnModule = (HiddenField)item.FindControl("hdnCustomerID");
    //        //Literal modulename = (Literal)item.FindControl("ltprojstatus");

    //        if (chkselect.Checked == true)
    //        {
    //            Vender += "," + hdnModule.Value.ToString();
    //        }
    //    }
    //    if (Vender != "")
    //    {
    //        Vender = Vender.Substring(1);
    //    }
    //    #endregion

    //    dt = ClstblStockOrders.tblStockOrders_SelectBySearchNewQuickStockNewExcelDate(ddlShow.SelectedValue, ddlDue.SelectedValue, Vender, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), Location, "", txtstockitemfilter.Text, ddlLocalInternation.SelectedValue, txtStockContainerFilter.Text, PCompany, ddlitemnamesearch.SelectedValue, ddlDelevery.SelectedValue, ddlStockOrderStatus.SelectedValue, ddlPaymentMethod.SelectedValue, ddlSearchTransCompName.SelectedValue, ddlPartial.SelectedValue);

    //    try
    //    {
    //        string[] columnNames = dt.Columns.Cast<DataColumn>()
    //              .Select(x => x.ColumnName)
    //            .ToArray();

    //        Export oExport = new Export();
    //        string FileName = "StockOrder_New" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
    //        int[] ColList = { 0, 2, 3, 4, 5, 6, 7, 8,
    //            9, 10, 11, 13 };

    //        string[] arrHeader = { "OrderNumber", "Category", "StockItem", "Stock Model", "Stock Size", "Stock Order", "TETA", "ETA",
    //        "WareHouse Date", "Locations", "Qty", "Delivery Type" };

    //        //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    //        //only change file extension to .xls for excel file
    //        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    //        //}
    //    }
    //    catch (Exception Ex)
    //    {
    //        //   lblError.Text = Ex.Message;
    //    }
    //}

    protected void ddlCurrency_TextChanged(object sender, EventArgs e)
    {
        BindGSTType();

        foreach (RepeaterItem item in rptattribute.Items)
        {
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            TextBox txtAmountGST = (TextBox)item.FindControl("txtAmountGST");

            decimal Amount = 0;
            decimal AmountGST = 0;
            if (txtAmount.Text != "")
            {
                Amount = Convert.ToDecimal(txtAmount.Text);
                if (ddlCurrency.SelectedValue == "2")
                {
                    AmountGST = Amount + (Amount * Convert.ToDecimal("0.10"));
                }
                else
                {
                    AmountGST = Amount;
                }
            }
            txtAmountGST.Text = AmountGST.ToString("F");
        }
    }

    protected void BindGSTType()
    {
        if (ddlCurrency.SelectedValue == "1")
        {
            ddlGSTType.SelectedValue = "2";
        }
        else if (ddlCurrency.SelectedValue == "2")
        {
            ddlGSTType.SelectedValue = "1";
        }
        else
        {
            ddlGSTType.SelectedValue = "";
        }
    }

    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        try
        {
            int index = GetControlIndex(((TextBox)sender).ClientID);
            RepeaterItem item = rptattribute.Items[index];

            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtUnitPrice = (TextBox)item.FindControl("txtUnitPrice");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            TextBox txtAmountGST = (TextBox)item.FindControl("txtAmountGST");

            decimal Amount = 0;
            decimal AmountGST = 0;
            if (txtAmount.Text != "")
            {
                Amount = Convert.ToDecimal(txtAmount.Text);
                if (ddlCurrency.SelectedValue == "2")
                {
                    AmountGST = Amount + (Amount * Convert.ToDecimal("0.10"));
                }
                else
                {
                    AmountGST = Amount;
                }
            }

            //txtAmount.Text = Amount.ToString("F");
            txtAmountGST.Text = AmountGST.ToString("F");

        }
        catch (Exception ex)
        {
            string Msg = "Error: " + ex.Message;
            Notification(Msg);
        }
    }

    protected void btnUpdateMinQty_Click(object sender, EventArgs e)
    {
        bool Success = false;

        if (FileUpload.HasFile)
        {
            DataTable dtMinQty = new DataTable();
            dtMinQty.Columns.Add("StockItemID", typeof(int));
            //dtMinQty.Columns.Add("StockItem", typeof(string));
            dtMinQty.Columns.Add("CompanyLocation", typeof(string));
            dtMinQty.Columns.Add("MinQty", typeof(int));

            FileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles\\MinQty\\" + FileUpload.FileName);
            string connectionString = "";
            if (FileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\MinQty\\" + FileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (FileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\MinQty\\" + FileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                //Find Sheet Name
                DataTable dtSchema = connection.GetSchema("Tables");
                string SheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                //foreach (DataRow row in dtSchema.Rows)
                //{
                //    SheetName = (string)row["TABLE_NAME"];
                //}

                using (DbCommand comm = connection.CreateCommand())
                {
                    comm.CommandText = "SELECT * FROM [" + SheetName + "]";
                    comm.CommandType = CommandType.Text;

                    using (DbDataReader dr = comm.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string StockItemID = dr["StockItemID"].ToString();
                                //string StockItem = dr["StockItem"].ToString();
                                string CompanyLocation = dr["CompanyLocation"].ToString();
                                string MinQty = dr["MinQty"].ToString();

                                dtMinQty.Rows.Add(StockItemID, CompanyLocation, MinQty);
                            }
                        }
                    }
                }

                connection.Close();

                // Delete File from Local Folder
                SiteConfiguration.deleteimage(FileUpload.FileName, "MinQty");

                int SucCount = ClstblStockOrders.USP_tblStockItemsLocation_UpdateMinQtyByExcel(dtMinQty);
                if (SucCount > 0)
                {
                    Notification("Transaction Success..");
                }
                else
                {
                    Notification("Error...");
                }
            }
        }
    }
}