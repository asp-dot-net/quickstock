<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stockitem.aspx.cs" Inherits="admin_adminfiles_stock_stockitem"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .req.form-control {
            border-color: #FF5F5F;
            /* background: rgb(249, 232, 232);*/
        }

        .height100 {
            height: 100px;
        }
    </style>
    <script>
        function ValidateTextBox(source, args) {
            //alert("sfsfsd");
            if (document.getElementById(source.controltovalidate).value != "") {
                //alert("true");
                // alert(document.getElementById(source.controltovalidate).value);
                $(document.getElementById(source.controltovalidate)).removeClass("req form-control");
                $(document.getElementById(source.controltovalidate)).addClass("form-control");
            } else {
                //alert("false");   
                $(document.getElementById(source.controltovalidate)).removeClass("form-control");
                $(document.getElementById(source.controltovalidate)).addClass("req form-control");
                $(document.getElementById(source.controltovalidate)).css("border-color", "#FF5F5F");

            }
        }


        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 200);
        }
        $(function () {
            $('form').on("click",'#<%=ibtnUploadPDF.ClientID %>', function () {
                //  $('form').on('submit', function () {
                //alert(this.selector);
                ShowProgress();
            });
        });

    </script>
    <%-- <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }
                function pageLoadedpro() {
                      $('#<%=btnAddInverter.ClientID %>').click(function () {
                          MyfunUploadSuccess();
                       });                    

         }
          function MyfunUploadSuccess() {

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "3000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "10000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Upload Successful');


            }
            </script>--%>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>


            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Item
                        <asp:Literal runat="server" ID="ltcompname"></asp:Literal>

                        <div id="hbreadcrumb" class="pull-right" runat="server" >
                            <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon" ><i class="fa fa-backward"></i> Back</asp:LinkButton>
                            <a href="#" id="anpanel" runat="server" class="btn  btn-warning" data-toggle="modal" data-target="#myModalpanel" >Panel</a>
                            &nbsp;<a href="#" class="btn btn-danger" id="aninvrter" runat="server" data-toggle="modal" data-target="#myModalinverter" >Inverter</a>
                            <a href="#" id="addBettery" runat="server" class="btn  btn-warning" data-toggle="modal" data-target="#myModalBettery" >Battery</a>

                            <asp:Button Text="Update Stock" ID="btnUpdateStock" runat="server" CssClass="btn btn-danger" OnClick="btnUpdateStock_Click" />
                        </div>
                        <h5></h5>
                        <div class="clear">
                        </div>
                    </h5>


                </div>
            </div>
            <link rel="stylesheet" type="text/css" href="<%=SiteURL %>admin/theme/assets/pages/notification/notification.css">
            <script src="<%=SiteURL %>admin/theme/assets/pages/notification/notification.js"></script>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    // alert("1");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

               <%--     function delayUpdateClick(sender, args) {
                        alert("22")
                        button = $get("<%= buttonUpdate.ClientID %>");
                        setTimeout(function () { button.click(); }, 5000);
                    }--%>

                    //alert("2");
                    document.getElementById('loader_div').style.visibility = "hidden";
                    //$('.loader_div').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    $("[data-toggle=tooltip]").tooltip();

                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $('#<%=btnAdd.ClientID %>').click(function (e) {
                        //alert("1");
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();

                    });
                    function toaster(msg) {
                        //alert("54345");
                        notifymsg(msg, 'inverse');
                    }
                }
                //function toaster(msg, type) {
                //    //alert(msg);
                //    //alert(type);
                //    notifymsg(msg, 'transection')
                //}
          <%--      function doRefresh() {
                   
                    setTimeout(function () {
                        $('#<%=GridView1.ClientID %>').load('stockitem.aspx');
                        doRefresh();
                    }, 20000);
                }--%>

           <%--     var auto_refresh = setInterval(
                    function () {
                        $('#<%=panel1.ClientID %>').load('#<%=panel1.ClientID %>');
                    }, 5000);--%>

                $(document).ready(function () {
                   <%-- $('#<%=GridView1.ClientID %>').load('stockitem.aspx');
                    //doRefresh();--%>


                });


<%--               function callitrept() {
                    //alert("233");
                    $('#<%=buttonUpdate.ClientID %>').click();

                }

                var auto_refresh = setInterval(
                    function () {
                         $('#<%=panel1.ClientID %>').fadeOut('slow').load('stockitem.aspx #<%=panel1.ClientID %>').fadeIn("slow");
                        callitrept();
                    }, 120000);--%>

<%--                var auto_refresh = setInterval(
                    function () {
                        $('#<%=panel1.ClientID %>').fadeOut('slow').load('stockitem.aspx #<%=panel1.ClientID %>').fadeIn("slow");
                        //page
                        //$('#<%=panel1.ClientID %>').load('stockitem.aspx #<%=panel1.ClientID %>');
                    }, 5000);--%>



</script>

            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Stock Item
                                </h5>
                            </div>
                            <div class="card-block padleft25">
                                <div class="form-horizontal row">
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <asp:Label ID="Label15" runat="server" class="col-sm-3 control-label">
                                                    Stock&nbsp;Category</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="reqerror"
                                                    ControlToValidate="ddlstockcategory" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <asp:Label ID="Label10" runat="server" class="col-sm-3 control-label">
                                                   Stock&nbsp;Item</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label1" runat="server" class="col-sm-3 control-label">
                                                    Brand</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label2" runat="server" class="col-sm-3 control-label">
                                                    Stock&nbsp;Model</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label3" runat="server" class="col-sm-3 control-label">
                                                    Stock&nbsp;Series</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label4" runat="server" class="col-sm-3 control-label">
                                                    Stock&nbsp;Size</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ErrorMessage="Enter valid digit"
                                                    ValidationExpression="^\d*\.?\d+$" ValidationGroup="Req" ForeColor="#F47442"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="lblStockPhase" runat="server" class="col-sm-3 control-label">
                                                    Stock&nbsp;Phase</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlPhase" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    <asp:ListItem Value="1">Single</asp:ListItem>
                                                    <asp:ListItem Value="2">Double</asp:ListItem>
                                                    <asp:ListItem Value="3">Three</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label5" runat="server" class="col-sm-3 control-label">
                                                    Min.&nbsp;Stock</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                    ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="Req" ForeColor="#F47442"></asp:RegularExpressionValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtminstock" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label16" runat="server" class="col-sm-3 control-label">
                                                    QuickForm&nbsp;ID</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtfixstockitemid" runat="server" MaxLength="8" CssClass="form-control modaltextbox"></asp:TextBox>



                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." 
                                                                    ControlToValidate="txtfixstockitemid" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Label ID="Label17" runat="server" class="col-sm-3 control-label">
                                                    Purchase Avg Price</asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtPurchaseAvgPrise" runat="server" CssClass="form-control modaltextbox" Text="0.00"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender TargetControlID="txtPurchaseAvgPrise" FilterMode="ValidChars" FilterType="Numbers, Custom" ValidChars="."
                                                    runat="server">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="form-group checkareanew row" id="sales" runat="server" visible="false">
                                            <div class="col-sm-3 rightalign">
                                                <asp:Label ID="Label12" runat="server" class=" control-label">
                                                  Sales&nbsp;Tag</asp:Label>
                                            </div>
                                            <div class="col-sm-6 checkbox-info checkbox">
                                                <asp:CheckBox ID="chksalestag" runat="server" />
                                                <label for="<%=chksalestag.ClientID %>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="form-group checkareanew row">
                                            <div class="col-sm-3 rightalign">
                                                <asp:Label ID="Label11" runat="server" class=" control-label">
                                                  Is Active?</asp:Label>
                                            </div>
                                            <div class="col-sm-6">




                                                <div class="checkbox-fade fade-in-primary d-">

                                                    <label for="<%=chkisactive.ClientID %>">
                                                        <asp:CheckBox ID="chkisactive" runat="server" />
                                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                        <span class="text">&nbsp;</span>
                                                    </label>

                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>


                                        <div class="form-group checkareanew row">
                                            <div class="col-sm-3 rightalign">
                                                <asp:Label ID="Label13" runat="server" class=" control-label">
                                                  Is&nbsp;Dashboard?</asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="checkbox-fade fade-in-primary d-">
                                                    <label for="<%=chkDashboard.ClientID %>">
                                                        <asp:CheckBox ID="chkDashboard" runat="server" />
                                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </div>


                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="form-group checkareanew row">
                                            <div class="col-sm-3 rightalign">
                                                <asp:Label ID="Label14" runat="server" class=" control-label">
                                                  Is&nbsp;Discontinue</asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="checkbox-fade fade-in-primary d-">
                                                    <label for="<%=chkDiscontinue.ClientID %>">
                                                        <asp:CheckBox ID="chkDiscontinue" runat="server" />
                                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </div>


                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="name form-group">

                                                    <label class="control-label">
                                                        <strong>Location </strong>
                                                    </label>

                                                </span>
                                            </div>
                                            <div class="col-md-2">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Quantity</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-2">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Min Qty</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-2">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Pur. Qty</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-2">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Tag</strong>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                            <ItemTemplate>

                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <span>
                                                            <asp:HiddenField ID="hndlocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                            <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span>
                                                            <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage=""
                                                                ControlToValidate="txtqty" ClientValidationFunction="ValidateTextBox" ValidateEmptyText="true"></asp:CustomValidator>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                                ControlToValidate="txtqty" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                            <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="Req" ForeColor="#F47442"></asp:RegularExpressionValidator>

                                                        </span>
                                                    </div>

                                                    <!-- Newly Added -->
                                                    <div class="col-md-2">
                                                        <span>
                                                            <asp:TextBox ID="txtMinQty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                            <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage=""
                                                                ControlToValidate="txtMinQty" ClientValidationFunction="ValidateTextBox" ValidateEmptyText="true"></asp:CustomValidator>

                                                            <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator8" runat="server" ValidationExpression="^[0-9]*$"
                                                                ErrorMessage="Enter valid digit" ControlToValidate="txtMinQty" ValidationGroup="Req" ForeColor="#F47442"></asp:RegularExpressionValidator>

                                                        </span>
                                                    </div>
                                                    <!-- END Newly Added -->

                                                    <!-- Newly Added -->
                                                    <div class="col-md-2">
                                                        <span>
                                                            <asp:TextBox ID="txtPurAvgQty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                            <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage=""
                                                                ControlToValidate="txtPurAvgQty" ClientValidationFunction="ValidateTextBox" ValidateEmptyText="true"></asp:CustomValidator>
                                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtPurAvgQty" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>

                                                        </span>
                                                    </div>
                                                    <!-- END Newly Added -->

                                                    <div class="col-md-2 right-text checkbox-info checkbox martop7">
                                                        <div class="thkmartop">
                                                            <div class="checkbox-fade fade-in-primary d-">
                                                                <label for="<%# Container.FindControl("chksalestagrep").ClientID %>">
                                                                    <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                                    <span class="text">&nbsp;</span>
                                                                </label>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>

                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req"
                                            Text="Add" />
                                        <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" ValidationGroup="Req"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                    <p>&nbsp;</p>



                                    <div id="Div4" class="form-group" runat="server" visible="false">
                                        <span class="name">
                                            <label class="control-label">
                                                Description</label>
                                        </span><span>
                                            <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                Width="100%" Style="resize: none;">
                                            </asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>




                                </div>
                            </div>

                        </div>




                    </div>
                </div>
            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal">

                            <asp:UpdatePanel ID="updatepanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel3">
                                        <div class="">
                                    </asp:Panel>
                                </ContentTemplate>
                                <%--                   <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" />

                                </Triggers>--%>
                            </asp:UpdatePanel>
                            <div class="card shadownone brdrgray pad10">
                                <div class="card-block">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <div class="inlineblock martop5">
                                            <div class="row">
                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:TextBox ID="txtSearchStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStockItem"
                                                        WatermarkText="StockItem" />
                                                </div>

                                                <%--  <div class="input-group col-sm-2 martop5">
                                                                    <asp:TextBox ID="txtSearchManufacturer" runat="server" placeholder="Stock Manufacturer" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchManufacturer"
                                                                        WatermarkText="StockManufacturer" />
                                                                </div>--%>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:TextBox ID="txtSearchModel" runat="server" placeholder="Stock Model" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchModel"
                                                        WatermarkText="Stock Model" />
                                                </div>
                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Category</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="false" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Location</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170">
                                                    <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Stock Active</asp:ListItem>
                                                        <asp:ListItem Value="True">Only Active</asp:ListItem>
                                                        <asp:ListItem Value="False">Not Active</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 martop5 max_width170" style="display: none">
                                                    <asp:DropDownList ID="ddlSalesTag" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Sales Tag</asp:ListItem>
                                                        <asp:ListItem Value="True">Only Tagged</asp:ListItem>
                                                        <asp:ListItem Value="False">Not Tagged</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group  sandbox-container">
                                                        <asp:TextBox ID="txtExpiryDate" placeholder="Expiry Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group martop5 col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />
                                                </div>
                                                <div class="input-group martop5 col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <div class="datashowbox inlineblock">
                                        <div class="row">
                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" Style="width: 170px!important;" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-2 martop5 max_width170">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </div>
            <div class="finalgrid">
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>

                        <%--  <asp:UpdatePanel runat="server" ID="upgrid" UpdateMode="Conditional">
                            <ContentTemplate>--%>
                        <div id="PanGrid" runat="server">
                            <div class="card shadownone brdrgray">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">

                                        <asp:GridView ID="GridView1" DataKeyNames="id" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                            OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockItemID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label31" runat="server" Width="70px">
                                                                <%#Eval("StockItemID")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stock Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockItem">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Width="170px">
                                                                <%#Eval("StockItem")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="Brand" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockManufacturer">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("StockManufacturer")%>' data-toggle="tooltip"
                                                            Width="150px"><%#Eval("StockManufacturer")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Model" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockModel">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label7" runat="server" Width="150px" data-placement="top" data-original-title='<%#Eval("StockModel")%>' data-toggle="tooltip">
                                                                <%#Eval("StockModel")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Series" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StockSeries">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label8" runat="server" Width="70px">
                                                                <%#Eval("StockSeries")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Size" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="StockSeries" HeaderStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSize" runat="server" Width="50px">
                                                                <%#Eval("StockSize")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phase" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="Phase" HeaderStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPhase" runat="server" Width="70px">
                                                                <%#Eval("Phase")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Width="70px">
                                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" SortExpression="StockQuantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStockQuantity" runat="server" Width="60px" Text='<%#Eval("StockQuantity")%>'> 
                                                        </asp:Label>
                                                        <asp:LinkButton ID="btnQty" runat="server" CommandName="UpdateQty" Width="60px"
                                                            CommandArgument='<%#Eval("StockItemID") + ";" +Eval("CompanyLocationID") + ";" +Eval("StockQuantity") %>' CausesValidation="false">
                                                                            <%#Eval("StockQuantity")%>
                                                        </asp:LinkButton>

                                                        <asp:HyperLink runat="server" ID="hypViewQty" CssClass="btn-mini" data-toggle="tooltip" data-placement="top" title="View Quantity History" data-original-title="View" Target="_blank" 
                                                            NavigateUrl='<%# "~/admin/adminfiles/stock/QuantityHistoryDetails.aspx?StockItemID=" + Eval("StockItemID") + "&StockItem="  + Eval("StockItem") + "&LocationId="  + Eval("CompanyLocationID") %>' >
                                                                         <i class="fa fa-eye"></i>
                                                        </asp:HyperLink>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                             
                                                <asp:TemplateField Visible="false" ItemStyle-Width="20px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="gvbtnDelete" runat="server" CommandName="Delete" ImageUrl="../../../images/icon_edit.png" CssClass="tooltips" data-toggle="tooltip"
                                                            data-placement="top" title="Update Stock Qty" data-original-title="Update Stock Quantity" CausesValidation="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="verticaaline" />
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" ItemStyle-Width="20px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="gvbtnEditItem" runat="server" CommandName="UpdateStock" CommandArgument='<%#Eval("StockItemID") %>' ImageUrl="../../../images/icon_edit.png"
                                                            data-toggle="tooltip" data-placement="top" data-original-title="Stock Item" CausesValidation="false"></asp:ImageButton>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="verticaaline" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Width="30px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                                            <i class="fa fa-edit"></i> Edit
                                                        </asp:LinkButton>
                                                        <asp:HyperLink runat="server" ID="btnserial" CommandName="SerialNo" CommandArgument='<%#Eval("StockItemID") +";"+ Eval("CompanyLocationID")%>' CssClass="btn btn-success btn-mini "
                                                            data-toggle="tooltip" data-placement="top" title="Item History" data-original-title="Edit" Target="_blank"
                                                            NavigateUrl='<%# SiteURL + "admin/adminfiles/stock/Stockdetail.aspx?StockItemID=" + Eval("StockItemID") + "&CompanyLocationId=" + Eval("CompanyLocationID") %>'>
                                                                         <i class="fa fa-eye"></i> View
                                                        </asp:HyperLink>
                                                        <asp:HiddenField runat="server" ID="hdnCompanyLocationID" Value='<%#Eval("CompanyLocationID")%>' />
                                                        <%--   <asp:HyperLink ID="btnserial" runat="server" CommandName="SerialNo" CommandArgument='<%#Eval("StockItemID") +";"+ Eval("CompanyLocationID")%>' CausesValidation="false" CssClass="btn btn-success btn-mini"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="SerialNo" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important;">
                                                            <i class="fa fa-eye"></i> View
                                                        </asp:HyperLink>--%>
                                                        <asp:LinkButton ID="gvbtnUpload" runat="server" CommandName="Upload" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Upload">
                                                            <i class="fa fa-upload"></i> Upload
                                                        </asp:LinkButton>

                                                        <asp:HiddenField ID="hdnStockitemID2" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                        <asp:HyperLink ID="lnkPDFUploded" runat="server" CommandName="PDFUploded" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false" CssClass="btn btn-warning"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFUploded" Visible="false" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important;">
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> PDF
                                                        </asp:HyperLink>
                                                        <asp:HyperLink ID="lnkPDFPending" runat="server" CommandName="PDFPending" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false" CssClass="btn btn-danger"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFPending" Visible="false" Style="font-size: 10px!important; padding: 5px 6px; height: 23px!important; color: #fff!important;">
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> PDF
                                                        </asp:HyperLink>
                                                        <%-- <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select"
                                                                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                                                            <i class="fa fa-edit"></i>--%>

                                                        <asp:HyperLink runat="server" ID="hypUnusedSerialNo" CssClass="btn btn-success btn-mini" data-toggle="tooltip" data-placement="top" title="View Unused Serial No" data-original-title="Edit" Target="_blank" 
                                                            NavigateUrl='<%# "~/admin/adminfiles/reports/UnusedStockItemDetails.aspx?StockItemID=" + Eval("StockItemID") + "&StockItem="  + Eval("StockItem") + "&LocationId="  + Eval("CompanyLocationID") %>' >
                                                                         <i class="fa fa-eye"></i> Unsed
                                                        </asp:HyperLink>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" CssClass="verticaaline" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--   </ContentTemplate>
                            <Triggers>
                              <%--  <asp:AsyncPostBackTrigger ControlID="buttonUpdate" />
                                  <asp:AsyncPostBackTrigger ControlID="timer" EventName="Tick"/>
                            </Triggers>
                        </asp:UpdatePanel>--%>
                        <%--   <asp:Button ID="buttonUpdate" runat="server" OnClick="buttonUpdate_Click" OnClientClick="return false;"/>--%>
                        <%--   <cc1:ToolkitScriptManager runat="server" id="scpManager" EnablePartialRendering="True" CombineScripts="True" />--%>
                        <%--<asp:Timer runat="server" ID="timer" Interval="2000" OnTick="timer_Tick" />--%>

                        <asp:Button ID="btnNULLData" Style="display: none;" Text="test" runat="server" />
                        <cc1:ModalPopupExtender ID="MPEUpdateStock" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="true" PopupControlID="divStockitem" TargetControlID="btnNULLData"
                            CancelControlID="btncanelStock">
                        </cc1:ModalPopupExtender>
                        <div id="divStockitem" runat="server" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div style="float: right">
                                            <asp:LinkButton ID="btncanelStock" CssClass="btn btn-danger btncancelicon" runat="server">Close</asp:LinkButton>
                                        </div>
                                        <h4 class="modal-title" id="H1">Update Stock Quantity</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <div class="formainline">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <asp:Literal ID="ltcategory" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Stock Item</label>
                                                            <asp:HiddenField ID="hdnid" runat="server" />
                                                            <asp:Literal ID="ltstockitem" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Brand</label>
                                                            <asp:Literal ID="ltbrand" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Model</label>
                                                            <asp:Literal ID="ltmodel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Series</label>
                                                            <asp:Literal ID="ltseries" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Stock Quantity</label>
                                                            <asp:Literal ID="ltstockquantity" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Location</label>
                                                            <asp:Literal ID="ltlocation" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Select Head</label>
                                                            <asp:DropDownList ID="ddlhead" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlhead_SelectedIndexChanged"
                                                                AutoPostBack="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                <asp:ListItem Value="">select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                ControlToValidate="ddlhead" Display="Dynamic" ValidationGroup="stockquantity"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Quantity</label>
                                                            <asp:TextBox ID="txtquantity" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                ControlToValidate="txtquantity" Display="Dynamic" ValidationGroup="stockquantity"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^-?\d+$" Display="Dynamic"
                                                                ErrorMessage="Enter valid digit" ControlToValidate="txtquantity" ValidationGroup="stockquantity"></asp:RegularExpressionValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^-[0-9]+$"
                                                                Visible="false" ErrorMessage="Enter valid digit" ControlToValidate="txtquantity" Display="Dynamic"
                                                                ValidationGroup="stockquantity"></asp:RegularExpressionValidator>
                                                        </div>
                                                        <div style="text-align: center;">
                                                            <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" CssClass="btn btn-danger savewhiteicon"
                                                                ValidationGroup="stockquantity" />
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnsave" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLDataItem" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="true" PopupControlID="divItem" TargetControlID="btnNULLDataItem"
                            CancelControlID="btncanelItem">
                        </cc1:ModalPopupExtender>
                        <div id="divItem" runat="server" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div style="float: right">
                                            <asp:LinkButton ID="btncanelItem" CssClass="btn btn-danger btncancelicon" runat="server">Close</asp:LinkButton>
                                        </div>
                                        <h4 class="modal-title" id="myModalLabel">Stock Item</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <div class="formainline">
                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <label>Stock Item</label>
                                                            <asp:TextBox ID="txtStockItemU" runat="server" CssClass="form-control" Width="200px" MaxLength="255"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Brand</label>
                                                            <asp:TextBox ID="txtStockManufacturerU" runat="server" MaxLength="100" Width="200px" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Model</label>
                                                            <asp:TextBox ID="txtStockModelU" runat="server" MaxLength="50" Width="200px" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Series</label>
                                                            <asp:TextBox ID="txtStockSeriesU" runat="server" MaxLength="50" Width="200px" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Active</label>
                                                            <asp:CheckBox ID="chkActiveU" runat="server" />
                                                            <label for="<%=chkActiveU.ClientID %>">
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                        <div id="Div5" class="form-group" runat="server" visible="false">
                                                            <label>Sales Tag</label>
                                                            <asp:CheckBox ID="chkSalesTagU" runat="server" />
                                                            <label for="<%=chkSalesTagU.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                        <div style="text-align: center;">
                                                            <asp:Button ID="btnSaveItem" runat="server" Text="Save" OnClick="btnSaveItem_Click" CssClass="btn btn-danger savewhiteicon"
                                                                ValidationGroup="stockitem" />
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnSaveItem" />

                                                    </Triggers>

                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="hndStockItemID" runat="server" />
                        </div>
                        <asp:Button ID="btnNULLUpload" Style="display: none;" Text="test" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderUpload" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="btnCancelUpload" DropShadow="false" PopupControlID="divUpload" TargetControlID="btnNULLUpload">
                        </cc1:ModalPopupExtender>
                        <div id="divUpload" runat="server" style="display: none" class="modal_popup">
                            <div>
                                <div class="modal-dialog" style="width: 440px;">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header">
                                            <%--<div style="float: right">
                                                <asp:LinkButton ID="btnCancelUpload" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                                                </asp:LinkButton>
                                            </div>--%>
                                            <h4 class="modal-title" id="H3">Upload Brochure</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <div class="formainline formnew">
                                                    <div class="clear"></div>
                                                    <div class="formainline">
                                                        <div class="addexcel_start">
                                                            <ul>
                                                                <li class="margin_right20">

                                                                    <div class="input-group margin_bottom0">
                                                                        <input type="text" class="form-control input-lg" disabled placeholder="Modules: Upload Excel File">
                                                                        <span class="input-group-btn">
                                                                            <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                                        </span>
                                                                    </div>
                                                                    <asp:HiddenField ID="hdnStockItemID" runat="server" />
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" class="file" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                        ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf)$" Style="color: red;"
                                                                        Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This field is required." CssClass="reqerror"
                                                                        ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf" Style="color: red;"></asp:RequiredFieldValidator>
                                                                </li>
                                                                <li>
                                                                    <%--<asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="Button1" runat="server" OnClick="btnaddmodule_Click"
                                        Text="Add" ValidationGroup="success1"/>--%>
                                                                    <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                        ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <%-- <div class="topfileuploadbox marbtm15">

                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div style="word-wrap: break-word;">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Upload File <span class="symbol required"></span>
                                                                        </label>
                                                                        <span class="">
                                                                            <asp:HiddenField ID="hdnStockItemID" runat="server" />
                                                                            <asp:FileUpload ID="FileUpload1"  runat="server" Style="display: inline-block;" />
                                                                            <div class="clear">
                                                                            </div>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf)$" Style="color: red;"
                                                                                Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This field is required." CssClass="reqerror"
                                                                                ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf" Style="color: red;"></asp:RequiredFieldValidator>
                                                                        </span>

                                                                        <div class="clear">
                                                                        </div>
                                                                </div>
                                                            </div>

                                                        </div>--%>

                                                        <div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <asp:LinkButton ID="btnCancelUpload" CausesValidation="false" runat="server" CssClass="btn btn-danger" data-dismiss="modal">
                                                                    Close
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="btnCancelUpload" DropShadow="false" PopupControlID="divUpload" TargetControlID="btnNULLUpload">
                        </cc1:ModalPopupExtender>
                </asp:Panel>
            </div>


            </div>
            
    <!-- The Modal -->
            <div class="modal" id="myModalpanel">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Panel Excel</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="addexcel_start">
                                <ul>
                                    <li class="margin_right20">

                                        <div class="input-group margin_bottom0">
                                            <input type="text" class="form-control input-lg" disabled placeholder="Modules: Upload Excel File">
                                            <span class="input-group-btn">
                                                <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                            </span>
                                        </div>
                                        <asp:FileUpload ID="ModuleFileUpload" runat="server" class="file" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="ModuleFileUpload"
                                            ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xls only" class="error_text"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="ModuleFileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>
                                    </li>

                                    <li>
                                        <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnaddmodule" runat="server" OnClick="btnaddmodule_Click"
                                            Text="Add" ValidationGroup="success1" />
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>


            <div class="modal" id="myModalinverter">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Inverter Excel</h4>
                            <%-- <button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="addexcel_start">
                                <ul>
                                    <li class="margin_right20">
                                        <div class="input-group margin_bottom0">
                                            <input type="text" class="form-control input-lg" disabled placeholder="Inverter: Upload Excel File">
                                            <span class="input-group-btn">
                                                <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                            </span>
                                        </div>
                                        <asp:FileUpload ID="InverterFileUpload" runat="server" class="file" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="InverterFileUpload"
                                            ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xls only" class="error_text"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="InverterFileUpload" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>

                                    </li>
                                    <li>
                                        <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnAddInverter" runat="server" OnClick="btnAddInverter_Click"
                                            Text="Add" ValidationGroup="success" />

                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- The Modal -->
            <div class="modal" id="myModalBettery">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Bettery Excel</h4>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="addexcel_start">
                                <ul>
                                    <li class="margin_right20">
                                        <div class="input-group margin_bottom0">
                                            <input type="text" class="form-control input-lg" disabled placeholder="Modules: Upload Excel File">
                                            <span class="input-group-btn">
                                                <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                            </span>
                                        </div>
                                        <asp:FileUpload ID="fuBettery" runat="server" class="file" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="fuBettery"
                                            ValidationGroup="ValBettery" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xlsx only" class="error_text"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="fuBettery" Display="Dynamic" ValidationGroup="ValBettery"></asp:RequiredFieldValidator>
                                    </li>

                                    <li>
                                        <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnAddBettery" runat="server" OnClick="btnAddBettery_Click"
                                            Text="Add" ValidationGroup="ValBettery" />
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderUpdateQty" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="UpdateQty" TargetControlID="Button3"
                CancelControlID="LinkButton11">
            </cc1:ModalPopupExtender>
            <div id="UpdateQty" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalQty">Update Quantity
                                <span style="float: right" class="printorder" />
                                <asp:LinkButton ID="LinkButton11" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="inlineblock martop5">
                                                <div class="row">
                                                    <div class="input-group col-sm-9 martop5">
                                                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control m-b" placeholder="Quantity"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtQty" FilterType="Numbers" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                    ControlToValidate="txtQty" Display="Dynamic" ValidationGroup="QtyReq" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                        <asp:HiddenField ID="hndStockID" runat="server" />
                                                        <asp:HiddenField ID="hndCompanyLocationID" runat="server" />
                                                        <asp:HiddenField ID="hndQty" runat="server" />

                                                    </div>
                                                    <div class="input-group col-sm-3 martop5">
                                                        <asp:DropDownList ID="ddlMode" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                            <asp:ListItem Value="Plus" Text="Plus"></asp:ListItem>
                                                            <asp:ListItem Value="Minus" Text="Minus"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" CssClass="reqerror"
                                                    ControlToValidate="ddlMode" Display="Dynamic" ValidationGroup="QtyReq" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-group col-sm-12 martop5">
                                                        <asp:TextBox ID="txtQtyNotes" runat="server" TextMode="MultiLine" Rows="5" Columns="5" placeholder="Note" CssClass="form-control m-b height100"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:LinkButton ID="lnkUpdateQty" CssClass="btn btn-info" ValidationGroup="QtyReq" runat="server" Text="Update"
                                                            OnClick="lnkUpdateQty_Click" CausesValidation="true"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button3" Style="display: none;" runat="server" />

        </ContentTemplate>
        <Triggers>
            <%--            <asp:PostBackTrigger ControlID="chkDashboard" />--%>
            <asp:AsyncPostBackTrigger ControlID="chkDashboard" EventName="CheckedChanged" />
            <%--<asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <asp:PostBackTrigger ControlID="btnaddmodule" />
            <asp:PostBackTrigger ControlID="btnAddInverter" />
            <asp:PostBackTrigger ControlID="btnAddBettery" />
            <asp:PostBackTrigger ControlID="ibtnUploadPDF" />
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>




    <%--    <script src="http://localhost:57341/admin/js/bootstrap.file-input.js"></script>--%>
    <script>
        $(document).ready(function () {
            $('input[type="file"].fileupload').change(function (e) {
                var fileName = e.target.files[0].name;
                alert('The file "' + fileName + '" has been selected.');
                $(label.filename).text(value);
            });
        });
    </script>
</asp:Content>


