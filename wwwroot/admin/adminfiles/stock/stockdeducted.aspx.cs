using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockdeducted : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv3;
    static DataView dv4;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();
            txtStartDate.Text= DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            txtEndDate.Text= DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            BindGrid(0);



            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //tdExport.Visible = true;
            }
            else
            {
                // tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddllocationsearch.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();
    }
    protected DataTable GetGridData1()
    {
        
        //DataTable dt1 = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, ddlDeductedOrNot.SelectedValue);
        DataTable dt1 = ClstblProjects.tblProjects_SelectWarehousePickListWise("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, ddlDeductedOrNot.SelectedValue);

        //int iTotalRecords = dt1.Rows.Count;
        //int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;

        //if (iEndRecord > iTotalRecords)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //if (iStartsRecods == 0)
        //{
        //    iStartsRecods = 1;
        //}
        //if (iEndRecord == 0)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt1;
    }


    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        ddllocationsearch.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        //DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, ddlDeductedOrNot.SelectedValue);
        DataTable dt = ClstblProjects.tblProjects_SelectWarehousePickListWise("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, ddlDeductedOrNot.SelectedValue);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            //int[] ColList = { 11, 14, 333, 331, 52, 332,334,336 };
            int[] ColList = { 17, 18, 22, 20, 19, 21, 23, 25 };
            string[] arrHeader = { "ProjectNumber", "Project", "InstallBookingDate", "Installer", "Details", "Store Name","Deducted On","Deducted By" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlSearchDate.SelectedValue = "1";
        ddlSearchState.SelectedValue = "";
        ddlDeductedOrNot.SelectedValue = "False";
        BindGrid(0);
    }
}