﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="StockOrderDelivery.aspx.cs" Inherits="admin_adminfiles_stock_StockOrderDelivery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }
            </script>
            
            <%--   <script>
               function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }

            </script>--%>
            <script>


                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                  <%--  $('form').on("click",'#<%=btndeliver.ClientID %>', function () {
                        //  $('form').on('submit', function () {
                        //alert(this.selector);
                        ShowProgress();
                    });--%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                    // alert("1");
                }
                function endrequesthandler(sender, args) {
                    //alert("2");
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    // alert("3");
                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#<%=btndeliver.ClientID %>').click(function () {
                        formValidate();
                    });
                    $("[data-toggle=tooltip]").tooltip();

                    //$('.datetimepicker1').datetimepicker({
                    //    format: 'DD/MM/YYYY'
                    //});
                    $(document).ready(function () {

                        $('[data-toggle="tooltip"]').tooltip({
                            trigger: 'hover'
                        });

                    <%--    $('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });--%>
                    });

                }
            </script>
            <script>
                function UploadExcel() {
                    //alert("Hii..");
                    //var uploadfiles = $('input[type="file"]').get(1);
                    var fileUpload = document.getElementById("<%=FileUpload1.ClientID %>");
                    <%--$('#<%=hdnFileName.ClientID %>').val(fileUpload.files[0].name);--%>
                    var fromdata = new FormData();
                    fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
                    var choice = {};
                    choice.url = "Handler/ExcelUploadHandler.ashx";
                    choice.type = "POST";
                    choice.data = fromdata;
                    choice.contentType = false;
                    choice.processData = false;
                    choice.success = function (result) {
                        //MyfunSuccess();

                        //UploadExcelData(fileUpload.files[0].name, fileUpload.files[0].Extension);
                    };
                    choice.error = function (err) {
                        MyfunManulMsg("Error When File Uploading..");
                    };
                    $.ajax(choice);
                }

            </script>
            <div class="page-body padtopzero printorder">


                <div class="page-body headertopbox printorder">
                    <div class="card">
                        <div class="card-block">
                            <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Order Detail
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                                <div id="hbreadcrumb" class="pull-right">
                                    <%--<asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" CssClass="btn btn-maroon" OnClick="lnkBack_Click"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                </div>
                            </h5>
                        </div>
                    </div>

                </div>
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                            <div class="alert alert-danger" id="PanSerialNo" runat="server" visible="false">
                                <i class="icon-info-sign"></i>
                                <asp:Literal ID="ltrerror" runat="server"></asp:Literal></literal></strong>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
                <div class="panel-body" id="detailsid" runat="server">
                    <div class="card">
                        <div class="card-block">

                            <div class="formainline">
                                <div class="row">
                                    <div class="col-md-6" style="margin-bottom: 20px;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                            <tr>
                                                <td width="200px" valign="top">Vendor
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Purchase Company
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblpcompany" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Manual Order No
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">V. Invoice No
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Container No
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblContainerNo" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Notes
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 20px;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                            <tr>
                                                <td width="200px" valign="top">Item Name
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblItemname" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Stock Location
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">BOL Received
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Expected Delevery
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">StockFrom
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblStockFrom" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Delivery Type 
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDeliveryType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Trans Charges
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltranscharges" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                            <tr id="trOrderItem" runat="server">
                                                <td colspan="2" style="padding: 0px!important;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                        <tr class="graybgarea">
                                                            <td colspan="4">
                                                                <h4>Stock Order Item Detail</h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th><b>Stock Category</b></th>
                                                            <th><b>Stock Item</b></th>
                                                            <th><b>Quantity</b></th>
                                                            <th><b>Amount</b></th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                        <asp:Repeater ID="rptOrder" runat="server" OnItemDataBound="rptOrder_ItemDataBound" OnItemCommand="rptOrder_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <asp:HiddenField runat="server" ID="hdnDeliveryOrderStatus" Value='<%#Eval("DeliveryOrderStatus") %>' />
                                                                    <asp:HiddenField runat="server" ID="hfStockOrderID" Value='<%#Eval("StockOrderID") %>' />
                                                                    <asp:HiddenField runat="server" ID="hfStockCategoryID" Value='<%#Eval("StockCategoryID") %>' />
                                                                    <asp:HiddenField runat="server" ID="hfStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                                    <asp:HiddenField runat="server" ID="hndStockLocation" Value='<%#Eval("StockLocation") %>' />
                                                                    <td><%#Eval("StockOrderItem") %></td>
                                                                    <td><%#Eval("StockItem") %></td>
                                                                    <td>
                                                                        <asp:Label runat="server" ID="lblOrderQuantity" Text='<%#Eval("OrderQuantity") %>'></asp:Label></td>
                                                                    <td><%#Eval("Amount") %></td>
                                                                    <td class="text-center">
                                                                        <asp:LinkButton ID="btnDelivery" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-warning btn-mini"
                                                                            data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" 
                                                                            CommandArgument='<%#Eval("StockOrderID") +","+ Eval("StockOrderItemID")%>'>
                                                                            <i class="fa fa-truck"></i> Delivery</asp:LinkButton>

                                                                        <asp:HyperLink ID="lnkExcelUploded" runat="server" CausesValidation="false" CssClass="btn btn-success btn-mini"
                                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Excel Uploded" Visible="false" Style="font-size: 10px!important; color: white">
                                                                        <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Excel Uploaded
                                                                        </asp:HyperLink>

                                                                        <asp:LinkButton ID="lnkrevert" runat="server" CausesValidation="false" CssClass="btn btn-inverse btn-mini" UseSubmitBehavior="false" Visible="false"
                                                                            CommandName="Revert" CommandArgument='<%#Eval("StockOrderID") +","+ Eval("StockOrderItemID")%>'> <i class="fa fa-retweet"></i> Revert</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <cc1:ModalPopupExtender ID="ModalPopupdeliver" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divdeliver" TargetControlID="btnNull1"
                CancelControlID="btnCancel1">
            </cc1:ModalPopupExtender>
            <div id="divdeliver" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid" runat="server" />
                <asp:HiddenField ID="hndStockOrderItemID" runat="server" />
                <div class="modal-dialog" style="width: 300px;">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Delivery Date</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12 custom_datepicker" style="display: none;">
                                        <div>
                                            <asp:Label ID="Label26" runat="server" class="control-label ">
                                                <strong>Delivery Date</strong></asp:Label>
                                            <div class="input-group date" data-provide="datepicker">
                                                <%--  <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>--%>
                                                <asp:TextBox ID="txtdatereceived" runat="server" class="form-control" placeholder="Delivery Date">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <asp:RequiredFieldValidator ID="reqfieldtxtdatereceived" runat="server" ForeColor="Red" ControlToValidate="txtdatereceived" ErrorMessage="" ValidationGroup="adddate"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group" style="margin-bottom: 0px;">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label22" runat="server" class="control-label ">
                                                <strong>Serial No.</strong></asp:Label>
                                        </span>
                                        <span class="">
                                            <div class="input-group margin_bottom0">
                                                <input type="text" class="form-control input-lg" disabled placeholder="Upload Excel File">
                                                <span class="input-group-btn">
                                                    <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                                </span>
                                            </div>
                                            <asp:FileUpload ID="FileUpload1" runat="server" class="file" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.xls|.XLS)$" Style="color: red;"
                                                Display="Dynamic" ErrorMessage="Upload .xls file only"></asp:RegularExpressionValidator>
                                            <div class="clear">
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="FileUpload1" ErrorMessage="This value is Required." ValidationGroup="adddate"></asp:RequiredFieldValidator>
                                        </span>
                                    </div>



                                    <div runat="server" class="form-group marginleft col-sm-12" style="text-align: center; margin-bottom: 0;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btndeliver" runat="server" Text="Save" OnClick="btndeliver_Click"
                                CssClass="btn btn-primary savewhiteicon btnsaveicon POPupLoader dnone" ValidationGroup="adddate" />
                            <button id="btnCancel1" runat="server" type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <%--         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--%>

                            <asp:Button ID="btnUpdateExcel" runat="server" Text="Save" OnClick="btnUpdateExcel_Click"
                                CssClass="btn btn-primary savewhiteicon btnsaveicon POPupLoader" ValidationGroup="adddate" />

                           <%-- <asp:Button ID="btnExcelUpdate" runat="server" Text="Save" OnClientClick="UploadExcel()" 
                                CssClass="btn btn-primary savewhiteicon btnsaveicon POPupLoader" ValidationGroup="adddate" />--%>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupRevert" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="DivRevertPopup" TargetControlID="btnNull2" CancelControlID="btnCancel2">
            </cc1:ModalPopupExtender>
            <div id="DivRevertPopup" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 320px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Reset Serial No.</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <asp:Label runat="server" ID="lblalert" Text="Are you sure you want to reset serial no.?"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnOK" runat="server" type="button" class="btn btn-danger POPupLoader" data-dismiss="modal" Text="Ok" OnClick="btnOK_Click"></asp:Button>
                            <asp:Button ID="btnCancel2" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>

                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />

        </ContentTemplate>
        <Triggers>
            <%-- <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <asp:PostBackTrigger ControlID="btndeliver" />
            <asp:PostBackTrigger ControlID="btnUpdateExcel" />
            <asp:PostBackTrigger ControlID="btnOK" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

