﻿<%@ WebHandler Language="C#" Class="ExcelUploadHandler" %>

using System;
using System.Web;

public class ExcelUploadHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {

        //string strData = (string)context.Session["SomeSessionData"];
        if (context.Request.Files.Count > 0)
        {
            try
            {
                //For Multiple Files
                //HttpFileCollection SelectedFiles = context.Request.Files;
                //for (int i = 0; i < SelectedFiles.Count; i++)
                //{
                //    HttpPostedFile PostedFile = SelectedFiles[i];
                //    context.Session["SomeSessionData"] = strData + "," + PostedFile.FileName;
                //    string FileName = context.Server.MapPath("~/userfiles/CustomerDocuments/" + PostedFile.FileName);
                //    PostedFile.SaveAs(FileName);
                //}


                //For Single Files
                HttpFileCollection SelectedFiles = context.Request.Files;
                HttpPostedFile PostedFile = SelectedFiles[0];
                //context.Session["SomeSessionData"] = strData + "," + PostedFile.FileName;
                string FileName = context.Server.MapPath("~/userfiles/Delivery/" + PostedFile.FileName);
                PostedFile.SaveAs(FileName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}