﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_WholesaleService : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    static int MaxAttribute = 1;
    protected DataTable rpttable;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropdown();

            init();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    #region GridView Comman Code
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        initAddUpdate("Update");

        getEditDetails(id);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Notes")
        {
            hndTicketNo.Value = e.CommandArgument.ToString();
            string TicketNo = e.CommandArgument.ToString();
           
            BindGridNotes(TicketNo);
            txtNotes.Text = string.Empty;
            ModalPopupExtenderNote.Show();
        }
    }
    #endregion

    public void BindDropdown()
    {
        DataTable dtCustomer = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlSearchCustomer.DataSource = dtCustomer;
        ddlSearchCustomer.DataTextField = "Customer";
        ddlSearchCustomer.DataValueField = "CustomerID";
        ddlSearchCustomer.DataBind();

        ddlCustomer.DataSource = dtCustomer;
        ddlCustomer.DataTextField = "Customer";
        ddlCustomer.DataValueField = "CustomerID";
        ddlCustomer.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string ticketNo = txtSerachTicketNo.Text;
        string invoiceNo = txtSerachInvoiceNo.Text;
        string customerId = ddlSearchCustomer.SelectedValue;
        string jobStatus = ddlSearchJobStatus.SelectedValue;
        string searchShipmentTrackingNo = txtSearchShipmentTrackingNo.Text;
        string dateType = ddlDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsWholesaleService.WholesaleService_Select(ticketNo, invoiceNo, customerId, jobStatus, searchShipmentTrackingNo, dateType, startDate, endDate);

        return dt;
    }

    public void bindTotal(DataTable dt)
    {

    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
            }
            panGrid.Visible = false;
            divnopage.Visible = false;
            //divtot.Visible = false;
        }
        else
        {
            panGrid.Visible = true;
            //divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            bindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    public void init()
    {
        reset();
        lnkAdd.Visible = true;
        lnkBack.Visible = false;

        PanAddUpdate.Visible = false;

        panSearch.Visible = true;
        panGrid.Visible = true;
        BindGrid(0);
    }

    public void reset()
    {
        txtInvoiceNo.Text = string.Empty;
        txtGB_BS_Formbay_Id.Text = string.Empty;
        txtServiceCompanyTicketNo.Text = string.Empty;
        txtContactName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhoneNo.Text = string.Empty;
        ddlJobStatus.ClearSelection();
        ddlCustomer.ClearSelection();
        txtShipmentTrackingNo.Text = string.Empty;
        txtServiceChargeInvoiceValue.Text = string.Empty;
        txtInvNoToClaim.Text = string.Empty;
        txtClaimedAmount.Text = string.Empty;
        txtFaultyPickUpStatus.Text = string.Empty;
        txtAddress.Text = string.Empty;

        MaxAttribute = 1;
        BindRepeater();
        BindAddedAttribute();
    }

    public void initAddUpdate(string Mode)
    {
        reset();
        lblAddUpdate.Text = Mode + " ";

        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        PanAddUpdate.Visible = true;

        panGrid.Visible = false;
        //panTotal.Visible = false;
        panSearch.Visible = false;

        if (Mode == "Add")
        {
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;

            MaxAttribute = 1;
            BindRepeater();
            BindAddedAttribute();
        }
        else
        {
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        initAddUpdate("Add");
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        init();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        int id = ClsWholesaleService.WholesaleService_Insert(createdOn, createdBy);
        if (id > 0)
        {
            bool sucUpdate = UpdateById(id);
            setAddUpdate();
        }
        else
        {
            SetError();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id = GridView1.DataKeys[GridView1.SelectedIndex].Value.ToString();

        bool sucUpdate = UpdateById(Convert.ToInt32(id));

        if (sucUpdate)
        {
            setAddUpdate();
        }
        else
        {
            SetError();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        init();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        reset();
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void setAddUpdate()
    {
        Notification("Transaction Success.");
        init();
    }

    public void setDelete()
    {
        Notification("Deleted Successfully.");
        init();
    }

    public void SetError()
    {
        Notification("Transaction Failed.");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtSerachTicketNo.Text = string.Empty;
        txtSerachInvoiceNo.Text = string.Empty;
        ddlSearchCustomer.ClearSelection();
        ddlSearchJobStatus.ClearSelection();
        txtSearchShipmentTrackingNo.Text = string.Empty;
        ddlDate.ClearSelection();
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "WholesaleService_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17
                    , 18, 19, 20, 21 };

            string[] arrHeader = { "TicketNo", "Invoice No", "GBBS Formbay Id", "Service Company TicketNo", "Contact Name", "Address", "Email", "Phone No", "Job Status", "Company Name", "Shipment Tracking No", "Service Char. Inv. Val", "Inv. No. To Claim", "Claimed Amt", "Faulty Pick Up Status", "Created On", "Created By"
              , "OrderItem", "Notes", "Notes Created By", "Notes Created On" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void getEditDetails(string id)
    {
        ClsWholesaleService.stWholesaleService st = ClsWholesaleService.WholesaleService_SelectByTicketNo(id);

        txtInvoiceNo.Text = st.InvoiceNo;
        txtGB_BS_Formbay_Id.Text = st.GB_BS_Formbay_Id;
        txtServiceCompanyTicketNo.Text = st.ServiceCompanyTicketNo;
        txtContactName.Text = st.ContactName;
        txtAddress.Text = st.Address;
        txtEmail.Text = st.Email;
        txtPhoneNo.Text = st.PhoneNo;
        txtShipmentTrackingNo.Text = st.ShipmentTrackingNo;
        txtServiceChargeInvoiceValue.Text = st.ServiceChargeInvoiceValue;
        txtInvNoToClaim.Text = st.InvNoToClaim;
        txtClaimedAmount.Text = st.CalimedAmount;
        txtFaultyPickUpStatus.Text = st.FaultyPickUpStatus;
        
        try
        {
            ddlJobStatus.SelectedValue = st.JobStatus;
            ddlCustomer.SelectedValue = st.CompanyName;
        }
        catch (Exception ex)
        {

        }

        DataTable dtStock = ClsWholesaleService.WholesaleServiceItems_SelectByTicketNo(id);
        if(dtStock.Rows.Count > 0)
        {
            MaxAttribute = dtStock.Rows.Count;
            rptStock.DataSource = dtStock;
            rptStock.DataBind();
        }
    }

    protected bool UpdateById(int id)
    {
        string invoiceNo = txtInvoiceNo.Text;
        string bsgbId = txtGB_BS_Formbay_Id.Text;
        string serviceCompanyTicketNo = txtServiceCompanyTicketNo.Text;
        string contactName = txtContactName.Text;
        string address = txtAddress.Text;
        string email = txtEmail.Text;
        string phoneNo = txtPhoneNo.Text;
        string jobStatus = ddlJobStatus.SelectedValue;
        string customerId = ddlCustomer.SelectedValue;
        string shipmentTrackingNo = txtShipmentTrackingNo.Text;
        string serviceChargeInvoiceValue = txtServiceChargeInvoiceValue.Text;
        string invNoToClaim = txtInvNoToClaim.Text;
        string calimedAmount = txtClaimedAmount.Text;
        string faultyPickUpStatus = txtFaultyPickUpStatus.Text;
        
        bool update = ClsWholesaleService.WholesaleService_Update(id.ToString(), invoiceNo, bsgbId, serviceCompanyTicketNo, contactName, address, email, phoneNo, jobStatus, customerId, shipmentTrackingNo, serviceChargeInvoiceValue, invNoToClaim, calimedAmount, faultyPickUpStatus);

        if(update)
        {
            ClsWholesaleService.WholesaleServiceItems_Delete(id.ToString());

            foreach (RepeaterItem item in rptStock.Items)
            {
                if (item.Visible != false)
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    TextBox txtStockNotes = (TextBox)item.FindControl("txtStockNotes");
                    TextBox txtOldSerialNo = (TextBox)item.FindControl("txtOldSerialNo");
                    TextBox txtNewSerialNo = (TextBox)item.FindControl("txtNewSerialNo");

                    int success1 = ClsWholesaleService.WholesaleServiceItems_Insert(id.ToString(), ddlStockItem.SelectedValue, txtStockNotes.Text, txtOldSerialNo.Text, txtNewSerialNo.Text);
                }
            }
        }
        else
        {
            ClsWholesaleService.WholesaleService_Delete(id.ToString());
        }

        return update;
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value;
        bool sucDelete = ClsWholesaleService.WholesaleService_Delete(id);
        bool sucDelete1 = ClsWholesaleService.WholesaleServiceItems_Delete(id);
        if (sucDelete)
        {
            setDelete();
        }
        else
        {
            SetError();
        }
    }

    #region Bind Stock Repeater
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockNotes", Type.GetType("System.String"));
        rpttable.Columns.Add("OldSerialNo", Type.GetType("System.String"));
        rpttable.Columns.Add("NewSerialNo", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptStock.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtStockNotes = (TextBox)item.FindControl("txtStockNotes");
            TextBox txtOldSerialNo = (TextBox)item.FindControl("txtOldSerialNo");
            TextBox txtNewSerialNo = (TextBox)item.FindControl("txtNewSerialNo");

            HiddenField hdnStockCategory = (HiddenField)item.FindControl("hdnStockCategory");
            HiddenField hdnStockItem = (HiddenField)item.FindControl("hdnStockItem");
            HiddenField hdnStockNotes = (HiddenField)item.FindControl("hdnStockNotes");
            HiddenField hdnOldSerialNo = (HiddenField)item.FindControl("hdnOldSerialNo");
            HiddenField hdnNewSerialNo = (HiddenField)item.FindControl("hdnNewSerialNo");
           
            DataRow dr = rpttable.NewRow();
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["StockNotes"] = txtStockNotes.Text;
            dr["OldSerialNo"] = txtOldSerialNo.Text;
            dr["NewSerialNo"] = txtNewSerialNo.Text;

            rpttable.Rows.Add(dr);
        }
    }

    protected void BindRepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockNotes", Type.GetType("System.String"));
            rpttable.Columns.Add("OldSerialNo", Type.GetType("System.String"));
            rpttable.Columns.Add("NewSerialNo", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["StockNotes"] = "";
            dr["OldSerialNo"] = "";
            dr["NewSerialNo"] = "";
            dr["type"] = "";
            rpttable.Rows.Add(dr);
        }
        rptStock.DataSource = rpttable;
        rptStock.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptStock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptStock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptStock.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        BindRepeater();
    }
    
    protected void rptStock_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtStockNotes = (TextBox)e.Item.FindControl("txtStockNotes");
        TextBox txtOldSerialNo = (TextBox)e.Item.FindControl("txtOldSerialNo");
        TextBox txtNewSerialNo = (TextBox)e.Item.FindControl("txtNewSerialNo");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdnStockNotes = (HiddenField)e.Item.FindControl("hdnStockNotes");
        HiddenField hdnOldSerialNo = (HiddenField)e.Item.FindControl("hdnOldSerialNo");
        HiddenField hdnNewSerialNo = (HiddenField)e.Item.FindControl("hdnNewSerialNo");

        DataTable dtStockItem = ClstblStockItems.tblStockItems_SelectbyAsc(hdnStockCategory.Value);
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtStockNotes.Text = hdnStockNotes.Value;
        txtOldSerialNo.Text = hdnOldSerialNo.Value;
        txtNewSerialNo.Text = hdnNewSerialNo.Value;

        if (e.Item.ItemIndex == 0)
        {
            litremove.Visible = false;
        }
        else
        {
            litremove.Visible = true;
        }
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptStock.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

        ddlStockItem.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlStockItem.Items.Add(item1);

        DataTable dtStock = ClstblStockItems.tblStockItems_SelectbyAsc(ddlStockCategoryID.SelectedValue);
        ddlStockItem.DataSource = dtStock;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();
    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptStock.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptStock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptStock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptStock.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
    }
    #endregion

    public void BindGridNotes(string ticketNo)
    {
        DataTable dt = new DataTable();
        dt = ClsWholesaleService.WholesaleServiceNotes_SelectByTicketNo(ticketNo);

        if (dt.Rows.Count > 0)
        {
            GridViewNote.DataSource = dt;
            GridViewNote.DataBind();
            PanGridNotes.Visible = true;
        }
        else
        {
            PanGridNotes.Visible = false;
        }
    }

    protected void GridViewNote_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Delete")
        {
            string ID = e.CommandArgument.ToString();

            if (ID != "")
            {
                bool succ = ClsWholesaleService.WholesaleServiceNotes_DeleteById(ID);
                if (succ)
                {
                    setDelete();
                    BindGridNotes(hndTicketNo.Value);
                    BindGrid(0);
                }
            }

            ModalPopupExtenderNote.Show();
        }
    }

    protected void GridViewNote_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void btnSaveNotes_Click(object sender, EventArgs e)
    {
        string ticketNo = hndTicketNo.Value;
        string notes = txtNotes.Text;
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        int id = ClsWholesaleService.WholesaleServiceNotes_Insert(createdOn, createdBy);
        if (id > 0)
        {
            bool sucUpdate = ClsWholesaleService.WholesaleServiceNotes_Update(id.ToString(), ticketNo, notes);
            BindGridNotes(ticketNo);
            ModalPopupExtenderNote.Show();
            txtNotes.Text = string.Empty;
            setAddUpdate();
        }
        else
        {
            BindGridNotes(ticketNo);
            ModalPopupExtenderNote.Show();
            SetError();
        }
    }
}