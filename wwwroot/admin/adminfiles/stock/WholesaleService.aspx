﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WholesaleService.aspx.cs" Inherits="admin_adminfiles_stock_WholesaleService"
    Culture="en-GB" UICulture="en-GB" MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>

                function toaster(msg) {
                    //alert("54345");
                    notifymsg(msg, 'inverse')
                }

                function notifymsg(message, type) {
                    $.growl({
                        message: message
                    }, {
                        type: type,
                        allow_dismiss: true,
                        label: 'Cancel',
                        className: 'btn-xs btn-inverse',
                        placement: {
                            from: 'top',
                            align: 'right'
                        },
                        delay: 30000,
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        offset: {
                            x: 30,
                            y: 30
                        }
                    });
                }

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });


                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("begin");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });

                    $("[data-toggle=tooltip]").tooltip();

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }

            </script>

            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Wholesale Service
                            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                            </div>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="finaladdupdate printorder">
                <asp:Panel ID="PanAddUpdate" runat="server">
                    <div class="panel-body animate-panel padtopzero stockorder">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Service Order
                                </h5>
                            </div>
                            <style>
                                .custom {
                                    margin-bottom: 15px;
                                }
                            </style>
                            <div class="card-block padleft25">
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <div class="col-md-2 custom">
                                            <span>Invoice No</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtInvoiceNo" FilterType="Numbers" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="" ControlToValidate="txtInvoiceNo"
                                                    Display="Dynamic" ValidateRequestMode="Enabled" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Greenbot/Formbay/Bridgeselect</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtGB_BS_Formbay_Id" runat="server" MaxLength="100" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Service Com. TicketNo</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtServiceCompanyTicketNo" runat="server" MaxLength="100" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Contact Name</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtContactName" runat="server" MaxLength="100" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Email</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Phone No</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtPhoneNo" runat="server" MaxLength="10" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtPhoneNo" FilterType="Numbers" />
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Job Status</span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlJobStatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="Open">Open</asp:ListItem>
                                                    <asp:ListItem Value="Close">Close</asp:ListItem>
                                                </asp:DropDownList><span>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Customer</span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlCustomer" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList><span>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Shipment Tracking No</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtShipmentTrackingNo" runat="server" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Service Charge Invoice Value</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtServiceChargeInvoiceValue" runat="server" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtServiceChargeInvoiceValue" FilterType="Numbers, Custom" ValidChars="." />
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Invoice No to Claim</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtInvNoToClaim" runat="server" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Claimed Amount</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtClaimedAmount" runat="server" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtClaimedAmount" FilterType="Numbers, Custom" ValidChars="." />
                                            </div>
                                        </div>

                                        <div class="col-md-2 custom">
                                            <span>Faulty PickUp Status</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtFaultyPickUpStatus" runat="server" MaxLength="100" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <span>Address</span>
                                            <div class="drpValidate">
                                                <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Height="100px" MaxLength="365" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="rptStock" runat="server" OnItemDataBound="rptStock_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:Panel ID="PanelRepeater" runat="server">
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <span>Stock Category</span>
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdnStockCategory" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <span>Stock Item</span>
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <span>Stock Notes</span>
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdnStockNotes" runat="server" Value='<%# Eval("StockNotes") %>' />
                                                                    <asp:TextBox ID="txtStockNotes" runat="server" TextMode="MultiLine" Height="50px" MaxLength="365" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <span>Old Serial No</span>
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdnOldSerialNo" runat="server" Value='<%# Eval("OldSerialNo") %>' />
                                                                    <asp:TextBox ID="txtOldSerialNo" runat="server" TextMode="MultiLine" Height="50px" MaxLength="365" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <span>New Serial No</span>
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdnNewSerialNo" runat="server" Value='<%# Eval("NewSerialNo") %>' />
                                                                    <asp:TextBox ID="txtNewSerialNo" runat="server" TextMode="MultiLine" Height="50px" MaxLength="365" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1">
                                                                <div class="drpValidate">
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnAddRow" OnClick="btnAddRow_Click" runat="server" Text="Add" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12 text-center">
                                        <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="Req"
                                            Text="Add" />
                                        <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CausesValidation="true" ValidationGroup="Req"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="finalgrid">
                <asp:Panel ID="panSearch" runat="server" DefaultButton="btnSearch">
                    <div class="animate-panel">
                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="input-group col-sm-2 max_width170 ">
                                                <asp:TextBox ID="txtSerachTicketNo" runat="server" placeholder="Ticket No" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSerachTicketNo"
                                                    WatermarkText="Ticket No" />
                                            </div>

                                            <div class="input-group col-sm-2 max_width170 ">
                                                <asp:TextBox ID="txtSerachInvoiceNo" runat="server" placeholder="Invoice No" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSerachInvoiceNo"
                                                    WatermarkText="Invoice No" />
                                            </div>

                                            <div class="input-group col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSearchCustomer" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSearchJobStatus" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Job Status</asp:ListItem>
                                                    <asp:ListItem Value="Open">Open</asp:ListItem>
                                                    <asp:ListItem Value="Close">Close</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 max_width170 ">
                                                <asp:TextBox ID="txtSearchShipmentTrackingNo" runat="server" placeholder="Shipment Tracking No" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtSearchShipmentTrackingNo"
                                                    WatermarkText="Shipment Tracking No" />
                                            </div>

                                            <div class="input-group col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Date</asp:ListItem>
                                                    <asp:ListItem Value="1">Created Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                <div class="input-group sandbox-container">
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon fullWidth"
                                                    CausesValidation="false"></asp:LinkButton>
                                            </div>

                                            <div class="input-group martop5 col-sm-1 max_width170">
                                                <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left" OnClick="btnClearAll_Click"
                                                    CausesValidation="false" CssClass="btn btn-primary btnclear fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </asp:Panel>

                <%--<asp:Panel ID="panTotal" runat="server">
                    <div class="page-header card" id="divtot" runat="server">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Total Number of Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of Inverters:&nbsp;</b><asp:Literal ID="lblTotalInverters" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Total Number of STC:&nbsp;</b><asp:Literal ID="lblTotalSTC" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                            </div>
                        </div>
                    </div>
                </asp:Panel>--%>

                <asp:Panel ID="panGrid" runat="server" CssClass="xsroll">
                    <div>
                        <div id="DivGrid" runat="server">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="TicketNo" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand" OnRowDeleting="GridView1_RowDeleting"
                                            AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("TicketNo") %>','tr<%# Eval("TicketNo") %>');">
                                                            <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TicketNo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="TicketNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTicketNo" runat="server"><%#Eval("TicketNoFull")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="invoiceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInvoiceNo" runat="server"><%#Eval("invoiceNo")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="GBBS Formbay Id" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="GB_BS_Formbay_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGB_BS_Formbay_Id" runat="server"><%#Eval("GB_BS_Formbay_Id")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Service Company TicketNo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="ServiceCompanyTicketNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServiceCompanyTicketNo" runat="server"><%#Eval("ServiceCompanyTicketNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contact Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="ContactName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContactName" runat="server"><%#Eval("ContactName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="Address">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAddress" runat="server"><%#Eval("Address")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmail" runat="server"><%#Eval("Email")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Phone No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="PhoneNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPhoneNo" runat="server"><%#Eval("PhoneNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Job Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="JobStatus">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblJobStatus" runat="server"><%#Eval("JobStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="Delivery">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompanyName" runat="server"><%#Eval("CompanyName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Shipment Tracking No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="ShipmentTrackingNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShipmentTrackingNo" runat="server"><%#Eval("ShipmentTrackingNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Service Char. Inv. Val" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="ServiceChargeInvoiceValue">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServiceChargeInvoiceValue" runat="server"><%#Eval("ServiceChargeInvoiceValue")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Inv. No. To Claim" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="InvNoToClaim">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInvNoToClaim" runat="server"><%#Eval("InvNoToClaim")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Calimed Amt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="CalimedAmount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCalimedAmount" runat="server"><%#Eval("CalimedAmount")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Faulty Pick Up Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="FaultyPickUpStatus">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFaultyPickUpStatus" runat="server"><%#Eval("FaultyPickUpStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Created On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="CreatedOn">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedOn" runat="server"><%#Eval("CreatedOn")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Created By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="CreatedBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" runat="server"><%#Eval("CreatedBy")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CssClass="btn btn-primary btn-mini" CommandName="Select">
                                                              <i class="fa fa-edit"></i>Edit
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" CssClass="btn btn-danger btn-mini" CommandName="Delete"
                                                            Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                              <i class="fa fa-delete"></i>Delete
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="lnkNotes" runat="server" CausesValidation="false" CssClass="btn btn-primary btn-mini" CommandName="Notes" CommandArgument='<%# Eval("TicketNo") %>'>
                                                              <i class="fa fa-sticky-note-o"></i>Notes
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="verticaaline" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("TicketNo") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("TicketNo") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td style="width: 10px"><b>Order Item</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="lblOrderItem" runat="server"><%#Eval("OrderItem")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 10px"><b>Notes</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="lblNotes" runat="server"><%#Eval("Notes")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 10px"><b>Notes Created By</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblNotesCreatedBy" runat="server"><%#Eval("NotesCreatedBy")%></asp:Label>
                                                                            </td>
                                                                            <td style="width: 10px"><b>Notes Created On</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblNotesCreatedOn" runat="server"><%#Eval("NotesCreatedOn")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                <div class="pagination">

                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid printorder" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth">Delete
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                            </h5>
                        </div>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:Button ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" Text="Ok" />
                            <asp:Button ID="LinkButton7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                        </div>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="hdndelete" runat="server" />

            <!-- FollowUp Model-->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divModalNote" TargetControlID="Button12" CancelControlID="Button11">
            </cc1:ModalPopupExtender>
            <div id="divModalNote" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Notes</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:HiddenField ID="hndTicketNo" runat="server" />
                                        <asp:TextBox ID="txtNotes" Height="100px" TextMode="MultiLine" runat="server" CssClass="form-control m-b modaltextbox" placeholder="Notes"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="txtNotes" Display="Dynamic" ValidationGroup="WholesaleNotes"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <asp:Panel ID="panel3" runat="server" CssClass="xsroll">
                                        <div id="PanGridNotes" runat="server">
                                            <div style="max-height: 230px; overflow: auto">
                                                <%--<div class="card-block">--%>
                                                    <div class="table-responsive BlockStructure">
                                                        <asp:GridView ID="GridViewNote" DataKeyNames="id" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" AutoGenerateColumns="false" OnRowCommand="GridViewNote_RowCommand" OnRowDeleting="GridViewNote_RowDeleting">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                                    SortExpression="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNoteDate" runat="server" Width="50px"><%#Eval("CreatedOn")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Notes">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Notes" runat="server" Width="80px"><%#Eval("Notes")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Added By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Added By">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCreatedBy" runat="server" Width="80px"><%#Eval("CreatedBy")%>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="10px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("id")%>'
                                                                            OnClientClick="return confirm('Are you sure you went to delete Notes?');" Visible='<%# Eval("DelFlag").ToString() == "1" ? true : false %>'>
                                                                    <i class="fa fa-trash"></i> Delete
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                <%--</div>--%>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSaveNotes" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Save" OnClick="btnSaveNotes_Click"></asp:Button>
                            <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button12" Style="display: none;" runat="server" />
            <!-- End model -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
