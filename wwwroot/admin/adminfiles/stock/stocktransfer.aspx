<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stocktransfer.aspx.cs" Inherits="admin_adminfiles_stock_stocktransfer"
    EnableEventValidation="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <script>
        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 100);
        }
        $(function () {
            $('form').on("click", '.POPupLoader', function () {
                ShowProgress();
            });
        });

        function doMyAction() {

            $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();
            });
        }

    </script>
    <script>
        function printContent() {
<%--                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();--%>
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>

    <script>               
        //function openModal() {
        //    $('[id*=modal_danger]').modal('show');
        //}



        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);

        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);

        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {


            //shows the modal popup - the update progress
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {

            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {
            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $("[data-toggle=tooltip]").tooltip();
            /* $('.datetimepicker1').datetimepicker({
                 format: 'DD/MM/YYYY'
             });*/
            $(".myvalstocktransfer").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true,
                minimumResultsForSearch: -1
            });

            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>

    <link rel="stylesheet" type="text/css" href="../../../admin/theme/assets/css/print.css" media="print" />
    <asp:UpdatePanel ID="updatepanelgrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Stock Transfer
                                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>

                        <div id="hbreadcrumb" class="pull-right">

                            <asp:LinkButton ID="lnkAdd" runat="server" Visible="false" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" Visible="false" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>

                        </div>
                    </h5>
                    <div class="clear"></div>


                </div>
            </div>
            <div class="supreme-container printorder">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="true">
                        <div class="panel-body animate-panel padtopzero">
                            <%--<div>
                <div class="row">
                    <div class="">--%>
                            <%-- <a href="../../css/print.css"></a>
                         <a href="../../../css/style.css"></a>--%>
                            <%-- <div class="col-lg-12">--%>
                            <%-- <div class="hpanel marbtmzero">--%>
                            <%-- <div class="panel-heading">--%>

                            <div class="card addform">
                                <div class="card-header bordered-blue">
                                    <h5>
                                        <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                        Stock Transfer
                                    </h5>
                                </div>

                                <div class="card-block padleft25">
                                    <div class="form-horizontal">

                                        <div class="form-group row">
                                            <div class="col-md-2">
                                                <asp:Label ID="Label4" runat="server">                                                            
                                                                Transfered&nbsp;Date</asp:Label>
                                                <%--    <div class="amtvaluebox form-control">
                                                    <asp:Literal ID="" runat="server"></asp:Literal>
                                                </div>--%>
                                                <asp:TextBox ID="ltTransfered" runat="server" ReadOnly="true" MaxLength="200" class="form-control modaltextbox" AutoPostBack="true"></asp:TextBox>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblLastName" runat="server">
                                                                Entered By</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlemployee" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlemployee" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">

                                                <span>Tracking&nbsp;No.
          
                                                </span>

                                                <asp:TextBox ID="txttracking" runat="server" MaxLength="200" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txttracking_TextChanged"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" ControlToValidate="txttracking"
                                                    Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblTransferCompany" runat="server">
                                                                Transfer Company</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlTransferCompany" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Transfer Company</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlTransferCompany" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label1" runat="server">
                                                                Transfer&nbsp;From</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddltransferfrom" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer" AutoPostBack="true" OnSelectedIndexChanged="ddltransferfrom_SelectedIndexChanged">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddltransferfrom" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label3" runat="server">
                                                                Transfer&nbsp;To</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddltransferto" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer" OnSelectedIndexChanged="ddltransferto_SelectedIndexChanged1">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddltransferto" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-2" id="DivLocation" runat="server" visible="false">
                                                <asp:Label ID="lblLocation" runat="server">
                                                                Location</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtLocation" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblAmount" runat="server">
                                                                Amount</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAmount" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-2">
                                                <asp:Label ID="lblCompany" runat="server">
                                                                Company</asp:Label>
                                                <div class="drpValidate">
                                                     <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Company</asp:ListItem>
                                                        <asp:ListItem Value="1">Ariser</asp:ListItem>
                                                        <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                        <asp:ListItem Value="3">Wholesale</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlCompany" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="display: none">


                                            <div class="col-sm-3">
                                                <div class="custom_datepicker">
                                                    <asp:Label ID="Label23" runat="server">
                                                                        Date&nbsp;Received</asp:Label>
                                                    <div class="input-group date" data-provide="datepicker">

                                                        <asp:TextBox ID="txtdatereceived" runat="server" class="form-control" placeholder="Date Received" type="text">
                                                        </asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>

                                        </div>


                                        <%--<div class="graybgarea">--%>
                                        <asp:HiddenField runat="server" ID="hdnstockId" />
                                        <div class="form-group row">
                                            <div class="col-sm-12 set_w99">
                                                <asp:Repeater ID="rptstock" runat="server" OnItemDataBound="rptstock_ItemDataBound">
                                                    <ItemTemplate>
                                                        <div class="form-group row">
                                                            <div class="col-sm-3">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Stock
                                                                    </label>
                                                                </span>

                                                                <span>
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type")%>' />
                                                                    <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("StockTransferItemsID") %>' />
                                                                    <asp:HiddenField ID="hndcategory" runat="server" Value='<%#Eval("StockCategoryID") %>' />
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlcategory" runat="server" aria-controls="DataTables_Table_0" class="myvalstocktransfer"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged"
                                                                            AppendDataBoundItems="true">
                                                                            <asp:ListItem Value="">Select Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlcategory" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Stock Item</label>
                                                                </span>
                                                                <span>
                                                                    <asp:HiddenField ID="hdnstockcode" runat="server" Value='<%#Eval("StockCode") %>' />
                                                                    <%--<asp:HiddenField ID="hdnstockcode" runat="server" Value='<%#Eval("StockItemID") %>' />--%>
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlitem" runat="server" aria-controls="DataTables_Table_0" class="myvalstocktransfer"
                                                                            AppendDataBoundItems="true" Style="width: 60%!important;" OnSelectedIndexChanged="ddlitem_SelectedIndexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlitem" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>
                                                                        Quantity</label>
                                                                </span>


                                                                <span>
                                                                    <asp:HiddenField ID="hdnQty" runat="server" Value='<%#Eval("TransferQty") %>' />
                                                                    <asp:TextBox ID="txtqty" runat="server" CssClass="form-control modaltextbox" MaxLength="7" OnTextChanged="txtqty_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="Req" ForeColor="#FF5370"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                                        ControlToValidate="txtqty" Display="Dynamic" ValidationGroup="Req" ForeColor="#FF5370"></asp:RequiredFieldValidator>
                                                                </span>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <span class="name disblock">
                                                                    <label>Available Qty</label>
                                                                </span>
                                                                <span>
                                                                    <asp:HiddenField ID="hdnAvailableQuantity" runat="server" Value='<%#Eval("AvailableQty") %>' />
                                                                    <asp:TextBox ID="txtAvailableQuantity" ReadOnly="true" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                                </span>

                                                            </div>

                                                            <div class="col-sm-3">
                                                                <div style="padding-top: 28px">
                                                                    <span class="name ">
                                                                        <%--  <asp:ImageButton ID="lbremove" runat="server" OnClientClick="return confirm('Are you sure you want to delete This Record?  ');" CausesValidation="false" data-placement="top" OnClick="lbremove_Click"
                                                                                data-toggle="tooltip" data-original-title="Delete" ImageUrl="~/admin/images/icon_delet.png"></asp:ImageButton><br />--%>
                                                                        <asp:Button ID="lbremove" runat="server" OnClick="lbremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                            CausesValidation="false" /><br />
                                                                    </span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="">
                                                    <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddBoxRow_Click" CssClass="btn btn-primary redreq btnaddicon"
                                                        CausesValidation="false" />
                                                </div>
                                            </div>

                                        </div>
                                        <%--</div>--%>





                                        <div class="form-group row">
                                            <div class="col-sm-10">
                                                <asp:Label ID="Label5" runat="server">
                                                                Notes</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtnote" runat="server" MaxLength="200" class="form-control modaltextbox" Width="100%"
                                                        Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtnote"
                                                        Display="None" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group row">
                                            <div class="col-sm-10 text-center">
                                                <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" ValidationGroup="Req" />
                                                <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" CausesValidation="true" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" ValidationGroup="Req" />
                                                <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%-- </div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%-- </div>
                </div>--%>
                    </div>
                </div>
            </div>
            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class=" animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Tracking Number already exists.</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block ">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                                            <div class="row">

                                                <div class="input-group col-sm-2" style="max-width: 160px; display: none;">
                                                    <asp:DropDownList ID="ddlby" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select By</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtstockitemmodelfilter" runat="server" placeholder="Stock Item/Model" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemmodelfilter"
                                                        WatermarkText="Stock Item/Model" />
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txttsfrNo" runat="server" placeholder="Transfer No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txttsfrNo"
                                                        WatermarkText="Transfer No" />
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txttsfrNo" FilterType="Numbers" />
                                                </div>
                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlsearchtransferfrom" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Location From</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2" style="max-width: 160px;">
                                                    <asp:DropDownList ID="ddlsearchtransferto" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Location To</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlTransferedOrNot" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="False" Selected="True">Not Transferred</asp:ListItem>
                                                        <asp:ListItem Value="True">Transferred</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1" style="max-width: 200px;">
                                                    <asp:DropDownList ID="ddlReceived" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="0" Selected="True">Not Received</asp:ListItem>
                                                        <asp:ListItem Value="1">Received</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlSearchTransferCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Transfer Company</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Received Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Transfer Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="input-group col-sm-2" style="max-width: 200px;">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>

                                            </div>

                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2" style="max-width: 180px;">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstocktransfer">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>


                <div class="finalgrid">
                    <asp:Panel ID="panel" runat="server">
                        <div class="page-header card" id="divtot" runat="server" visible="false">
                            <div class="card-block brd_ornge">
                                <div class="printorder" style="font-size: medium">
                                    <b>Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Followmont:&nbsp;</b><asp:Literal ID="lblFollowmont" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>TNT:&nbsp;</b><asp:Literal ID="lblTNT" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Mainfreight:&nbsp;</b><asp:Literal ID="lblMainfreight" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>IN Person:&nbsp;</b><asp:Literal ID="lblINPerson" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Hi-Trans:&nbsp;</b><asp:Literal ID="lblHiTrans" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                        <div class="xscroll">
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div>
                                            <div class="table-responsive BlockStructure">
                                                <asp:GridView ID="GridView1" DataKeyNames="StockTransferID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                    OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" OnRowDataBound="GridView1_RowDataBound" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Tsfr No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" SortExpression="StockTransferNumber" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>

                                                                <%#Eval("StockTransferNumber")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="80px" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transfer Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" SortExpression="TransferDate" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Entered By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="TransferByName">
                                                            <ItemTemplate>
                                                                <%#Eval("TransferByName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyName">
                                                            <ItemTemplate>
                                                                <%#Eval("CompanyName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transfer From" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="LocationFromName">
                                                            <ItemTemplate>
                                                                <%#Eval("LocationFromName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" Width="130px" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Transfer To" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px" SortExpression="LocationToName">
                                                            <ItemTemplate>
                                                                <%#Eval("LocationToName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Items Transferred" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="TranferedItems">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label645" runat="server" data-placement="top" data-original-title='<%#Eval("TranferedItems")%>' data-toggle="tooltip"
                                                                    Width="260px"><%#Eval("TranferedItems")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Qty">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbtnQty" runat="server" data-placement="top" data-original-title='<%#Eval("Qty")%>' data-toggle="tooltip"
                                                                    Width="40px"><%#Eval("Qty")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Transport Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px" SortExpression="TransferCompany" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%#Eval("TransferCompany")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cornet No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px" SortExpression="TrackingNumber" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%#Eval("TrackingNumber")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" SortExpression="Amount">
                                                            <ItemTemplate>
                                                                <asp:Literal runat="server" ID="ltrAmount" Text='<%# Eval("Amount")%>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transferred" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="TransferByDate" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>

                                                                <asp:Literal runat="server" ID="lttransfered" Text='<%# DataBinder.Eval(Container.DataItem, "TransferByDate", "{0:dd MMM yyyy}")%>'></asp:Literal>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Transferred By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" SortExpression="TransferByName1">
                                                            <ItemTemplate>

                                                                <asp:Literal runat="server" ID="lttranferbyname" Text='<%# Eval("TransferByName1")%>'></asp:Literal>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Received" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="ReceivedByDate" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>

                                                                <asp:Literal runat="server" ID="ltrecdt" Text='<%# DataBinder.Eval(Container.DataItem, "ReceivedByDate", "{0:dd MMM yyyy}")%>'></asp:Literal>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Received By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" SortExpression="ReceivedByName1">
                                                            <ItemTemplate>

                                                                <asp:Literal runat="server" ID="ltrecdt1" Text='<%# Eval("ReceivedByName1")%>'></asp:Literal>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        

                                                        <asp:TemplateField HeaderText="Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hyptransferfrom" runat="server" NavigateUrl='<%# "~/admin/adminfiles/stock/additemqty.aspx?StockTransferID="+Eval("StockTransferID") %>'
                                                                    data-toggle="tooltip" data-placement="top" data-original-title="New Item">
                                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/addinvoice.png" />
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="center"
                                                            ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField Value='<%#Eval("StockTransferID") %>' runat="server" ID="hdnstockid" />
                                                                <%--Visible='<%#Eval("Received").ToString()=="False"?true:false %>'--%>
                                                                <asp:LinkButton ID="lnkReceived" CausesValidation="false" CssClass="btn btn-yellow btn-mini" Visible="false" runat="server" OnClick="lnkReceived_Click" data-toggle="tooltip" data-placement="top" data-original-title="Received">
                                                         <i class=" fa fa-tasks"></i>  Received     </asp:LinkButton>
                                                                <%-- <asp:LinkButton ID="lnkrevert" CausesValidation="false" Visible='<%#Eval("Received").ToString()=="True"?true:false %>' CssClass="btn btn-maroon btn-xs wdth100" runat="server" OnClick="lnkrevert_Click" data-toggle="tooltip" data-placement="top" data-original-title="Revert">
                                                       <i class="fa fa-retweet"></i> Revert      
                                                            </asp:LinkButton>--%>
                                                                <%-- <asp:Image ID="Image31" runat="server" ImageUrl="~/images/recieved.png" Height="25px" />--%>

                                                                <asp:LinkButton ID="lbtnTsfrNo" runat="server" CommandArgument='<%# Eval("StockTransferNumber") %>' CommandName="print"
                                                                    data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-mini">
                                                                <i class="fa fa-link"></i> Detail
                                                               <%-- <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icon_detail.png" />--%>
                                                                </asp:LinkButton>


                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" Visible='<%#Eval("TransferByDate").ToString()=="" || Roles.IsUserInRole("Administrator") ?true:false%>'
                                                                    CssClass="btn btn-info btn-mini" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i>Edit</asp:LinkButton>

                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("StockTransferID")%>' CssClass="btn btn-danger btn-mini" CausesValidation="false"><i class="fa fa-trash"></i>Delete
                                                                </asp:LinkButton>



                                                                <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-mini" CommandName="printpage" CommandArgument='<%#Eval("StockTransferID")%>' OnClick="gvbtnPrint_Click" CausesValidation="false" data-original-title="Print" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-print"></i> Print
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="gvlnkDownload" runat="server" CommandName="Download" CausesValidation="false" CommandArgument='<%#Eval("StockTransferID") + ";" + Eval("LocationFrom") + ";" + Eval("StockTransferNumber") %>'
                                                                    CssClass="btn btn-info btn-mini" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" Visible='<%#Eval("IsStockOut").ToString()=="1"?true:false%>'>
                                                            <i class="fa fa-download"></i> Download</asp:LinkButton>

                                                                <asp:LinkButton runat="server" ID="lbtnView" CommandName="View" CommandArgument='<%#Eval("StockTransferID") + ";" + Eval("Received")%>' CssClass="btn btn-success btn-mini " Style="color: white;" CausesValidation="false"
                                                                    data-toggle="tooltip" data-placement="top" title="View" data-original-title="View" Visible='<%# Eval("IsStockOut").ToString() == "0" ? false : true %>'>
                                                                         <i class="fa fa-eye"></i> View
                                                        </asp:LinkButton>

                                                                <asp:LinkButton ID="lbtnExportSerialNo" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title="Export Serial No" class="btn btn-success btn-mini Excel"
                                                                CausesValidation="false" OnClick="lbtnExportSerialNo_Click" CommandArgument='<%# Eval("StockTransferID") %>' Style="background-color: #218838; border-color: #218838;"
                                                                    Visible='<%# Eval("IsStockOut").ToString() == "1" && Eval("Received").ToString() == "False" ? true : false %>' ><i class="fa fa-file-excel-o"></i> SerialNo </asp:LinkButton>

                                                                <asp:LinkButton ID="lbtnPdf" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title="Barcode PDF" class="btn btn-success btn-mini"
                                                                CausesValidation="false" OnClick="lbtnPdf_Click" CommandArgument='<%# Eval("StockTransferID") %>'
                                                                    Visible='<%# Eval("IsStockOut").ToString() == "1" && Eval("Received").ToString() == "False" ? true : false %>' ><i class="fa fa-file-pdf-o"></i> Pdf </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle />
                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn btn-mini"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn btn-mini"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid printorder" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                            <div class="paginationnew1" runat="server" id="divnopage">
                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>

                </div>
            </div>







            <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderTracker" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="divEmailCheck" DropShadow="false" CancelControlID="ibtnCancelActive" OkControlID="btnOKMobile" TargetControlID="btnNULLData3">
            </cc1:ModalPopupExtender>
            <div id="divEmailCheck" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false" Visible="false"
                                    runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="ibtnCancelActive_Click">
                   Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title center-text" id="H2">Project Status</h4>
                        </div>
                        <%--     <asp:Button ID="ibtnCancel" runat="server" OnClick="ibtnCancel_Click1" />close--%>
                        <%--  <button id="ibtnCancel" runat="server" onclick="" type="button" class="close" data-dismiss="modal" causesvalidation="false"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>--%>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                    <tbody>
                                        <tr align="center">
                                            <td>
                                                <h3 class="noline"><b>There is a Stock in the database already who has this tracking number.
                                                </b></h3>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                                    Text="Dupe" CausesValidation="false" />
                                                <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                                    CssClass="btn" Text=" OK " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="tablescrolldiv">
                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                        <asp:GridView ID="GridView2" DataKeyNames="StockTransferID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Transfer Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="TransferDate">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="120px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tsfr No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="StockTransferNumber">
                                                    <ItemTemplate>
                                                        <%#Eval("StockTransferNumber")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Transfer From" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="LocationFromName">
                                                    <ItemTemplate>
                                                        <%#Eval("LocationFromName")%>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="brdnoneleft" Width="150px" />
                                                    <HeaderStyle CssClass="brdnoneleft" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Transfer To" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" SortExpression="LocationToName">
                                                    <ItemTemplate>
                                                        <%#Eval("LocationToName")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <asp:Button ID="Button2" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnNo" DropShadow="false" PopupControlID="divstockdetail" TargetControlID="Button2">
            </cc1:ModalPopupExtender>
            <div id="divstockdetail" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog" style="max-width: 300px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="Button3_Click">
                        Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H1" runat="server">
                                <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                Stock Transfer Details</h4>

                        </div>
                        <div class="modal-body paddnone" runat="server" id="divdetail" visible="false">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="">
                                        <div class="" style="background: none!important;">
                                            <div>
                                                <div class="col-md-12">
                                                    <div class="form-group wd100">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                            </label>
                                                        </span><span>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                <thead>
                                                                    <tr>
                                                                        <td style="width: 70%">Stock Item
                                                                        </td>
                                                                        <td style="width: 20%" class="center-text">Transfer Qty
                                                                        </td>
                                                                    </tr>
                                                                </thead>
                                                                <asp:Repeater ID="rptstockdetail" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 70%;">
                                                                                <%#Eval("StockItem")%>
                                                                            </td>
                                                                            <td style="width: 20%" class="center-text">
                                                                                <%#Eval("TransferQty")%>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group marginleft text-center col-md-12">
                                                    <asp:Button class="btn btn-primary addwhiteicon" ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes" Visible="false" />
                                                    <asp:Button class="btn btn-primary addwhiteicon" ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="Return" Visible="false" />
                                                    <asp:Button class="btn btn-primary addwhiteicon" ID="btntansite" runat="server" OnClick="btntansite_Click" Text="Transite" Visible="false" />
                                                    <asp:Button class="btn btn-primary addwhiteicon" ID="btnNo" runat="server" OnClick="btnNo_Click" Text="No" CausesValidation="false" Visible="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body paddnone" runat="server" id="divdetailmsg" visible="false">
                            <div class="panel-body" style="overflow: scroll;">
                                <div class="formainline">
                                    <div class="panel panel-default">
                                        <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group ">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                            </label>
                                                        </span><span>

                                                            <div class="messesgarea">
                                                                <div class="alert alert-info" id="Div1" runat="server">
                                                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="LinkButton6" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup">

                <div class="modal-dialog">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <h5 class="modal-title" id="H2">Delete</h5>
                            <%-- <div style="float: right">
                                <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"> <span aria-hidden="true">x</span></asp:LinkButton>
                            </div>--%>
                        </div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL" CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup modal-danger modal-message ">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                                <asp:LinkButton ID="gvbtnPrintmodal" runat="server" CssClass="btn btn-success btn-xs" OnClientClick="javascript:printContent();" CausesValidation="false">
                                                              <i class="fa fa-print"></i> Print
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">

                                        <table class="table table-bordered">
                                            <tr>
                                                <td align="left">
                                                    <strong>From&nbsp;Location:&nbsp;</strong>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                                                <td align="left"><strong>To&nbsp;Location:&nbsp;</strong></td>
                                                <td align="left">
                                                    <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                            </tr>

                                            <tr>
                                                <td align="left"><strong>Transfer Date:</strong> </td>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label></td>
                                                <td align="left"><strong>Transfer By:</strong> </td>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferBy" runat="server"></asp:Label></td>
                                            </tr>

                                        </table>
                                        <br />

                                        <div class="qty marbmt25">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="5%" align="center">Qty</th>
                                                    <th width="20%" align="left">Code</th>
                                                    <th align="left">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblQty" runat="server"><%#Eval("TransferQty") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                        <br />

                                        <div>
                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="printarea table table-bordered">
                                                <tr>
                                                    <td><strong>Total Qty :</strong>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td width="20%"><strong>Tracking No :</strong>
                                                    </td>
                                                    <td width="40%">
                                                        <asp:Label ID="lblTrackingNo" runat="server"></asp:Label></td>
                                                    <td width="20%"><strong>Received Date :</strong> </td>
                                                    <td>
                                                        <asp:Label ID="lblReceivedDate" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="2"><strong>Notes :</strong>
                                                    </td>


                                                    <td><strong>Received By :</strong></td>
                                                    <td>
                                                        <asp:Label ID="lblReceivedBy" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label></td>

                                                </tr>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <asp:HiddenField ID="hdndelete" runat="server" />
            <!--End Danger Modal Templates-->
            <asp:HiddenField runat="server" ID="hdnstocktransferid" />

            <!-- New View Button Code -->

            <cc1:ModalPopupExtender ID="ModalPopupExtenderView" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="Div8" TargetControlID="Button7" CancelControlID="Button6">
            </cc1:ModalPopupExtender>
            <div id="Div8" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="color-line "></div>
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalTitle">Transfered Serial Number
                                <div style="float: right" class="printorder">
                                    <asp:Button ID="Button6" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />
                                   <%-- <asp:LinkButton ID="lbtnPDF" runat="server" CssClass="btn btn-success" CausesValidation="false">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF
                                    </asp:LinkButton>--%>
                                    <asp:LinkButton ID="lbtnExcelSerialNo" runat="server" data-toggle="tooltip" class="btn btn-success" OnClick="lbtnExcelSerialNo_Click" 
                                        CausesValidation="false" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                <asp:HiddenField runat="server" ID="hndSerialNoTransferID" />
                                <asp:HiddenField runat="server" ID="hndReceived" />
                                </div>
                            </h5>
                        </div>
                        <div class="modal-body paddnone" style="width: auto">
                            <div class="panel-body" id="Div9" runat="server">
                                <div class="formainline" style="width: auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                <tr id="tr1" runat="server">
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                            <tr>
                                                                <td><b>#</b></td>
                                                                <td><b>Category</b></td>
                                                                <td><b>Stock Item</b></td>
                                                                <td><b>Serial Number</b></td>
                                                            </tr>
                                                            <asp:Repeater ID="rptSerialNo" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%#Eval("id") %></td>
                                                                        <td><%#Eval("StockCategory") %></td>
                                                                        <td><%#Eval("StockItem") %></td>
                                                                        <td><%#Eval("SerailNo") %></td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />


        </ContentTemplate>
        <Triggers>
            <%-- <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="lnkdelete" />--%>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%-- <asp:PostBackTrigger ControlID="LinkButton6" />--%>
            <%-- <asp:AsyncPostBackTrigger ControlID="btnAdd" />--%>
             <%--<asp:AsyncPostBackTrigger ControlID="gvlnkDownload" />--%>
            <asp:PostBackTrigger ControlID="GridView1" />
            <asp:PostBackTrigger ControlID="lbtnExcelSerialNo" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
