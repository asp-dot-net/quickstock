using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockorder : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static int AddButtonDisable = 0;
    protected string mode = "";
    protected string SiteURL;
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            
            BindLocation();
            BindVendor();
            BindPurchaseCompany();
            BindTransCompany();

            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();

            BindGrid(0);

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Installation Manager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else if ((Roles.IsUserInRole("WarehouseManager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //  GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                // GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }
        }

        ModeAddUpdate();
    }

    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlStockLocation.DataSource = dt;
        ddlStockLocation.DataTextField = "location";
        ddlStockLocation.DataValueField = "CompanyLocationID";
        ddlStockLocation.DataBind();

        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();

        //ddlloc.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddlloc.DataTextField = "location";
        //ddlloc.DataValueField = "CompanyLocationID";
        //ddlloc.DataMember = "CompanyLocationID";
        //ddlloc.DataBind();
        rptLocation.DataSource = dt;
        rptLocation.DataBind();
    }

    public void BindPurchaseCompany()
    {
        DataTable dt = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        ddlpurchasecompany.DataSource = dt;
        ddlpurchasecompany.DataTextField = "PurchaseCompanyName";
        ddlpurchasecompany.DataValueField = "Id";
        ddlpurchasecompany.DataBind();

        //ddlpurchasecompanysearch.DataSource = dt;
        //ddlpurchasecompanysearch.DataTextField = "PurchaseCompanyName";
        //ddlpurchasecompanysearch.DataValueField = "Id";
        //ddlpurchasecompanysearch.DataBind();

        rptCompany.DataSource = dt;
        rptCompany.DataBind();

    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();
        ddlVendor.DataSource = dt;
        ddlVendor.DataTextField = "Customer";
        ddlVendor.DataValueField = "CustomerID";
        ddlVendor.DataBind();

        //ddlSearchVendor.DataSource = dt;
        //ddlSearchVendor.DataTextField = "Customer";
        //ddlSearchVendor.DataValueField = "CustomerID";
        //ddlSearchVendor.DataBind();
        rptVender.DataSource = dt;
        rptVender.DataBind();



    }

    public void BindTransCompany()
    {
        DataTable dt = ClstblStockOrders.tbl_Transport_CompMaster_GetData();
        ddlSearchTransCompName.DataSource = dt;
        ddlSearchTransCompName.DataValueField = "Trans_Comp_Id";
        ddlSearchTransCompName.DataTextField = "Trans_Company_Name";
        ddlSearchTransCompName.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        #region getCheckbox Item
        string Location = "";
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Location += "," + hdnModule.Value.ToString();
            }
        }
        if (Location != "")
        {
            Location = Location.Substring(1);
        }

        string PCompany = "";
        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                PCompany += "," + hdnModule.Value.ToString();
            }
        }
        if (PCompany != "")
        {
            PCompany = PCompany.Substring(1);
        }

        string Vender = "";
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Vender += "," + hdnModule.Value.ToString();
            }
        }
        if (Vender != "")
        {
            Vender = Vender.Substring(1);
        }
        #endregion
        //dt = ClstblStockOrders.tblStockOrders_SelectBySearch(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), ddlloc.SelectedValue, "", "");
        dt = ClstblStockOrders.tblStockOrders_SelectBySearchNewQuickStock(ddlShow.SelectedValue, ddlDue.SelectedValue, Vender, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), Location, "", txtstockitemfilter.Text, ddlLocalInternation.SelectedValue, txtStockContainerFilter.Text, PCompany, ddlitemnamesearch.SelectedValue, ddlDelevery.SelectedValue, ddlStockOrderStatus.SelectedValue, ddlPaymentMethod.SelectedValue, ddlSearchTransCompName.SelectedValue, ddlPartial.SelectedValue, "", "");

        //dt = ClstblStockOrders.tblStockOrders_GetData(txtSearchOrderNo.Text, txtstockitemfilter.Text, Location, ddlStockOrderStatus.SelectedValue, PCompany, Vender, ddlShow.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view.");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + (dt.Rows.Count - 1) + " entries";
                }
            }

        }
        bind(dt);
    }

    public void bind(DataTable dt)
    {
        //DataTable dt = new DataTable();
        //dt = GetGridData();

        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockOrderID"].ToString()))
            {

                PanGrid.Visible = false;
                divnopage.Visible = false;
                Notification("There are no items to show in this view.");
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int exist = ClstblStockOrders.tblStockOrders_Exists_VendorInvoiceNo(txtVendorInvoiceNo.Text);
        int existManualNo = ClstblStockOrders.tblStockOrders_Exists_ManualOrderNo(txtManualOrderNumber.Text);
        int existContainerNo = ClstblStockOrders.tblStockOrders_Exists_StockContainer(txtstockcontainer.Text);

        if (exist == 0)
        {
            //if (existManualNo == 0)
            //{
            if (existContainerNo == 0)
            {
                string StockLocation = ddlStockLocation.SelectedValue.ToString();
                // string StockCategoryID = ddlStockCategoryID.SelectedValue.ToString();

                string DateOrdered = DateTime.Now.AddHours(14).ToString();

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

                string Notes = txtNotes.Text;

                string BOLReceived = "";
                if (txtBOLReceived.Text != "")
                {
                    BOLReceived = Convert.ToDateTime(txtBOLReceived.Text).ToString("yyyy/MM/dd");
                }

                string ExpectedDelivery = "";
                if (txtExpectedDelivery.Text != "")
                {
                    ExpectedDelivery = Convert.ToDateTime(txtExpectedDelivery.Text).ToString("yyyy/MM/dd");
                }

                // DateTime BOLReceived = DateTime.ParseExact(txtBOLReceived.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //string BOLReceived = DateTime.ParseExact(txtBOLReceived.Text.ToString(),
                //              "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture).ToShortDateString();
                string vendor = ddlVendor.SelectedValue.ToString();
                string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
                string ManualOrderNumber = txtManualOrderNumber.Text;
                //string purchasecompany = ddlpurchasecompany.SelectedValue.ToString();
                //string itemnameid = ddlitemname.SelectedItem.ToString();
                //string itemnamevalue = ddlitemname.SelectedValue.ToString();
                //string ExpectedDelivery = txtExpectedDelivery.Text;


                var vals = "";
                var vals1 = "";

                if (ddlStockLocation.SelectedValue != string.Empty)
                {
                    vals = StockLocation_Text.Split(':')[0];
                    vals1 = StockLocation_Text.Split(':')[1];
                }
                string state = Convert.ToString(vals);
                //string CompanyLocation = Convert.ToString(vals1).Substring(1);
                //int OrderStatus = 0;
                //if (chkDeliveryOrderStatus.Checked == true)
                //{
                //    OrderStatus = 1;
                //}
                int success = ClstblStockOrders.tblStockOrders_Insert(DateOrdered, st.EmployeeID, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
                if (success > 0)
                {
                    bool s1 = ClstblStockOrders.tblStockOrders_Update_userid(Convert.ToString(success), userid);
                }
                ClstblStockOrders.tblStockOrders_Update_OrderNumber(success.ToString(), success.ToString());
                ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(success.ToString(), txtVendorInvoiceNo.Text);
                //ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(success.ToString(), Convert.ToInt32(ddlorderstatus.SelectedValue));
                ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(success.ToString(), ddlorderstatusnew.SelectedValue);
                ClstblStockOrders.tblStockOrders_Update_DeliveryOrderStatusNew(success.ToString(), ddlorderstatusnew.SelectedValue);
                ClstblStockOrders.tblStockOrders_Update_StockContainer(success.ToString(), txtstockcontainer.Text);
                ClstblStockOrders.tblStockOrders_Update_PurchaseCompanyID(success.ToString(), Convert.ToInt32(ddlpurchasecompany.SelectedValue));
                ClstblStockOrders.tblStockOrders_Update_ItemName(success.ToString(), Convert.ToInt32(ddlitemname.SelectedValue), ddlitemname.SelectedItem.ToString());
                ClstblStockOrders.tblStockOrders_Update_DeleveryType(success.ToString(), Convert.ToInt32(ddldeleverytype.SelectedValue), ddldeleverytype.SelectedItem.ToString());
                ClstblStockOrders.tblStockOrders_Update_TransCharges(success.ToString(), txtcharges.Text);

                if (ddlOrdStatus.SelectedValue != null && ddlOrdStatus.SelectedValue != "")
                {
                    bool ord = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(success.ToString(), ddlOrdStatus.SelectedValue);
                }

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    if (item.Visible != false)
                    {
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                        TextBox ExpiryDate = (TextBox)item.FindControl("txtexpdate");
                        TextBox txtmodelNo = (TextBox)item.FindControl("txtmodelno");
                        string StockItem = ddlStockItem.SelectedValue.ToString();
                        string OrderQuantity = txtOrderQuantity.Text;
                        //String Amount = txtAmount.Text;
                        string Amount = "0";
                        if (!String.IsNullOrEmpty(txtAmount.Text))
                        {
                            Amount = txtAmount.Text;
                        }

                        //if (ddlorderstatus.SelectedValue == "0")
                        //{
                        //    int GSTamt = Convert.ToInt32(txtAmount.Text) / 10;
                        //    Amount = Convert.ToString(Convert.ToInt32(txtAmount.Text) + GSTamt);
                        //}
                        string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                        if (!chkdelete.Checked)
                        {
                            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "" && txtAmount.Text != "")
                            {
                                int success1 = ClstblStockOrders.tblStockOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, Amount, StockOrderItem, state);
                                bool s1 = ClstblStockOrders.tblStockOrderItems_updateExpAndMondelNo(Convert.ToString(success1), ExpiryDate.Text, txtmodelNo.Text);
                            }
                        }
                    }
                }


                //Change by Suresh on 04-05-2020
                bool updatePaymentMethod = ClstblStockOrders.tblStockOrders_Update_PaymentMethod(success.ToString(), ddlPMethod.SelectedValue);

                bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(success.ToString(), txtTargetDate.Text);

                bool scl = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(success.ToString(), txtpmtduedate.Text);

                bool UpdateVendorDisCount = ClstblStockOrders.tblStockOrders_Update_Discount(success.ToString(), txtVendorDiscount.Text);

                //--- do not chage this code start------
                if (success > 0)
                {
                    SetAdd();
                }
                else
                {
                    SetError();
                }
                BindGrid(0);
                // Reset();
                mode = "Add";
                //--- do not chage this code end------
            }
            else
            {
                Notification("Container No. already exist.");
                txtstockcontainer.Text = string.Empty;
                txtstockcontainer.Focus();
            }
            //}
            //else
            //{
            //    Notification("Manual Order No. already exist.");
            //    txtManualOrderNumber.Text = string.Empty;
            //    txtManualOrderNumber.Focus();
            //}
        }
        else
        {
            Notification("Vendor Invoice No. already exist.");
            txtVendorInvoiceNo.Text = string.Empty;
            txtVendorInvoiceNo.Focus();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        int exist = ClstblStockOrders.tblStockOrders_ExistsByIdVendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
        int existManualNo = ClstblStockOrders.tblStockOrders_ExistsByIdManualOrderNo(id1, txtManualOrderNumber.Text);
        int existContainerNo = ClstblStockOrders.tblStockOrders_ExistsByIdStockContainer(id1, txtstockcontainer.Text);
        if (exist == 0)
        {
            //if(existManualNo==0)
            //{
            if (existContainerNo == 0)
            {

                string Notes = txtNotes.Text;
                string BOLReceived = txtBOLReceived.Text;
                string vendor = ddlVendor.SelectedValue.ToString();
                string StockLocation = ddlStockLocation.SelectedValue.ToString();
                string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
                var vals = StockLocation_Text.Split(':')[0];
                var vals1 = StockLocation_Text.Split(':')[1];
                string state = Convert.ToString(vals);
                string ManualOrderNumber = txtManualOrderNumber.Text;
                string ExpectedDelivery = txtExpectedDelivery.Text;
                int OrderStatus = 0;
                //if (chkDeliveryOrderStatus.Checked == true)
                //{
                //    OrderStatus = 1;
                //}
                if (id1 != null && id1 != "")
                {
                    bool s1 = ClstblStockOrders.tblStockOrders_Update_userid(id1, userid);
                }
                bool success = ClstblStockOrders.tblStockOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
                ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
                //ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, Convert.ToInt32(ddlorderstatus.SelectedValue));
                ClstblStockOrders.tblStockOrders_Update_DeliveryStatus(id1, ddlorderstatusnew.SelectedValue);
                ClstblStockOrders.tblStockOrders_Update_DeliveryOrderStatusNew(id1, ddlorderstatusnew.SelectedValue);
                ClstblStockOrders.tblStockOrders_Update_StockContainer(id1, txtstockcontainer.Text);
                ClstblStockOrders.tblStockOrders_Update_PurchaseCompanyByID(id1, Convert.ToInt32(ddlpurchasecompany.SelectedValue));
                ClstblStockOrders.tblStockOrders_Update_ItemNameByID(id1, Convert.ToInt32(ddlitemname.SelectedValue), ddlitemname.SelectedItem.ToString());
                ClstblStockOrders.tblStockOrders_Update_DeleveryType(id1, Convert.ToInt32(ddldeleverytype.SelectedValue), ddldeleverytype.SelectedItem.ToString());
                ClstblStockOrders.tblStockOrders_Update_TransCharges(id1, txtcharges.Text);
                //foreach (RepeaterItem item in rptviewdata.Items)
                //{
                //    HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
                //    CheckBox chkdelete1 = (CheckBox)item.FindControl("chkdelete1");
                //    if (chkdelete1.Checked)
                //    {
                //        ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
                //    }
                //}

                if (ddlOrdStatus.SelectedValue != null && ddlOrdStatus.SelectedValue != "")
                {
                    bool ord = ClstblStockOrders.tblStockOrders_UpdateStockOrderStatus(id1, ddlOrdStatus.SelectedValue);
                }

                ClstblStockOrders.tblStockOrderItems_whole_Delete(id1);
                foreach (RepeaterItem item in rptattribute.Items)
                {
                    if (item.Visible != false)
                    {
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                        HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
                        TextBox ExpiryDate = (TextBox)item.FindControl("txtexpdate");
                        TextBox txtmodelNo = (TextBox)item.FindControl("txtmodelno");
                        string StockItem = ddlStockItem.SelectedValue.ToString();
                        string OrderQuantity = txtOrderQuantity.Text;
                        string Amount = "0";
                        if (!String.IsNullOrEmpty(txtAmount.Text))
                        {
                            Amount = txtAmount.Text;
                        }

                        //if (ddlorderstatus.SelectedValue == "0")
                        //{
                        //    //int amt = Convert.ToInt32(txtAmount.Text);
                        //    decimal GSTamt = Convert.ToDecimal(txtAmount.Text) / 10;
                        //    Amount = Convert.ToString(Convert.ToDecimal(txtAmount.Text) + GSTamt);
                        //}

                        string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");

                        if (!chkdelete.Checked)
                        {

                            if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "" && txtAmount.Text != "")
                            {
                                int success1 = ClstblStockOrders.tblStockOrderItems_Insert(id1, StockItem, OrderQuantity, Amount, StockOrderItem, state);
                                bool s1 = ClstblStockOrders.tblStockOrderItems_updateExpAndMondelNo(Convert.ToString(success1), ExpiryDate.Text, txtmodelNo.Text);
                            }
                        }

                        if (chkdelete.Checked)
                        {
                            ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
                        }


                    }
                }

                bool updatePaymentMethod = ClstblStockOrders.tblStockOrders_Update_PaymentMethod(id1.ToString(), ddlPMethod.SelectedValue);

                bool UpdateTargetDate = ClstblStockOrders.tblStockOrders_Update_TargetDate(id1.ToString(), txtTargetDate.Text);

                bool scl = ClstblStockOrders.tblStockOrders_UpdatepmtDueDate(id1.ToString(), txtpmtduedate.Text);

                bool UpdateVendorDisCount = ClstblStockOrders.tblStockOrders_Update_Discount(id1.ToString(), txtVendorDiscount.Text);


                //--- do not chage this code Start------
                if (success)
                {
                    SetUpdate();
                }
                else
                {
                    SetError();
                }
                //Reset();
                BindScript();
                BindGrid(0);
            }
            else
            {
                Notification("Container No. already exist.");
                txtstockcontainer.Text = string.Empty;
                txtstockcontainer.Focus();
            }
            //}
            //else
            //{
            //    Notification("Manual Order No. already exist.");
            //    txtManualOrderNumber.Text = string.Empty;
            //    txtManualOrderNumber.Focus();
            //}  
        }
        else
        {
            //InitUpdate();
            // PAnAddress.Visible = true;
            Notification("Vendor Invoice No. already exist.");
            txtVendorInvoiceNo.Text = string.Empty;
            txtVendorInvoiceNo.Focus();
            //txtVendorInvoiceNo.Attributes.Add("onFocus", "DoFocus(this);");
        }

        //Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

        //--- do not chage this code end------
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        AddButtonDisable = 0;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
        mode = "Edit";

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(id);
        try
        {
            ddlStockLocation.SelectedValue = st.CompanyLocationID;
        }
        catch { }
        try
        {
            ddlVendor.SelectedValue = st.CustomerID;

        }
        catch { }

        ddlOrdStatus.SelectedValue = st.StockOrderStatus;
        //if (!string.IsNullOrEmpty(st.DeliveryOrderStatus) && st.DeliveryOrderStatus != "0")
        //{
        //    chkDeliveryOrderStatus.Checked = true;
        //}
        //else
        //{
        //    chkDeliveryOrderStatus.Checked = false;
        //}

        ddlDate.SelectedValue = st.DeliveryOrderStatus;

        if(st.BOLReceived != "")
        {
            txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
        }
        
        txtNotes.Text = st.Notes;

        if(st.ExpectedDelivery != "")
        {
            txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
        }
        txtManualOrderNumber.Text = st.ManualOrderNumber;
        
        txtVendorInvoiceNo.Text = st.VendorInvoiceNo;
        ddlorderstatus.SelectedValue = st.DeliveryOrderStatus;
        ddlorderstatusnew.SelectedValue = st.DeliveryOrderStatusNew;
        txtstockcontainer.Text = st.StockContainer;
        hdnStockOrderID2.Value = id;
        hdnActualDElivery2.Value = st.ActualDelivery;
        txtcharges.Text = st.TransCharges;
        try
        {
            ddlitemname.SelectedValue = st.ItemNameId;
        }
        catch { }
        ddlpurchasecompany.SelectedValue = st.PurchaseCompanyId;
        try
        {
            ddldeleverytype.SelectedValue = st.DeleveryId;
        }
        catch { }

        DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(id);


        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;
        }

        DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(id);
        //if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        {
            if (!string.IsNullOrEmpty(st.ActualDelivery))
            {
                panel234.Enabled = false;
                txtNotes.Enabled = false;
            }
            else
            {
                panel234.Enabled = true;
                txtNotes.Enabled = true;
            }
        }
        else
        {
            panel234.Enabled = true;
            txtNotes.Enabled = true;
        }

        //Changes By Suresh On 01-05-2020
        DataTable dtPaymentMethod = ClstblStockOrders.tblStockOrders_SelectPaymentMethod_ByStockOrderID(id);
        if (dtPaymentMethod.Rows.Count > 0)
        {
            if (dtPaymentMethod.Rows[0]["PaymentMethodID"].ToString() != "0")
            {
                ddlPMethod.SelectedValue = dtPaymentMethod.Rows[0]["PaymentMethodID"].ToString();
            }
            else
            {
                ddlPMethod.SelectedValue = "";
            }

        }
        else
        {
            ddlPMethod.SelectedValue = "";
        }

        DataTable dtTargetDate = ClstblStockOrders.tblStockOrders_SelectTargetDate_ByStockOrderID(id);
        if (dtTargetDate.Rows.Count > 0)
        {
            if (dtTargetDate.Rows[0]["TargetDate"].ToString() != "")
            {
                txtTargetDate.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(dtTargetDate.Rows[0]["TargetDate"]));
            }
        }

        // Changes by Suresh on 11 MAY 2020
        if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(st.ActualDelivery))
        {
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Purchase Manager"))
            {
                btnaddnew.Visible = false;
                foreach (RepeaterItem item1 in rptattribute.Items)
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item1.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item1.FindControl("ddlStockItem");
                    TextBox txtOrderQuantity = (TextBox)item1.FindControl("txtOrderQuantity");
                    TextBox txtAmount = (TextBox)item1.FindControl("txtAmount");
                    //Button btnaddnew = (Button)item1.FindControl("btnaddnew");

                    ddlStockCategoryID.Attributes.Add("disabled", "disabled");
                    ddlStockItem.Attributes.Add("disabled", "disabled");
                    txtOrderQuantity.ReadOnly = true;
                    txtAmount.ReadOnly = true;
                }
            }
        }
        else
        {
            btnaddnew.Visible = true;
        }

        if (st.pmtDueDate != null && st.pmtDueDate != "")
        {
            txtpmtduedate.Text = Convert.ToDateTime(st.pmtDueDate).ToShortDateString();
        }

        if (st.VendorDiscount != "")
        {
            txtVendorDiscount.Text = Convert.ToDecimal(st.VendorDiscount).ToString("F");
        }
        else
        {
            txtVendorDiscount.Text = "0.00";
        }

        ModeAddUpdate();
        //MaxAttribute = 1;
        //bindrepeater();
        //BindAddedAttribute();
        //--- do not chage this code start------
        //lnkBack.Visible = true;
        InitUpdate();
        //--- do not chage this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.ToString() == "Delivered")
        {
            //Response.Write("cbv");
            //Response.End();
            ModalPopupdeliver.Show();
            // Button lnkBtn = (Button)e.CommandSource;    // the button
            //GridViewRow myRow = (GridViewRow)lnkBtn.Parent.Parent;  // the row
            //GridView myGrid = (GridView)sender; // the gridview
            //GridViewRow gvr = (GridViewRow)lnkBtn.NamingContainer;
            string ID = e.CommandArgument.ToString();
            hndid.Value = ID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            //hdnStockOrderID
            //int id = (int)GridView1.DataKeys[gvr.RowIndex].Value;
            //string ID = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, stEmployeeid.EmployeeID, "1");
            //SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);
            //if (success)
            //{
            //    DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
            //            ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
            //            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
            //        }
            //    }
            //    SetDelete();
            //}
            //else
            //{
            //    SetError();
            //}
            //BindGrid(0);

        }

        if (e.CommandName == "Revert")
        {
            ModalPopupRevert2.Show();
            string StockOrderID = e.CommandArgument.ToString();
            hndid2.Value = StockOrderID;
        }

        if (e.CommandName == "print")
        {
            string StockOrderID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/order.aspx?id=" + StockOrderID);
        }

        //if (e.CommandName.ToLower() == "detail")
        //{
        //    //ModalPopupExtenderDetail.Show();
        //    string StockOrderID = e.CommandArgument.ToString();

        //    Response.Redirect("~/admin/adminfiles/stock/StockOrderDelivery.aspx?StockOrderID=" + StockOrderID);   
        //}
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindScript();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        MaxAttribute = 1;
        bindrepeater();
        BindAddedAttribute();
        BindScript();
        PanGridSearch.Visible = false;
        PanGrid.Visible = false;
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Reset();
        InitAdd();
        mode = "Add";
        if (mode == "Add")
        {
            string qr = "select ordernumber from tblstockorders order by stockorderid desc";
            DataTable dt = ClstblCustomers.query_execute(qr);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["ordernumber"].ToString() != null && dt.Rows[0]["ordernumber"].ToString() != "")
                {
                    int ordno = Convert.ToInt32(dt.Rows[0]["ordernumber"].ToString());
                    TextBox4.Text = Convert.ToString(ordno + 1);
                }
            }

            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
        }
        PanGrid.Visible = false;
        //  PanGridSearch.Visible = false;
        BindScript();
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        Notification("Transaction Successful.");
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();

        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        //BindGrid(0);
        ClearAllContent();
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        Notification("Transaction Failed.");
        //PanError.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        rptattribute.DataSource = null;
        rptattribute.DataBind();
        //rptviewdata.DataSource = null;
        //rptattribute.DataBind();
        ddlStockLocation.ClearSelection();
        ddlVendor.ClearSelection();
        txtBOLReceived.Text = "";
        txtNotes.Text = "";
        txtManualOrderNumber.Text = string.Empty;
        txtExpectedDelivery.Text = string.Empty;
        txtpmtduedate.Text = string.Empty;
        //rptviewdata.Visible = false;

        ddlOrdStatus.SelectedValue = "1";

        ddlstockcategory.ClearSelection();
        //txtstockitem.Text = string.Empty;
        //txtbrand.Text = string.Empty;
        //txtmodel.Text = string.Empty;
        //txtseries.Text = string.Empty;
        //txtminstock.Text = string.Empty;
        //chksalestag.Checked = false;
        //chkisactive.Checked = false;
        //txtdescription.Text = string.Empty;
        //txtStockSize.Text = string.Empty;
        txtVendorInvoiceNo.Text = string.Empty;
        txtstockcontainer.Text = string.Empty;
        ddlorderstatus.SelectedValue = "";
        txtstockitemfilter.Text = string.Empty;
        ddlLocalInternation.SelectedValue = "";
        ddldeleverytype.SelectedValue = "";
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            txtqty.Text = "0";
        }
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            txtAmount.Text = "0";
            txtOrderQuantity.Text = "";
            ddlStockItem.SelectedValue = "";
            ddlStockCategoryID.SelectedValue = "";
        }

    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void ddlStockLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

            ddlStockItem.Items.Clear();

            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            if (ddlStockLocation.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "")
            {

                DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
                ddlStockItem.DataSource = dtStockItem;
                ddlStockItem.DataTextField = "StockItem";
                ddlStockItem.DataValueField = "StockItemID";
                ddlStockItem.DataBind();
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(
        //             PanGridSearch,
        //             this.GetType(),
        //             "MyAction",
        //             "doMyAction();",
        //             true);

        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

        if (ddlStockLocation.SelectedValue != "")
        {
            ddlStockItem.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalAlertExpire.Hide();
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        DateTime CurrentDate = Convert.ToDateTime(DateTime.Now.AddHours(14).ToString("dd/MM/yyyy"));
        DateTime CurrentDate_30Dayplus = CurrentDate.AddDays(30);
        TextBox txtModelName = (TextBox)item.FindControl("txtmodelno");
        TextBox txtExpDate = (TextBox)item.FindControl("txtexpdate");
        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue))
        {
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlStockItem.SelectedValue);
            if (!string.IsNullOrEmpty(st.ExpiryDate))
            {
                DateTime ExpiryDate = Convert.ToDateTime(st.ExpiryDate);
                if (ExpiryDate <= CurrentDate_30Dayplus)
                {
                    txtExpDate.Text = ExpiryDate.ToShortDateString();
                    ModalAlertExpire.Show();
                    if (ExpiryDate < CurrentDate)
                    {
                        lblexpire.Text = "Selected Stock Item is already expired on " + ExpiryDate.ToString("dd MMM yyyy") + ".";
                    }
                    else
                    {
                        lblexpire.Text = "Selected Stock Item is going to expire on " + ExpiryDate.ToString("dd MMM yyyy") + ".";
                    }
                }
            }
            if (!string.IsNullOrEmpty(st.StockModel))
            {
                txtModelName.Text = st.StockModel;

            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        //  BindScript();
    }

    protected void btnAddUpdateRow_Click(object sender, EventArgs e)
    {
        InitUpdate();
        AddmoreAttribute();
    }

    protected void rptattribute_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtOrderQuantity");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");
        HiddenField hdnmdlno = (HiddenField)e.Item.FindControl("hdnmdlno");
        HiddenField hdnExpiryDate = (HiddenField)e.Item.FindControl("hdnExpiryDate");
        TextBox txtexpdate = (TextBox)e.Item.FindControl("txtexpdate");
        TextBox txtmodelno = (TextBox)e.Item.FindControl("txtmodelno");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");

        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdnStockOrderItemID = (HiddenField)e.Item.FindControl("hdnStockOrderItemID");


        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(hdnStockCategory.Value, ddlStockLocation.SelectedValue.ToString());
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();
        HiddenField hdnOrderQuantity = (HiddenField)e.Item.FindControl("hdnOrderQuantity");
        HiddenField hdnAmount = (HiddenField)e.Item.FindControl("hndamount");
        Panel PanelRepeater = (Panel)e.Item.FindControl("PanelRepeater");

        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtOrderQuantity.Text = hdnOrderQuantity.Value;
        if (string.IsNullOrEmpty(hdnAmount.Value))
        {
            hdnAmount.Value = "0";
        }
        txtAmount.Text = hdnAmount.Value;

        string Modelno = hdnmdlno.Value;
        txtmodel.Text = Modelno;
        txtexpdate.Text = hdnExpiryDate.Value;

        btnaddnew.Attributes.Remove("disabled");
        litremove.Attributes.Remove("disabled");

        if (e.Item.ItemIndex == 0)
        {
            //btnDelete.Visible = false;
            chkdelete.Visible = false;
            litremove.Visible = false;
            //RequiredFieldValidator11.Visible = true;
            // RequiredFieldValidator1.Visible = true;
            //   RequiredFieldValidator19.Visible = true;
        }
        else
        {
            //btnDelete.Visible = true;
            chkdelete.Visible = true;
            litremove.Visible = true;

            //RequiredFieldValidator11.Visible = false;
            //RequiredFieldValidator1.Visible = false;
            //  RequiredFieldValidator19.Visible = false;
        }

        try
        {
            DataTable dtstock = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hdnStockOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);
            DataTable dtstock2 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(hdnStockOrderID2.Value);
            //SttblStockOrderItems st = ClstblStockOrders.tblStockOrderItems_SelectByStockOrderItemID(hdnStockOrderItemID.Value);
            if (dtstock.Rows.Count == Convert.ToInt32(hdnOrderQuantity.Value))
            {
                PanelRepeater.Enabled = false;
                AddButtonDisable = AddButtonDisable + 1;
                litremove.Attributes.Add("disabled", "");
                btnaddnew.Attributes.Add("disabled", "");
                // ddlStockCategoryID.Enabled = false;
            }
            else
            {
                PanelRepeater.Enabled = true;
                litremove.Attributes.Remove("disabled");
                btnaddnew.Attributes.Remove("disabled");

                //AddButtonDisable = 0;

                //   ddlStockCategoryID.Enabled = true;
            }

            if (dtstock.Rows.Count > 0)
            {
                litremove.Attributes.Add("disabled", "");
                btnaddnew.Attributes.Add("disabled", "");
            }
            else
            {
                litremove.Attributes.Remove("disabled");
                btnaddnew.Attributes.Remove("disabled");
            }
            //if (dtstock2.Rows.Count > 0)
            //{
            //    litremove.Attributes.Add("disabled", "");
            //    btnaddnew.Attributes.Add("disabled", "");
            //}
            //else
            //{
            //    litremove.Attributes.Remove("disabled");
            //    btnaddnew.Attributes.Remove("disabled");
            //}
        }
        catch { }

        // HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");

        //Response.Write(hdnStockCategory.Value);
        //Response.End();

    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        rpttable.Columns.Add("Amount", Type.GetType("System.String"));
        rpttable.Columns.Add("ModelNo", Type.GetType("System.String"));
        rpttable.Columns.Add("ExpiryDate", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");

            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            string date = " ";
            string Model = "";
            if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlStockItem.SelectedValue);

                if (st.ExpiryDate != null && st.ExpiryDate != "")
                {
                    DateTime ExpiryDate = Convert.ToDateTime(st.ExpiryDate);
                    date = ExpiryDate.ToShortDateString();
                }
                Model = st.StockModel;
            }
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["OrderQuantity"] = txtOrderQuantity.Text;
            dr["Amount"] = txtAmount.Text;
            dr["StockOrderItemID"] = hdnStockOrderItemID.Value;
            dr["type"] = hdntype.Value;
            dr["ModelNo"] = Model;
            dr["ExpiryDate"] = date;

            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
            rpttable.Columns.Add("Amount", Type.GetType("System.String"));
            rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("ModelNo", Type.GetType("System.String"));
            rpttable.Columns.Add("ExpiryDate", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["OrderQuantity"] = "";
            dr["Amount"] = "";
            dr["StockOrderItemID"] = "";
            dr["type"] = "";
            dr["ModelNo"] = "";
            dr["ExpiryDate"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    protected void ibtnaddvendor_click(object sender, EventArgs e)
    {

        if (txtCompany.Text != string.Empty)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string employeeid = st.EmployeeID;

            int success = ClstblCustomers.tblCustomers_Insert("", employeeid, "false", "13", "3", "", "", "", "1", employeeid, "", "", "", "", "", txtCompany.Text, "", "", "", "", "", "", "", "", "", "", "australia", "", "", "", "", "", "", "", "", "", "", "false", "", "", "", "", "", "", "", "");
            int succontacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", txtContFirst.Text.Trim(), txtContLast.Text.Trim(), "", "", employeeid, employeeid);
            int succustinfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "customer entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(succontacts), employeeid, "1");

            if (Convert.ToString(success) != string.Empty)
            {
                ModalPopupExtenderVendor.Hide();
                BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(success);
                Notification("Transaction Successful.");
            }
            else
            {
                Notification("Transaction Failed.");

            }
        }
    }

    protected void btnNewStock_Click(object sender, EventArgs e)
    {
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        chkDashboard.Checked = false;

        PanAddUpdate.Visible = true;
        ModalPopupExtenderStock.Show();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlstockcategory.Items.Clear();
        ddlstockcategory.Items.Add(item8);

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dtStockCategory;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindScript();
    }

    protected void ibtnAddStock_Click(object sender, EventArgs e)
    {
        InitAdd();
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();

        int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hyplocationid = (HiddenField)item.FindControl("hyplocationid");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            if (txtqty.Text != "" || hyplocationid.Value != "")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
            }
        }
        if (success > 0)
        {
            ModalPopupExtenderStock.Hide();
            Notification("Transaction Successful.");
        }
        else
        {
            ModalPopupExtenderStock.Show();
            Notification("Transaction Failed.");
        }
        BindScript();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btndeliver_Click(object sender, EventArgs e)
    {

        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Section = "";
        string Message = "";
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        //if (receive != string.Empty)
        //{
        //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
        //}

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        //if (success)
        //{
        //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
        //if (dt.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
        //        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
        //        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
        //    }
        //}
        //SetDelete();

        int success1 = 0;
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;

        if (FileUpload1.HasFile)
        {
            // Notification("Transaction Successful.");
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
            string connectionString = "";

            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}
            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            //else if (FileUpload1.FileName.EndsWith(".xlsx"))
            //{
            //    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            //}


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    // string employeeid = string.Empty;
                    //string flag = "true";
                    // SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

                    //DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum(hndid.Value);
                    DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum_ByStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr2 = dt3.Rows[0];
                    string qty = dr2["Qty"].ToString();

                    //DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                    DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr1 = dt2.Rows[0];
                    string stockid = dr1["StockItemID"].ToString();
                    int count = 0;
                    int exist = 0;
                    var ExistSerialNo = new List<string>();
                    List<string> NovalueList = new List<string>();
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        //Response.Write(dr.Depth);
                        //Response.End();
                        int blank = 0;
                        int flagcolNames = 0;

                        if (dr.HasRows)
                        {
                            var columns = new List<string>();
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                columns.Add(dr.GetName(i));
                                if (!string.IsNullOrEmpty(dr.GetName(i)))
                                {
                                    string attributevalue0 = "Serial No";
                                    string attributevalue1 = "Pallet";
                                    int j = 0;
                                    string columnname = dr.GetName(i).ToString();
                                    if (columnname.StartsWith(attributevalue0) == true || columnname.StartsWith(attributevalue1) == true)
                                    {
                                        flagcolNames = 0;
                                    }
                                    else
                                    {
                                        flagcolNames = 1;
                                        break;
                                    }
                                }
                            }

                            if (flagcolNames == 0)
                            {
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";
                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();

                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank = 1;
                                        break;
                                    }
                                    if (blank != 1)
                                    {
                                        exist = ClstblStockOrders.tblStockSerialNo_Exist(SerialNo);
                                        if (exist == 0)
                                        {
                                            count++;
                                        }
                                        else
                                        {
                                            sttblStockSerialNo stitem = ClstblStockOrders.tblStockSerialNo_Getdata_BySerialNo(SerialNo);
                                            //blank = 1;
                                            ExistSerialNo.Add(stitem.SerialNo.ToString());
                                            NovalueList.Add("exist");
                                            //  break;
                                        }

                                    }
                                }
                            }
                            else
                            {
                                //ExcelFileHeader();
                            }
                            //Response.Write();
                        }
                    }
                    //Response.Write(count);
                    //Response.End();
                    if ((NovalueList != null) && (!NovalueList.Any()))
                    {
                        PanSerialNo.Visible = false;
                        if ((count == Convert.ToInt32(qty)) && (!NovalueList.Any()))
                        {

                            using (DbDataReader dr = command.ExecuteReader())
                            {

                                if (dr.HasRows)
                                {
                                    // Notification("Transaction Successful.");

                                    //if (flag == "false")
                                    //{
                                    //    PanEmpty.Visible = true;
                                    //}
                                    //else
                                    //{


                                    while (dr.Read())
                                    {

                                        int blank2 = 0;
                                        string SerialNo = "";
                                        string Pallet = "";

                                        SerialNo = dr["Serial No"].ToString();
                                        Pallet = dr["Pallet"].ToString();
                                        if (SerialNo == "" && Pallet == "")
                                        {
                                            blank2 = 1;
                                        }
                                        if (blank2 != 1)
                                        {
                                            if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                            {
                                                //try
                                                //{

                                                //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                                // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                                //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                                //Response.End();
                                                // success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);

                                                string StockOrderID = hndid.Value;
                                                Section = "Stock Order";
                                                Message = "Stock In for Order Number:" + StockOrderID;

                                                //success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID);
                                                success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID_ForPortal(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID, "");
                                                ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, StockOrderID, SerialNo, Section, Message, Currendate);
                                                //}
                                                //catch { }
                                            }
                                            if (success1 > 0)
                                            {
                                                Notification("Transaction Successful.");
                                            }
                                            else
                                            {
                                                Notification("Transaction Failed.");
                                            }
                                        }
                                        //}
                                    }
                                }
                            }
                            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
                            if (hndid.Value != null && hndid.Value != "")
                            {
                                bool s1 = ClstblStockOrders.tblStockOrderItems_Update_Delivered(hndid.Value, "1");

                            }
                            if (success)
                            {
                                if (receive != string.Empty)
                                {
                                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                                }

                                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                SetError();
                            }


                        }
                        else
                        {
                            //if (exist == 0)
                            //{
                            Notification("Quantity and No. of excel record didn't match.");
                            Notification("Quantity and No. of excel record didn't match.");
                            // }
                            //if (exist == 1)
                            //{
                            //    SerialNoExist();
                            //}
                        }
                    }
                    else
                    {
                        PanSerialNo.Visible = true;
                        int sizeOfList = ExistSerialNo.Count;
                        List<string> NovalueListmsg = new List<string>();
                        for (int i = 0; i < sizeOfList; i++)
                        {
                            string Something = "Serial No: " + Convert.ToString(ExistSerialNo[i]) + ",<br/>";
                            string error = Something.TrimEnd(',');
                            NovalueListmsg.Add(Something);


                            //Response.Write(Convert.ToString(orderList[i]) + "<br \\");
                        }
                        ltrerror.Text = string.Join(",", NovalueListmsg).TrimEnd(',') + "Are already Exists";
                    }

                }
            }
        }
        else
        {
            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
            if (success)
            {
                if (receive != string.Empty)
                {
                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                }

                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                    }
                }
                Notification("Transaction Successful.");
            }
            else
            {
                SetError();
            }

        }

        //}
        //else
        //{
        //    SetError();
        //}
        txtdatereceived.Text = string.Empty;

        // txtserialno.Text = string.Empty;


        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnNewVendor_Click1(object sender, EventArgs e)
    {

        ModalPopupExtenderVendor.Show();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        txtCompany.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        BindScript();
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ClearCheckBox();
        ClearAllContent();
    }

    protected void ClearAllContent()
    {
        ddlShow.SelectedValue = "False";
        //ddlpurchasecompanysearch.SelectedValue = "";
        ddlitemnamesearch.SelectedValue = "";
        ddlDue.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        //ddlloc.SelectedValue = "";
        txtstockitemfilter.Text = string.Empty;
        ddlLocalInternation.SelectedValue = "";
        txtStockContainerFilter.Text = string.Empty;
        ddlDelevery.SelectedValue = "";
        txtTargetDate.Text = "";
        ddlSearchTransCompName.SelectedValue = "";
        ddlPartial.SelectedValue = "2";
        
        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void btnDelivered_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblStockOrders.tblStockOrders_Update_Cancelled(id, Convert.ToString(true));
        ClstblStockOrders.tblStockOrderItems_DeleteStockOrderID(id);
        ClstblStockOrders.tblStockOrders_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
            Notification("Transaction Successful.");
            //PanSuccess.Visible = true;
        }
        else
        {
            Notification("Transaction Failed.");
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnStockOrderID = (HiddenField)e.Row.FindControl("hdnStockOrderID");
            string StockOrderID = hdnStockOrderID.Value;
            Label lblAmount = (Label)e.Row.FindControl("lblAmount");
            Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            Label lblqty = (Label)e.Row.FindControl("Label8");
            HyperLink hypDetail = (HyperLink)e.Row.FindControl("hypDetail");
            HyperLink lnkExcelUploded = (HyperLink)e.Row.FindControl("lnkExcelUploded");
            HyperLink lnkExcelPending = (HyperLink)e.Row.FindControl("lnkExcelPending");

            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            Image imgdiv = (Image)e.Row.FindControl("imgdiv");
            //if (Roles.IsUserInRole("Wholesale"))
            //{
            //    gvbtnUpdate.Visible = false;
            //    gvbtnDelete.Visible = false;
            //}
            //else
            //{
            //    gvbtnUpdate.Visible = true;
            //    gvbtnDelete.Visible = true;
            //}

            if (!string.IsNullOrEmpty(StockOrderID))
            {
                DataTable dtstock = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(StockOrderID);
                SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);

                HiddenField hdnactualdelivery = (HiddenField)e.Row.FindControl("hdnactualdelivery");
                HiddenField hdnOrderStatus = (HiddenField)e.Row.FindControl("hdnOrderStatus");
                hypDetail.NavigateUrl = SiteURL + "admin/adminfiles/stock/StockOrderDelivery.aspx?StockOrderID=" + StockOrderID;
                HiddenField hdnQty = (HiddenField)e.Row.FindControl("hdnQty");
                int Qty = 0;
                if (!string.IsNullOrEmpty(hdnQty.Value))
                {
                    Qty = Convert.ToInt32(hdnQty.Value);
                }
                hypDetail.Target = "_blank";

                if (hdnOrderStatus.Value == "1")
                {
                    if (dtstock.Rows.Count == Convert.ToInt32(st.OrderQuantity))
                    {
                        lnkExcelUploded.Visible = true;
                        lnkExcelPending.Visible = false;
                    }
                    else
                    {
                        lnkExcelUploded.Visible = false;
                        lnkExcelPending.Visible = true;
                    }
                }

                //if (dtstock.Rows.Count > 0 || !string.IsNullOrEmpty(hdnactualdelivery.Value))
                //{
                //    if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Purchase Manager"))
                //    {
                //        gvbtnDelete.Visible = true;
                //    }
                //    else
                //    {
                //        gvbtnDelete.Visible = false;
                //    }
                //}
                //else
                //{
                //    if (Roles.IsUserInRole("Administrator"))
                //    {
                //        gvbtnDelete.Visible = true;
                //    }
                //    else
                //    {
                //        gvbtnDelete.Visible = false;
                //    }
                //}

                if (!string.IsNullOrEmpty(hdnactualdelivery.Value) || (dtstock.Rows.Count > Qty))
                {
                    //gvbtnUpdate.Visible = false;
                    //Newly Added Condition on 11-May-2020
                    if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Purchase Manager"))
                    {
                        gvbtnUpdate.Visible = true;
                    }
                    else
                    {
                        gvbtnUpdate.Visible = false;
                    }
                }
                else
                {
                    gvbtnUpdate.Visible = true;
                }

            }

            //DataTable dt = new DataTable();
            //dt = GetGridData();
            //int Amount = 0;
            //int TotAmount = 0;
            //int Qty1 = 0;
            //int TotQty = 0;

            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);
            //    Qty1 = Convert.ToInt32(dt.Rows[i]["Qty"]);
            //    TotAmount += Amount;
            //    TotQty += Qty1;
            //    if (i == dt.Rows.Count - 1 && string.IsNullOrEmpty(StockOrderID))
            //    {
            //        // lblAmount.Text =Amount.ToString();
            //        lblqty.Text = TotQty.ToString();
            //        lblAmount.Text = TotAmount.ToString();
            //        hypDetail.Visible = false;
            //        lnkExcelUploded.Visible = false;
            //        lnkExcelPending.Visible = false;
            //        gvbtnUpdate.Visible = false;
            //        gvbtnDelete.Visible = false;
            //        imgdiv.Visible = false;
            //    }
            //}

        }
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int CategoryID;
        int OrderQuantity;
        HiddenField hfStockOrderID = (HiddenField)e.Item.FindControl("hfStockOrderID");
        HiddenField hfStockItemID = (HiddenField)e.Item.FindControl("hfStockItemID");
        HiddenField StockCategoryID = (HiddenField)e.Item.FindControl("hfStockCategoryID");
        LinkButton btnDelivery = (LinkButton)e.Item.FindControl("btnDelivery");
        ImageButton checkimag = (ImageButton)e.Item.FindControl("checkimag");
        Label lblOrderQuantity = (Label)e.Item.FindControl("lblOrderQuantity");
        DataTable dtGetData = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(Convert.ToString(hfStockOrderID.Value));
        //1 For Module 2 For Inveerter
        if (dtGetData.Rows.Count > 0)
        {
            foreach (DataRow dr in dtGetData.Rows)
            {
                int DeliveryOrderStatus = Convert.ToInt32(dr["DeliveryOrderStatus"].ToString());
                if (DeliveryOrderStatus == 1)
                {
                    // divdelivery.Visible = true;
                    if (Convert.ToInt32(StockCategoryID.Value) == 1)
                    {

                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 1)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }

                    }
                    else if (Convert.ToInt32(StockCategoryID.Value) == 2)
                    {
                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 2)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // divdelivery.Visible = false;
                    btnDelivery.Visible = false;
                }
            }
        }
    }

    protected void rptOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delivered")
        {
            ModalPopupdeliver.Show();
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string StockOrderID = commandArgs[0];
            string StockOrderItemID = commandArgs[1];
            hndStockOrderItemID.Value = StockOrderItemID;


            hndid.Value = StockOrderID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        Response.Clear();
        //dt.Constraints = false;
        int count = dt.Rows.Count;
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "Stock Order_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            //int[] ColList = { 1, 17, 36, 5, 24, 15, 19, 34, 26, 37, 35, 39, 29, 21, 6 };
            int[] ColList = { 1, 2, 3, 4,
                              6, 7, 8, 9,
                              10, 11, 40, 12,
                              5, 17, 18, 19, 33,
                              42, 13, 14, 16, 43};

            string[] arrHeader = { "Order Number","StockOrderStatus", "Invoice No.", "P.Company",
                                    "Ordered Date", "Exp.DeliveryDate","Manual Order No", "Container No.",
                                    "Items Ordered", "Vendor", "Payment Status","Payment Method",
                                    "Stock For", "Delivery Date", "Delivered By", "Delivery Type", "Order Type",
                                    "TransCompName", "Qty", "Pending Qty", "Amount", "Partial"         
                                };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void btnOK3_Click(object sender, EventArgs e)
    {
        string Section = "";
        string Message = "";
        DateTime Currendate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string ID = hndid2.Value;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, "", "0");
        bool success2 = ClstblStockOrders.tblStockOrders_Update_ExcelUploaded(ID, "false");
        ClstblStockOrders.tblStockOrders_Update_ActualDelivery(ID, "");

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);

        DataTable dtQty = ClstblStockOrders.tblStockOrderItems_SelectQty(ID);
        string OrderQuantity = "";
        string StockOrderID = "";
        string StockItemID = "";
        if (dtQty.Rows.Count > 0)
        {
            OrderQuantity = dtQty.Rows[0]["OrderQuantity"].ToString();
            StockOrderID = dtQty.Rows[0]["StockOrderID"].ToString();
            StockItemID = dtQty.Rows[0]["StockItemID"].ToString();
        }
        if (success)
        {
            DataTable dt1 = ClstblStockOrders.tblStockSerialNo_Select_ByStockOrderID(ID);
            if (dt1.Rows.Count > 0)
            {
                string SerialNo = "";
                foreach (DataRow dtserial in dt1.Rows)
                {
                    SerialNo = dtserial["SerialNo"].ToString();
                    bool sucess = ClstblMaintainHistory.tblMaintainHistory_Delete_BySerailNo(SerialNo);
                }
            }

            ClstblStockOrders.tblStockSerialNo_Delete(ID);
            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //Response.Write(row["OrderQuantity"].ToString() + "==" + row["StockItemID"].ToString());
                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                    ClstblStockOrders.tblStockOrderItems_Update_ItemDeliveredOnly(row["StockItemID"].ToString(), "0");
                    //Response.Write(StOldQty.StockQuantity);
                    //Response.End();
                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, (0 - Convert.ToInt32(row["OrderQuantity"].ToString())).ToString(), userid, "0");
                    ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                }
                //Response.End();
            }
            SetDelete();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
    }

    protected void lnkpurchasecompany_Click(object sender, EventArgs e)
    {

    }

    protected void txtOrderQuantity_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];

        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

        HiddenField hdnStockCategory = (HiddenField)item.FindControl("hdnStockCategory");
        HiddenField hdnStockItem = (HiddenField)item.FindControl("hdnStockItem");


        if (!string.IsNullOrEmpty(txtOrderQuantity.Text) && (!string.IsNullOrEmpty(hdnStockOrderID2.Value)))
        {
            DataTable dtstock = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(hdnStockOrderID2.Value, hdnStockCategory.Value, hdnStockItem.Value);

            if (Convert.ToInt32(txtOrderQuantity.Text) < dtstock.Rows.Count)
            {
                Notification("Enter more then " + dtstock.Rows.Count + " Quantity");

            }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }
}