﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="ContainerTracker.aspx.cs" Inherits="admin_adminfiles_stock_ContainerTracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <div class="card">
                    <div class="card-block">
                        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Container Tracker
                    <asp:Label runat="server" ID="lblStockItem" />
                            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <%--  <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>
                            </div>
                        </h5>

                    </div>
                </div>

            </div>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script src="<%=SiteURL%>admin/theme/assets/js/select2.js"></script>
            <script language="javascript" type="text/javascript">
                function WaterMark(txtName, event) {
                    var defaultText = "Enter Username Here";
                }
            </script>
            <script>      
                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
            ///$('form').on("click", '#<%=lbtnExport.ClientID %>', function () {
                        ShowProgress();
                    });
                });



                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox2();
                    });
                }

                function pageLoadedpro() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox();
                    //});
                    $(function () {
                        $('#datetimepicker02').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                    });

                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    //$('.mutliSelect input[type="checkbox"]').on('click', function () {
                    //    callMultiCheckbox1();
                    //});



                    //$('.datetimepicker1').datetimepicker({
                    //    format: 'DD/MM/YYYY'
                    //});


                }

                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }
            </script>
            <style>
                .txt_height {
                    height: 100px;
                }

                .table tbody .brd_ornge td, .brd_ornge {
                    border-bottom: 3px solid #ff784f;
                }
                .modal-body{max-height:auto!important; overflow:visible!important;}
            </style>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    callMultiCheckbox1();
                    callMultiCheckbox2();

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(".myval").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $(".myvalemployee").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox2();
                    });

                    $(".selectlocation .dropdown dt a").on('click', function () {
                        $(".selectlocation .dropdown dd ul").slideToggle('fast');
                    });
                    $(".selectlocation .dropdown dd ul li a").on('click', function () {
                        $(".selectlocation .dropdown dd ul").hide();
                    });

                    $(".Vender .dropdown dt a").on('click', function () {
                        $(".Vender .dropdown dd ul").slideToggle('fast');
                    });
                    $(".Vender .dropdown dd ul li a").on('click', function () {
                        $(".Vender .dropdown dd ul").hide();
                    });

                }
            </script>
            <script>
                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddlLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }
                function callMultiCheckbox2() {
                    var title = "";
                    $("#<%=ddVender.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel1').show();
                        $('.multiSel1').html(html);
                        $(".hida1").hide();
                    }
                    else {
                        $('#spanselect1').show();
                        $('.multiSel1').hide();
                    }

                }
            </script>
            <div class="page-body padtopzero minheight500">
                <asp:Panel runat="server" ID="PanGridSearch" class="">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170" id="divprojNo" runat="server">
                                                    <asp:TextBox ID="txtProjNo" runat="server" placeholder="Stock Order No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjNo" FilterType="Numbers, Custom" ValidChars="," />
                                                </div>
                                                <div class="input-group col-sm-2 max_width170" id="div1" runat="server">
                                                    <asp:TextBox ID="txtstockitem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                </div>
                                                
                                                <div class="input-group col-sm-2 max_width170" id="div3" runat="server">
                                                    <asp:TextBox ID="txtStockManufacturer" runat="server" placeholder="Manufacturer" CssClass="form-control m-b"></asp:TextBox>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170" id="div2" runat="server">
                                                    <asp:TextBox ID="txtStockSize" runat="server" placeholder="Size" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtStockSize" FilterType="Numbers" />
                                                </div>

                                                <div class="input-group col-sm-2 max_width170" style="display: none">
                                                    <asp:TextBox ID="txtserailno" runat="server" placeholder="Serial No./Pallet No." CssClass="form-control m-b"></asp:TextBox>

                                                </div>
                                                
                                                <div class="input-group col-sm-2 martop5 max_width170" >
                                                        <asp:TextBox ID="txtContainderNo" runat="server" placeholder="Container No" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtContainderNo"
                                                            WatermarkText="Container No" />
                                                    </div>

                                                <div class="form-group spical multiselect selectlocation martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Location</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddlLocation" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptLocation" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />

                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Vendor</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>
                                                <div class="form-group spical multiselect Vender martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida1" id="spanselect1">Vender</span>
                                                                <p class="multiSel1"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddVender" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptVender" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnPurchaseCompanyName" runat="server" Value='<%# Eval("PurchaseCompanyName") %>' />
                                                                                <asp:HiddenField ID="hdnid" runat="server" Value='<%# Eval("id") %>' />

                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="Customer" Text='<%# Eval("PurchaseCompanyName")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlStockOrderStatus" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Stock Order Status</asp:ListItem>
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                        <asp:ListItem Value="0">OnHold</asp:ListItem>
                                                        <asp:ListItem Value="2">Void</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlSearchTransCompName" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">TransCompName</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="0">Date</asp:ListItem>

                                                        <asp:ListItem Value="2">ETD</asp:ListItem>
                                                        <%--ExpDeliveryDate--%>
                                                        <asp:ListItem Value="3">ETA</asp:ListItem>
                                                        <%--BOL REC--%>
                                                        <asp:ListItem Value="1">Telex Release Date</asp:ListItem>
                                                        <asp:ListItem Value="4">TransportDel.Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" Visible="false" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth" OnClick="lbtnExport_Click1"
                                                        CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>

                                                <%--<div class="input-group col-sm-1 max_width170" style="display:none">
                                            <asp:LinkButton ID="btnEmail" runat="server" Visible="false"
                                                CausesValidation="false" OnClick="btnEmail_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>SendEmail </asp:LinkButton>
                                        </div>--%>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row bewtten_div">

                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <%--                                         <div class="input-group col-sm-1 martop5 max_width170">
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838;border-color:#218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </asp:Panel>

                <div class="finalgrid">
                    <asp:Panel ID="panel" runat="server">
                        <div class="page-header card" id="divtot" runat="server" style="display: none">
                            <div class="card-block brd_ornge">
                                <div class="printorder" style="font-size: medium; display: none">
                                    <b>Total Number of Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnOrderID" runat="server" />
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="table-responsive  BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="OrderNumber" runat="server" CssClass="tooltip-demo text-center table GridviewScrollItem table-bordered Gridview"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                                OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("OrderNumber") %>','tr<%# Eval("OrderNumber") %>');">
                                                                <%--<asp:Image ID="img" runat="server" ImageUrl="../../../images/icon_plus.png" />--%>
                                                                <img id='imgdiv<%# Eval("OrderNumber") %>' src="../../../images/icon_plus.png" />
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="CompanyLocation" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%--<asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("ExpectedDelivery")%>' data-toggle="tooltip"><%#Eval("ExpectedDelivery")%></asp:Label>--%>
                                                            <asp:Label ID="Label56789" runat="server">
                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OrderNo" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="OrderNo" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("OrderNumber")%>' data-toggle="tooltip"><%#Eval("OrderNumber")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Manual Order No" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ManualOrderNumber" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinvoicenoq" runat="server" Width="80px" data-placement="top" data-original-title='<%#Eval("ManualOrderNumber")%>' data-toggle="tooltip">
                                                <%#Eval("ManualOrderNumber")%></asp:Label>
                                                            <%-- <asp:Label ID="Label89" runat="server" data-placement="top" data-original-title='<%#Eval("PurchaseCompanyName")%>' </asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="P.Company" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="PurchaseCompanyName" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinvoiceno" runat="server" Width="80px" data-placement="top" data-original-title='<%#Eval("PurchaseCompanyName")%>' data-toggle="tooltip">
                                                <%#Eval("PurchaseCompanyName")%></asp:Label>
                                                            <%-- <asp:Label ID="Label89" runat="server" data-placement="top" data-original-title='<%#Eval("PurchaseCompanyName")%>' </asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Container NO" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="StockContainer">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label5" runat="server">
                                                <%#Eval("StockContainer")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Target Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="TargetDate" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%-- <asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("telex_Release_date)%>' data-toggle="tooltip"><%#Eval("telex_Release_date")%></asp:Label>--%>
                                                            <asp:Label ID="lblGridTargetDate" runat="server">
                                                <%#Eval("TargetDate","{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ETD" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="BOLReceived" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%--<asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("ExpectedDelivery")%>' data-toggle="tooltip"><%#Eval("ExpectedDelivery")%></asp:Label>--%>
                                                            <asp:Label ID="lblETD" runat="server" Text='<%# Eval("BOLReceived","{0:dd MMM yyyy}") %>'> </asp:Label>
                                                            <%--          <%#Eval("BOLReceived","{0:dd MMM yyyy}")%></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ETA" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="ExpectedDelivery" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%--<asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("ExpectedDelivery")%>' data-toggle="tooltip"><%#Eval("ExpectedDelivery")%></asp:Label>--%>
                                                            <asp:Label ID="lblETA" runat="server" Text='<%# Eval("ExpectedDelivery","{0:dd MMM yyyy}") %>'></asp:Label>
                                                            <%-- <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TelexReleaseDate" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="telex_Release_date" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%-- <asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("telex_Release_date)%>' data-toggle="tooltip"><%#Eval("telex_Release_date")%></asp:Label>--%>
                                                            <asp:Label ID="Label56" runat="server">
                                                <%#Eval("telex_Release_date","{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Transport.Del Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="arrivaldate" HeaderStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <%-- <asp:Label ID="Label6788" runat="server" data-placement="top" data-original-title='<%#Eval("telex_Release_date)%>' data-toggle="tooltip"><%#Eval("telex_Release_date")%></asp:Label>--%>
                                                            <asp:Label ID="Label565" runat="server">
                                                <%#Eval("arrivaldate")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    

                                                    <asp:TemplateField HeaderText="Stock Order Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="StockOrderStatus1">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label589" runat="server">
                                                <%#Eval("StockOrderStatus1")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Trans.Co Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="Trans_Company_Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrans_Company_Name" runat="server">
                                                <%#Eval("Trans_Company_Name")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Extra Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label8" runat="server">
                                                <%#Eval("extranotes")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnDate" runat="server" CssClass="btn btn-info btn-mini" CausesValidation="false" CommandName="UpdateDate" 
                                                                CommandArgument='<%#Eval("StockOrderID") + ";" + Eval("BOLReceived") + ";" + Eval("ExpectedDelivery") + ";" + Eval("telex_Release_date") + ";" + Eval("arrivaldate") + ";" + Eval("TargetDate") %>'>
                                                        UpdateDate
                                                            </asp:LinkButton>

                                                            <asp:HyperLink ID="lnkExcelUploded" runat="server" CausesValidation="false" CssClass="btn btn-success btn-mini" 
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Excel Uploded" Style="font-size: 10px!important; color: white"
                                                                Visible='<%# Eval("DeliveryOrderStatus").ToString() == "1" ? Eval("OrderQuantity").ToString() == Eval("UploadSerialCount").ToString() ? true:false : false %>' 
                                                                 >
                                                            <i class="btn-label fa fa-check" style="font-size:11px!important;padding: 0px;"></i> Excel Uploaded
                                                            </asp:HyperLink>

                                                            <asp:HyperLink ID="lnkExcelPending" runat="server" CausesValidation="false" CssClass="btn btn-warning btn-mini"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Excel Uploded" Style="font-size: 10px!important; color: white"
                                                                Visible='<%# Eval("DeliveryOrderStatus").ToString() == "1" ? Eval("OrderQuantity").ToString() != Eval("UploadSerialCount").ToString() ? true:false : false %>' 
                                                                Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/stock/StockOrderDelivery.aspx?StockOrderID=" + Eval("StockOrderID") %>' >
                                                            <i class="btn-label fa fa-close" style="font-size:11px!important;padding: 0px;"></i> Excel Pending
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("OrderNumber") %>' style="display: none; text-align: start;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("OrderNumber") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 1%;"><b>Ordered Items :</b>
                                                                                </td>
                                                                                <td style="width: 10%;">
                                                                                    <asp:Label ID="Label50" runat="server">
                                                                                     <%#Eval("StockOrderItem") %></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>
            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divModalNote" TargetControlID="Button12" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divModalNote" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog" style="width: 1000px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Details</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="card">
                                <div class="card-block">
                                    <div class="form-group row marginbtm0">
                                        <div class="col-sm-3 custom_datepicker">
                                            <asp:Label ID="lblTargetDate" runat="server" class="disblock">
                                             Target Date  </asp:Label>
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtTargetDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3 custom_datepicker">
                                            <asp:Label ID="Label46" runat="server" class="disblock">
                                               ETD</asp:Label>
                                            <div class="input-group sandbox-container">

                                                <asp:TextBox ID="txtETD" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-3 custom_datepicker">
                                            <asp:Label ID="Label1" runat="server" class="disblock">
                                               ETA</asp:Label>
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtETA" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3 custom_datepicker">
                                            <asp:Label ID="Label2" runat="server" class="disblock">
                                               TelexReleaseDate	</asp:Label>
                                            <div class="input-group sandbox-container">
                                                <asp:TextBox ID="txtTelexReleaseDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <asp:Label ID="Label3" runat="server" class="disblock">
                                               Transport.Del Date		</asp:Label>
                                            <div class="input-group date" id="datetimepicker02">
                                                <asp:TextBox ID="txtTransportDelDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSave" runat="server" type="button" class="btn btn-danger POPupLoader" Text="Update" OnClick="btnSave_Click1"></asp:Button>
                            <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button12" Style="display: none;" runat="server" />
            <script>
                $(document).ready(function () {
                    $('.js-example-basic-multiple').select2();
                });

            </script>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

