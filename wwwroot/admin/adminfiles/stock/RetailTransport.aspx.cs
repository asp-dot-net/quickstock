﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_RetailTransport : System.Web.UI.Page
{
    protected string mode = "";
    protected static string SiteURL;
    static DataView dv;
    protected static string EmployeeID;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        HidePanels();


        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindGrid(0);

            txtTransportDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            RoleGroup();
            BindDropDown();
            BindProjectNo();

            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
            if (dt_user.Rows.Count > 0)
            {
                EmployeeID = dt_user.Rows[0]["EmployeeID"].ToString();
            }
        }

        BindScript();

    }

    public void RoleGroup()
    {
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Warehouse")) || (Roles.IsUserInRole("Wholesale")))
        {
            lnkAdd.Visible = true;

            GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
        }
        else
        {
            lnkAdd.Visible = false;
            GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
        }
    }

    public void BindDropDown()
    {
        DataTable dt = ClstblEmployees.tblEmployees_SelectAll();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();

        DataTable dt2 = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlTransportFrom.DataSource = dt2;
        ddlTransportFrom.DataTextField = "location";
        ddlTransportFrom.DataValueField = "CompanyLocationID";
        ddlTransportFrom.DataBind();

        ddlSearchTransportFrom.DataSource = dt2;
        ddlSearchTransportFrom.DataTextField = "location";
        ddlSearchTransportFrom.DataValueField = "CompanyLocationID";
        ddlSearchTransportFrom.DataBind();
        
        DataTable dtTransfComp = ClstblStockTransfers.tblTransferCompany_GetData();
        ddlTransportCompany.DataSource = dtTransfComp;
        ddlTransportCompany.DataTextField = "TransferCompany";
        ddlTransportCompany.DataValueField = "TransferCompanyID";
        ddlTransportCompany.DataBind();

        ddlSearchTransferCompany.DataSource = dtTransfComp;
        ddlSearchTransferCompany.DataTextField = "TransferCompany";
        ddlSearchTransferCompany.DataValueField = "TransferCompanyID";
        ddlSearchTransferCompany.DataBind();

    }

    protected void BindProjectNo()
    {
        //string sql = "select convert(varchar(20),ProjectNumber) as ProjectNumber from tblProjects";
        string sql = "select ProjectNumber from tbl_PickListLog where IsDeduct = 0 Union all select (InvoiceNo) as ProjectNumber from tbl_WholesaleOrders where IsDeduct = 0";
        DataTable dt = ClstblCustomers.query_execute(sql);

        lstProjectNo.DataSource = dt;
        lstProjectNo.DataMember = "ProjectNumber";
        lstProjectNo.DataTextField = "ProjectNumber";
        lstProjectNo.DataValueField = "ProjectNumber";
        lstProjectNo.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        dt = ClstblRetailsTranport.tbl_RetailTransport_GetData(txtTnsprtNo.Text, ddlSearchTransportFrom.SelectedValue, ddlSearchTransferCompany.SelectedValue, txtSearchLocation.Text, txtTrackingNo.Text, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {

        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        divtot.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string TransportDate = txtTransportDate.Text;
        string EnteredBy = ddlEmployee.SelectedValue;
        string CompanyId = ddlCompany.SelectedValue;
        string TransportCompany = ddlTransportCompany.SelectedValue;
        string TransportFrom = ddlTransportFrom.SelectedValue;
        string Location = txtLocation.Text;
        string Quantity = txtQuantity.Text;
        decimal Weight = Convert.ToDecimal(txtWeight.Text);
        decimal Amount = Convert.ToDecimal(txtAmount.Text);
        string TrackingNo = txtTrackingNo.Text;
        string Notes = txtNote.Text;
        string ProjectNo = "";
        foreach (ListItem itemval in lstProjectNo.Items)
        {
            if (itemval.Selected)
            {
                ProjectNo += "," + itemval.Value;
            }
        }
        if (ProjectNo != string.Empty)
        {
            ProjectNo = ProjectNo.Substring(1).ToString();
        }

        int ExistTrackingNo = ClstblRetailsTranport.tbl_RetailTransport_Exits_TrackingNumber(TrackingNo);

        if (ExistTrackingNo == 0)
        {
            int Success = ClstblRetailsTranport.tbl_RetailTransport_Insert(TransportDate, EnteredBy, CompanyId, TransportCompany, TransportFrom, Location, TrackingNo, Quantity, txtWeight.Text, txtAmount.Text, Notes);

            if (Success > 0)
            {
                string[] PNo = ProjectNo.Split(',');

                for (int i = 0; i < PNo.Length; i++)
                {
                    int SucProject = ClstblRetailsTranport.tbl_RetailTransportProjectNo_Insert(Success.ToString(), PNo[i].ToString());
                }

                SetAdd1();
                BindScript();
                PanAddUpdate.Visible = true;
                Reset();
                lnkBack.Visible = false;
                lnkAdd.Visible = true;
                BindGrid(0);
            }
            else
            {
                Notification("Error");
            }
        }
        else
        {
            Notification("Already Exists");
            //PanAddUpdate.Visible = true;
            Reset();
            lnkBack.Visible = false;
            lnkAdd.Visible = true;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string ID = GridView1.SelectedDataKey.Value.ToString();

        string TransportDate = txtTransportDate.Text;
        string EnteredBy = ddlEmployee.SelectedValue;
        string CompanyId = ddlCompany.SelectedValue;
        string TransportCompany = ddlTransportCompany.SelectedValue;
        string TransportFrom = ddlTransportFrom.SelectedValue;
        string Location = txtLocation.Text;
        string Quantity = txtQuantity.Text;
        decimal Weight = Convert.ToDecimal(txtWeight.Text);
        decimal Amount = Convert.ToDecimal(txtAmount.Text);
        string TrackingNo = txtTrackingNo.Text;
        string Notes = txtNote.Text;
        string ProjectNo = "";
        foreach (ListItem itemval in lstProjectNo.Items)
        {
            if (itemval.Selected)
            {
                ProjectNo += "," + itemval.Value;
            }
        }
        if (ProjectNo != string.Empty)
        {
            ProjectNo = ProjectNo.Substring(1).ToString();
        }

        bool Success = ClstblRetailsTranport.tbl_RetailTransport_Update(ID, TransportDate, EnteredBy, CompanyId, TransportCompany, TransportFrom, Location, TrackingNo, Quantity, txtWeight.Text, txtAmount.Text, Notes);

        bool SucProRemove = ClstblRetailsTranport.tbl_RetailTransportProjectNo_DeleteByRetailtransportID(ID);

        string[] PNo = ProjectNo.Split(',');
        for (int i = 0; i < PNo.Length; i++)
        {
            int SucProject = ClstblRetailsTranport.tbl_RetailTransportProjectNo_Insert(ID, PNo[i].ToString());
        }

        if(Success)
        {
            SetAdd1();
        }
        else
        {
            SetError1();
        }
        
        BindScript();
        PanAddUpdate.Visible = true;
        Reset();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindGrid(0);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        mode = "Edit";
        PanGrid.Visible = false;
        divtot.Visible = false;

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        SttblRetailTransport st = ClstblRetailsTranport.tbl_RetailTransport_SelectByID(id);
        DataTable dtProject = ClstblRetailsTranport.tbl_RetailTransportProjectNo_SearchByRetailtransportID(st.ID);

        try
        {
            if (st.TransportDate != "")
            {
                txtTransportDate.Text = SiteConfiguration.ConvertToDateView(st.TransportDate);
            }

            ddlEmployee.SelectedValue = st.EnteredBy;
            ddlTransportFrom.SelectedValue = st.TransportFrom;
            ddlTransportCompany.SelectedValue = st.TransportCompany;
            ddlCompany.SelectedValue = st.CompanyId;
            txtTrackingNo.Text = st.TrackingNo;
            txtQuantity.Text = st.Quantity;
            txtWeight.Text = st.Weight;
            txtAmount.Text = st.Amount;
            txtNote.Text = st.Notes;
            txtLocation.Text = st.Location;

            if (dtProject.Rows.Count > 0)
            {
                for (int i = 0; i < dtProject.Rows.Count; i++)
                {
                    int searchIndex = 0;
                    foreach (ListItem itemval in lstProjectNo.Items)
                    {
                        if (itemval.Value == dtProject.Rows[i]["ProjectNo"].ToString())
                        {
                            lstProjectNo.Items[searchIndex].Selected = true;
                        }
                        searchIndex++;
                    }
                }
            }
        }
        catch
        {
        }

        ////--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
        BindScript();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
        BindScript();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        Reset();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        divtot.Visible = false;
        Reset();
        PanAddUpdate.Visible = true;

        if (EmployeeID != string.Empty)
        {
            ddlEmployee.SelectedValue = EmployeeID;
        }
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Warehouse")))
        {
            ddlEmployee.Enabled = true;
        }
        else
        {
            ddlEmployee.Enabled = false;
        }

        InitAdd();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();

        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;

        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {
        PanAddUpdate.Visible = true;
        ddlTransportFrom.SelectedValue = "";
        txtTrackingNo.Text = string.Empty;
        ddlTransportCompany.SelectedValue = "";
        txtNote.Text = string.Empty;
        ddlEmployee.SelectedValue = EmployeeID;
        lstProjectNo.ClearSelection();

        ddlCompany.SelectedValue = "";
        txtLocation.Text = string.Empty;
        txtQuantity.Text = string.Empty;
        txtWeight.Text = string.Empty;
        txtAmount.Text = string.Empty;
        BindScript();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        BindGrid(0);
        BindScript();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "received")
        {
            string StockTransferID = e.CommandArgument.ToString();

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived(StockTransferID);
        }

        if (e.CommandName.ToLower() == "printpage")
        {
            ModalPopupExtenderDetail.Hide();

            string id = e.CommandArgument.ToString();

            SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
            lblFrom.Text = st.FromLocation;
            lblTo.Text = st.ToLocation;
            try
            {
                lblTransferDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.TransferDate));
            }
            catch { }
            lblTransferBy.Text = st.TransferByName;

            lblTrackingNo.Text = st.TrackingNumber;
            try
            {
                lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedDate));
            }
            catch { }
            lblReceivedBy.Text = st.ReceivedByName;
            lblNotes.Text = st.TransferNotes;

            DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
            if (dt.Rows.Count > 0)
            {

                rptItems.DataSource = dt;
                rptItems.DataBind();

                int qty = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["TransferQty"].ToString() != string.Empty)
                    {
                        qty += Convert.ToInt32(dr["TransferQty"].ToString());
                    }
                }
                lbltotalqty.Text = qty.ToString();
            }
            ModalPopupExtenderDetail.Show();
            // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>printContent();</script>");

        }

        BindGrid(0);
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void gvbtnPrint_Click(object sender, EventArgs e)
    {
        LinkButton gvbtnPrint = (LinkButton)sender;
        GridViewRow item1 = gvbtnPrint.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchTransferCompany.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlSearchTransferCompany.SelectedValue = "";
        txtTnsprtNo.Text = string.Empty;
        txtSearchTrackingNo.Text = string.Empty;
        txtSearchLocation.Text = string.Empty;

        BindGrid(0);
        BindScript();
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string ID = hdndelete.Value;

        bool Suc = ClstblRetailsTranport.tbl_RetailTransport_DeleteByID(ID);
        bool SucProRemove = ClstblRetailsTranport.tbl_RetailTransportProjectNo_DeleteByRetailtransportID(ID);

        Notification("Transaction Successful.");
        SetDelete();
        //updatepanelgrid.Update();
        PanAddUpdate.Visible = false;
        Reset();
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    LinkButton gvbtnPrint = e.Row.FindControl("gvbtnPrint") as LinkButton;
        //    gvbtnPrint.Attributes.Add("onclick", "printContent()");
        //}
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        Response.Clear();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = "Retail Transport_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 0, 1, 2, 3, 4, 5,
                              6, 7, 8, 9, 10, 12, 11 };

            string[] arrHeader = { "Transport No", "Transport Date", "Entered By", "Company Name", "Transport Company","Transport From", "Location", "Tracking No", "Qty", "Weight", "Amount", "Pro/Inv No", "Notes"
                                 };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void BindTotal(DataTable dt)
    {
        string Amount = dt.Compute("SUM(Amount)", string.Empty).ToString();
        lblTotalAmount.Text = Amount == "" ? "0.00" : Amount;

        string Followmont = dt.Compute("SUM(Amount)", "TransferCompany='Followmont'").ToString();
        lblFollowmont.Text = Followmont == "" ? "0.00" : Followmont;

        string TNT = dt.Compute("SUM(Amount)", "TransferCompany='TNT'").ToString();
        lblTNT.Text = TNT == "" ? "0.00" : TNT;

        string Mainfreight = dt.Compute("SUM(Amount)", "TransferCompany='Mainfreight'").ToString();
        lblMainfreight.Text = Mainfreight == "" ? "0.00" : Mainfreight;

        string INPerson = dt.Compute("SUM(Amount)", "TransferCompany='IN Person'").ToString();
        lblINPerson.Text = INPerson == "" ? "0.00" : INPerson;

        string Hi_Trans = dt.Compute("SUM(Amount)", "TransferCompany='Hi-Trans'").ToString();
        lblHiTrans.Text = Hi_Trans == "" ? "0.00" : Hi_Trans;
    }
}