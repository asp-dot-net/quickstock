using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_assignedstocktransfer : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv2;
    static DataView dv3;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords3.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords3.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

            if (Roles.IsUserInRole("Warehouse"))
            {
                BindDropDown();
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                ddllocationsearch.SelectedValue = CompanyLocationID;
                ddllocationsearch.Enabled = false;
                BindGrid(0);
            }

            if (Roles.IsUserInRole("WholeSale"))
            {
                TabProjectNo.Visible = false;
                BindGrid3(0);
                BindDropDown3();
            }
            else
            {
                TabProjectNo.Visible = true;
                BindGrid(0);
                BindDropDown();
            }


        }

    }

    public void BindDropDown()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataSource = dt;
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();

        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch.DataSource = dtcategory;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();
    }

    public void BindDropDown3()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch3.DataSource = dt;
        ddllocationsearch3.DataTextField = "location";
        ddllocationsearch3.DataValueField = "CompanyLocationID";
        ddllocationsearch3.DataMember = "CompanyLocationID";
        ddllocationsearch3.DataBind();

        DataTable dtcategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategorysearch3.DataSource = dtcategory;
        ddlcategorysearch3.DataTextField = "StockCategory";
        ddlcategorysearch3.DataValueField = "StockCategoryID";
        ddlcategorysearch3.DataBind();
    }

    protected DataTable GetGridData1()//projectnumberwise
    {
        DataTable dt1 = Reports.AssignedStockTransfer_ForProjWholesale("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "", ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        return dt1;
    }

    protected DataTable GetGridData3()//wholesaleorderid wise
    {
        DataTable dt1 = Reports.AssignedStockTransfer_ForProjWholesale("3", txtstockitemfilter3.Text, ddlcategorysearch3.SelectedValue, ddllocationsearch3.SelectedValue, txtSearchOrderNo3.Text, txtWholesaleOrderNo.Text, txtserailno3.Text, txtInvoiceNo.Text, ddldate3.SelectedValue, txtstartdate3.Text, txtenddate3.Text);
        return dt1;
    }


    public void BindGrid(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    public void BindGrid3(int deleteFlag)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();
        dv3 = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid3.Visible = false;
            divnopage3.Visible = false;
        }
        else
        {
            PanGrid3.Visible = true;
            GridView3.DataSource = dt;
            GridView3.DataBind();
            if (dt.Rows.Count > 0 && ddlSelectRecords3.SelectedValue != string.Empty && ddlSelectRecords3.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords3.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage3.Visible = false;
                }
                else
                {
                    divnopage3.Visible = true;
                    int iTotalRecords = dv3.ToTable().Rows.Count;
                    int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords3.SelectedValue == "All")
                {
                    divnopage3.Visible = true;
                    ltrPage3.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged3(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords3.SelectedValue) == "All")
        {
            GridView3.AllowPaging = false;
            BindGrid3(0);
        }
        else
        {
            GridView3.AllowPaging = true;
            GridView3.PageSize = Convert.ToInt32(ddlSelectRecords3.SelectedValue);
            BindGrid3(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        GridView3.DataSource = dv3;
        GridView3.DataBind();
        BindGrid3(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }


    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid3(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView3.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView3.PageIndex - 2;
            page[1] = GridView3.PageIndex - 1;
            page[2] = GridView3.PageIndex;
            page[3] = GridView3.PageIndex + 1;
            page[4] = GridView3.PageIndex + 2;
            page[5] = GridView3.PageIndex + 3;
            page[6] = GridView3.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView3.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView3.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView3.PageIndex == GridView3.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage3 = (Label)gvrow.Cells[0].FindControl("ltrPage3");
            if (dv3.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv3.ToTable().Rows.Count;
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage3.Text = "";
            }
        }
        catch { }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    void lb_Command3(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid3(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command3);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command3);
        }
    }


    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {

        DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("1", txtstockitemfilter.Text, ddlcategorysearch.SelectedValue, ddllocationsearch.SelectedValue, txtSearchOrderNo.Text, txtProjectNumber.Text, txtserailno.Text, "", "", ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "SerialNoProjNoWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 19, 22, 25, 3, 4, 21, 16, 20, 24 };
            string[] arrHeader = { "Project No.", "Installer Name", "Install Booked Date", "Serial No.", "Pallet No.", "Category", "Stock Item", "Location", "Deducted On" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnExport3_Click(object sender, EventArgs e)
    {
        DataTable dt = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("3", txtstockitemfilter3.Text, ddlcategorysearch3.SelectedValue, ddllocationsearch3.SelectedValue, txtSearchOrderNo3.Text, txtWholesaleOrderNo.Text, txtserailno3.Text, txtInvoiceNo.Text, "", ddldate3.SelectedValue, txtstartdate3.Text, txtenddate3.Text);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "SerialNoWholesaleOrderWise" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 9, 20, 3, 4, 21, 16, 18, 22 };
            string[] arrHeader = { "Wholesale Order No.", "Invoice No.", "Serial No.", "Pallet No.", "Customer", "Stock Item", "Location", "Deducted On" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtstockitemfilter.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        txtserailno.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlcategorysearch.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {
         
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch.SelectedValue = CompanyLocationID;
            ddllocationsearch.Enabled = false;
          
        }
        BindGrid(0);
    }

    protected void btnClearAll3_Click(object sender, EventArgs e)
    {
        txtWholesaleOrderNo.Text = string.Empty;
        txtstockitemfilter3.Text = string.Empty;
        txtSearchOrderNo3.Text = string.Empty;
        txtserailno3.Text = string.Empty;
        txtInvoiceNo.Text = string.Empty;
        ddllocationsearch3.SelectedValue = "";
        ddlcategorysearch3.SelectedValue = "";
        ddldate3.SelectedValue = "";
        txtstartdate3.Text = string.Empty;
        txtenddate3.Text = string.Empty;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (Roles.IsUserInRole("Warehouse"))
        {

         
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddllocationsearch3.SelectedValue = CompanyLocationID;
            ddllocationsearch3.Enabled = false;
       

        }
        BindGrid3(0);
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("WholeSale"))
        {
            TabContainer1.ActiveTabIndex = 0;

            BindGrid3(0);
            BindDropDown3();
        }
        else
        {
            if (TabContainer1.ActiveTabIndex == 0)
            {
                BindGrid(0);
                BindDropDown();
            }
            else if (TabContainer1.ActiveTabIndex == 1)
            {
                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

                if (Roles.IsUserInRole("Warehouse"))
                {

                    BindDropDown3();
                    DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                    string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                    ddllocationsearch3.SelectedValue = CompanyLocationID;
                    ddllocationsearch3.Enabled = false;
                    BindGrid3(0);

                }
                else
                {
                    BindGrid3(0);
                    BindDropDown3();
                }
            }
        }
    }

    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "printpage3")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("3", "", "", "", "", WholesaleOrderID, "", "", "", "", "", "");
            rptItems.DataBind();
            ModalPopupExtenderDetail.Show();
        }
        BindGrid3(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "printpage1")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            string ProjectNo = arg[0];
            string PicklistId = arg[1];

            if (ProjectNo != "0" && PicklistId != "0")
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("1", "", "", "", "", ProjectNo, "", "", PicklistId, "", "", "");
                rptItems.DataBind();
            }
            else
            {
                rptItems.DataSource = Reports.SerialNumberWiseReport_ByProjNoTransfIDWholeIDExcel("1", "", "", "", "", ProjectNo, "", "", "", "", "", "");
                rptItems.DataBind();
            }
            ModalPopupExtenderDetail.Show();
        }
        BindGrid(0);
    }
}