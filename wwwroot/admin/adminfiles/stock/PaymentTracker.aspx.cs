﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_Payment_Tracker : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindVendor();
            BindPaymentMethodType();
            BindPurchaseCompany();
            HidePanels();
            //txtStartDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");
            //txtEndDate.Text = DateTime.Now.AddHours(14).ToString("dd/MM/yyyy");

            BindGrid(0);
        }

    }

    public void BindPurchaseCompany()
    {
        rptCompany.DataSource = ClsPurchaseCompany.tbl_PurchaseCompany_Select();
        rptCompany.DataBind();
    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();

        //ddlSearchVendor.DataSource = dt;
        //ddlSearchVendor.DataTextField = "Customer";
        //ddlSearchVendor.DataValueField = "CustomerID";
        //ddlSearchVendor.DataBind();

        rptVender.DataSource = dt;
        rptVender.DataBind();
    }

    protected void BindPaymentMethodType()
    {
        DataTable dt = ClstblStockOrders.tblPaymentMethodtype_GetData();

        //ddlPaymentMethodType.DataSource = dt;
        //ddlPaymentMethodType.DataTextField = "PaymentMethodtype";
        //ddlPaymentMethodType.DataValueField = "PaymentMethodtypeID";
        //ddlPaymentMethodType.DataBind();
        //dt.Rows.Add(4, "Order Booked");

        ddlPMType.DataSource = dt;
        ddlPMType.DataTextField = "PaymentMethodtype";
        ddlPMType.DataValueField = "PaymentMethodtypeID";
        ddlPMType.DataBind();

        rptPaymentMethodType.DataSource = dt;
        rptPaymentMethodType.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string Vender = "";
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnCustomerID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                Vender += "," + hdnModule.Value.ToString();
            }
        }
        if (Vender != "")
        {
            Vender = Vender.Substring(1);
        }

        string PCompany = "";
        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                PCompany += "," + hdnModule.Value.ToString();
            }
        }
        if (PCompany != "")
        {
            PCompany = PCompany.Substring(1);
        }

        string PaymentMethodType = "";
        foreach (RepeaterItem item in rptPaymentMethodType.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnPaymentMethodTypeID");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                PaymentMethodType += "," + hdnModule.Value.ToString();
            }
        }
        if (PaymentMethodType != "")
        {
            PaymentMethodType = PaymentMethodType.Substring(1);
        }

        dt = ClstblStockOrders.tblStockOrders_SelectBySearchNewQuickStock_PaymentTracker(Vender, txtStartDate.Text, txtEndDate.Text, ddlDate.SelectedValue, txtSearchOrderNo.Text, txtstockitemfilter.Text, ddlhideshow.SelectedValue, ddlStockOrderStatus.SelectedValue, PaymentMethodType, ddlShow.SelectedValue, ddlPaymentMethod.SelectedValue, ddlPayment.SelectedValue, PCompany, ddlLocalInternation.SelectedValue, ddlDeliveryType.SelectedValue, ddlSearchGST_Type.SelectedValue);

        return dt;
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            decimal OrderAmount = 0;
            decimal TotalPaidAmt = 0;
            decimal RemAmount = 0;
            decimal PaidAud = 0;
            decimal PaidAudGST = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["GSTAmount"].ToString() != string.Empty)
                {
                    OrderAmount += Convert.ToDecimal(dr["GSTAmount"]);
                }
                if (dr["RemAmount"].ToString() != string.Empty)
                {
                    TotalPaidAmt += Convert.ToDecimal(dr["RemAmount"]);
                }
                if (dr["Deposit_amountAsd"].ToString() != string.Empty)
                {
                    PaidAud += Convert.ToDecimal(dr["Deposit_amountAsd"]);
                }
                if (dr["Deposit_amountAsdWithGST"].ToString() != string.Empty)
                {
                    PaidAudGST += Convert.ToDecimal(dr["Deposit_amountAsdWithGST"]);
                }
            }

            RemAmount = OrderAmount - TotalPaidAmt;
            lblttlordamt.Text = Convert.ToString(OrderAmount);
            lblttlpaidamt.Text = Convert.ToString(TotalPaidAmt);
            lblttlremamt.Text = Convert.ToString(RemAmount);
            lblPaidAud.Text = Convert.ToString(PaidAud);
            lblPaidAudGST.Text = PaidAudGST.ToString("F");
        }
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view.");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            panelTot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            panelTot.Visible = true;
            PanGrid.Visible = true;
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + (dt.Rows.Count - 1) + " entries";
                }
            }

        }
        //bind();
        BindTotal(dt);
    }

    public void bind()
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        if (dt.Rows.Count == 1)
        {
            if (string.IsNullOrEmpty(dt.Rows[0]["StockOrderID"].ToString()))
            {
                PanGrid.Visible = false;
                divnopage.Visible = false;
                Notification("There are no items to show in this view.");
            }
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtStartDate.Text = "";
        txtEndDate.Text = "";
        txtSearchOrderNo.Text = "";
        txtstockitemfilter.Text = "";
        ddlhideshow.SelectedValue = "";
        //ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        ddlPaymentMethod.SelectedValue = "";
        ddlPaymentStatus.SelectedValue = "";
        ClearCheckBox();
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

        Export oExport = new Export();
        string FileName = "PaymentTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        string[] arrHeader = { "OrderNo","InvoiceNo" ,"Location","BOLReceived", "Delivery Date", "DueDate" ,
                               "ManualOrderNo", "Payment Method Type", "ItemsOrdered", "Vendor", //"Exp.DeliveryDate",
                               "GST Type", "Qty", "Amt(USD)","Paid(USD)","Rem(USD)", "Paid(AUD)", "StockOrderStatus" //,"Rem(AUD)", 

                                //"Paid(USD) with GST", "Rem(USD) with GST"
                               //"Stock Order Status", "Order Type",
                               //"Full Payment", "Payment"
        };

        int[] ColList = { 1, 17, 38, 13, 16, 31,
                          15, 25, 39, 40, //6, 
                          49, 32, 34, 35, 44, 46, 47
                          //50, 51
                          // 47, 48,
                          //51, 52

        };

        try
        {
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

        }
        catch (Exception ex)
        {

        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    private void HidePanels()
    {

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch (Exception ex) { }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Payment")
        {
            // int index = Convert.ToInt32(e.CommandArgument);
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');
            string OrderId = arg[0].ToString();
            hdnStockOrderID1.Value = OrderId;
            hdnpmt.Value = arg[1].ToString();
            btnSave.Visible = true;
            btnEdit.Visible = false;
            txtpaymentnotes.Text = "";
            txtaud.Text = "";
            ddlGSTType.SelectedValue = "";

            SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(OrderId);
            ddlGSTType.SelectedValue = st.GST_Type;

            DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetDataByStockOrderId(OrderId);
            if (dt != null && dt.Rows.Count > 0)
            {
                PanGridNotes.Visible = true;
                GridViewNote.DataSource = dt;
                GridViewNote.DataBind();

                //if(dt.Rows[0]["GST_Type"].ToString() != "")
                //{
                //    ddlGSTType.SelectedValue = dt.Rows[0]["GST_Type"].ToString();
                //}
            }
            else
            {
                PanGridNotes.Visible = false;
            }

            tctdepodate.Text = "";
            txtRate.Text = "";
            txtAmountUsd.Text = "";
            txtaud.Text = "";
            txtpaymentnotes.Text = "";
            ddlPMType.SelectedValue = "";
            
            ModalPopupExtenderNote.Show();

        }
        // BindGrid(0);
    }

    protected void BindGridNotes(string OrderID)
    {
        DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetDataByStockOrderId(OrderID);
        if (dt != null && dt.Rows.Count > 0)
        {
            PanGridNotes.Visible = true;
            GridViewNote.DataSource = dt;
            GridViewNote.DataBind();
        }
        else
        {
            PanGridNotes.Visible = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string id1 = hdnStockOrderID1.Value;
        if (id1 != null && id1 != "")
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

            int s1 = ClstblStockOrders.Tblstockorder_PaymentDetail_Insert(id1, tctdepodate.Text, txtRate.Text, txtAmountUsd.Text, txtaud.Text, userid);

            if (s1 > 0)
            {
                bool pmtNo = ClstblStockOrders.Tblstockorder_PaymentDetail_updatepaymentNotes(Convert.ToString((s1)), txtpaymentnotes.Text);

                bool Pmtype = ClstblStockOrders.Tblstockorder_PaymentDetail_UpdatePaymentMethodTypeByStockOrderId(s1.ToString(), ddlPMType.SelectedValue);

                //bool GST_Type = ClstblStockOrders.Tblstockorder_PaymentDetail_Update_GST_Type(s1.ToString(), ddlGSTType.SelectedValue);

                bool pmtupdate1 = ClstblStockOrders.tblstockorders_updatepaymentStatus(id1, ddlPMType.SelectedValue, ddlPMType.SelectedItem.Text);

                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(Convert.ToString((s1)));
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataTable dtStockOrderData = ClstblStockOrders.Tblstockorder_PaymentDetail_GetDataByStockOrderId(id1);
                    if (dtStockOrderData.Rows.Count > 0)
                    {
                        string sum = dtStockOrderData.Compute("Sum(Deposit_amountAsd)", "").ToString();

                        if (sum.Equals(hdnpmt.Value))
                        {
                            bool pmtupdate = ClstblStockOrders.tblstockorders_updatepaymentStatus(id1, "2", "Final Payment");
                        }
                    }
                    PanGridNotes.Visible = true;
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();
                }
                else
                {
                    PanGridNotes.Visible = false;
                    Notification("No data Found..");
                }

                SetAdd();

            }
            else
            {
                SetError();
            }

            tctdepodate.Text = "";
            txtRate.Text = "";
            txtAmountUsd.Text = "";
            txtaud.Text = "";
            txtpaymentnotes.Text = "";
            ddlPMType.SelectedValue = "";
            //ddlGSTType.SelectedValue = "";

            //BindGridNotes(id1);
            BindGrid(0);

            //ModalPopupExtenderNote.Show();
        }
    }

    protected void GridViewNote_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void GridViewNote_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();

            if (ID != "")
            {
                bool succ = ClstblStockOrders.Tblstockorder_PaymentDetail_DeleteByStockOrderId(ID);
                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(ID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();
                    ModalPopupExtenderNote.Show();
                }
                else
                {
                    ModalPopupExtenderNote.Hide();
                }
            }
            BindGridNotes(hdnStockOrderID1.Value);

            BindGrid(0);
        }

        if (e.CommandName == "Edit")
        {
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            btnEdit.Visible = true;
            btnSave.Visible = false;
            hndpmtDetailsId1.Value = ID;

            DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(ID);
            if (dt.Rows.Count > 0)
            {
                PanGridNotes.Visible = true;
                txtRate.Text = dt.Rows[0]["UsdRate"].ToString();
                if (dt.Rows[0]["DepositDate"].ToString() != null && dt.Rows[0]["DepositDate"].ToString() != "")
                {
                    DateTime dt1 = Convert.ToDateTime(dt.Rows[0]["DepositDate"].ToString());
                    tctdepodate.Text = dt1.ToShortDateString();
                }
                txtAmountUsd.Text = dt.Rows[0]["Deposit_amountUsd"].ToString();
                txtaud.Text = dt.Rows[0]["Deposit_amountAsd"].ToString();
                txtpaymentnotes.Text = dt.Rows[0]["PaymentNotes"].ToString();

                if (dt.Rows[0]["PaymentMethodTypeID"].ToString() != "")
                {
                    ddlPMType.SelectedValue = dt.Rows[0]["PaymentMethodTypeID"].ToString();
                }

                //if (dt.Rows[0]["GST_Type"].ToString() != "")
                //{
                //    ddlGSTType.SelectedValue = dt.Rows[0]["GST_Type"].ToString();
                //}
            }
            else
            {
                PanGridNotes.Visible = false;
            }
            ModalPopupExtenderNote.Show();
            //BindGrid(0);
        }
    }

    protected void txtAmountUsd_TextChanged(object sender, EventArgs e)
    {
        decimal rate = 0;
        decimal Aud = 0;
        decimal usd = 0;
        if (txtAmountUsd.Text != null && txtAmountUsd.Text != "")
        {
            usd = Convert.ToDecimal(txtAmountUsd.Text);
        }
        if (txtRate.Text != null && txtRate.Text != "")
        {
            rate = Convert.ToDecimal(txtRate.Text);

        }
        Aud = usd / rate;
        decimal Aud1 = Math.Round(Aud, 2);
        txtaud.Text = Convert.ToString(Aud1);

        ModalPopupExtenderNote.Show();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (hndpmtDetailsId1.Value != null && hndpmtDetailsId1.Value != "")
        {
            bool s1 = ClstblStockOrders.Tblstockorder_PaymentDetail_Update(hndpmtDetailsId1.Value, txtRate.Text, txtaud.Text, txtAmountUsd.Text, tctdepodate.Text);
            bool s2 = ClstblStockOrders.Tblstockorder_PaymentDetail_updatepaymentNotes(hndpmtDetailsId1.Value, txtpaymentnotes.Text);
            //bool Pmtype = ClstblStockOrders.Tblstockorder_PaymentDetail_UpdatePaymentMethodTypeByStockOrderId(s1.ToString(), ddlPMType.SelectedValue);
            //bool s3 = ClstblStockOrders.Tblstockorder_PaymentDetail_Update_GST_Type(hndpmtDetailsId1.Value, ddlGSTType.SelectedValue);
            if (s1)
            {
                DataTable dt = ClstblStockOrders.Tblstockorder_PaymentDetail_GetData(hndpmtDetailsId1.Value);
                if (dt.Rows.Count > 0)
                {
                    GridViewNote.DataSource = dt;
                    GridViewNote.DataBind();
                    tctdepodate.Text = "";
                    txtRate.Text = "";
                    txtAmountUsd.Text = "";
                    txtaud.Text = "";
                    txtpaymentnotes.Text = "";
                    ddlPMType.SelectedValue = "";
                }
                else
                {
                    tctdepodate.Text = "";
                    txtRate.Text = ""; txtAmountUsd.Text = "";
                    txtaud.Text = "";
                    txtpaymentnotes.Text = "";
                    ddlPMType.SelectedValue = "";
                    SetError();
                }
                SetAdd();
            }
            else
            {
                SetError();
            }
            ModalPopupExtenderNote.Hide();

        }
        //BindGridNotes(hndpmtDetailsId1.Value);
        BindGrid(0);
    }

    protected void GridViewNote_SelectedIndexChanged(object sender, EventArgs e)
    {

        //   string id = GridViewNote.DataKeys[e.NewSelectedIndex].Value.ToString();
    }

    protected void GridViewNote_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void GridViewNote_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }

    public void SetAdd()
    {
        //  Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetError()
    {
        //Reset();
        HidePanels();
        Notification("Transaction Failed.");
        //PanError.Visible = true;
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rptVender.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptCompany.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptPaymentMethodType.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false; 
        }
    }

    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        decimal rate = 0;
        decimal Aud = 0;
        decimal usd = 0;
        if (txtAmountUsd.Text != null && txtAmountUsd.Text != "")
        {
            usd = Convert.ToDecimal(txtAmountUsd.Text);
        }
        if (txtRate.Text != null && txtRate.Text != "")
        {
            rate = Convert.ToDecimal(txtRate.Text);

        }
        Aud = usd / rate;
        decimal Aud1 = Math.Round(Aud, 2);
        txtaud.Text = Convert.ToString(Aud1);

        ModalPopupExtenderNote.Show();
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

        Export oExport = new Export();
        string FileName = "PaymentTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

        string[] arrHeader = { "OrderNo","InvoiceNo","Location","Order Date","Warehouse Delivery Date"
                ,"Due Date","Manual OrderNo","Vendor","Qty","Total PI Amount (USD)","Paid (USD)"
                ,"Paid (AUD)", "Balance (USD)", "StockOrderStatus" //,"Balance Paid (AUD)","Remaning Amt (USD)"
        };

        int[] ColList = { 1, 17, 38, 5, 16, 31, 15, 40, 32, 34, 35, 46, 44, 47 };

        try
        {
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);

        }
        catch (Exception ex)
        {

        }

    }
}