using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;

public partial class admin_adminfiles_stock_stocktransfer : System.Web.UI.Page
{

    protected string mode = "";
    protected string SiteURL;
    static DataView dv;
    static int MaxAttribute = 1;
    protected DataTable rptproduct;
    //protected string tqty;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        HidePanels();

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            if (Roles.IsUserInRole("Warehouse"))
            {
                BindDropDown();
                DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
                string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
                ddlsearchtransferfrom.SelectedValue = CompanyLocationID;
                ddlsearchtransferfrom.Enabled = false;
                ddltransferfrom.SelectedValue = CompanyLocationID;
                ddltransferfrom.Enabled = false;
            }
            else { BindDropDown(); }

            BindGrid(0);
          
            MaxAttribute = 1;
            bindrepeaterBox();
            BindAddedAttributeBox();

            SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            //    lblname.Text = st1.EmpFirst + " " + st1.EmpLast;
            // lblname.Text = "";

            ltTransfered.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Warehouse")) || (Roles.IsUserInRole("Wholesale")) || (Roles.IsUserInRole("Quick Stock")))
            {
                lnkAdd.Visible = true;

                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
        }
        BindScript();
    }

    public void BindDropDown()
    {
        DataTable dt = ClstblEmployees.tblEmployees_SelectAll();
        ddlemployee.DataSource = dt;
        ddlemployee.DataTextField = "fullname";
        ddlemployee.DataValueField = "EmployeeID";
        ddlemployee.DataBind();
        if ((Roles.IsUserInRole("Administrator"))|| (Roles.IsUserInRole("Warehouse")))
        {
            ddlemployee.Enabled = true;

            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

            DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
            //  Response.Write(dt_user.Rows.Count);
            if (dt_user.Rows.Count > 0)
            {
                // Response.Write(dt_user.Rows[0]["EmployeeID"].ToString());
                ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

            }
        }
        else
        {
            ddlemployee.Enabled = false;
            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            // Response.Write(userid2);
            DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
            if (dt_user.Rows.Count > 0)
            {

                ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

            }
            //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
            //  ddlemployee.SelectedValue = st.EmployeeID;
        }
        DataTable dt2 = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddltransferfrom.DataSource = dt2;
        ddltransferfrom.DataTextField = "location";
        ddltransferfrom.DataValueField = "CompanyLocationID";
        ddltransferfrom.DataBind();

        ddltransferto.DataSource = dt2;
        ddltransferto.DataTextField = "location";
        ddltransferto.DataValueField = "CompanyLocationID";
        ddltransferto.DataBind();

        ddlsearchtransferfrom.DataSource = dt2;
        ddlsearchtransferfrom.DataTextField = "location";
        ddlsearchtransferfrom.DataValueField = "CompanyLocationID";
        ddlsearchtransferfrom.DataBind();

        ddlsearchtransferto.DataSource = dt2;
        ddlsearchtransferto.DataTextField = "location";
        ddlsearchtransferto.DataValueField = "CompanyLocationID";
        ddlsearchtransferto.DataBind();

        ddlby.DataSource = dt;
        ddlby.DataTextField = "fullname";
        ddlby.DataValueField = "EmployeeID";
        ddlby.DataBind();

        DataTable dtTransfComp = ClstblStockTransfers.tblTransferCompany_GetData();
        ddlTransferCompany.DataSource = dtTransfComp;
        ddlTransferCompany.DataTextField = "TransferCompany";
        ddlTransferCompany.DataValueField = "TransferCompanyID";
        ddlTransferCompany.DataBind();

        ddlSearchTransferCompany.DataSource = dtTransfComp;
        ddlSearchTransferCompany.DataTextField = "TransferCompany";
        ddlSearchTransferCompany.DataValueField = "TransferCompanyID";
        ddlSearchTransferCompany.DataBind();

    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        dt = ClstblStockTransfers.tblStockTransfersGetDataBySearchNew(txtstockitemmodelfilter.Text, txttsfrNo.Text, ddlby.SelectedValue.ToString(), ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString(), ddlReceived.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddlTransferedOrNot.SelectedValue, ddlSearchTransferCompany.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {

        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        divtot.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divtot.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            BindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string Transfered = ltTransfered.Text;
        string employee = ddlemployee.SelectedValue.ToString();
        string transferfrom = ddltransferfrom.SelectedValue.ToString();
        string transferto = ddltransferto.SelectedValue.ToString();
        string tracking = txttracking.Text;
        string datereceived = txtdatereceived.Text;
        string note = txtnote.Text;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        int success = 0;
        int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(tracking);
        if (existaddress != 1)
        {
            success = ClstblStockTransfers.tblStockTransfers_Insert(transferfrom, transferto, Transfered, employee, datereceived, "", note, tracking);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            foreach (RepeaterItem item in rptstock.Items)
            {
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
                DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
                string item1 = ddlitem.SelectedValue.ToString();
                string qty = txtqty.Text.Trim();

                if (hdntype.Value == string.Empty)
                {
                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(success.ToString());
                    SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationTo);

                    string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();
                    //string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    //comment by kiran 13-02-2019
                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1);
                    ////ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationTo, qty2);

                    ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationFrom, stLocationFrom.StockQuantity, (0 - Convert.ToInt32(qty)).ToString(), userid, "0");
                    //ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationTo, stLocationTo.StockQuantity, qty, userid, "0");

                    int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(success.ToString(), item1, qty);
                    //Response.Write(success1);
                }
            }
            //Response.End();

            int UpdateTrabfCom = ClstblStockTransfers.tblStockTransfers_UpdateTransferCompanyID(success.ToString(), ddlTransferCompany.SelectedValue);

            int UpdateAmount = ClstblStockTransfers.tblStockTransfers_UpdateAmount(success.ToString(), txtAmount.Text);
            int UpdateCompanyID = ClstblStockTransfers.tblStockTransfers_UpdateCompanyID(success.ToString(), ddlCompany.SelectedValue);

            if (DivLocation.Visible == true)
            {
                int UpdateLocation = ClstblStockTransfers.tblStockTransfers_UpdateLocation(success.ToString(), txtLocation.Text);
            }

            if (success > 0)
            {
                ClstblStockTransfers.tblStockTransfers_UpdateTransferNumber(Convert.ToString(success));
                SetAdd();
            }
            else
            {
                SetError();
            }

        }
        else
        {
            PAnAddress.Visible = true;

        }
        BindGrid(0);

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string Transfered = ltTransfered.Text;
        string employee = ddlemployee.SelectedValue.ToString();
        string transferfrom = ddltransferfrom.SelectedValue.ToString();
        string transferto = ddltransferto.SelectedValue.ToString();
        string tracking = txttracking.Text;
        string datereceived = txtdatereceived.Text;
        string note = txtnote.Text;

        //Response.Write(dtQty.Rows.Count);
        //Response.End();

        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);

        bool success = false;
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id1);
        //int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(tracking);
        //if (existaddress != 1)
        {
            success = ClstblStockTransfers.tblStockTransfers_Update(id1, transferfrom, transferto, Transfered, employee, txtdatereceived.Text, stemp.EmployeeID, note, tracking);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            ClstblStockTransfers.tblStockTransferItems_DeletebyStockTransferID(id1);
            foreach (RepeaterItem item in rptstock.Items)
            {
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
                DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
                string item1 = "";
                if (ddlitem.SelectedValue.ToString() == string.Empty)
                {
                    item1 = "0";
                }
                else
                {
                    item1 = ddlitem.SelectedValue.ToString();
                }

                string qty = txtqty.Text.Trim();


                if (hdntype.Value == string.Empty)
                {
                    SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    //SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    //SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationTo);

                    //string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();
                    //string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1);
                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationTo, qty2);


                    string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                    DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(id1);
                    //   int qty2 = 0;
                    //   //Response.Write(dt.Rows.Count);
                    // if (dt.Rows.Count > 0)
                    //{
                    //     foreach (DataRow dr in dt.Rows)
                    //       {
                    //         if (dr["TransferQty"].ToString() != string.Empty)
                    //           {
                    //              qty2 += Convert.ToInt32(dr["TransferQty"].ToString());
                    //           }
                    //       }


                    // }
                    // Response.End();

                    //Comment by kiran 13-2-2019
                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1.ToString());
                    ////ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationFrom, stLocationFrom.StockQuantity, (0 - Convert.ToInt32(qty)).ToString(), userid, "0");
                    ////ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationTo, stLocationTo.StockQuantity, qty, userid, "0");

                    int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(id1, item1, qty);
                    //Response.Write(success1);
                }
            }
            //Response.End();

            int UpdateTrabfCom = ClstblStockTransfers.tblStockTransfers_UpdateTransferCompanyID(id1, ddlTransferCompany.SelectedValue);
            int UpdateAmount = ClstblStockTransfers.tblStockTransfers_UpdateAmount(id1, txtAmount.Text);
            int UpdateCompanyID = ClstblStockTransfers.tblStockTransfers_UpdateCompanyID(id1, ddlCompany.SelectedValue);
            if (DivLocation.Visible == true)
            {
                int UpdateLocation = ClstblStockTransfers.tblStockTransfers_UpdateLocation(id1, txtLocation.Text);
            }

            if (success)
            {
                ClstblStockTransfers.tblStockTransfers_UpdateTransferNumber(id1);
                SetUpdate();

            }
            else
            {
                SetError();
            }
            Reset();
            BindGrid(0);
        }
        //else
        //{
        //    PAnAddress.Visible = true;
        //    BindGrid(0);
        //    //lblexistame.Visible = true;
        //    //lblexistame.Text = "Record with this Address already exists ";
        //}
        BindScript();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        mode = "Edit";
        PanGrid.Visible = false;
        divtot.Visible = false;

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnstockId.Value = id;
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
        //Response.Write(st.TransferBy);
        //Response.End();

        ltTransfered.Text = SiteConfiguration.ConvertToDateView(st.TransferDate);//st.TransferDate;
        try
        {

            ddlemployee.SelectedValue = st.TransferBy;
        }
        catch
        {
        }
        ddltransferfrom.SelectedValue = st.LocationFrom;
        try
        {
            ddltransferto.SelectedValue = st.LocationTo;
            txttracking.Text = st.TrackingNumber;
            txtnote.Text = st.TransferNotes;
            //DateTime dte = Convert.ToDateTime(st.ReceivedDate);
            //txtdatereceived.Text = dte.ToString("dd/MM/yyyy");

        }
        catch
        {
        }
        try
        {
            ddlTransferCompany.SelectedValue = st.TransferCompanyID;
        }
        catch
        {

        }

        txtAmount.Text = st.Amount;

        if(st.CompanyID != "")
        {
            ddlCompany.SelectedValue = st.CompanyID;
        }
        txtLocation.Text = st.Location;

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
        //Response.Write(id);
        //Response.End();
        if (dt.Rows.Count > 0)
        {
            MaxAttribute = dt.Rows.Count;
            rptstock.DataSource = dt;
            rptstock.DataBind();
        }
        else
        {
            MaxAttribute = 1;
            bindrepeaterBox();
            BindAddedAttributeBox();
        }
        ////--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
        BindScript();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
        BindScript();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        Reset();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        divtot.Visible = false;
        DivLocation.Visible = false;
        Reset();
        PanAddUpdate.Visible = true;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
        //  Response.Write(dt_user.Rows.Count);
        if (dt_user.Rows.Count > 0)
        {
            // Response.Write(dt_user.Rows[0]["EmployeeID"].ToString());
            ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

        }

        if (Roles.IsUserInRole("Warehouse"))
        {
            BindDropDown();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid2);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
        
            ddltransferfrom.SelectedValue = CompanyLocationID;
            ddltransferfrom.Enabled = false;
        }


        //ModalPopupExtender2.Show();
        InitAdd();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();

        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        Notification("Transaction Successful.");
        //SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindDropDown();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
        
            ddltransferfrom.SelectedValue = CompanyLocationID;
            ddltransferfrom.Enabled = false;
        }

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true; 
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;

        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void Reset()
    {

        PanAddUpdate.Visible = true;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        ddlemployee.SelectedValue = st.EmployeeID;
        ddltransferfrom.ClearSelection();
        ddlemployee.ClearSelection();
        ddltransferto.ClearSelection();
        txttracking.Text = string.Empty;
        ddlTransferCompany.SelectedValue = "";
        txtdatereceived.Text = string.Empty;
        txtnote.Text = string.Empty;
        ddlTransferCompany.SelectedValue = "";
       
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindDropDown();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid2);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();

            ddltransferfrom.SelectedValue = CompanyLocationID;
            ddltransferfrom.Enabled = false;
        }

        MaxAttribute = 1;
        bindrepeaterBox();
        BindAddedAttributeBox();

        BindScript();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        BindGrid(0);
        BindScript();
    }

    protected void ddltransferfrom_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddltransferto.Items.Remove(ddltransferto.Items.FindByValue(ddltransferfrom.SelectedValue.ToString()));
        // ModalPopupExtender2.Show();
        BindScript();
    }

    protected void ddltransferto_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddltransferfrom.Items.Remove(ddltransferfrom.Items.FindByValue(ddltransferto.SelectedValue.ToString()));
        // ModalPopupExtender2.Show();
        BindScript();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "received")
        {
            string StockTransferID = e.CommandArgument.ToString();

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived(StockTransferID);
            BindGrid(0);
        }

        if (e.CommandName.ToLower() == "print")
        {

            string StockTransferID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/transfer.aspx?id=" + StockTransferID);
        }

        if (e.CommandName.ToLower() == "printpage")
        {
            ModalPopupExtenderDetail.Hide();

            string id = e.CommandArgument.ToString();

            SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
            lblFrom.Text = st.FromLocation;
            lblTo.Text = st.ToLocation;
            try
            {
                lblTransferDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.TransferDate));
            }
            catch { }
            lblTransferBy.Text = st.TransferByName;

            lblTrackingNo.Text = st.TrackingNumber;
            try
            {
                lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedDate));
            }
            catch { }
            lblReceivedBy.Text = st.ReceivedByName;
            lblNotes.Text = st.TransferNotes;

            DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
            if (dt.Rows.Count > 0)
            {

                rptItems.DataSource = dt;
                rptItems.DataBind();

                int qty = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["TransferQty"].ToString() != string.Empty)
                    {
                        qty += Convert.ToInt32(dr["TransferQty"].ToString());
                    }
                }
                lbltotalqty.Text = qty.ToString();
            }
            ModalPopupExtenderDetail.Show();
            // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>printContent();</script>");

        }

        if (e.CommandName.ToLower() == "download")
        {
            string [] arg = e.CommandArgument.ToString().Split(';');

            string StockTransferID = arg[0];
            string LocationID = arg[1];
            string StockTransferNumber = arg[2];

            DataTable dt = ClstblStockTransfers.tblStockSerialNo_GetData_ByTransferID(StockTransferID, LocationID);

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            try
            {
                Export oExport = new Export();
                string FileName = "StockTransferSerialNo_" + StockTransferNumber + ".xls";
                int[] ColList = { 1, 0 };
                string[] arrHeader = { "Stock Item", "Serial No" };
                //only change file extension to .xls for excel file
                oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
            }
            catch (Exception Ex)
            {
                //   lblError.Text = Ex.Message;
            }
        }

        if (e.CommandName == "View")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            string StockTransferID = arg[0];
            string Received = arg[1];

            hndSerialNoTransferID.Value = StockTransferID;
            hndReceived.Value = Received;

            string ModuleName = "";
            if(Received == "False")
            {
                ModuleName = "Transfer Out";
            }
            else
            {
                ModuleName = "Transfer In";
            }

            DataTable dt = ClstblStockTransfers.tblMaintainHistory_GetSerialNo_BySectionIDModuleName(StockTransferID, ModuleName);
            rptSerialNo.DataSource = dt;
            rptSerialNo.DataBind();
            ModalPopupExtenderView.Show();
        }

        //if (e.CommandName == "View")
        //{
        //    string[] arg = e.CommandArgument.ToString().Split(';');

        //    string StockTransferID = arg[0];
        //    string Received = arg[1];

        //    hndSerialNoTransferID.Value = StockTransferID;
        //    hndReceived.Value = Received;

        //    string ModuleName = "";
        //    if (Received == "False")
        //    {
        //        ModuleName = "Transfer Out";
        //    }
        //    else
        //    {
        //        ModuleName = "Transfer In";
        //    }

        //    DataTable dt = ClstblStockTransfers.tblMaintainHistory_GetSerialNo_BySectionIDModuleName(StockTransferID, ModuleName);
        //    rptSerialNo.DataSource = dt;
        //    rptSerialNo.DataBind();
        //    ModalPopupExtenderView.Show();
        //}
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(hdnstockid.Value);

        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            H1.Visible = true;
            btnYes.Visible = true;
            btnNo.Visible = true;
            btnReturn.Visible = false;
            btntansite.Visible = false;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void btnYes_Click(object sender, EventArgs e)
    {

        if (hdnstocktransferid.Value != string.Empty)
        {
            string StockTransferID = hdnstocktransferid.Value;

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";

            if (dtQty.Rows.Count > 0)
            {


                for (int i = 0; i < dtQty.Rows.Count; i++)
                {

                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();
                    bool succ = ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);

                    //SttblStockItemsLocation stLocationfrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationFrom);
                    //string qty3 = (Convert.ToInt32(stLocationfrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationFrom, qty3);

                }

            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived(StockTransferID);
            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
            bool success = ClstblStockTransfers.tblStockTransfers_Update_receive(StockTransferID, SiteConfiguration.Getdate_current(), stemp.EmployeeID);

        }
        BindGrid(0);
        BindScript();
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;
    }

    //==Repeater
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*]{2})",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);

        return Convert.ToInt32(match.Value);
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        BindItem(item);
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        BindScript();
    }

    public void BindItem(RepeaterItem item)
    {
        DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
        DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");

        ddlitem.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Value = "";
        item1.Text = "Select";
        ddlitem.Items.Add(item1);

        //if (!string.IsNullOrEmpty(Request.QueryString["StockTransferID"]))
        {
            //string StockTransferID = Request.QueryString["StockTransferID"];
            //SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
            if (ddltransferfrom.SelectedValue != string.Empty && ddltransferto.SelectedValue != string.Empty)
            {
                if (ddlcategory.SelectedValue != "")
                {
                    DataTable dt = ClstblStockItems.tblStockItems_SelectByCategoryID_CompanyLocationID(ddlcategory.SelectedValue, ddltransferfrom.SelectedValue, ddltransferto.SelectedValue);
                    ddlitem.DataSource = dt;
                    ddlitem.DataTextField = "StockItemName";
                    ddlitem.DataValueField = "StockCode";
                    ddlitem.DataBind();
                }
            }
        }
    }

    protected void rptstock_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hndcategory = (HiddenField)e.Item.FindControl("hndcategory");
        HiddenField hndid = (HiddenField)e.Item.FindControl("hndid");
        HiddenField hdnstockcode = (HiddenField)e.Item.FindControl("hdnstockcode");
        HiddenField hdnQty = (HiddenField)e.Item.FindControl("hdnQty");
        HiddenField hdnAvailableQuantity = (HiddenField)e.Item.FindControl("hdnAvailableQuantity");
        TextBox txtqty = (TextBox)e.Item.FindControl("txtqty");
        TextBox txtAvailableQuantity = (TextBox)e.Item.FindControl("txtAvailableQuantity");
        DropDownList ddlitem = (DropDownList)e.Item.FindControl("ddlitem");
        DropDownList ddlcategory = (DropDownList)e.Item.FindControl("ddlcategory");
        
        txtqty.Text = hdnQty.Value;
        txtAvailableQuantity.Text = hdnAvailableQuantity.Value;

        DataTable dt = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategory.DataSource = dt;
        ddlcategory.DataTextField = "StockCategory";
        ddlcategory.DataValueField = "StockCategoryID";
        ddlcategory.DataBind();

        ddlcategory.SelectedValue = hndcategory.Value;
        ddlitem.SelectedValue = hdnstockcode.Value;

        BindItem(item);

        if (mode == "Edit")
        {
            DataTable dt11 = ClstblStockItems.tblStockItems_Select();
            ddlitem.DataSource = dt11;
            ddlitem.DataTextField = "StockItem";
            ddlitem.DataValueField = "StockCode";
            ddlitem.DataBind();
            ddlitem.SelectedValue = hdnstockcode.Value;

            DataTable dtC = ClstblStockItems.tblStockItems_SelectByItemID_CompanyLocationID(ddlitem.SelectedValue, ddltransferfrom.SelectedValue, ddltransferto.SelectedValue);
            //   Response.Write(ddlitem.SelectedValue + "==" + ddltransferfrom.SelectedValue + "==" + ddltransferto.SelectedValue);
            ddlcategory.DataSource = dtC;
            ddlcategory.DataTextField = "StockCategory";
            ddlcategory.DataValueField = "StockCategoryID";
            ddlcategory.DataBind();
            try
            {
                // Response.Write(dtC.Rows[0]["StockCategoryID"].ToString());
                ddlcategory.SelectedValue = dtC.Rows[0]["StockCategoryID"].ToString();
            }
            catch
            {
            }
            BindItem(item);
            ScannedItems(item);
        }
        // Response.End();
        



        Button lbremove = (Button)e.Item.FindControl("lbremove");
        if (MaxAttribute == 1)
        {
            lbremove.Visible = false;
        }

    }

    protected void BindAddedAttributeBox()
    {
        rptproduct = new DataTable();
        rptproduct.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rptproduct.Columns.Add("StockCode", Type.GetType("System.String"));
        rptproduct.Columns.Add("TransferQty", Type.GetType("System.String"));
        rptproduct.Columns.Add("AvailableQty", Type.GetType("System.String"));
        rptproduct.Columns.Add("StockTransferItemsID", Type.GetType("System.String"));
        rptproduct.Columns.Add("type", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptstock.Items)
        {
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField hndcategory = (HiddenField)item.FindControl("hndcategory");
            HiddenField hndid = (HiddenField)item.FindControl("hndid");
            HiddenField hdnstockcode = (HiddenField)item.FindControl("hdnstockcode");
            HiddenField hdnQty = (HiddenField)item.FindControl("hdnQty");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
            DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
            DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");

            DataRow dr = rptproduct.NewRow();
            dr["StockCategoryID"] = ddlcategory.SelectedValue;
            dr["StockCode"] = ddlitem.SelectedValue;
            dr["TransferQty"] = txtqty.Text;
            dr["AvailableQty"] = txtAvailableQuantity.Text;
            dr["type"] = hdntype.Value;
            rptproduct.Rows.Add(dr);
        }
    }

    protected void bindrepeaterBox()
    {
        if (rptproduct == null)
        {
            rptproduct = new DataTable();
            rptproduct.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rptproduct.Columns.Add("StockCode", Type.GetType("System.String"));
            rptproduct.Columns.Add("TransferQty", Type.GetType("System.String"));
            rptproduct.Columns.Add("AvailableQty", Type.GetType("System.String"));
            rptproduct.Columns.Add("StockTransferItemsID", Type.GetType("System.String"));
            rptproduct.Columns.Add("type", Type.GetType("System.String"));
        }

        for (int i = rptproduct.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rptproduct.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockCode"] = "";
            dr["TransferQty"] = "";
            dr["AvailableQty"] = "";
            dr["type"] = "";
            rptproduct.Rows.Add(dr);
        }

        rptstock.DataSource = rptproduct;
        rptstock.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptstock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("lbremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptstock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptstock.Items)
                {
                    Button lbremove = (Button)item2.FindControl("lbremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void btnAddBoxRow_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptstock.Items)
        {
            HiddenField hndcategory = (HiddenField)item.FindControl("hndcategory");
            HiddenField hndid = (HiddenField)item.FindControl("hndid");
            HiddenField hdnstockcode = (HiddenField)item.FindControl("hdnstockcode");
            HiddenField hdnQty = (HiddenField)item.FindControl("hdnQty");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");

            DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
            DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

            hndcategory.Value = ddlcategory.SelectedValue;
            hdnstockcode.Value = ddlitem.SelectedValue;
            hdnQty.Value = txtqty.Text;
        }
        AddmoreAttributeBox();
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        //ModalPopupExtender2.Show();
    }

    protected void AddmoreAttributeBox()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttributeBox();
        bindrepeaterBox();
    }

    protected void lbremove_Click(object sender, EventArgs e)
    {

        int index = GetControlIndex(((Button)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;

        foreach (RepeaterItem item1 in rptstock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("lbremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptstock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptstock.Items)
                {
                    Button lbremove = (Button)item2.FindControl("lbremove");
                    lbremove.Visible = false;
                }
            }
        }
        PanAddUpdate.Visible = true;
        // ModalPopupExtender2.Show();
        divstockdetail.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;
        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void txttracking_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        // ModalPopupExtender2.Show();
        //  myModal.Visible = true;
        txttracking.Focus();

        if (txttracking.Text.Trim() != string.Empty)
        {
            int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(txttracking.Text.Trim());
            if (existaddress == 1)
            {
                ModalPopupExtenderTracker.Show();
                DataTable dt = ClstblStockTransfers.tblStockTransfers_SelectExits_TrackingNumber(txttracking.Text.Trim());
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
        }
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        BindScript();
    }

    protected void gvbtnPrint_Click(object sender, EventArgs e)
    {

        LinkButton gvbtnPrint = (LinkButton)sender;
        GridViewRow item1 = gvbtnPrint.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");




    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }
    
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlReceived.SelectedValue = "0";
        ddlby.SelectedValue = "";
        ddlsearchtransferfrom.SelectedValue = "";
        ddlsearchtransferto.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlTransferedOrNot.SelectedValue = "False";
        ddlSearchTransferCompany.SelectedValue = "";
        txtstockitemmodelfilter.Text = string.Empty;
        txttsfrNo.Text = string.Empty;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Warehouse"))
        {
            BindDropDown();
            DataTable dt1 = ClstblCompanyLocations.tblCompanyLocation_selectUserId(userid);
            string CompanyLocationID = dt1.Rows[0]["CompanyLocationID"].ToString();
            ddlsearchtransferfrom.SelectedValue = CompanyLocationID;
            ddlsearchtransferfrom.Enabled = false;
            ddltransferfrom.SelectedValue = CompanyLocationID;
            ddltransferfrom.Enabled = false;
        }


        BindGrid(0);
        BindScript();
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    //protected void ibtnCancel_Click(object sender, EventArgs e)
    //{
    //    ModalPopupExtenderTracker.Hide();

    //}
    protected void ibtnCancel_Click1(object sender, EventArgs e)
    {
        // Response.End();
        //  ModalPopupExtenderTracker.Hide();
    }

    protected void ibtnCancelActive_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderTracker.Hide();

    }

    protected void lnkrevert_Click(object sender, EventArgs e)
    {
        LinkButton lnkrevert = (LinkButton)sender;
        GridViewRow item1 = lnkrevert.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(hdnstockid.Value);
        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;

            btnReturn.Visible = true;
            btntansite.Visible = true;
            divdetail.Visible = true;
            H1.Visible = true;
            btnYes.Visible = false;
            btnNo.Visible = false;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void btn_yes_Click(object sender, EventArgs e)
    {

    }

    protected void btntansite_Click(object sender, EventArgs e)
    {

        if (hdnstocktransferid.Value != string.Empty)
        {
            string StockTransferID = hdnstocktransferid.Value;

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived_revert(StockTransferID);
        }

        BindGrid(0);
        BindScript();
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        if (hdnstocktransferid.Value != string.Empty)
        {
            string StockTransferID = hdnstocktransferid.Value;

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);

                    SttblStockItemsLocation stLocationfrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationFrom);
                    string qty3 = (Convert.ToInt32(stLocationfrom.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationFrom, qty3);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived_revert(StockTransferID);
        }

        BindGrid(0);
        BindScript();
    }

    protected void btnDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderTracker.Hide();
        HidePanels();
        txttracking.Text = string.Empty;
    }

    protected void btnNotDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderTracker.Hide();
        DupButton();
        txtdatereceived.Focus();
        BindScript();
    }

    public void DupButton()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //PanSearch.Visible = false;
        PanGrid.Visible = false;
        divtot.Visible = false;
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
            string id = hdndelete.Value.Replace(",", "");

            SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
            DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
            foreach (DataRow dr in dt.Rows)
            {

                string item1 = dr["StockItem"].ToString();
                string qty = dr["TransferQty"].ToString();

                if (item1 != string.Empty && qty != string.Empty)
                {
                    //Response.Write(item1+"=="+ st.LocationFrom);
                    SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dr["StockCode"].ToString(), st.LocationFrom);

                    string qty1 = (Convert.ToInt64(stLocationFrom.StockQuantity) + (Convert.ToInt64(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(dr["StockCode"].ToString(), st.LocationFrom, qty1);

                    //int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(id, dr["StockCode"].ToString(), qty);
                }
            }

            ClstblStockTransfers.tblStockTransferItems_DeletebyStockTransferID(id);
            ClstblStockTransfers.tblStockTransfers_Delete(id);
            //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
            Notification("Transaction Successful.");
            SetDelete();            
        //updatepanelgrid.Update();
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);

        //Response.Write( Session["deleteId"].ToString());
        //Response.End();
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    LinkButton gvbtnPrint = e.Row.FindControl("gvbtnPrint") as LinkButton;
        //    gvbtnPrint.Attributes.Add("onclick", "printContent()");
        //}
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        Response.Clear();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = "Stock Transfer_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1, 6, 28, 35, 26, 27, 32,
                             33, 34, 12, 23, 18, 31, 19, 30,
                             25 };
            string[] arrHeader = { "Transfer No", "Transfer Date", "Entered By", "Company Name", "Transfer From","Transfer To", "Items Transferred"
                                    ,"Qty","Transfer Company", "Cornet No.", "Amount", "Transferred","Transferred By", "Received", "Received By",
                                      "Location"
                };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlcategory");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlitem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");

        if (ddltransferfrom.SelectedValue != "")
        {
            if (ddlStockCategoryID.SelectedValue != "")
            {
                DataTable dtstock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddltransferfrom.SelectedValue, ddlStockItem.SelectedValue);
                if (dtstock.Rows.Count > 0)
                {
                    txtAvailableQuantity.Text = dtstock.Rows[0]["StockQuantity"].ToString();
                }
                else
                {
                    txtAvailableQuantity.Text = string.Empty;
                }
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();

    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlcategory");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlitem");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtqty");
        //HiddenField hdntransferid = (HiddenField)item.FindControl("hndid");
        string stockitemid = hdnstockId.Value;
        if (!string.IsNullOrEmpty(txtOrderQuantity.Text) && (!string.IsNullOrEmpty(stockitemid)))
        {
            DataTable datacount = Clstbl_WholesaleOrders.tblStockSerialNo_StockTransferCount(stockitemid, ddlStockItem.SelectedValue);
            string Count = datacount.Rows[0]["Column1"].ToString();
            if (Convert.ToInt32(txtOrderQuantity.Text) < Convert.ToInt32(Count.ToString()) )
        {
            Notification("Enter more then " + Convert.ToInt32(Count.ToString()) + " Quantity");
        }
        }
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void BindTotal(DataTable dt)
    {
        string Amount = dt.Compute("SUM(Amount)", string.Empty).ToString();
        lblTotalAmount.Text = Amount == "" ? "0.00" : Amount;

        string Followmont = dt.Compute("SUM(Amount)", "TransferCompany='Followmont'").ToString();
        lblFollowmont.Text = Followmont == "" ? "0.00" : Followmont;

        string TNT = dt.Compute("SUM(Amount)", "TransferCompany='TNT'").ToString();
        lblTNT.Text = TNT == "" ? "0.00" : TNT;

        string Mainfreight = dt.Compute("SUM(Amount)", "TransferCompany='Mainfreight'").ToString();
        lblMainfreight.Text = Mainfreight == "" ? "0.00" : Mainfreight;

        string INPerson = dt.Compute("SUM(Amount)", "TransferCompany='IN Person'").ToString();
        lblINPerson.Text = INPerson == "" ? "0.00" : INPerson;

        string Hi_Trans = dt.Compute("SUM(Amount)", "TransferCompany='Hi-Trans'").ToString();
        lblHiTrans.Text = Hi_Trans == "" ? "0.00" : Hi_Trans;
    }

    protected void ddltransferto_SelectedIndexChanged1(object sender, EventArgs e)
    {
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        if(ddltransferto.SelectedValue == "8")
        {
            DivLocation.Visible = true;
        }
        else
        {
            DivLocation.Visible = false;
        }
        BindScript();
    }

    protected void lbtnExcelSerialNo_Click(object sender, EventArgs e)
    {
        string StockTransferID = hndSerialNoTransferID.Value;
        string Received = hndReceived.Value;

        string ModuleName = "";
        if (Received == "False")
        {
            ModuleName = "Transfer Out";
        }
        else
        {
            ModuleName = "Transfer In";
        }

        DataTable dt = ClstblStockTransfers.tblMaintainHistory_GetSerialNo_BySectionIDModuleName(StockTransferID, ModuleName);
        Response.Clear();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = "TransferSerialNo_" + StockTransferID + ".xls";
            int[] ColList = { 0, 4, 3, 2 };
            string[] arrHeader = { "#", "Category", "Stock Item", "Serial No" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void ddlitem_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void lbtnExportSerialNo_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;

        string stockTransferId = lbtn.CommandArgument.ToString();

        DataTable dt = ClstblStockTransfers.SP_GetSerial_ByStockTransferId(stockTransferId);

        Response.Clear();

        string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

        try
        {
            Export oExport = new Export();
            string FileName = stockTransferId + " Transfer SerialNo.xls";
            int[] ColList = { 0, 1, 2, 3 };
            string[] arrHeader = { "Category", "Stock Item", "Stock Model", "Serial No" };
            
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnPdf_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;

        string stockTransferId = lbtn.CommandArgument.ToString();

        DataTable dt = ClstblStockTransfers.SP_GetSerial_ByStockTransferId(stockTransferId);

        DataTable dtSerialNo = new DataTable();
        dtSerialNo.Columns.Add("SerialNo1", typeof(string));
        dtSerialNo.Columns.Add("SerialNo2", typeof(string));
        dt.Columns[1].AllowDBNull = true;

        if (dt.Rows.Count % 2 != 0)
        {
            DataRow row = dt.NewRow();
            dt.Rows.Add(row);
        }

        for (int i = 0; i < dt.Rows.Count; i += 2)
        {
            DataRow dr = dtSerialNo.NewRow();
            dr["SerialNo1"] = dt.Rows[i]["SerialNo"];
            dr["SerialNo2"] = dt.Rows[i + 1]["SerialNo"];
            dtSerialNo.Rows.Add(dr);
        }

        if (dtSerialNo.Rows.Count > 0)
        {
            Telerik_reports.Generate_Barcode(stockTransferId, dtSerialNo);
        }
    }

    public void ScannedItems(RepeaterItem item)
    {
        DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
        DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
        TextBox txtqty = (TextBox)item.FindControl("txtqty");
        TextBox txtAvailableQuantity = (TextBox)item.FindControl("txtAvailableQuantity");
        Button lbremove = (Button)item.FindControl("lbremove");

        int Qty = ClstblStockTransfers.ScannedItem(ddlcategory.SelectedValue, hdnstockId.Value, ddlitem.SelectedValue);

        int TextQty = txtqty.Text == "" ? 0 : Convert.ToInt32(txtqty.Text);

        if(Qty == TextQty)
        {
            ddlcategory.Enabled = false;
            ddlitem.Enabled = false;
            txtqty.Enabled = false;
            txtAvailableQuantity.Enabled = false;
            lbremove.Visible = false;
        }
        else
        {
            ddlcategory.Enabled = true;
            ddlitem.Enabled = true;
            txtqty.Enabled = true;
            txtAvailableQuantity.Enabled = true;
            lbremove.Visible = true;
        }
    }
}