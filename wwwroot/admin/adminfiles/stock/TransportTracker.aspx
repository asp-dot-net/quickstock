﻿<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="TransportTracker.aspx.cs" Inherits="admin_adminfiles_stock_TransportTracker" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<meta  http-equiv="Refresh" content="5"> --%>

    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });


                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }


                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    //alert("begin");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress

                }

                function pageLoadedpro() {

                    //alert("end");

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/
                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $(document).ready(function () {


                    });

                    callMultiCheckbox1();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                    $(".Location .dropdown dt a").on('click', function () {
                        $(".Location .dropdown dd ul").slideToggle('fast');
                    });

                    callMultiCheckbox2();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox2();
                    });
                    $(".Customer .dropdown dt a").on('click', function () {
                        $(".Customer .dropdown dd ul").slideToggle('fast');
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }

                function doMyAction() {


                }

            </script>
            <script>
                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddLocation.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }
                }

                function callMultiCheckbox2() {
                    var title = "";
                    $("#<%=ddCustomer.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel1').show();
                        $('.multiSel1').html(html);
                        $(".hida1").hide();
                    }
                    else {
                        $('#spanselect1').show();
                        $('.multiSel1').hide();
                    }
                }
            </script>
            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Transport Tracker
                                       
                        </h5>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <%--<div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                        </div>--%>


                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170 ">
                                                    <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Invoice No\Order No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                        WatermarkText="Invoice No\Order No" />
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSearchOrderNo" ValidChars="," FilterType="Numbers, Custom"></cc1:FilteredTextBoxExtender>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlInvoiceType" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Invoice Type</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Draft</asp:ListItem>
                                                        <asp:ListItem Value="2">Invoice</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Locations</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect Location martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">Location</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddLocation" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rpLocation" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("location") %>' />
                                                                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("CompanyLocationID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("location")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchjobtype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Job Type</asp:ListItem>
                                                        <asp:ListItem Value="1">Cash</asp:ListItem>
                                                        <asp:ListItem Value="2">Credit</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtReferenceNo" runat="server" placeholder="STC ID" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtreferenceno"
                                                        WatermarkText="STC ID" />
                                                </div>

                                                <div class="input-group col-sm-2 max_width170 dnone">
                                                    <asp:DropDownList ID="ddlSearchdeliveryoption" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Delivery Option </asp:ListItem>
                                                        <asp:ListItem Value="1">Pick up</asp:ListItem>
                                                        <asp:ListItem Value="2">Transport</asp:ListItem>
                                                        <asp:ListItem Value="3">STC OutRight</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlTransporttype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Transport Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="Txtconsignno" runat="server" placeholder="Consign/Person" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="Txtconsignno"
                                                        WatermarkText="Consign/Person" />
                                                </div>

                                                <div class="form-group spical multiselect Customer martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida1" id="spanselect1">Customer</span>
                                                                <p class="multiSel1"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddCustomer" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptCustomer" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                <asp:HiddenField ID="hdnCustomerId" runat="server" Value='<%# Eval("CustomerID") %>' />


                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltCustomer" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Employee Name</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlCustDelivered" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Cust.Delivered</asp:ListItem>
                                                        <asp:ListItem Value="1">Not Dispatch</asp:ListItem>
                                                        <asp:ListItem Value="2">Transit</asp:ListItem>
                                                        <asp:ListItem Value="3">Delivered</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlPaid" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="1">Paid</asp:ListItem>
                                                        <asp:ListItem Value="2">Yes</asp:ListItem>
                                                        <asp:ListItem Value="3">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                        <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                        <asp:ListItem Value="3">Delivered</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    <%--<asp:Button ID="Button1" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click1" />--%>
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server" CausesValidation="false" OnClick="btnClearAll_Click1"
                                                        CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="finalgrid">
                 <asp:Panel ID="panel" runat="server">
                    <div class="page-header card" id="divtot" runat="server" visible="false">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Job:&nbsp;</b><asp:Literal ID="lblTotalJob" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Invoice:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Frieght Charge:&nbsp;</b><asp:Literal ID="lblTotalFriCharge" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Cust Frieght Charge:&nbsp;</b><asp:Literal ID="lblTotalCustFriCharge" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="GridView1_RowCreated" OnDataBound="GridView1_DataBound"
                                            OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand" OnRowUpdating="GridView1_RowUpdating"
                                            AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">
                                                            <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />--%>
                                                            <img id='imgdiv<%# Eval("WholesaleOrderID") %>' src="../../../images/icon_plus.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Order No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Width="80px" CssClass="gridmainspan">
                                                            <%#Eval("OrderNumber")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInvoiceNo" runat="server" Width="80px">
                                                            <%#Eval("InvoiceNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="InvoiceType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelinvtype2" runat="server" Width="80px">
                                                             <%#Eval("InvoiceType")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="Invoice Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="DateOrdered">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Width="100px">
                                                            <%#Eval("DateOrdered")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                               <%-- <asp:TemplateField HeaderText="Stock For" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Width="80px">
                                                            <%#Eval("CompanyLocation")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="Job Type" ItemStyle-VerticalAlign="top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="JobType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblJobType" runat="server" Width="80px" Text='<%# Eval("JobType")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Consign/Person" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="ConsignPerson">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label100" runat="server" Width="50px"><%#Eval("ConsignPerson")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Transport Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="TransportTypeName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTransportTypeName" runat="server" Width="80px" Text='<%# Eval("TransportTypeName") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="ReferenceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Width="80px">
                                                            <%#Eval("ReferenceNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delivery Option" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="DeliveryOption">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label50" runat="server" Width="80px">
                                                            <%#Eval("DeliveryOption")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="Customer">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label7" runat="server" data-placement="top" data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip"
                                                            Width="170px"><%#Eval("Customer")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label8" runat="server" Width="50px"><%#Eval("Qty")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Frieght Charge" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="FrieghtCharge">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFrieghtCharge" runat="server" Width="50px"><%#Eval("FrieghtCharge")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Cust.Frieght Charge" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="CustFrieghtCharge">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCustFrieghtCharge" runat="server" Width="50px"><%#Eval("CustFrieghtCharge")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delivered" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="StockDeductDateFormated">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Width="50px"><%#Eval("StockDeductDate")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delivered By" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="StockDeductedByName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label99" runat="server" Width="50px"><%#Eval("StockDeductedByName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="CustDelivered">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCustDelivered" runat="server" Width="70px"><%#Eval("CustDelivered")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="CustDeliveredNotes">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCustDeliveredNotes" runat="server" Width="70px" data-placement="top" data-original-title='<%#Eval("CustDeliveredNotes")%>' 
                                                            data-toggle="tooltip"><%#Eval("CustDeliveredNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Update" CausesValidation="false" CommandArgument='<%# Eval("WholesaleOrderID") %>'
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" CssClass="btn btn-info btn-mini">
                                                            <i class="fa fa-edit"></i> Update
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="gvbtnPaid" runat="server" CssClass="btn btn-danger btn-mini" CommandName="Paid"
                                                            CommandArgument='<%# Eval("WholesaleOrderID") %>' Visible='<%# Eval("IsPaid").ToString() == "0" ? true : false %>'
                                                            CausesValidation="false">                                                                          
                                              <i class="btn-label fa fa-close"></i> Paid
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="gvbtnPaid1" runat="server" CssClass="btn btn-success btn-mini"
                                                            CommandArgument='<%# Eval("WholesaleOrderID") %>' Visible='<%# Eval("IsPaid").ToString() == "1" ? true : false %>'
                                                            CausesValidation="false" >                                                                          
                                              <i class="btn-label fa fa-check"></i> Paid
                                                        </asp:LinkButton>

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="verticaaline" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("WholesaleOrderID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("WholesaleOrderID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td style="width: 2%;"><b>Id</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblwholesaleid" runat="server" Width="80px" Text='<%# Eval("WholesaleOrderID") %>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%;"><b>STC</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="LblSTC" runat="server" Width="80px" Text='<%#Eval("STCNumber").ToString()==""?"0":Eval("STCNumber")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%"><b>InstallType</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="LblInstallType" runat="server" Width="80px" Text='<%#Eval("InstallTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 7%"><b>Employee Name</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblEmployeeName" runat="server" Width="80px" Text='<%#Eval("EmployeeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%;"><b>InstallerName</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblInstallerName" runat="server" Width="80px" Text='<%#Eval("InstallerName")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%;"><b>Install Date</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblInstalldate" runat="server" Width="80px" Text='<%#Eval("Expecteddelivery","{0:dd MMM yyyy}")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%;"><b>Amount</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblamount" runat="server" Width="80px" Text='<%#Eval("InvoiceAmount")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%"><b>SolarType</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblSolarType" runat="server" Width="80px" Text='<%#Eval("SolarTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td><b>JobStatus</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblJobStatus" runat="server" Width="80px" Text='<%#Eval("JobTypeName")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%"><b>OrderItem</b>
                                                                                <asp:Label ID="Label33" runat="server" Width="80px" Text='<%#Eval("WholesaleOrderItem")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%;"><b>Stock For</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblStockfor" runat="server" Width="80px" Text='<%#Eval("CompanyLocation")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%;"><b>Invoice Type</b>
                                                                            </td>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label ID="lblInvoiceType" runat="server" Width="80px" Text='<%#Eval("InvoiceType")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                <div class="pagination">

                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid printorder" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelivery" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divModal" TargetControlID="Button12" CancelControlID="Button11">
            </cc1:ModalPopupExtender>
            <div id="divModal" runat="server" style="display: none" class="modal_popup ">
                <div class="modal-dialog" style="width: 500px; max-width: 1100px!important;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Customer Delivery</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-block" style="padding-left:12px">
                                    <div class="form-group row" style="padding-left: 15px;">
                                        <div style="padding-top: 8px">
                                            <asp:Label Text="Cust. Delivery" runat="server" />
                                            <asp:HiddenField runat="server" ID="hndWholesaleOrderID" />

                                        </div>
                                        <div class="input-group col-sm-4 max_width170">
                                            <asp:DropDownList ID="ddlYesNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Not Dispatch</asp:ListItem>
                                                <asp:ListItem Value="2">Transit</asp:ListItem>
                                                <asp:ListItem Value="3">Delivered</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row" style="padding-left: 15px;">
                                        <div style="padding-top:8px; width:95px">
                                            <asp:Label Text="Notes" runat="server" />
                                        </div>
                                        <div class="input-group col-sm-8">
                                           <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="100px" Width="100%"
                                                    CssClass="form-control modaltextbox">
                                                </asp:TextBox>
                                            <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnUpdate" runat="server" class="btn btn-danger POPupLoader" Text="Update" OnClick="btnUpdate_Click"></asp:Button>

                            <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button12" Style="display: none;" runat="server" />

            <asp:Button ID="btnverify" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderverify" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="modal_verify" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btnverify">
    </cc1:ModalPopupExtender>
    <div id="modal_verify" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
        <asp:HiddenField ID="hdnVerifyWholesaleOrderID" runat="server" />
        <div class="modal-dialog " style="margin-top: -300px">
            <div class=" modal-content">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header">
                    <h5 class="modal-title fullWidth">Paid
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                    </h5>
                </div>
                <div class="modal-body ">Are You Sure You want to Approve?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:Button ID="lnkapproved" runat="server" class="btn btn-danger POPupLoader" Text="Ok" OnClick="lnkapproved_Click" />
                    <asp:Button ID="Button7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                </div>
            </div>
        </div>

    </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>


    </asp:UpdatePanel>
</asp:Content>
