﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_PurchaseOrders : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected static string Void = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindDropdown();

            init();
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    #region GridView Comman Code
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        initAddUpdate("Update");

        getEditDetails(id);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewPDF")
        {
        }
    }
    #endregion

    public void BindDropdown()
    {
        DataTable dtCustomer = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlSearchCustomer.DataSource = dtCustomer;
        ddlSearchCustomer.DataTextField = "Customer";
        ddlSearchCustomer.DataValueField = "CustomerID";
        ddlSearchCustomer.DataBind();

        ddlCustomer.DataSource = dtCustomer;
        ddlCustomer.DataTextField = "Customer";
        ddlCustomer.DataValueField = "CustomerID";
        ddlCustomer.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string invoiceNo = txtSerachInvoiceNo.Text;
        string customerId = ddlSearchCustomer.SelectedValue;
        string stcId = txtSerachSTCId.Text;

        dt = Clstbl_WholesalePO.tabl_WholesalePO_GetData(invoiceNo, customerId, stcId);

        return dt;
    }

    public void bindTotal(DataTable dt)
    {

    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
            }
            panGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            panGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            bindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    public void init()
    {
        reset();
        lnkAdd.Visible = true;
        lnkBack.Visible = false;

        PanAddUpdate.Visible = false;

        panSearch.Visible = true;
        panGrid.Visible = true;
        BindGrid(0);
    }

    public void reset()
    {
        txtInvoiceNo.Text = string.Empty;
        ddlCustomer.ClearSelection();
        txtSTCID.Text = string.Empty;
        txtSTCQty.Text = string.Empty;
        txtSTCRate.Text = string.Empty;
        ddlPaymentStatus.ClearSelection();
        ddlPOType.ClearSelection();
    }

    public void initAddUpdate(string Mode)
    {
        reset();
        lblAddUpdate.Text = Mode + " ";

        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        PanAddUpdate.Visible = true;

        panGrid.Visible = false;
        panTotal.Visible = false;
        panSearch.Visible = false;

        if (Mode == "Add")
        {
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
        }
        else
        {
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        initAddUpdate("Add");
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        init();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int id = Clstbl_WholesalePO.tabl_WholesalePO_Insert();
        if (id > 0)
        {
            bool sucUpdate = updatePO(id);
            setAddUpdate();
        }
        else
        {
            SetError();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id = GridView1.DataKeys[GridView1.SelectedIndex].Value.ToString();

        bool sucUpdate = updatePO(Convert.ToInt32(id));

        if (sucUpdate)
        {
            setAddUpdate();
        }
        else
        {
            SetError();
        }
        //int PoExists = Clstbl_WholesaleOrders.tabl_WholesalePO_ExistsByInoviceNo(txtInvoiceNo.Text);
        //if (PoExists == 1)
        //{
        //    if (string.IsNullOrEmpty(txtManualOrderNumber.Text))
        //    {
        //        ModalPopupExtender5.Show();
        //    }
        //    else
        //    {
        //        UpdateInvoice("0");
        //    }
        //}
        //else
        //{
        //    UpdateInvoice("0");
        //}
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        init();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        reset();
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void setAddUpdate()
    {
        Notification("Transaction Success.");
        init();
    }

    public void setDelete()
    {
        Notification("Deleted Successfully.");
        init();
    }

    public void SetError()
    {
        Notification("Transaction Failed.");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtSerachInvoiceNo.Text = string.Empty;
        ddlSearchCustomer.ClearSelection();
        txtSerachSTCId.Text = string.Empty;

        BindGrid(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "Wholesaleorder_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 17, 58, 91, 74, 23, 15,
                              50, 29, 76, 72, 79, 81, 86,
                              0, 82, 83, 48, 87,
                              90, 24, 85, 84, 75 };

            string[] arrHeader = { "Invoice No", "Invoice Type", "Invoice Date", "Stock For", "Job Type", "STC ID",
                                "STC","Delivery Option","Customer","Qty","Delivered","Delivered By","EmployeeName",
                                "Id", "TransportType", "InstallType", "Consign/Person", "InstallerName",
                                "Install Date", "Amount", "SolarType", "JobStatus", "OrderItem" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void getEditDetails(string id)
    {
        Clstbl_WholesalePO.sttbl_WholesalePO st = Clstbl_WholesalePO.tabl_WholesalePO_GetDataByInvoiceNo(id);

        txtInvoiceNo.Text = st.invoiceNo;
        txtSTCID.Text = st.stcNo;
        txtSTCQty.Text = st.stcQty;
        txtSTCRate.Text = st.stcRate;
        try
        {
            ddlCustomer.SelectedValue = st.customerId;
            ddlPaymentStatus.SelectedValue = st.paymentStatus;
            ddlPOType.SelectedValue = st.POType;
        }
        catch (Exception ex)
        {

        }
    }

    protected bool updatePO(int id)
    {
        string invoiceNo = txtInvoiceNo.Text;
        string customerId = ddlCustomer.SelectedValue;
        string stcId = txtSTCID.Text;
        string stcQty = txtSTCQty.Text;
        string stcRate = txtSTCRate.Text;
        string paymentStatus = ddlPaymentStatus.SelectedValue;
        string poType = ddlPOType.SelectedValue;
        string poTypeText = ddlPOType.SelectedItem.Text;
        string createdOn = System.DateTime.Now.AddHours(15).ToString();
        string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        //decimal amount = (!string.IsNullOrEmpty(stcQty) ? Convert.ToDecimal(stcQty) : 0) * (!string.IsNullOrEmpty(stcRate) ? Convert.ToDecimal(stcRate) : 0);

        bool update = Clstbl_WholesalePO.tabl_WholesalePO_Update(id.ToString(), invoiceNo, customerId, stcId, stcQty, stcRate, paymentStatus, poType, poTypeText, createdOn, createdBy);

        return update;
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value;
        bool sucDelete = Clstbl_WholesalePO.tabl_WholesalePO_Delete(id);
        if (sucDelete)
        {
            setDelete();
        }
        else
        {
            SetError();
        }
    }
}