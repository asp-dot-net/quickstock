﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="stctracker.aspx.cs" Inherits="admin_adminfiles_stock_stctracker"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }

        .drpValidate {
            width: 100%;
        }

        .set1 .select2-container--default .select2-selection--multiple {
            border-radius: 0px;
            height: 37px;
            border: 1px solid #ccc !important;
        }

        .padding01 {
            padding-top: 20px;
        }
    </style>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });


                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                <%--function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }--%>


                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    //alert("begin");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress

                }

                function pageLoadedpro() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/
                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    <%--$('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    });--%>

                    $(document).ready(function () {

                        <%--$('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });--%>

                        $('.mutliSelect input[type="checkbox"]').on('click', function () {
                            callMultiCheckbox1();
                        });

                    });

                    callMultiCheckbox1();

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox1();
                    });
                   
                    $(".PVDStatus .dropdown dt a").on('click', function () {
                        $(".PVDStatus .dropdown dd ul").slideToggle('fast');
                    });
                    $(".PVDStatus .dropdown dd ul li a").on('click', function () {
                        $(".PVDStatus .dropdown dd ul").hide();
                    });


                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }

                function doMyAction() {

                   <%-- $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });--%>
                }

                function callMultiCheckbox1() {
                    var title = "";
                    $("#<%=ddPVDStatus.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }
                }


                function transfer(id) {
                    if (id == 3) {
                        //document.getElementById('DivTransferInvoiceNo').removeAttribute("class");
                        document.getElementById('DivTransferInvoiceNo').setAttribute("class", 'input-group col-sm-3 padd_btm10');
                        document.getElementById('DivPaymentType').setAttribute("class", 'input-group col-sm-3 padd_btm10');
                        document.getElementById('DivAmount').setAttribute("class", 'input-group col-sm-3 padd_btm10');
                        document.getElementById('Divbtn').setAttribute("class", 'col-sm-3 padd_btm10');
                    } else {

                        //document.getElementById('DivTransferInvoiceNo').removeAttribute("class");
                        document.getElementById('DivTransferInvoiceNo').setAttribute("class", 'input-group col-sm-3 padd_btm10 dnone');
                        document.getElementById('DivPaymentType').setAttribute("class", 'input-group col-sm-5 padd_btm10');
                        document.getElementById('DivAmount').setAttribute("class", 'input-group col-sm-5 padd_btm10');
                        document.getElementById('Divbtn').setAttribute("class", 'col-sm-2 padd_btm10');
                    }
                };

            </script>

            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>STC Tracker
                            <div id="hbreadcrumb" class="pull-right" style="display: none;">
                            </div>
                            <div class="pull-right">
                               <%-- <a href="#" id="anpanel" runat="server" class="btn  btn-warning" data-toggle="modal" data-target="#myModalpanel">Add</a> &nbsp;&nbsp; --%>
                        <div class="clear"></div>
                            </div>
                        </h5>

                    </div>
                </div>
            </div>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtreferenceno" runat="server" placeholder="STC ID" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtreferenceno"
                                                        WatermarkText="STC ID" />
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtpvdnumber" runat="server" placeholder="PVD Number" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtpvdnumber"
                                                        WatermarkText="PVD Number" />
                                                </div>

                                                <div class="form-group spical multiselect PVDStatus martop5 col-sm-2 max_width170 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselect">PVD Status</span>
                                                                <p class="multiSel"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddPVDStatus" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptPVDStatus" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnPVDStatus" runat="server" Value='<%# Eval("PVDStatus") %>' />
                                                                                <asp:HiddenField ID="hdnPVDStatusID" runat="server" Value='<%# Eval("PVDStatusID") %>' />

                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <%-- </span>--%>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltPVDStatus" Text='<%# Eval("PVDStatus")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlPaymentStatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                       <asp:ListItem Value="">Payment Status</asp:ListItem>
                                                        <asp:ListItem Value="Not Process">Not Process</asp:ListItem>
                                                        <asp:ListItem Value="In Process">In Process</asp:ListItem>
                                                        <asp:ListItem Value="Paid">Paid</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Payment Date</asp:ListItem>
                                                        <asp:ListItem Value="2">STC Submit</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server" CausesValidation="false"
                                                        OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa">
                                                    </i>Clear </asp:LinkButton>
                                                    &nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport1_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>

                </asp:Panel>
            </div>

            <div class="finalgrid ">
                <asp:Panel ID="PanTotal" runat="server" CssClass="xsroll">
                    <div>
                        <div class="table-responsive BlockStructure">
                            <div class="page-header card" id="div5" runat="server">
                                <div class="card-block brd_ornge" style="font-size: large">
                                    Total :  <b>
                                        <asp:Literal ID="lblPVDTotal" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    <%--Panels :  <b>
                                        <asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Inverters :  <b>
                                        <asp:Literal ID="lblTotalInverters" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Amount :  <b>
                                        <asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;--%>
                                    STC :  <b>
                                        <asp:Literal ID="lblTotalSTC" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Avg :  <b>
                                        <asp:Literal ID="lblAvg" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    New Submission :  <b>
                                        <asp:Literal ID="lblNewSubmission" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Submit to Trade :  <b>
                                        <asp:Literal ID="lblSubmittoTrade" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Pending Approval :  <b>
                                        <asp:Literal ID="lblPendingApproval" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Ready To Create :  <b>
                                        <asp:Literal ID="lblReadyToCreate" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    CER Failed :  <b>
                                        <asp:Literal ID="lblCERFailed" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Cannot re-create :  <b>
                                        <asp:Literal ID="lblCannotrecreate" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Not Yet Submitted :  <b>
                                        <asp:Literal ID="lblNotYetSubmitted" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Compliance Issues :  <b>
                                        <asp:Literal ID="lblComplianceIssues" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    CER Approved :  <b>
                                        <asp:Literal ID="lblCERApproved" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Under Review :  <b>
                                        <asp:Literal ID="lblUnderReview" runat="server"></asp:Literal></b>
                                    &nbsp;&nbsp;
                                    Blank :  <b>
                                        <asp:Literal ID="lblBlank" runat="server"></asp:Literal></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>

                        <div id="PanGrid" runat="server">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated1" OnRowUpdating="GridView1_RowUpdating" OnDataBound="GridView1_DataBound1" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                               
                                                <asp:TemplateField HeaderText="STC ID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="ReferenceNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Width="80px">
                                                <%#Eval("ReferenceNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC REC" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StatusID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTCValue" runat="server" Width="80px" Text='<%#string.IsNullOrEmpty(Eval("Stc_Value_Excel").ToString())?"":Eval("Stc_Value_Excel")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="StcValue">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStcValue" runat="server" Width="80px">
                                                            <%#Eval("StcValue")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PVD Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="PVDNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPVDNumber" runat="server" Width="80px">
                                                <%#Eval("PVDNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pvd Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="GB_STCStatus">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGB_STCStatus" runat="server" Width="80px" Text='<%#string.IsNullOrEmpty(Eval("GB_STCStatus").ToString())?"":Eval("GB_STCStatus")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC Submission Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Applied_date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplied_date" runat="server" Width="80px" Text='<%#Eval("Applied_date")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Payment Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="PaymentStatus">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaymentStatus" runat="server" Width="80px" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                <div class="pagination">

                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid printorder" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End Danger Modal Templates-->
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
