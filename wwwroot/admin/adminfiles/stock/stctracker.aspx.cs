﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stctracker : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string mode = "";
    protected string SiteURL;
    static DataView dv;

    #region Class
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    public class ClsBridgeSelect
    {
        public string crmid;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindPVDStatus();

            BindGrid(0);
        }
    }

    public void BindPVDStatus()
    {
        DataTable dt = Clstbl_WholesaleOrders.tbl_GBSTCStatus_Select();
        rptPVDStatus.DataSource = dt;
        rptPVDStatus.DataBind();
    }

    protected DataTable GetGridData()
    {
        string PVDStatus = "";
        foreach (RepeaterItem item in rptPVDStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnPVDStatusID = (HiddenField)item.FindControl("hdnPVDStatusID");

            if (chkselect.Checked == true)
            {
                PVDStatus += "," + hdnPVDStatusID.Value.ToString();
            }
        }
        if (PVDStatus != "")
        {
            PVDStatus = PVDStatus.Substring(1);
        }

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_GetSTCTracker(txtreferenceno.Text.Trim(), txtpvdnumber.Text.Trim(), PVDStatus, ddlDate.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text, ddlPaymentStatus.SelectedValue, userid);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

        BindTotal(dt);
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            //====================================STC Status Wise Sum.===============================================
            //string PendingSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Pending").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string ApprSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Approved").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string TradedSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Traded").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string ApPVD = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "PVD Applied").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string FailedSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Failed").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string BlankSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Blank").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //string VoidSTC = dt.AsEnumerable().Where(y => y.Field<string>("Status") == "Void").Sum(x => x.Field<int>("STCNumber1")).ToString();
            //===========================================End=========================================================

            lblPVDTotal.Text = dt.Compute("Count(GB_STCStatus)", string.Empty).ToString();

            //string Panels = dt.Compute("SUM(PanelQty)", string.Empty).ToString();
            //lblTotalPanels.Text = Panels == "" ? "0" : Panels;

            //string Inverter = dt.Compute("SUM(InverterQty)", string.Empty).ToString();
            //lblTotalInverters.Text = Inverter == "" ? "0" : Inverter;

            //string Amount = dt.Compute("SUM(InvoiceAmount)", string.Empty).ToString();
            //lblTotalAmount.Text = Amount == "" ? "0" : Amount;

            int STC = 0;
            int TotalSTC = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int STCNumber = 0;
                if (!string.IsNullOrEmpty(dt.Rows[i]["STCNumber"].ToString()))
                {
                    STCNumber = Convert.ToInt32(dt.Rows[i]["STCNumber"]);
                }

                STC = STCNumber;
                TotalSTC += STC;
            }

            lblTotalSTC.Text = TotalSTC.ToString();

            dt.Columns.Add("Stc_Value_Excel1", typeof(int));
            dt.Columns.Add("TotalSTCValue", typeof(decimal));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["Stc_Value_Excel1"] = dt.Rows[i]["Stc_Value_Excel"].ToString() == "" ? 0 : dt.Rows[i]["Stc_Value_Excel"];

                int STCNumber = 0;
                if (!string.IsNullOrEmpty(dt.Rows[i]["STCNumber"].ToString()))
                {
                    STCNumber = Convert.ToInt32(dt.Rows[i]["STCNumber"]);
                }

                dt.Rows[i]["TotalSTCValue"] = Convert.ToDecimal(STCNumber) * Convert.ToDecimal(dt.Rows[i]["StcValue"].ToString() == "" ? 0 : dt.Rows[i]["StcValue"]);
            }

            string TotalSTCValue = dt.Compute("SUM(TotalSTCValue)", string.Empty).ToString();
            if (!TotalSTCValue.Contains("0.00"))
            {
                decimal avg = (Convert.ToDecimal(TotalSTCValue) / TotalSTC);
                lblAvg.Text = avg.ToString("F");
            }
            else
            {
                lblAvg.Text = "0.00";
            }


            //GB Status
            string NewSubmissionSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "New Submission").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblNewSubmission.Text = dt.Select("GB_STCStatus = 'New Submission'").Length.ToString() + "/" + NewSubmissionSTC;

            string SubmittoTradeSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Submit to Trade").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblSubmittoTrade.Text = dt.Select("GB_STCStatus = 'Submit to Trade'").Length.ToString() + "/" + SubmittoTradeSTC;

            string PendingApprovalSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Pending Approval").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblPendingApproval.Text = dt.Select("GB_STCStatus = 'Pending Approval'").Length.ToString() + "/" + PendingApprovalSTC;

            string ReadyToCreateSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Ready To Create").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblReadyToCreate.Text = dt.Select("GB_STCStatus = 'Ready To Create'").Length.ToString() + "/" + ReadyToCreateSTC;

            string CERFailedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "CER Failed").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCERFailed.Text = dt.Select("GB_STCStatus = 'CER Failed'").Length.ToString() + "/" + CERFailedSTC;

            string CannotrecreateSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Cannot re-create").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCannotrecreate.Text = dt.Select("GB_STCStatus = 'Cannot re-create'").Length.ToString() + "/" + CannotrecreateSTC;

            string NotYetSubmittedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Not Yet Submitted").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblNotYetSubmitted.Text = dt.Select("GB_STCStatus = 'Not Yet Submitted'").Length.ToString() + "/" + NotYetSubmittedSTC;

            string ComplianceIssuesSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Compliance Issues").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblComplianceIssues.Text = dt.Select("GB_STCStatus = 'Compliance Issues'").Length.ToString() + "/" + ComplianceIssuesSTC;

            string CERApprovedSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "CER Approved").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblCERApproved.Text = dt.Select("GB_STCStatus = 'CER Approved'").Length.ToString() + "/" + CERApprovedSTC;

            string UnderReviewSTC = dt.AsEnumerable().Where(y => y.Field<string>("GB_STCStatus") == "Under Review").Sum(x => x.Field<int>("Stc_Value_Excel1")).ToString();
            lblUnderReview.Text = dt.Select("GB_STCStatus = 'Under Review'").Length.ToString() + "/" + UnderReviewSTC;

            string BlankSTC = dt.AsEnumerable().Where(y => y.Field<Nullable<Int32>>("GB_STCStatusID") == 10).Sum(x => x.Field<Nullable<Int32>>("Stc_Value_Excel1")).ToString();
            lblBlank.Text = dt.Select("GB_STCStatusID = '10'").Length.ToString() + "/" + BlankSTC;
        }
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //hdndelete.Value = id;
        //ModalPopupExtenderDelete.Show();
        //BindGrid(0);

        //--- do not chage this code end------
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        //--- do not change this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        //  ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtreferenceno.Text = string.Empty;
        txtpvdnumber.Text = string.Empty;
        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "STC Tracker_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 3, 4, 5, 6, 7, 8 };

            string[] arrHeader = { "STC ID", "STC REC", "STC Price", "PVD Number", "PVD Status", "STC Submission Date", "PaymentStatus" };

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
}