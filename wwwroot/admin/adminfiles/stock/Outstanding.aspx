﻿<%@ page language="C#" autoeventwireup="true" codefile="Outstanding.aspx.cs" inherits="admin_adminfiles_stock_Outstanding"
    culture="en-GB" uiculture="en-GB" masterpagefile="~/admin/templates/MasterPageAdmin.master" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<meta  http-equiv="Refresh" content="5"> --%>

    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }

        .focusred {
            border-color: #FF5F5F !important;
        }

        .autocomplete_completionListElement {
            z-index: 9999999 !important;
        }

        .paddtop5 {
            padding-top: 5px;
        }

        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <contenttemplate>
            <script>

                function toaster(msg) {
                    //alert("54345");
                    notifymsg(msg, 'inverse')
                }

                function notifymsg(message, type) {
                    $.growl({
                        message: message
                    }, {
                        type: type,
                        allow_dismiss: true,
                        label: 'Cancel',
                        className: 'btn-xs btn-inverse',
                        placement: {
                            from: 'top',
                            align: 'right'
                        },
                        delay: 30000,
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        offset: {
                            x: 30,
                            y: 30
                        }
                    });
                }

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            //alert("hide");
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });


                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    //alert("begin");
                    document.getElementById('loader_div').style.visibility = "visible";

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });

                    $("[data-toggle=tooltip]").tooltip();

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                }

                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }


            </script>

            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Outstanding
                            <div id="hbreadcrumb" class="pull-right">
                                <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" CausesValidation="false" CssClass="btn btn-primary purple btnaddicon">Add</asp:LinkButton>
                            </div>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2" style="width: 170px">
                                                    <asp:DropDownList ID="ddlCustomer" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlCreditAmount" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Credit</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlJobType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Job Type</asp:ListItem>
                                                        <asp:ListItem Value="1">STC</asp:ListItem>
                                                        <asp:ListItem Value="2">Cash</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDelivered" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="2">Not Dispatched</asp:ListItem>
                                                        <asp:ListItem Value="1">Dispatched</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                        <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                        <asp:ListItem Value="3">Delivered</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary">
                                                        <i class="fa-refresh fa"></i>Clear
                                                    </asp:LinkButton>
                                                </div>

                                                <div class="input-group col-sm-1 dnone">
                                                    <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" Visible="true">
                                                        <asp:ListItem Value="">Show</asp:ListItem>
                                                        <asp:ListItem Value="False">Not Received</asp:ListItem>
                                                        <asp:ListItem Value="True">Received</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838">
                                                    <i class="fa fa-file-excel-o"></i>Excel
                                                </asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>

                </asp:Panel>
            </div>

            <div class="finalgrid">
                <asp:Panel ID="panel" runat="server">
                    <div class="page-header card" id="divtot" runat="server" visible="false">
                        <div class="card-block brd_ornge">
                            <div class="printorder" style="font-size: medium">
                                <b>Credit Amount:&nbsp;</b><asp:Literal ID="lblTotCreditAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Inv. Amt:&nbsp;</b><asp:Literal ID="lblTotInvoiceAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Xero Amt:&nbsp;</b><asp:Literal ID="lblTotXeroAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Xero Amt Due:&nbsp;</b><asp:Literal ID="lblTotXeroAmtDue" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>STC OutRight:&nbsp;</b><asp:Literal ID="lblTotSTCOutRight" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Unreconciled Inv Amt:&nbsp;</b><asp:Literal ID="lblTotUnreconciledInvAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>STC Value:&nbsp;</b><asp:Literal ID="lblTotSTCValue" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Actual Outstanding:&nbsp;</b><asp:Literal ID="lblTotActualOutstanding" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <%--<b>Xero Outstanding:&nbsp;</b><asp:Literal ID="lblTotXeroActualOutstanding" runat="server"></asp:Literal>
                                &nbsp;&nbsp--%>
                                <b>Overdue Amt:&nbsp;</b><asp:Literal ID="lblTotOverdueAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                                <b>Over Limit Amt:&nbsp;</b><asp:Literal ID="lblTotOverLimitAmt" runat="server"></asp:Literal>
                                &nbsp;&nbsp
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                            OnSorting="GridView1_Sorting" OnRowCreated="GridView1_RowCreated1" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand"
                                            AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <columns>
                                                <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblCustomer" runat="server" Width="250px">
                                                            <%#Eval("Customer")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Credit Amt" SortExpression="CreditAmount">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblCreditAmount" runat="server" Width="40px">
                                                            <%#Eval("CreditAmount")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Invoice Amt" SortExpression="InvoiceAmount">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblInvoiceAmount" runat="server" Width="40px">
                                                            <%#Eval("InvoiceAmount")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Xero Amt" SortExpression="xeroTotalAmt">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblxeroTotalAmt" runat="server" Width="40px">
                                                            <%#Eval("xeroTotalAmt")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Xero Amt Due" SortExpression="xeroInvAmtDue">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblxeroInvAmtDue" runat="server" Width="40px">
                                                            <%#Eval("xeroInvAmtDue")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC OutRight" SortExpression="STCOutRight">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblSTCOutRight" runat="server" Width="40px">
                                                            <%#Eval("STCOutRight")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Unreconciled Inv Amt" SortExpression="UnreconciledInvAmt">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblUnreconciledInvAmt" runat="server" Width="40px">
                                                            <%#Eval("UnreconciledInvAmt")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC Value" SortExpression="STCValue">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblSTCValue" runat="server" Width="40px">
                                                            <%#Eval("STCValue")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STC Remaining" SortExpression="STCRemaining">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblSTCRemaining" runat="server" Width="40px">
                                                            <%#Eval("STCRemaining")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <headertemplate>
                                                        <asp:LinkButton Text="Actual Outstanding" SortExpression="ActualOutstanding" runat="server"
                                                            data-original-title="(InvoiceAmount - STCOutRight - UnreconciledInvAmt - STCRemaining)" data-toggle="tooltip" data-placement="top" />
                                                    </headertemplate>
                                                    <itemtemplate>
                                                        <asp:Label ID="lblActualOutstanding" runat="server" Width="40px">
                                                            <%#Eval("ActualOutstanding")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField >
                                                    <HeaderTemplate>
                                                        <asp:LinkButton Text="Xero Outstanding" SortExpression="ActualXeroOutstanding" runat="server" 
                                                            data-original-title="(InvoiceAmount - STCOutRight - UnreconciledInvAmt)" data-toggle="tooltip" data-placement="top" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActualXeroOutstanding" runat="server" Width="40px">
                                                            <%#Eval("ActualXeroOutstanding")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="Overdue Amt" SortExpression="OverDueAmt">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblOverDueAmt" runat="server" Width="40px">
                                                            <%#Eval("OverDueAmt")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <headertemplate>
                                                        <asp:LinkButton Text="Over Limit Amt" SortExpression="OverLimitAmt" runat="server"
                                                            data-original-title="(CreditAmount > (xeroInvAmtDue - STCOutRight - UnreconciledInvAmt - STCValue) Then 0 else (xeroInvAmtDue - STCOutRight - UnreconciledInvAmt - STCValue) - CreditAmount" data-toggle="tooltip" data-placement="top" />
                                                    </headertemplate>
                                                    <itemtemplate>
                                                        <asp:Label ID="lblOverLimitAmt" runat="server" Width="40px">
                                                            <%#Eval("OverLimitAmt")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pending Inv Days" SortExpression="PendingInvDays">
                                                    <itemtemplate>
                                                        <asp:Label ID="lblPendingInvDays" runat="server" Width="40px">
                                                            <%#Eval("PendingInvDays")%>
                                                        </asp:Label>
                                                    </itemtemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="30px">
                                                    <itemtemplate>
                                                        <asp:HyperLink ID="hypDetail" runat="server" CausesValidation="false" CssClass="btn btn-primary btn-mini" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/stock/OutstandingDetails.aspx?&custId=" + Eval("CustomerID") + "&jobType=" + ddlJobType.SelectedValue
                                                                + "&dateType=" + ddlDateType.SelectedValue + "&startDate=" + txtStartDate.Text + "&endDate=" + txtEndDate.Text + "&isDeduct=" + ddlDelivered.SelectedValue %>'>
                                                            <i class="fa fa-link"></i>Detail
                                                        </asp:HyperLink>
                                                    </itemtemplate>
                                                    <itemstyle cssclass="verticaaline" />
                                                </asp:TemplateField>
                                            </columns>
                                            <alternatingrowstyle />

                                            <pagertemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                <div class="pagination">

                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </pagertemplate>
                                            <pagerstyle cssclass="paginationGrid printorder" />
                                            <pagersettings mode="NumericFirstLast" firstpagetext="FIRST" lastpagetext="LAST" pagebuttoncount="4" />

                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>

            <!-- Start New Notes Popup-->
            <cc1:modalpopupextender id="ModalPopupExtenderAmount" runat="server" backgroundcssclass="modalbackground"
                dropshadow="false" popupcontrolid="ModelAdd" targetcontrolid="Button7"
                cancelcontrolid="LinkButton9">
            </cc1:modalpopupextender>
            <div id="ModelAdd" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalNewNote">Add Unreconciled  Invoice Amount
                                <span style="float: right" class="printorder" />
                                <asp:LinkButton ID="LinkButton9" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">
                                    Close
                                </asp:LinkButton>
                            </h5>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">
                                        <div class="qty marbmt25">
                                            <br />
                                            <div class="row">
                                                <asp:HiddenField runat="server" ID="hndInvoiceNo" />
                                                <div class="input-group col-sm-4 padd_btm10">
                                                    <asp:TextBox ID="txtInvoiceNo" runat="server" placeholder="Invoice No" CssClass="form-control m-b"></asp:TextBox>
                                                </div>
                                                 <div class="input-group col-sm-4 padd_btm10">
                                                    <asp:DropDownList ID="ddlUnReconciledType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">UnReconciled Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-4 padd_btm10">
                                                    <asp:TextBox ID="txtAmount" runat="server" placeholder="Unreconciled  Inv. Amt" CssClass="form-control m-b"></asp:TextBox>
                                                </div>
                                               
                                                <asp:HiddenField runat="server" ID="hndMode" />
                                                <asp:HiddenField runat="server" ID="hndID" />
                                                <div class="col-lg-12 padd_btm10">
                                                    <asp:LinkButton ID="lbtnSave" CssClass="btn btn-info" CausesValidation="false" runat="server" Text="Save" OnClick="lbtnSave_Click"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-12 padd_btm10 dnone" runat="server" id="DivNotes">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Stock</th>
                                                            <th>No Of Panel</th>
                                                            <th>Entered On</th>
                                                            <th>Entered By</th>
                                                            <th>Notes</th>
                                                            <th></th>
                                                        </tr>
                                                        <asp:Repeater ID="RptNewNotes" runat="server">
                                                            <itemtemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%# Container.ItemIndex + 1 %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("StockOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("NoOfPanels") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("CreatedOn") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("CreatedBy") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("Notes") %>
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                        <asp:LinkButton ID="lbtnEditNote" runat="server" CssClass="btn btn-warning btn-mini" CommandName="EditNotes" CommandArgument='<%#Eval("Id") %>'
                                                                            CausesValidation="false" data-original-title="Note" data-toggle="tooltip" data-placement="top" Visible='<%# Eval("EditFlag").ToString() == "1" || Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                                            <i class="btn-label fa fa-pencil"></i>Edit
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="DeleteNotes"
                                                                            CommandArgument='<%# Eval("Id") %>' Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'
                                                                            CausesValidation="false"
                                                                            OnClientClick="return confirm('Are you sure you want to delete?');">
                                                                            <i class="btn-label fa fa-close"></i>Delete
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </itemtemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button7" Style="display: none;" runat="server" />
            <!--End New Notes Popup-->
        </contenttemplate>
        <triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </triggers>
    </asp:UpdatePanel>
</asp:Content>
