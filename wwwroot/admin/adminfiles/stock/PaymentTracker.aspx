﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="PaymentTracker.aspx.cs" Inherits="admin_adminfiles_stock_Payment_Tracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <style type="text/css">
                .table {
                    margin-bottom: 0;
                }

                /*.bcolor {
                     border-color: red;
                 }*/
                .HeaderManualTitle {
                    padding-top: 8px;
                    font-size: larger;
                }
            </style>
            <style>
                .txt_height {
                    height: 100px;
                }

                .table tbody .brd_ornge td, .brd_ornge {
                    border-bottom: 3px solid #ff784f;
                }
            </style>
            <script>

                //function DoFocus(fld) {
                //    fld.className = 'form-control modaltextbox bcolor';
                //}

                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }

                function ShowProgress() {
                    setTimeout(function () {
                        //alert("check");
                        if (Page_IsValid) {
                            //alert("true");
                            $('.modal_popup').css('z-index', '901');
                            //this is because when popup is open, and loader is called background color becomes dark.
                            document.getElementById('loader_div').style.visibility = "visible";
                        }
                        else {
                            //alert("false");
                        }
                    }, 200);
                }
                $(function () {
                <%--    $('form').on("click",'#<%=btnOK3.ClientID %>', function () {
                        ShowProgress();
                    });
                     $('form').on("click",'#<%=lnkdelete.ClientID %>', function () {
                        ShowProgress();
                    }); --%>
                    $('form').on("click", '.POPupLoader', function () {
                        ShowProgress();
                    });
                });

                function toaster(msg = "Record Exist.") {
                    //alert("54345");
                    notifymsg(msg, 'inverse');
                }

                function notifymsg(message, type) {
                    $.growl({
                        message: message
                    }, {
                            type: type,
                            allow_dismiss: true,
                            label: 'Cancel',
                            className: 'btn-xs btn-inverse',
                            placement: {
                                from: 'top',
                                align: 'right'
                            },
                            delay: 30000,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            },
                            offset: {
                                x: 30,
                                y: 30
                            }
                        });
                }

                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    /* $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });*/
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    function toaster(msg) {
                        //alert("54345");
                        notifymsg(msg, 'inverse');
                    }

                    $(document).ready(function () {


                        $('[data-toggle="tooltip"]').tooltip({
                            trigger: 'hover'
                        });

                        function toaster(msg) {
                            //alert("54345");
                            notifymsg(msg, 'inverse');
                        }
                        //$(".myval").select2({
                        //    //placeholder: "select",
                        //    allowclear: true,
                        //    minimumResultsForSearch: -1
                        //});
                    });

                    callMultiCheckbox3();
                    callMultiCheckbox2();
                    callMultiCheckbox4();

                    $(".Vender .dropdown dt a").on('click', function () {
                        $(".Vender .dropdown dd ul").slideToggle('fast');
                    });
                    $(".PurchaseCompany .dropdown dt a").on('click', function () {
                        $(".PurchaseCompany .dropdown dd ul").slideToggle('fast');
                    });
                    $(".PaymentMethodType .dropdown dt a").on('click', function () {
                        $(".PaymentMethodType .dropdown dd ul").slideToggle('fast');
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox3();
                    });
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox2();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox4();
                    });

                }
                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }
            </script>
            <script>
                function callMultiCheckbox2() {
                    var title = "";
                    $("#<%=ddCompany.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel1').show();
                        $('.multiSel1').html(html);
                        $(".hida1").hide();
                    }
                    else {
                        $('#spanselect1').show();
                        $('.multiSel1').hide();
                    }

                }
            
                function callMultiCheckbox3() {
                    var title = "";
                    $("#<%=ddVender.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel2').show();
                        $('.multiSel2').html(html);
                        $(".hida2").hide();
                    }
                    else {
                        $('#spanselect2').show();
                        $('.multiSel2').hide();
                    }

                }

                function callMultiCheckbox4() {
                    var title = "";
                    $("#<%=ddPaymentMethodType.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel3').show();
                        $('.multiSel3').html(html);
                        $(".hida3").hide();
                    }
                    else {
                        $('#spanselect3').show();
                        $('.multiSel3').hide();
                    }

                }
            </script>


            <div class="page-body headertopbox printorder">
                <div class="card">
                    <div class="card-block">
                        <h5>Payment Tracker
                                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <%--<div id="hbreadcrumb" class="pull-right">            
                                            <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click"  CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>            
                                    </div>--%>
                        </h5>
                        <div class="clear"></div>
                    </div>
                </div>


            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    //$(".myval").select2({
                    //    //placeholder: "select",
                    //    allowclear: true,
                    //    minimumResultsForSearch: -1
                    //});


                }
            </script>
            <script>


                function doMyAction() {

                   <%-- $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();


                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });--%>



                }

            </script>

            <script>
                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                        function (isConfirm) {
                            if (isConfirm) {
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                eval(defaultAction);

                                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                return true;
                            } else {
                                // swal("Cancelled", "Your imaginary file is safe :)", "error");
                                return false;
                            }
                        });
                }
            </script>


            <div class="finaladdupdate printorder">

                <div id="PanAddUpdate" runat="server" visible="true">

                    <div class="panel-body animate-panel padtopzero stockorder">
                        <div class="card addform">
                            <div class="card-header bordered-blue">
                                <h5>
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Stock Order
                                </h5>
                            </div>
                            <%--  <style>
                                .div_block{ display:block; float:none; margin-top:-10px;}
                                .margin_set1{margin-top:30px;}
                                .set_w99 .select2-container--default .select2-selection--single .select2-selection__rendered{width:99%;}

                            </style>--%>
                        </div>
                    </div>
                </div>
            </div>


            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanSerialNo" runat="server" visible="false">
                                <i class="icon-info-sign"></i>
                                <asp:Literal ID="ltrerror" runat="server"></asp:Literal></literal></strong>
                            </div>
                        </div>


                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Order No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                        WatermarkText="Order No" />
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSearchOrderNo" FilterType="Numbers" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtstockitemfilter" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtstockitemfilter"
                                                        WatermarkText="Stock Item" />
                                                </div>
                                                <%--  <div class="input-group col-sm-2 max_width170">
                                                            <asp:TextBox ID="txtStockContainerFilter" runat="server" placeholder="Container No." CssClass="form-control m-b" MaxLength="50"></asp:TextBox>       
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStockContainerFilter"
                                                                WatermarkText="Container No." />
                                                        </div>--%>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                            <asp:DropDownList ID="ddlloc" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                                <asp:ListItem Value="">Location</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>--%>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                            <asp:DropDownList ID="ddlpurchasecompanysearch" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                                <asp:ListItem Value="">Purchase Company</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>--%>
                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Vendor</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect PurchaseCompany martop5 col-sm-1 max_width200 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida1" id="spanselect1">Company</span>
                                                                <p class="multiSel1"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddCompany" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptCompany" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnPurchaseCompanyName" runat="server" Value='<%# Eval("PurchaseCompanyName") %>' />
                                                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("Id") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("PurchaseCompanyName")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="form-group spical multiselect Vender martop5 col-sm-2 max_width170 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida2" id="spanselect2">Vender</span>
                                                                <p class="multiSel2"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddVender" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptVender" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnCustomer" runat="server" Value='<%# Eval("Customer") %>' />
                                                                                <asp:HiddenField ID="hdnCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltLocation" Text='<%# Eval("Customer")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Delivery Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Stock Not Received</asp:ListItem>
                                                        <asp:ListItem Value="0">Stock Received</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <%-- <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlitemnamesearch" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Payment Status</asp:ListItem>
                                                        <asp:ListItem Text="Dep Paid" Value="1" />
                                                        <asp:ListItem Text="Final Payment" Value="2" />
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <%--<div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlPaymentMethodType" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Payment Method Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <div class="form-group spical multiselect PaymentMethodType martop5 col-sm-2 max_width170 specail1_select">
                                                    <dl class="dropdown">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida3" id="spanselect3">Payment Method Type</span>
                                                                <p class="multiSel3"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddPaymentMethodType" runat="server">
                                                            <div class="mutliSelect" id="mutliSelect">
                                                                <ul>
                                                                    <asp:Repeater ID="rptPaymentMethodType" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnPaymentMethodType" runat="server" Value='<%# Eval("PaymentMethodType") %>' />
                                                                                <asp:HiddenField ID="hdnPaymentMethodTypeID" runat="server" Value='<%# Eval("PaymentMethodTypeID") %>' />

                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltPaymentMethodType" Text='<%# Eval("PaymentMethodType")%>'></asp:Literal>
                                                                                </label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170 dnone">
                                                    <asp:DropDownList ID="ddlPaymentStatus" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem" Width="150px">
                                                        <asp:ListItem Value="">Payment Status</asp:ListItem>
                                                        <asp:ListItem Text="Order Booked" Value="4" />
                                                        <asp:ListItem Text="Dep Paid" Value="1" />
                                                        <asp:ListItem Text="Final Payment" Value="2" />
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlPaymentMethod" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Payment Method</asp:ListItem>
                                                        <asp:ListItem Text="Cash" Value="1" />
                                                        <asp:ListItem Text="Credit" Value="2" />
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlLocalInternation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Order Type</asp:ListItem>
                                                        <asp:ListItem Value="0">Local</asp:ListItem>
                                                        <asp:ListItem Value="1">International</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlStockOrderStatus" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Stock Order Status</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                                        <asp:ListItem Value="0">OnHold</asp:ListItem>
                                                        <asp:ListItem Value="2">Void</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2 max_width170" >
                                                    <asp:DropDownList ID="ddlhideshow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Full Payment</asp:ListItem>
                                                        <%--<asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                                <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                                <asp:ListItem Value="3">ExpectedDate</asp:ListItem>--%>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlPayment" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Payment</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2" Selected="True">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlDeliveryType" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">Delivery Type</asp:ListItem>
                                                        <asp:ListItem Value="1">Warehouse</asp:ListItem>
                                                        <asp:ListItem Value="2">Direct</asp:ListItem>
                                                        <asp:ListItem Value="3">Third Party</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:DropDownList ID="ddlSearchGST_Type" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                        <asp:ListItem Value="">GST Type</asp:ListItem>
                                                        <asp:ListItem Value="1">With GST</asp:ListItem>
                                                        <asp:ListItem Value="2">Without GST</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Payment Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Due Date</asp:ListItem>
                                                        <asp:ListItem Value="3">Order Date</asp:ListItem>
                                                        <asp:ListItem Value="4">Delivery Date</asp:ListItem>
                                                        <asp:ListItem Value="5">ETA Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>

                                                <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="true" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>

                                                    <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                    <asp:LinkButton ID="lbtnExportV2" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                        CausesValidation="true" OnClick="lbtnExportV2_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel V2 </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="datashowbox inlineblock">
                                        <div class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div class="finalgrid ">
                    <asp:Panel ID="panelTot" runat="server">
                        <div class="page-header card" id="divtot" runat="server">
                            <div class="card-block brd_ornge">

                                <div class="printorder" style="font-size: large">
                                </div>


                                <div class="row">
                                    <div class="col-sm-2 max_width170">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myvalstockitem">
                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-10 HeaderManualTitle">
                                        Order (USD):&nbsp;<b><asp:Literal ID="lblttlordamt" runat="server"></asp:Literal>&nbsp;</b>
                                        Paid (USD):&nbsp;<b><asp:Literal ID="lblttlpaidamt" runat="server"></asp:Literal>&nbsp; </b>
                                        Remaining (USD):&nbsp;<b><asp:Literal ID="lblttlremamt" runat="server"></asp:Literal>&nbsp; </b>
                                        </br>
                                        Paid (AUD):&nbsp;<b><asp:Literal ID="lblPaidAud" runat="server"></asp:Literal>&nbsp; </b>
                                        AUD with GST:&nbsp;<b><asp:Literal ID="lblPaidAudGST" runat="server"></asp:Literal>&nbsp; </b>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <asp:HiddenField ID="hdnStockOrderID1" runat="server" />
                        <asp:HiddenField ID="hdnpmt" runat="server" />
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <div class="card-block">

                                        <div class="table-responsive BlockStructure">
                                            <asp:GridView ID="GridView1" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand"
                                                AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("OrderNumber") %>','tr<%# Eval("OrderNumber") %>');">
                                                                <asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />
                                                                <%--<img id='imgdiv<%# Eval("WholesaleOrderID") %>' src="../../../images/icon_plus.png" />--%>
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Order No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="OrderNumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Width="10px" CssClass="gridmainspan">
                                                                <%--<asp:HiddenField ID="hdnStockOrderID" runat="server" Value='<%# Eval("StockOrderID") %>' />
                                                        <asp:HiddenField ID="hdnactualdelivery" runat="server" Value='<%# Eval("ActualDelivery") %>' />--%>
                                                                <%-- <asp:HiddenField ID="hdnOrderStatus" runat="server" Value='<%# Eval("DeliveryOrderStatus") %>' />--%>
                                                                <%#Eval("OrderNumber")%>
                                                                <asp:HyperLink ID="hlDetails1" runat="server" Width="10px" NavigateUrl='<%# "~/admin/adminfiles/reports/order.aspx?id="+ Eval("StockOrderID").ToString()%>' Visible="false">
                                                                    <%#Eval("OrderNumber")%>
                                                                </asp:HyperLink>

                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="VendorInvoiceNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblinvoiceno" runat="server" Width="80px" data-placement="top" data-original-title='<%#Eval("VendorInvoiceNo")%>' data-toggle="tooltip">
                                                <%#Eval("VendorInvoiceNo")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="StockOrderStatus" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="StockOrderStatus" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ordst" runat="server" Width="80px" data-placement="top" data-toggle="tooltip">
                                                <%#Eval("StockOrderStatus1")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <%--<asp:TemplateField HeaderText="P.Company" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                SortExpression="PurchaseCompany">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hdnPurchaseCompanyName" Value='<%#Eval("PurchaseCompanyId")%>'/>
                                                    <asp:Label ID="lblPurchaseCompanyName" runat="server" Width="100px"><%#Eval("PurchaseCompanyName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <%--<asp:HiddenField runat="server" ID="hdnPurchaseCompanyName" Value='<%#Eval("PurchaseCompanyId")%>'/>--%>
                                                            <asp:Label ID="lblPurchaseCompanyName7878" runat="server" Width="100px"><%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ordered Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="DateOrdered">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "DateOrdered", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Exp.DeliveryDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="ExpectedDelivery" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblordDate" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "ExpectedDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Manual Order #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ManualOrderNumber">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label5" runat="server" Width="120px" data-placement="top" data-original-title='<%#Eval("ManualOrderNumber")%>' data-toggle="tooltip">
                                                <%#Eval("ManualOrderNumber")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                   
                                                   
                                                     <asp:TemplateField HeaderText="Delivery Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDeliveryDate" runat="server" Width="120px">
                                               
                                                             <%# DataBinder.Eval(Container.DataItem, "ActualDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Payment Method" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="ItemNameValue">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblPaymentMethod" runat="server" Width="50px"><%#Eval("PaymentMethod")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Currency" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Currency">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrency" runat="server" ><%#Eval("Currency")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="GST Type" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="GST_Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGST_Type" runat="server" ><%#Eval("GST_Type")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Qty">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnQty" Value='<%#Eval("Qty")%>' />
                                                            <asp:Label ID="Label8" runat="server" Width="50px" Text='<%#Eval("Qty")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amt" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="Amount">
                                                        <ItemTemplate>
                                                            <%--<asp:HiddenField runat="server" ID="hdnAmount" Value='<%#Eval("Amount")%>' />--%>
                                                            <asp:Label ID="lblAmount" runat="server" Width="50px">
                                                        <%#Eval("GSTAmount")%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="RemAmount">
                                                        <ItemTemplate>
                                                            <%--<asp:HiddenField runat="server" ID="hdnAmount" Value='<%#Eval("Amount")%>'/>--%>
                                                            <asp:Label ID="lblAmount1" runat="server" Width="50px">
                                                        <%#Eval("RemAmount")%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rem" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                        SortExpression="RemAmount">
                                                        <ItemTemplate>
                                                            <%--<asp:HiddenField runat="server" ID="hdnAmount" Value='<%#Eval("Amount")%>'/>--%>
                                                            <asp:Label ID="lblAmount198" runat="server" Width="50px">
                                                        <%#Eval("amt")%>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnPmt" runat="server" CssClass="btn btn-info btn-mini" CausesValidation="false" CommandName="Payment" CommandArgument='<%#Eval("OrderNumber") + ";" +Eval("GSTAmount")%>'>
                        <i class="fa fa-trash"></i> Payment
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>
                                                            <tr id='tr<%# Eval("OrderNumber") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("OrderNumber") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                            <tr>
                                                                                <td style="width: 15%;"><b>Purchase Company</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="lblPCompany" runat="server">
                                                                                     <%#Eval("PurchaseCompanyName")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 15%;"><b>Stock Order Status</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label4" runat="server">
                                                                                     <%#Eval("StockOrderStatus1")%></asp:Label>
                                                                                </td>

                                                                                <td style="width: 15%;"><b>Order Type</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label9" runat="server">
                                                                                     <%#Eval("DeliveryOrderStatusNew1")%></asp:Label>
                                                                                </td>

                                                                                <td style="width: 15%;"><b>Exp.DeliveryDate</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label3" runat="server">
                                                                                     <%#Eval("ExpectedDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%;"><b>Due Date</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label11" runat="server">
                                                                                     <%#Eval("paymentDueDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 15%;"><b>Payment Method Type</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label12" runat="server">
                                                                                     <%#Eval("ItemNameValue")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 15%;"><b>Items Ordered</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label13" runat="server">
                                                                                     <%#Eval("StockOrderItem")%></asp:Label>
                                                                                </td>
                                                                                <td style="width: 15%;"><b>Vendor</b>
                                                                                </td>
                                                                                <td style="width: 10%; text-align:left">
                                                                                    <asp:Label ID="Label14" runat="server">
                                                                                     <%#Eval("Vendor")%></asp:Label>
                                                                                </td>
                                                                            </tr>

                                                                        </table>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Danger Modal Templates-->
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <asp:Button ID="btnNull2" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divModalNote" TargetControlID="Button12" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divModalNote" runat="server" style="display: none" class="modal_popup ">
                <div class="modal-dialog" style="width: 100%; max-width: 1100px!important;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Add Payment Details</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="card">
                                <div class="card-block">
                                    <div class="form-group row">

                                        <div class="col-md-2">
                                            <asp:Label ID="lblPayMethodType" runat="server" class="disblock">
                                                Payment Type</asp:Label>
                                            <div class="input-group">
                                                <asp:DropDownList ID="ddlPMType" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="div_block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlPMType" InitialValue="" ErrorMessage="" ValidationGroup="PaymentReq"
                                                    ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-sm-2 custom_datepicker">
                                            <asp:Label ID="Label46" runat="server" class="disblock">
                                               Payment Date</asp:Label>
                                            <div class="input-group sandbox-container">

                                                <asp:TextBox ID="tctdepodate" runat="server" class="form-control">
                                                </asp:TextBox>

                                                <div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="div_block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                    ControlToValidate="tctdepodate" InitialValue="" ErrorMessage="" ValidationGroup="PaymentReq"
                                                    ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="col-md-2">
                                            <asp:Label ID="Label10" runat="server" class="disblock">
                                                GST Type</asp:Label>
                                            <div class="input-group">
                                                <asp:DropDownList ID="ddlGSTType" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">With GST</asp:ListItem>
                                                    <asp:ListItem Value="2">Without GST</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="div_block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlPMType" InitialValue="" ErrorMessage="" ValidationGroup="PaymentReq"
                                                    ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label47" runat="server" class="disblock">
                                                Rate</asp:Label>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtRate" onkeypress="return isNumberKey(event)" runat="server" class="form-control" OnTextChanged="txtRate_TextChanged"
                                                    AutoPostBack="true">
                                                </asp:TextBox>

                                            </div>
                                            <div class="div_block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                    ControlToValidate="txtRate" InitialValue="" ErrorMessage="" ValidationGroup="PaymentReq"
                                                    ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label48" runat="server" class="disblock">
                                                Amount USD</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtAmountUsd" OnTextChanged="txtAmountUsd_TextChanged" onkeypress="return isNumberKey(event)" AutoPostBack="true" runat="server" class="form-control">
                                                </asp:TextBox>

                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                            <div class="div_block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                    ControlToValidate="txtAmountUsd" InitialValue="" ErrorMessage="" ValidationGroup="PaymentReq"
                                                    ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label49" runat="server" class="disblock">
                                                Amount AUD</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtaud" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>




                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label65" runat="server" class="disblock">
                                                Payment Notes</asp:Label>
                                            <div class="input-group">

                                                <asp:TextBox ID="txtpaymentnotes" TextMode="MultiLine" Rows="3" Columns="6" runat="server" class="form-control" Style="height: 100px; max-width: 1000px;">
                                                </asp:TextBox>
                                                <%--<div class="input-group-addon">
                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="panel3" runat="server" CssClass="xsroll">
                                <div id="PanGridNotes" runat="server">
                                    <div class="card" style="max-height: 230px; overflow: auto">
                                        <div class="card-block">
                                            <div class="table-responsive BlockStructure">
                                                <asp:HiddenField ID="hndpmtDetailsId1" runat="server"></asp:HiddenField>
                                                <asp:HiddenField ID="hndWholesaleOrderID" runat="server"></asp:HiddenField>
                                                <asp:GridView ID="GridViewNote" DataKeyNames="stock_payment_DetailsId" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" AutoGenerateColumns="false" OnRowCommand="GridViewNote_RowCommand" OnRowUpdating="GridViewNote_RowUpdating" OnRowEditing="GridViewNote_RowEditing" OnRowDeleting="GridViewNote_RowDeleting" OnSelectedIndexChanged="GridViewNote_SelectedIndexChanged">
                                                    <Columns>
                                                        <%--<asp:TemplateField ItemStyle-Width="20px">
                                                                            <ItemTemplate>--%>
                                                        <%--<a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">--%>
                                                        <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />

                                                                                </a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>


                                                        <asp:TemplateField HeaderText="DepositDate" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                            SortExpression="DepositDate">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndpmtDetailsId" runat="server" Value='<%# Eval("stock_payment_DetailsId") %>' />
                                                                <asp:Label ID="lblNoteDate" runat="server" Width="50px"><%#Eval("DepositDate","{0:dd MMM yyyy}")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Rate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="UsdRate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Rate" runat="server" Width="80px">
                                                                                <%#Eval("UsdRate")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Deposit_amountUsd" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Deposit_amountUsd">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Note1" runat="server" Width="80px">
                                                                                <%#Eval("Deposit_amountUsd")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Deposit_amountAud" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Deposit_amountAsd">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Note" runat="server" Width="80px">
                                                                                <%#Eval("Deposit_amountAsd")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Payment Method type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="PaymentMethodtype">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPaymentMethodType" runat="server" Width="80px">
                                                                                <%#Eval("PaymentMethodtype")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Payment Note" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="UsdRate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="PaymentNotes" runat="server" Width="80px">
                                                                                <%#Eval("PaymentNotes")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("stock_payment_DetailsId")%>'>
                                                                    <i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnEdit" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Edit" CommandArgument='<%#Eval("stock_payment_DetailsId")%>'>
                                                                    <i class="fa fa-trash"></i> Edit
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnSave" runat="server" ValidationGroup="PaymentReq" type="button" class="btn btn-danger POPupLoader" Text="Save" Visible="false" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button ID="btnEdit" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Save" Visible="false" OnClick="btnEdit_Click"></asp:Button>
                            <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button12" Style="display: none;" runat="server" />

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <%-- <asp:PostBackTrigger ControlID="btndeliver" />--%>
            <%--    <asp:PostBackTrigger ControlID="btnaddnew" />        --%>
            <%-- <asp:PostBackTrigger ControlID="btnReset" />    --%>
            <%-- <asp:PostBackTrigger ControlID="btnCancel" />  --%>
            <%-- <asp:PostBackTrigger ControlID="btnAdd" />   --%>
            <%--<asp:PostBackTrigger ControlID="btnUpdate"/>--%>
            <%--<asp:PostBackTrigger ControlID="txtManualOrderNumber" />   
            <asp:PostBackTrigger ControlID="ibtnAddVendor" />
            <asp:PostBackTrigger ControlID="ibtnAddStock" />
             <asp:PostBackTrigger ControlID="txtBOLReceived" />--%>
            <%--<asp:PostBackTrigger ControlID="btnNewVendor" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="ddlVendor" EventName="SelectedIndexChanged" />
             <asp:PostBackTrigger ControlID="chkDeliveryOrderStatus"/>--%>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="lbtnExportV2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>



