﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="StockQuantityDeductReport.aspx.cs" Inherits="admin_adminfiles_stock_StockQuantityDeductReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <script>
        function toaster(msg) {
            //alert("54345");
            notifymsg(msg, 'transection')
        }

        function notifymsg(message, type) {
            $.growl({
                message: message
            }, {
                    type: type,
                    allow_dismiss: true,
                    label: 'Cancel',
                    className: 'btn-xs btn-inverse',
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    delay: 30000,
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    },
                    offset: {
                        x: 30,
                        y: 30
                    }
                });
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">

        <ContentTemplate>
            <div class="page-body headertopbox">
                <div class="card">
                    <div class="card-block">
                        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Quantity Report
                    <asp:Label runat="server" ID="lblStockItem" />
                            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                            <div id="hbreadcrumb" class="pull-right">
                                <a href="#" id="Add" runat="server" class="btn btn-warning" data-toggle="modal" data-target="#myModalAdd">Add</a>
                            </div>
                        </h5>

                    </div>
                </div>

            </div>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                }

                function pageLoadedpro() {

                    document.getElementById('loader_div').style.visibility = "hidden";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                    $('.sandbox-container input').datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });
                }

                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }

            </script>

            <div class="page-body padtopzero minheight500">
                <asp:Panel runat="server" ID="PanGridSearch" class="">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="row">

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Category</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtStockItem"
                                                        WatermarkText="Stock Item" />
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Location</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlMode" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Mode</asp:ListItem>
                                                        <asp:ListItem Value="Plus">Plus</asp:ListItem>
                                                        <asp:ListItem Value="Minus">Minus</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Audit Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group  sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group  sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon fullWidth" Text="Search" OnClick="btnSearch_Click" />

                                                </div>
                                                <div class="input-group col-sm-1 max_width170">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary fullWidth"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">

                                            <div class="col-sm-2 max_width170">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    OnClick="lbtnExport_Click" CausesValidation="true" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <div class="finalgrid">
                    <asp:Panel ID="PanTotal" runat="server" Visible="false">
                        <div class="page-header card" id="divtot" runat="server">
                            <div class="card-block brd_ornge">
                                <div class="printorder" style="font-size: medium">
                                    <b>Total Unused Serial No:
                                        <asp:Literal ID="lblTotal" runat="server"></asp:Literal></b>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="table-responsive  BlockStructure">
                                            <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo text-center table GridviewScrollItem table-bordered Gridview"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                                OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="StockItem" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="StockItem">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStockItem" runat="server" data-placement="top" data-original-title='<%#Eval("StockItem")%>' data-toggle="tooltip"><%#Eval("StockItem")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Model" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="StockModel">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStockModel" runat="server" data-placement="top" Width="100px"
                                                                data-original-title='<%#Eval("StockModel")%>' data-toggle="tooltip"><%#Eval("StockModel")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="CompanyLocation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompanyLocation" runat="server"><%#Eval("CompanyLocation")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Old Quantity" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="OldQuantity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOldQuantity" runat="server">
                                                                <%#Eval("OldQuantity")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="+ / -" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMinusQuantity" runat="server">
                                                                <%#Eval("MinusQuantity")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Mode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMode" runat="server">
                                                                <%#Eval("Mode")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Quantity" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="Quantity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblQuantity" runat="server">
                                                                <%#Eval("Quantity")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Audit By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="CreatedBy">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCreatedBy" runat="server">
                                                                <%#Eval("CreatedBy")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Audit Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="CreatedDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCreatedDate" runat="server">
                                                                <%#Eval("CreatedDate")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server">
                                                                <%#Eval("Status")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" SortExpression="Notes">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label6788" runat="server" data-placement="top" Width="500px"
                                                                data-original-title='<%#Eval("Notes")%>' data-toggle="tooltip"><%#Eval("Notes")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="UpdateStatus" CausesValidation="false" CssClass="btn btn-info btn-mini"
                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Status" CommandArgument='<%#Eval("ID")%>'
                                                                Visible='<%# Roles.IsUserInRole("Administrator") ? true : false %>'>
                                                                            <i class="fa fa-edit"></i> Status
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="left" CssClass="verticaaline" />
                                                    </asp:TemplateField>


                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid printorder" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderUpdateStatus" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStatus" TargetControlID="Button9" CancelControlID="btncancelpvd">
            </cc1:ModalPopupExtender>
            <div id="divStatus" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="HiddenField3" runat="server" />

                <div class="modal-dialog" style="width: 700px;">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header" style="justify-content: center;">
                            <h4 class="modal-title">Update Status</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="card-block padleft25">
                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-8">
                                            <span class="name disblock">
                                                <label>
                                                    Status
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlStatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                                        <asp:ListItem Value="Approved">Approved</asp:ListItem>
                                                        <asp:ListItem Value="Loss">Loss</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-4">
                                            <span class="name disblock">
                                                <label>
                                                    Amount
                                                </label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"
                                                    placeholder="Amount"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal row martop5">
                                        <div class="col-sm-12">
                                            <span class="name disblock">
                                                <label>
                                                    Notes
                                                </label>
                                            </span>
                                            <span>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtStutusNotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        placeholder="Notes" Height="100px"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtStutusNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars='""'>
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtStutusNotes"
                                                        FilterMode="InvalidChars" FilterType="Custom" InvalidChars="''">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" style="justify-content: center;">
                            <asp:Button ID="btnUpdateStatus" runat="server" type="button" class="btn btn-primary POPupLoader" data-dismiss="modal" Text="Update"
                                OnClick="btnUpdateStatus_Click"></asp:Button>
                            <asp:Button ID="btncancelpvd" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="Button9" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndQueryID" runat="server" />

            <!-- The Modal -->
            <div class="modal" id="myModalAdd">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Opening Qty Excel</h4>
                            <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="addexcel_start">
                                <ul>
                                    <li class="margin_right20">

                                        <div class="input-group margin_bottom0">
                                            <input type="text" class="form-control input-lg" disabled placeholder="Opening Qty: Upload Excel File">
                                            <span class="input-group-btn">
                                                <button class="browse btn btn-primary input-lg form-control" type="button"><i class="glyphicon glyphicon-search"></i>Browse</button>
                                            </span>
                                            <asp:HyperLink ToolTip="Download" ID="hypsubscriber" class="pull-right" CssClass="btn btn-blue" runat="server" NavigateUrl="~/userfiles/Format/Opening Quantity.xlsx" Style="padding-left: 5px; padding-right: 5px; margin-left: 5px;">
                                        <i class="fa fa-download"></i></asp:HyperLink>
                                        </div>
                                        <asp:FileUpload ID="ModuleFileUpload" runat="server" class="file" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="ModuleFileUpload"
                                            ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xls only" class="error_text"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="ModuleFileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>

                                    </li>

                                    <li>
                                        <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnOpening" runat="server"
                                            Text="Add" ValidationGroup="success1" OnClick="btnOpening_Click" />
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="btnOpening" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


