﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_TransportTracker : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        //StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        //SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            BindSearchRecord();
            BindCheckBox();

            BindEmployee();
            BindTransportType();
            BindGrid(0);
        }
    }

    #region Bind Search Filter Dropdown
    protected void BindSearchRecord()
    {
        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();
    }
    
    protected void BindEmployee()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblEmployees_select_WholeSaleEmp();
        ddlSearchEmployee.DataSource = dt;
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();
    }

    protected void BindCheckBox()
    {
        rpLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rpLocation.DataBind();

        rptCustomer.DataSource = ClstblContacts.tblCustType_SelectWholesaleVendor();
        rptCustomer.DataBind();
    }

    protected void BindTransportType()
    {
        DataTable dt = Clstbl_WholesaleOrders.tblTransportType_Select("2");
        ddlTransporttype.DataSource = dt;
        ddlTransporttype.DataTextField = "TransportTypeName";
        ddlTransporttype.DataValueField = "Id";
        ddlTransporttype.DataBind();
    }
    #endregion

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        //dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectBySearchNew(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), txtreferenceno.Text.Trim(), ddlSearchState.SelectedValue, "", ddlDeliveredOrNot.SelectedValue, ddlSearchTransportType.SelectedValue, ddlsearchInstallType.SelectedValue, ddlsearchinstallername.SelectedValue, ddlsearchJobStatus.SelectedValue, ddlsearchSolarType.SelectedValue, ddlSearchjobtype.SelectedValue, ddlSearchdeliveryoption.SelectedValue, ddlEmail.SelectedValue, ddlSearchEmployee.SelectedValue, ddlinvoicetype1.SelectedValue, txtConsignPerson.Text.Trim());

        string Location = GetLocation();
        string Customer = GetCustomer();

        dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Transporttracker_GetData(txtSearchOrderNo.Text, ddlInvoiceType.SelectedValue, ddlDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Location, ddlSearchjobtype.SelectedValue, txtReferenceNo.Text, ddlSearchdeliveryoption.SelectedValue, Customer, ddlSearchEmployee.SelectedValue, ddlCustDelivered.SelectedValue, ddlTransporttype.SelectedValue, Txtconsignno.Text, ddlPaid.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        divtot.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                Notification("There are no items to show in this view");
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divtot.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            divtot.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            //PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    #region GridView1

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Update")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            hndWholesaleOrderID.Value = WholesaleOrderID;

            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Get_IsCustDeliveredByWholesaleOrderID(WholesaleOrderID);

            if(dt.Rows.Count > 0)
            {
                ddlYesNo.SelectedValue = dt.Rows[0]["IsCustDelivered"].ToString();
            }

            ModalPopupExtenderDelivery.Show();
        }

        if (e.CommandName.ToString() == "Paid")
        {
            string WholesaleOrderID = e.CommandArgument.ToString();
            hdnVerifyWholesaleOrderID.Value = WholesaleOrderID;

            ModalPopupExtenderverify.Show();
            
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = GridView1.BottomPagerRow;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
        catch { }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
        }
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    #endregion

    public void Notification(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("toaster('{0}');", msg), true);
    }

    public void SetDelete()
    {
        // Notification("There are no items to show in this view");
        //PanSuccess.Visible = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        try
        {
            Export oExport = new Export();

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();

            string FileName = "TransportTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6,
                              7, 8, 9, 10, 11, 12, 13,
                              14, 0, 15, 16, 17, 18,
                              19, 20, 25, 26,
                              27, 21, 22, 23, 24,
                               };

            string[] arrHeader = { "Order No","Invoice No", "Invoice Type", "Invoice Date", "Stock For", "Job Type",                        "STC ID", "STC","Delivery Option","Customer","Qty","Delivered","Delivered By",                           "EmployeeName",
                                   "Id", "TransportType", "InstallType", "Consign/Person", "InstallerName",
                                   "Install Date", "Amount", "Frieght Charge", "Cust Frieght Charge",
                                   "RemainingCharge", "SolarType", "JobStatus", "OrderItem", "Cust Delivered",
                                   };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSearchOrderNo.Text = string.Empty;
        ddlInvoiceType.SelectedValue = "";
        txtReferenceNo.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlSearchjobtype.SelectedValue = "";
        ddlSearchdeliveryoption.SelectedValue = "";
        ddlCustDelivered.SelectedValue = "";
        ddlPaid.SelectedValue = "1";
        Txtconsignno.Text = string.Empty;

        ClearCheckBox();

        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }

    public void ClearCheckBox()
    {
        foreach (RepeaterItem item in rpLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        foreach (RepeaterItem item in rptCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
    }

    #region Get value from MultCheckBox
    protected string GetLocation()
    {
        string LocationItem = "";
        foreach (RepeaterItem item in rpLocation.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnLocationId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                LocationItem += "," + hdnModule.Value.ToString();
            }
        }
        if (LocationItem != "")
        {
            LocationItem = LocationItem.Substring(1);
        }

        return LocationItem;
    }

    protected string GetCustomer()
    {
        string CustomerItem = "";
        foreach (RepeaterItem item in rptCustomer.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnModule = (HiddenField)item.FindControl("hdnCustomerId");
            //Literal modulename = (Literal)item.FindControl("ltprojstatus");

            if (chkselect.Checked == true)
            {
                CustomerItem += "," + hdnModule.Value.ToString();
            }
        }
        if (CustomerItem != "")
        {
            CustomerItem = CustomerItem.Substring(1);
        }

        return CustomerItem;
    }
    #endregion

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hndWholesaleOrderID.Value;

        bool suc = false;
        bool sucNote = false;
        if (WholesaleOrderID != "" || WholesaleOrderID != null)
        {
            suc = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsCustDelivered(WholesaleOrderID, ddlYesNo.SelectedValue);

            sucNote = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_CustDeliveredNote(WholesaleOrderID, txtNotes.Text);

            ModalPopupExtenderDelivery.Hide();
        }

        if(suc)
        {
            Notification("Record Updated Successfully..");
        }
        BindGrid(0);
    }

    protected void BindTotal(DataTable dt)
    {
        if(dt.Rows.Count > 0)
        {
            lblTotalJob.Text = dt.Rows.Count.ToString();

            lblTotalAmount.Text = "0.00";
            lblTotalFriCharge.Text = "0.00";
            lblTotalCustFriCharge.Text = "0.00";

            if (dt.Compute("SUM(InvoiceAmount)", string.Empty).ToString() != "")
            {
                lblTotalAmount.Text = dt.Compute("SUM(InvoiceAmount)", string.Empty).ToString();
            }

            if (dt.Compute("SUM(FrieghtCharge)", string.Empty).ToString() != "")
            {
                lblTotalFriCharge.Text = dt.Compute("SUM(FrieghtCharge)", string.Empty).ToString();
            }

            if (dt.Compute("SUM(CustFrieghtCharge)", string.Empty).ToString() != "")
            {
                lblTotalCustFriCharge.Text = dt.Compute("SUM(CustFrieghtCharge)", string.Empty).ToString();
            }
        }
    }

    protected void lnkapproved_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hdnVerifyWholesaleOrderID.Value;

        bool UpdatePaid = false;
        if (WholesaleOrderID != "")
        {
            UpdatePaid = Clstbl_WholesaleOrders.tbl_WholesaleOrders_Update_IsPaidByWholesaleOrderID(WholesaleOrderID, "1");
        }

        if (UpdatePaid)
        {
            BindGrid(0);
        }
    }

}