﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RetailTransport.aspx.cs" Inherits="admin_adminfiles_stock_RetailTransport"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" EnableEventValidation="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table tbody .brd_ornge td, .brd_ornge {
            border-bottom: 3px solid #ff784f;
        }
    </style>
    <style> /*select ListBox*/
        .form-control:focus, .select2-container--focus {
            border: #ddd solid 1px !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            padding: 0px 12px !important;
        }

        .select2-container--default .select2-selection--multiple {
            padding: 0px !important;
        }

        .select2-container .select2-selection--multiple {
            min-height: 37px !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            margin-top: 4px;
            
        }
    </style>
    <script src="<%=SiteURL%>admin/theme/assets/js/select2.js"></script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modal_popup').css('z-index', '901');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    document.getElementById('loader_div').style.visibility = "visible";
                }
                else {
                    //alert("false");
                }
            }, 100);
        }
        $(function () {
            $('form').on("click", '.POPupLoader', function () {
                ShowProgress();
            });
        });

        function doMyAction() {

            $('#<%=btnAdd.ClientID %>').click(function (e) {
                formValidate();
            });

            $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();
            });
        }

    </script>
    <script>
        function printContent() {
<%--                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();--%>
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>

    <script>               
        //function openModal() {
        //    $('[id*=modal_danger]').modal('show');
        //}



        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);

        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);

        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {


            //shows the modal popup - the update progress
            document.getElementById('loader_div').style.visibility = "visible";

        }
        function endrequesthandler(sender, args) {

            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {
            document.getElementById('loader_div').style.visibility = "hidden";
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $("[data-toggle=tooltip]").tooltip();
            /* $('.datetimepicker1').datetimepicker({
                 format: 'DD/MM/YYYY'
             });*/
            $(".myvalstocktransfer").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true,
                minimumResultsForSearch: -1
            });

            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.sandbox-container input').datepicker({
                autoclose: true,
                todayHighlight: true
            });

            $(document).ready(function () {
                $('.js-example-basic-multiple').select2();
            });

            $(".myvalemployee").select2({
                allowclear: true,
                //minimumResultsForSearch: -1
            });
        }
    </script>

    <link rel="stylesheet" type="text/css" href="../../../admin/theme/assets/css/print.css" media="print" />
    <asp:UpdatePanel ID="updatepanelgrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="page-header card">
                <div class="card-block">
                    <h5>Retail Tranport
                                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>

                        <div id="hbreadcrumb" class="pull-right">

                            <asp:LinkButton ID="lnkAdd" runat="server" Visible="false" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-primary purple btnaddicon"> Add</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" Visible="false" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>

                        </div>
                    </h5>
                    <div class="clear"></div>


                </div>
            </div>
            <div class="supreme-container printorder">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="false">
                        <div class="panel-body animate-panel padtopzero">
                            <div class="card addform">
                                <div class="card-header bordered-blue">
                                    <h5>
                                        <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                        Retail Transport
                                    </h5>
                                </div>

                                <div class="card-block padleft25">
                                    <div class="form-horizontal">

                                        <div class="form-group row">

                                            <div class="col-md-2">
                                                <asp:Label ID="Label4" runat="server">Transport Date</asp:Label>
                                                <asp:TextBox ID="txtTransportDate" runat="server" ReadOnly="true" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblEnteredBy" runat="server">
                                                                Entered By</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlEmployee" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlEmployee" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblCompany" runat="server">
                                                                Company</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Company</asp:ListItem>
                                                        <asp:ListItem Value="1">Ariser</asp:ListItem>
                                                        <asp:ListItem Value="2">Solar Miner</asp:ListItem>
                                                        <asp:ListItem Value="3">Wholesale</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlCompany" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>

                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblTransferCompany" runat="server">
                                                                Transfer Company</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlTransportCompany" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Transport Company</asp:ListItem>
                                                    </asp:DropDownList>
                                                    
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlTransportCompany" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="Label1" runat="server">
                                                                Transport From</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlTransportFrom" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlTransportFrom" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblLocation" runat="server">
                                                                Location</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtLocation" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtLocation" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-md-2">
                                                <asp:Label ID="lable2" runat="server">Tracking No.</asp:Label>
                                                <asp:TextBox ID="txtTrackingNo" runat="server" MaxLength="200" class="form-control modaltextbox" ></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" ControlToValidate="txtTrackingNo"
                                                    Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-sm-5">
                                                <asp:Label ID="Label2" runat="server">
                                                                Project No/Invoice No</asp:Label>
                                                <div class="drpValidate">
                                                    <%--<asp:ListBox ID="lstProjectNo" runat="server" ToolTip="ProjectNo/InvoiceNo" SelectionMode="Multiple" CssClass="js-example-basic-multiple" AppendDataBoundItems="true">
                                                        
                                                    </asp:ListBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                                        ControlToValidate="lstProjectNo" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>--%>
                                                    <asp:ListBox ID="lstProjectNo" runat="server" SelectionMode="Multiple" CssClass="myvalemployee" ></asp:ListBox>
                                                    <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="lstProjectNo" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblQuantity" runat="server">
                                                                Quantity</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtQuantity" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtQuantity" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblWeight" runat="server">
                                                                Weight</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtWeight" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtWeight" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lblAmount" runat="server">
                                                                Amount</asp:Label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtAmount" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-sm-12">
                                                <asp:Label ID="Label5" runat="server">
                                                                Notes</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtNote" runat="server" MaxLength="200" class="form-control modaltextbox" Width="100%"
                                                        Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtNote"
                                                        Display="None" ValidationGroup="Req" ForeColor="#F47442"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group row">
                                            <div class="col-sm-10 text-center">
                                                <asp:Button CssClass="btn btn-info purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" ValidationGroup="Req" />
                                                <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" CausesValidation="true" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" ValidationGroup="Req" />
                                                <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class=" animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Tracking Number already exists.</strong>
                            </div>
                        </div>

                        <div class="searchfinal">
                            <div class="card pad10">
                                <div class="card-block ">
                                    <div class="inlineblock">
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                                            <div class="row">

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtTnsprtNo" runat="server" placeholder="Transport No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtTnsprtNo"
                                                        WatermarkText="Transport No" />
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTnsprtNo" FilterType="Numbers" />
                                                </div>

                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlSearchTransportFrom" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Location From</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                
                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlSearchTransferCompany" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Transfer Company</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtSearchLocation" runat="server" placeholder="Location" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchLocation"
                                                        WatermarkText="Location" />
                                                </div>

                                                <div class="input-group col-sm-2 max_width170">
                                                    <asp:TextBox ID="txtSearchTrackingNo" runat="server" placeholder="Tracking No" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchTrackingNo"
                                                        WatermarkText="Tracking No" />
                                                </div>

                                                <div class="input-group col-sm-2" style="max-width: 180px;">
                                                    <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                        <asp:ListItem Value="">Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Transport Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="date datetimepicker1 col-sm-2 martop5 custom_datepicker max_width170">
                                                    <div class="input-group sandbox-container">
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group col-sm-2" style="max-width: 200px;">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    &nbsp;&nbsp;
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                </div>

                                            </div>

                                        </asp:Panel>
                                    </div>
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="col-sm-2" style="max-width: 180px;">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalstocktransfer">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group martop5" style="width: 110px; padding: 0 5px;">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel fullWidth"
                                                    CausesValidation="false" OnClick="lbtnExport_Click" Style="background-color: #218838; border-color: #218838"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <div class="finalgrid">
                    <asp:Panel ID="panel" runat="server">
                        <div class="page-header card" id="divtot" runat="server" visible="false">
                            <div class="card-block brd_ornge">
                                <div class="printorder" style="font-size: medium">
                                    <b>Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Followmont:&nbsp;</b><asp:Literal ID="lblFollowmont" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>TNT:&nbsp;</b><asp:Literal ID="lblTNT" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Mainfreight:&nbsp;</b><asp:Literal ID="lblMainfreight" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>IN Person:&nbsp;</b><asp:Literal ID="lblINPerson" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                <b>Hi-Trans:&nbsp;</b><asp:Literal ID="lblHiTrans" runat="server"></asp:Literal>
                                    &nbsp;&nbsp
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                        <div class="xscroll">
                            <div id="PanGrid" runat="server">
                                <div class="card shadownone brdrgray">
                                    <div class="card-block">
                                        <div>
                                            <div class="table-responsive BlockStructure">
                                                <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                    OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" OnRowDataBound="GridView1_RowDataBound" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Tsprt No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" SortExpression="ID" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>

                                                                <%#Eval("ID")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="80px" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transport Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="TransferDate" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%# DataBinder.Eval(Container.DataItem, "TransportDate", "{0:dd MMM yyyy}") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Entered By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="EnteredBy">
                                                            <ItemTemplate>
                                                                <%#Eval("EnteredBy")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyName">
                                                            <ItemTemplate>
                                                                <%#Eval("CompanyName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transport Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90px" SortExpression="TransferCompany" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%#Eval("TransferCompany")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transport From" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                            <ItemTemplate>
                                                                <%#Eval("CompanyLocation")%>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="brdnoneleft" Width="130px" />
                                                            <HeaderStyle CssClass="brdnoneleft" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px" SortExpression="Location">
                                                            <ItemTemplate>
                                                                <%#Eval("Location")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Tracking No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90px" SortExpression="TrackingNo" HeaderStyle-CssClass="center-text">
                                                            <ItemTemplate>
                                                                <%#Eval("TrackingNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Quantity">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblQuantity" runat="server" data-placement="top" data-original-title='<%#Eval("Quantity")%>' data-toggle="tooltip"
                                                                    Width="40px"><%#Eval("Quantity")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Weight" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" SortExpression="Weight">
                                                            <ItemTemplate>
                                                                <asp:Literal runat="server" ID="ltrWeight" Text='<%# Eval("Weight")%>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" SortExpression="Amount">
                                                            <ItemTemplate>
                                                                <asp:Literal runat="server" ID="ltrAmount" Text='<%# Eval("Amount")%>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Pro/Inv No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label685" runat="server" data-placement="top" data-original-title='<%#Eval("ProjectNo")%>' data-toggle="tooltip"
                                                                    Width="40px"><%#Eval("ProjectNo")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Quantity">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNotes" runat="server" data-placement="top" data-original-title='<%#Eval("Notes")%>' data-toggle="tooltip"
                                                                    ><%#Eval("Notes")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="center"
                                                            ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" 
                                                                    CssClass="btn btn-info btn-mini" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i>Edit</asp:LinkButton>

                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("ID")%>' CssClass="btn btn-danger btn-mini" CausesValidation="false"><i class="fa fa-trash"></i>Delete
                                                                </asp:LinkButton>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>




                                                    </Columns>
                                                    <AlternatingRowStyle />
                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn btn-mini"><i class="fa fa-angle-double-left" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn btn-mini"><i class="fa fa-caret-left" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton btn-mini" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn"><i class="fa fa-caret-right" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid printorder" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                            <div class="paginationnew1" runat="server" id="divnopage">
                                                <table class="table table-bordered table-hover RemoveTopBorder" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </div>
            </div>

            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="LinkButton6" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup">

                <div class="modal-dialog">
                    <div class=" modal-content ">

                        <div class="modal-header text-center">
                            <h5 class="modal-title" id="H2">Delete</h5>

                        </div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger POPupLoader" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-danger" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL" CancelControlID="LinkButton5">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup modal-danger modal-message ">
                <div class="modal-dialog" style="max-width: 700px;">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title fullWidth" id="myModalLabel">Stock Order Detail
                                <span style="float: right" class="printorder" />

                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-danger btn-xs btncancelicon" data-dismiss="modal">Close
                                </asp:LinkButton>
                                <asp:LinkButton ID="gvbtnPrintmodal" runat="server" CssClass="btn btn-success btn-xs" OnClientClick="javascript:printContent();" CausesValidation="false">
                                                              <i class="fa fa-print"></i> Print
                                </asp:LinkButton>
                            </h5>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12">

                                        <table class="table table-bordered">
                                            <tr>
                                                <td align="left">
                                                    <strong>From&nbsp;Location:&nbsp;</strong>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                                                <td align="left"><strong>To&nbsp;Location:&nbsp;</strong></td>
                                                <td align="left">
                                                    <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                            </tr>

                                            <tr>
                                                <td align="left"><strong>Transfer Date:</strong> </td>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label></td>
                                                <td align="left"><strong>Transfer By:</strong> </td>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferBy" runat="server"></asp:Label></td>
                                            </tr>

                                        </table>
                                        <br />

                                        <div class="qty marbmt25">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                                <tr>
                                                    <th width="5%" align="center">Qty</th>
                                                    <th width="20%" align="left">Code</th>
                                                    <th align="left">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblQty" runat="server"><%#Eval("TransferQty") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                        <br />

                                        <div>
                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="printarea table table-bordered">
                                                <tr>
                                                    <td><strong>Total Qty :</strong>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td width="20%"><strong>Tracking No :</strong>
                                                    </td>
                                                    <td width="40%">
                                                        <asp:Label ID="lblTrackingNo" runat="server"></asp:Label></td>
                                                    <td width="20%"><strong>Received Date :</strong> </td>
                                                    <td>
                                                        <asp:Label ID="lblReceivedDate" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="2"><strong>Notes :</strong>
                                                    </td>


                                                    <td><strong>Received By :</strong></td>
                                                    <td>
                                                        <asp:Label ID="lblReceivedBy" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label></td>

                                                </tr>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <asp:HiddenField ID="hdndelete" runat="server" />
            <!--End Danger Modal Templates-->
            <%-- <asp:HiddenField runat="server" ID="hdnstocktransferid" />--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
</asp:Content>
