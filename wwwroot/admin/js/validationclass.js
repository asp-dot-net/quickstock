﻿function formValidate() {
    if (typeof (Page_Validators) != "undefined") {
        for (var i = 0; i < Page_Validators.length; i++) {
            if (!Page_Validators[i].isvalid) {
                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5370");
                $('#' + Page_Validators[i].controltovalidate).css("background", "#f9e8e8");
                $('#' + Page_Validators[i].controltovalidate).next().find('.select2-selection__rendered:first').attr("style", " border:1px solid #FF5370 !important;background:#f9e8e8;");
                //$('#' + Page_Validators[i].controltovalidate).next().find('.select2-selection__rendered:first').attr("style", " border:1px solid #FF5370 !important;color:#FF5370 !important;background:#f9e8e8;");
               // $('#' + Page_Validators[i].controltovalidate).next().find('.select2-selection__arrow b:first').attr("style", " border-color:#FF5370 transparent transparent transparent !important");
                $('#' + Page_Validators[i].controltovalidate + '_chzn').children('a').css("border", "1px solid #FF5370");             
            }
            else {
                var cs1 = $('#' + Page_Validators[i].controltovalidate).attr("class");
                if (cs1 !== 'req form-control') {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#' + Page_Validators[i].controltovalidate).css("background", "");
                    $('#' + Page_Validators[i].controltovalidate).next().find('.select2-selection__rendered:first').attr("style", " border:1px solid #ccc !important;color:#333 !important;background:none;");
                  //  $('#' + Page_Validators[i].controltovalidate).next().find('.select2-selection__arrow b:first').attr("style", " border-color:#333 transparent transparent transparent !important");
                    $('#' + Page_Validators[i].controltovalidate + '_chzn').children('a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
}