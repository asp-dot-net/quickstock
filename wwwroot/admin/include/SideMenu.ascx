﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideMenu.ascx.cs" Inherits="admin_include_SideMenu" %>


<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">

      <!--  <div class="pcoded-navigation-label">Navigation</div>-->
          <ul class="pcoded-item pcoded-left-item">
            <asp:Repeater runat="server" ID="menu1" OnItemDataBound="menu1_onitemdatabound" OnItemCommand="menu1_onitemcommand">
                <ItemTemplate>
                  <li id="limain" class="pcoded-hasmenu" runat="server">
                        <a id="mainUrl" runat="server" target="_blank" >
                           
                            
                            <span class="pcoded-micon"><i class='<%# Eval("description").ToString().Split(',').Count()>1?"menu-icon fa "+Eval("description").ToString().Split(',')[1]:""%>'></i><b>P</b></span>
                            <span class="pcoded-mtext">
                              <%# Eval("title")%>
                                 </span>
                              
                                  
                           
                            <span class="pcoded-mcaret"></span>
                            <asp:HiddenField ID="hdndesc1" Value='<%# Eval("description").ToString()%>' runat="server" />
                            <asp:HiddenField ID="hdnmain" runat="server" />
                        </a>
                        <ul id="UL1" class="pcoded-submenu" runat="server">
                              <asp:Repeater ID="repsubmenu" runat="server" OnItemDataBound="repsubmenu_OnItemDataBound">
                                <ItemTemplate>
                            <li id="lisub" class="" runat="server">
                                <a href='<%# Eval("Url")%>'  target="_blank" >
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">
                                        <%-- <a href='<%# Eval("Url") %>' Target="_blank" >--%>
                                            <%# Eval("title")%>
                                              <%--</a>--%>

                                    </span>
                                    <span class="pcoded-mcaret"></span>
                                    <asp:HiddenField ID="hdndesc" Value='<%# Eval("description").ToString()%>' runat="server" />
                                </a>
                            </li>
                            </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                      </li>
                       </ItemTemplate>
            </asp:Repeater>
        </ul>
     <%--   <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu active pcoded-trigger">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="active">
                        <a href="index.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Default</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="dashboard-ecommerce.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Ecommerce</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>

                    <li class=" ">
                        <a href="dashboard-analytics.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Analytics</span>
                            <span class="pcoded-badge label label-info ">NEW</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="ti-layout"></i><b>P</b></span>
                    <span class="pcoded-mtext">Page layouts</span>
                    <span class="pcoded-badge label label-warning">NEW</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" pcoded-hasmenu">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">Vertical</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class=" ">
                                <a href="menu-static.html">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Static Layout</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-header-fixed.html">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Header Fixed</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-compact.html">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Compact</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-sidebar.html">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Sidebar Fixed</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class=" pcoded-hasmenu">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">Horizontal</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class=" ">
                                <a href="menu-horizontal-static.html" target="_blank">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Static Layout</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-horizontal-fixed.html" target="_blank">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Fixed layout</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-horizontal-icon.html" target="_blank">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Static With Icon</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="menu-horizontal-icon-fixed.html" target="_blank">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext">Fixed With Icon</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class=" ">
                        <a href="menu-bottom.html">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">Bottom Menu</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="box-layout.html" target="_blank">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">Box Layout</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.html" target="_blank">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">RTL</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="ti-view-grid"></i><b>W</b></span>
                    <span class="pcoded-mtext">Widget</span>
                    <span class="pcoded-badge label label-danger">100+</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="widget-statistic.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Statistic</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="widget-data.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Data</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="widget-chart.html">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Chart Widget</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>--%>

      
   
       


      <%--  
                          <%--<ul id="UL1" runat="server" class="pcoded-submenu">
                          
                                    <li id="lisub" runat="server">

                                        <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>--%>

                                        <%--<asp:HyperLink ID="hypsubmenu" runat="server" NavigateUrl='<%# Eval("Url") %>' Target="_blank" Style="text-transform: capitalize;">--%>
                                                <%--<span class="pcoded-mtext">--%><%--</span>--%>
                                        <%--</asp:HyperLink>--%>

                                        <%--<asp:HiddenField ID="hdndesc" Value='<%# Eval("description").ToString()%>' runat="server" />--%>

                                        <%--  <span class="pcoded-mcaret"></span>--%>
                                       
                                    <%--</li>--%>
                                
                        <%--</ul>--%>
                  <%--  </li>--%>



                   <%-- <li id="limain" runat="server" class="pcoded-hasmenu pcoded-trigger">--%>


                        <%-- <asp:LinkButton ID="hdnheadermenu" runat="server" CommandArgument='<%# Eval("url") %>' Target="_blank" href='<%# Eval("url") %>'
                            CausesValidation="false">--%>
                        <%--<a href="javascript:void(0)">--%>
                            <%--<span class="pcoded-micon"><i class='<%# Eval("description").ToString().Split(',').Count()>1?"menu-icon fa "+Eval("description").ToString().Split(',')[1]:""%>'></i><b>D</b></span>--%>
                            <%-- <span class="pcoded-mtext">--%>
                            <%--<asp:HiddenField ID="hdndesc1" Value='<%# Eval("description").ToString()%>' runat="server" />
                            <asp:HiddenField ID="hdnmain" runat="server" />--%>

                       <%--     <%# Eval("title")%>
                        </a>--%>
                        <%--                                </span>--%>
                        <%-- </asp:LinkButton>--%>

                        <%--<asp:LinkButton href="javascript:void(0)" ID="lblarraow" runat="server" Visible="false"></asp:LinkButton>--%>


                      

                    <%--</li>--%>
             
    </div>
</nav>
<div class="page-sidebar " id="sidebar">
    <aside id="menu" class="printorder">
        <div id="navigation" class="printorder">
            <%--<div class="profile-picture">
         <%--   <a href="<%=Siteurl %>admin/adminfiles/dashboard.aspx">--%>
            <%-- <img src="<%=Siteurl %>admin/images/profile.jpg" class="img-circle m-b" alt="logo">--%>
            <%--            </a>
            <div class="stats-label text-color">--%>
            <%-- <span class="font-extra-bold font-uppercase">
                    <asp:Label ID="lblRolename" runat="server"></asp:Label></span>--%>

            <%--  <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">--%>
            <%--<small class="text-muted">
                           <%-- <asp:Label ID="lblUsserName" runat="server"></asp:Label><b class="caret"></b></small>--%>
            <%-- </a>--%>
            <%-- <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/admin/adminfiles/changepassword/Changepassword.aspx">Change Password</asp:HyperLink>
                        </li>
                        <li runat="server" visible="false"><a href="contacts.html">Contacts</a></li>
                        <li runat="server" visible="false"><a href="profile.html">Profile</a></li>
                        <li runat="server" visible="false"><a href="analytics.html">Analytics</a></li>
                        <li class="divider"></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus2" runat="server" LogoutText="Logout" Style="background: none; padding-right: 5px;"
                                OnLoggedOut="LoginStatus1_LoggedOut" LogoutAction="Redirect" />
                        </li>
                    </ul>--%>
            <%--     </div>--%>
            <%-- <div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <h4 class="font-extra-bold m-b-xs">$260 104,200
                    </h4>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>--%>
            <%--    </div>
        </div>--%>
        </div>
         </aside>
    
    </div>
      
   

