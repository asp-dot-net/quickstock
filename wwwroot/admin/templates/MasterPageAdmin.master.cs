using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class BossAdmin_MasterPageAdmin : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //   StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
        //string pagename = "";
        //string Url1 = Request.Url.PathAndQuery;
        //if(Url1!=null && Url1!="")
        //{
        //    string[] arr = Url1.Split('/');
        //    if (arr.Length > 3)
        //    {
        //        string Pagename = arr[4].ToString();
        //        string nm = Pagename.Substring(0, Pagename.LastIndexOf("?StockItemID"));
        //        pagename = Utilities.GetPageName(nm);
        //    }
        //    else
        //    {

        //    }
        //}
        // 
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery);
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1"); ;
        Siteurl = st.siteurl;
        if (!IsPostBack)
        {
            //StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");;
            //Siteurl = st.siteurl;
            SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            System.IO.FileInfo image = new System.IO.FileInfo(Request.PhysicalApplicationPath + "/userfiles/sitelogo/" + st.sitelogoupload);
            bool isfileUploaded = image.Exists;
        }

        if (!Request.IsAuthenticated)
        {
            Response.End();
            Response.Redirect("~/admin/Default.aspx");
        }

        switch (pagename.ToLower())
        {
            //case "dashboard":
            //    dash.Visible = true;
            //    Dashboard.Attributes.Add("class", "activePriNav");
            //    break;

            //case "adminutilities":
            //    divutility.Visible = true;
            //    hyputilities.CssClass = "menuOn";
            //    break;

            //case "adminsiteconfiguration":
            //     divutility.Visible = true;
            //     hypsiteconfig.CssClass = "menuOn";
            //    break;

            //case "announcement":
            //    divannouncement.Visible = true;
            //    hypannounctment.CssClass = "menuOn";
            //    liannouncement.Attributes.Add("class", "activePriNav");
            //    break;

            //case "news":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "newsdetail":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "demo":
            //    divdemo.Visible = true;
            //    liDemo.Attributes.Add("class", "activePriNav");
            //    hypdemo.CssClass = "menuOn";
            //    break;
        }


        if (pagename != "profile")
        {
            // secpage.Attributes.Add("class", "scrollable padder shdoimg");
        }
        else
        {
            //   secpage.Attributes.Add("class", "scrollable shdoimg");
        }


        //////title

        if (pagename == "profile")
        {
            pagetitle.Text = "QuickStock | Profile";
        }
        if (pagename == "dashboard")
        {
            pagetitle.Text = "QuickStock | Dashboard";
        }
        if (pagename == "setting")
        {
            pagetitle.Text = "QuickStock | Setting";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "QuickStock | Profile";
        }
        if (pagename == "help")
        {
            pagetitle.Text = "QuickStock | Help";
        }
        if (pagename == "project")
        {
            pagetitle.Text = "QuickStock | Project";
        }
        if (pagename == "installations")
        {
            pagetitle.Text = "QuickStock | Installation";
        }
        if (pagename == "invoicesissued")
        {
            pagetitle.Text = "QuickStock | Invoices Issued";
        }
        if (pagename == "invoicespaid")
        {
            pagetitle.Text = "QuickStock | Invoices Paid";
        }
        if (pagename == "lead")
        {
            pagetitle.Text = "QuickStock | Lead";
        }
        if (pagename == "lteamcalendar")
        {
            pagetitle.Text = "QuickStock | Team Calender";
        }
        if (pagename == "installercalendar")
        {
            pagetitle.Text = "QuickStock | Installer Calender";
        }


        #region master
        if (pagename == "companylocations")
        {
            pagetitle.Text = "QuickStock | Company Locations";
        }
        if (pagename == "custsource")
        {
            pagetitle.Text = "QuickStock | Company Source";
        }
        if (pagename == "custsourcesub")
        {
            pagetitle.Text = "QuickStock | Company Source Sub";
        }
        if (pagename == "CustType")
        {
            pagetitle.Text = "QuickStock | Company Type";
        }
        if (pagename == "elecdistributor")
        {
            pagetitle.Text = "QuickStock | Elec Distributor";
        }
        if (pagename == "elecretailer")
        {
            pagetitle.Text = "QuickStock | Elec Retailer";
        }
        if (pagename == "employee")
        {
            pagetitle.Text = "QuickStock | Employee";
        }
        if (pagename == "FinanceWith")
        {
            pagetitle.Text = "QuickStock | Finance With";
        }
        if (pagename == "housetype")
        {
            pagetitle.Text = "QuickStock | House Type";
        }
        if (pagename == "leadcancelreason")
        {
            pagetitle.Text = "QuickStock | Lead Cancel Reason";
        }
        if (pagename == "mtcereason")
        {
            pagetitle.Text = "QuickStock | Mtce Reasons";
        }
        if (pagename == "mtcereasonsub")
        {
            pagetitle.Text = "QuickStock | Mtce Reasons Sub";
        }
        if (pagename == "projectcancel")
        {
            pagetitle.Text = "QuickStock | Project Cancel";
        }
        if (pagename == "projectonhold")
        {
            pagetitle.Text = "QuickStock | Project On-Hold";
        }
        if (pagename == "projectstatus")
        {
            pagetitle.Text = "QuickStock | Project Status";
        }
        if (pagename == "projecttrackers")
        {
            pagetitle.Text = "QuickStock | Project Trackers";
        }
        if (pagename == "projecttypes")
        {
            pagetitle.Text = "QuickStock | Project Types";
        }
        if (pagename == "projectvariations")
        {
            pagetitle.Text = "QuickStock | Project Variations";
        }
        if (pagename == "promotiontype")
        {
            pagetitle.Text = "QuickStock | Promotion Type";
        }
        if (pagename == "prospectcategory")
        {
            pagetitle.Text = "QuickStock | Prospect Categories";
        }
        if (pagename == "roofangles")
        {
            pagetitle.Text = "QuickStock | Roof Angles";
        }
        if (pagename == "roottye")
        {
            pagetitle.Text = "QuickStock | Root Type";
        }
        if (pagename == "salesteams")
        {
            pagetitle.Text = "QuickStock | Sales Team";
        }
        if (pagename == "StockCategory")
        {
            pagetitle.Text = "QuickStock | Stock Category";
        }
        if (pagename == "transactiontypes")
        {
            pagetitle.Text = "QuickStock | Transaction Types";
        }
        if (pagename == "paymenttype")
        {
            pagetitle.Text = "QuickStock | Finance Payment Type";
        }
        if (pagename == "invcommontype")
        {
            pagetitle.Text = "QuickStock | Invoice Type";
        }
        if (pagename == "postcodes")
        {
            pagetitle.Text = "QuickStock | Post Codes";
        }
        if (pagename == "custinstusers")
        {
            pagetitle.Text = "QuickStock | Cust/Inst Users";
        }
        if (pagename == "newsletter")
        {
            pagetitle.Text = "QuickStock | News Letter";
        }
        if (pagename == "logintracker")
        {
            pagetitle.Text = "QuickStock | Login Tracker";
        }
        if (pagename == "leftempprojects")
        {
            pagetitle.Text = "QuickStock | Left Employee Projects";
        }
        if (pagename == "lteamtime")
        {
            pagetitle.Text = "QuickStock | Manage L-Team App Time";
        }
        if (pagename == "refundoptions")
        {
            pagetitle.Text = "QuickStock | Refund Options";
        }
        if (pagename == "PurchaseCompany")
        {
            pagetitle.Text = "QuickStock | Purchase Company Location";
        }
        #endregion

        if (pagename == "Company")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "QuickStock | Customer";
        }
        if (pagename == "contacts")
        {
            pagetitle.Text = "QuickStock | Contact";
        }

        if (pagename == "leadtracker")
        {
            pagetitle.Text = "QuickStock | Lead Tracker";
        }
        if (pagename == "gridconnectiontracker")
        {
            pagetitle.Text = "QuickStock | GridConnection Tracker.";
        }

        if (pagename == "lteamtracker")
        {
            pagetitle.Text = "QuickStock | Team Lead Tracker";
        }
        if (pagename == "stctracker")
        {
            pagetitle.Text = "QuickStock | STC Tracker";
        }
        if (pagename == "instinvoice")
        {
            pagetitle.Text = "QuickStock | Inst Invoice Tracker";
        }
        if (pagename == "salesinvoice")
        {
            pagetitle.Text = "QuickStock | Sales Invoice Tracker";
        }
        if (pagename == "installbookingtracker")
        {
            pagetitle.Text = "QuickStock | Install Booking Tracker";
        }
        if (pagename == "accrefund")
        {
            pagetitle.Text = "QuickStock | Refund";
        }
        if (pagename == "formbay")
        {
            pagetitle.Text = "QuickStock | FormBay";
        }
        if (pagename == "stockitem")
        {
            pagetitle.Text = "QuickStock | Stock Item";
        }
        if (pagename == "Stockdetail")
        {
            pagetitle.Text = "QuickStock | Stock Item Detail";
        }
        if (pagename == "ViewSerialHistory")
        {
            pagetitle.Text = "QuickStock | View Serial No History";
        }
        if (pagename == "stocktransfer")
        {
            pagetitle.Text = "QuickStock | Stock Transfer";
        }
        if (pagename == "stockorder")
        {
            pagetitle.Text = "QuickStock | Stock Order";
        }
        if (pagename == "StockOrderDelivery")
        {
            pagetitle.Text = "QuickStock | StockOrderDelivery";
        }
        if (pagename == "stockdeducted")
        {
            pagetitle.Text = "QuickStock | Stock Deduct";
        }
        if (pagename == "Wholesale")
        {
            pagetitle.Text = "QuickStock | Wholesale";
        }
        if (pagename == "company")
        {
            pagetitle.Text = "QuickStock | Customer";
        }
        if (pagename == "serialnumberreport")
        {
            pagetitle.Text = "QuickStock | Serial Number Wise";
        }
        if (pagename == "paninvreport")
        {
            pagetitle.Text = "QuickStock | Pan Inv Report";
        }
        if (pagename == "reconciliationreport")
        {
            pagetitle.Text = "QuickStock | Reconciliation Report";
        }
        if (pagename == "reconciliationreportnew")
        {
            pagetitle.Text = "QuickStock | Reconciliation Report";
        }
        if (pagename == "installerwisereport")
        {
            pagetitle.Text = "QuickStock | Stock Pending - Installer";
        }
        if (pagename == "installerwisereporttwo")
        {
            pagetitle.Text = "QuickStock | Stock Verification";
        }
        if (pagename == "stockwisereport")
        {
            pagetitle.Text = "QuickStock | Stock Wise Report";
        }
        if (pagename == "serialnohistoryreport")
        {
            pagetitle.Text = "QuickStock | Serial No History";
        }
        if (pagename == "stockitemlivereport")
        {
            pagetitle.Text = "QuickStock | Stockitem Live Report";
        }
        if (pagename == "stockitemlivereportdetailprojdeduct")
        {
            pagetitle.Text = "QuickStock | Stockitem Live Report Detail";
        }
        if (pagename == "stockitemlivereportdetailtnsfdeduct")
        {
            pagetitle.Text = "QuickStock | Stockitem Live Report Detail";
        }
        if (pagename == "stockitemlivereportdetailwholesalededuct")
        {
            pagetitle.Text = "QuickStock | Stockitem Live Report Detail";
        }
        if (pagename == "assignedstocktransfer")
        {
            pagetitle.Text = "QuickStock | Cancelled Stock ";
        }
        if (pagename == "wholesalededuct")
        {
            pagetitle.Text = "QuickStock | Wholesale Deduct";
        }
        if (pagename == "stockdeduct")
        {
            pagetitle.Text = "QuickStock | Stock Deduct";
        }
        if (pagename == "stockin")
        {
            pagetitle.Text = "QuickStock | Stock In";
        }
        if (pagename == "WholesaleIn")
        {
            pagetitle.Text = "QuickStock | Wholesale In";
        }
        if (pagename == "stockusage")
        {
            pagetitle.Text = "QuickStock | Stock Usage";
        }
        if (pagename == "stockorderreport")
        {
            pagetitle.Text = "QuickStock | Stock Order Report";
        }

        if (pagename == "promosend")
        {
            pagetitle.Text = "QuickStock | Promo Send";
        }

        if (pagename == "lead")
        {
            pagetitle.Text = "QuickStock | Lead";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "casualmtce")
        {
            pagetitle.Text = "QuickStock| Casual Maintenance";
        }
        if (pagename == "noinstalldate")
        {
            pagetitle.Text = "QuickStock | NoInstall Date Report";
        }
        if (pagename == "installdate")
        {
            pagetitle.Text = "QuickStock | Install Date Report";
        }
        if (pagename == "panelscount")
        {
            pagetitle.Text = "QuickStock | Panel Graph";
        }
        if (pagename == "accountreceive")
        {
            pagetitle.Text = "QuickStock | Account Receive Report";
        }
        if (pagename == "paymentstatus")
        {
            pagetitle.Text = "QuickStock | Payment Status Report";
        }
        if (pagename == "formbaydocsreport")
        {
            pagetitle.Text = "QuickStock | Formbay Docs Report";
        }
        if (pagename == "weeklyreport")
        {
            pagetitle.Text = "QuickStock | Weekly Report";
        }
        if (pagename == "leadassignreport")
        {
            pagetitle.Text = "QuickStock | Lead Assign Report";
        }
        if (pagename == "stockreport")
        {
            pagetitle.Text = "QuickStock | Stock Report";
        }
        if (pagename == "custproject")
        {
            pagetitle.Text = "QuickStock | Customer System Detail";
        }
        if (pagename == "customerdetail")
        {
            pagetitle.Text = "QuickStock | Customer System Detail";
        }
        if (pagename == "instavailable")
        {
            pagetitle.Text = "QuickStock | Installer Available";
        }
        if (pagename == "printinstallation")
        {
            pagetitle.Text = "QuickStock | Installations";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "QuickStock | Maintenance";
        }
        if (pagename == "stocktransferreportdetail")
        {
            pagetitle.Text = "QuickStock | Stock Transfer Report";
        }
        if (pagename == "promotracker")
        {
            pagetitle.Text = "QuickStock | Promo Tracker";
        }
        if (pagename == "unittype")
        {
            pagetitle.Text = "QuickStock | Unit Type";
        }
        if (pagename == "streettype")
        {
            pagetitle.Text = "QuickStock | Street Type";
        }
        if (pagename == "streetname")
        {
            pagetitle.Text = "QuickStock | Street Name";
        }
        if (pagename == "promooffer")
        {
            pagetitle.Text = "QuickStock | Promo Offer";
        }
        if (pagename == "updateformbayId")
        {
            pagetitle.Text = "QuickStock | updateformbayId";
        }
        if (pagename == "pickup")
        {
            pagetitle.Text = "QuickStock | Pick Up";
        }
        if (pagename == "paywaytracker")
        {
            pagetitle.Text = "QuickStock | PayWay";
        }
        if (pagename == "quickform")
        {
            pagetitle.Text = "QuickStock | Quick Form";
        }
        if (pagename == "clickcustomer")
        {
            pagetitle.Text = "QuickStock | Click Customer";
        }
        if (pagename == "stockdeductedreport")
        {
            pagetitle.Text = "QuickStock | Stock Deducted";
        }
        if (pagename == "WholesaleTracker")
        {
            pagetitle.Text = "QuickStock | Tracker";
        }
        if (pagename == "stockreceivedreport")
        {
            pagetitle.Text = "QuickStock | Stock Order Received Report";
        }
        if (pagename == "stockpredicationreport")
        {
            pagetitle.Text = "QuickStock | Stock Predication Report";
        }
        if (pagename == "pendingscanjob")
        {
            pagetitle.Text = "QuickStock | Pending Scan job Report";
        }
        if (pagename == "BrokenSerialNoReport")
        {
            pagetitle.Text = "QuickStock | Broken SerialNo Report";
        }

        if (pagename == "SwipeJobReport")
        {
            pagetitle.Text = "QuickStock | Swipe Job Report";
        }
        if (pagename == "stockreceivedreportDetailpage")
        {
            pagetitle.Text = "QuickStock | Stock Received Report Detail";
        }

        if (pagename == "stockitemwisereport")
        {
            pagetitle.Text = "QuickStock | Stock Item Wise Report";
        }
        if (pagename == "stockdeductreportDetailsPage")
        {
            pagetitle.Text = "QuickStock | stock Rceived Details Page";
        }

        if (pagename == "stockdeductrprt")
        {
            pagetitle.Text = "QuickStock | Stock Deduct Report";
        }

        if (pagename == "stockreceivedreportDetailpage")
        {
            pagetitle.Text = "QuickStock | Stock Deduct Details";
        }

        if (pagename == "stockreceivedreportDetailpage")
        {
            pagetitle.Text = "QuickStock | Stock Deduct Details";
        }

        if (pagename == "StockOrderNew")
        {
            pagetitle.Text = "QuickStock | StockOrderNew";
        }

        if (pagename == "PaymentTracker")
        {
            pagetitle.Text = "QuickStock | Payment Tracker";
        }

        if (pagename == "ContainerTracker")
        {
            pagetitle.Text = "QuickStock  | Container Tracker";
        }

        if (pagename == "StockPredictionReportDetails")
        {
            pagetitle.Text = "QuickStock  | Stock Predication Details";
        }

        if (pagename == "StockPendingProjectWiseReport")
        {
            pagetitle.Text = "QuickStock  | Stock Pending Project Wise";
        }

        if (pagename == "StockPendingItemWiseReport")
        {
            pagetitle.Text = "QuickStock  | Stock Pending Item Wise";
        }

        if (pagename == "StockPendingReportNew")
        {
            pagetitle.Text = "QuickStock  | Stock Pending New";
        }

        if (pagename == "UnUsedSerialNo")
        {
            pagetitle.Text = "QuickStock  | Unused Stock Serial Number";
        }

        if (pagename == "UnusedStockItemDetails")
        {
            pagetitle.Text = "QuickStock  | Unused Serial Number Details";
        }

        if (pagename == "TransportTracker")
        {
            pagetitle.Text = "QuickStock  | Transport Tracker";
        }

        if (pagename == "RetailTransport")
        {
            pagetitle.Text = "QuickStock  | Reatil Transport";
        }

        if (pagename == "PendingScan")
        {
            pagetitle.Text = "QuickStock  | Pending Scan";
        }

        if (pagename == "OtherScanJob")
        {
            pagetitle.Text = "QuickStock  | Others Scan Job";
        }

        if (pagename == "ManageReturnStock")
        {
            pagetitle.Text = "QuickStock  | Stock Query";
        }

        if (pagename == "StockQuantityDeductReport")
        {
            pagetitle.Text = "QuickStock  | Quantity Report";
        }

        if (pagename == "StokcLiveReportDetails")
        {
            pagetitle.Text = "QuickStock  | Stokc Live Report Details";
        }
    }


    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/admin/Default.aspx");
    }


}


