﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace App_Greenbot_AutoUpdate
{
    class ExceptionLogging
    {
        private static String exepurl;
        static SqlConnection con;

        private static void connecttion()
        {
            string connectionString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            con = new SqlConnection(connectionString);
            con.Open();
        }

        public static void SendExcepToDB(Exception exdb)
        {
            //exepurl = context.Current.Request.Url.ToString();
            connecttion();
            SqlCommand com = new SqlCommand("ExceptionLoggingToDataBase", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ExceptionMsg", exdb.Message.ToString());
            com.Parameters.AddWithValue("@ExceptionType", exdb.GetType().Name.ToString());
            //com.Parameters.AddWithValue("@ExceptionURL", exepurl);
            com.Parameters.AddWithValue("@ExceptionSource", exdb.StackTrace.ToString());
            com.ExecuteNonQuery();
        }
    }
}
