﻿using System;
using System.Data;
using System.Data.Common;

namespace App_Greenbot_AutoUpdate
{
    class ClsGreenbot
    {
        public static int SP_GreenbotAllData_BulkInsertUpdate(DataTable dt)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "SP_GreenbotAllData_BulkInsertUpdate";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@dt";
            if (dt != null)
                param.Value = dt;
            else
                param.Value = DBNull.Value;
            comm.Parameters.Add(param);

            int result = 0;
            try
            {
                result = DataAccess.ExecuteNonQuery(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
                ExceptionLogging.SendExcepToDB(ex);
            }
            return result;
        }

        public static DataTable tbl_APIFetchData_UtilitiesBySource(string APISource)
        {
            DbCommand comm = DataAccess.CreateCommand();
            comm.CommandText = "tbl_APIFetchData_UtilitiesBySource";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@APISource";
            if (!string.IsNullOrEmpty(APISource))
                param.Value = APISource;
            else
                param.Value = DBNull.Value;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);

            DataTable result = new DataTable();
            try
            {
                result = DataAccess.ExecuteSelectCommand(comm);
            }
            catch (Exception ex)
            {
                // log errors if any
                ExceptionLogging.SendExcepToDB(ex);
            }
            return result;
        }
    }
}
