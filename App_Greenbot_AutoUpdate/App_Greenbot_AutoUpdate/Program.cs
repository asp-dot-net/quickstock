﻿using System;
using System.Data;

namespace App_Greenbot_AutoUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dtSource = ClsGreenbot.tbl_APIFetchData_UtilitiesBySource("Greenbot");

            //jobsResponse.root jobData = getAsyc("arisesolar", "arisesolar1");
            //jobsResponse.root jobData = getAsyc("mac.solarminer@gmail.com", "sminer234");
            //jobsResponse.root jobData = getAsyc("achieversenergy", "achievers1");
            //jobsResponse.root jobData = getAsyc("ariseregistry", "arise980");
            //jobResponse.root jobData = CallApi.getAsyc("solarbridgeregistry", "solarbridge123");

            for (int i = 0; i < dtSource.Rows.Count; i++)
            {
                string companyId = dtSource.Rows[i]["CompanyID"].ToString();
                string uName = dtSource.Rows[i]["username"].ToString();
                string pwd = dtSource.Rows[i]["password"].ToString();
                string fromDate = dtSource.Rows[i]["FromDate"].ToString();

                jobResponse.root jobData = CallApi.GetAsyc(uName, pwd, fromDate);

                DataTable dtDetails = CallApi.ConvertClassToDataTable(jobData, companyId);

                if (dtDetails.Rows.Count > 0)
                {
                    int UpdateLog = ClsGreenbot.SP_GreenbotAllData_BulkInsertUpdate(dtDetails);
                }
            }
        }
    }
}
